<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Web (Frontend) Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    'login' => [
        'remember' => '保持登入',
        'login_submit' => '登入',
    ],

];