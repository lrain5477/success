<?php

namespace Minmax\Member\Events;

use Minmax\Base\Events\BaseEvent;
use Minmax\Member\Models\Member;
use Minmax\Notify\Events\EmailNotifyDefault;

/**
 * Class MemberRegistered
 */
class MemberRegistered extends BaseEvent
{
    use EmailNotifyDefault;

    protected $notifyCode = 'registered';

    /**
     * MemberRegistered constructor.
     *
     * @param  Member $member
     * @return void
     */
    public function __construct(Member $member)
    {
        $this->mailerParameters[] = $member;

        $this->emails = $member->email;

        parent::__construct();
    }
}
