<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class CreateMemberTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Member 會員帳號
        Schema::create('member', function (Blueprint $table) {
            $table->string('id', 64)->primary();
            $table->string('username')->unique()->comment('帳號');
            $table->string('password')->comment('密碼');
            $table->rememberToken();
            $table->string('name')->nullable()->comment('姓名');
            $table->string('email')->nullable()->comment('Email');
            $table->datetime('expired_at')->nullable()->comment('過期時間');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
            $table->softDeletes();
        });

        // MemberDetail 會員資料
        Schema::create('member_detail', function (Blueprint $table) {
            $table->string('member_id')->primary()->comment('會員ID');
            $table->json('name')->comment('會員姓名');                          // {full_name, family_name, given_name, nickname}
            $table->json('contact')->nullable()->comment('聯絡資訊');           // {email, mobile, tel, country, state, county, zip, city, address}
            $table->json('social')->nullable()->comment('社群連結');            // {facebook, instagram, twitter, ...}
            $table->json('profile')->nullable()->comment('個人資料');           // {avatar, gender, birthday, occupation, company, income, ...}
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('member')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // MemberRecord 會員紀錄
        Schema::create('member_record', function (Blueprint $table) {
            $table->string('member_id')->index()->comment('會員ID');
            $table->string('code')->comment('狀態代碼');
            $table->json('details')->nullable()->comment('詳細記錄');           // {tag, remark, ...}
            $table->timestamp('created_at')->useCurrent();
            $table->softDeletes();

            $table->foreign('member_id')->references('id')->on('member')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // MemberAuthentication 會員認證狀態
        Schema::create('member_authentication', function (Blueprint $table) {
            $table->string('member_id')->index()->comment('會員ID');
            $table->string('type')->comment('認證類型');
            $table->string('token')->primary()->comment('認證金鑰');
            $table->boolean('authenticated')->default(false)->comment('認證狀態');
            $table->timestamp('authenticated_at')->nullable()->comment('認證時間');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('member_id')->references('id')->on('member')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // MemberTerm
        Schema::create('member_term', function (Blueprint $table) {
            $table->string('id', 64)->primary();
            $table->string('title')->comment('標題');
            $table->longText('editor')->nullable()->comment('條款內容');
            $table->datetime('start_at')->nullable()->comment('開始時間');
            $table->datetime('end_at')->nullable()->comment('結束時間');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
            $table->softDeletes();
        });

        $this->insertSiteParameters();

        $this->insertExtensionColumns();

        $this->insertMemberTerm();

        $this->insertMemberRole();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->deleteMemberRole();

        Schema::dropIfExists('member_term');
        Schema::dropIfExists('member_authentication');
        Schema::dropIfExists('member_record');
        Schema::dropIfExists('member_detail');
        Schema::dropIfExists('member');
    }
    public function insertSiteParameters()
    {
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $startGroupId = $rowGroupId = SeederHelper::getTableNextIncrement('site_parameter_group');
        $rowGroupId--;
        $siteGroupData = [
            ['code' => 'gender', 'title' => 'site_parameter_group.title.' . ++$rowGroupId],
        ];

        DB::table('site_parameter_group')->insert($siteGroupData);

        // 多語系
        $siteGroupLanguage = [
            'zh-Hant' => [
                ['title' => '性別']
            ],
            'zh-Hans' => [
                ['title' => '性别']
            ],
            'ja' => [
                ['title' => '性別']
            ],
            'en' => [
                ['title' => 'Gender']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'site_parameter_group', $siteGroupLanguage, $languageList, $startGroupId, false);


        $startItemId = $rowItemId = SeederHelper::getTableNextIncrement('site_parameter_item');
        $rowItemId--;
        $siteItemData = [
            [
                'group_id' => $startGroupId,
                'value' => '1',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId,
                'value' => '0',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 2,
            ],
        ];

        DB::table('site_parameter_item')->insert($siteItemData);

        // 多語系
        $siteItemLanguage = [
            'zh-Hant' => [
                ['label' => '男'], ['label' => '女']
            ],
            'zh-Hans' => [
                ['label' => '男'], ['label' => '女']
            ],
            'ja' => [
                ['label' => '男'], ['label' => '女']
            ],
            'en' => [
                ['label' => 'Male'], ['label' => 'Female']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'site_parameter_item', $siteItemLanguage, $languageList, $startItemId, false);


        DB::table('language_resource')->insert($languageResourceData);
    }

    protected function insertExtensionColumns()
    {
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        // 欄位擴充
        $startExtensionId = $extensionRowId = SeederHelper::getTableNextIncrement('column_extension');
        $extensionRowId--;
        $columnExtensionData = [
            ['table_name' => 'member_detail', 'column_name' => 'name', 'sub_column_name' => 'first_name', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText'])],
            ['table_name' => 'member_detail', 'column_name' => 'name', 'sub_column_name' => 'family_name', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText'])],
            ['table_name' => 'member_detail', 'column_name' => 'name', 'sub_column_name' => 'full_name', 'sort' => 3, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText'])],
            ['table_name' => 'member_detail', 'column_name' => 'contact', 'sub_column_name' => 'email', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText', 'type' => 'email'])],
            ['table_name' => 'member_detail', 'column_name' => 'contact', 'sub_column_name' => 'phone', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText', 'crypt' => 'true', 'text_mask' => '6'])],
            ['table_name' => 'member_detail', 'column_name' => 'contact', 'sub_column_name' => 'mobile', 'sort' => 3, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText', 'crypt' => 'true', 'text_mask' => '4'])],
            ['table_name' => 'member_detail', 'column_name' => 'contact', 'sub_column_name' => 'zip', 'sort' => 4, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText', 'size' => '3'])],
            ['table_name' => 'member_detail', 'column_name' => 'contact', 'sub_column_name' => 'street', 'sort' => 5, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText', 'crypt' => 'true', 'text_mask' => '4'])],
            ['table_name' => 'member_detail', 'column_name' => 'contact', 'sub_column_name' => 'full_address', 'sort' => 6, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText', 'crypt' => 'true', 'text_mask' => '8'])],
            ['table_name' => 'member_detail', 'column_name' => 'social', 'sub_column_name' => 'facebook', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText'])],
            ['table_name' => 'member_detail', 'column_name' => 'social', 'sub_column_name' => 'google', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText'])],
            ['table_name' => 'member_detail', 'column_name' => 'profile', 'sub_column_name' => 'pid', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText', 'size' => '5', 'crypt' => 'true', 'text_mask' => '3'])],
            ['table_name' => 'member_detail', 'column_name' => 'profile', 'sub_column_name' => 'gender', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldRadio', 'siteParam' => 'gender', 'inline' => 'true'])],
            ['table_name' => 'member_detail', 'column_name' => 'profile', 'sub_column_name' => 'birthday', 'sort' => 3, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldDatePicker', 'size' => '3'])],
        ];

        DB::table('column_extension')->insert($columnExtensionData);

        // 多語系
        $columnExtensionLanguage = [
            ['title' => '名字'], ['title' => '姓氏'], ['title' => '全名'],
            ['title' => 'Email'], ['title' => '電話'], ['title' => '手機'], ['title' => '郵遞區號'], ['title' => '街道'], ['title' => '完整地址'],
            ['title' => 'Facebook'], ['title' => 'Google'],
            ['title' => '身分證字號'], ['title' => '性別'], ['title' => '生日']
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'column_extension', $columnExtensionLanguage, $languageList, $startExtensionId);


        DB::table('language_resource')->insert($languageResourceData);
    }

    protected function insertMemberTerm()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        try {
            $templateTermEditor = view('MinmaxMember::templates.data-member-term-editor')->render();
        } catch (\Throwable $e) {
            $templateTermEditor = '';
        }

        $insertTermData = [
            [
                'id' => $termId = uuidl(),
                'title' => 'member_term.title.' . $termId,
                'editor' => 'member_term.editor.' . $termId,
                'start_at' => $timestamp,
                'end_at' => null,
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
            ],
        ];
        DB::table('member_term')->insert($insertTermData);

        // 多語系
        $termLanguage = [
            'zh-Hant' => [
                ['id' => $termId, 'title' => '會員服務條款', 'editor' => $templateTermEditor]
            ],
            'zh-Hans' => [
                ['id' => $termId, 'title' => '会员服务条款', 'editor' => $templateTermEditor]
            ],
            'ja' => [
                ['id' => $termId, 'title' => '会員の利用規約', 'editor' => $templateTermEditor]
            ],
            'en' => [
                ['id' => $termId, 'title' => 'Terms of Service', 'editor' => $templateTermEditor]
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'member_term', $termLanguage, $languageList, null, false);

        DB::table('language_resource')->insert($languageResourceData);
    }

    public function insertMemberRole()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $startRoleId = $rowRoleId = SeederHelper::getTableNextIncrement('roles');
        $rowRoleId--;

        // 新增權限角色
        $rolesData = [
            [
                'guard' => 'web', 'name' => 'default', 'display_name' => 'roles.display_name.' . ++$rowRoleId,
                'description' => 'roles.description.' . $rowRoleId, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];
        DB::table('roles')->insert($rolesData);

        $roleLanguage = [
            'zh-Hant' => [
                ['display_name' => '一般會員', 'description' => '一般會員']
            ],
            'zh-Hans' => [
                ['display_name' => '一般会员', 'description' => '一般会员']
            ],
            'ja' => [
                ['display_name' => '一般会員', 'description' => '一般会員']
            ],
            'en' => [
                ['display_name' => 'Default', 'description' => 'Default member']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'roles', $roleLanguage, $languageList, $startRoleId, false);

        DB::table('language_resource')->insert($languageResourceData);
    }

    public function deleteMemberRole()
    {
        $uriSet = ['default'];

        DB::table('roles')->where('guard', 'web')->whereIn('name', $uriSet)->delete();
    }
}
