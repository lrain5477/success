<?php

namespace Minmax\Member\Web;

use Minmax\Base\Web\ColumnExtensionRepository;
use Minmax\Base\Web\Repository;
use Minmax\Member\Models\Member;

/**
 * Class MemberRepository
 * @property Member $model
 * @method Member find($id)
 * @method Member one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method Member create($attributes)
 * @method Member save($model, $attributes)
 * @method Member|\Illuminate\Database\Eloquent\Builder query()
 */
class MemberRepository extends Repository
{
    const MODEL = Member::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'member';
    }

    /**
     * Search by primary key and hide some private data
     *
     * @param  mixed $id
     * @return mixed
     */
    public function findAndHide($id)
    {
        if ($member = $this->find($id)) {
            $subColumns = (new ColumnExtensionRepository)->all(['table_name' => 'member_detail', 'active' => true]);

            $member->load('memberDetail');

            $member->memberDetail->name = transform($member->memberDetail->name, function ($nameSet) use ($subColumns) {
                foreach ($nameSet as $column => $value) {
                    if ($subColumn = $subColumns->where('column_name', 'name')->where('sub_column_name', $column)->first()) {
                        if (array_get($subColumn->options, 'crypt') == 'true') {
                            $result = base64_decode($value);
                            $nameSet[$column] = $result == false ? $value : $result;
                        }
                    }
                }
                return $nameSet;
            });

            $member->memberDetail->contact = transform($member->memberDetail->contact, function ($contactSet) use ($subColumns) {
                foreach ($contactSet as $column => $value) {
                    if ($subColumn = $subColumns->where('column_name', 'contact')->where('sub_column_name', $column)->first()) {
                        if (array_get($subColumn->options, 'crypt') == 'true') {
                            $result = base64_decode($value);
                            $contactSet[$column] = $result == false ? $value : $result;
                        }
                    }
                }
                return $contactSet;
            });

            $member->memberDetail->social = transform($member->memberDetail->social, function ($socialSet) use ($subColumns) {
                foreach ($socialSet as $column => $value) {
                    if ($subColumn = $subColumns->where('column_name', 'social')->where('sub_column_name', $column)->first()) {
                        if (array_get($subColumn->options, 'crypt') == 'true') {
                            $result = base64_decode($value);
                            $socialSet[$column] = $result == false ? $value : $result;
                        }
                    }
                }
                return $socialSet;
            });

            $member->memberDetail->profile = transform($member->memberDetail->profile, function ($profileSet) use ($subColumns) {
                foreach ($profileSet as $column => $value) {
                    if ($subColumn = $subColumns->where('column_name', 'profile')->where('sub_column_name', $column)->first()) {
                        if (array_get($subColumn->options, 'crypt') == 'true') {
                            $result = base64_decode($value);
                            $profileSet[$column] = $result == false ? $value : $result;
                        }
                    }
                }
                return $profileSet;
            });

            return $member;
        }

        return null;
    }
}
