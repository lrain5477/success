<?php

namespace Minmax\Member;

use Illuminate\Foundation\Support\Providers\EventServiceProvider;

class ServiceProvider extends EventServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Minmax\Member\Events\MemberRegistered' => [
            'Minmax\Notify\Listeners\EmailNotification',
        ],
    ];

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/admin.php');
        $this->loadRoutesFrom(__DIR__ . '/routes/administrator.php');
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/translations', 'MinmaxMember');
        $this->loadViewsFrom(__DIR__ . '/views', 'MinmaxMember');

        parent::boot();
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }
}
