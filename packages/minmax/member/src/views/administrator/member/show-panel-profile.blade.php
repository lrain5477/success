<?php
/**
 * @var \Minmax\Base\Models\Administrator $adminData
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 * @var \Minmax\Member\Models\Member $formData
 */
?>

@inject('modelPresenter', 'Minmax\Member\Administrator\MemberDetailPresenter')

<fieldset id="accountFieldSet">
    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxMember::models.MemberDetail.name')</legend>

    {!! $modelPresenter->getShowColumnExtension($formData->memberDetail, 'name') !!}

</fieldset>

<fieldset class="mt-4">
    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxMember::models.MemberDetail.contact')</legend>

    {!! $modelPresenter->getShowColumnExtension($formData->memberDetail, 'contact') !!}

</fieldset>

<fieldset class="mt-4">
    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxMember::models.MemberDetail.social')</legend>

    {!! $modelPresenter->getShowColumnExtension($formData->memberDetail, 'social') !!}

</fieldset>

<fieldset class="mt-4">
    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxMember::models.MemberDetail.profile')</legend>

    {!! $modelPresenter->getShowColumnExtension($formData->memberDetail, 'profile') !!}

</fieldset>
