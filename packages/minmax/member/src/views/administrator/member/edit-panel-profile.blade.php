<?php
/**
 * @var \Minmax\Base\Models\Administrator $adminData
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 * @var \Minmax\Member\Models\Member $formData
 */
?>

<form id="profileForm" class="form-horizontal validate editForm" name="profileForm"
      action="{{ langRoute("administrator.{$pageData->uri}-detail.update", ['id' => $formData->id]) }}"
      method="post"
      enctype="multipart/form-data">
    @method('PUT')
    @csrf

    @inject('modelPresenter', 'Minmax\Member\Administrator\MemberDetailPresenter')

    <fieldset id="accountFieldSet">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxMember::models.MemberDetail.name')</legend>

        {!! $modelPresenter->getFieldColumnExtension($formData->memberDetail, 'name') !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxMember::models.MemberDetail.contact')</legend>

        {!! $modelPresenter->getFieldColumnExtension($formData->memberDetail, 'contact') !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxMember::models.MemberDetail.social')</legend>

        {!! $modelPresenter->getFieldColumnExtension($formData->memberDetail, 'social') !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxMember::models.MemberDetail.profile')</legend>

        {!! $modelPresenter->getFieldColumnExtension($formData->memberDetail, 'profile') !!}

    </fieldset>

    <div class="text-center my-4 form-btn-group">
        <input class="btn btn-main" type="submit" id="submitBut" value="@lang('MinmaxBase::administrator.form.button.send')">
        <input class="btn btn-default" type="reset" value="@lang('MinmaxBase::administrator.form.button.reset')" onclick="window.location.reload(true)">
    </div>
</form>

@push('scripts')
<script>
(function ($) {
    $(function () {
        $('#profileForm input')
            .each(function () {
                var $input = $(this), inputType = ['text', 'tel', 'email', 'date'];
                if (inputType.indexOf($input.attr('type')) !== -1) {
                    $input.attr('data-name', $input.attr('name')).removeAttr('name');
                }
            })
            .on('change', function () {
                var $input = $(this), inputType = ['text', 'tel', 'email', 'date'];
                if (inputType.indexOf($input.attr('type')) !== -1) {
                    if ($input.val().search('Ｏ') !== -1 && $input.is('[name]')) {
                        $input.attr('data-name', $input.attr('name')).removeAttr('name');
                    } else if ($input.val().search('Ｏ') === -1 && $input.is('[data-name]')) {
                        $input.attr('name', $input.attr('data-name')).removeAttr('data-name');
                    }
                }
            });
    });
})(jQuery);
</script>
@endpush
