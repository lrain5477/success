<?php

namespace Minmax\Member\Administrator;

use Minmax\Base\Administrator\ColumnExtensionRepository;
use Minmax\Base\Administrator\Repository;
use Minmax\Member\Events\MemberRegistered;
use Minmax\Member\Models\Member;

/**
 * Class MemberRepository
 * @property Member $model
 * @method Member find($id)
 * @method Member one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method Member|\Illuminate\Database\Eloquent\Builder query()
 * @method Member saveLanguage($model, $columns = [])
 */
class MemberRepository extends Repository
{
    const MODEL = Member::class;

    protected $roleSelected = [];

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'member';
    }

    /**
     * Search by primary key and hide some private data
     *
     * @param  mixed $id
     * @return mixed
     */
    public function findAndHide($id)
    {
        if ($member = $this->find($id)) {
            $subColumns = (new ColumnExtensionRepository)->all(['table_name' => 'member_detail', 'active' => true]);

            $member->load('memberDetail');

            $member->memberDetail->name = transform($member->memberDetail->name, function ($nameSet) use ($subColumns) {
                foreach ($nameSet as $column => $value) {
                    if ($subColumn = $subColumns->where('column_name', 'name')->where('sub_column_name', $column)->first()) {
                        if (array_get($subColumn->options, 'crypt') == 'true') {
                            $result = minmaxDecrypt($value);
                            $nameSet[$column] = $result == false ? $value : $result;
                        }
                        if ($mask = array_get($subColumn->options, 'text_mask')) {
                            $maskSetting = isset($mask) ? explode(',', $mask) : [];
                            if (array_key_exists(0, $maskSetting)) $maskSetting[0] = intval($maskSetting[0]);
                            if (array_key_exists(1, $maskSetting)) $maskSetting[1] = intval($maskSetting[1]);
                            if (array_key_exists(2, $maskSetting)) $maskSetting[2] = intval($maskSetting[2]);
                            $nameSet[$column] = str_cover($nameSet[$column], ...$maskSetting);
                        }
                    }
                }
                return $nameSet;
            });

            $member->memberDetail->contact = transform($member->memberDetail->contact, function ($contactSet) use ($subColumns) {
                foreach ($contactSet as $column => $value) {
                    if ($subColumn = $subColumns->where('column_name', 'contact')->where('sub_column_name', $column)->first()) {
                        if (array_get($subColumn->options, 'crypt') == 'true') {
                            $result = minmaxDecrypt($value);
                            $contactSet[$column] = $result == false ? $value : $result;
                        }
                        if ($mask = array_get($subColumn->options, 'text_mask')) {
                            $maskSetting = isset($mask) ? explode(',', $mask) : [];
                            if (array_key_exists(0, $maskSetting)) $maskSetting[0] = intval($maskSetting[0]);
                            if (array_key_exists(1, $maskSetting)) $maskSetting[1] = intval($maskSetting[1]);
                            if (array_key_exists(2, $maskSetting)) $maskSetting[2] = intval($maskSetting[2]);
                            $contactSet[$column] = str_cover($contactSet[$column], ...$maskSetting);
                        }
                    }
                }
                return $contactSet;
            });

            $member->memberDetail->social = transform($member->memberDetail->social, function ($socialSet) use ($subColumns) {
                foreach ($socialSet as $column => $value) {
                    if ($subColumn = $subColumns->where('column_name', 'social')->where('sub_column_name', $column)->first()) {
                        if (array_get($subColumn->options, 'crypt') == 'true') {
                            $result = minmaxDecrypt($value);
                            $socialSet[$column] = $result == false ? $value : $result;
                        }
                        if ($mask = array_get($subColumn->options, 'text_mask')) {
                            $maskSetting = isset($mask) ? explode(',', $mask) : [];
                            if (array_key_exists(0, $maskSetting)) $maskSetting[0] = intval($maskSetting[0]);
                            if (array_key_exists(1, $maskSetting)) $maskSetting[1] = intval($maskSetting[1]);
                            if (array_key_exists(2, $maskSetting)) $maskSetting[2] = intval($maskSetting[2]);
                            $socialSet[$column] = str_cover($socialSet[$column], ...$maskSetting);
                        }
                    }
                }
                return $socialSet;
            });

            $member->memberDetail->profile = transform($member->memberDetail->profile, function ($profileSet) use ($subColumns) {
                foreach ($profileSet as $column => $value) {
                    if ($subColumn = $subColumns->where('column_name', 'profile')->where('sub_column_name', $column)->first()) {
                        if (array_get($subColumn->options, 'crypt') == 'true') {
                            $result = minmaxDecrypt($value);
                            $profileSet[$column] = $result == false ? $value : $result;
                        }
                        if ($mask = array_get($subColumn->options, 'text_mask')) {
                            $maskSetting = isset($mask) ? explode(',', $mask) : [];
                            if (array_key_exists(0, $maskSetting)) $maskSetting[0] = intval($maskSetting[0]);
                            if (array_key_exists(1, $maskSetting)) $maskSetting[1] = intval($maskSetting[1]);
                            if (array_key_exists(2, $maskSetting)) $maskSetting[2] = intval($maskSetting[2]);
                            $profileSet[$column] = str_cover($profileSet[$column], ...$maskSetting);
                        }
                    }
                }
                return $profileSet;
            });

            return $member;
        }

        return null;
    }

    protected function beforeCreate()
    {
        if ($password = array_pull($this->attributes, 'password')) {
            if (isset($password)) {
                $this->attributes['password'] = \Hash::make($password);
            }
        }

        array_forget($this->attributes, 'password_confirmation');

        $this->roleSelected = array_pull($this->attributes, 'role_id', []);
    }

    protected function afterCreate()
    {
        $this->model->syncRoles($this->roleSelected);

        $this->model->memberDetail()->create([
            'name' => ['full_name' => $this->model->name],
            'contact' => ['email' => $this->model->email],
        ]);

        $this->model->memberAuthentications()->create([
            'type' => 'email',
            'token' => \Illuminate\Support\Str::uuid(),
        ]);

        $this->model->memberRecords()->create([
            'code' => 'created',
            'details' => ['tag' => 'Created', 'remark' => 'New member ' . $this->model->username . ' is created.'],
        ]);

        event(new MemberRegistered($this->model));
    }

    protected function beforeSave()
    {
        if ($password = array_pull($this->attributes, 'password')) {
            if (isset($password)) {
                $this->attributes['password'] = \Hash::make($password);
            }
        }

        array_forget($this->attributes, 'password_confirmation');

        $this->roleSelected = array_pull($this->attributes, 'role_id', []);
    }

    protected function afterSave()
    {
        $this->model->syncRoles($this->roleSelected);

        $this->model->memberRecords()->create([
            'code' => 'updated',
            'details' => ['tag' => 'Updated', 'remark' => 'Member account is updated.'],
        ]);
    }

    protected function afterDelete()
    {
        \DB::table('role_user')->where(['user_id' => $this->model->getKey(), 'user_type' => get_class($this->model)])->delete();
        \DB::table('permission_user')->where(['user_id' => $this->model->getKey(), 'user_type' => get_class($this->model)])->delete();

        $this->model->memberRecords()->create([
            'code' => 'deleted',
            'details' => ['tag' => 'Deleted', 'remark' => 'Member account is deleted.'],
        ]);
    }
}
