<?php

namespace Minmax\Member\Administrator;

use Minmax\Base\Administrator\ColumnExtensionRepository;
use Minmax\Base\Administrator\Repository;
use Minmax\Member\Models\MemberDetail;

/**
 * Class MemberDetailRepository
 * @property MemberDetail $model
 * @method MemberDetail find($id)
 * @method MemberDetail one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method MemberDetail|\Illuminate\Database\Eloquent\Builder query()
 * @method MemberDetail saveLanguage($model, $columns = [])
 */
class MemberDetailRepository extends Repository
{
    const MODEL = MemberDetail::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'member_detail';
    }

    protected function beforeSave()
    {
        $subColumns = (new ColumnExtensionRepository)->all(['table_name' => 'member_detail', 'active' => true]);

        if (array_key_exists('name', $this->attributes)) {
            $this->attributes['name'] = array_merge($this->model->name ?? [], transform(array_get($this->attributes, 'name'), function ($subs) use ($subColumns) {
                foreach ($subs as $column => $value) {
                    if ($subColumn = $subColumns->where('column_name', 'name')->where('sub_column_name', $column)->first()) {
                        if (array_get($subColumn->options, 'crypt') == 'true') {
                            $subs[$column] = minmaxEncrypt($value);
                        }
                    }
                }
                return $subs;
            }, []));
        }
        if (array_key_exists('contact', $this->attributes)) {
            $this->attributes['contact'] = array_merge($this->model->contact ?? [], transform(array_get($this->attributes, 'contact'), function ($subs) use ($subColumns) {
                foreach ($subs as $column => $value) {
                    if ($subColumn = $subColumns->where('column_name', 'contact')->where('sub_column_name', $column)->first()) {
                        if (array_get($subColumn->options, 'crypt') == 'true') {
                            $subs[$column] = minmaxEncrypt($value);
                        }
                    }
                }
                return $subs;
            }, []));
        }
        if (array_key_exists('social', $this->attributes)) {
            $this->attributes['social'] = array_merge($this->model->social ?? [], transform(array_get($this->attributes, 'social'), function ($subs) use ($subColumns) {
                foreach ($subs as $column => $value) {
                    if ($subColumn = $subColumns->where('column_name', 'social')->where('sub_column_name', $column)->first()) {
                        if (array_get($subColumn->options, 'crypt') == 'true') {
                            $subs[$column] = minmaxEncrypt($value);
                        }
                    }
                }
                return $subs;
            }, []));
        }
        if (array_key_exists('profile', $this->attributes)) {
            $this->attributes['profile'] = array_merge($this->model->profile ?? [], transform(array_get($this->attributes, 'profile'), function ($subs) use ($subColumns) {
                foreach ($subs as $column => $value) {
                    if ($subColumn = $subColumns->where('column_name', 'profile')->where('sub_column_name', $column)->first()) {
                        if (array_get($subColumn->options, 'crypt') == 'true') {
                            $subs[$column] = minmaxEncrypt($value);
                        }
                    }
                }
                return $subs;
            }, []));
        }
    }

    protected function afterSave()
    {
        $this->model->member()->touch();

        $this->model->member->memberRecords()->create([
            'code' => 'updated',
            'details' => ['tag' => 'Updated', 'remark' => 'Member profile is updated.'],
        ]);
    }
}
