<?php

namespace Minmax\Member\Administrator;

use Minmax\Base\Administrator\Controller;

/**
 * Class MemberController
 */
class MemberController extends Controller
{
    protected $packagePrefix = 'MinmaxMember::';

    public function __construct(MemberRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    /**
     * Set datatable filter.
     *
     * @param  mixed $datatable
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    protected function doDatatableFilter($datatable, $request)
    {
        $datatable->filter(function($query) use ($request) {
            /** @var \Illuminate\Database\Query\Builder $query */

            if($request->has('filter')) {
                $query->where(function ($query) use ($request) {
                    /** @var \Illuminate\Database\Query\Builder $query */

                    foreach ($request->input('filter', []) as $column => $value) {
                        if (empty($column) || is_null($value) || $value === '') continue;

                        $query->orWhere($column, 'like', "%{$value}%");
                    }
                });
            }

            if($request->has('equal')) {
                foreach($request->input('equal', []) as $column => $value) {
                    if (empty($column) || is_null($value) || $value === '') continue;

                    if($column == 'role_id') {
                        $query->whereRaw('`id` in (select `user_id` from `role_user` where `role_id` = ?)', [$value]);
                        continue;
                    }

                    $query->where($column, $value);
                }
            }
        });

        return $datatable;
    }

    /**
     * Model Show
     *
     * @param  string $id
     * @return \Illuminate\Http\Response
     * @throws \DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException
     */
    public function show($id)
    {
        $this->viewData['formData'] = $this->modelRepository->findAndHide($id) ?? abort(404);

        $this->setCustomViewDataShow();

        $this->buildBreadcrumbsShow();

        try {
            return view($this->packagePrefix . 'administrator.' . $this->uri . '.show', $this->viewData);
        } catch(\Exception $e) {
            return abort(404);
        }
    }

    /**
     * Model Edit
     *
     * @param  string $id
     * @return \Illuminate\Http\Response
     * @throws \DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException
     */
    public function edit($id)
    {
        $this->viewData['formData'] = $this->modelRepository->findAndHide($id) ?? abort(404);

        $this->setCustomViewDataEdit();

        $this->buildBreadcrumbsEdit($id);

        try {
            return view($this->packagePrefix . 'administrator.' . $this->uri . '.edit', $this->viewData);
        } catch(\Exception $e) {
            return abort(404);
        }
    }
}
