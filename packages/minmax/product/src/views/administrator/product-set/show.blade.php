<?php
/**
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 * @var \Minmax\Product\Models\ProductSet $formData
 */
?>

@extends('MinmaxBase::administrator.layouts.page.show')

@section('action-buttons')
    @component('MinmaxBase::administrator.layouts.right-links', ['languageActive' => $languageActive])
    <a class="btn btn-sm btn-light" href="{{ langRoute("administrator.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::administrator.form.back_list')">
        <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.back_list')</span>
    </a>
    <a class="btn btn-sm btn-main" href="{{ langRoute("administrator.{$pageData->uri}.edit", [$formData->id]) }}" title="@lang('MinmaxBase::administrator.form.edit')">
        <i class="icon-pencil"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.edit')</span>
    </a>
    @endcomponent
@endsection

@section('views')
    @inject('modelPresenter', 'Minmax\Product\Administrator\ProductSetPresenter')

    <fieldset id="baseFieldSet">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::administrator.form.fieldSet.default')</legend>

        {!! $modelPresenter->getShowNormalText($formData, 'title') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'sku') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'serial') !!}

        {!! $modelPresenter->getShowSelection($formData, 'brand_id') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'categories', ['defaultValue' => $formData->productCategories->pluck('title')->implode(', ')]) !!}

        {!! $modelPresenter->getShowColumnExtension($formData, 'details') !!}

        {!! $modelPresenter->getShowSelection($formData, 'rank') !!}

    </fieldset>

    <fieldset class="mt-4" id="mediaFieldSet">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.media')</legend>

        {!! $modelPresenter->getShowMediaImage($formData, 'pic') !!}

    </fieldset>

    @if(in_array(\Minmax\Ecommerce\ServiceProvider::class, config('app.providers')))
    <fieldset class="mt-4" id="ecommerceFieldSet">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxProduct::administrator.form.fieldSet.ecommerce')</legend>

        {!! $modelPresenter->getShowColumnExtension($formData, 'ec_parameters') !!}

    </fieldset>
    @endif

    <fieldset class="mt-4" id="advFieldSet">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::administrator.form.fieldSet.advanced')</legend>

        {!! $modelPresenter->getShowNormalText($formData, 'start_at') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'end_at') !!}

        {!! $modelPresenter->getShowSelection($formData, 'searchable') !!}

        {!! $modelPresenter->getShowSelection($formData, 'visible') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'sort') !!}

        {!! $modelPresenter->getShowSelection($formData, 'active') !!}

    </fieldset>

    <fieldset class="mt-4" id="seoFieldSet">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::administrator.form.fieldSet.seo')</legend>

        {!! $modelPresenter->getShowNormalText($formData, 'seo', ['subColumn' => 'meta_description']) !!}

        {!! $modelPresenter->getShowNormalText($formData, 'seo', ['subColumn' => 'meta_keywords']) !!}

    </fieldset>

    <fieldset class="mt-4" id="sysFieldSet">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::administrator.form.fieldSet.system_record')</legend>

        {!! $modelPresenter->getShowNormalText($formData, 'created_at') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'updated_at') !!}

    </fieldset>

@endsection
