<?php
/**
 * @var string $id
 * @var string $value
 */
?>
<div class="text-nowrap talbe-sort">
    <input class="form-control form-control-sm text-center d-inline-block inputQty"
           type="text"
           value="{{ $value }}"
           data-id="{{ $id }}"
           data-qty="{{ $value }}"
           style="margin-right: 0;" />
    <a class="btn btn-link icon-loop2 p-0 m-0 update-qty"></a>
</div>