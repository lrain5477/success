<?php
/**
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 */
?>

@extends('MinmaxBase::administrator.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::administrator.layouts.right-links')
        <a class="btn btn-sm btn-main" href="{{ langRoute("administrator.{$pageData->uri}.create") }}" title="@lang('MinmaxBase::administrator.form.create')">
            <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.create')</span>
        </a>
        @isset($importLink)
        <a class="btn btn-sm btn-main" href="{{ $importLink }}" title="@lang('MinmaxBase::administrator.form.import')">
            <i class="icon-upload2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.import')</span>
        </a>
        @endisset
        @isset($exportLink)
        <a class="btn btn-sm btn-main" href="{{ $exportLink }}" title="@lang('MinmaxBase::administrator.form.export')">
            <i class="icon-download"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.export')</span>
        </a>
        @endisset
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Product\Administrator\ProductPackagePresenter')

@section('grid-filter')
    @component('MinmaxBase::administrator.layouts.grid.filter-keyword')
    <option value="set_sku">@lang('MinmaxProduct::models.ProductPackage.set_sku')</option>
    <option value="item_sku">@lang('MinmaxProduct::models.ProductPackage.item_sku')</option>
    @endcomponent

    @component('MinmaxBase::administrator.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('active', 'searchActive', ['emptyLabel' => __('MinmaxProduct::models.ProductPackage.active')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="nosort w-5">@lang('MinmaxProduct::models.ProductSet.pic')</th>
        <th class="w-50">@lang('MinmaxProduct::administrator.grid.ProductPackage.title')</th>
        <th>@lang('MinmaxProduct::models.ProductPackage.start_at')</th>
        <th>@lang('MinmaxProduct::models.ProductPackage.price_sell')</th>
        <th>@lang('MinmaxProduct::models.ProductPackage.sort')</th>
        <th>@lang('MinmaxProduct::models.ProductPackage.active')</th>
        <th class="nosort">@lang('MinmaxBase::administrator.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'pic', name: 'pic'},
            {data: 'set_sku', name: 'set_sku'},
            {data: 'start_at', name: 'start_at'},
            {data: 'price_sell', name: 'price_sell', className: 'text-right text-nowrap'},
            {data: 'sort', name: 'sort'},
            {data: 'active', name: 'active'},
            {data: 'action', name: 'action'}
        ],
        ['set_sku', 'item_sku'],
        {"active":"searchActive"},
        [[1, 'asc'], [4, 'asc']],
        '{{ langRoute("administrator.{$pageData->uri}.ajaxDataTable", ['set' => request('set')]) }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("administrator.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
