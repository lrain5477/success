<?php
/**
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 */
?>

@extends('MinmaxBase::admin.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links')
        @if($adminData->can('productPackageCreate'))
        <a class="btn btn-sm btn-main" href="{{ langRoute("admin.{$pageData->uri}.create") }}" title="@lang('MinmaxBase::admin.form.create')">
            <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.create')</span>
        </a>
        @endif
        @if(!is_null($importLink) && $adminData->can('ioDataImport') && $adminData->can('productPackageImport'))
        <a class="btn btn-sm btn-main" href="{{ $importLink }}" title="@lang('MinmaxBase::admin.form.import')">
            <i class="icon-upload2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.import')</span>
        </a>
        @endif
        @if(!is_null($exportLink) && $adminData->can('ioDataExport') && $adminData->can('productPackageExport'))
        <a class="btn btn-sm btn-main" href="{{ $exportLink }}" title="@lang('MinmaxBase::admin.form.export')">
            <i class="icon-download"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.export')</span>
        </a>
        @endif
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Product\Admin\ProductPackagePresenter')

@section('grid-filter')
    @component('MinmaxBase::admin.layouts.grid.filter-keyword')
    <option value="set_sku">@lang('MinmaxProduct::models.ProductPackage.set_sku')</option>
    <option value="item_sku">@lang('MinmaxProduct::models.ProductPackage.item_sku')</option>
    @endcomponent

    @component('MinmaxBase::admin.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('active', 'searchActive', ['emptyLabel' => __('MinmaxProduct::models.ProductPackage.active')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="nosort w-5">@lang('MinmaxProduct::models.ProductSet.pic')</th>
        <th class="w-50">@lang('MinmaxProduct::admin.grid.ProductPackage.title')</th>
        <th>@lang('MinmaxProduct::models.ProductPackage.start_at')</th>
        <th>@lang('MinmaxProduct::models.ProductPackage.price_sell')</th>
        <th>@lang('MinmaxProduct::models.ProductPackage.sort')</th>
        <th>@lang('MinmaxProduct::models.ProductPackage.active')</th>
        <th class="nosort">@lang('MinmaxBase::admin.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'pic', name: 'pic'},
            {data: 'set_sku', name: 'set_sku'},
            {data: 'start_at', name: 'start_at'},
            {data: 'price_sell', name: 'price_sell', className: 'text-right text-nowrap'},
            {data: 'sort', name: 'sort'},
            {data: 'active', name: 'active'},
            {data: 'action', name: 'action'}
        ],
        ['set_sku', 'item_sku'],
        {"active":"searchActive"},
        [[1, 'asc'], [4, 'asc']],
        '{{ langRoute("admin.{$pageData->uri}.ajaxDataTable", ['set' => request('set')]) }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("admin.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
