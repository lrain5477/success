<?php

namespace Minmax\Product\Admin;

use Illuminate\Http\Request;
use Minmax\Base\Admin\Controller;
use Minmax\Io\Admin\IoConstructRepository;

/**
 * Class ProductPackageController
 */
class ProductPackageController extends Controller
{
    protected $packagePrefix = 'MinmaxProduct::';

    public function __construct(ProductPackageRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getQueryBuilder()
    {
        $query = $this->modelRepository->query()->with(['productItem', 'productSet', 'productMarkets']);

        if ($set_id = request('set')) {
            $query->whereHas('productSet', function ($query) use ($set_id) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->where('product_set.id', $set_id);
            });
        }

        if ($item_id = request('item')) {
            $query->whereHas('productItem', function ($query) use ($item_id) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->where('product_item.id', $item_id);
            });
        }

        return $query;
    }

    /**
     * Set datatable filter.
     *
     * If filter column is json type, you can use whereRaw() or orWhereRaw() with sql function json_contains() or json_search().
     *
     * @param  mixed $datatable
     * @param  Request $request
     * @return mixed
     */
    protected function doDatatableFilter($datatable, $request)
    {
        $datatable->filter(function($query) use ($request) {
            /** @var \Illuminate\Database\Query\Builder $query */

            if($request->has('filter')) {
                $query->where(function ($query) use ($request) {
                    /** @var \Illuminate\Database\Query\Builder $query */

                    foreach ($request->input('filter', []) as $column => $value) {
                        if (empty($column) || is_null($value) || $value === '') continue;

                        $query->orWhere($column, 'like', "%{$value}%");
                    }
                });
            }

            if($request->has('equal')) {
                foreach($request->input('equal', []) as $column => $value) {
                    if (empty($column) || is_null($value) || $value === '') continue;

                    $query->where($column, $value);
                }
            }
        });

        $currency = getCurrency(null, app()->getLocale());
        $datatable->orderColumn('price_sell', "convert(json_extract(`price_sell`, '$.{$currency}'), SIGNED INTEGER) $1");

        return $datatable;
    }

    protected function setCustomViewDataIndex()
    {
        $ioModel = (new IoConstructRepository)->one(['uri' => 'product-package', 'active' => true]);
        $this->viewData['importLink'] = is_null($ioModel) ? null : ($ioModel->import_enable ? langRoute('admin.io-data.config', ['id' => $ioModel->id, 'method' => 'import']) : null);
        $this->viewData['exportLink'] = is_null($ioModel) ? null : ($ioModel->export_enable ? langRoute('admin.io-data.config', ['id' => $ioModel->id, 'method' => 'export']) : null);
    }
}
