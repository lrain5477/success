<?php

namespace Minmax\Product\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class HelperController
 */
class HelperController extends BaseController
{
    /**
     * @param  Request $request
     * @param  string $type
     * @return \Illuminate\Http\Response
     */
    public function isProductSet(Request $request, $type)
    {
        if ($productSet = (new ProductSetRepository)->one($type, $request->get($type))) {
            return response([
                $type => $productSet->getAttribute($type),
                'title' => $productSet->title
            ], 200, ['Content-Type' => 'application/json']);
        }

        return abort(404);
    }
}
