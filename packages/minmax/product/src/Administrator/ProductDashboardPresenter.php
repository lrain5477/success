<?php

namespace Minmax\Product\Administrator;

use Minmax\Base\Administrator\Presenter;
use Minmax\Base\Models\SiteDashboard;

/**
 * Class ProductDashboardPresenter
 */
class ProductDashboardPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxProduct::';

    protected $productSetRepository;

    /**
     * Presenter constructor.
     *
     * @param  ProductSetRepository $productSetRepository
     */
    public function __construct(ProductSetRepository $productSetRepository)
    {
        $this->productSetRepository = $productSetRepository;

        parent::__construct();
    }

    /**
     * @param  SiteDashboard $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function totalProductSet(SiteDashboard $model)
    {
        $amount = $this->productSetRepository->query()
            ->where('active', true)
            ->where(function ($query) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->whereNull('start_at')->orWhere('start_at', '<=', date('Y-m-d H:i:s'));
            })
            ->where(function ($query) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->whereNull('end_at')->orWhere('end_at', '>=', date('Y-m-d H:i:s'));
            })
            ->count();

        $componentData = [
            'id' => "dashboard-{$model->id}",
            'title' => $model->title,
            'position' => $model->position,
            'options' => $model->options,
            'amount' => number_format($amount),
        ];

        return view('MinmaxProduct::administrator.product-set.dashboard-total-product-set', $componentData);
    }
}
