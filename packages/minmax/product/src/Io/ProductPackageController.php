<?php

namespace Minmax\Product\Io;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Minmax\Io\Abstracts\IoController;
use Minmax\Io\Admin\IoConstructRepository;
use Minmax\Io\Models\IoRecord;
use Minmax\Product\Admin\ProductMarketRepository;
use Minmax\Product\Admin\ProductPackageRepository;
use Minmax\World\Admin\WorldCurrencyRepository;

/**
 * Class ProductPackageController
 */
class ProductPackageController extends IoController
{
    protected $packagePrefix = 'MinmaxProduct::';

    public function example($id)
    {
        $ioData = (new IoConstructRepository)->find($id) ?? abort(404);

        $filename = ($ioData->filename ?? $ioData->title) . ' (Sample)';

        $currenciesSet = (new WorldCurrencyRepository)->getSelectParameters('active', true);
        $marketSet = (new ProductMarketRepository)->getSelectParameters();

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Use sheet 0
        $sheet = $spreadsheet->getSheet(0);
        $sheet->setTitle('import');

        $titleColumnIndex = 0;
        $titleRowIndex = 1;
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.set_sku') . ' *', 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(12);
        $sheet->getCommentByColumnAndRow($titleColumnIndex, $titleRowIndex)->getText()->createTextRun(__('MinmaxProduct::io.ProductPackage.export.comments.set_sku'))->getFont()->setSize(9);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.item_sku') . ' *', 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(12);
        $sheet->getCommentByColumnAndRow($titleColumnIndex, $titleRowIndex)->getText()->createTextRun(__('MinmaxProduct::io.ProductPackage.export.comments.item_sku'))->getFont()->setSize(9);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.amount') . ' *', 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(10);
        $sheet->getCommentByColumnAndRow($titleColumnIndex, $titleRowIndex)->getText()->createTextRun(__('MinmaxProduct::io.ProductPackage.export.comments.amount'))->getFont()->setSize(9);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.limit'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(10);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.description'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(25);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.price_advice') . ' *', 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(12);
        $sheet->getCommentByColumnAndRow($titleColumnIndex, $titleRowIndex)->getText()->createTextRun(__('MinmaxProduct::io.ProductPackage.export.comments.price_advice'))->getFont()->setSize(9);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.price_sell') . ' *', 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(12);
        $sheet->getCommentByColumnAndRow($titleColumnIndex, $titleRowIndex)->getText()->createTextRun(__('MinmaxProduct::io.ProductPackage.export.comments.price_sell'))->getFont()->setSize(9);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.productMarkets'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(25);
        $sheet->getCommentByColumnAndRow($titleColumnIndex, $titleRowIndex)->getText()->createTextRun(__('MinmaxProduct::io.ProductPackage.export.comments.productMarkets'))->getFont()->setSize(9);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.start_at'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(17);
        $sheet->getCommentByColumnAndRow($titleColumnIndex, $titleRowIndex)->getText()->createTextRun(__('MinmaxProduct::io.ProductPackage.export.comments.start_at'))->getFont()->setSize(9);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.end_at'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(17);
        $sheet->getCommentByColumnAndRow($titleColumnIndex, $titleRowIndex)->getText()->createTextRun(__('MinmaxProduct::io.ProductPackage.export.comments.end_at'))->getFont()->setSize(9);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.sort'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(10);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.active') . ' *', 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(10);
        $sheet->getCommentByColumnAndRow($titleColumnIndex, $titleRowIndex)->getText()->createTextRun(__('MinmaxProduct::io.ProductPackage.export.comments.active'))->getFont()->setSize(9);

        $dataColumnIndex = 0;
        $dataRowIndex = 2;
        $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, 'DEMO001', 's');
        $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, 'DEMO001', 's');
        $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, '1', 's');
        $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, '', 's');
        $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, 'This is a demo package', 's');
        $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, 'TWD:699', 's');
        $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, 'TWD:350', 's');
        $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, '', 's');
        $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, date('Y-m-d H:i:s'), 's');
        $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, '', 's');
        $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, '1', 's');
        $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, '1', 's');

        // Set sheet style
        $this->setSheetStyle($sheet, [1, 1, $dataColumnIndex, $dataRowIndex]);
        $this->setSheetStyle($sheet, [1, 1, $titleColumnIndex, $titleRowIndex], [
            'font' => ['bold' => true],
            'fill' => ['fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'startColor' => ['rgb' => 'EFEFEF']]
        ]);

        $sheet->setSelectedCellByColumnAndRow(1, 1);

        // Use sheet 1
        $sheet = $spreadsheet->createSheet(1);
        $sheet->setTitle(__("MinmaxProduct::{$this->guard}.form.ProductPackage.currency"));

        $titleColumnIndex = 0;
        $titleRowIndex = 1;
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxWorld::models.WorldCurrency.code'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(20);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __("MinmaxProduct::{$this->guard}.form.ProductItem.currency"), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(50);

        $dataColumnIndex = 0;
        $dataRowIndex = 1;
        foreach ($currenciesSet as $currencyCode => $currencyData) {
            $dataColumnIndex = 0;
            $dataRowIndex++;
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, $currencyCode, 's');
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, array_get($currencyData, 'title', ''), 's');
        }

        // Set sheet style
        $this->setSheetStyle($sheet, [1, 1, $dataColumnIndex, $dataRowIndex]);
        $this->setSheetStyle($sheet, [1, 1, $titleColumnIndex, $titleRowIndex], [
            'font' => ['bold' => true],
            'fill' => ['fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'startColor' => ['rgb' => 'EFEFEF']]
        ]);

        $sheet->setSelectedCellByColumnAndRow(1, 1);

        // Use sheet 2
        $sheet = $spreadsheet->createSheet(2);
        $sheet->setTitle(__("MinmaxProduct::models.ProductPackage.productMarkets"));

        $titleColumnIndex = 0;
        $titleRowIndex = 1;
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductMarket.code'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(20);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductMarket.title'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(50);

        $dataColumnIndex = 0;
        $dataRowIndex = 1;
        foreach ($marketSet as $marketCode => $marketData) {
            $dataColumnIndex = 0;
            $dataRowIndex++;
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, $marketCode, 's');
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, array_get($marketData, 'title', ''), 's');
        }

        // Set sheet style
        $this->setSheetStyle($sheet, [1, 1, $dataColumnIndex, $dataRowIndex]);
        $this->setSheetStyle($sheet, [1, 1, $titleColumnIndex, $titleRowIndex], [
            'font' => ['bold' => true],
            'fill' => ['fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'startColor' => ['rgb' => 'EFEFEF']]
        ]);

        $sheet->setSelectedCellByColumnAndRow(1, 1);

        $spreadsheet->setActiveSheetIndex(0);

        // 寫入檔案並輸出
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

        $response = response()->streamDownload(
            function () use ($writer) { $writer->save('php://output'); },
            "{$filename}.xlsx",
            ['Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'Cache-Control' => 'max-age=0']
        );

        return $response;
    }

    public function import(Request $request, $id)
    {
        $ioData = (new IoConstructRepository)->find($id) ?? abort(404);

        $fileField = 'ProductPackage.file';

        $sheetData = $this->getSheetFromFile($request, $fileField, 'import', 1);

        if (is_null($sheetData)) {
            return redirect(langRoute("{$this->guard}.{$this->ioUri}.config", ['id' => $id]))
                ->withErrors([__("MinmaxIo::{$this->guard}.form.message.import_error", ['title' => $ioData->title])]);
        }

        // Insert data
        $timestamp = date('Y-m-d H:i:s');
        $setSkuPool = DB::table('product_set')->pluck('sku')->toArray();
        $itemSkuPool = DB::table('product_item')->pluck('sku')->toArray();
        $packageSkuPool = DB::table('product_package')->pluck('set_sku')->toArray();
        $insertData = [];
        $updateSkuSet = [];
        $errorRecord = [];
        foreach ($sheetData as $rowIndex => $rowData) {
            $errorCounter = 0;

            if (empty($rowData[0] ?? '')) {
                $errorRecord[] = ['row' => $rowIndex, 'message' => __('validation.required', ['attribute' => __('MinmaxProduct::models.ProductPackage.set_sku')])];
                $errorCounter++;
            }

            if (! in_array($rowData[0] ?? '', $setSkuPool)) {
                $errorRecord[] = ['row' => $rowIndex, 'message' => __('validation.exists', ['attribute' => __('MinmaxProduct::models.ProductPackage.set_sku')])];
                $errorCounter++;
            }

            if (empty($rowData[1] ?? '')) {
                $errorRecord[] = ['row' => $rowIndex, 'message' => __('validation.required', ['attribute' => __('MinmaxProduct::models.ProductPackage.item_sku')])];
                $errorCounter++;
            }

            if (! in_array($rowData[1] ?? '', $itemSkuPool)) {
                $errorRecord[] = ['row' => $rowIndex, 'message' => __('validation.exists', ['attribute' => __('MinmaxProduct::models.ProductPackage.item_sku')])];
                $errorCounter++;
            }

            if (empty($rowData[2] ?? '')) {
                $errorRecord[] = ['row' => $rowIndex, 'message' => __('validation.required', ['attribute' => __('MinmaxProduct::models.ProductPackage.amount')])];
                $errorCounter++;
            }

            if ($errorCounter > 0) continue;

            if (intval($request->input('ProductPackage.override', 0)) == 1 && in_array($rowData[0], $packageSkuPool)) {
                if (! in_array($rowData[0], $updateSkuSet)) {
                    $updateSkuSet[] = $rowData[0];
                }
            }

            $insertData[] = [
                'set_sku' => $rowData[0],
                'item_sku' => $rowData[1],
                'amount' => intval($rowData[2] ?? 1),
                'limit' => intval($rowData[3] ?? 0),
                'description' => $rowData[4] ?? null,
                'price_advice' => $this->priceStringToArray($rowData[5] ?? ''),
                'price_sell' => $this->priceStringToArray($rowData[6] ?? ''),
                'productMarkets' => isset($rowData[7]) ? explode(',', $rowData[7]) : [],
                'start_at' => isset($rowData[8]) ? $rowData[8] : $timestamp,
                'end_at' => $rowData[9] ?? null,
                'sort' => isset($rowData[10]) ? intval($rowData[10]) : 1,
                'active' => boolval($rowData[11] ?? 0),
            ];
        }

        try {
            if (count($insertData) < 1) throw new \Exception;

            DB::beginTransaction();

            $packageRepository = new ProductPackageRepository();

            foreach ($updateSkuSet as $removeSku) {
                foreach ($packageRepository->all('set_sku', strval($removeSku)) as $removeModel) {
                    $packageRepository->delete($removeModel);
                }
            }

            foreach ($insertData as $rowIndex => $rowData) {
                if (is_null($packageRepository->create($rowData))) {
                    $errorRecord[] = ['row' => $rowIndex, 'message' => 'Insert data failed.'];
                }
            }

            DB::commit();

            // Import record
            IoRecord::create([
                'title' => $ioData->title,
                'uri' => $ioData->uri,
                'type' => 'import',
                'errors' => $errorRecord,
                'total' => count($sheetData),
                'success' => count($sheetData) - count($errorRecord),
                'result' => true,
                'file' => $request->file($fileField)->getClientOriginalName(),
            ]);

            return redirect(langRoute("{$this->guard}.{$this->ioUri}.config", ['id' => $id, 'method' => 'import']))->with('success', __("MinmaxIo::{$this->guard}.form.message.import_success", ['title' => $ioData->title]));
        } catch (\Exception $e) {
            DB::rollBack();

            // Import record
            IoRecord::create([
                'title' => $ioData->title,
                'uri' => $ioData->uri,
                'type' => 'import',
                'errors' => $errorRecord,
                'total' => count($sheetData),
                'success' => 0,
                'result' => false,
                'file' => $request->file($fileField)->getClientOriginalName(),
            ]);

            throw new \Exception;
        }
    }

    public function export(Request $request, $id)
    {
        $ioData = (new IoConstructRepository)->find($id) ?? abort(404);

        $packageData = (new ProductPackageRepository)->query()
            ->with(['productMarkets'])
            ->where(function ($query) use ($request) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                if ($setSku = $request->input('ProductPackage.set_sku')) {
                    $query->where('set_sku', '=', $setSku);
                }
                if ($itemSku = $request->input('ProductPackage.item_sku')) {
                    $query->where('item_sku', '=', $itemSku);
                }
                if ($createdAtStart = $request->input('ProductPackage.created_at.start')) {
                    $query->where('created_at', '>=', "{$createdAtStart} 00:00:00");
                }
                if ($createdAtEnd = $request->input('ProductPackage.created_at.end')) {
                    $query->where('created_at', '<=', "{$createdAtEnd} 23:59:59");
                }
                if ($updatedAtStart = $request->input('ProductPackage.updated_at.start')) {
                    $query->where('updated_at', '>=', "{$updatedAtStart} 00:00:00");
                }
                if ($updatedAtEnd = $request->input('ProductPackage.updated_at.end')) {
                    $query->where('updated_at', '<=', "{$updatedAtEnd} 23:59:59");
                }
                if (! is_null($active = $request->input('ProductPackage.active'))) {
                    $query->where('active', boolval($active));
                }
            })
            ->orderBy('set_sku')
            ->orderBy('sort')
            ->get();

        $filename = ($ioData->filename ?? $ioData->title) . ' (' . date('YmdHis') . ')';

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Use sheet 0
        $sheet = $spreadsheet->getSheet(0);
        $sheet->setTitle('import');

        $titleColumnIndex = 0;
        $titleRowIndex = 1;
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.set_sku') . ' *', 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(12);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.item_sku') . ' *', 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(12);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.amount') . ' *', 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(10);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.limit'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(10);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.description'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(25);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.price_advice') . ' *', 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(12);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.price_sell') . ' *', 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(12);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.productMarkets'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(25);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.start_at'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(17);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.end_at'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(17);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.sort'), 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(10);
        $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, __('MinmaxProduct::models.ProductPackage.active') . ' *', 's')
            ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(10);

        $dataColumnIndex = 0;
        $dataRowIndex = 1;
        foreach ($packageData as $rowData) {
            /** @var \Minmax\Product\Models\ProductPackage $rowData */
            $dataRowIndex++;
            $dataColumnIndex = 0;
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, $rowData->set_sku, 's');
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, $rowData->item_sku, 's');
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, $rowData->amount, 's');
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, $rowData->getAttribute('limit'), 's');
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, $rowData->description, 's');
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, collect($rowData->price_advice)->map(function ($item, $key) { return "{$key}:{$item}"; })->implode(','), 's');
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, collect($rowData->price_sell)->map(function ($item, $key) { return "{$key}:{$item}"; })->implode(','), 's');
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, $rowData->productMarkets->pluck('id')->implode(','), 's');
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, $rowData->start_at->format('Y-m-d H:i:s'), 's');
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, $rowData->end_at->format('Y-m-d H:i:s'), 's');
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, $rowData->sort, 's');
            $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, intval($rowData->active), 's');
        }

        // Set sheet style
        $this->setSheetStyle($sheet, [1, 1, $dataColumnIndex < 1 ? $titleColumnIndex : $dataColumnIndex, $dataRowIndex]);
        $this->setSheetStyle($sheet, [1, 1, $titleColumnIndex, $titleRowIndex], [
            'font' => ['bold' => true],
            'fill' => ['fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'startColor' => ['rgb' => 'EFEFEF']]
        ]);

        // 寫入檔案並輸出
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

        $writer->save(storage_path("app\\admin\\export\\{$filename}.xlsx"));

        // Export record
        IoRecord::create([
            'title' => $ioData->title,
            'uri' => $ioData->uri,
            'type' => 'export',
            'errors' => [],
            'total' => $packageData->count(),
            'success' => $packageData->count(),
            'result' => true,
            'file' => "{$filename}.xlsx",
        ]);

        // Remove over 3 files
        $keepFiles = IoRecord::query()->where(['uri' => $ioData->uri, 'type' => 'export'])->orderByDesc('id')->limit(3)->pluck('id')->toArray();
        $removeFiles = IoRecord::query()->where(['uri' => $ioData->uri, 'type' => 'export'])->whereNotIn('id', $keepFiles)->pluck('file')
            ->map(function ($item) {
                return storage_path("app\\admin\\export\\{$item}");
            })->toArray();
        File::delete($removeFiles);

        return response(['msg' => 'success'], 200, ['Content-type' => 'application/json']);
    }

    /**
     * @param  string $price
     * @param  boolean $toJson
     * @return array|string
     */
    protected function priceStringToArray($price, $toJson = false)
    {
        $result = collect();

        if (isset($price)) {
            $currencies = (new WorldCurrencyRepository)->query()->orderBy('sort')->get();

            if (strpos($price, ',') !== false) {
                $result = collect(explode(',', $price))
                    ->mapWithKeys(function ($item) {
                        $set = explode($item, ':');
                        return [$set[0] => $set[1]];
                    })
                    ->filter(function ($item, $key) use ($currencies) {
                        return $item != '' && !is_null($item) && $currencies->where('code', $key)->count() > 0;
                    });
            } else {
                if (strpos($price, ':') !== false) {
                    $set = explode(':', $price);
                    $result->put($set[0], $set[1]);
                } else {
                    if ($firstCurrency = $currencies->first()) {
                        $result->put($firstCurrency->code, $price);
                    }
                }
            }
        }

        if ($toJson) {
            return $result->toJson();
        }

        return $result->toArray();
    }
}
