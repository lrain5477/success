<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class CreateProductTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 品牌
        Schema::create('product_brand', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('title')->comment('品牌名稱');
            $table->json('pic')->nullable()->comment('品牌圖片');
            $table->string('details')->nullable()->comment('說明細節');     // {description, editor}
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // 品項
        Schema::create('product_item', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('sku')->unique()->comment('品項貨號');
            $table->string('serial')->nullable()->comment('原廠貨號');
            $table->string('title')->comment('品項名稱');
            $table->json('pic')->nullable()->comment('品項圖片');
            $table->string('details')->nullable()->comment('產品簡介');     // {description, editor}
            $table->json('cost')->nullable()->comment('成本金額');          // {currency: price}
            $table->json('price')->nullable()->comment('安全單價');         // {currency: price}
            $table->boolean('qty_enable')->default(false)->comment('庫存管理');
            $table->unsignedInteger('qty_safety')->default(0)->comment('安全庫存');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // 品項庫存
        Schema::create('product_quantity', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_id')->comment('品項ID');
            $table->integer('amount')->comment('變動數量');
            $table->text('remark')->nullable()->comment('變動說明');
            $table->integer('summary')->comment('結算數量');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('item_id')->references('id')->on('product_item')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // 商品
        Schema::create('product_set', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('sku')->unique()->comment('商品貨號');
            $table->string('serial')->nullable()->comment('原廠貨號');
            $table->string('title')->comment('商品名稱');
            $table->json('pic')->nullable()->comment('商品圖片');
            $table->string('details')->nullable()->comment('產品簡介');             // {feature, detail, specification, video, accessory}
            $table->datetime('start_at')->nullable()->comment('開始時間');
            $table->datetime('end_at')->nullable()->comment('結束時間');
            $table->string('brand_id')->nullable()->comment('品牌ID');
            $table->unsignedInteger('rank')->default(0)->comment('評價分數');       // 0 表示自動計算
            $table->string('spec_group')->nullable()->comment('群組代碼');
            $table->json('specifications')->nullable()->comment('產品規格');        // id list of parameter (spec)
            $table->json('tags')->nullable()->comment('關聯標籤');                  // id list of parameter (tag)
            $table->string('seo')->nullable()->comment('SEO');                     // {meta_description, meta_keywords}
            $table->boolean('searchable')->default(true)->comment('搜尋顯示');      // 1:顯示 0:隱藏
            $table->boolean('visible')->default(true)->comment('前臺顯示');         // 1:顯示 0:隱藏
            $table->json('properties')->nullable()->comment('自訂屬性');            // id list of parameter (property)
            $table->json('ec_parameters')->nullable()->comment('購物車參數');
            //{
            //  payment_types: [], delivery_types: [],
            //  billing: int, shipping: int, maximum: int,
            //  continued: bool, additional: bool, wrapped: bool, returnable: bool, rewarded: bool,
            //}
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // 組合 / 商品-品項 (Many-to-Many)
        Schema::create('product_package', function (Blueprint $table) {
            $table->increments('id');
            $table->string('set_sku')->comment('商品貨號');
            $table->string('item_sku')->comment('品項貨號');
            $table->unsignedInteger('amount')->default(1)->comment('組合數量');
            $table->unsignedInteger('limit')->nullable()->comment('商品限量');
            $table->string('description')->nullable()->comment('簡短說明');
            $table->json('price_advice')->nullable()->comment('建議售價');      // {currency: price}
            $table->json('price_sell')->nullable()->comment('優惠售價');        // {currency: price}
            $table->datetime('start_at')->nullable()->comment('開始時間');
            $table->datetime('end_at')->nullable()->comment('結束時間');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();

            $table->foreign('set_sku')->references('sku')->on('product_set')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('item_sku')->references('sku')->on('product_item')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // 賣場
        Schema::create('product_market', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('code')->unique()->comment('賣場代碼');
            $table->string('title')->comment('賣場名稱');
            $table->string('admin_id')->nullable()->comment('管理員ID');
            $table->string('details')->nullable()->comment('說明細節');     // {editor, pic}
            $table->json('options')->nullable()->comment('參數設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // 賣場-商品組合 (Many-to-Many)
        Schema::create('product_market_package', function (Blueprint $table) {
            $table->string('market_id')->comment('賣場ID');
            $table->unsignedInteger('package_id')->comment('組合ID');

            $table->foreign('market_id')->references('id')->on('product_market')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('package_id')->references('id')->on('product_package')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // 分類
        Schema::create('product_category', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('title')->comment('分類標題');
            $table->string('details')->nullable()->comment('說明細節');             // {description, editor, pic}
            $table->string('parent_id')->nullable()->comment('上層分類');
            $table->boolean('visible')->default(true)->comment('前臺顯示');         // 1:顯示 0:隱藏
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // 分類-商品 (Many-to-Many)
        Schema::create('product_category_set', function (Blueprint $table) {
            $table->string('category_id');
            $table->string('set_id');

            $table->primary(['category_id', 'set_id']);

            $table->foreign('category_id')->references('id')->on('product_category')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('set_id')->references('id')->on('product_set')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // 分類-會員 (Many-to-Many)
        Schema::create('product_category_role', function (Blueprint $table) {
            $table->string('category_id');
            $table->unsignedInteger('role_id');

            $table->primary(['category_id', 'role_id']);

            $table->foreign('category_id')->references('id')->on('product_category')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // 商品瀏覽紀錄
        Schema::create('product_set_track', function (Blueprint $table) {
            $table->string('set_id')->index()->comment('商品ID');
            $table->string('member_id')->nullable()->comment('會員ID');
            $table->ipAddress('ip')->comment('IP位址');
            $table->date('click_at')->comment('點擊日期');
            $table->timestamp('created_at')->useCurrent()->comment('建立時間');

            $table->unique(['set_id', 'member_id', 'ip', 'click_at']);

            $table->foreign('set_id')->references('id')->on('product_set')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // 建立系統參數資料
        $this->insertSystemParameters();

        // 建立網站參數資料
        $this->insertSiteParameters();

        // 建立匯入匯出項目
        $this->insertIoConstructs();

        // 建立商品管理預設資料
        $this->insertProductData();

        // 建立儀錶板項目
        $this->insertDashboard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 刪除匯入匯出項目
        $this->deleteIoConstructs();

        // 刪除網站參數資料
        $this->deleteSiteParameters();

        // 刪除系統參數資料
        $this->deleteSystemParameters();

        Schema::dropIfExists('product_set_track');
        Schema::dropIfExists('product_category_role');
        Schema::dropIfExists('product_category_set');
        Schema::dropIfExists('product_category');
        Schema::dropIfExists('product_market_package');
        Schema::dropIfExists('product_market');
        Schema::dropIfExists('product_package');
        Schema::dropIfExists('product_set');
        Schema::dropIfExists('product_quantity');
        Schema::dropIfExists('product_item');
        Schema::dropIfExists('product_brand');
    }

    /**
     * Insert system parameters for this module.
     *
     * @return void
     */
    public function insertSystemParameters()
    {
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $startGroupId = $rowGroupId = SeederHelper::getTableNextIncrement('system_parameter_group');
        $rowGroupId--;

        $systemGroupData = [
            ['code' => 'qty_enable', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
        ];

        DB::table('system_parameter_group')->insert($systemGroupData);

        // 多語系
        $systemGroupLanguage = [
            'zh-Hant' => [
                ['title' => '庫存管理']
            ],
            'zh-Hans' => [
                ['title' => '库存管理']
            ],
            'ja' => [
                ['title' => '在庫管理']
            ],
            'en' => [
                ['title' => 'Manage Quantity']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'system_parameter_group', $systemGroupLanguage, $languageList, $startGroupId, false);


        $startItemId = $rowItemId = SeederHelper::getTableNextIncrement('system_parameter_item');
        $rowItemId--;

        $systemItemData = [
            [
                'group_id' => $startGroupId,
                'value' => '1',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'danger']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId,
                'value' => '0',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 2,
            ],
        ];

        DB::table('system_parameter_item')->insert($systemItemData);

        // 多語系
        $systemItemLanguage = [
            'zh-Hant' => [
                ['label' => '啟用'], ['label' => '停用']
            ],
            'zh-Hans' => [
                ['label' => '启用'], ['label' => '停用']
            ],
            'ja' => [
                ['label' => '有効'], ['label' => '無効']
            ],
            'en' => [
                ['label' => 'Enable'], ['label' => 'Disable']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'system_parameter_item', $systemItemLanguage, $languageList, $startItemId, false);

        // 欄位擴充
        $startExtensionId = $extensionRowId = SeederHelper::getTableNextIncrement('column_extension');
        $extensionRowId--;
        $columnExtensionData = [
            ['table_name' => 'product_set', 'column_name' => 'details', 'sub_column_name' => 'description', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldTextarea'])],
            ['table_name' => 'product_set', 'column_name' => 'details', 'sub_column_name' => 'feature', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'product_set', 'column_name' => 'details', 'sub_column_name' => 'detail', 'sort' => 3, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'product_set', 'column_name' => 'details', 'sub_column_name' => 'specification', 'sort' => 4, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'product_set', 'column_name' => 'details', 'sub_column_name' => 'video', 'sort' => 5, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'product_set', 'column_name' => 'details', 'sub_column_name' => 'accessory', 'sort' => 6, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
        ];

        DB::table('column_extension')->insert($columnExtensionData);

        // 多語系
        $columnExtensionLanguage = [
            ['title' => '簡短敘述'], ['title' => '商品特色'], ['title' => '詳細介紹'],
            ['title' => '規格資訊'], ['title' => '影音資訊'], ['title' => '配件說明'],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'column_extension', $columnExtensionLanguage, $languageList, $startExtensionId);


        DB::table('language_resource')->insert($languageResourceData);
    }

    /**
     * Insert site parameters for this module.
     *
     * @return void
     */
    public function insertSiteParameters()
    {
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $startGroupId = $rowGroupId = SeederHelper::getTableNextIncrement('site_parameter_group');
        $rowGroupId--;

        $siteGroupData = [
            ['code' => 'rank', 'title' => 'site_parameter_group.title.' . ++$rowGroupId, 'category' => null, 'editable' => false],
            ['code' => 'property', 'title' => 'site_parameter_group.title.' . ++$rowGroupId, 'category' => null, 'editable' => false],
            ['code' => 'color', 'title' => 'site_parameter_group.title.' . ++$rowGroupId, 'category' => 'spec', 'editable' => true],
            ['code' => 'tags', 'title' => 'site_parameter_group.title.' . ++$rowGroupId, 'category' => null, 'editable' => true],
        ];

        DB::table('site_parameter_group')->insert($siteGroupData);

        // 多語系
        $siteGroupLanguage = [
            'zh-Hant' => [
                ['title' => '評價分數'], ['title' => '綜合屬性'], ['title' => '顏色'], ['title' => '標籤']
            ],
            'zh-Hans' => [
                ['title' => '评价分数'], ['title' => '综合属性'], ['title' => '颜色'], ['title' => '标签']
            ],
            'ja' => [
                ['title' => '評価スコア'], ['title' => '総合属性'], ['title' => '色'], ['title' => 'タグ']
            ],
            'en' => [
                ['title' => 'Rank Score'], ['title' => 'Property'], ['title' => 'Color'], ['title' => 'Tag']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'site_parameter_group', $siteGroupLanguage, $languageList, $startGroupId, false);


        $startItemId = $rowItemId = SeederHelper::getTableNextIncrement('site_parameter_item');
        $rowItemId--;

        $siteItemData = [
            [
                'group_id' => $startGroupId,
                'value' => '0',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId,
                'value' => '1',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId,
                'value' => '2',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 3,
            ],
            [
                'group_id' => $startGroupId,
                'value' => '3',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 4,
            ],
            [
                'group_id' => $startGroupId,
                'value' => '4',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 5,
            ],
            [
                'group_id' => $startGroupId,
                'value' => '5',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 6,
            ],
            [
                'group_id' => $startGroupId + 1,
                'value' => 'top',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 1,
                'value' => 'hot-sell',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 1,
                'value' => 'selected-recommend',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 3,
            ],
            [
                'group_id' => $startGroupId + 1,
                'value' => 'category-recommend',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 4,
            ],
            [
                'group_id' => $startGroupId + 2,
                'value' => null,
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 2,
                'value' => null,
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 2,
                'value' => null,
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 3,
            ],
            [
                'group_id' => $startGroupId + 3,
                'value' => null,
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 3,
                'value' => null,
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 3,
                'value' => null,
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'sort' => 3,
            ],
        ];

        DB::table('site_parameter_item')->insert($siteItemData);

        // 多語系
        $siteItemLanguage = [
            'zh-Hant' => [
                ['label' => '自動'], ['label' => '一星評價'], ['label' => '二星評價'], ['label' => '三星評價'], ['label' => '四星評價'], ['label' => '五星評價'],
                ['label' => '置頂'], ['label' => '熱門商品'], ['label' => '嚴選推薦'], ['label' => '分類推薦'],
                ['label' => '紅色'], ['label' => '黃色'], ['label' => '藍色'],
                ['label' => '10歲'], ['label' => '新品'], ['label' => '日韓風']
            ],
            'zh-Hans' => [
                ['label' => '自动'], ['label' => '一星评价'], ['label' => '二星评价'], ['label' => '三星评价'], ['label' => '四星评价'], ['label' => '五星评价'],
                ['label' => '置顶'], ['label' => '热门商品'], ['label' => '严选推荐'], ['label' => '分类推荐'],
                ['label' => '红色'], ['label' => '黄色'], ['label' => '蓝色'],
                ['label' => '10岁'], ['label' => '新品'], ['label' => '日韩风']
            ],
            'ja' => [
                ['label' => '自動'], ['label' => '1つ星評価'], ['label' => '2つ星評価'], ['label' => '3つ星評価'], ['label' => '4つ星評価'], ['label' => '5つ星評価'],
                ['label' => '頂上'], ['label' => '人気販売'], ['label' => '特別な推奨'], ['label' => '分類の推奨'],
                ['label' => '赤'], ['label' => 'イエロー'], ['label' => 'ブルー'],
                ['label' => '10歳'], ['label' => '新品'], ['label' => '日韓スタイル']
            ],
            'en' => [
                ['label' => 'Auto'], ['label' => '1 Star'], ['label' => '2 Stars'], ['label' => '3 Stars'], ['label' => '4 Stars'], ['label' => '5 Stars'],
                ['label' => 'Top'], ['label' => 'Hot Sell'], ['label' => 'Selected Recommend'], ['label' => 'Category Recommend'],
                ['label' => 'Red'], ['label' => 'Yellow'], ['label' => 'Blue'],
                ['label' => '10 years old'], ['label' => 'New arrival'], ['label' => 'J&K POP']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'site_parameter_item', $siteItemLanguage, $languageList, $startItemId, false);


        DB::table('language_resource')->insert($languageResourceData);
    }

    /**
     * Insert IO config setting for product.
     *
     * @return void
     */
    public function insertIoConstructs()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $startItemId = $rowItemId = SeederHelper::getTableNextIncrement('io_construct');
        $rowItemId--;

        $ioConfigData = [
            [
                'title' => 'io_construct.title.' . ++$rowItemId,
                'uri' => 'product-item',
                'import_enable' => true,
                'export_enable' => true,
                'import_permission' => 'productItemImport',
                'export_permission' => 'productItemExport',
                'import_view' => 'MinmaxProduct::admin.product-item.import',
                'export_view' => 'MinmaxProduct::admin.product-item.export',
                'controller' => 'Minmax\Product\Io\ProductItemController',
                'example' => 'controller',
                'filename' => 'io_construct.filename.' . $rowItemId,
                'sort' => $rowItemId, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'title' => 'io_construct.title.' . ++$rowItemId,
                'uri' => 'product-set',
                'import_enable' => true,
                'export_enable' => true,
                'import_permission' => 'productSetImport',
                'export_permission' => 'productSetExport',
                'import_view' => 'MinmaxProduct::admin.product-set.import',
                'export_view' => 'MinmaxProduct::admin.product-set.export',
                'controller' => 'Minmax\Product\Io\ProductSetController',
                'example' => 'controller',
                'filename' => 'io_construct.filename.' . $rowItemId,
                'sort' => $rowItemId, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'title' => 'io_construct.title.' . ++$rowItemId,
                'uri' => 'product-package',
                'import_enable' => true,
                'export_enable' => true,
                'import_permission' => 'productPackageImport',
                'export_permission' => 'productPackageExport',
                'import_view' => 'MinmaxProduct::admin.product-package.import',
                'export_view' => 'MinmaxProduct::admin.product-package.export',
                'controller' => 'Minmax\Product\Io\ProductPackageController',
                'example' => 'controller',
                'filename' => 'io_construct.filename.' . $rowItemId,
                'sort' => $rowItemId, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];

        DB::table('io_construct')->insert($ioConfigData);

        // 多語系
        $constructLanguage = [
            'zh-Hant' => [
                ['title' => '商品管理 - 品項資料', 'filename' => null], ['title' => '商品管理 - 商品資料', 'filename' => null], ['title' => '商品管理 - 價格組合', 'filename' => null]
            ],
            'zh-Hans' => [
                ['title' => '商品管理 - 品项资料', 'filename' => null], ['title' => '商品管理 - 商品资料', 'filename' => null], ['title' => '商品管理 - 价格组合', 'filename' => null]
            ],
            'ja' => [
                ['title' => '商品管理 - 項目情報', 'filename' => null], ['title' => '商品管理 - 商品情報', 'filename' => null], ['title' => '商品管理 - 価格組み合', 'filename' => null]
            ],
            'en' => [
                ['title' => 'Product Manage / Item Data', 'filename' => null], ['title' => 'Product Manage / Product Data', 'filename' => null], ['title' => 'Product Manage / Package Data', 'filename' => null]
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'io_construct', $constructLanguage, $languageList, $startItemId, false);

        DB::table('language_resource')->insert($languageResourceData);
    }

    /**
     * Insert product default data.
     *
     * @return void
     */
    public function insertProductData()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $productMarketData = [
            [
                'id' => $marketId = uuidl(),
                'code' => 'default',
                'title' => "product_market.title.{$marketId}",
                'details' => "product_market.details.{$marketId}",
                'sort' => 1, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];

        DB::table('product_market')->insert($productMarketData);

        // 多語系
        $marketLanguage = [
            'zh-Hant' => [
                ['id' => $marketId, 'title' => '預設賣場', 'details' => json_encode(['editor' => null, 'pic' => []])]
            ],
            'zh-Hans' => [
                ['id' => $marketId, 'title' => '预设卖场', 'details' => json_encode(['editor' => null, 'pic' => []])]
            ],
            'ja' => [
                ['id' => $marketId, 'title' => '既定店舗', 'details' => json_encode(['editor' => null, 'pic' => []])]
            ],
            'en' => [
                ['id' => $marketId, 'title' => 'Default', 'details' => json_encode(['editor' => null, 'pic' => []])]
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'product_market', $marketLanguage, $languageList, null, false);

        DB::table('language_resource')->insert($languageResourceData);
    }

    public function insertDashboard()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        // SiteDashboard
        $startDashboardId = $rowDashboardId = SeederHelper::getTableNextIncrement('site_dashboard');
        $rowDashboardId--;
        $siteDashboardData = [
            [
                'guard' => 'admin',
                'title' => 'site_dashboard.title.' . ++$rowDashboardId,
                'presenter' => '\Minmax\Product\Admin\ProductDashboardPresenter@totalProductSet',
                'position' => json_encode([
                    'row' => '2',
                    'column' => '3',
                    'width' => '3',
                    'height' => '0'
                ]),
                'options' => null,
                'sort' => 7,
                'active' => true,
                'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'guard' => 'administrator',
                'title' => 'site_dashboard.title.' . ++$rowDashboardId,
                'presenter' => '\Minmax\Product\Administrator\ProductDashboardPresenter@totalProductSet',
                'position' => json_encode([
                    'row' => '2',
                    'column' => '3',
                    'width' => '3',
                    'height' => '0'
                ]),
                'options' => null,
                'sort' => 7,
                'active' => true,
                'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];
        DB::table('site_dashboard')->insert($siteDashboardData);

        $siteDashboardLanguage = [
            'zh-Hant' => [
                ['title' => '商品數量'], ['title' => '商品數量']
            ],
            'zh-Hans' => [
                ['title' => '商品数量'], ['title' => '商品数量']
            ],
            'ja' => [
                ['title' => '商品数量'], ['title' => '商品数量']
            ],
            'en' => [
                ['title' => 'Products'], ['title' => 'Products']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'site_dashboard', $siteDashboardLanguage, $languageList, $startDashboardId, false);

        DB::table('language_resource')->insert($languageResourceData);
    }

    /**
     * Delete system parameters for this module.
     *
     * @return void
     */
    public function deleteSystemParameters()
    {
        $parameterCodeSet = ['qty_enable'];

        DB::table('system_parameter_group')->whereIn('code', $parameterCodeSet)->get()
            ->each(function ($group) {
                DB::table('system_parameter_item')->where('group_id', $group->id)->get()
                    ->each(function ($item) {
                        DB::table('language_resource')->where('key', 'system_parameter_item.label.' . $item->id)->delete();
                    });
                DB::table('language_resource')->where('key', 'system_parameter_group.title.' . $group->id)->delete();
            });

        DB::table('system_parameter_group')->whereIn('code', $parameterCodeSet)->delete();

        $columnExtensionTableSet = ['product_set'];

        DB::table('column_extension')->whereIn('table_name', $columnExtensionTableSet)->delete();
    }

    /**
     * Delete site parameters for this module.
     *
     * @return void
     */
    public function deleteSiteParameters()
    {
        $parameterCodeSet = ['property', 'color'];

        DB::table('site_parameter_group')->whereIn('code', $parameterCodeSet)->get()
            ->each(function ($group) {
                DB::table('site_parameter_item')->where('group_id', $group->id)->get()
                    ->each(function ($item) {
                        DB::table('language_resource')->where('key', 'site_parameter_item.label.' . $item->id)->delete();
                    });
                DB::table('language_resource')->where('key', 'site_parameter_group.title.' . $group->id)->delete();
            });

        DB::table('site_parameter_group')->whereIn('code', $parameterCodeSet)->delete();
    }

    /**
     * Delete IO constructs for this module.
     *
     * @return void
     */
    public function deleteIoConstructs()
    {
        $ioUriSet = ['product-item', 'product-set', 'product-package'];

        DB::table('io_construct')->whereIn('uri', $ioUriSet)->get()
            ->each(function ($item) {
                DB::table('language_resource')
                    ->where('key', 'io_construct.title.' . $item->id)
                    ->orWhere('key', 'io_construct.filename.' . $item->id)
                    ->delete();
            });

        DB::table('io_construct')->whereIn('uri', $ioUriSet)->delete();
    }
}
