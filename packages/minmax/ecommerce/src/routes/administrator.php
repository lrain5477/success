<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['administrator', 'localizationRedirect'],
    'as' => 'administrator.' . app()->getLocale() . '.'
], function() {

    Route::group(['prefix' => 'administrator', 'namespace' => 'Minmax\Ecommerce\Administrator', 'middleware' => 'auth:administrator'], function () {
        /*
         |--------------------------------------------------------------------------
         | 需要登入的路由。
         |--------------------------------------------------------------------------
         */

        /*
         * CartConfig 購物車設定
         */
        Route::get('cart-config', 'CartConfigController@index')->name('cart-config.index');
        Route::post('cart-config', 'CartConfigController@store')->name('cart-config.store');
        Route::get('cart-config/create', 'CartConfigController@create')->name('cart-config.create');
        Route::get('cart-config/{id}', 'CartConfigController@show')->name('cart-config.show');
        Route::put('cart-config/{id}', 'CartConfigController@update')->name('cart-config.update');
        Route::delete('cart-config/{id}', 'CartConfigController@destroy')->name('cart-config.destroy');
        Route::get('cart-config/{id}/edit', 'CartConfigController@edit')->name('cart-config.edit');
        Route::post('cart-config/ajax/datatables', 'CartConfigController@ajaxDataTable')->name('cart-config.ajaxDataTable');

        /*
         * CartDelivery 取貨方式
         */
        Route::get('cart-delivery', 'CartDeliveryController@index')->name('cart-delivery.index');
        Route::post('cart-delivery', 'CartDeliveryController@store')->name('cart-delivery.store');
        Route::get('cart-delivery/create', 'CartDeliveryController@create')->name('cart-delivery.create');
        Route::get('cart-delivery/{id}', 'CartDeliveryController@show')->name('cart-delivery.show');
        Route::put('cart-delivery/{id}', 'CartDeliveryController@update')->name('cart-delivery.update');
        Route::delete('cart-delivery/{id}', 'CartDeliveryController@destroy')->name('cart-delivery.destroy');
        Route::get('cart-delivery/{id}/edit', 'CartDeliveryController@edit')->name('cart-delivery.edit');
        Route::post('cart-delivery/ajax/datatables', 'CartDeliveryController@ajaxDataTable')->name('cart-delivery.ajaxDataTable');
        Route::patch('cart-delivery/ajax/switch', 'CartDeliveryController@ajaxSwitch')->name('cart-delivery.ajaxSwitch');
        Route::patch('cart-delivery/ajax/sort', 'CartDeliveryController@ajaxSort')->name('cart-delivery.ajaxSort');

        /*
         * CartPayment 付款方式
         */
        Route::get('cart-payment', 'CartPaymentController@index')->name('cart-payment.index');
        Route::post('cart-payment', 'CartPaymentController@store')->name('cart-payment.store');
        Route::get('cart-payment/create', 'CartPaymentController@create')->name('cart-payment.create');
        Route::get('cart-payment/{id}', 'CartPaymentController@show')->name('cart-payment.show');
        Route::put('cart-payment/{id}', 'CartPaymentController@update')->name('cart-payment.update');
        Route::delete('cart-payment/{id}', 'CartPaymentController@destroy')->name('cart-payment.destroy');
        Route::get('cart-payment/{id}/edit', 'CartPaymentController@edit')->name('cart-payment.edit');
        Route::post('cart-payment/ajax/datatables', 'CartPaymentController@ajaxDataTable')->name('cart-payment.ajaxDataTable');
        Route::patch('cart-payment/ajax/switch', 'CartPaymentController@ajaxSwitch')->name('cart-payment.ajaxSwitch');
        Route::patch('cart-payment/ajax/sort', 'CartPaymentController@ajaxSort')->name('cart-payment.ajaxSort');

        /*
         * CartShipping 運費設定
         */
        Route::get('cart-shipping', 'CartShippingController@index')->name('cart-shipping.index');
        Route::post('cart-shipping', 'CartShippingController@store')->name('cart-shipping.store');
        Route::get('cart-shipping/create', 'CartShippingController@create')->name('cart-shipping.create');
        Route::get('cart-shipping/{id}', 'CartShippingController@show')->name('cart-shipping.show');
        Route::put('cart-shipping/{id}', 'CartShippingController@update')->name('cart-shipping.update');
        Route::delete('cart-shipping/{id}', 'CartShippingController@destroy')->name('cart-shipping.destroy');
        Route::get('cart-shipping/{id}/edit', 'CartShippingController@edit')->name('cart-shipping.edit');
        Route::post('cart-shipping/ajax/datatables', 'CartShippingController@ajaxDataTable')->name('cart-shipping.ajaxDataTable');
        Route::patch('cart-shipping/ajax/switch', 'CartShippingController@ajaxSwitch')->name('cart-shipping.ajaxSwitch');
        Route::patch('cart-shipping/ajax/sort', 'CartShippingController@ajaxSort')->name('cart-shipping.ajaxSort');
        Route::patch('cart-shipping/ajax/multi/switch', 'CartShippingController@ajaxMultiSwitch')->name('cart-shipping.ajaxMultiSwitch');
        Route::delete('cart-shipping/ajax/multi/delete', 'CartShippingController@ajaxMultiDestroy')->name('cart-shipping.ajaxMultiDestroy');

    });

});
