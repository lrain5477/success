<?php

if (! function_exists('cartConfig')) {
    /**
     * @param  string $key
     * @param  string $guard
     * @return mixed|null
     */
    function cartConfig($key, $guard = 'web')
    {
        $configs = Cache::rememberForever("cartConfigs.{$guard}", function () use ($guard) {
            return \Minmax\Ecommerce\Models\CartConfig::query()
                ->where('id', $guard)
                ->first();
        });

        if (blank($configs)) {
            Cache::forget("cartConfigs.{$guard}");
            return null;
        }

        return array_get($configs, $key);
    }
}
