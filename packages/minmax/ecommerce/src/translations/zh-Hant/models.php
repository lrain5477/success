<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Models (Database Column) Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in backend platform page.
    |
    */

    'CartConfig' => [
        'id' => 'ID',
        'browse_history' => '瀏覽紀錄保存',
        'track_list' => '追蹤清單保存',
        'shipping_notify' => '貨到通知保存',
        'return_limit' => '退貨有效期限',
        'rank_limit' => '評價有效期限',
        'less_quantity' => '最低庫存警示',
        'skip_pick_relation' => '自動出貨設定',
        'skip_pick_auto' => '自動出貨方式',
        'wrap_price' => '額外包裝費用',
        'bonus_exchange' => '折抵兌率',
        'bonus_percent' => '折抵比例',
        'information' => '購物說明',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'return_limit' => '退貨將於出貨起算幾天後不可退貨。',
            'less_quantity' => '低於此庫存的品項將發信件或簡訊通知管理者。',
            'skip_pick_relation' => '預設為未付款不能檢貨與出貨，但您可勾選付款方式，即可於未付款狀態中撿貨或出貨商品。',
            'skip_pick_auto' => '設定未付款仍可出貨的付款模式。',
        ],
    ],

    'CartDelivery' => [
        'id' => 'ID',
        'code' => '取貨代碼',
        'title' => '取貨名稱',
        'details' => '說明細節',
        'limits' => '金額限制',
        'delivery_types' => '物流類型',
        'delivery_service' => '物流服務',
        'options' => '參數設定',
        'sort' => '排序',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'code' => '取貨代碼必須唯一，不可與其他取貨方式重複。',
            'limits' => '設定訂單金額限制，未於該金額範圍之訂單將無法使用此方式。設定為 0 表示不設限。',
            'delivery_types' => '群組化以便提供快速批次選擇，可複選多種模式。更多設定請至 參數項目 設定。',
            'delivery_service' => '設定取貨方式所適用的貨運服務。',
        ],
    ],

    'CartPayment' => [
        'id' => 'ID',
        'code' => '付款代碼',
        'title' => '付款名稱',
        'details' => '說明細節',
        'limits' => '金額限制',
        'payment_types' => '金流類型',
        'delivery_types' => '物流類型',
        'payment_service' => '金流服務',
        'options' => '參數設定',
        'sort' => '排序',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'code' => '付款代碼必須唯一，不可與其他取貨方式重複。',
            'limits' => '設定訂單金額限制，未於該金額範圍之訂單將無法使用此方式。設定為 0 表示不設限。',
            'payment_types' => '群組化以便提供快速批次選擇，可複選多種模式。更多設定請至 參數項目 設定。',
            'delivery_types' => '群組化以便提供快速批次選擇，可複選多種模式。更多設定請至 參數項目 設定。',
            'payment_service' => '設定付款方式所適用的金流服務。',
        ],
    ],

    'CartShipping' => [
        'id' => 'ID',
        'title' => '設定名稱',
        'details' => '說明細節',
        'price' => '運費金額',
        'deliveries' => '取貨方式',
        'location_limits' => [
            'country' => '國家設定',
            'state' => '州區設定',
            'county' => '縣市設定',
            'city' => '城鎮設定',
        ],
        'measuring_limits' => [
            'perimeter' => '尺寸限制',
            'volume' => '體積限制',
            'space' => '材積限制',
            'weight' => '重量限制',
        ],
        'product_brands' => '商品品牌',
        'product_categories' => '商品分類',
        'product_sets' => '適用商品',
        'start_at' => '開始時間',
        'end_at' => '結束時間',
        'sort' => '排序',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'deliveries' => '設定取貨方式加以限定適用範圍，可複選多種方式。更多設定請至 取貨方式 設定。',
            'location_limits' => '設定符合此運費規則的運送目的地國家、州區、縣市、城鎮。',
            'measuring_limits' => [
                'perimeter' => '設定貨物的尺寸最高上限。尺寸(cm) = 長 + 寬 + 高。',
                'volume' => '設定貨物的體積最高上限。體積(cm³) = 長 x 寬 x 高。',
                'space' => '設定貨物的材積最高上限。材積(Cuft) = 體積(cm³) x 0.0000353。',
                'weight' => '設定貨物的重量最高上限。貨物的實際秤測重量(kg)。',
            ],
            'product_brands' => '更多設定請至 品牌管理 設定。',
            'product_sets' => '請輸入商品貨號，以將商品加入條件列表，商品貨號請參考自 <a href=":link" target="_blank">商品管理</a> 列表。',
            'start_at' => '若運費規則有時效限制，可在此設定生效時間。',
            'end_at' => '若運費規則有時效限制，可在此設定失效時間。',
            'sort' => '排序數字越小表示越優先排序。',
        ],
    ],

];
