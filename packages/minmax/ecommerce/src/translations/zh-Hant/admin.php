<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    'form' => [
        'fieldSet' => [
            'auto_patch' => '自動批次設定',
            'wrap_setting' => '包裝費用',
            'bonus_setting' => '紅利折抵',
            'workflow' => '流程說明',
            'locations' => '適用地區',
            'measuring' => '適用大小',
            'products' => '適用商品',
            'shipping_price' => '運費設定',
            'range' => '適用範圍',
        ],
        'CartConfig' => [
            'wrap_price' => '包裝費用',
            'bonus_setting' => '折抵設定',
            'currency' => '貨幣別',
            'please_select' => '請選擇',
            'price' => '金額',
            'bonus_exchange' => '每1點折抵訂單金額',
            'bonus_percent' => '最高折抵訂單百分比',
            'actions' => '動作',
        ],
        'CartDelivery' => [
            'limits' => '金額限制',
            'currency' => '貨幣別',
            'please_select' => '請選擇',
            'price_min' => '最低接受金額',
            'price_max' => '最高接受金額',
            'actions' => '動作',
        ],
        'CartPayment' => [
            'limits' => '金額限制',
            'currency' => '貨幣別',
            'please_select' => '請選擇',
            'price_min' => '最低接受金額',
            'price_max' => '最高接受金額',
            'actions' => '動作',
        ],
        'CartShipping' => [
            'product_sku' => '商品貨號',
            'product_title' => '商品名稱',
            'please_product_sku' => '請輸入商品貨號',
            'currency' => '貨幣別',
            'please_select' => '請選擇',
            'price' => '運費金額',
            'fee' => '額外費用',
            'tax' => '附加稅率',
            'minimum' => '最低訂單金額',
            'actions' => '動作',
            'tooltip' => [
                'fee' => '例如：手續費 50 元',
                'tax' => '例如：進口稅 25%',
            ],
            'alert' => [
                'product_not_exist' => [
                    'title' => '商品不存在',
                    'info' => '您所指定的商品貨號無法找到商品！'
                ],
                'product_duplicate' => [
                    'title' => '商品重複',
                    'info' => '您所指定的商品貨號已存在於列表！'
                ],
            ],
        ],
    ],

];
