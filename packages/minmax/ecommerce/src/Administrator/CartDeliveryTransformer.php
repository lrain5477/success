<?php

namespace Minmax\Ecommerce\Administrator;

use Minmax\Base\Administrator\Transformer;
use Minmax\Ecommerce\Models\CartDelivery;

/**
 * Class CartDeliveryTransformer
 */
class CartDeliveryTransformer extends Transformer
{
    /**
     * Transformer constructor.
     * @param  CartDeliveryPresenter $presenter
     * @param  string $uri
     */
    public function __construct(CartDeliveryPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  CartDelivery $model
     * @return array
     * @throws \Throwable
     */
    public function transform(CartDelivery $model)
    {
        return [
            'code' => $this->presenter->getGridText($model, 'code'),
            'title' => $this->presenter->getGridText($model, 'title'),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
