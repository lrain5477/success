<?php

namespace Minmax\Ecommerce\Administrator;

use Minmax\Base\Administrator\Presenter;
use Minmax\Base\Administrator\ServiceConfigRepository;
use Minmax\World\Administrator\WorldCurrencyRepository;

/**
 * Class CartPaymentPresenter
 */
class CartPaymentPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxEcommerce::';

    protected $languageColumns = ['title', 'details'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'currencies' => (new WorldCurrencyRepository)->getSelectParameters('active', true),
            'payment_types' => siteParam('payment_type'),
            'delivery_types' => siteParam('delivery_type'),
            'delivery_service' => (new ServiceConfigRepository)->getSelectParameters('payment'),
            'active' => systemParam('active'),
        ];
    }

    /**
     * @param  \Minmax\Ecommerce\Models\CartPayment $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFieldLimitsPriceList($model)
    {
        $modelName = 'CartPayment';
        $columnValue = $this->getModelValue($model, 'limits') ?? [];

        $fieldLabel = __("MinmaxEcommerce::models.CartPayment.limits");
        $fieldValue = $columnValue;

        $componentData = [
            'id' => 'CartPayment-limits',
            'label' => $fieldLabel,
            'name' => $modelName,
            'currencies' => array_get($this->parameterSet, 'currencies', []),
            'values' => $fieldValue,
        ];

        return view('MinmaxEcommerce::administrator.cart-payment.form-limits-price-list', $componentData);
    }

    /**
     * @param  \Minmax\Ecommerce\Models\CartPayment $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getShowLimitsPriceList($model)
    {
        $modelName = 'CartPayment';
        $columnValue = $this->getModelValue($model, 'limits') ?? [];

        $fieldLabel = __("MinmaxEcommerce::models.CartPayment.limits");
        $fieldValue = $columnValue;

        $componentData = [
            'id' => 'CartPayment-limits',
            'label' => $fieldLabel,
            'name' => $modelName,
            'currencies' => array_get($this->parameterSet, 'currencies', []),
            'values' => $fieldValue,
        ];

        return view('MinmaxEcommerce::administrator.cart-payment.show-limits-price-list', $componentData);
    }
}
