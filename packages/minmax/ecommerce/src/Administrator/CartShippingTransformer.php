<?php

namespace Minmax\Ecommerce\Administrator;

use Minmax\Base\Administrator\Transformer;
use Minmax\Ecommerce\Models\CartShipping;

/**
 * Class CartShippingTransformer
 */
class CartShippingTransformer extends Transformer
{
    /**
     * Transformer constructor.
     * @param  CartShippingPresenter $presenter
     * @param  string $uri
     */
    public function __construct(CartShippingPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  CartShipping $model
     * @return array
     * @throws \Throwable
     */
    public function transform(CartShipping $model)
    {
        return [
            'id' => $this->presenter->getGridCheckBox($model),
            'title' => $this->presenter->getGridText($model, 'title'),
            'updated_at' => $this->presenter->getPureString($model->updated_at->format('Y-m-d')),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
