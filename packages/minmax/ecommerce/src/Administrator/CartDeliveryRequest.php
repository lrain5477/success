<?php

namespace Minmax\Ecommerce\Administrator;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class CartDeliveryRequest
 */
class CartDeliveryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'CartDelivery.code' => [
                        'required',
                        'string',
                        Rule::unique('cart_delivery', 'code')->ignore($this->route('id'))
                    ],
                    'CartDelivery.title' => 'required|string',
                    'CartDelivery.details' => 'nullable|array',
                    'CartDelivery.limits' => 'nullable|array',
                    'CartDelivery.delivery_types' => 'nullable|array',
                    'CartDelivery.delivery_service' => 'nullable|array',
                    'CartDelivery.sort' => 'required|integer',
                    'CartDelivery.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'CartDelivery.code' => 'required|string|unique:cart_delivery,code',
                    'CartDelivery.title' => 'required|string',
                    'CartDelivery.details' => 'nullable|array',
                    'CartDelivery.limits' => 'nullable|array',
                    'CartDelivery.delivery_types' => 'nullable|array',
                    'CartDelivery.delivery_service' => 'nullable|array',
                    'CartDelivery.sort' => 'nullable|integer',
                    'CartDelivery.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'CartDelivery.code' => __('MinmaxEcommerce::models.CartDelivery.code'),
            'CartDelivery.title' => __('MinmaxEcommerce::models.CartDelivery.title'),
            'CartDelivery.details' => __('MinmaxEcommerce::models.CartDelivery.details'),
            'CartDelivery.limits' => __('MinmaxEcommerce::models.CartDelivery.limits'),
            'CartDelivery.delivery_types' => __('MinmaxEcommerce::models.CartDelivery.delivery_types'),
            'CartDelivery.delivery_service' => __('MinmaxEcommerce::models.CartDelivery.delivery_service'),
            'CartDelivery.options' => __('MinmaxEcommerce::models.CartDelivery.options'),
            'CartDelivery.sort' => __('MinmaxEcommerce::models.CartDelivery.sort'),
            'CartDelivery.active' => __('MinmaxEcommerce::models.CartDelivery.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('administrator', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('administrator', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
