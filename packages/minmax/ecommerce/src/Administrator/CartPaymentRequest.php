<?php

namespace Minmax\Ecommerce\Administrator;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class CartPaymentRequest
 */
class CartPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'CartPayment.code' => [
                        'required',
                        'string',
                        Rule::unique('cart_payment', 'code')->ignore($this->route('id'))
                    ],
                    'CartPayment.title' => 'required|string',
                    'CartPayment.details' => 'nullable|array',
                    'CartPayment.limits' => 'nullable|array',
                    'CartPayment.payment_types' => 'nullable|array',
                    'CartPayment.delivery_types' => 'nullable|array',
                    'CartPayment.payment_service' => 'nullable|string',
                    'CartPayment.options' => 'nullable|array',
                    'CartPayment.sort' => 'required|integer',
                    'CartPayment.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'CartPayment.code' => 'required|string|unique:cart_payment,code',
                    'CartPayment.title' => 'required|string',
                    'CartPayment.details' => 'nullable|array',
                    'CartPayment.limits' => 'nullable|array',
                    'CartPayment.payment_types' => 'nullable|array',
                    'CartPayment.delivery_types' => 'nullable|array',
                    'CartPayment.payment_service' => 'nullable|string',
                    'CartPayment.options' => 'nullable|array',
                    'CartPayment.sort' => 'nullable|integer',
                    'CartPayment.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'CartPayment.code' => __('MinmaxEcommerce::models.CartPayment.code'),
            'CartPayment.title' => __('MinmaxEcommerce::models.CartPayment.title'),
            'CartPayment.details' => __('MinmaxEcommerce::models.CartPayment.details'),
            'CartPayment.limits' => __('MinmaxEcommerce::models.CartPayment.limits'),
            'CartPayment.payment_types' => __('MinmaxEcommerce::models.CartPayment.payment_types'),
            'CartPayment.delivery_types' => __('MinmaxEcommerce::models.CartPayment.delivery_types'),
            'CartPayment.payment_service' => __('MinmaxEcommerce::models.CartPayment.payment_service'),
            'CartPayment.options' => __('MinmaxEcommerce::models.CartPayment.options'),
            'CartPayment.sort' => __('MinmaxEcommerce::models.CartPayment.sort'),
            'CartPayment.active' => __('MinmaxEcommerce::models.CartPayment.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('administrator', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('administrator', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
