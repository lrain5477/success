<?php

namespace Minmax\Ecommerce\Administrator;

use Minmax\Base\Administrator\Repository;
use Minmax\Ecommerce\Models\CartDelivery;

/**
 * Class CartDeliveryRepository
 * @property CartDelivery $model
 * @method CartDelivery find($id)
 * @method CartDelivery one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method CartDelivery[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method CartDelivery create($attributes)
 * @method CartDelivery save($model, $attributes)
 * @method CartDelivery|\Illuminate\Database\Eloquent\Builder query()
 */
class CartDeliveryRepository extends Repository
{
    const MODEL = CartDelivery::class;

    protected $sort = 'sort';

    protected $sorting = true;

    protected $languageColumns = ['title', 'details'];

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'cart_delivery';
    }

    public function getSelectParameters()
    {
        return $this->all(...func_get_args())
            ->sortBy('sort')
            ->mapWithKeys(function ($item) {
                /** @var CartDelivery $item */
                return [$item->code => ['title' => $item->title, 'options' => $item->options]];
            })
            ->toArray();
    }
}
