<?php

namespace Minmax\Ecommerce\Administrator;

use Illuminate\Support\Facades\Cache;
use Minmax\Base\Administrator\Repository;
use Minmax\Ecommerce\Models\CartConfig;

/**
 * Class CartConfigRepository
 * @property CartConfig $model
 * @method CartConfig find($id)
 * @method CartConfig one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method CartConfig[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method CartConfig create($attributes)
 * @method CartConfig save($model, $attributes)
 * @method CartConfig|\Illuminate\Database\Eloquent\Builder query()
 */
class CartConfigRepository extends Repository
{
    const MODEL = CartConfig::class;

    protected $languageColumns = ['information'];

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'cart_config';
    }

    /**
     * Serialize input attributes to a new model
     *
     * @param  array $attributes
     * @return \Minmax\Ecommerce\Models\CartConfig
     */
    protected function serialization($attributes = null)
    {
        $this->clearLanguageBuffer();

        $attributes = $attributes ?? $this->attributes;

        $model = static::MODEL;
        /** @var \Minmax\Ecommerce\Models\CartConfig $model */
        $model = new $model();

        $primaryKey = array_get($this->attributes, 'id', uuidl());

        foreach ($attributes as $column => $value) {
            if (in_array($column, $this->languageColumns)) {
                $model->setAttribute($column, $this->exchangeLanguage($attributes, $column, $primaryKey));
            } else {
                $model->setAttribute($column, $value);
            }
        }

        return $model;
    }

    protected function afterCreate()
    {
        Cache::forget("cartConfigs.{$this->model->id}");
    }

    protected function afterSave()
    {
        Cache::forget("cartConfigs.{$this->model->id}");
    }
}
