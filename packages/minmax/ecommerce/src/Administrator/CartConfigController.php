<?php

namespace Minmax\Ecommerce\Administrator;

use Minmax\Base\Administrator\Controller;

/**
 * Class CartConfigController
 */
class CartConfigController extends Controller
{
    protected $packagePrefix = 'MinmaxEcommerce::';

    public function __construct(CartConfigRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }
}
