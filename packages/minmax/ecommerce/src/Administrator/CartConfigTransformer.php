<?php

namespace Minmax\Ecommerce\Administrator;

use Minmax\Base\Administrator\Transformer;
use Minmax\Ecommerce\Models\CartConfig;

/**
 * Class CartConfigTransformer
 */
class CartConfigTransformer extends Transformer
{
    /**
     * Transformer constructor.
     * @param  CartConfigPresenter $presenter
     * @param  string $uri
     */
    public function __construct(CartConfigPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  CartConfig $model
     * @return array
     * @throws \Throwable
     */
    public function transform(CartConfig $model)
    {
        return [
            'id' => $this->presenter->getGridText($model, 'id'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
