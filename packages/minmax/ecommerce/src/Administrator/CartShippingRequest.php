<?php

namespace Minmax\Ecommerce\Administrator;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class CartShippingRequest
 */
class CartShippingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'CartShipping.title' => 'required|string',
                    'CartShipping.details' => 'nullable|array',
                    'CartShipping.price' => 'required|array',
                    'CartShipping.deliveries' => 'nullable|array',
                    'CartShipping.location_limits' => 'nullable|array',
                    'CartShipping.measuring_limits.perimeter' => 'nullable|integer',
                    'CartShipping.measuring_limits.volume' => 'nullable|integer',
                    'CartShipping.measuring_limits.space' => 'nullable|integer',
                    'CartShipping.measuring_limits.weight' => 'nullable|integer',
                    'CartShipping.product_brands' => 'nullable|array',
                    'CartShipping.product_categories' => 'nullable|array',
                    'CartShipping.product_sets' => 'nullable|array',
                    'CartShipping.start_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'CartShipping.end_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'CartShipping.sort' => 'required|integer|min:1',
                    'CartShipping.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'CartShipping.title' => 'required|string',
                    'CartShipping.details' => 'nullable|array',
                    'CartShipping.price' => 'required|array',
                    'CartShipping.deliveries' => 'nullable|array',
                    'CartShipping.location_limits' => 'nullable|array',
                    'CartShipping.measuring_limits.perimeter' => 'nullable|integer',
                    'CartShipping.measuring_limits.volume' => 'nullable|integer',
                    'CartShipping.measuring_limits.space' => 'nullable|integer',
                    'CartShipping.measuring_limits.weight' => 'nullable|integer',
                    'CartShipping.product_brands' => 'nullable|array',
                    'CartShipping.product_categories' => 'nullable|array',
                    'CartShipping.product_sets' => 'nullable|array',
                    'CartShipping.start_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'CartShipping.end_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'CartShipping.sort' => 'nullable|integer|min:1',
                    'CartShipping.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'CartShipping.title' => __('MinmaxEcommerce::models.CartShipping.title'),
            'CartShipping.details' => __('MinmaxEcommerce::models.CartShipping.details'),
            'CartShipping.price' => __('MinmaxEcommerce::models.CartShipping.price'),
            'CartShipping.deliveries' => __('MinmaxEcommerce::models.CartShipping.deliveries'),
            'CartShipping.location_limits' => __('MinmaxEcommerce::models.CartShipping.location_limits'),
            'CartShipping.measuring_limits.perimeter' => __('MinmaxEcommerce::models.CartShipping.measuring_limits.perimeter'),
            'CartShipping.measuring_limits.volume' => __('MinmaxEcommerce::models.CartShipping.measuring_limits.volume'),
            'CartShipping.measuring_limits.space' => __('MinmaxEcommerce::models.CartShipping.measuring_limits.space'),
            'CartShipping.measuring_limits.weight' => __('MinmaxEcommerce::models.CartShipping.measuring_limits.weight'),
            'CartShipping.product_brands' => __('MinmaxEcommerce::models.CartShipping.product_brands'),
            'CartShipping.product_categories' => __('MinmaxEcommerce::models.CartShipping.product_categories'),
            'CartShipping.product_sets' => __('MinmaxEcommerce::models.CartShipping.product_sets'),
            'CartShipping.start_at' => __('MinmaxEcommerce::models.CartShipping.start_at'),
            'CartShipping.end_at' => __('MinmaxEcommerce::models.CartShipping.end_at'),
            'CartShipping.sort' => __('MinmaxEcommerce::models.CartShipping.sort'),
            'CartShipping.active' => __('MinmaxEcommerce::models.CartShipping.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('administrator', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('administrator', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
