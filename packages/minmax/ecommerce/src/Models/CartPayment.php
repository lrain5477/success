<?php

namespace Minmax\Ecommerce\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CartPayment
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property array $details
 * @property array $limits
 * @property array $payment_types
 * @property array $delivery_types
 * @property string $payment_service
 * @property array $options
 * @property integer $sort
 * @property boolean $active
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class CartPayment extends Model
{
    protected $table = 'cart_payment';
    protected $guarded = [];
    protected $casts = [
        'limits' => 'array',
        'payment_types' => 'array',
        'delivery_types' => 'array',
        'options' => 'array',
        'active' => 'boolean',
    ];

    public function getTitleAttribute()
    {
        return langDB($this->getAttributeFromArray('title'));
    }

    public function getDetailsAttribute()
    {
        return json_decode(langDB($this->getAttributeFromArray('details')), true);
    }
}
