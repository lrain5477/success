<?php

namespace Minmax\Ecommerce\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CartDelivery
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property array $details
 * @property array $limits
 * @property array $delivery_types
 * @property array $delivery_service
 * @property array $options
 * @property integer $sort
 * @property boolean $active
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class CartDelivery extends Model
{
    protected $table = 'cart_delivery';
    protected $guarded = [];
    protected $casts = [
        'limits' => 'array',
        'delivery_types' => 'array',
        'delivery_service' => 'array',
        'options' => 'array',
        'active' => 'boolean',
    ];

    public function getTitleAttribute()
    {
        return langDB($this->getAttributeFromArray('title'));
    }

    public function getDetailsAttribute()
    {
        return json_decode(langDB($this->getAttributeFromArray('details')), true);
    }
}
