<?php

namespace Minmax\Ecommerce\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CartShipping
 * @property integer $id
 * @property string $title
 * @property array $details
 * @property array $price
 * @property array $deliveries
 * @property array $location_limits
 * @property array $measuring_limits
 * @property array $product_brands
 * @property array $product_categories
 * @property array $product_sets
 * @property \Illuminate\Support\Carbon $start_at
 * @property \Illuminate\Support\Carbon $end_at
 * @property integer $sort
 * @property boolean $active
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class CartShipping extends Model
{
    protected $table = 'cart_shipping';
    protected $guarded = [];
    protected $dates = ['start_at', 'end_at', 'created_at', 'updated_at'];
    protected $casts = [
        'price' => 'array',
        'deliveries' => 'array',
        'location_limits' => 'array',
        'measuring_limits' => 'array',
        'product_brands' => 'array',
        'product_categories' => 'array',
        'product_sets' => 'array',
        'active' => 'boolean',
    ];

    public function getTitleAttribute()
    {
        return langDB($this->getAttributeFromArray('title'));
    }

    public function getDetailsAttribute()
    {
        return json_decode(langDB($this->getAttributeFromArray('details')), true);
    }
}
