<?php

namespace Minmax\Ecommerce\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CartConfig
 * @property string $id
 * @property integer $browse_history
 * @property integer $track_list
 * @property integer $shipping_notify
 * @property integer $return_limit
 * @property integer $rank_limit
 * @property integer $less_quantity
 * @property array $skip_pick_relation
 * @property boolean $skip_pick_auto
 * @property array $wrap_price
 * @property array $bonus_exchange
 * @property array $bonus_percent
 * @property array $information
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class CartConfig extends Model
{
    protected $table = 'cart_config';
    protected $guarded = [];
    protected $casts = [
        'skip_pick_relation' => 'array',
        'skip_pick_auto' => 'boolean',
        'wrap_price' => 'array',
        'bonus_exchange' => 'array',
        'bonus_percent' => 'array',
    ];

    public $incrementing = false;

    public function getInformationAttribute()
    {
        return json_decode(langDB($this->getAttributeFromArray('information')), true);
    }
}
