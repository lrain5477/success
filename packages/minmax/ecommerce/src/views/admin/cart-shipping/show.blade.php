<?php
/**
 * Show page of model CartShipping
 *
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Ecommerce\Models\CartShipping $formData
 */
?>

@extends('MinmaxBase::admin.layouts.page.show')

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links', ['languageActive' => $languageActive])
        @if($adminData->can('cartShippingShow'))
        <a class="btn btn-sm btn-light" href="{{ langRoute("admin.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::admin.form.back_list')">
            <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.back_list')</span>
        </a>
        @endif
        @if($adminData->can('cartShippingEdit'))
        <a class="btn btn-sm btn-main" href="{{ langRoute("admin.{$pageData->uri}.edit", [$formData->id]) }}" title="@lang('MinmaxBase::admin.form.edit')">
            <i class="icon-pencil"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.edit')</span>
        </a>
        @endif
    @endcomponent
@endsection

@section('views')
    @inject('modelPresenter', 'Minmax\Ecommerce\Admin\CartShippingPresenter')

    <fieldset>
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.default')</legend>

        {!! $modelPresenter->getShowNormalText($formData, 'title') !!}

        {!! $modelPresenter->getShowColumnExtension($formData, 'details') !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::admin.form.fieldSet.locations')</legend>

        {!! $modelPresenter->getShowLocations($formData, 'location_limits') !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::admin.form.fieldSet.measuring')</legend>

        {!! $modelPresenter->getShowNormalText($formData, 'measuring_limits', ['subColumn' => 'perimeter']) !!}

        {!! $modelPresenter->getShowNormalText($formData, 'measuring_limits', ['subColumn' => 'volume']) !!}

        {!! $modelPresenter->getShowNormalText($formData, 'measuring_limits', ['subColumn' => 'space']) !!}

        {!! $modelPresenter->getShowNormalText($formData, 'measuring_limits', ['subColumn' => 'weight']) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::admin.form.fieldSet.range')</legend>

        {!! $modelPresenter->getShowMultiSelection($formData, 'deliveries') !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::admin.form.fieldSet.products')</legend>

        {!! $modelPresenter->getShowMultiSelection($formData, 'product_brands') !!}

        {!! $modelPresenter->getShowMultiSelection($formData, 'product_categories') !!}

        {!! $modelPresenter->getShowProducts($formData, 'product_sets') !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::admin.form.fieldSet.shipping_price')</legend>

        {!! $modelPresenter->getShowPriceList($formData, 'price') !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.advanced')</legend>

        {!! $modelPresenter->getShowNormalText($formData, 'start_at') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'end_at') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'sort') !!}

        {!! $modelPresenter->getShowSelection($formData, 'active') !!}

    </fieldset>

    <fieldset class="mt-4" id="sysFieldSet">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.system_record')</legend>

        {!! $modelPresenter->getShowNormalText($formData, 'created_at') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'updated_at') !!}

    </fieldset>

@endsection
