<?php
/**
 * @var string $id
 * @var string $label
 * @var string $name
 * @var array $currencies
 * @var array $values
 */
?>
<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="{{ $id }}">{{ $label }}<span class="text-danger ml-1">*</span></label>
    <div class="col-sm-10" id="{{ $id }}">
        <input type="hidden" id="{{ $id }}-price-empty" name="{{ $name }}[price]" value="" {{ count($values) > 0 ? 'disabled' : '' }} />
        <table class="table table-sm table-bordered mb-1">
            <thead class="thead-default">
            <tr class="text-center">
                <th>@lang("MinmaxEcommerce::admin.form.CartShipping.currency")</th>
                <th>@lang("MinmaxEcommerce::admin.form.CartShipping.price")</th>
                <th data-toggle="tooltip" data-placement="top" data-original-title="@lang("MinmaxEcommerce::admin.form.CartShipping.tooltip.fee")">@lang("MinmaxEcommerce::admin.form.CartShipping.fee")</th>
                <th data-toggle="tooltip" data-placement="top" data-original-title="@lang("MinmaxEcommerce::admin.form.CartShipping.tooltip.tax")">@lang("MinmaxEcommerce::admin.form.CartShipping.tax")</th>
                <th>@lang("MinmaxEcommerce::admin.form.CartShipping.minimum")</th>
                <th>@lang("MinmaxEcommerce::admin.form.CartShipping.actions")</th>
            </tr>
            </thead>
            <tbody class="text-center">
            @foreach($values as $key => $value)
            <tr>
                <td>
                    {{ array_get($currencies, "{$key}.title") }}
                </td>
                <td>
                    {{ array_get($value, 'price') }}
                    <input type="hidden" name="{{ $name }}[price][{{ $key }}][price]" value="{{ array_get($value, 'price') }}" />
                </td>
                <td>
                    {{ array_get($value, 'fee') }}
                    <input type="hidden" name="{{ $name }}[price][{{ $key }}][fee]" value="{{ array_get($value, 'fee') }}" />
                </td>
                <td>
                    {{ array_get($value, 'tax') }}%
                    <input type="hidden" name="{{ $name }}[price][{{ $key }}][tax]" value="{{ array_get($value, 'tax') }}" />
                </td>
                <td>
                    {{ array_get($value, 'minimum') }}
                    <input type="hidden" name="{{ $name }}[price][{{ $key }}][minimum]" value="{{ array_get($value, 'minimum') }}" />
                </td>
                <td class="text-center text-nowrap">
                    <button class="btn btn-danger btn-sm repeat-remove" type="button"><i class="icon-cross"></i></button>
                </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td class="text-center">
                    <select class="form-control bs-select repeat-key" data-style="btn-outline-light btn-sm">
                        <option value="">@lang("MinmaxEcommerce::admin.form.CartShipping.please_select")</option>
                        @foreach ($currencies as $key => $currency)
                        <option value="{{ $key }}" {{ array_key_exists($key, $values) ? 'disabled' : '' }}>{{ array_get($currency, 'title') }}</option>
                        @endforeach
                    </select>
                </td>
                <td><input class="form-control form-control-sm ignore-valid repeat-price" type="text" /></td>
                <td><input class="form-control form-control-sm ignore-valid repeat-fee" type="text" /></td>
                <td><input class="form-control form-control-sm ignore-valid repeat-tax" type="text" placeholder="(%)" /></td>
                <td><input class="form-control form-control-sm ignore-valid repeat-minimum" type="text" /></td>
                <td class="text-center text-nowrap">
                    <button type="button" class="btn btn-primary btn-sm repeat-add"><i class="icon-plus"></i></button>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>

<template id="template-{{ $id }}">
    <tr>
        <td>DUMP_CURRENCY_LABEL</td>
        <td>DUMP_PRICE<input type="hidden" name="{{ $name }}[price][DUMP_CURRENCY_KEY][price]" value="DUMP_PRICE" /></td>
        <td>DUMP_FEE<input type="hidden" name="{{ $name }}[price][DUMP_CURRENCY_KEY][fee]" value="DUMP_FEE" /></td>
        <td>DUMP_TAX%<input type="hidden" name="{{ $name }}[price][DUMP_CURRENCY_KEY][tax]" value="DUMP_TAX" /></td>
        <td>DUMP_MINIMUM<input type="hidden" name="{{ $name }}[price][DUMP_CURRENCY_KEY][minimum]" value="DUMP_MINIMUM" /></td>
        <td class="text-center text-nowrap">
            <button class="btn btn-danger btn-sm repeat-remove" type="button"><i class="icon-cross"></i></button>
        </td>
    </tr>
</template>

@push('scripts')
<script>
(function ($) {
    $(function () {
        function reflashCurrencyList() {
            $('#{{ $id }} table > tfoot .repeat-key option').prop('disabled', false);
            $('#{{ $id }} table > tbody input[name^="{{ $name }}[price]"][name$="[fee]"]').each(function() {
                var $this = $(this);
                var currency = $this.attr('name').replace(/^CartShipping\[price]\[/i, '').replace(/]\[fee]$/i, '');
                $('#{{ $id }} table > tfoot .repeat-key option[value="' + currency + '"]').prop('disabled', true);
            });
            $('#{{ $id }} table > tfoot .repeat-key').selectpicker('refresh');
            $('#{{ $id }} table > tfoot .repeat-key').selectpicker('val', '');
            $('#{{ $id }} table > tfoot .repeat-price').val('');
            $('#{{ $id }} table > tfoot .repeat-fee').val('');
            $('#{{ $id }} table > tfoot .repeat-tax').val('');
            $('#{{ $id }} table > tfoot .repeat-minimum').val('');
        }

        $('#{{ $id }}')
            .on('click', '.repeat-add', function () {
                var currency = $('#{{ $id }} table > tfoot .repeat-key > option:selected').val();
                var currencyLabel = $('#{{ $id }} table > tfoot .repeat-key > option:selected').text();
                var priceValue = $('#{{ $id }} table > tfoot .repeat-price').val();
                var feeValue = $('#{{ $id }} table > tfoot .repeat-fee').val();
                var taxValue = $('#{{ $id }} table > tfoot .repeat-tax').val();
                var minimumValue = $('#{{ $id }} table > tfoot .repeat-minimum').val();

                if (currency !== '' && priceValue !== '') {
                    $('#{{ $id }} table > tbody').append(
                        $('#template-{{ $id }}').html()
                            .replace(/DUMP_CURRENCY_LABEL/g, currencyLabel)
                            .replace(/DUMP_CURRENCY_KEY/g, currency)
                            .replace(/DUMP_PRICE/g, priceValue)
                            .replace(/DUMP_FEE/g, feeValue === '' ? 0 : feeValue)
                            .replace(/DUMP_TAX/g, taxValue === '' ? 0 : taxValue)
                            .replace(/DUMP_MINIMUM/g, minimumValue === '' ? 0 : minimumValue)
                    );
                    $('#{{ $id }}-price-empty').prop('disabled', $('#{{ $id }} table > tbody > tr').length > 0);
                    reflashCurrencyList();
                }
            })
            .on('click', '.repeat-remove', function () {
                $(this).parents('tr').remove();
                $('#{{ $id }}-price-empty').prop('disabled', $('#{{ $id }} table > tbody > tr').length > 0);
                reflashCurrencyList();
            });
    });
})(jQuery);
</script>
@endpush
