<?php
/**
 * Create page of model CartShipping
 *
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Ecommerce\Models\CartShipping $formData
 */
?>

@extends('MinmaxBase::admin.layouts.page.create')

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links', ['languageActive' => $languageActive])
        @if($adminData->can('cartShippingShow'))
        <a class="btn btn-sm btn-light" href="{{ langRoute("admin.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::admin.form.back_list')">
            <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.back_list')</span>
        </a>
        @endif
    @endcomponent
@endsection

@section('forms')
    @inject('modelPresenter', 'Minmax\Ecommerce\Admin\CartShippingPresenter')

    <fieldset>
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.default')</legend>

        {!! $modelPresenter->getFieldText($formData, 'title', ['required' => true]) !!}

        {!! $modelPresenter->getFieldColumnExtension($formData, 'details') !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::admin.form.fieldSet.locations')</legend>

        {!! $modelPresenter->getFieldLocations($formData, 'location_limits') !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::admin.form.fieldSet.measuring')</legend>

        {!! $modelPresenter->getFieldText($formData, 'measuring_limits', ['size' => 4, 'subColumn' => 'perimeter', 'hint' => true]) !!}

        {!! $modelPresenter->getFieldText($formData, 'measuring_limits', ['size' => 4, 'subColumn' => 'volume', 'hint' => true]) !!}

        {!! $modelPresenter->getFieldText($formData, 'measuring_limits', ['size' => 4, 'subColumn' => 'space', 'hint' => true]) !!}

        {!! $modelPresenter->getFieldText($formData, 'measuring_limits', ['size' => 4, 'subColumn' => 'weight', 'hint' => true]) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::admin.form.fieldSet.range')</legend>

        {!! $modelPresenter->getFieldMultiSelect($formData, 'deliveries') !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::admin.form.fieldSet.products')</legend>

        {!! $modelPresenter->getFieldMultiSelect($formData, 'product_brands') !!}

        {!! $modelPresenter->getFieldMultiSelect($formData, 'product_categories', ['type' => 'dropdown']) !!}

        {!! $modelPresenter->getFieldProducts($formData, 'product_sets') !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::admin.form.fieldSet.shipping_price')</legend>

        {!! $modelPresenter->getFieldPriceList($formData, 'price') !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.advanced')</legend>

        {!! $modelPresenter->getFieldDatePicker($formData, 'start_at', ['type' => 'datetime', 'hint' => true]) !!}

        {!! $modelPresenter->getFieldDatePicker($formData, 'end_at', ['type' => 'datetime', 'hint' => true]) !!}

        {!! $modelPresenter->getFieldText($formData, 'sort', ['size' => 2, 'hint' => true]) !!}

        {!! $modelPresenter->getFieldRadio($formData, 'active', ['required' => true, 'inline' => true]) !!}

    </fieldset>

    <div class="text-center my-4 form-btn-group">
        <input class="btn btn-main" type="submit" id="submitBut" value="@lang('MinmaxBase::admin.form.button.send')" />
        <input class="btn btn-default" type="reset" value="@lang('MinmaxBase::admin.form.button.reset')" onclick="window.location.reload(true)" />
    </div>
@endsection
