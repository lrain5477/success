<?php
/**
 * @var string $id
 * @var string $name
 * @var array $value
 * @var array $countries
 * @var string $hint
 */
?>
<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="{{ $id }}-country">@lang('MinmaxEcommerce::models.CartShipping.location_limits.country')</label>
    <div class="col-sm-10">
        <select class="bs-select form-control" id="{{ $id }}-country" name="{{ $name }}[country]" data-live-search="true">
            <option value="">@lang('MinmaxBase::admin.form.select_nothing_title')</option>
            @foreach($countries as $countryKey => $countryItem)
            <option value="{{ $countryKey }}">{{ array_get($countryItem, 'title') }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="{{ $id }}-state">@lang('MinmaxEcommerce::models.CartShipping.location_limits.state')</label>
    <div class="col-sm-10">
        <select class="bs-select form-control" id="{{ $id }}-state" name="{{ $name }}[state]" data-live-search="true">
            <option value="">@lang('MinmaxBase::admin.form.select_nothing_title')</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="{{ $id }}-county">@lang('MinmaxEcommerce::models.CartShipping.location_limits.county')</label>
    <div class="col-sm-10">
        <select class="bs-select form-control" id="{{ $id }}-county" name="{{ $name }}[county]" data-live-search="true">
            <option value="">@lang('MinmaxBase::admin.form.select_nothing_title')</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="{{ $id }}-city">@lang('MinmaxEcommerce::models.CartShipping.location_limits.city')</label>
    <div class="col-sm-10">
        <select class="bs-select form-control" id="{{ $id }}-city" name="{{ $name }}[city]" data-live-search="true">
            <option value="">@lang('MinmaxBase::admin.form.select_nothing_title')</option>
        </select>
    </div>
    @if($hint !== '')
    <small class="form-text text-muted ml-sm-auto col-sm-10">{!! $hint !!}</small>
    @endif
</div>

@push('scripts')
<script>
$(function() {
    var defaults = {
        "country": '{{ array_get(old(str_replace(['[', ']'], ['.', ''], $name), $value), 'country') }}',
        "state": '{{ array_get(old(str_replace(['[', ']'], ['.', ''], $name), $value), 'state') }}',
        "county": '{{ array_get(old(str_replace(['[', ']'], ['.', ''], $name), $value), 'county') }}',
        "city": '{{ array_get(old(str_replace(['[', ']'], ['.', ''], $name), $value), 'city') }}',
    };

    var $country = $('#{{ $id }}-country');
    $country.on('change', function () {
        var $countrySelect = $(this);
        var $stateSelect = $('#{{ $id }}-state');
        var $emptyOption = $('option:first-child', $stateSelect);
        $stateSelect.empty().append($emptyOption);
        if ($countrySelect.val() === '') {
            $stateSelect.selectpicker('refresh');
            $stateSelect.change();
        } else {
            var url = ('{{ langRoute('admin.ajax.world-states', 'id') }}').replace(/\/id\//, '/' + $countrySelect.val() + '/');
            $.get(url, function (response) {
                for (var item of response) {
                    if (item.hasOwnProperty('id') && item.hasOwnProperty('name')) {
                        $stateSelect.append('<option value="' + item.id + '">' + item.name + '</option>');
                    }
                }
                $stateSelect.selectpicker('refresh');
                if (defaults.state !== '') {
                    $stateSelect.selectpicker('val', defaults.state).change();
                } else {
                    $stateSelect.change();
                }
            });
        }
    });
    $('#{{ $id }}-state').on('change', function () {
        var $stateSelect = $(this);
        var $countySelect = $('#{{ $id }}-county');
        var $emptyOption = $('option:first-child', $countySelect);
        $countySelect.empty().append($emptyOption);
        if ($stateSelect.val() === '') {
            $countySelect.selectpicker('refresh');
            $countySelect.change();
        } else {
            var url = ('{{ langRoute('admin.ajax.world-counties', 'id') }}').replace(/\/id\//, '/' + $stateSelect.val() + '/');
            $.get(url, function (response) {
                for (var item of response) {
                    if (item.hasOwnProperty('id') && item.hasOwnProperty('name')) {
                        $countySelect.append('<option value="' + item.id + '">' + item.name + '</option>');
                    }
                }
                $countySelect.selectpicker('refresh');
                if (defaults.county !== '') {
                    $countySelect.selectpicker('val', defaults.county).change();
                } else {
                    $countySelect.change();
                }
            });
        }
    });
    $('#{{ $id }}-county').on('change', function () {
        var $countySelect = $(this);
        var $citySelect = $('#{{ $id }}-city');
        var $emptyOption = $('option:first-child', $citySelect);
        $citySelect.empty().append($emptyOption);
        if ($countySelect.val() === '') {
            $citySelect.selectpicker('refresh');
            $citySelect.change();
        } else {
            var url = ('{{ langRoute('admin.ajax.world-cities', 'id') }}').replace(/\/id\//, '/' + $countySelect.val() + '/');
            $.get(url, function (response) {
                for (var item of response) {
                    if (item.hasOwnProperty('id') && item.hasOwnProperty('zip') && item.hasOwnProperty('name')) {
                        $citySelect.append('<option value="' + item.id + '" data-zip="' + item.zip + '">' + item.name + '</option>');
                    }
                }
                $citySelect.selectpicker('refresh');
                if (defaults.city !== '') {
                    $citySelect.selectpicker('val', defaults.city);
                }
            });
        }
    });

    if (defaults.country !== '') {
        $country.selectpicker('val', defaults.country).change();
    }
});
</script>
@endpush
