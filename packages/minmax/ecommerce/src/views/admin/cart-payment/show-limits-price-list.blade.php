<?php
/**
 * @var string $id
 * @var string $label
 * @var string $name
 * @var array $currencies
 * @var array $values
 */
?>
<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="{{ $id }}">{{ $label }}</label>
    <div class="col-sm-10" id="{{ $id }}">
        <table class="table table-sm table-bordered mb-1">
            <thead class="thead-default">
            <tr class="text-center">
                <th>@lang("MinmaxEcommerce::admin.form.CartPayment.currency")</th>
                <th>@lang("MinmaxEcommerce::admin.form.CartPayment.price_min")</th>
                <th>@lang("MinmaxEcommerce::admin.form.CartPayment.price_max")</th>
            </tr>
            </thead>
            <tbody class="text-center">
            @foreach($values as $key => $value)
            <tr>
                <td>
                    {{ array_get($currencies, "{$key}.title") }}
                </td>
                <td>
                    {{ array_get($value, 'price_min') }}
                </td>
                <td>
                    {{ array_get($value, 'price_max') }}
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
