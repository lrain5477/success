<?php
/**
 * Edit page of model CartConfig
 *
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Ecommerce\Models\CartConfig $formData
 */
?>

@extends('MinmaxBase::admin.layouts.page.edit', ['formDataId' => $formData->id])

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links', ['languageActive' => $languageActive])
    @endcomponent
@endsection

@section('forms')
    @inject('modelPresenter', 'Minmax\Ecommerce\Admin\CartConfigPresenter')

    <fieldset>
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.default')</legend>

        {!! $modelPresenter->getFieldText($formData, 'browse_history', ['required' => true, 'size' => 3, 'suffix' => '天']) !!}

        {!! $modelPresenter->getFieldText($formData, 'track_list', ['required' => true, 'size' => 3, 'suffix' => '天']) !!}

        {!! $modelPresenter->getFieldText($formData, 'shipping_notify', ['required' => true, 'size' => 3, 'suffix' => '天']) !!}

        {!! $modelPresenter->getFieldText($formData, 'return_limit', ['required' => true, 'size' => 3, 'suffix' => '天', 'hint' => true]) !!}

        {!! $modelPresenter->getFieldText($formData, 'rank_limit', ['required' => true, 'size' => 3, 'suffix' => '天']) !!}

        {!! $modelPresenter->getFieldText($formData, 'less_quantity', ['required' => true, 'size' => 3, 'suffix' => '個', 'hint' => true]) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::admin.form.fieldSet.auto_patch')</legend>

        {!! $modelPresenter->getFieldMultiSelect($formData, 'skip_pick_relation', ['hint' => true]) !!}

        {!! $modelPresenter->getFieldRadio($formData, 'skip_pick_auto', ['required' => true, 'inline' => true, 'hint' => true]) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::admin.form.fieldSet.wrap_setting')</legend>

        {!! $modelPresenter->getFieldWrapPriceList($formData) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::admin.form.fieldSet.bonus_setting')</legend>

        {!! $modelPresenter->getFieldBonusSettingList($formData) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::admin.form.fieldSet.workflow')</legend>

        {!! $modelPresenter->getFieldColumnExtension($formData, 'information') !!}

    </fieldset>

    <div class="text-center my-4 form-btn-group">
        <input class="btn btn-main" type="submit" id="submitBut" value="@lang('MinmaxBase::admin.form.button.send')" />
        <input class="btn btn-default" type="reset" value="@lang('MinmaxBase::admin.form.button.reset')" onclick="window.location.reload(true)" />
    </div>
@endsection
