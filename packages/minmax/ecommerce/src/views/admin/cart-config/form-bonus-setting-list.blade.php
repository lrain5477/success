<?php
/**
 * @var string $id
 * @var string $label
 * @var string $name
 * @var array $currencies
 * @var array $values
 */
?>
<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="{{ $id }}">{{ $label }}</label>
    <div class="col-sm-10" id="{{ $id }}">
        <input type="hidden" id="{{ $id }}-exchange-empty" name="{{ $name }}[bonus_exchange]" value="" {{ count($values) > 0 ? 'disabled' : '' }} />
        <input type="hidden" id="{{ $id }}-percent-empty" name="{{ $name }}[bonus_percent]" value="" {{ count($values) > 0 ? 'disabled' : '' }} />
        <table class="table table-sm table-bordered mb-1">
            <thead class="thead-default">
            <tr class="text-center">
                <th>@lang("MinmaxEcommerce::admin.form.CartConfig.currency")</th>
                <th>@lang("MinmaxEcommerce::admin.form.CartConfig.bonus_exchange")</th>
                <th>@lang("MinmaxEcommerce::admin.form.CartConfig.bonus_percent")</th>
                <th>@lang("MinmaxEcommerce::admin.form.CartConfig.actions")</th>
            </tr>
            </thead>
            <tbody class="text-center">
            @foreach($values as $key => $value)
            <tr>
                <td>
                    {{ array_get($currencies, "{$key}.title") }}
                </td>
                <td>
                    {{ array_get($value, 'exchange') }}
                    <input type="hidden" name="{{ $name }}[bonus_exchange][{{ $key }}]" value="{{ array_get($value, 'exchange') }}" />
                </td>
                <td>
                    {{ array_get($value, 'percent') }}
                    <input type="hidden" name="{{ $name }}[bonus_percent][{{ $key }}]" value="{{ array_get($value, 'percent') }}" />
                </td>
                <td class="text-center text-nowrap">
                    <button class="btn btn-danger btn-sm repeat-remove" type="button"><i class="icon-cross"></i></button>
                </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td class="text-center">
                    <select class="form-control bs-select repeat-key" data-style="btn-outline-light btn-sm">
                        <option value="">@lang("MinmaxEcommerce::admin.form.CartConfig.please_select")</option>
                        @foreach ($currencies as $key => $currency)
                        <option value="{{ $key }}" {{ array_key_exists($key, $values) ? 'disabled' : '' }}>{{ array_get($currency, 'title') }}</option>
                        @endforeach
                    </select>
                </td>
                <td><input class="form-control form-control-sm ignore-valid repeat-exchange" type="text" /></td>
                <td><input class="form-control form-control-sm ignore-valid repeat-percent" type="text" /></td>
                <td class="text-center text-nowrap">
                    <button type="button" class="btn btn-primary btn-sm repeat-add"><i class="icon-plus"></i></button>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>

<template id="template-{{ $id }}">
    <tr>
        <td>DUMP_CURRENCY_LABEL</td>
        <td>DUMP_EXCHANGE<input type="hidden" name="{{ $name }}[bonus_exchange][DUMP_CURRENCY_KEY]" value="DUMP_EXCHANGE" /></td>
        <td>DUMP_PERCENT<input type="hidden" name="{{ $name }}[bonus_percent][DUMP_CURRENCY_KEY]" value="DUMP_PERCENT" /></td>
        <td class="text-center text-nowrap">
            <button class="btn btn-danger btn-sm repeat-remove" type="button"><i class="icon-cross"></i></button>
        </td>
    </tr>
</template>

@push('scripts')
<script>
(function ($) {
    $(function () {
        function reflashCurrencyList() {
            $('#{{ $id }} table > tfoot .repeat-key option').prop('disabled', false);
            $('#{{ $id }} table > tbody input[name^="{{ $name }}[bonus_exchange]"]').each(function() {
                var $this = $(this);
                var currency = $this.attr('name').replace(/^CartConfig\[bonus_exchange]\[/i, '').replace(/]$/i, '');
                $('#{{ $id }} table > tfoot .repeat-key option[value="' + currency + '"]').prop('disabled', true);
            });
            $('#{{ $id }} table > tfoot .repeat-key').selectpicker('refresh');
            $('#{{ $id }} table > tfoot .repeat-key').selectpicker('val', '');
            $('#{{ $id }} table > tfoot .repeat-exchange').val('');
            $('#{{ $id }} table > tfoot .repeat-percent').val('');
        }

        $('#{{ $id }}')
            .on('click', '.repeat-add', function () {
                var currency = $('#{{ $id }} table > tfoot .repeat-key > option:selected').val();
                var currencyLabel = $('#{{ $id }} table > tfoot .repeat-key > option:selected').text();
                var exchangeValue = $('#{{ $id }} table > tfoot .repeat-exchange').val();
                var percentValue = $('#{{ $id }} table > tfoot .repeat-percent').val();

                if (currency !== '' && exchangeValue !== '' && percentValue !== '') {
                    $('#{{ $id }} table > tbody').append(
                        $('#template-{{ $id }}').html()
                            .replace(/DUMP_CURRENCY_LABEL/ig, currencyLabel)
                            .replace(/DUMP_CURRENCY_KEY/ig, currency)
                            .replace(/DUMP_EXCHANGE/ig, exchangeValue)
                            .replace(/DUMP_PERCENT/ig, percentValue)
                    );
                    $('#{{ $id }}-exchange-empty').prop('disabled', $('#{{ $id }} table > tbody > tr').length > 0);
                    $('#{{ $id }}-percent-empty').prop('disabled', $('#{{ $id }} table > tbody > tr').length > 0);
                    reflashCurrencyList();
                }
            })
            .on('click', '.repeat-remove', function () {
                $(this).parents('tr').remove();
                $('#{{ $id }}-exchange-empty').prop('disabled', $('#{{ $id }} table > tbody > tr').length > 0);
                $('#{{ $id }}-percent-empty').prop('disabled', $('#{{ $id }} table > tbody > tr').length > 0);
                reflashCurrencyList();
            });
    });
})(jQuery);
</script>
@endpush
