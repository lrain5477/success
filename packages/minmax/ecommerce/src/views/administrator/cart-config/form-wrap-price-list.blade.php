<?php
/**
 * @var string $id
 * @var string $label
 * @var string $name
 * @var array $currencies
 * @var array $values
 */
?>
<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="{{ $id }}">{{ $label }}</label>
    <div class="col-sm-10" id="{{ $id }}">
        <input type="hidden" id="{{ $id }}-empty" name="{{ $name }}[wrap_price]" value="" {{ count($values) > 0 ? 'disabled' : '' }} />
        <table class="table table-sm table-bordered mb-1">
            <thead class="thead-default">
            <tr class="text-center">
                <th>@lang("MinmaxEcommerce::administrator.form.CartConfig.currency")</th>
                <th>@lang("MinmaxEcommerce::administrator.form.CartConfig.wrap_price")</th>
                <th>@lang("MinmaxEcommerce::administrator.form.CartConfig.actions")</th>
            </tr>
            </thead>
            <tbody class="text-center">
            @foreach($values as $key => $value)
            <tr>
                <td>
                    {{ array_get($currencies, "{$key}.title") }}
                </td>
                <td>
                    {{ $value }}
                    <input type="hidden" name="{{ $name }}[wrap_price][{{ $key }}]" value="{{ $value }}" />
                </td>
                <td class="text-center text-nowrap">
                    <button class="btn btn-danger btn-sm repeat-remove" type="button"><i class="icon-cross"></i></button>
                </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td class="text-center">
                    <select class="form-control bs-select repeat-key" data-style="btn-outline-light btn-sm">
                        <option value="">@lang("MinmaxEcommerce::administrator.form.CartConfig.please_select")</option>
                        @foreach ($currencies as $key => $currency)
                        <option value="{{ $key }}" {{ array_key_exists($key, $values) ? 'disabled' : '' }}>{{ array_get($currency, 'title') }}</option>
                        @endforeach
                    </select>
                </td>
                <td><input class="form-control form-control-sm ignore-valid repeat-price" type="text" /></td>
                <td class="text-center text-nowrap">
                    <button type="button" class="btn btn-primary btn-sm repeat-add"><i class="icon-plus"></i></button>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>

<template id="template-{{ $id }}">
    <tr>
        <td>DUMP_CURRENCY_LABEL</td>
        <td>DUMP_PRICE<input type="hidden" name="{{ $name }}[wrap_price][DUMP_CURRENCY_KEY]" value="DUMP_PRICE" /></td>
        <td class="text-center text-nowrap">
            <button class="btn btn-danger btn-sm repeat-remove" type="button"><i class="icon-cross"></i></button>
        </td>
    </tr>
</template>

@push('scripts')
<script>
(function ($) {
    $(function () {
        function reflashCurrencyList() {
            $('#{{ $id }} table > tfoot .repeat-key option').prop('disabled', false);
            $('#{{ $id }} table > tbody input[name^="{{ $name }}[wrap_price]"]').each(function() {
                var $this = $(this);
                var currency = $this.attr('name').replace(/^CartConfig\[wrap_price]\[/i, '').replace(/]$/i, '');
                $('#{{ $id }} table > tfoot .repeat-key option[value="' + currency + '"]').prop('disabled', true);
            });
            $('#{{ $id }} table > tfoot .repeat-key').selectpicker('refresh');
            $('#{{ $id }} table > tfoot .repeat-key').selectpicker('val', '');
            $('#{{ $id }} table > tfoot .repeat-price').val('');
        }

        $('#{{ $id }}')
            .on('click', '.repeat-add', function () {
                var currency = $('#{{ $id }} table > tfoot .repeat-key > option:selected').val();
                var currencyLabel = $('#{{ $id }} table > tfoot .repeat-key > option:selected').text();
                var price = $('#{{ $id }} table > tfoot .repeat-price').val();

                if (currency !== '' && price !== '') {
                    $('#{{ $id }} table > tbody').append(
                        $('#template-{{ $id }}').html()
                            .replace(/DUMP_CURRENCY_LABEL/ig, currencyLabel)
                            .replace(/DUMP_CURRENCY_KEY/ig, currency)
                            .replace(/DUMP_PRICE/ig, price)
                    );
                    $('#{{ $id }}-empty').prop('disabled', $('#{{ $id }} table > tbody > tr').length > 0);
                    reflashCurrencyList();
                }
            })
            .on('click', '.repeat-remove', function () {
                $(this).parents('tr').remove();
                $('#{{ $id }}-empty').prop('disabled', $('#{{ $id }} table > tbody > tr').length > 0);
                reflashCurrencyList();
            });
    });
})(jQuery);
</script>
@endpush
