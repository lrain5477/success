<?php
/**
 * Show page of model CartConfig
 *
 * @var \Minmax\Base\Models\Administrator $adminData
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 * @var \Minmax\Ecommerce\Models\CartConfig $formData
 */
?>

@extends('MinmaxBase::administrator.layouts.page.show')

@section('action-buttons')
    @component('MinmaxBase::administrator.layouts.right-links', ['languageActive' => $languageActive])
    <a class="btn btn-sm btn-light" href="{{ langRoute("administrator.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::administrator.form.back_list')">
        <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.back_list')</span>
    </a>
    <a class="btn btn-sm btn-main" href="{{ langRoute("administrator.{$pageData->uri}.edit", [$formData->id]) }}" title="@lang('MinmaxBase::administrator.form.edit')">
        <i class="icon-pencil"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.edit')</span>
    </a>
    @endcomponent
@endsection

@section('views')
    @inject('modelPresenter', 'Minmax\Ecommerce\Administrator\CartConfigPresenter')

    <fieldset>
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::administrator.form.fieldSet.default')</legend>

        {!! $modelPresenter->getShowNormalText($formData, 'id') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'browse_history', ['suffix' => ' 天']) !!}

        {!! $modelPresenter->getShowNormalText($formData, 'track_list', ['suffix' => ' 天']) !!}

        {!! $modelPresenter->getShowNormalText($formData, 'shipping_notify', ['suffix' => ' 天']) !!}

        {!! $modelPresenter->getShowNormalText($formData, 'return_limit', ['suffix' => ' 天']) !!}

        {!! $modelPresenter->getShowNormalText($formData, 'rank_limit', ['suffix' => ' 天']) !!}

        {!! $modelPresenter->getShowNormalText($formData, 'less_quantity', ['suffix' => ' 個']) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::administrator.form.fieldSet.auto_patch')</legend>

        {!! $modelPresenter->getShowMultiSelection($formData, 'skip_pick_relation') !!}

        {!! $modelPresenter->getShowSelection($formData, 'skip_pick_auto') !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::administrator.form.fieldSet.wrap_setting')</legend>

        {!! $modelPresenter->getShowWrapPriceList($formData) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::administrator.form.fieldSet.bonus_setting')</legend>

        {!! $modelPresenter->getShowBonusSettingList($formData) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxEcommerce::administrator.form.fieldSet.workflow')</legend>

        {!! $modelPresenter->getShowColumnExtension($formData, 'information') !!}

    </fieldset>

    <fieldset class="mt-4" id="sysFieldSet">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::administrator.form.fieldSet.system_record')</legend>

        {!! $modelPresenter->getShowNormalText($formData, 'created_at') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'updated_at') !!}

    </fieldset>

@endsection
