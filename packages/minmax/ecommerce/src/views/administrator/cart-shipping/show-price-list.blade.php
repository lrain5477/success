<?php
/**
 * @var string $id
 * @var string $label
 * @var array $currencies
 * @var array $values
 */
?>
<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="{{ $id }}">{{ $label }}</label>
    <div class="col-sm-10" id="{{ $id }}">
        <table class="table table-sm table-bordered mb-1">
            <thead class="thead-default">
            <tr class="text-center">
                <th>@lang("MinmaxEcommerce::administrator.form.CartShipping.currency")</th>
                <th>@lang("MinmaxEcommerce::administrator.form.CartShipping.price")</th>
                <th>@lang("MinmaxEcommerce::administrator.form.CartShipping.fee")</th>
                <th>@lang("MinmaxEcommerce::administrator.form.CartShipping.tax")</th>
                <th>@lang("MinmaxEcommerce::administrator.form.CartShipping.minimum")</th>
            </tr>
            </thead>
            <tbody class="text-center">
            @foreach($values as $key => $value)
            <tr>
                <td>
                    {{ array_get($currencies, "{$key}.title") }}
                </td>
                <td>
                    {{ array_get($value, 'price') }}
                </td>
                <td>
                    {{ array_get($value, 'fee') }}
                </td>
                <td>
                    {{ array_get($value, 'tax') }}%
                </td>
                <td>
                    {{ array_get($value, 'minimum') }}
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
