<?php
/**
 * @var string $label
 * @var array $values
 * @var array $products
 */
?>
<div class="form-group row">
    <label class="col-sm-2 col-form-label">{{ $label }}</label>
    <div class="col-sm-10">
        <table class="table table-sm table-bordered mb-1">
            <thead class="thead-default">
            <tr class="text-center">
                <th class="w-25">@lang("MinmaxEcommerce::administrator.form.CartShipping.product_sku")</th>
                <th>@lang("MinmaxEcommerce::administrator.form.CartShipping.product_title")</th>
            </tr>
            </thead>
            <tbody>
            @foreach($values as $value)
            <tr>
                <td>
                    {{ $value }}
                </td>
                <td>
                    {{ array_get($products, $value) }}
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
