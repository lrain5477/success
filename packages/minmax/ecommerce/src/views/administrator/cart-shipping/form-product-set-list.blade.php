<?php
/**
 * @var string $id
 * @var string $label
 * @var string $name
 * @var array $values
 * @var array $products
 */
?>
<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="{{ $id }}">{{ $label }}</label>
    <div class="col-sm-10" id="{{ $id }}">
        <input type="hidden" id="{{ $id }}-empty" name="{{ $name }}" value="" {{ count($values) > 0 ? 'disabled' : '' }} />
        <table class="table table-sm table-bordered mb-1">
            <thead class="thead-default">
            <tr class="text-center">
                <th class="w-20">@lang("MinmaxEcommerce::administrator.form.CartShipping.product_sku")</th>
                <th>@lang("MinmaxEcommerce::administrator.form.CartShipping.product_title")</th>
                <th>@lang("MinmaxEcommerce::administrator.form.CartShipping.actions")</th>
            </tr>
            </thead>
            <tbody>
            @foreach($values as $value)
            <tr>
                <td>
                    {{ $value }}
                    <input type="hidden" name="{{ $name }}[]" value="{{ $value }}" />
                </td>
                <td>
                    {{ array_get($products, $value) }}
                </td>
                <td class="text-center text-nowrap">
                    <button class="btn btn-danger btn-sm repeat-remove" type="button"><i class="icon-cross"></i></button>
                </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2"><input class="form-control form-control-sm ignore-valid repeat-sku" type="text" placeholder="@lang('MinmaxEcommerce::administrator.form.CartShipping.please_product_sku')" /></td>
                <td class="text-center text-nowrap">
                    <button type="button" class="btn btn-primary btn-sm repeat-add"><i class="icon-plus"></i></button>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <small class="form-text text-muted ml-sm-auto col-sm-10">@lang('MinmaxEcommerce::models.CartShipping.hint.product_sets', ['link' => langRoute('administrator.product-set.index')])</small>
</div>

<template id="template-{{ $id }}">
    <tr>
        <td>DUMP_SKU<input type="hidden" name="{{ $name }}[]" value="DUMP_SKU" /></td>
        <td>DUMP_TITLE</td>
        <td class="text-center text-nowrap">
            <button class="btn btn-danger btn-sm repeat-remove" type="button"><i class="icon-cross"></i></button>
        </td>
    </tr>
</template>

@push('scripts')
<script>
(function ($) {
    $(function () {
        $('#{{ $id }}')
            .on('click', '.repeat-add', function () {
                var skuValue = $('#{{ $id }} table > tfoot .repeat-sku').val();

                if (skuValue !== '') {
                    if ($('#{{ $id }} table > tbody > tr input[value=' + skuValue + ']').length < 1) {
                        $.get('{{ langRoute('administrator.ajax.product-set.exists', ['type' => 'sku', 'sku' => 'input']) }}'.replace('input', skuValue), function (response) {
                            if (response.hasOwnProperty('sku') && response.hasOwnProperty('title')) {
                                $('#{{ $id }} table > tbody').append(
                                    $('#template-{{ $id }}').html()
                                        .replace(/DUMP_SKU/g, response.sku)
                                        .replace(/DUMP_TITLE/g, response.title)
                                );
                                $('#{{ $id }}-empty').prop('disabled', $('#{{ $id }} table > tbody > tr').length > 0);
                                $('#{{ $id }} table > tfoot .repeat-sku').val('');
                            }
                        }).fail(function () {
                            swal("@lang('MinmaxEcommerce::administrator.form.CartShipping.alert.product_not_exist.title')", "@lang('MinmaxEcommerce::administrator.form.CartShipping.alert.product_not_exist.info')", 'error');
                            $('#{{ $id }} table > tfoot .repeat-sku').val('');
                        });
                    } else {
                        swal("@lang('MinmaxEcommerce::administrator.form.CartShipping.alert.product_duplicate.title')", "@lang('MinmaxEcommerce::administrator.form.CartShipping.alert.product_duplicate.info')", 'error');
                        $('#{{ $id }} table > tfoot .repeat-sku').val('');
                    }
                }
            })
            .on('click', '.repeat-remove', function () {
                $(this).parents('tr').remove();
                $('#{{ $id }}-empty').prop('disabled', $('#{{ $id }} table > tbody > tr').length > 0);
            });
    });
})(jQuery);
</script>
@endpush
