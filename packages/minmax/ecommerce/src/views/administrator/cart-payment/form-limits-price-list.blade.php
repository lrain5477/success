<?php
/**
 * @var string $id
 * @var string $label
 * @var string $name
 * @var array $currencies
 * @var array $values
 */
?>
<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="{{ $id }}">{{ $label }}</label>
    <div class="col-sm-10" id="{{ $id }}">
        <input type="hidden" id="{{ $id }}-limits-empty" name="{{ $name }}[limits]" value="" {{ count($values) > 0 ? 'disabled' : '' }} />
        <table class="table table-sm table-bordered mb-1">
            <thead class="thead-default">
            <tr class="text-center">
                <th>@lang("MinmaxEcommerce::administrator.form.CartPayment.currency")</th>
                <th>@lang("MinmaxEcommerce::administrator.form.CartPayment.price_min")</th>
                <th>@lang("MinmaxEcommerce::administrator.form.CartPayment.price_max")</th>
                <th>@lang("MinmaxEcommerce::administrator.form.CartPayment.actions")</th>
            </tr>
            </thead>
            <tbody class="text-center">
            @foreach($values as $key => $value)
            <tr>
                <td>
                    {{ array_get($currencies, "{$key}.title") }}
                </td>
                <td>
                    {{ array_get($value, 'price_min') }}
                    <input type="hidden" name="{{ $name }}[limits][{{ $key }}][price_min]" value="{{ array_get($value, 'price_min') }}" />
                </td>
                <td>
                    {{ array_get($value, 'price_max') }}
                    <input type="hidden" name="{{ $name }}[limits][{{ $key }}][price_max]" value="{{ array_get($value, 'price_max') }}" />
                </td>
                <td class="text-center text-nowrap">
                    <button class="btn btn-danger btn-sm repeat-remove" type="button"><i class="icon-cross"></i></button>
                </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td class="text-center">
                    <select class="form-control bs-select repeat-key" data-style="btn-outline-light btn-sm">
                        <option value="">@lang("MinmaxEcommerce::administrator.form.CartPayment.please_select")</option>
                        @foreach ($currencies as $key => $currency)
                        <option value="{{ $key }}" {{ array_key_exists($key, $values) ? 'disabled' : '' }}>{{ array_get($currency, 'title') }}</option>
                        @endforeach
                    </select>
                </td>
                <td><input class="form-control form-control-sm ignore-valid repeat-min" type="text" /></td>
                <td><input class="form-control form-control-sm ignore-valid repeat-max" type="text" /></td>
                <td class="text-center text-nowrap">
                    <button type="button" class="btn btn-primary btn-sm repeat-add"><i class="icon-plus"></i></button>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <small class="form-text text-muted ml-sm-auto col-sm-10">@lang('MinmaxEcommerce::models.CartPayment.hint.limits')</small>
</div>

<template id="template-{{ $id }}">
    <tr>
        <td>DUMP_CURRENCY_LABEL</td>
        <td>DUMP_MIN<input type="hidden" name="{{ $name }}[limits][DUMP_CURRENCY_KEY][price_min]" value="DUMP_MIN" /></td>
        <td>DUMP_MAX<input type="hidden" name="{{ $name }}[limits][DUMP_CURRENCY_KEY][price_max]" value="DUMP_MAX" /></td>
        <td class="text-center text-nowrap">
            <button class="btn btn-danger btn-sm repeat-remove" type="button"><i class="icon-cross"></i></button>
        </td>
    </tr>
</template>

@push('scripts')
<script>
(function ($) {
    $(function () {
        function reflashCurrencyList() {
            $('#{{ $id }} table > tfoot .repeat-key option').prop('disabled', false);
            $('#{{ $id }} table > tbody input[name^="{{ $name }}[limits]"][name$="[price_min]').each(function() {
                var $this = $(this);
                var currency = $this.attr('name').replace(/^CartPayment\[limits]\[/i, '').replace(/]\[price_min]$/i, '');
                $('#{{ $id }} table > tfoot .repeat-key option[value="' + currency + '"]').prop('disabled', true);
            });
            $('#{{ $id }} table > tfoot .repeat-key').selectpicker('refresh');
            $('#{{ $id }} table > tfoot .repeat-key').selectpicker('val', '');
            $('#{{ $id }} table > tfoot .repeat-min').val('');
            $('#{{ $id }} table > tfoot .repeat-max').val('');
        }

        $('#{{ $id }}')
            .on('click', '.repeat-add', function () {
                var currency = $('#{{ $id }} table > tfoot .repeat-key > option:selected').val();
                var currencyLabel = $('#{{ $id }} table > tfoot .repeat-key > option:selected').text();
                var minValue = $('#{{ $id }} table > tfoot .repeat-min').val();
                var maxValue = $('#{{ $id }} table > tfoot .repeat-max').val();

                if (currency !== '' && minValue !== '' && maxValue !== '') {
                    $('#{{ $id }} table > tbody').append(
                        $('#template-{{ $id }}').html()
                            .replace(/DUMP_CURRENCY_LABEL/g, currencyLabel)
                            .replace(/DUMP_CURRENCY_KEY/g, currency)
                            .replace(/DUMP_MIN/g, minValue)
                            .replace(/DUMP_MAX/g, maxValue)
                    );
                    $('#{{ $id }}-limits-empty').prop('disabled', $('#{{ $id }} table > tbody > tr').length > 0);
                    reflashCurrencyList();
                }
            })
            .on('click', '.repeat-remove', function () {
                $(this).parents('tr').remove();
                $('#{{ $id }}-limits-empty').prop('disabled', $('#{{ $id }} table > tbody > tr').length > 0);
                reflashCurrencyList();
            });
    });
})(jQuery);
</script>
@endpush
