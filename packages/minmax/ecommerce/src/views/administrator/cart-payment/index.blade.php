<?php
/**
 * List of model CartPayment
 *
 * @var \Minmax\Base\Models\Administrator $adminData
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 * @var \Minmax\Ecommerce\Models\CartPayment $formData
 */
?>

@extends('MinmaxBase::administrator.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::administrator.layouts.right-links')
    <a class="btn btn-sm btn-main" href="{{ langRoute("administrator.{$pageData->uri}.create") }}" title="@lang('MinmaxBase::administrator.form.create')">
        <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.create')</span>
    </a>
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Ecommerce\Administrator\CartPaymentPresenter')

@section('grid-filter')
    @component('MinmaxBase::administrator.layouts.grid.filter-keyword')
    <option value="title">@lang('MinmaxEcommerce::models.CartPayment.title')</option>
    <option value="code">@lang('MinmaxEcommerce::models.CartPayment.code')</option>
    @endcomponent

    @component('MinmaxBase::administrator.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('active', 'searchActive', ['emptyLabel' => __('MinmaxEcommerce::models.CartPayment.active')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="w-10">@lang('MinmaxEcommerce::models.CartPayment.code')</th>
        <th class="w-50">@lang('MinmaxEcommerce::models.CartPayment.title')</th>
        <th>@lang('MinmaxEcommerce::models.CartPayment.sort')</th>
        <th>@lang('MinmaxEcommerce::models.CartPayment.active')</th>
        <th class="nosort">@lang('MinmaxBase::administrator.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'code', name: 'code'},
            {data: 'title', name: 'title'},
            {data: 'sort', name: 'sort'},
            {data: 'active', name: 'active'},
            {data: 'action', name: 'action'}
        ],
        ['title', 'code'],
        {"active":"searchActive"},
        [[2, 'asc']],
        '{{ langRoute("administrator.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("administrator.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
