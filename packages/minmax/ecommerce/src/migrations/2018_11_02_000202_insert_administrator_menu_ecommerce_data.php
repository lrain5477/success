<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertAdministratorMenuEcommerceData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 建立預設資料
        $this->insertDatabase();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 刪除預設資料
        $this->deleteDatabase();
    }

    /**
     * Insert default data
     *
     * @return void
     */
    public function insertDatabase()
    {
        $timestamp = date('Y-m-d H:i:s');

        // 管理員選單 - 分類
        if ($menuClassId = DB::table('administrator_menu')->where('uri', 'root-module')->value('id')) {
            $administratorMenuData = [
                [
                    'id' => $menuParentId1 = uuidl(),
                    'title' => '購物車',
                    'uri' => 'control-cart',
                    'controller' => null,
                    'model' => null,
                    'parent_id' => $menuClassId,
                    'link' => null,
                    'icon' => 'icon-cart',
                    'sort' => 202, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '付款方式',
                    'uri' => 'cart-payment',
                    'controller' => 'CartPaymentController',
                    'model' => 'CartPayment',
                    'parent_id' => $menuParentId1,
                    'link' => 'cart-payment',
                    'icon' => null,
                    'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '取貨方式',
                    'uri' => 'cart-delivery',
                    'controller' => 'CartDeliveryController',
                    'model' => 'CartDelivery',
                    'parent_id' => $menuParentId1,
                    'link' => 'cart-delivery',
                    'icon' => null,
                    'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '運費設定',
                    'uri' => 'cart-shipping',
                    'controller' => 'CartShippingController',
                    'model' => 'CartShipping',
                    'parent_id' => $menuParentId1,
                    'link' => 'cart-shipping',
                    'icon' => null,
                    'sort' => 3, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '購物車設定',
                    'uri' => 'cart-config',
                    'controller' => 'CartConfigController',
                    'model' => 'CartConfig',
                    'parent_id' => $menuParentId1,
                    'link' => 'cart-config',
                    'icon' => null,
                    'sort' => 4, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
            ];
            DB::table('administrator_menu')->insert($administratorMenuData);
        }
    }

    public function deleteDatabase()
    {
        $uriSet = ['control-cart', 'cart-payment', 'cart-delivery', 'cart-config'];

        DB::table('administrator_menu')->whereIn('uri', $uriSet)->delete();
    }
}
