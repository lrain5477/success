<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class CreateEcommerceTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 購物車設定
        Schema::create('cart_config', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->unsignedInteger('browse_history')->default(180)->comment('瀏覽紀錄保存(天)');
            $table->unsignedInteger('track_list')->default(180)->comment('追蹤清單保存(天)');
            $table->unsignedInteger('shipping_notify')->default(180)->comment('貨到通知保存(天)');
            $table->unsignedInteger('return_limit')->default(7)->comment('退貨有效期限(天)');
            $table->unsignedInteger('rank_limit')->default(30)->comment('評價期限(天)');
            $table->unsignedInteger('less_quantity')->default(1)->comment('最低庫存警示(個)');
            $table->json('skip_pick_relation')->nullable()->comment('自動出貨設定');
            $table->boolean('skip_pick_auto')->default(false)->comment('自動出貨方式');
            $table->json('wrap_price')->nullable()->comment('額外包裝費用');          // {currency: price}
            $table->json('bonus_exchange')->nullable()->comment('折抵兌率');            // {currency: rate}
            $table->json('bonus_percent')->nullable()->comment('折抵比例');             // {currency: percent}
            $table->string('information')->nullable()->comment('購物說明');             // Language {cart, checkout, billing, completed, cancel, return, rank}
            $table->timestamps();
        });

        // 取貨方式
        Schema::create('cart_delivery', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique()->comment('取貨代碼');
            $table->string('title')->comment('取貨名稱');
            $table->string('details')->nullable()->comment('說明細節');         // {description, editor, pic}
            $table->json('limits')->nullable()->comment('金額限制');
            $table->json('delivery_types')->nullable()->comment('物流類型');    // id list of parameter
            $table->json('delivery_service')->nullable()->comment('物流服務');
            $table->json('options')->nullable()->comment('參數設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // 付款方式
        Schema::create('cart_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique()->comment('付款代碼');
            $table->string('title')->comment('付款名稱');
            $table->string('details')->nullable()->comment('說明細節');         // {description, editor, pic}
            $table->json('limits')->nullable()->comment('金額限制');
            $table->json('payment_types')->nullable()->comment('金流類型');     // id list of parameter
            $table->json('delivery_types')->nullable()->comment('物流類型');    // id list of parameter
            $table->string('payment_service')->nullable()->comment('金流服務');
            $table->json('options')->nullable()->comment('參數設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // 運費設定
        Schema::create('cart_shipping', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('設定名稱');
            $table->string('details')->nullable()->comment('說明細節');             // {description}
            $table->json('price')->nullable()->comment('金額設定');                 // {currency:{price,fee,tax,minimum}}
            $table->json('deliveries')->nullable()->comment('取貨方式');            // id list of parameter
            $table->json('location_limits')->nullable()->comment('地區限制');       // {country,state,county,city}
            $table->json('measuring_limits')->nullable()->comment('測量限制');      // {perimeter,volume,space,weight}
            $table->json('product_brands')->nullable()->comment('商品品牌');        // id list of parameter
            $table->json('product_categories')->nullable()->comment('商品分類');    // id list of parameter
            $table->json('product_sets')->nullable()->comment('適用商品');          // id list of parameter
            $table->dateTime('start_at')->nullable()->comment('開始時間');
            $table->dateTime('end_at')->nullable()->comment('結束時間');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // 建立系統參數資料
        $this->insertSystemParameters();

        // 建立網站參數資料
        $this->insertSiteParameters();

        // 建立欄位擴充
        $this->insertColumnExtensions();

        // 建立購物車設定
        $this->insertCartConfig();

        // 建立取貨方式
        $this->insertCartDelivery();

        // 建立取貨方式
        $this->insertCartPayment();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 刪除欄位擴充
        $this->deleteColumnExtensions();

        // 刪除網站參數資料
        $this->deleteSiteParameters();

        // 刪除系統參數資料
        $this->deleteSystemParameters();

        Schema::dropIfExists('cart_shipping');
        Schema::dropIfExists('cart_payment');
        Schema::dropIfExists('cart_delivery');
        Schema::dropIfExists('cart_config');
    }

    /**
     * Insert system parameters for this module.
     *
     * @return void
     */
    public function insertSystemParameters()
    {
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $startGroupId = $rowGroupId = SeederHelper::getTableNextIncrement('system_parameter_group');
        $rowGroupId--;

        $systemGroupData = [
            ['code' => 'continued', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
            ['code' => 'additional', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
            ['code' => 'wrapped', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
            ['code' => 'returnable', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
            ['code' => 'rewarded', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
            ['code' => 'skip_pick_auto', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
            ['code' => 'delivery_kind', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
            ['code' => 'delivery_temperature', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
            ['code' => 'delivery_place', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
            ['code' => 'payment_kind', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
        ];

        DB::table('system_parameter_group')->insert($systemGroupData);

        // 多語系
        $systemGroupLanguage = [
            'zh-Hant' => [
                ['title' => '無庫存狀態'], ['title' => '加購限定'], ['title' => '額外包裝'], ['title' => '可否退貨'], ['title' => '計算紅利'], ['title' => '自動出貨方式'],
                ['title' => '貨運類型'], ['title' => '貨運溫層'], ['title' => '送貨對象'], ['title' => '付款類型'],
            ],
            'zh-Hans' => [
                ['title' => '无库存状态'], ['title' => '加购限定'], ['title' => '额外包装'], ['title' => '可否退货'], ['title' => '计算红利'], ['title' => '自动出货方式'],
                ['title' => '货运类型'], ['title' => '货运温层'], ['title' => '送货对象'], ['title' => '付款类型'],
            ],
            'ja' => [
                ['title' => '在庫切れの状態'], ['title' => 'プラス購入限られた'], ['title' => 'その他の包装'], ['title' => '返す可能'], ['title' => 'ボーナスを計算'], ['title' => '自動配送方法'],
                ['title' => '配送タイプ'], ['title' => '配送温度'], ['title' => '配送対象'], ['title' => '支払いタイプ'],
            ],
            'en' => [
                ['title' => 'Empty Status'], ['title' => 'Additional Only'], ['title' => 'Extra Packaging'], ['title' => 'Returnable'], ['title' => 'Has Reward'], ['title' => 'Auto Shipping'],
                ['title' => 'Kind'], ['title' => 'Temperature'], ['title' => 'Place'], ['title' => 'Kind'],
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'system_parameter_group', $systemGroupLanguage, $languageList, $startGroupId, false);


        $startItemId = $rowItemId = SeederHelper::getTableNextIncrement('system_parameter_item');
        $rowItemId--;

        $systemItemData = [
            [
                'group_id' => $startGroupId,
                'value' => '1',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'danger']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId,
                'value' => '0',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 1,
                'value' => '0',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 1,
                'value' => '1',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'danger']),
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 2,
                'value' => '0',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 2,
                'value' => '1',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'danger']),
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 3,
                'value' => '1',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'danger']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 3,
                'value' => '0',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 4,
                'value' => '1',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'danger']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 4,
                'value' => '0',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 5,
                'value' => '0',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 5,
                'value' => '1',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 6,
                'value' => 'express',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 6,
                'value' => 'mart',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 6,
                'value' => 'store',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 3,
            ],
            [
                'group_id' => $startGroupId + 6,
                'value' => 'web',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 4,
            ],
            [
                'group_id' => $startGroupId + 7,
                'value' => '0',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 7,
                'value' => '1',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'danger']),
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 8,
                'value' => 'address',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'info']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 8,
                'value' => 'mart',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'success']),
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 8,
                'value' => 'store',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'warning']),
                'sort' => 3,
            ],
            [
                'group_id' => $startGroupId + 8,
                'value' => 'empty',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 4,
            ],
            [
                'group_id' => $startGroupId + 9,
                'value' => 'credit',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 9,
                'value' => 'atm',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 9,
                'value' => 'mart',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 3,
            ],
            [
                'group_id' => $startGroupId + 9,
                'value' => 'app-pay',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 4,
            ],
            [
                'group_id' => $startGroupId + 9,
                'value' => 'free',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 5,
            ],
        ];

        DB::table('system_parameter_item')->insert($systemItemData);

        // 多語系
        $systemItemLanguage = [
            'zh-Hant' => [
                ['label' => '顯示補貨中'], ['label' => '前臺不顯示'],
                ['label' => '不限'], ['label' => '僅限加購'],
                ['label' => '不計包裝'], ['label' => '另計包裝'],
                ['label' => '可以退貨'], ['label' => '不可退貨'],
                ['label' => '計算紅利'], ['label' => '不計紅利'],
                ['label' => '須撿貨完成才可出貨'], ['label' => '未撿貨商品可直接出貨'],
                ['label' => '貨運配送'], ['label' => '便利商店'], ['label' => '門市自取'], ['label' => '網站'],
                ['label' => '無溫層'], ['label' => '有溫層'],
                ['label' => '指定地址'], ['label' => '指定超商'], ['label' => '指定門市'], ['label' => '無'],
                ['label' => '信用卡'], ['label' => 'ATM'], ['label' => '超商'], ['label' => 'APP'], ['label' => '免付款']
            ],
            'zh-Hans' => [
                ['label' => '显示补货中'], ['label' => '前台不显示'],
                ['label' => '不限'], ['label' => '仅限加购'],
                ['label' => '不计包装'], ['label' => '另计包装'],
                ['label' => '可以退货'], ['label' => '不可退货'],
                ['label' => '计算红利'], ['label' => '不计红利'],
                ['label' => '须捡货完成才可出货'], ['label' => '未捡货商品可直接出货'],
                ['label' => '货运配送'], ['label' => '便利超商'], ['label' => '门市亲取'], ['label' => '网站'],
                ['label' => '无温层'], ['label' => '有温层'],
                ['label' => '指定地址'], ['label' => '指定超商'], ['label' => '指定门市'], ['label' => '无'],
                ['label' => '信用卡'], ['label' => 'ATM'], ['label' => '便利店'], ['label' => 'APP'], ['label' => '免付款']
            ],
            'ja' => [
                ['label' => '補充を表示する'], ['label' => '前景が表示されない'],
                ['label' => '無制限'], ['label' => '購入を追加限られた'],
                ['label' => '計算しない'], ['label' => '計算価格'],
                ['label' => '返す可能'], ['label' => '返す無効'],
                ['label' => '計算ボーナス'], ['label' => '計算しない'],
                ['label' => '出荷前にピッキング必要'], ['label' => '直接出荷できます'],
                ['label' => '貨物配送'], ['label' => 'コンビニ'], ['label' => '自分で店舗から'], ['label' => 'ウェブ'],
                ['label' => '同じ温度'], ['label' => '異なります'],
                ['label' => '住所入力'], ['label' => '指定コンビニ'], ['label' => '指定店舗'], ['label' => 'なし'],
                ['label' => 'クレジット'], ['label' => 'ATM'], ['label' => 'コンビニ'], ['label' => 'アプリ'], ['label' => '無料']
            ],
            'en' => [
                ['label' => 'Replenishment'], ['label' => 'Hide'],
                ['label' => 'No Limit'], ['label' => 'Additional Only'],
                ['label' => 'Disable'], ['label' => 'Enable'],
                ['label' => 'Enable'], ['label' => 'Disable'],
                ['label' => 'Enable'], ['label' => 'Disable'],
                ['label' => 'Picking before shipping'], ['label' => 'Shipping without picking'],
                ['label' => 'Express'], ['label' => 'Mart'], ['label' => 'Store'], ['label' => 'Web'],
                ['label' => 'Constant'], ['label' => 'Different'],
                ['label' => 'Address'], ['label' => 'Mart'], ['label' => 'Store'], ['label' => 'Empty'],
                ['label' => 'Credit'], ['label' => 'ATM'], ['label' => 'Mart'], ['label' => 'APP'], ['label' => 'Free']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'system_parameter_item', $systemItemLanguage, $languageList, $startItemId, false);


        DB::table('language_resource')->insert($languageResourceData);
    }

    /**
     * Insert site parameters for this module.
     *
     * @return void
     */
    public function insertSiteParameters()
    {
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $startGroupId = $rowGroupId = SeederHelper::getTableNextIncrement('site_parameter_group');
        $rowGroupId--;

        $siteGroupData = [
            ['code' => 'payment_type', 'title' => 'site_parameter_group.title.' . ++$rowGroupId, 'editable' => true],
            ['code' => 'delivery_type', 'title' => 'site_parameter_group.title.' . ++$rowGroupId, 'editable' => true],
            ['code' => 'billing', 'title' => 'site_parameter_group.title.' . ++$rowGroupId, 'editable' => true],
            ['code' => 'shipping', 'title' => 'site_parameter_group.title.' . ++$rowGroupId, 'editable' => true],
        ];

        DB::table('site_parameter_group')->insert($siteGroupData);

        // 多語系
        $siteGroupLanguage = [
            'zh-Hant' => [
                ['title' => '金流類型'], ['title' => '物流類型'], ['title' => '付款說明'], ['title' => '運送說明']
            ],
            'zh-Hans' => [
                ['title' => '金流类型'], ['title' => '物流类型'], ['title' => '付款说明'], ['title' => '运送说明']
            ],
            'ja' => [
                ['title' => '支払いタイプ'], ['title' => '発送タイプ'], ['title' => '支払い説明'], ['title' => '発送説明']
            ],
            'en' => [
                ['title' => 'Payment Type'], ['title' => 'Delivery Type'], ['title' => 'About Billing'], ['title' => 'About Shipping']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'site_parameter_group', $siteGroupLanguage, $languageList, $startGroupId, false);


        $startItemId = $rowItemId = SeederHelper::getTableNextIncrement('site_parameter_item');
        $rowItemId--;

        $siteItemData = [
            [
                'group_id' => $startGroupId,
                'value' => 'cash',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'info']),
                'details' => 'site_parameter_item.details.' . $rowItemId,
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId,
                'value' => 'bank',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'info']),
                'details' => 'site_parameter_item.details.' . $rowItemId,
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId,
                'value' => 'credit',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'info']),
                'details' => 'site_parameter_item.details.' . $rowItemId,
                'sort' => 3,
            ],
            [
                'group_id' => $startGroupId + 1,
                'value' => 'store',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'info']),
                'details' => 'site_parameter_item.details.' . $rowItemId,
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 1,
                'value' => 'delivery',
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'info']),
                'details' => 'site_parameter_item.details.' . $rowItemId,
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 2,
                'value' => null,
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode([]),
                'details' => 'site_parameter_item.details.' . $rowItemId,
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 3,
                'value' => null,
                'label' => 'site_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode([]),
                'details' => 'site_parameter_item.details.' . $rowItemId,
                'sort' => 1,
            ],
        ];

        DB::table('site_parameter_item')->insert($siteItemData);

        // 多語系
        $siteItemLanguage = [
            'zh-Hant' => [
                ['label' => '現金付款', 'details' => json_encode([])], ['label' => '銀行轉帳', 'details' => json_encode([])], ['label' => '信用卡', 'details' => json_encode([])],
                ['label' => '門市取貨', 'details' => json_encode([])], ['label' => '宅配到府', 'details' => json_encode([])],
                ['label' => '通用付款說明', 'details' => json_encode(['editor' => '<p>通用付款說明</p>'])], ['label' => '通用運送說明', 'details' => json_encode(['editor' => '<p>通用運送說明</p>'])]
            ],
            'zh-Hans' => [
                ['label' => '现金付款', 'details' => json_encode([])], ['label' => '银行转帐', 'details' => json_encode([])], ['label' => '信用卡', 'details' => json_encode([])],
                ['label' => '门市取货', 'details' => json_encode([])], ['label' => '配送到府', 'details' => json_encode([])],
                ['label' => '通用付款说明', 'details' => json_encode(['editor' => '<p>通用付款说明</p>'])], ['label' => '通用运送说明', 'details' => json_encode(['editor' => '<p>通用运送说明</p>'])]
            ],
            'ja' => [
                ['label' => '現金払い', 'details' => json_encode([])], ['label' => '銀行振込', 'details' => json_encode([])], ['label' => 'クレジット', 'details' => json_encode([])],
                ['label' => '店で拾う', 'details' => json_encode([])], ['label' => '宅配配達', 'details' => json_encode([])],
                ['label' => '一般支払い説明', 'details' => json_encode(['editor' => '<p>一般支払い説明</p>'])], ['label' => '一般発送説明', 'details' => json_encode(['editor' => '<p>一般発送説明</p>'])]
            ],
            'en' => [
                ['label' => 'Cash', 'details' => json_encode([])], ['label' => 'Bank Transfer', 'details' => json_encode([])], ['label' => 'Credit Card', 'details' => json_encode([])],
                ['label' => 'Store Pickup', 'details' => json_encode([])], ['label' => 'Delivery', 'details' => json_encode([])],
                ['label' => 'General Billing', 'details' => json_encode(['editor' => '<p>General Billing</p>'])], ['label' => 'General Shipping', 'details' => json_encode(['editor' => '<p>General Shipping</p>'])]
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'site_parameter_item', $siteItemLanguage, $languageList, $startItemId, false);


        DB::table('language_resource')->insert($languageResourceData);
    }

    /**
     * Insert column extensions.
     *
     * @return void
     */
    public function insertColumnExtensions()
    {
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        // 欄位擴充
        $startExtensionId = $extensionRowId = SeederHelper::getTableNextIncrement('column_extension');
        $extensionRowId--;
        $columnExtensionData = [
            ['table_name' => 'product_set', 'column_name' => 'ec_parameters', 'sub_column_name' => 'payment_types', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldCheckbox', 'siteParam' => 'payment_type', 'required' => 'true', 'inline' => 'true'])],
            ['table_name' => 'product_set', 'column_name' => 'ec_parameters', 'sub_column_name' => 'delivery_types', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldCheckbox', 'siteParam' => 'delivery_type', 'required' => 'true', 'inline' => 'true'])],
            ['table_name' => 'product_set', 'column_name' => 'ec_parameters', 'sub_column_name' => 'billing', 'sort' => 3, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldRadio', 'siteParam' => 'billing', 'required' => 'true', 'inline' => 'true'])],
            ['table_name' => 'product_set', 'column_name' => 'ec_parameters', 'sub_column_name' => 'shipping', 'sort' => 4, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldRadio', 'siteParam' => 'shipping', 'required' => 'true', 'inline' => 'true'])],
            ['table_name' => 'product_set', 'column_name' => 'ec_parameters', 'sub_column_name' => 'continued', 'sort' => 5, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldRadio', 'systemParam' => 'continued', 'required' => 'true', 'inline' => 'true'])],
            ['table_name' => 'product_set', 'column_name' => 'ec_parameters', 'sub_column_name' => 'additional', 'sort' => 6, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldRadio', 'systemParam' => 'additional', 'required' => 'true', 'inline' => 'true'])],
            ['table_name' => 'product_set', 'column_name' => 'ec_parameters', 'sub_column_name' => 'wrapped', 'sort' => 7, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldRadio', 'systemParam' => 'wrapped', 'required' => 'true', 'inline' => 'true'])],
            ['table_name' => 'product_set', 'column_name' => 'ec_parameters', 'sub_column_name' => 'returnable', 'sort' => 8, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldRadio', 'systemParam' => 'returnable', 'required' => 'true', 'inline' => 'true'])],
            ['table_name' => 'product_set', 'column_name' => 'ec_parameters', 'sub_column_name' => 'rewarded', 'sort' => 9, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldRadio', 'systemParam' => 'rewarded', 'required' => 'true', 'inline' => 'true'])],

            ['table_name' => 'cart_config', 'column_name' => 'information', 'sub_column_name' => 'cart', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'cart_config', 'column_name' => 'information', 'sub_column_name' => 'checkout', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'cart_config', 'column_name' => 'information', 'sub_column_name' => 'billing', 'sort' => 3, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'cart_config', 'column_name' => 'information', 'sub_column_name' => 'completed', 'sort' => 4, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'cart_config', 'column_name' => 'information', 'sub_column_name' => 'cancel', 'sort' => 5, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'cart_config', 'column_name' => 'information', 'sub_column_name' => 'return', 'sort' => 6, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'cart_config', 'column_name' => 'information', 'sub_column_name' => 'rank', 'sort' => 7, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],

            ['table_name' => 'cart_delivery', 'column_name' => 'details', 'sub_column_name' => 'description', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldTextarea'])],
            ['table_name' => 'cart_delivery', 'column_name' => 'details', 'sub_column_name' => 'editor', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'cart_delivery', 'column_name' => 'options', 'sub_column_name' => 'delivery_kind', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldRadio', 'systemParam' => 'delivery_kind', 'inline' => 'true', 'required' => 'true'])],
            ['table_name' => 'cart_delivery', 'column_name' => 'options', 'sub_column_name' => 'delivery_temperature', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldRadio', 'systemParam' => 'delivery_temperature', 'inline' => 'true', 'required' => 'true'])],
            ['table_name' => 'cart_delivery', 'column_name' => 'options', 'sub_column_name' => 'delivery_place', 'sort' => 3, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldRadio', 'systemParam' => 'delivery_place', 'inline' => 'true', 'required' => 'true'])],

            ['table_name' => 'cart_payment', 'column_name' => 'details', 'sub_column_name' => 'description', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldTextarea'])],
            ['table_name' => 'cart_payment', 'column_name' => 'details', 'sub_column_name' => 'editor', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'cart_payment', 'column_name' => 'options', 'sub_column_name' => 'payment_kind', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldRadio', 'systemParam' => 'payment_kind', 'inline' => 'true', 'required' => 'true'])],

            ['table_name' => 'cart_shipping', 'column_name' => 'details', 'sub_column_name' => 'description', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldTextarea'])],
        ];

        DB::table('column_extension')->insert($columnExtensionData);

        // 多語系
        $columnExtensionLanguage = [
            ['title' => '金流類型'], ['title' => '物流類型'], ['title' => '付款說明'],
            ['title' => '運送說明'], ['title' => '無庫存狀態'], ['title' => '加購限定'],
            ['title' => '額外包裝'], ['title' => '可否退貨'], ['title' => '計算紅利'],
            ['title' => '購物說明'], ['title' => '購物確認'], ['title' => '付款說明'], ['title' => '訂購完成訊息'], ['title' => '取消訂單說明'], ['title' => '退換貨說明'], ['title' => '評價說明'],
            ['title' => '取貨簡述'], ['title' => '取貨說明'], ['title' => '貨運類型'], ['title' => '貨運溫層'], ['title' => '送貨對象'],
            ['title' => '付款簡述'], ['title' => '付款說明'], ['title' => '付款類型'],
            ['title' => '設定簡述'],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'column_extension', $columnExtensionLanguage, $languageList, $startExtensionId);

        DB::table('language_resource')->insert($languageResourceData);
    }

    protected function insertCartConfig()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $configData = [
            [
                'id' => 'web',
                'browse_history' => 180,
                'track_list' => 180,
                'shipping_notify' => 180,
                'return_limit' => 7,
                'rank_limit' => 30,
                'less_quantity' => 1,
                'wrap_price' => json_encode(['TWD' => '60']),
                'bonus_exchange' => json_encode(['TWD' => '1']),
                'bonus_percent' => json_encode(['TWD' => '30']),
                'information' => 'cart_config.information.web',
                'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];
        DB::table('cart_config')->insert($configData);

        // 多語系
        $configLanguage = [
            'zh-Hant' => [
                ['id' => 'web', 'information' => json_encode([
                    'cart' => '<p>購物車說明文字</p>',
                    'checkout' => '<p>購物確認說明文字</p>',
                    'billing' => '<p>付款確認說明文字</p>',
                    'completed' => '<p>訂單完成說明文字</p>',
                    'cancel' => '<p>取消訂單說明文字</p>',
                    'return' => '<p>退換貨說明文字</p>',
                    'rank' => '<p>評價說明文字</p>',
                ])],
            ],
            'zh-Hans' => [
                ['id' => 'web', 'information' => json_encode([
                    'cart' => '<p>购物车说明文字</p>',
                    'checkout' => '<p>购物确认说明文字</p>',
                    'billing' => '<p>付款确认说明文字</p>',
                    'completed' => '<p>订单完成说明文字</p>',
                    'cancel' => '<p>取消订单说明文字</p>',
                    'return' => '<p>退货说明文字</p>',
                    'rank' => '<p>评价说明文字</p>',
                ])],
            ],
            'ja' => [
                ['id' => 'web', 'information' => json_encode([
                    'cart' => '<p>ショッピングカートの説明</p>',
                    'checkout' => '<p>買い物確認の説明</p>',
                    'billing' => '<p>預託確認の説明</p>',
                    'completed' => '<p>注文完了の説明</p>',
                    'cancel' => '<p>注文キャンセルの説明</p>',
                    'return' => '<p>返すの説明</p>',
                    'rank' => '<p>評価の説明</p>',
                ])],
            ],
            'en' => [
                ['id' => 'web', 'information' => json_encode([
                    'cart' => '<p>Cart Information</p>',
                    'checkout' => '<p>Checkout Information</p>',
                    'billing' => '<p>Billing Information</p>',
                    'completed' => '<p>Completed Information</p>',
                    'cancel' => '<p>Cancel Information</p>',
                    'return' => '<p>Return Information</p>',
                    'rank' => '<p>Ranking Information</p>',
                ])],
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'cart_config', $configLanguage, $languageList, null, false);

        DB::table('language_resource')->insert($languageResourceData);
    }

    protected function insertCartDelivery()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $startDeliveryId = $rowDeliveryId = SeederHelper::getTableNextIncrement('cart_delivery');
        $rowDeliveryId--;
        $deliveryData = [
            [
                'code' => 'express-normal',
                'title' => 'cart_delivery.title.' . ++$rowDeliveryId,
                'details' => 'cart_delivery.details.' . $rowDeliveryId,
                'limits' => null,
                'delivery_types' => json_encode(['store']),
                'delivery_service' => json_encode([]),
                'options' => json_encode([
                    'delivery_kind' => 'express',
                    'delivery_temperature' => '0',
                    'delivery_place' => 'address',
                ]),
                'sort' => 1, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'code' => 'express-low',
                'title' => 'cart_delivery.title.' . ++$rowDeliveryId,
                'details' => 'cart_delivery.details.' . $rowDeliveryId,
                'limits' => null,
                'delivery_types' => json_encode(['store']),
                'delivery_service' => json_encode([]),
                'options' => json_encode([
                    'delivery_kind' => 'express',
                    'delivery_temperature' => '1',
                    'delivery_place' => 'address',
                ]),
                'sort' => 2, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'code' => 'mart',
                'title' => 'cart_delivery.title.' . ++$rowDeliveryId,
                'details' => 'cart_delivery.details.' . $rowDeliveryId,
                'limits' => null,
                'delivery_types' => json_encode(['store']),
                'delivery_service' => json_encode([]),
                'options' => json_encode([
                    'delivery_kind' => 'mart',
                    'delivery_temperature' => '0',
                    'delivery_place' => 'mart',
                ]),
                'sort' => 3, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'code' => 'self',
                'title' => 'cart_delivery.title.' . ++$rowDeliveryId,
                'details' => 'cart_delivery.details.' . $rowDeliveryId,
                'limits' => null,
                'delivery_types' => json_encode(['store']),
                'delivery_service' => json_encode([]),
                'options' => json_encode([
                    'delivery_kind' => 'store',
                    'delivery_temperature' => '0',
                    'delivery_place' => 'store',
                ]),
                'sort' => 4, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];

        DB::table('cart_delivery')->insert($deliveryData);

        $deliveryLanguage = [
            'zh-Hant' => [
                [
                    'title' => '宅配到府',
                    'details' => json_encode([
                        'description' => '將由貨運公司將商品送至您指定的地點。',
                        'editor' => '<p>請確認指定之地址可以有人收件。若因未收件而遭退件須重新寄送，將會向您酌收期間產生之額外物流費用。</p>',
                    ]),
                ],
                [
                    'title' => '低溫宅配',
                    'details' => json_encode([
                        'description' => '將由貨運公司將商品送至您指定的地點。',
                        'editor' => '<p>請確認指定之地址可以有人收件。若因未收件而遭退件須重新寄送，將會向您酌收期間產生之額外物流費用。</p>',
                    ]),
                ],
                [
                    'title' => '超商取貨',
                    'details' => json_encode([
                        'description' => '超商物流會將商品送至您指定的超商。',
                        'editor' => '<p>請於到貨通知後盡速前往指定超商收件。若因未收件而遭退件須重新寄送，將會向您酌收期間產生之額外物流費用。</p>',
                    ]),
                ],
                [
                    'title' => '門市自取',
                    'details' => json_encode([
                        'description' => '請攜帶訂單編號前往門市，並出示證件以領取商品。',
                        'editor' => '<p>全台門市皆可取貨，前往取貨前請務必確認是否收到到貨通知，或電洽取貨門市確認，以免向隅。</p>',
                    ]),
                ],
            ],
            'zh-Hans' => [
                [
                    'title' => '宅配到府',
                    'details' => json_encode([
                        'description' => '将由货运公司将商品送至您指定的地点。',
                        'editor' => '<p>请确认指定之地址可以有人收件。若因未收件而遭退件须重新寄送，将会向您酌收期间产生之额外物流费用。</p>',
                    ]),
                ],
                [
                    'title' => '低温宅配',
                    'details' => json_encode([
                        'description' => '将由货运公司将商品送至您指定的地点。',
                        'editor' => '<p>请确认指定之地址可以有人收件。若因未收件而遭退件须重新寄送，将会向您酌收期间产生之额外物流费用。</p>',
                    ]),
                ],
                [
                    'title' => '超商取货',
                    'details' => json_encode([
                        'description' => '超商物流会将商品送至您指定的超商。',
                        'editor' => '<p>请于到货通知后尽速前往指定超商收件。若因未收件而遭退件须重新寄送，将会向您酌收期间产生之额外物流费用。</p>',
                    ]),
                ],
                [
                    'title' => '门市自取',
                    'details' => json_encode([
                        'description' => '请携带订单编号前往门市，并出示证件以领取商品。',
                        'editor' => '<p>全台湾门市皆可取货，前往取货前请务必确认是否收到到货通知，或电洽取货门市确认，以免向隅。</p>',
                    ]),
                ],
            ],
            'ja' => [
                [
                    'title' => '常温宅配',
                    'details' => json_encode([
                        'description' => '商品は物流会社によって指定された場所に配送されます。',
                        'editor' => '<p>誰かがそのアドレスを受信できることを確認してください。 未受信の商品が原因で返品された場合は、再送され、その期間中に発生した追加の物流費が請求されます。</p>',
                    ]),
                ],
                [
                    'title' => '低温宅配',
                    'details' => json_encode([
                        'description' => '商品は物流会社によって指定された場所に配送されます。',
                        'editor' => '<p>誰かがそのアドレスを受信できることを確認してください。 未受信の商品が原因で返品された場合は、再送され、その期間中に発生した追加の物流費が請求されます。</p>',
                    ]),
                ],
                [
                    'title' => 'コンビニピックアップ',
                    'details' => json_encode([
                        'description' => 'コンビニ物流はあなたの指定されたスーパービジネスにアイテムを届けます。',
                        'editor' => '<p>到着通知後できるだけ早く指定スーパーディーラーに行きなさい。 未受信の商品が原因で返品された場合は、再送され、その期間中に発生した追加の物流費が請求されます。</p>',
                    ]),
                ],
                [
                    'title' => '自分でピックアップ',
                    'details' => json_encode([
                        'description' => '注文番号を店に持って行き、あなたのIDを提示して商品を受け取ります。',
                        'editor' => '<p>台湾のすべての店で商品を拾うことができます｡商品を拾う前に、必ず到着通知を受け取ったかどうかを確認するか、店で商品をチェックして戸惑いを避けてください。</p>',
                    ]),
                ],
            ],
            'en' => [
                [
                    'title' => 'Normal Express',
                    'details' => json_encode([
                        'description' => 'Express company will ship your products to the address you give.',
                        'editor' => '<p>Please check the address you give can someone signature package. If you do not get and it is returned, you maybe need pay some fee for shipping.</p>',
                    ]),
                ],
                [
                    'title' => 'Low-Temperature Express',
                    'details' => json_encode([
                        'description' => 'Express company will ship your products to the address you give.',
                        'editor' => '<p>Please check the address you give can someone signature package. If you do not get and it is returned, you maybe need pay some fee for shipping.</p>',
                    ]),
                ],
                [
                    'title' => 'Convenient Store',
                    'details' => json_encode([
                        'description' => 'Convenient Store will ship your products to the store you chose.',
                        'editor' => '<p>Please get your products as soon as you can after get arriving notify. If you do not get and it is returned, you maybe need pay some fee for shipping.</p>',
                    ]),
                ],
                [
                    'title' => 'Store Self-Get',
                    'details' => json_encode([
                        'description' => 'Please go to our store with your order number and show up your license to get your products.',
                        'editor' => '<p>Before go to store, please check you get arriving notify or call store to check your order.</p>',
                    ]),
                ],
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'cart_delivery', $deliveryLanguage, $languageList, $startDeliveryId, false);

        DB::table('language_resource')->insert($languageResourceData);
    }

    protected function insertCartPayment()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $startPaymentId = $rowPaymentId = SeederHelper::getTableNextIncrement('cart_payment');
        $rowPaymentId--;
        $paymentData = [
            [
                'code' => 'credit-full',
                'title' => 'cart_payment.title.' . ++$rowPaymentId,
                'details' => 'cart_payment.details.' . $rowPaymentId,
                'limits' => null,
                'payment_types' => null,
                'delivery_types' => null,
                'payment_service' => null,
                'options' => json_encode([
                    'payment_kind' => 'credit',
                ]),
                'sort' => 1, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'code' => 'atm',
                'title' => 'cart_payment.title.' . ++$rowPaymentId,
                'details' => 'cart_payment.details.' . $rowPaymentId,
                'limits' => null,
                'payment_types' => null,
                'delivery_types' => null,
                'payment_service' => null,
                'options' => json_encode([
                    'payment_kind' => 'atm',
                ]),
                'sort' => 2, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'code' => 'mart',
                'title' => 'cart_payment.title.' . ++$rowPaymentId,
                'details' => 'cart_payment.details.' . $rowPaymentId,
                'limits' => null,
                'payment_types' => null,
                'delivery_types' => null,
                'payment_service' => null,
                'options' => json_encode([
                    'payment_kind' => 'mart',
                ]),
                'sort' => 3, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'code' => 'free',
                'title' => 'cart_payment.title.' . ++$rowPaymentId,
                'details' => 'cart_payment.details.' . $rowPaymentId,
                'limits' => null,
                'payment_types' => null,
                'delivery_types' => null,
                'payment_service' => null,
                'options' => json_encode([
                    'payment_kind' => 'zero',
                ]),
                'sort' => 4, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];

        DB::table('cart_payment')->insert($paymentData);

        $paymentLanguage = [
            [
                'title' => '信用卡付款 (一次付清)',
                'details' => json_encode([
                    'description' => '線上刷卡，可接受 VISA、Master、JCB。',
                    'editor' => '<p>信用卡付款詳細說明。</p>',
                ]),
            ],
            [
                'title' => 'ATM 轉帳',
                'details' => json_encode([
                    'description' => '我們將會提供 ATM 轉帳專用虛擬帳號供您轉帳匯款。',
                    'editor' => '<p>ATM 轉帳詳細說明。</p>',
                ]),
            ],
            [
                'title' => '超商付款',
                'details' => json_encode([
                    'description' => '付款條碼列印後至全台各大便利商店，全家、萊爾富、OK、7-ELEVEN 等繳費付款3。',
                    'editor' => '<p>超商付款詳細說明。</p>',
                ]),
            ],
            [
                'title' => '免付費訂單',
                'details' => json_encode([
                    'description' => '適用免付費訂單，如果結帳金額為零時，請選擇此方式結帳。',
                    'editor' => '<p>免付費訂單詳細說明。</p>',
                ]),
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'cart_payment', $paymentLanguage, $languageList, $startPaymentId);

        DB::table('language_resource')->insert($languageResourceData);
    }

    /**
     * Delete system parameters for this module.
     *
     * @return void
     */
    public function deleteSystemParameters()
    {
        $parameterCodeSet = [
            'continued', 'additional', 'wrapped', 'returnable', 'rewarded',
            'skip_pick_auto', 'delivery_kind', 'delivery_temperature', 'delivery_place', 'payment_kind'
        ];

        SeederHelper::deleteSystemParametersByGroupCode($parameterCodeSet);
    }

    /**
     * Delete site parameters for this module.
     *
     * @return void
     */
    public function deleteSiteParameters()
    {
        $parameterCodeSet = ['payment_type', 'delivery_type', 'billing', 'shipping'];

        SeederHelper::deleteSiteParametersByGroupCode($parameterCodeSet);
    }

    /**
     * Delete column extensions.
     *
     * @return void
     */
    public function deleteColumnExtensions()
    {
        DB::table('column_extension')
            ->where('table_name', 'product_set')
            ->where('column_name', 'ec_parameters')
            ->delete();

        $tables = ['cart_config', 'cart_delivery', 'cart_payment', 'cart_shipping'];
        DB::table('column_extension')->whereIn('table_name', $tables)->delete();
    }
}
