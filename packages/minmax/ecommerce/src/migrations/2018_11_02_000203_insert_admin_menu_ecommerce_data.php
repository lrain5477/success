<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class InsertAdminMenuEcommerceData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 建立預設資料
        $this->insertDatabase();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 刪除預設資料
        $this->deleteDatabase();
    }

    /**
     * Insert default data
     *
     * @return void
     */
    public function insertDatabase()
    {
        $timestamp = date('Y-m-d H:i:s');

        // 建立權限物件
        $permissionsData = [];
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'cartPayment', '付款方式', 221));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'cartDelivery', '取貨方式', 222));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'cartShipping', '運費設定', 223));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'cartConfig', '購物車設定', ['U'], 224));
        DB::table('permissions')->insert($permissionsData);

        // 管理員選單
        if ($menuClassId = DB::table('admin_menu')->where('uri', 'root-module')->value('id')) {
            $adminMenuData = [
                [
                    'id' => $menuParentId1 = uuidl(),
                    'title' => '購物車',
                    'uri' => 'control-cart',
                    'controller' => null,
                    'model' => null,
                    'parent_id' => $menuClassId,
                    'link' => null,
                    'icon' => 'icon-cart',
                    'permission_key' => null,
                    'sort' => 202, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '付款方式',
                    'uri' => 'cart-payment',
                    'controller' => 'CartPaymentController',
                    'model' => 'CartPayment',
                    'parent_id' => $menuParentId1,
                    'link' => 'cart-payment',
                    'icon' => null,
                    'permission_key' => 'cartPaymentShow',
                    'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '取貨方式',
                    'uri' => 'cart-delivery',
                    'controller' => 'CartDeliveryController',
                    'model' => 'CartDelivery',
                    'parent_id' => $menuParentId1,
                    'link' => 'cart-delivery',
                    'icon' => null,
                    'permission_key' => 'cartDeliveryShow',
                    'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '運費設定',
                    'uri' => 'cart-shipping',
                    'controller' => 'CartShippingController',
                    'model' => 'CartShipping',
                    'parent_id' => $menuParentId1,
                    'link' => 'cart-shipping',
                    'icon' => null,
                    'permission_key' => 'cartShippingShow',
                    'sort' => 3, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '購物車設定',
                    'uri' => 'cart-config',
                    'controller' => 'CartConfigController',
                    'model' => 'CartConfig',
                    'parent_id' => $menuParentId1,
                    'link' => 'cart-config/web/edit',
                    'icon' => null,
                    'permission_key' => 'cartConfigEdit',
                    'sort' => 4, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
            ];
            DB::table('admin_menu')->insert($adminMenuData);
        }
    }

    public function deleteDatabase()
    {
        $uriSet = ['control-cart', 'cart-payment', 'cart-delivery', 'cart-config'];

        DB::table('admin_menu')->whereIn('uri', $uriSet)->delete();

        $permissionSet = ['cartPayment', 'cartDelivery', 'cartConfig'];

        DB::table('permissions')->whereIn('group', $permissionSet)->delete();
    }
}
