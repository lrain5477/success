<?php

namespace Minmax\Ecommerce\Admin;

use Minmax\Base\Admin\Presenter;
use Minmax\Product\Admin\ProductBrandRepository;
use Minmax\Product\Admin\ProductCategoryRepository;
use Minmax\Product\Admin\ProductSetRepository;
use Minmax\World\Admin\WorldCityRepository;
use Minmax\World\Admin\WorldCountryRepository;
use Minmax\World\Admin\WorldCountyRepository;
use Minmax\World\Admin\WorldCurrencyRepository;
use Minmax\World\Admin\WorldStateRepository;

/**
 * Class CartShippingPresenter
 */
class CartShippingPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxEcommerce::';

    protected $languageColumns = ['title', 'details'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'currencies' => (new WorldCurrencyRepository)->getSelectParameters('active', true),
            'deliveries' => (new CartDeliveryRepository)->getSelectParameters(),
            'product_brands' => (new ProductBrandRepository())->getSelectParameters(),
            'product_categories' => (new ProductCategoryRepository)->getSelectParameters(false, true),
            'active' => systemParam('active'),
        ];
    }

    /**
     * @param  \Minmax\Ecommerce\Models\CartShipping $model
     * @param  string $column
     * @param  array $options
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getShowLocations($model, $column, $options = [])
    {
        $modelName = class_basename($model);
        $columnValue = $this->getModelValue($model, $column) ?? [];

        $fieldId = "{$modelName}-{$column}";
        $fieldLabel = array_get($options, 'label', __($this->packagePrefix . "models.{$modelName}.{$column}.country"));
        $fieldValue = array_get($options, 'defaultValue', $columnValue);

        $fullAddress = '';

        if (! in_array('country', array_get($options, 'excepts', [])) && array_key_exists('country', $fieldValue)) {
            $country = (new WorldCountryRepository)->find(array_get($fieldValue, 'country'));
            $fullAddress .= blank($country) ? '' : ((blank($fullAddress) ? '' : ', ') . $country->name);
            $fieldLabel = array_get($options, 'label', blank($country) ? $fieldLabel : __($this->packagePrefix . "models.{$modelName}.{$column}.country"));
        }

        if (! in_array('state', array_get($options, 'excepts', [])) && array_key_exists('state', $fieldValue)) {
            $state = (new WorldStateRepository)->find(array_get($fieldValue, 'state'));
            $fullAddress .= blank($state) ? '' : ((blank($fullAddress) ? '' : ', ') . $state->name);
            $fieldLabel = array_get($options, 'label', blank($state) ? $fieldLabel : __($this->packagePrefix . "models.{$modelName}.{$column}.state"));
        }

        if (! in_array('county', array_get($options, 'excepts', [])) && array_key_exists('county', $fieldValue)) {
            $county = (new WorldCountyRepository)->find(array_get($fieldValue, 'county'));
            $fullAddress .= blank($county) ? '' : ((blank($fullAddress) ? '' : ', ') . $county->name);
            $fieldLabel = array_get($options, 'label', blank($county) ? $fieldLabel : __($this->packagePrefix . "models.{$modelName}.{$column}.county"));
        }

        if (! in_array('city', array_get($options, 'excepts', [])) && array_key_exists('city', $fieldValue)) {
            $city = (new WorldCityRepository)->find(array_get($fieldValue, 'city'));
            $fullAddress .= blank($city) ? '' : ((blank($fullAddress) ? '' : ', ') . $city->name);
            $fieldLabel = array_get($options, 'label', blank($city) ? $fieldLabel : __($this->packagePrefix . "models.{$modelName}.{$column}.city"));
        }

        $componentData = [
            'id' => $fieldId,
            'language' => false,
            'label' => $fieldLabel,
            'value' => $this->getPureString($fullAddress),
        ];

        return view('MinmaxBase::admin.layouts.show.normal-text', $componentData);
    }

    /**
     * @param  \Minmax\Ecommerce\Models\CartShipping $model
     * @param  string $column
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getShowPriceList($model, $column)
    {
        $columnValue = $this->getModelValue($model, $column) ?? [];

        $fieldLabel = __("MinmaxEcommerce::models.CartShipping.{$column}");
        $fieldValue = $columnValue;

        $componentData = [
            'id' => 'CartPayment-limits',
            'label' => $fieldLabel,
            'currencies' => array_get($this->parameterSet, 'currencies', []),
            'values' => $fieldValue,
        ];

        return view('MinmaxEcommerce::admin.cart-shipping.show-price-list', $componentData);
    }

    /**
     * @param  \Minmax\Ecommerce\Models\CartShipping $model
     * @param  string $column
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getShowProducts($model, $column)
    {
        $columnValue = $this->getModelValue($model, $column) ?? [];

        $fieldLabel = __("MinmaxEcommerce::models.CartShipping.{$column}");
        $fieldValue = $columnValue;

        $products = (new ProductSetRepository)->query()->whereIn('sku', $fieldValue)->pluck('title', 'sku');

        $componentData = [
            'label' => $fieldLabel,
            'values' => $fieldValue,
            'products' => $products,
        ];

        return view('MinmaxEcommerce::admin.cart-shipping.show-product-set-list', $componentData);
    }

    /**
     * @param  \Minmax\Ecommerce\Models\CartShipping $model
     * @param  string $column
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getFieldLocations($model, $column)
    {
        $modelName = class_basename($model);
        $columnValue = $this->getModelValue($model, $column) ?? [];

        $fieldId = "{$modelName}-{$column}";
        $fieldName = "{$modelName}[{$column}]";
        $fieldValue = $columnValue;

        $countries = (new WorldCountryRepository)->getSelectParameters();

        $hintValue = __($this->packagePrefix . "models.{$modelName}.hint.{$column}");

        $componentData = [
            'id' => $fieldId,
            'name' => $fieldName,
            'value' => $fieldValue,
            'countries' => $countries,
            'hint' => $hintValue,
        ];

        return view('MinmaxEcommerce::admin.cart-shipping.form-location-selections', $componentData);
    }

    /**
     * @param  \Minmax\Ecommerce\Models\CartShipping $model
     * @param  string $column
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getFieldProducts($model, $column)
    {
        $modelName = class_basename($model);
        $columnValue = $this->getModelValue($model, $column) ?? [];

        $fieldId = "{$modelName}-{$column}";
        $fieldLabel = __("MinmaxEcommerce::models.CartShipping.{$column}");
        $fieldName = "{$modelName}[{$column}]";
        $fieldValue = $columnValue;

        $products = (new ProductSetRepository)->query()->whereIn('sku', $fieldValue)->pluck('title', 'sku');

        $componentData = [
            'id' => $fieldId,
            'label' => $fieldLabel,
            'name' => $fieldName,
            'values' => $fieldValue,
            'products' => $products,
        ];

        return view('MinmaxEcommerce::admin.cart-shipping.form-product-set-list', $componentData);
    }

    /**
     * @param  \Minmax\Ecommerce\Models\CartShipping $model
     * @param  string $column
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getFieldPriceList($model, $column)
    {
        $modelName = class_basename($model);
        $columnValue = $this->getModelValue($model, $column) ?? [];

        $fieldLabel = __("MinmaxEcommerce::models.CartShipping.{$column}");
        $fieldValue = $columnValue;

        $componentData = [
            'id' => 'CartPayment-limits',
            'label' => $fieldLabel,
            'name' => $modelName,
            'currencies' => array_get($this->parameterSet, 'currencies', []),
            'values' => $fieldValue,
        ];

        return view('MinmaxEcommerce::admin.cart-shipping.form-price-list', $componentData);
    }
}
