<?php

namespace Minmax\Ecommerce\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Ecommerce\Models\CartShipping;

/**
 * Class CartShippingRepository
 * @property CartShipping $model
 * @method CartShipping find($id)
 * @method CartShipping one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method CartShipping[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method CartShipping create($attributes)
 * @method CartShipping save($model, $attributes)
 * @method CartShipping|\Illuminate\Database\Eloquent\Builder query()
 */
class CartShippingRepository extends Repository
{
    const MODEL = CartShipping::class;

    protected $sort = 'sort';

    protected $sorting = true;

    protected $languageColumns = ['title', 'details'];

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'cart_shipping';
    }
}
