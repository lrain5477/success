<?php

namespace Minmax\Ecommerce\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Ecommerce\Models\CartPayment;

/**
 * Class CartPaymentTransformer
 */
class CartPaymentTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'cartPaymentShow',
        'U' => 'cartPaymentEdit',
        'D' => 'cartPaymentDestroy',
    ];

    /**
     * Transformer constructor.
     * @param  CartPaymentPresenter $presenter
     * @param  string $uri
     */
    public function __construct(CartPaymentPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  CartPayment $model
     * @return array
     * @throws \Throwable
     */
    public function transform(CartPayment $model)
    {
        return [
            'code' => $this->presenter->getGridText($model, 'code'),
            'title' => $this->presenter->getGridText($model, 'title'),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
