<?php

namespace Minmax\Ecommerce\Admin;

use Illuminate\Support\Facades\Cache;
use Minmax\Base\Admin\Repository;
use Minmax\Ecommerce\Models\CartConfig;

/**
 * Class CartConfigRepository
 * @property CartConfig $model
 * @method CartConfig find($id)
 * @method CartConfig one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method CartConfig[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method CartConfig create($attributes)
 * @method CartConfig save($model, $attributes)
 * @method CartConfig|\Illuminate\Database\Eloquent\Builder query()
 */
class CartConfigRepository extends Repository
{
    const MODEL = CartConfig::class;

    protected $languageColumns = ['information'];

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'cart_config';
    }

    protected function afterCreate()
    {
        Cache::forget("cartConfigs.{$this->model->id}");
    }

    protected function afterSave()
    {
        Cache::forget("cartConfigs.{$this->model->id}");
    }
}
