<?php

namespace Minmax\Ecommerce\Admin;

use Minmax\Base\Admin\Controller;

/**
 * Class CartConfigController
 */
class CartConfigController extends Controller
{
    protected $packagePrefix = 'MinmaxEcommerce::';

    public function __construct(CartConfigRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }
}
