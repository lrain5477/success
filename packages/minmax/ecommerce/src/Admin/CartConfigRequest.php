<?php

namespace Minmax\Ecommerce\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class CartConfigRequest
 */
class CartConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('cartConfigEdit');
            case 'POST':
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'CartConfig.browse_history' => 'required|integer',
                    'CartConfig.track_list' => 'required|integer',
                    'CartConfig.shipping_notify' => 'required|integer',
                    'CartConfig.return_limit' => 'required|integer',
                    'CartConfig.rank_limit' => 'required|integer',
                    'CartConfig.less_quantity' => 'required|integer',
                    'CartConfig.skip_pick_relation' => 'nullable|array',
                    'CartConfig.skip_pick_auto' => 'required|boolean',
                    'CartConfig.wrap_price' => 'nullable|array',
                    'CartConfig.bonus_exchange' => 'nullable|array',
                    'CartConfig.bonus_percent' => 'nullable|array',
                    'CartConfig.information' => 'nullable|array',
                ];
            case 'POST':
            default:
                return [];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'CartConfig.browse_history' => __('MinmaxEcommerce::models.CartConfig.browse_history'),
            'CartConfig.track_list' => __('MinmaxEcommerce::models.CartConfig.track_list'),
            'CartConfig.shipping_notify' => __('MinmaxEcommerce::models.CartConfig.shipping_notify'),
            'CartConfig.return_limit' => __('MinmaxEcommerce::models.CartConfig.return_limit'),
            'CartConfig.rank_limit' => __('MinmaxEcommerce::models.CartConfig.rank_limit'),
            'CartConfig.less_quantity' => __('MinmaxEcommerce::models.CartConfig.less_quantity'),
            'CartConfig.skip_pick_relation' => __('MinmaxEcommerce::models.CartConfig.skip_pick_relation'),
            'CartConfig.skip_pick_auto' => __('MinmaxEcommerce::models.CartConfig.skip_pick_auto'),
            'CartConfig.wrap_price' => __('MinmaxEcommerce::models.CartConfig.wrap_price'),
            'CartConfig.bonus_exchange' => __('MinmaxEcommerce::models.CartConfig.bonus_exchange'),
            'CartConfig.bonus_percent' => __('MinmaxEcommerce::models.CartConfig.bonus_percent'),
            'CartConfig.information' => __('MinmaxEcommerce::models.CartConfig.information'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
