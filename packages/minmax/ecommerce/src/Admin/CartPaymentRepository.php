<?php

namespace Minmax\Ecommerce\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Ecommerce\Models\CartPayment;

/**
 * Class CartPaymentRepository
 * @property CartPayment $model
 * @method CartPayment find($id)
 * @method CartPayment one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method CartPayment[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method CartPayment create($attributes)
 * @method CartPayment save($model, $attributes)
 * @method CartPayment|\Illuminate\Database\Eloquent\Builder query()
 */
class CartPaymentRepository extends Repository
{
    const MODEL = CartPayment::class;

    protected $sort = 'sort';

    protected $sorting = true;

    protected $languageColumns = ['title', 'details'];

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'cart_payment';
    }

    public function getSelectParameters()
    {
        return $this->all(...func_get_args())
            ->sortBy('sort')
            ->mapWithKeys(function ($item) {
                /** @var CartPayment $item */
                return [$item->code => ['title' => $item->title, 'options' => $item->options]];
            })
            ->toArray();
    }
}
