<?php

namespace Minmax\Ecommerce\Admin;

use Minmax\Base\Admin\Presenter;
use Minmax\World\Admin\WorldCurrencyRepository;

/**
 * Class CartConfigPresenter
 */
class CartConfigPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxEcommerce::';

    protected $languageColumns = ['information'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'skip_pick_relation' => (new CartPaymentRepository)->getSelectParameters(),
            'skip_pick_auto' => systemParam('skip_pick_auto'),
            'currencies' => (new WorldCurrencyRepository)->getSelectParameters('active', true),
        ];
    }

    /**
     * @param  \Minmax\Ecommerce\Models\CartConfig $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFieldWrapPriceList($model)
    {
        $modelName = 'CartConfig';
        $priceValue = $this->getModelValue($model, 'wrap_price') ?? [];

        $fieldLabel = __("MinmaxEcommerce::admin.form.CartConfig.wrap_price");
        $fieldValue = $priceValue;

        $componentData = [
            'id' => 'CartConfig-wrap_price',
            'label' => $fieldLabel,
            'name' => $modelName,
            'currencies' => array_get($this->parameterSet, 'currencies', []),
            'values' => $fieldValue,
        ];

        return view('MinmaxEcommerce::admin.cart-config.form-wrap-price-list', $componentData);
    }

    /**
     * @param  \Minmax\Ecommerce\Models\CartConfig $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFieldBonusSettingList($model)
    {
        $modelName = 'CartConfig';
        $exchangeValue = $this->getModelValue($model, 'bonus_exchange') ?? [];
        $percentValue = $this->getModelValue($model, 'bonus_percent') ?? [];

        $fieldLabel = __("MinmaxEcommerce::admin.form.CartConfig.bonus_setting");
        $fieldValue = [];

        foreach ($exchangeValue as $currency => $exchange) {
            $fieldValue[$currency] = ['exchange' => $exchange, 'percent' => 0];
        }
        foreach ($percentValue as $currency => $percent) {
            if (! isset($fieldValue[$currency])) {
                $fieldValue[$currency]['exchange'] = 0;
            }
            $fieldValue[$currency]['percent'] = $percent;
        }

        $componentData = [
            'id' => 'CartConfig-bonus',
            'label' => $fieldLabel,
            'name' => $modelName,
            'currencies' => array_get($this->parameterSet, 'currencies', []),
            'values' => $fieldValue,
        ];

        return view('MinmaxEcommerce::admin.cart-config.form-bonus-setting-list', $componentData);
    }
}
