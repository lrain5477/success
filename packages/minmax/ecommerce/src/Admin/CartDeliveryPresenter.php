<?php

namespace Minmax\Ecommerce\Admin;

use Minmax\Base\Admin\Presenter;
use Minmax\Base\Admin\ServiceConfigRepository;
use Minmax\World\Admin\WorldCurrencyRepository;

/**
 * Class CartDeliveryPresenter
 */
class CartDeliveryPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxEcommerce::';

    protected $languageColumns = ['title', 'details'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'currencies' => (new WorldCurrencyRepository)->getSelectParameters('active', true),
            'delivery_types' => siteParam('delivery_type'),
            'delivery_service' => (new ServiceConfigRepository)->getSelectParameters('express'),
            'active' => systemParam('active'),
        ];
    }

    /**
     * @param  \Minmax\Ecommerce\Models\CartDelivery $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFieldLimitsPriceList($model)
    {
        $modelName = 'CartDelivery';
        $columnValue = $this->getModelValue($model, 'limits') ?? [];

        $fieldLabel = __("MinmaxEcommerce::models.CartDelivery.limits");
        $fieldValue = $columnValue;

        $componentData = [
            'id' => 'CartDelivery-limits',
            'label' => $fieldLabel,
            'name' => $modelName,
            'currencies' => array_get($this->parameterSet, 'currencies', []),
            'values' => $fieldValue,
        ];

        return view('MinmaxEcommerce::admin.cart-delivery.form-limits-price-list', $componentData);
    }

    /**
     * @param  \Minmax\Ecommerce\Models\CartDelivery $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getShowLimitsPriceList($model)
    {
        $modelName = 'CartDelivery';
        $columnValue = $this->getModelValue($model, 'limits') ?? [];

        $fieldLabel = __("MinmaxEcommerce::models.CartDelivery.limits");
        $fieldValue = $columnValue;

        $componentData = [
            'id' => 'CartDelivery-limits',
            'label' => $fieldLabel,
            'name' => $modelName,
            'currencies' => array_get($this->parameterSet, 'currencies', []),
            'values' => $fieldValue,
        ];

        return view('MinmaxEcommerce::admin.cart-delivery.show-limits-price-list', $componentData);
    }
}
