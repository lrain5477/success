<?php

namespace Minmax\Inbox\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Inbox\Models\InboxReceived;
use Minmax\Inbox\Admin\InboxReceivedPresenter;

/**
 * Class InboxReceivedTransformer
 */
class InboxReceivedTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'inboxReceivedShow',
        'U' => 'inboxReceivedEdit',
        'D' => 'inboxReceivedDestroy',
    ];

    /**
     * Transformer constructor.
     * @param  InboxReceivedPresenter $presenter
     * @param  string $uri
     */
    public function __construct(InboxReceivedPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  InboxReceived $model
     * @return array
     * @throws \Throwable
     */
    public function transform(InboxReceived $model)
    {
        if (request('trashed', 0) == 1) {
            $this->presenter->setPermissions(array_only($this->permissions, 'R'));
        }

        return [
            'id' => $this->presenter->getGridCheckBox($model),
            'starred' => $this->presenter->getGridStarred($model),
            'subject' => $this->presenter->getGridTitle($model),
            'serial' => $this->presenter->getGridText($model, 'serial'),
            'inbox_status' => $this->presenter->getGridSelection($model, 'inbox_status'),
            'created_at' => $this->presenter->getPureString($model->created_at->format('Y-m-d')),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
