<?php

namespace Minmax\Inbox\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Inbox\Models\InboxSent;

/**
 * Class InboxSentTransformer
 */
class InboxSentTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'inboxSentShow',
        'U' => 'inboxSentEdit',
        'D' => 'inboxSentDestroy',
    ];

    /**
     * Transformer constructor.
     * @param  InboxSentPresenter $presenter
     * @param  string $uri
     */
    public function __construct(InboxSentPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  InboxSent $model
     * @return array
     * @throws \Throwable
     */
    public function transform(InboxSent $model)
    {
        return [
            'id' => $this->presenter->getGridCheckBox($model),
            'subject' => $this->presenter->getGridTitle($model),
            'admin_id' => $this->presenter->getPureString($model->admin->name),
            'created_at' => $this->presenter->getPureString($model->created_at->format('Y-m-d')),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
