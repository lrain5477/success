<?php

namespace Minmax\Inbox\Admin;

use Minmax\Base\Admin\Controller;

/**
 * Class InboxReceivedController
 */
class InboxReceivedController extends Controller
{
    protected $packagePrefix = 'MinmaxInbox::';

    public function __construct(InboxReceivedRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    protected function getQueryBuilder()
    {
        if (request('trashed', 0) == 1) {
            return $this->modelRepository->query()->onlyTrashed();
        } else {
            return $this->modelRepository->query();
        }
    }

    public function show($id)
    {
        $this->checkPermissionShow();

        $this->viewData['formData'] = $this->modelRepository->query()->withTrashed()->first() ?? abort(404);

        $this->setCustomViewDataShow();

        $this->buildBreadcrumbsShow();

        try {
            return view($this->packagePrefix . 'admin.' . $this->uri . '.show', $this->viewData);
        } catch(\Exception $e) {
            return abort(404);
        }
    }
}
