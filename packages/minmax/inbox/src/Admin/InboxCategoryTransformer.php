<?php

namespace Minmax\Inbox\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Inbox\Models\InboxCategory;

/**
 * Class InboxCategoryTransformer
 */
class InboxCategoryTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'inboxCategoryShow',
        'U' => 'inboxCategoryEdit',
        'D' => 'inboxCategoryDestroy',
    ];

    protected $menuList;

    /**
     * Transformer constructor.
     * @param  InboxCategoryPresenter $presenter
     * @param  string $uri
     */
    public function __construct(InboxCategoryPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  InboxCategory $model
     * @return array
     * @throws \Throwable
     */
    public function transform(InboxCategory $model)
    {
        if ($model->editable) {
            $this->presenter->setPermissions($this->permissions);
        } else {
            $this->presenter->setPermissions($this->permissions, ['U', 'D']);
        }

        return [
            'title' => $this->presenter->getGridText($model, 'title'),
            'amount' => $this->presenter->getPureString($model->inbox_received_count),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
