<?php

namespace Minmax\Inbox\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class InboxReceivedRequest
 */
class InboxReceivedRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('inboxReceivedEdit');
            case 'POST':
                return $this->user('admin')->can('inboxReceivedCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'InboxReceived.details' => 'required|array',
                    'InboxReceived.remarks' => 'nullable|array',
                    'InboxReceived.inbox_status' => 'required|string',
                ];
            case 'POST':
            default:
                return [
                    'InboxReceived.details' => 'required|array',
                    'InboxReceived.remarks' => 'nullable|array',
                    'InboxReceived.inbox_status' => 'required|string',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'InboxReceived.details' => __('MinmaxInbox::models.InboxReceived.details'),
            'InboxReceived.inbox_status' => __('MinmaxInbox::models.InboxReceived.inbox_status'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
