<?php

namespace Minmax\Inbox\Admin;

use Minmax\Base\Admin\Presenter;

/**
 * Class InboxCategoryPresenter
 */
class InboxCategoryPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxInbox::';

    protected $languageColumns = ['title'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'notifiable' => systemParam('notifiable'),
            'active' => systemParam('active'),
        ];
    }
}
