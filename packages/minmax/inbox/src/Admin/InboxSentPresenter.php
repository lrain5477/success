<?php

namespace Minmax\Inbox\Admin;

use Minmax\Base\Admin\Presenter;

/**
 * Class InboxSentPresenter
 */
class InboxSentPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxInbox::';

    /**
     * @param  \Minmax\Inbox\Models\InboxSent $model
     * @return string
     */
    public function getGridTitle($model)
    {
        $titleValue = $model->subject;
        $shortValue = str_replace("\n", '', trim(strip_tags($model->content)));
        $shortValue = mb_strlen($shortValue) <= 60 ? $shortValue : (mb_substr($shortValue, 0, 60) . '...');

        $urlValue = 'javascript:void(0);';
        if (in_array('R', $this->permissionSet)) {
            $urlValue = langRoute('admin.inbox-sent.show', ['id' => $model->id]);
        }

        $serialValue = $model->serial;

        $thisHtml = <<<HTML
<h3 class="h6 d-inline d-sm-block">
    <a class="text-pre-line" href="{$urlValue}">{$titleValue}</a>
</h3>
<p class="m-0 p-0"><small>{$shortValue}</small></p>
<small class="text-success float-right">{$serialValue}</small>
HTML;

        return $thisHtml;
    }
}
