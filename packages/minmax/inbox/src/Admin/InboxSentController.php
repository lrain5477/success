<?php

namespace Minmax\Inbox\Admin;

use Illuminate\Http\Request;
use Minmax\Base\Admin\Controller;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class InboxSentController
 */
class InboxSentController extends Controller
{
    protected $packagePrefix = 'MinmaxInbox::';

    public function __construct(InboxSentRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    protected function setCustomViewDataCreate()
    {
        if ($receivedModel = (new InboxReceivedRepository)->one('id', request('received'))) {
            $this->viewData['receivedModel'] = $receivedModel;

            $this->viewData['formData']->received_id = $receivedModel->id;
            $this->viewData['formData']->receiver = [
                'to' => array_get($receivedModel->details, 'email')
            ];
            $this->viewData['formData']->subject = 'Re: ' . $receivedModel->subject;
            $this->viewData['formData']->content = '<p>&nbsp;</p>' .
                '<p>------ Original Content ------</p>' .
                '<p>' . nl2br(e($receivedModel->content)) . '</p>';
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getQueryBuilder()
    {
        return $this->modelRepository->query()->with(['admin']);
    }

    /**
     * Model Store
     *
     * @param  Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->checkPermissionCreate();

        $this->checkValidate();

        $inputSet = $request->input($this->pageData->getAttribute('model'));

        $inputSet = $this->doFileUpload($inputSet, $request);

        // 儲存新建資料
        try {
            \DB::beginTransaction();

            if ($modelData = $this->modelRepository->create($inputSet)) {
                if (isset($modelData->received_id)) {
                    if ($status = $request->input('InboxReceived.inbox_status')) {
                        $receivedModel = $modelData->inboxReceived;
                        $receivedModel->inbox_status = $status;
                        $receivedModel->save();
                    }
                }

                \DB::commit();
                LogHelper::system('admin', $request->path(), $request->method(), $modelData->getKey(), $this->adminData->username, 1, __('MinmaxBase::admin.form.message.create_success'));
                return redirect(langRoute("admin.{$this->uri}.show", [$modelData->getKey()]))->with('success', __('MinmaxBase::admin.form.message.create_success'));
            }

            \DB::rollBack();
        } catch (\Exception $e) {
            \DB::rollBack();
        }

        LogHelper::system('admin', $request->path(), $request->method(), '', $this->adminData->username, 0, __('MinmaxBase::admin.form.message.create_error'));
        return redirect(langRoute("admin.{$this->uri}.create"))->withErrors([__('MinmaxBase::admin.form.message.create_error')])->withInput();
    }
}
