<?php

namespace Minmax\Inbox\Admin;

use Minmax\Base\Admin\AdminRepository;
use Minmax\Base\Admin\Presenter;

/**
 * Class InboxReceivedPresenter
 */
class InboxReceivedPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxInbox::';

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'category_id' => (new InboxCategoryRepository)->getSelectParameters(),
            'inbox_status' => systemParam('inbox_status'),
            'starred' => systemParam('starred')
        ];
    }

    /**
     * @param  \Minmax\Inbox\Models\InboxReceived $model
     * @return string
     */
    public function getGridStarred($model)
    {
        $columnValue = $model->getAttribute('starred') ?? '0';
        $value = intval($columnValue);
        $parameter = array_get($this->parameterSet, "starred.{$value}") ?? [];

        if(in_array('U',  $this->permissionSet)) {
            try {
                return view('MinmaxInbox::admin.inbox-received.grid-starred-switch', [
                    'id' => $model->getKey(),
                    'column' => 'starred',
                    'value' => $value,
                    'uri' => $this->uri,
                    'parameter' => $parameter,
                ])
                    ->render();
            } catch (\Exception $e) {
                return '';
            } catch (\Throwable $e) {
                return '';
            }
        } else {
            return '<i class="icon-star-full ' . array_get($parameter, 'options.class') . '"></i>';
        }
    }

    /**
     * @param  \Minmax\Inbox\Models\InboxReceived $model
     * @return string
     */
    public function getGridTitle($model)
    {
        $titleValue = $model->subject;
        $shortValue = str_replace("\n", '', trim(strip_tags($model->content)));
        $shortValue = mb_strlen($shortValue) <= 60 ? $shortValue : (mb_substr($shortValue, 0, 60) . '...');

        $urlValue = 'javascript:void(0);';
        if (in_array('R', $this->permissionSet)) {
            $urlValue = langRoute('admin.inbox-received.show', ['id' => $model->id]);
        }
        if (in_array('U', $this->permissionSet)) {
            $urlValue = langRoute('admin.inbox-received.edit', ['id' => $model->id]);
        }

        $nameValue = trim(strip_tags(array_get($model->details, 'name') ?? ''));

        $thisHtml = <<<HTML
<h3 class="h6 d-inline d-sm-block">
    <a class="text-pre-line" href="{$urlValue}">{$titleValue}</a>
</h3>
<p class="m-0 p-0"><small>{$shortValue}</small></p>
<small class="text-success float-right">{$nameValue}</small>
HTML;

        return $thisHtml;
    }

    /**
     * @param  \Minmax\Inbox\Models\InboxReceived $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFieldDetails($model)
    {
        $modelName = class_basename($model);

        $fieldId = "{$modelName}-details";
        $fieldName = "{$modelName}[details]";
        $fieldValue = array_except($model->details ?? [], ['name', 'email']);

        $componentData = [
            'id' => $fieldId,
            'name' => $fieldName,
            'values' => $fieldValue,
        ];

        return view('MinmaxInbox::admin.inbox-received.form-details-list', $componentData);
    }

    /**
     * @param  \Minmax\Inbox\Models\InboxReceived $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFieldRemarks($model)
    {
        $modelName = class_basename($model);

        $adminSet = (new AdminRepository)->getSelectParameters();

        $fieldId = "{$modelName}-remarks";
        $fieldName = "{$modelName}[remarks]";
        $fieldValue = collect($model->remarks)
            ->map(function ($item) use ($adminSet) {
                $item['admin_name'] = array_get($adminSet, $item['admin_id'] . '.title') ?? '-';
                return $item;
            })
            ->toArray();

        $componentData = [
            'id' => $fieldId,
            'name' => $fieldName,
            'values' => $fieldValue,
        ];

        return view('MinmaxInbox::admin.inbox-received.form-remarks-list', $componentData);
    }

    /**
     * @param  \Minmax\Inbox\Models\InboxReceived $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getShowDetails($model)
    {
        $modelName = class_basename($model);

        $fieldId = "{$modelName}-details";
        $fieldValue = array_except($model->details ?? [], ['name', 'email']);

        $componentData = [
            'id' => $fieldId,
            'values' => $fieldValue,
        ];

        return view('MinmaxInbox::admin.inbox-received.show-details-list', $componentData);
    }

    /**
     * @param  \Minmax\Inbox\Models\InboxReceived $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getShowRemarks($model)
    {
        $modelName = class_basename($model);

        $adminSet = (new AdminRepository)->getSelectParameters();

        $fieldId = "{$modelName}-remarks";
        $fieldName = "{$modelName}[remarks]";
        $fieldValue = collect($model->remarks)
            ->map(function ($item) use ($adminSet) {
                $item['admin_name'] = array_get($adminSet, $item['admin_id'] . '.title') ?? '-';
                return $item;
            })
            ->toArray();

        $componentData = [
            'id' => $fieldId,
            'name' => $fieldName,
            'values' => $fieldValue,
        ];

        return view('MinmaxInbox::admin.inbox-received.show-remarks-list', $componentData);
    }
}
