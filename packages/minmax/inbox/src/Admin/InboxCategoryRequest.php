<?php

namespace Minmax\Inbox\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class InboxCategoryRequest
 */
class InboxCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('inboxCategoryEdit');
            case 'POST':
                return $this->user('admin')->can('inboxCategoryCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'InboxCategory.title' => 'required|string',
                    'InboxCategory.notifiable' => 'required|boolean',
                    'InboxCategory.sort' => 'required|integer',
                    'InboxCategory.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'InboxCategory.title' => 'required|string',
                    'InboxCategory.notifiable' => 'required|boolean',
                    'InboxCategory.sort' => 'nullable|integer',
                    'InboxCategory.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'InboxCategory.title' => __('MinmaxInbox::models.InboxCategory.title'),
            'InboxCategory.notifiable' => __('MinmaxInbox::models.InboxCategory.notifiable'),
            'InboxCategory.sort' => __('MinmaxInbox::models.InboxCategory.sort'),
            'InboxCategory.active' => __('MinmaxInbox::models.InboxCategory.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
