<?php

namespace Minmax\Inbox\Admin;

use Minmax\Base\Admin\Presenter;
use Minmax\Base\Models\SiteDashboard;

/**
 * Class InboxDashboardPresenter
 */
class InboxDashboardPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxInbox::';

    protected $inboxReceivedRepository;

    /**
     * Presenter constructor.
     *
     * @param  InboxReceivedRepository $inboxReceivedRepository
     */
    public function __construct(InboxReceivedRepository $inboxReceivedRepository)
    {
        $this->inboxReceivedRepository = $inboxReceivedRepository;

        parent::__construct();
    }

    /**
     * @param  SiteDashboard $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function totalInboxReceived(SiteDashboard $model)
    {
        $componentData = [
            'id' => "dashboard-{$model->id}",
            'title' => $model->title,
            'position' => $model->position,
            'options' => $model->options,
            'amount' => number_format($this->inboxReceivedRepository->query()->count()),
        ];

        return view('MinmaxInbox::admin.inbox-received.dashboard-total-inbox-received', $componentData);
    }
}
