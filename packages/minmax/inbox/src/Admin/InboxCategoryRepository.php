<?php

namespace Minmax\Inbox\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Inbox\Models\InboxCategory;

/**
 * Class InboxCategoryRepository
 * @property InboxCategory $model
 * @method InboxCategory find($id)
 * @method InboxCategory one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method InboxCategory[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method InboxCategory create($attributes)
 * @method InboxCategory save($model, $attributes)
 * @method InboxCategory|\Illuminate\Database\Eloquent\Builder query()
 */
class InboxCategoryRepository extends Repository
{
    const MODEL = InboxCategory::class;

    protected $sort = 'sort';

    protected $sorting = true;

    protected $languageColumns = ['title'];

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'inbox_category';
    }

    protected function beforeCreate()
    {
        if (array_key_exists('bcc', $this->attributes)) {
            $this->attributes['bcc'] = isset($this->attributes['bcc']) ? explode(',', $this->attributes['bcc']) : [];
        }
    }

    protected function beforeSave()
    {
        if (array_key_exists('bcc', $this->attributes)) {
            $this->attributes['bcc'] = isset($this->attributes['bcc']) ? explode(',', $this->attributes['bcc']) : [];
        }
    }

    public function getSelectParameters()
    {
        return $this->all()
            ->mapWithKeys(function ($item) {
                /** @var InboxCategory $item */
                return [$item->id => ['title' => $item->title, 'options' => $item->options]];
            })
            ->toArray();
    }
}
