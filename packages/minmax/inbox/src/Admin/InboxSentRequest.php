<?php

namespace Minmax\Inbox\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class InboxSentRequest
 */
class InboxSentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('inboxSentEdit');
            case 'POST':
                return $this->user('admin')->can('inboxSentCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
            default:
                return [
                    'InboxSent.received_id' => 'nullable|exists:inbox_received,id',
                    'InboxSent.receiver' => 'required|array',
                    'InboxSent.subject' => 'required|string',
                    'InboxSent.content' => 'required|string',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'InboxSent.received_id' => __('MinmaxInbox::models.InboxSent.received_id'),
            'InboxSent.receiver' => __('MinmaxInbox::models.InboxSent.receiver'),
            'InboxSent.subject' => __('MinmaxInbox::models.InboxSent.subject'),
            'InboxSent.content' => __('MinmaxInbox::models.InboxSent.content'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
