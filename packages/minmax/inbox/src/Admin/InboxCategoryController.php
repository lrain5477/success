<?php

namespace Minmax\Inbox\Admin;

use Illuminate\Http\Request;
use Minmax\Base\Admin\Controller;

/**
 * Class InboxCategoryController
 */
class InboxCategoryController extends Controller
{
    protected $packagePrefix = 'MinmaxInbox::';

    public function __construct(InboxCategoryRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    protected function getQueryBuilder()
    {
        return $this->modelRepository->query()->withCount('inboxReceived');
    }

    /**
     * Set datatable filter.
     *
     * @param  mixed $datatable
     * @param  Request $request
     * @return mixed
     */
    protected function doDatatableFilter($datatable, $request)
    {
        $datatable->filter(function($query) use ($request) {
            /** @var \Illuminate\Database\Query\Builder $query */

            if($request->has('filter')) {
                $query->where(function ($query) use ($request) {
                    /** @var \Illuminate\Database\Query\Builder $query */

                    foreach ($request->input('filter', []) as $column => $value) {
                        if (empty($column) || is_null($value) || $value === '') continue;

                        if ($column == 'title') {
                            try {
                                $filterDisplayName = collect(cache('langMap.' . app()->getLocale() . '.inbox_category', []))
                                    ->filter(function ($item, $key) use ($value) {
                                        return preg_match('/^inbox_category\.title\./', $key) > 0 && strpos($item, $value) !== false;
                                    })
                                    ->keys()
                                    ->toArray();
                                $query->orWhereIn($column, $filterDisplayName);
                            } catch (\Exception $e) {
                            }
                            continue;
                        }

                        $query->orWhere($column, 'like', "%{$value}%");
                    }
                });
            }

            if($request->has('equal')) {
                foreach($request->input('equal', []) as $column => $value) {
                    if (empty($column) || is_null($value) || $value === '') continue;

                    $query->where($column, $value);
                }
            }
        });

        return $datatable;
    }
}
