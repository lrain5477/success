<?php

namespace Minmax\Inbox\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Inbox\Mails\DefaultMailer;
use Minmax\Inbox\Models\InboxSent;

/**
 * Class InboxSentRepository
 * @property InboxSent $model
 * @method InboxSent find($id)
 * @method InboxSent one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method InboxSent[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method InboxSent create($attributes)
 * @method InboxSent save($model, $attributes)
 * @method InboxSent|\Illuminate\Database\Eloquent\Builder query()
 */
class InboxSentRepository extends Repository
{
    const MODEL = InboxSent::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'inbox_sent';
    }

    protected function beforeCreate()
    {
        $receiver = array_map(function ($emails) {
                $emailSet = [];
                foreach (explode(';', $emails) as $email) {
                    if (isset($email) && filter_var(trim($email), FILTER_VALIDATE_EMAIL)) {
                        $emailSet[] = trim($email);
                    }
                }
                return $emailSet;
            }, array_get($this->attributes, 'receiver') ?? []);
        $this->attributes['receiver'] = $receiver;

        if ($serialData = \DB::table('inbox_serial')->where('date', date('Y-m-d'))->first()) {
            \DB::table('inbox_serial')->where('date', date('Y-m-d'))->increment('serial');
            $serialNumber = $serialData->serial + 1;
        } else {
            \DB::table('inbox_serial')->insert(['date' => date('Y-m-d'), 'serial' => 1]);
            $serialNumber = 1;
        }
        $this->attributes['serial'] = 'R' . date('ymd') . str_pad($serialNumber, 4, '0', STR_PAD_LEFT);
        $this->attributes['admin_id'] = request()->user('admin')->id;
        $this->attributes['ip'] = request()->ip();
    }

    protected function afterCreate()
    {
        // Send email
        try {
            $mailable = new DefaultMailer($this->model);

            if (config('mail.driver') == 'minmaxproxy') {
                sendProxyMail(
                    array_get($this->model->receiver, 'to') ?? [],
                    $mailable,
                    '',
                    array_get($this->model->receiver, 'cc') ?? [],
                    array_get($this->model->receiver, 'bcc') ?? []);
            } else {
                \Mail::to(array_get($this->model->receiver, 'to') ?? [])
                    ->cc(array_get($this->model->receiver, 'cc') ?? [])
                    ->bcc(array_get($this->model->receiver, 'bcc') ?? [])
                    ->send($mailable);
            }
        } catch (\Exception $e) {}
    }
}
