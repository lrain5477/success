<?php

namespace Minmax\Inbox\Events;

use Minmax\Base\Events\BaseEvent;
use Minmax\Inbox\Models\InboxReceived;
use Minmax\Notify\Events\EmailNotifyDefault;

/**
 * Class Received
 */
class Received extends BaseEvent
{
    use EmailNotifyDefault;

    protected $notifyCode = 'contact-received';

    /**
     * Received constructor.
     *
     * @param  InboxReceived $inbox
     * @return void
     */
    public function __construct(InboxReceived $inbox)
    {
        $this->mailerParameters[] = $inbox;

        $this->emails = array_get($inbox->details, 'email');

        parent::__construct();
    }
}
