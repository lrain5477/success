<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class CreateInboxTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 收信類別
        Schema::create('inbox_category', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('code')->unique()->nullable()->comment('類別代碼');
            $table->string('title')->comment('類別名稱');
            $table->boolean('notifiable')->default(true)->comment('信件通知');
            $table->json('bcc')->nullable()->comment('附加收件人');
            $table->json('options')->nullable()->comment('類別設定');
            $table->boolean('editable')->default(true)->comment('可否編輯');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // 收信夾
        Schema::create('inbox_received', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('category_id')->comment('收信類別ID');
            $table->string('serial')->nullable()->comment('信件單號');
            $table->string('subject')->comment('主旨');
            $table->longText('content')->comment('內容');
            $table->json('details')->nullable()->comment('信件細節');
            $table->json('remarks')->nullable()->comment('備註事項');
            $table->ipAddress('ip')->nullable()->comment('IP');
            $table->string('inbox_status')->nullable()->comment('信件狀態');
            $table->boolean('starred')->default(false)->comment('關注狀態');
            $table->timestamps();
            $table->softDeletes();
        });

        // 寄件備份
        Schema::create('inbox_sent', function (Blueprint $table) {
            $table->increments('id');
            $table->string('received_id')->nullable()->comment('收信郵件ID');
            $table->string('serial')->nullable()->comment('信件單號');
            $table->string('admin_id')->comment('管理員ID');
            $table->json('receiver')->comment('收件者');                       // {to, cc, bcc}
            $table->string('subject')->comment('主旨');
            $table->longText('content')->comment('內容');
            $table->ipAddress('ip')->nullable()->comment('IP');
            $table->timestamps();
            $table->softDeletes();
        });

        // 信件流水號
        Schema::create('inbox_serial', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->unique()->comment('日期');
            $table->unsignedInteger('serial')->default(0)->comment('信件流水號');
        });

        // 建立系統參數資料
        $this->insertSystemParameters();

        // 建立儀錶板項目
        $this->insertDashboard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 刪除系統參數資料
        $this->deleteSystemParameters();

        Schema::dropIfExists('inbox_serial');
        Schema::dropIfExists('inbox_sent');
        Schema::dropIfExists('inbox_received');
        Schema::dropIfExists('inbox_category');
    }

    /**
     * Insert system parameters for this module.
     *
     * @return void
     */
    public function insertSystemParameters()
    {
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $startGroupId = $rowGroupId = SeederHelper::getTableNextIncrement('system_parameter_group');
        $rowGroupId--;
        $systemGroupData = [
            ['code' => 'inbox_status', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
        ];

        DB::table('system_parameter_group')->insert($systemGroupData);

        // 多語系
        $systemGroupLanguage = [
            'zh-Hant' => [
                ['title' => '信件狀態']
            ],
            'zh-Hans' => [
                ['title' => '信件状态']
            ],
            'ja' => [
                ['title' => 'レター状態']
            ],
            'en' => [
                ['title' => 'Email Status']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'system_parameter_group', $systemGroupLanguage, $languageList, $startGroupId, false);


        $startItemId = $rowItemId = SeederHelper::getTableNextIncrement('system_parameter_item');
        $rowItemId--;

        $systemItemData = [
            [
                'group_id' => $startGroupId,
                'value' => 'new',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'danger']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId,
                'value' => 'processing',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'warning']),
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId,
                'value' => 'completed',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 3,
            ],
        ];

        DB::table('system_parameter_item')->insert($systemItemData);

        // 多語系
        $systemItemLanguage = [
            'zh-Hant' => [
                ['label' => '待處理'], ['label' => '處理中'], ['label' => '已結案']
            ],
            'zh-Hans' => [
                ['label' => '待处理'], ['label' => '处理中'], ['label' => '已结案']
            ],
            'ja' => [
                ['label' => '待っている'], ['label' => '処理中'], ['label' => '完了した']
            ],
            'en' => [
                ['label' => 'Waiting'], ['label' => 'Processing'], ['label' => 'Completed']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'system_parameter_item', $systemItemLanguage, $languageList, $startItemId, false);


        DB::table('language_resource')->insert($languageResourceData);
    }

    public function insertDashboard()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        // SiteDashboard
        $startDashboardId = $rowDashboardId = SeederHelper::getTableNextIncrement('site_dashboard');
        $rowDashboardId--;
        $siteDashboardData = [
            [
                'guard' => 'admin',
                'title' => 'site_dashboard.title.' . ++$rowDashboardId,
                'presenter' => '\Minmax\Inbox\Admin\InboxDashboardPresenter@totalInboxReceived',
                'position' => json_encode([
                    'row' => '2',
                    'column' => '2',
                    'width' => '3',
                    'height' => '0'
                ]),
                'options' => null,
                'sort' => 6,
                'active' => true,
                'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'guard' => 'administrator',
                'title' => 'site_dashboard.title.' . ++$rowDashboardId,
                'presenter' => '\Minmax\Inbox\Administrator\InboxDashboardPresenter@totalInboxReceived',
                'position' => json_encode([
                    'row' => '2',
                    'column' => '2',
                    'width' => '3',
                    'height' => '0'
                ]),
                'options' => null,
                'sort' => 6,
                'active' => true,
                'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];
        DB::table('site_dashboard')->insert($siteDashboardData);

        $siteDashboardLanguage = [
            'zh-Hant' => [
                ['title' => '客服信函'], ['title' => '客服信函']
            ],
            'zh-Hans' => [
                ['title' => '客服信函'], ['title' => '客服信函']
            ],
            'ja' => [
                ['title' => '受信箱'], ['title' => '受信箱']
            ],
            'en' => [
                ['title' => 'Service Inbox'], ['title' => 'Service Inbox']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'site_dashboard', $siteDashboardLanguage, $languageList, $startDashboardId, false);

        DB::table('language_resource')->insert($languageResourceData);
    }

    /**
     * Delete system parameters for this module.
     *
     * @return void
     */
    public function deleteSystemParameters()
    {
        SeederHelper::deleteSystemParametersByGroupCode([
            'inbox_status'
        ]);
    }
}
