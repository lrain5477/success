<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class InsertAdminMenuInboxData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 建立預設資料
        $this->insertDatabase();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 刪除預設資料
        $this->deleteDatabase();
    }

    /**
     * Insert default data
     *
     * @return void
     */
    public function insertDatabase()
    {
        $timestamp = date('Y-m-d H:i:s');

        // 建立權限物件
        $permissionsData = [];
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'inboxReceived', '收信匣', 291));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'inboxSent', '寄件備份', ['C', 'R', 'D'], 292));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'inboxCategory', '信件類別管理', 293));
        DB::table('permissions')->insert($permissionsData);

        // 管理員選單
        if ($menuClassId = DB::table('admin_menu')->where('uri', 'root-module')->value('id')) {
            $adminMenuData = [
                [
                    'id' => $menuParentId = uuidl(),
                    'title' => '客服中心',
                    'uri' => 'control-inbox',
                    'controller' => null,
                    'model' => null,
                    'parent_id' => $menuClassId,
                    'link' => null,
                    'icon' => 'icon-man',
                    'permission_key' => null,
                    'sort' => 209, 'created_at' => $timestamp, 'updated_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '收信匣',
                    'uri' => 'inbox-received',
                    'controller' => 'InboxReceivedController',
                    'model' => 'InboxReceived',
                    'parent_id' => $menuParentId,
                    'link' => 'inbox-received',
                    'icon' => null,
                    'permission_key' => 'inboxReceivedShow',
                    'sort' => 1, 'created_at' => $timestamp, 'updated_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '編寫郵件',
                    'uri' => 'inbox-send',
                    'controller' => null,
                    'model' => null,
                    'parent_id' => $menuParentId,
                    'link' => url('siteadmin/inbox-sent/create'),
                    'icon' => null,
                    'permission_key' => 'inboxSentCreate',
                    'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '寄件備份',
                    'uri' => 'inbox-sent',
                    'controller' => 'InboxSentController',
                    'model' => 'InboxSent',
                    'parent_id' => $menuParentId,
                    'link' => 'inbox-sent',
                    'icon' => null,
                    'permission_key' => 'inboxSentShow',
                    'sort' => 3, 'created_at' => $timestamp, 'updated_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '類別管理',
                    'uri' => 'inbox-category',
                    'controller' => 'InboxCategoryController',
                    'model' => 'InboxCategory',
                    'parent_id' => $menuParentId,
                    'link' => 'inbox-category',
                    'icon' => null,
                    'permission_key' => 'inboxCategoryShow',
                    'sort' => 4, 'created_at' => $timestamp, 'updated_at' => $timestamp
                ],
            ];
            DB::table('admin_menu')->insert($adminMenuData);
        }
    }

    public function deleteDatabase()
    {
        $uriSet = ['control-inbox', 'inbox-received', 'inbox-sent', 'inbox-category'];

        DB::table('admin_menu')->whereIn('uri', $uriSet)->delete();

        $permissionSet = ['inboxReceived', 'inboxSent', 'inboxCategory'];

        DB::table('permissions')->whereIn('group', $permissionSet)->delete();
    }
}
