<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertAdministratorMenuInboxData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 建立預設資料
        $this->insertDatabase();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 刪除預設資料
        $this->deleteDatabase();
    }

    /**
     * Insert default data
     *
     * @return void
     */
    public function insertDatabase()
    {
        $timestamp = date('Y-m-d H:i:s');

        // 管理員選單 - 分類
        if ($menuClassId = DB::table('administrator_menu')->where('uri', 'root-module')->value('id')) {
            $administratorMenuData = [
                [
                    'id' => $menuParentId = uuidl(),
                    'parent_id' => $menuClassId,
                    'title' => '客服中心',
                    'uri' => 'control-inbox',
                    'controller' => null,
                    'model' => null,
                    'link' => null,
                    'icon' => 'icon-man',
                    'sort' => 209, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'parent_id' => $menuParentId,
                    'title' => '類別管理',
                    'uri' => 'inbox-category',
                    'controller' => 'InboxCategoryController',
                    'model' => 'InboxCategory',
                    'link' => 'inbox-category',
                    'icon' => null,
                    'sort' => 4, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
            ];
            DB::table('administrator_menu')->insert($administratorMenuData);
        }
    }

    public function deleteDatabase()
    {
        $uriSet = ['control-inbox', 'inbox-category'];

        DB::table('administrator_menu')->whereIn('uri', $uriSet)->delete();
    }
}
