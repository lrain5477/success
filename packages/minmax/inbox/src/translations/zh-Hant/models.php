<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Models (Database Column) Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in backend platform page.
    |
    */

    'InboxCategory' => [
        'id' => 'ID',
        'code' => '類別代碼',
        'title' => '類別名稱',
        'notifiable' => '信件通知',
        'bcc' => '附加收件人',
        'options' => '類別設定',
        'editable' => '可否編輯',
        'sort' => '排序',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'code' => '類別代碼為唯一值，供前臺可透過該代碼取得資料。',
            'notifiable' => '當接收到這個類別的信件時傳送通知信件。',
            'bcc' => '除系統通知設定之收件者外，可另外設定其他 Email。請使用半形逗號 <code>,</code> 分隔多個信箱。',
        ],
        'amount' => '信件數',
    ],

    'InboxReceived' => [
        'id' => 'ID',
        'category_id' => '收信類別',
        'serial' => '信件單號',
        'subject' => '主旨',
        'content' => '內容',
        'details' => '信件細節',
        'remarks' => '備註事項',
        'ip' => 'IP',
        'inbox_status' => '信件狀態',
        'starred' => '關注狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'deleted_at' => '刪除時間',
        'hint' => [
            'inbox_status' => '可於此同時更新原信件之狀態。',
        ],
    ],

    'InboxSent' => [
        'id' => 'ID',
        'received_id' => '收信郵件',
        'serial' => '信件單號',
        'admin_id' => '寄件管理員',
        'receiver' => [
            'to' => '收件者 (to)',
            'cc' => '副本 (cc)',
            'bcc' => '密件副本 (bcc)',
        ],
        'subject' => '主旨',
        'content' => '內容',
        'ip' => 'IP',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'deleted_at' => '刪除時間',
        'hint' => [
            'receiver' => [
                'bcc' => '多組收件人以上請使用半形分號 <code>;</code> 區隔。',
            ],
        ],
    ],

];
