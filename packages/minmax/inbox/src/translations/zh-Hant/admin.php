<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    'grid' => [
        'send_reply' => '立即回覆',
        'send_new' => '撰寫新郵件',
        'inbox_normal' => '一般信件',
        'inbox_trashed' => '垃圾桶',
    ],

    'form' => [
        'email_content' => '信件內容',
        'email_manage' => '信件管理',
        'received_source' => '來源信件',

        'header' => [
            'title' => '信件管理'
        ],

        'fieldSet' => [
            'detail' => '詳細資料',
            'remark' => '備註事項',
            'receiver' => '收件人資訊',
            'content' => '信件內容設定',
        ],

        'InboxReceived' => [
            'sender' => '寄件者',
            'details' => [
                'key' => '資料名稱',
                'value' => '內容',
                'action' => '操作',
            ],
            'remarks' => [
                'content' => '內容紀錄',
                'admin_id' => '人員',
                'created_at' => '日期',
                'action' => '操作',
            ],
        ],

        'InboxSent' => [
            'receiver' => [
                'to' => '收件者',
                'cc' => '副本',
                'bcc' => '密件副本',
            ],
            'change_status' => '更新原信件狀態',
        ],
    ],

];
