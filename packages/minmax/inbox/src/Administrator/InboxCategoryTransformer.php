<?php

namespace Minmax\Inbox\Administrator;

use Minmax\Base\Administrator\Transformer;
use Minmax\Inbox\Models\InboxCategory;

/**
 * Class InboxCategoryTransformer
 */
class InboxCategoryTransformer extends Transformer
{
    /**
     * Transformer constructor.
     * @param  InboxCategoryPresenter $presenter
     * @param  string $uri
     */
    public function __construct(InboxCategoryPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  InboxCategory $model
     * @return array
     * @throws \Throwable
     */
    public function transform(InboxCategory $model)
    {
        return [
            'title' => $this->presenter->getGridText($model, 'title'),
            'amount' => $this->presenter->getPureString($model->inbox_received_count),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'editable' => $this->presenter->getGridSwitch($model, 'editable'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
