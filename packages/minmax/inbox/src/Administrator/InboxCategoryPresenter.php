<?php

namespace Minmax\Inbox\Administrator;

use Minmax\Base\Administrator\Presenter;

/**
 * Class InboxCategoryPresenter
 */
class InboxCategoryPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxInbox::';

    protected $languageColumns = ['title'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'notifiable' => systemParam('notifiable'),
            'editable' => systemParam('editable'),
            'active' => systemParam('active'),
        ];
    }
}
