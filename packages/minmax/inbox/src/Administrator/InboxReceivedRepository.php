<?php

namespace Minmax\Inbox\Administrator;

use Minmax\Base\Administrator\Repository;
use Minmax\Inbox\Models\InboxReceived;

/**
 * Class InboxReceivedRepository
 * @property InboxReceived $model
 * @method InboxReceived find($id)
 * @method InboxReceived one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method InboxReceived[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method InboxReceived create($attributes)
 * @method InboxReceived save($model, $attributes)
 * @method InboxReceived|\Illuminate\Database\Eloquent\Builder query()
 */
class InboxReceivedRepository extends Repository
{
    const MODEL = InboxReceived::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'inbox_received';
    }
}
