<?php
/**
 * @var string $id
 * @var array $values
 */
?>
<table class="table table-sm table-bordered mb-1" id="{{ $id }}">
    <thead class="thead-default">
    <tr class="text-center table-secondary">
        <th>@lang('MinmaxInbox::admin.form.InboxReceived.details.key')</th>
        <th>@lang('MinmaxInbox::admin.form.InboxReceived.details.value')</th>
    </tr>
    </thead>
    <tbody>
    @foreach($values as $key => $value)
    <tr>
        <td style="width:20%">{{ $key }}</td>
        <td>{{ $value }}</td>
    </tr>
    @endforeach
    </tbody>
</table>
