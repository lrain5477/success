<?php
/**
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Inbox\Models\InboxReceived $formData
 */
?>

@extends('MinmaxBase::admin.layouts.site')

@section('breadcrumbs', Breadcrumbs::view('MinmaxBase::admin.layouts.breadcrumbs', 'edit'))

@section('content')
<section class="panel panel-default">
    <header class="panel-heading">
        <h1 class="h5 float-left font-weight-bold">{{ $formData->serial }} @lang('MinmaxInbox::admin.form.email_content')</h1>

        @component('MinmaxBase::admin.layouts.right-links', ['languageActive' => $languageActive])
            @if($adminData->can('inboxReceivedShow'))
            <a class="btn btn-sm btn-light" href="{{ langRoute("admin.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::admin.form.back_list')">
                <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.back_list')</span>
            </a>
            @endif
            @if($adminData->can('inboxSentCreate'))
            <a class="btn btn-sm ml-1" href="{{ langRoute("admin.inbox-sent.create", ['received' => $formData->id]) }}" title="Re: {{ $formData->subject }}">
                <i class="icon-edit"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxInbox::admin.grid.send_reply')</span>
            </a>
            @endif
        @endcomponent
    </header>

    <div class="panel-wrapper">
        <div class="panel-body">
            <h2>{{ $formData->inboxCategory->title }}</h2>
            <h3>{{ $formData->subject }}</h3>
            <hr />
            <p class="mb-0">@lang('MinmaxInbox::admin.form.InboxReceived.sender')："<strong class="text-primary">{{ array_get($formData->details, 'name') }}</strong>"
                @if (array_get($formData->details, 'email'))
                &lt;<span class="text-success pl-1 pr-1">{{ array_get($formData->details, 'email') }}</span>&gt;
                @endif
                on<span class="text-muted ml-2">{{ $formData->created_at->format('Y-m-d H:i:s') }}</span></p>
            <hr />
            <div class="inbox-view">
                <p>{!! str_replace("\n", "</p>\n<p>", e($formData->content)) !!}</p>
            </div>
        </div>
    </div>
</section>

<section class="panel panel-default">
    <header class="panel-heading">
        <h1 class="h5 float-left font-weight-bold">{{ $pageData->title }} @lang('MinmaxInbox::admin.form.header.title')</h1>
    </header>

    <div class="panel-wrapper">
        <div class="panel-body">
            <form id="editForm" class="form-horizontal validate editForm" name="editForm"
                  action="{{ langRoute("admin.{$pageData->uri}.update", ['id' => $formData->id]) }}"
                  method="post"
                  enctype="multipart/form-data">
                @method('PUT')
                @csrf

                @inject('modelPresenter', 'Minmax\Inbox\Admin\InboxReceivedPresenter')

                <fieldset>
                    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxInbox::admin.form.fieldSet.detail')</legend>

                    <input type="hidden" name="InboxReceived[details][name]" value="{{ array_get($formData->details, 'name') }}" />

                    @if (array_get($formData->details, 'email'))
                    <input type="hidden" name="InboxReceived[details][email]" value="{{ array_get($formData->details, 'email') }}" />
                    @endif

                    {!! $modelPresenter->getFieldDetails($formData) !!}

                </fieldset>

                <fieldset class="mt-4">
                    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxInbox::admin.form.fieldSet.remark')</legend>

                    {!! $modelPresenter->getFieldRemarks($formData) !!}

                </fieldset>

                <fieldset class="mt-4">
                    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.advanced')</legend>

                    {!! $modelPresenter->getShowNormalText($formData, 'created_at') !!}

                    {!! $modelPresenter->getShowNormalText($formData, 'updated_at') !!}

                    {!! $modelPresenter->getShowNormalText($formData, 'ip') !!}

                    {!! $modelPresenter->getFieldRadio($formData, 'inbox_status', ['required' => true, 'inline' => true]) !!}

                </fieldset>

                <div class="text-center my-4 form-btn-group">
                    <input class="btn btn-main" type="submit" id="submitBut" value="@lang('MinmaxBase::admin.form.button.send')">
                    <input class="btn btn-default" type="reset" value="@lang('MinmaxBase::admin.form.button.reset')" onclick="window.location.reload(true)">
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
