<?php
/**
 * @var string $id
 * @var string $name
 * @var array $values
 */
?>
<table class="table table-sm table-bordered mb-1" id="{{ $id }}">
    <thead class="thead-default">
    <tr class="text-center table-secondary">
        <th>@lang('MinmaxInbox::admin.form.InboxReceived.details.key')</th>
        <th>@lang('MinmaxInbox::admin.form.InboxReceived.details.value')</th>
        <th>@lang('MinmaxInbox::admin.form.InboxReceived.details.action')</th>
    </tr>
    </thead>
    <tbody>
    @foreach($values as $key => $value)
    <tr>
        <td style="width:20%">{{ $key }}</td>
        <td>{{ $value }}</td>
        <td class="text-center" style="width:10%">
            <input type="hidden" name="{{ $name }}[{{ $key }}]" value="{{ $value }}" />
            <button class="btn btn-danger btn-sm repeat-remove" type="button"><i class="icon-cross"></i></button>
        </td>
    </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td style="width:20%">
            <input class="form-control form-control-sm repeat-key" type="text" value="" />
        </td>
        <td>
            <input class="form-control form-control-sm repeat-value" type="text" value="" />
        </td>
        <td class="text-center" style="width:10%">
            <input type="hidden" class="repeat-default" name="{{ $name }}[name]" value="" {{ count($values) > 0 ? 'disabled' : '' }} />
            <button class="btn btn-primary btn-sm repeat-add" type="button"><i class="icon-plus"></i></button>
        </td>
    </tr>
    </tfoot>
</table>

<template id="template-{{ $id }}">
    <tr>
        <td style="width:20%">#DummyKey#</td>
        <td>#DummyValue#</td>
        <td class="text-center" style="width:10%">
            <input type="hidden" name="{{ $name }}[#DummyKey#]" value="#DummyValue#" />
            <button class="btn btn-danger btn-sm repeat-remove" type="button"><i class="icon-cross"></i></button>
        </td>
    </tr>
</template>

@push('scripts')
<script>
(function ($) {
    $(function () {
        $('#{{ $id }}')
            .on('click', '.repeat-add', function () {
                var $repeater = $(this).parents('tr');
                var newKey = $('.repeat-key', $repeater).val();
                var newValue = $('.repeat-value', $repeater).val();
                if (newKey !== '' && newValue !== '') {
                    var newLine = $('#template-{{ $id }}').html()
                        .replace(/#DummyKey#/g, newKey)
                        .replace(/#DummyValue#/g, newValue);
                    $('#{{ $id }} tbody').append(newLine);
                    $('.repeat-key', $repeater).val('');
                    $('.repeat-value', $repeater).val('');
                    $('#{{ $id }} .repeat-default').prop('disabled', true);
                } else {
                    alert("Key and Value can not be empty!");
                }
            })
            .on('click', '.repeat-remove', function () {
                $(this).parents('tr').remove();
                if ($('#{{ $id }} tbody tr').length < 1) {
                    $('#{{ $id }} .repeat-default').prop('disabled', false);
                }
            });
    });
})(jQuery);
</script>
@endpush
