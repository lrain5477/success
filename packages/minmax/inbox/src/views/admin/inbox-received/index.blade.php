<?php
/**
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 */
?>

@extends('MinmaxBase::admin.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links')
        @if(request('trashed', 0) == 0)
            <a class="btn btn-sm btn-main" href="{{ langRoute("admin.{$pageData->uri}.index", ['trashed' => 1]) }}" title="@lang('MinmaxInbox::admin.grid.inbox_trashed')">
                <i class="icon-trash2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxInbox::admin.grid.inbox_trashed')</span>
            </a>
            @slot('batchActions')
            <button class="dropdown-item" type="button" onclick="multiDelete('{{ langRoute("admin.{$pageData->uri}.ajaxMultiDestroy") }}')"><i class="icon-trashcan mr-2 text-muted"></i>刪除</button>
            @endslot
        @else
            <a class="btn btn-sm btn-main" href="{{ langRoute("admin.{$pageData->uri}.index") }}" title="@lang('MinmaxInbox::admin.grid.inbox_normal')">
                <i class="icon-markunread"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxInbox::admin.grid.inbox_normal')</span>
            </a>
        @endif
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Inbox\Admin\InboxReceivedPresenter')

@section('grid-filter')
    @component('MinmaxBase::admin.layouts.grid.filter-keyword')
    <option value="subject">@lang('MinmaxInbox::models.InboxReceived.subject')</option>
    <option value="serial">@lang('MinmaxInbox::models.InboxReceived.serial')</option>
    @endcomponent

    @component('MinmaxBase::admin.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('category_id', 'searchCategory', ['emptyLabel' => __('MinmaxInbox::models.InboxReceived.category_id')]) !!}
    {!! $modelPresenter->getFilterSelection('starred', 'searchStarred', ['emptyLabel' => __('MinmaxInbox::models.InboxReceived.starred')]) !!}
    {!! $modelPresenter->getFilterSelection('inbox_status', 'searchStatus', ['emptyLabel' => __('MinmaxInbox::models.InboxReceived.inbox_status')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="nosort w-3">
            <div class="custom-control custom-checkbox">
                <input class="custom-control-input group-checkable" type="checkbox" aria-label="Select" data-set="#tableList .checkboxes input" id="checkAll" />
                <label class="custom-control-label" for="checkAll"></label>
            </div>
        </th>
        <th class="w-3"></th>
        <th class="w-50">@lang('MinmaxInbox::models.InboxReceived.subject')</th>
        <th>@lang('MinmaxInbox::models.InboxReceived.serial')</th>
        <th>@lang('MinmaxInbox::models.InboxReceived.inbox_status')</th>
        <th>@lang('MinmaxInbox::models.InboxReceived.created_at')</th>
        <th class="nosort">@lang('MinmaxBase::admin.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    $('#tableList')
        .on('click', '.starred-switch', function(e) {
            e.preventDefault();
            var $this = $(this);
            var url = $this.attr('data-url'), status = parseInt($this.attr('data-value')), id = $this.attr('data-id'), column = $this.attr('data-column');
            var switchTo = status === 1 ? 0 : 1;

            $.ajax({
                url: url, type: 'PATCH', dataType:'json',
                data: {id:id,column:column,oriValue:status,switchTo:switchTo},
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function() {
                    $this.attr('data-value', switchTo);
                    if (switchTo === 1) {
                        $this.removeClass('text-main').addClass('text-muted');
                    } else {
                        $this.removeClass('text-muted').addClass('text-main');
                    }
                },
                complete:function(){$('#tableList').DataTable().draw(false);}
            });
            return false;
        });
    datatableInit(
        [
            {data: 'id', name: 'id'},
            {data: 'starred', name: 'starred'},
            {data: 'subject', name: 'subject'},
            {data: 'serial', name: 'serial'},
            {data: 'inbox_status', name: 'inbox_status'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action'}
        ],
        ['title', 'serial'],
        {"category_id":"searchCategory", "starred":"searchStarred", "inbox_status":"searchStatus"},
        [[3, 'desc']],
        '{{ langRoute("admin.{$pageData->uri}.ajaxDataTable", ['trashed' => request('trashed')]) }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("admin.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
