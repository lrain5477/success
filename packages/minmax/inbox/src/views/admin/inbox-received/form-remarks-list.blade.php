<?php
/**
 * @var string $id
 * @var string $name
 * @var array $values
 */
?>
<table class="table table-sm table-bordered mb-1" id="{{ $id }}">
    <thead class="thead-default">
    <tr class="text-center table-secondary">
        <th style="width:50%">@lang('MinmaxInbox::admin.form.InboxReceived.remarks.content')</th>
        <th>@lang('MinmaxInbox::admin.form.InboxReceived.remarks.admin_id')</th>
        <th>@lang('MinmaxInbox::admin.form.InboxReceived.remarks.created_at')</th>
        <th style="width:10%">@lang('MinmaxInbox::admin.form.InboxReceived.remarks.action')</th>
    </tr>
    </thead>
    <tbody class="text-center">
    @foreach($values as $remark)
    <tr>
        <td class="text-left">
            <input type="hidden" name="{{ $name }}[{{ $loop->index }}][content]" data-column="content" value="{{ array_get($remark, 'content') }}" />
            {{ array_get($remark, 'content') }}
        </td>
        <td>
            <input type="hidden" name="{{ $name }}[{{ $loop->index }}][admin_id]" data-column="admin_id" value="{{ array_get($remark, 'admin_id') }}" />
            {{ array_get($remark, 'admin_name') }}
        </td>
        <td>
            <input type="hidden" name="{{ $name }}[{{ $loop->index }}][created_at]" data-column="created_at" value="{{ array_get($remark, 'created_at') }}" />
            {{ array_get($remark, 'created_at') }}
        </td>
        <td>
            <button class="btn btn-danger btn-sm repeat-remove" type="button"><i class="icon-cross"></i></button>
        </td>
    </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="3">
            <input type="hidden" class="repeat-default" name="{{ $name }}" value="" {{ count($values) > 0 ? 'disabled' : '' }} />
            <input class="form-control form-control-sm repeat-content" type="text" value="" />
        </td>
        <td class="text-center">
            <button class="btn btn-primary btn-sm repeat-add" type="button"><i class="icon-plus"></i></button>
        </td>
    </tr>
    </tfoot>
</table>

<template id="template-{{ $id }}">
    <tr>
        <td class="text-left">
            <input type="hidden" name="{{ $name }}[#DummyIndex#][content]" data-column="content" value="#DummyContent#" />
            #DummyContent#
        </td>
        <td>
            <input type="hidden" name="{{ $name }}[#DummyIndex#][admin_id]" data-column="admin_id" value="#DummyAdminId#" />
            #DummyAdminName#
        </td>
        <td>
            <input type="hidden" name="{{ $name }}[#DummyIndex#][created_at]" data-column="created_at" value="#DummyCreatedAt#" />
            #DummyCreatedAt#
        </td>
        <td>
            <button class="btn btn-danger btn-sm repeat-remove" type="button"><i class="icon-cross"></i></button>
        </td>
    </tr>
</template>

@push('scripts')
<script>
(function ($) {
    $(function () {
        $('#{{ $id }}')
            .on('click', '.repeat-add', function () {
                var $repeater = $(this).parents('tr');
                var newValue = $('.repeat-content', $repeater).val();
                var rowLength = $('#{{ $id }} tbody tr').length;
                var nowDate = new Date();
                var timestamp = nowDate.getFullYear() + '-' + (nowDate.getMonth() + 1) + '-' + nowDate.getDate() + ' '
                    + ('0' + nowDate.getHours()).substr(-2) + ':'
                    + ('0' + nowDate.getMinutes()).substr(-2) + ':'
                    + ('0' + nowDate.getSeconds()).substr(-2);
                if (newValue !== '') {
                    var newLine = $('#template-{{ $id }}').html()
                        .replace(/#DummyIndex#/g, rowLength)
                        .replace(/#DummyContent#/g, newValue)
                        .replace(/#DummyAdminId#/g, '{{ request()->user('admin')->id }}')
                        .replace(/#DummyAdminName#/g, '{{ request()->user('admin')->name }} ({{ request()->user('admin')->username }})')
                        .replace(/#DummyCreatedAt#/g, timestamp);
                    $('#{{ $id }} tbody').append(newLine);
                    $('.repeat-content', $repeater).val('');
                    $('#{{ $id }} .repeat-default').prop('disabled', true);
                } else {
                    alert("Remark can not be empty!");
                }
            })
            .on('click', '.repeat-remove', function () {
                $(this).parents('tr').remove();
                $('#{{ $id }} tbody tr').each(function () {
                    var index = $(this).index();
                    $('input', $(this)).each(function () {
                        var $this = $(this);
                        var column = $this.attr('data-column');
                        $this.attr('name', '{{ $name }}[' + index + '][' + column + ']');
                    });
                });
                if ($('#{{ $id }} tbody tr').length < 1) {
                    $('#{{ $id }} .repeat-default').prop('disabled', false);
                }
            });
    });
})(jQuery);
</script>
@endpush
