<?php
/**
 * @var string $id
 * @var string $name
 * @var array $values
 */
?>
<table class="table table-sm table-bordered mb-1" id="{{ $id }}">
    <thead class="thead-default">
    <tr class="text-center table-secondary">
        <th style="width:50%">@lang('MinmaxInbox::admin.form.InboxReceived.remarks.content')</th>
        <th>@lang('MinmaxInbox::admin.form.InboxReceived.remarks.admin_id')</th>
        <th>@lang('MinmaxInbox::admin.form.InboxReceived.remarks.created_at')</th>
    </tr>
    </thead>
    <tbody class="text-center">
    @foreach($values as $remark)
    <tr>
        <td class="text-left">
            {{ array_get($remark, 'content') }}
        </td>
        <td>
            {{ array_get($remark, 'admin_name') }}
        </td>
        <td>
            {{ array_get($remark, 'created_at') }}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
