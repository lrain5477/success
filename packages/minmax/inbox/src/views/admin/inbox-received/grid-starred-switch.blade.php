<?php
/**
 * @var string $id
 * @var string $column
 * @var integer $value it's 0 or 1
 * @var string $uri
 * @var array $parameter
 */
?>
<a class="icon-star-full {{ array_get($parameter, 'options.class', 'text-muted') }} starred-switch" href="#"
   title="@lang('MinmaxBase::admin.grid.click_to_switch')"
   style="text-decoration: none"
   data-url="{{ langRoute("admin.{$uri}.ajaxSwitch") }}"
   data-column="{{ $column }}"
   data-value="{{ $value }}"
   data-id="{{ $id }}">
</a>
