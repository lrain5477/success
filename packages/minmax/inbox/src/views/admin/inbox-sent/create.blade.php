<?php
/**
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Inbox\Models\InboxReceived $receivedModel
 * @var \Minmax\Inbox\Models\InboxSent $formData
 */
?>

@extends('MinmaxBase::admin.layouts.page.create')

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links', ['languageActive' => $languageActive])
        @if($adminData->can('inboxSentShow'))
        <a class="btn btn-sm btn-light" href="{{ langRoute("admin.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::admin.form.back_list')">
            <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.back_list')</span>
        </a>
        @endif
        @if(isset($receivedModel) && $adminData->can('inboxReceivedShow'))
        <a class="btn btn-sm btn-main" href="{{ langRoute("admin.inbox-received.show", [$receivedModel->id]) }}" title="@lang('MinmaxInbox::admin.form.received_source')">
            <i class="icon-markunread"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxInbox::admin.form.received_source')</span>
        </a>
        @endif
    @endcomponent
@endsection

@section('forms')
    @inject('modelPresenter', 'Minmax\Inbox\Admin\InboxSentPresenter')

    <fieldset id="baseFieldSet">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxInbox::admin.form.fieldSet.receiver')</legend>

        <input type="hidden" name="InboxSent[received_id]" value="{{ old('InboxSent.received_id', isset($receivedModel) ? $receivedModel->id : '') }}" />

        {!! $modelPresenter->getFieldText($formData, 'receiver', ['subColumn' => 'to', 'required' => true]) !!}

        {!! $modelPresenter->getFieldText($formData, 'receiver', ['subColumn' => 'cc']) !!}

        {!! $modelPresenter->getFieldText($formData, 'receiver', ['subColumn' => 'bcc', 'hint' => true]) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxInbox::admin.form.fieldSet.content')</legend>

        {!! $modelPresenter->getFieldText($formData, 'subject', ['required' => true]) !!}

        {!! $modelPresenter->getFieldEditor($formData, 'content', ['height' => '550px']) !!}


    </fieldset>

    @isset($receivedModel)
    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.advanced')</legend>

        @inject('receivedPresenter', 'Minmax\Inbox\Admin\InboxReceivedPresenter')

        {!! $receivedPresenter->getFieldRadio($formData->inboxReceived, 'inbox_status', ['inline' => true, 'required' => true, 'hint' => true]) !!}

    </fieldset>
    @endisset

    <div class="text-center my-4 form-btn-group">
        <input class="btn btn-main" type="submit" id="submitBut" value="@lang('MinmaxBase::admin.form.button.send')">
        <input class="btn btn-default" type="reset" value="@lang('MinmaxBase::admin.form.button.reset')" onclick="window.location.reload(true)">
    </div>
@endsection
