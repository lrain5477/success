<?php
/**
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Inbox\Models\InboxCategory $parentModel
 */
?>

@extends('MinmaxBase::admin.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links')
        @if($adminData->can('inboxSentCreate'))
        <a class="btn btn-sm ml-1" href="{{ langRoute("admin.{$pageData->uri}.create") }}" title="@lang('MinmaxInbox::admin.grid.send_new')">
            <i class="icon-edit"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxInbox::admin.grid.send_new')</span>
        </a>
        @endif
        @slot('batchActions')
        <button class="dropdown-item" type="button" onclick="multiDelete('{{ langRoute("admin.{$pageData->uri}.ajaxMultiDestroy") }}')"><i class="icon-trashcan mr-2 text-muted"></i>刪除</button>
        @endslot
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Inbox\Admin\InboxSentPresenter')

@section('grid-filter')
    @component('MinmaxBase::admin.layouts.grid.filter-keyword')
    <option value="subject">@lang('MinmaxInbox::models.InboxSent.subject')</option>
    <option value="serial">@lang('MinmaxInbox::models.InboxSent.serial')</option>
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="nosort w-3">
            <div class="custom-control custom-checkbox">
                <input class="custom-control-input group-checkable" type="checkbox" aria-label="Select" data-set="#tableList .checkboxes input" id="checkAll" />
                <label class="custom-control-label" for="checkAll"></label>
            </div>
        </th>
        <th class="w-50">@lang('MinmaxInbox::models.InboxSent.subject')</th>
        <th>@lang('MinmaxInbox::models.InboxSent.admin_id')</th>
        <th>@lang('MinmaxInbox::models.InboxSent.created_at')</th>
        <th class="nosort">@lang('MinmaxBase::admin.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'id', name: 'id'},
            {data: 'subject', name: 'subject'},
            {data: 'admin_id', name: 'admin_id'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action'}
        ],
        ['title', 'serial'],
        {},
        [[3, 'desc']],
        '{{ langRoute("admin.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("admin.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
