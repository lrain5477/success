<?php
/**
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Inbox\Models\InboxSent $formData
 */
?>

@extends('MinmaxBase::admin.layouts.site')

@section('breadcrumbs', Breadcrumbs::view('MinmaxBase::admin.layouts.breadcrumbs', 'show'))

@section('content')
<section class="panel panel-default">
    <header class="panel-heading">
        <h1 class="h5 float-left font-weight-bold">{{ $formData->serial }} @lang('MinmaxInbox::admin.form.email_content')</h1>

        @component('MinmaxBase::admin.layouts.right-links', ['languageActive' => $languageActive])
            @if($adminData->can('inboxSentShow'))
            <a class="btn btn-sm btn-light" href="{{ langRoute("admin.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::admin.form.back_list')">
                <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.back_list')</span>
            </a>
            @endif
            @if(isset($formData->inboxReceived) && $adminData->can('inboxReceivedShow'))
            <a class="btn btn-sm btn-main" href="{{ langRoute("admin.inbox-received.show", [$formData->received_id]) }}" title="{{ $formData->inboxReceived->subject }}">
                <i class="icon-markunread"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxInbox::admin.form.received_source')</span>
            </a>
            @endif
        @endcomponent
    </header>

    <div class="panel-wrapper">
        <div class="panel-body">
            <h3>{{ $formData->subject }}</h3>
            <hr />
            @if(count(array_get($formData->receiver, 'to') ?? []) > 0)
            <p class="mb-0">@lang('MinmaxInbox::admin.form.InboxSent.receiver.to')：
                @foreach(array_get($formData->receiver, 'to') as $email)
                &lt;<span class="text-success pl-1 pr-1">{{ $email }}</span>&gt;{{ !$loop->last ? ', ' : '' }}
                @endforeach
            </p>
            @endif
            @if(count(array_get($formData->receiver, 'cc') ?? []) > 0)
            <p class="mb-0">@lang('MinmaxInbox::admin.form.InboxSent.receiver.cc')：
                @foreach(array_get($formData->receiver, 'cc') as $email)
                &lt;<span class="text-success pl-1 pr-1">{{ $email }}</span>&gt;{{ !$loop->last ? ', ' : '' }}
                @endforeach
            </p>
            @endif
            @if(count(array_get($formData->receiver, 'bcc') ?? []) > 0)
            <p class="mb-0">@lang('MinmaxInbox::admin.form.InboxSent.receiver.bcc')：
                @foreach(array_get($formData->receiver, 'bcc') as $email)
                &lt;<span class="text-success pl-1 pr-1">{{ $email }}</span>&gt;{{ !$loop->last ? ', ' : '' }}
                @endforeach
            </p>
            @endif
            <p class="mb-0">on<span class="text-muted ml-2">{{ $formData->created_at->format('Y-m-d H:i:s') }}</span></p>
            <hr />
            <div class="inbox-view">
                {!! $formData->content !!}
            </div>
        </div>
    </div>
</section>

<section class="panel panel-default">
    <header class="panel-heading">
        <h1 class="h5 float-left font-weight-bold">{{ $pageData->title }} @lang('MinmaxInbox::admin.form.header.title')</h1>
    </header>

    <div class="panel-wrapper">
        <div class="panel-body">
            @inject('modelPresenter', 'Minmax\Inbox\Admin\InboxSentPresenter')

            <fieldset>
                <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.advanced')</legend>

                {!! $modelPresenter->getShowNormalText($formData, 'admin_id', ['defaultValue' => $formData->admin->name . '(' . $formData->admin->username . ')']) !!}

                {!! $modelPresenter->getShowNormalText($formData, 'created_at') !!}

                {!! $modelPresenter->getShowNormalText($formData, 'updated_at') !!}

                {!! $modelPresenter->getShowNormalText($formData, 'ip') !!}

            </fieldset>

        </div>
    </div>
</section>
@endsection
