<?php
/**
 * @var string $id
 * @var string $title
 * @var array $position
 * @var array $options
 * @var integer $amount
 */
?>
<section class="panel p-3 bg-warning text-white">
    <div class="row">
        <div class="col"><span id="{{ $id }}" class="d-block mb-1 h3">{{ $amount }}</span><span class="d-block">{{ $title }}</span></div>
        <div class="col"><span class="{{ array_get($options, 'icon', 'icon-mail-envelope-open') }} display-4"></span></div>
    </div>
</section>
