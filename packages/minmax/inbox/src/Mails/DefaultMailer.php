<?php

namespace Minmax\Inbox\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Minmax\Inbox\Models\InboxSent;

class DefaultMailer extends Mailable
{
    use Queueable, SerializesModels;

    protected $inboxSent;

    /**
     * Create a new message instance.
     *
     * @param  InboxSent $inboxSent
     * @return void
     */
    public function __construct(InboxSent $inboxSent)
    {
        $this->inboxSent = $inboxSent;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject($this->inboxSent->subject)
            ->html($this->inboxSent->content);
    }
}
