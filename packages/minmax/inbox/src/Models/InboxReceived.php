<?php

namespace Minmax\Inbox\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class InboxReceived
 * @property string $id
 * @property string $category_id
 * @property string $serial
 * @property string $subject
 * @property string $content
 * @property array $details
 * @property array $remarks
 * @property string $ip
 * @property string $inbox_status
 * @property boolean $starred
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \Illuminate\Support\Carbon $deleted_at
 * @property \Minmax\Inbox\Models\InboxCategory $inboxCategory
 */
class InboxReceived extends Model
{
    use SoftDeletes;

    protected $table = 'inbox_received';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'details' => 'array',
        'remarks' => 'array',
        'starred' => 'boolean',
    ];

    public $incrementing = false;

    public function inboxCategory()
    {
        return $this->belongsTo(InboxCategory::class, 'category_id', 'id');
    }
}
