<?php

namespace Minmax\Inbox\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Minmax\Base\Models\Admin;

/**
 * Class InboxSent
 * @property integer $id
 * @property string $received_id
 * @property string $serial
 * @property string $admin_id
 * @property array $receiver
 * @property string $subject
 * @property string $content
 * @property string $ip
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \Illuminate\Support\Carbon $deleted_at
 * @property \Minmax\Base\Models\Admin $admin
 * @property \Minmax\Inbox\Models\InboxReceived $inboxReceived
 */
class InboxSent extends Model
{
    use SoftDeletes;

    protected $table = 'inbox_sent';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'receiver' => 'array',
    ];

    public function admin()
    {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }

    public function inboxReceived()
    {
        return $this->belongsTo(InboxReceived::class, 'received_id', 'id');
    }
}
