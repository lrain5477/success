<?php

namespace Minmax\Inbox\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class InboxCategory
 * @property string $id
 * @property string $code
 * @property string $title
 * @property boolean $notifiable
 * @property array $bcc
 * @property array $options
 * @property boolean $editable
 * @property integer $sort
 * @property boolean $active
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \Illuminate\Database\Eloquent\Collection|InboxReceived[] $inboxReceived
 * @property integer $inbox_received_count
 */
class InboxCategory extends Model
{
    protected $table = 'inbox_category';
    protected $guarded = [];
    protected $casts = [
        'notifiable' => 'boolean',
        'bcc' => 'array',
        'options' => 'array',
        'editable' => 'boolean',
        'active' => 'boolean',
    ];

    public $incrementing = false;

    public function getTitleAttribute()
    {
        return langDB($this->getAttributeFromArray('title'));
    }

    public function inboxReceived()
    {
        return $this->hasMany(InboxReceived::class, 'category_id', 'id');
    }
}
