<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['admin', 'localizationRedirect'],
    'as' => 'admin.' . app()->getLocale() . '.'
], function() {

    Route::group(['prefix' => 'siteadmin', 'namespace' => 'Minmax\Inbox\Admin', 'middleware' => 'auth:admin'], function () {
        /*
         |--------------------------------------------------------------------------
         | 需要登入的路由。
         |--------------------------------------------------------------------------
         */

        /*
         * InboxReceived 收信匣
         */
        Route::get('inbox-received', 'InboxReceivedController@index')->name('inbox-received.index');
        Route::get('inbox-received/{id}', 'InboxReceivedController@show')->name('inbox-received.show');
        Route::put('inbox-received/{id}', 'InboxReceivedController@update')->name('inbox-received.update');
        Route::delete('inbox-received/{id}', 'InboxReceivedController@destroy')->name('inbox-received.destroy');
        Route::get('inbox-received/{id}/edit', 'InboxReceivedController@edit')->name('inbox-received.edit');
        Route::post('inbox-received/ajax/datatables', 'InboxReceivedController@ajaxDataTable')->name('inbox-received.ajaxDataTable');
        Route::patch('inbox-received/ajax/switch', 'InboxReceivedController@ajaxSwitch')->name('inbox-received.ajaxSwitch');
        Route::delete('inbox-received/ajax/multi/delete', 'InboxReceivedController@ajaxMultiDestroy')->name('inbox-received.ajaxMultiDestroy');

        /*
         * InboxSent 寄件備份
         */
        Route::get('inbox-sent', 'InboxSentController@index')->name('inbox-sent.index');
        Route::post('inbox-sent', 'InboxSentController@store')->name('inbox-sent.store');
        Route::get('inbox-sent/create', 'InboxSentController@create')->name('inbox-sent.create');
        Route::get('inbox-sent/{id}', 'InboxSentController@show')->name('inbox-sent.show');
        Route::delete('inbox-sent/{id}', 'InboxSentController@destroy')->name('inbox-sent.destroy');
        Route::post('inbox-sent/ajax/datatables', 'InboxSentController@ajaxDataTable')->name('inbox-sent.ajaxDataTable');
        Route::patch('inbox-sent/ajax/switch', 'InboxSentController@ajaxSwitch')->name('inbox-sent.ajaxSwitch');
        Route::patch('inbox-sent/ajax/sort', 'InboxSentController@ajaxSort')->name('inbox-sent.ajaxSort');
        Route::delete('inbox-sent/ajax/multi/delete', 'InboxSentController@ajaxMultiDestroy')->name('inbox-sent.ajaxMultiDestroy');

        /*
         * InboxCategory 類別管理
         */
        Route::get('inbox-category', 'InboxCategoryController@index')->name('inbox-category.index');
        Route::post('inbox-category', 'InboxCategoryController@store')->name('inbox-category.store');
        Route::get('inbox-category/create', 'InboxCategoryController@create')->name('inbox-category.create');
        Route::get('inbox-category/{id}', 'InboxCategoryController@show')->name('inbox-category.show');
        Route::put('inbox-category/{id}', 'InboxCategoryController@update')->name('inbox-category.update');
        Route::delete('inbox-category/{id}', 'InboxCategoryController@destroy')->name('inbox-category.destroy');
        Route::get('inbox-category/{id}/edit', 'InboxCategoryController@edit')->name('inbox-category.edit');
        Route::post('inbox-category/ajax/datatables', 'InboxCategoryController@ajaxDataTable')->name('inbox-category.ajaxDataTable');
        Route::patch('inbox-category/ajax/switch', 'InboxCategoryController@ajaxSwitch')->name('inbox-category.ajaxSwitch');
        Route::patch('inbox-category/ajax/sort', 'InboxCategoryController@ajaxSort')->name('inbox-category.ajaxSort');

    });

});
