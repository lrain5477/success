<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['administrator', 'localizationRedirect'],
    'as' => 'administrator.' . app()->getLocale() . '.'
], function() {

    Route::group(['prefix' => 'administrator', 'namespace' => 'Minmax\Inbox\Administrator', 'middleware' => 'auth:administrator'], function () {
        /*
         |--------------------------------------------------------------------------
         | 需要登入的路由。
         |--------------------------------------------------------------------------
         */

        /*
         * InboxCategory 類別管理
         */
        Route::get('inbox-category', 'InboxCategoryController@index')->name('inbox-category.index');
        Route::post('inbox-category', 'InboxCategoryController@store')->name('inbox-category.store');
        Route::get('inbox-category/create', 'InboxCategoryController@create')->name('inbox-category.create');
        Route::get('inbox-category/{id}', 'InboxCategoryController@show')->name('inbox-category.show');
        Route::put('inbox-category/{id}', 'InboxCategoryController@update')->name('inbox-category.update');
        Route::delete('inbox-category/{id}', 'InboxCategoryController@destroy')->name('inbox-category.destroy');
        Route::get('inbox-category/{id}/edit', 'InboxCategoryController@edit')->name('inbox-category.edit');
        Route::post('inbox-category/ajax/datatables', 'InboxCategoryController@ajaxDataTable')->name('inbox-category.ajaxDataTable');
        Route::patch('inbox-category/ajax/switch', 'InboxCategoryController@ajaxSwitch')->name('inbox-category.ajaxSwitch');
        Route::patch('inbox-category/ajax/sort', 'InboxCategoryController@ajaxSort')->name('inbox-category.ajaxSort');

    });

});
