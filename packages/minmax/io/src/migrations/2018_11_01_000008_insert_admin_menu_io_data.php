<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class InsertAdminMenuIoData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 建立預設資料
        $this->insertDatabase();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 刪除預設資料
        $this->deleteDatabase();
    }

    /**
     * Insert default data
     *
     * @return void
     */
    public function insertDatabase()
    {
        $timestamp = date('Y-m-d H:i:s');

        // 建立權限物件
        $permissionsData = [
            [
                'guard' => 'admin', 'group' => 'ioData',
                'name' => 'ioDataShow', 'label' => '瀏覽', 'display_name' => '資料匯入匯出 [瀏覽]', 'description' => '資料匯入匯出 [瀏覽]',
                'sort' => 372, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'guard' => 'admin', 'group' => 'ioData',
                'name' => 'ioDataImport', 'label' => '匯入', 'display_name' => '資料匯入匯出 [匯入]', 'description' => '資料匯入匯出 [匯入]',
                'sort' => 372, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'guard' => 'admin', 'group' => 'ioData',
                'name' => 'ioDataExport', 'label' => '匯出', 'display_name' => '資料匯入匯出 [匯出]', 'description' => '資料匯入匯出 [匯出]',
                'sort' => 372, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];
        DB::table('permissions')->insert($permissionsData);

        // 管理員選單
        if ($menuParentId = DB::table('admin_menu')->where('uri', 'control-integration')->value('id')) {
            $adminMenuData = [
                [
                    'id' => uuidl(),
                    'title' => '資料匯入匯出',
                    'uri' => 'io-data',
                    'controller' => 'IoConstructController',
                    'model' => 'IoConstruct',
                    'parent_id' => $menuParentId,
                    'link' => 'io-data',
                    'icon' => null,
                    'permission_key' => 'ioDataShow',
                    'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
            ];
            DB::table('admin_menu')->insert($adminMenuData);
        }
    }

    public function deleteDatabase()
    {
        $uriSet = ['io-data'];

        DB::table('admin_menu')->whereIn('uri', $uriSet)->delete();

        $permissionSet = ['ioData'];

        DB::table('permissions')->whereIn('group', $permissionSet)->delete();
    }
}
