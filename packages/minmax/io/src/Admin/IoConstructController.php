<?php

namespace Minmax\Io\Admin;

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Minmax\Base\Admin\Controller;

/**
 * Class IoConstructController
 */
class IoConstructController extends Controller
{
    protected $packagePrefix = 'MinmaxIo::';

    public function __construct(IoConstructRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    /**
     * @throws \DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException
     */
    protected function buildBreadcrumbsConfig()
    {
        Breadcrumbs::register('config', function ($breadcrumbs) {
            /** @var \DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $breadcrumbs */
            $breadcrumbs->parent('admin.home');
            $breadcrumbs->push($this->pageData->title, langRoute("admin.{$this->uri}.index"));
            $breadcrumbs->push(__('MinmaxIo::admin.form.config'));
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getQueryBuilder()
    {
        $permissions = $this->adminData->allPermissions()->pluck('name')->toArray();

        return $this->modelRepository->query()
            ->where(function ($query) use ($permissions) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->whereNull('import_permission')->orWhereIn('import_permission', $permissions);
            })
            ->orWhere(function ($query) use ($permissions) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->whereNull('export_permission')->orWhereIn('export_permission', $permissions);
            });
    }

    /**
     * Set datatable filter.
     *
     * @param  mixed $datatable
     * @param  Request $request
     * @return mixed
     */
    protected function doDatatableFilter($datatable, $request)
    {
        $datatable->filter(function($query) use ($request) {
            /** @var \Illuminate\Database\Query\Builder $query */

            if($request->has('filter')) {
                $query->where(function ($query) use ($request) {
                    /** @var \Illuminate\Database\Query\Builder $query */

                    foreach ($request->input('filter', []) as $column => $value) {
                        if (empty($column) || is_null($value) || $value === '') continue;

                        if ($column == 'title') {
                            try {
                                $filterDisplayName = collect(cache('langMap.' . app()->getLocale() . '.io_construct', []))
                                    ->filter(function ($item, $key) use ($value) {
                                        return preg_match('/^io_construct\.title\./', $key) > 0 && strpos($item, $value) !== false;
                                    })
                                    ->keys()
                                    ->toArray();
                                $query->orWhereIn($column, $filterDisplayName);
                            } catch (\Exception $e) {
                            }
                            continue;
                        }

                        $query->orWhere($column, 'like', "%{$value}%");
                    }
                });
            }

            if($request->has('equal')) {
                foreach($request->input('equal', []) as $column => $value) {
                    if (empty($column) || is_null($value) || $value === '') continue;

                    $query->where($column, $value);
                }
            }
        });

        return $datatable;
    }

    /**
     * @param  integer $id
     * @return \Illuminate\Http\Response
     * @throws \DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException
     */
    public function config($id)
    {
        $this->viewData['formData'] = $this->modelRepository->find($id) ?? abort(404);

        $this->buildBreadcrumbsConfig();

        return view($this->packagePrefix . 'admin.io-data.config', $this->viewData);
    }

    /**
     * @param  integer $id
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function example($id)
    {
        if ($model = $this->modelRepository->find($id)) {
            if ($model->example == 'controller') {
                try {
                    return app($model->controller, ['guard' => 'admin'])->example($id);
                } catch (\InvalidArgumentException $e) {
                    return abort(404);
                } catch (\Exception $e) {
                    return abort(404);
                }
            }

            if (Storage::exists($model->example)) {
                return Storage::response($model->example, null, [], 'attachment');
            }
        }

        return abort(404);
    }

    /**
     * @param  Request $request
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request, $id)
    {
        if ($model = $this->modelRepository->find($id)) {
            try {
                return app($model->controller, ['guard' => 'admin'])->import($request, $id);
            } catch (\InvalidArgumentException $e) {
                return redirect(langRoute("admin.io-data.config", ['id' => $id]))
                    ->withErrors([__('MinmaxIo::admin.form.message.import_error', ['title' => $model->title])]);
            } catch (\Exception $e) {
                return redirect(langRoute("admin.io-data.config", ['id' => $id]))
                    ->withErrors([__('MinmaxIo::admin.form.message.import_error', ['title' => $model->title])]);
            }
        }

        return abort(404);
    }

    /**
     * @param  Request $request
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request, $id)
    {
        if ($model = $this->modelRepository->find($id)) {
            try {
                set_time_limit(0);
                return app($model->controller, ['guard' => 'admin'])->export($request, $id);
            } catch (\InvalidArgumentException $e) {
                return redirect(langRoute("admin.io-data.config", ['id' => $id]))
                    ->withErrors([__('MinmaxIo::admin.form.message.export_error', ['title' => $model->title])]);
            } catch (\Exception $e) {
                return redirect(langRoute("admin.io-data.config", ['id' => $id]))
                    ->withErrors([__('MinmaxIo::admin.form.message.import_error', ['title' => $model->title])]);
            }
        }

        return abort(404);
    }

    /**
     * @param  \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet
     * @param  array $range eq: [1, 1, 6, 6]
     * @param  array $styles
     * @return \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet
     */
    protected function setSheetStyle($sheet, $range, $styles = [])
    {
        $styles = empty($styles)
            ? [
                'font' => [
                    'size' => 10,
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['rgb' => '888888'],
                    ]
                ],
            ]
            : $styles;

        $sheet->getStyleByColumnAndRow(...$range)->applyFromArray($styles);

        return $sheet;
    }

    /**
     * @param  integer $id
     * @param  integer $record
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function report($id, $record)
    {
        if ($model = $this->modelRepository->find($id)) {
            if ($recordModel = $model->ioRecords()->where(['id' => $record, 'type' => 'import'])->first()) {
                /** @var \Minmax\Io\Models\IoRecord $recordModel */
                $filename = $model->title . ' (Report-' . $recordModel->created_at->format('YmdHis') . ')';

                $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

                // Use sheet 0
                $sheet = $spreadsheet->getSheet(0);
                $sheet->setTitle('record');

                $titleColumnIndex = 0;
                $titleRowIndex = 1;
                $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, 'Row', 's')
                    ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(8);
                $sheet->setCellValueExplicitByColumnAndRow(++$titleColumnIndex, $titleRowIndex, 'Status', 's')
                    ->getColumnDimensionByColumn($titleColumnIndex)->setWidth(25);

                $dataColumnIndex = 0;
                $dataRowIndex = 1;
                foreach ($recordModel->errors ?? [] as $rowData) {
                    /** @var array $rowData */
                    $dataRowIndex++;
                    $dataColumnIndex = 0;
                    $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, array_get($rowData, 'row'), 's');
                    $sheet->setCellValueExplicitByColumnAndRow(++$dataColumnIndex, $dataRowIndex, array_get($rowData, 'message'), 's');
                }

                // Set sheet style
                $this->setSheetStyle($sheet, [1, 1, $dataColumnIndex < 1 ? $titleColumnIndex : $dataColumnIndex, $dataRowIndex]);
                $this->setSheetStyle($sheet, [1, 1, $titleColumnIndex, $titleRowIndex], [
                    'font' => ['bold' => true],
                    'fill' => ['fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'startColor' => ['rgb' => 'EFEFEF']]
                ]);

                // 寫入檔案並輸出
                $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

                $response = response()->streamDownload(
                    function () use ($writer) { $writer->save('php://output'); },
                    "{$filename}.xlsx",
                    ['Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'Cache-Control' => 'max-age=0']
                );

                return $response;
            }
        }

        return abort(404);
    }

    /**
     * @param  integer $id
     * @param  integer $record
     * @return \Illuminate\Http\Response
     */
    public function download($id, $record)
    {
        if ($model = $this->modelRepository->find($id)) {
            if ($recordModel = $model->ioRecords()->where(['id' => $record, 'type' => 'export', 'result' => true])->first()) {
                /** @var \Minmax\Io\Models\IoRecord $recordModel */
                $filePath = storage_path("app/admin/export/{$recordModel->file}");
                if (file_exists($filePath)) {
                    return response()->download($filePath);
                }
            }
        }

        return abort(404);
    }
}
