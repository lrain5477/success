<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    'grid' => [
        'actions' => [
            'config' => '匯入匯出',
            'example' => '範例下載',
        ],
    ],

    'form' => [
        'config' => '設置',
        'import' => '匯入',
        'export' => '匯出',
        'download' => '下載 (僅保留最後3項)',
        'get_report' => '下載結果報告',
        'fieldSet' => [
            'import_record' => '匯入紀錄',
            'export_record' => '匯出紀錄',
        ],
        'message' => [
            'import_error' => ':title 匯入失敗',
            'import_success' => ':title 匯入成功',
            'export_error' => ':title 匯出失敗',
            'export_success' => ':title 匯出成功',
        ],
        'alert' => [
            'exporting_title' => '匯出中',
            'exporting_text' => '系統正在匯出您所請求的資料，請勿關閉視窗或操作其他功能',
            'export_success_title' => '匯出成功',
            'export_success_text' => '您所請求的資料已經匯出成功',
            'export_error_title' => '匯出失敗',
            'export_error_text' => '您所請求的資料匯出失敗',
        ],
    ],

];
