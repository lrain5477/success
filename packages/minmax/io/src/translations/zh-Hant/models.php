<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Models (Database Column) Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in backend platform page.
    |
    */

    'IoConstruct' => [
        'id' => 'ID',
        'title' => '資料處理名稱',
        'uri' => 'Uri',
        'import_enable' => '匯入啟用',
        'export_enable' => '匯出啟用',
        'import_permission' => '匯入權限',
        'export_permission' => '匯出權限',
        'import_view' => '匯入視圖',
        'export_view' => '匯出視圖',
        'controller' => 'Controller',
        'example' => '範例檔案',
        'filename' => '檔案名稱',
        'sort' => '排序',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'filename' => '此名稱為匯出後的檔案名稱，請輸入包含副檔名的檔案名稱。',
        ],
    ],

    'IoRecord' => [
        'id' => 'ID',
        'title' => '資料處理名稱',
        'uri' => 'Uri',
        'type' => '資料處理類型',
        'errors' => '錯誤紀錄',
        'total' => '資料數',
        'success' => '成功數量',
        'result' => '操作結果',
        'file' => '檔案路徑',
        'created_at' => '建立時間',
    ],

];
