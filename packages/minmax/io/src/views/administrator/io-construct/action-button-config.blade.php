<?php
/**
 * @var string $uri
 * @var string $id
 */
?>
<a class="btn btn-outline-default btn-sm" role="button" aria-pressed="true" title="@lang('MinmaxIo::administrator.grid.actions.config')" href="{{ langRoute("administrator.{$uri}.config", [$id]) }}">
    <i class="icon-loop"></i><span class="text-hide">@lang('MinmaxIo::administrator.grid.actions.config')</span>
</a>