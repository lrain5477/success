<?php
/**
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 * @var \Minmax\Io\Models\IoConstruct $formData
 */
?>

@extends('MinmaxBase::administrator.layouts.site')

@section('breadcrumbs', Breadcrumbs::view('MinmaxBase::administrator.layouts.breadcrumbs', 'config'))

@section('content')
    <section class="panel panel-default">
        <header class="panel-heading">
            <h1 class="h5 float-left font-weight-bold">{{ $pageData->title }} @lang('MinmaxIo::administrator.form.config')</h1>

            <div class="float-right">
                <a class="btn btn-sm btn-light" href="{{ langRoute("administrator.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::administrator.grid.back')">
                    <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.grid.back')</span>
                </a>
            </div>
        </header>

        <div class="panel-wrapper">
            <div class="panel-body">
                <header class="mb-4">
                    <ul class="nav nav-tabs" id="ioTab" role="tablist">
                        @if($formData->import_enable)
                        <li class="nav-item">
                            <a class="nav-link active"
                               id="tab-import" data-toggle="tab"
                               href="#tab-pane-import" role="tab"
                               aria-controls="tab-pane-import" aria-selected="true">@lang('MinmaxIo::administrator.form.import')</a>
                        </li>
                        @endif
                        @if($formData->export_enable)
                        <li class="nav-item">
                            <a class="nav-link {{ $formData->import_enable ? '' : 'active' }}"
                               id="tab-export" data-toggle="tab"
                               href="#tab-pane-export" role="tab"
                               aria-controls="tab-pane-export" aria-selected="true">@lang('MinmaxIo::administrator.form.export')</a>
                        </li>
                        @endif
                    </ul>
                </header>
                <div class="tab-content" id="myTabContent">
                    @if($formData->import_enable && ! is_null($formData->import_view))
                    <div class="tab-pane fade show active" id="tab-pane-import" role="tabpanel" aria-labelledby="tab-import">
                        @include($formData->import_view)

                        @if($formData->ioRecords()->where('type', 'import')->count() > 0)
                        <fieldset class="mt-4">
                            <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-3"></i>@lang('MinmaxIo::admin.form.fieldSet.import_record')</legend>
                            <div class="table-responsive">
                                <table class="table table-sm table-bordered mb-1">
                                    <thead class="thead-default">
                                    <tr class="text-center">
                                        <th class="w-20">@lang('MinmaxIo::models.IoRecord.created_at')</th>
                                        <th class="w-10">@lang('MinmaxIo::models.IoRecord.result')</th>
                                        <th class="w-10">@lang('MinmaxIo::models.IoRecord.total')</th>
                                        <th class="w-10">@lang('MinmaxIo::models.IoRecord.success')</th>
                                        <th>@lang('MinmaxIo::administrator.form.get_report')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($formData->ioRecords()->where('type', 'export')->orderByDesc('id')->limit(10)->get() as $exportRecord)
                                    <tr class="text-center">
                                        <td>{{ $exportRecord->created_at }}</td>
                                        <td>{{ systemParam('result.' . intval($exportRecord->result) . '.title') }}</td>
                                        <td>{{ $exportRecord->total }}</td>
                                        <td>{{ $exportRecord->success }}</td>
                                        <td class="text-left">
                                            @if($exportRecord->success < $exportRecord->total)
                                                <a href="{{ langRoute('administrator.io-construct.report', ['id' => $formData->id, 'record' => $exportRecord->id]) }}" target="_blank"><i class="icon-cloud-download"></i></a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </fieldset>
                        @endif
                    </div>
                    @endif
                    @if($formData->export_enable && ! is_null($formData->export_view))
                    <div class="tab-pane fade {{ $formData->import_enable ? '' : 'show active' }}" id="tab-pane-export" role="tabpanel" aria-labelledby="tab-export">
                        @include($formData->export_view)

                        @if($formData->ioRecords()->where('type', 'export')->count() > 0)
                        <fieldset class="mt-4">
                            <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-3"></i>@lang('MinmaxIo::administrator.form.fieldSet.export_record')</legend>
                            <div class="table-responsive">
                                <table class="table table-sm table-bordered mb-1">
                                    <thead class="thead-default">
                                    <tr class="text-center">
                                        <th class="w-20">@lang('MinmaxIo::models.IoRecord.created_at')</th>
                                        <th class="w-10">@lang('MinmaxIo::models.IoRecord.result')</th>
                                        <th class="w-10">@lang('MinmaxIo::models.IoRecord.success')</th>
                                        <th>@lang('MinmaxIo::administrator.form.download')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($formData->ioRecords()->where('type', 'export')->orderByDesc('id')->limit(10)->get() as $exportRecord)
                                    <tr class="text-center">
                                        <td>{{ $exportRecord->created_at }}</td>
                                        <td>{{ systemParam('result.' . intval($exportRecord->result) . '.title') }}</td>
                                        <td class="text-right">{{ $exportRecord->success }}</td>
                                        <td class="text-left">
                                            @if($loop->index < 3 && file_exists(storage_path("app\\admin\\export\\{$exportRecord->file}")))
                                            <a href="{{ langRoute('administrator.io-construct.download', ['id' => $formData->id, 'record' => $exportRecord->id]) }}" target="_blank">{{ $exportRecord->file }}</a>
                                            @else
                                            {{ $exportRecord->file }}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </fieldset>
                        @endif
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

@if($formData->export_enable && ! is_null($formData->export_view))
@push('scripts')
<script>
$(function () {
    $('#exportForm').submit(function (e) {
        e.preventDefault();
        swal({
            title: '@lang("MinmaxIo::administrator.form.alert.exporting_title")',
            text: '@lang("MinmaxIo::administrator.form.alert.exporting_text")',
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false
        });
        var $form = $(this);
        $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function (response) {
                swal({title: '@lang("MinmaxIo::administrator.form.alert.export_success_title")', text: '@lang("MinmaxIo::administrator.form.alert.export_success_text")', type: 'success'}, function() {
                    location.reload(true);
                });
            },
            error: function (response) {
                swal('@lang("MinmaxIo::administrator.form.alert.export_error_title")', '@lang("MinmaxIo::administrator.form.alert.export_error_text")', 'error');
            }
        });
    });
});
</script>
@endpush
@endif
