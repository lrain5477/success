<?php
/**
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 * @var \Minmax\Io\Models\IoConstruct $formData
 */
?>

@extends('MinmaxBase::administrator.layouts.page.show')

@section('action-buttons')
    @component('MinmaxBase::administrator.layouts.right-links', ['languageActive' => $languageActive])
    <a class="btn btn-sm btn-light" href="{{ langRoute("administrator.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::administrator.form.back_list')">
        <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.back_list')</span>
    </a>
    <a class="btn btn-sm btn-main" href="{{ langRoute("administrator.{$pageData->uri}.edit", [$formData->id]) }}" title="@lang('MinmaxBase::administrator.form.edit')">
        <i class="icon-pencil"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.edit')</span>
    </a>
    @endcomponent
@endsection

@section('views')
    @inject('modelPresenter', 'Minmax\Io\Administrator\IoConstructPresenter')

    <fieldset id="baseFieldSet">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::administrator.form.fieldSet.default')</legend>

        {!! $modelPresenter->getShowNormalText($formData, 'title') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'uri') !!}

        {!! $modelPresenter->getShowSelection($formData, 'import_enable') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'import_permission') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'import_view') !!}

        {!! $modelPresenter->getShowSelection($formData, 'export_enable') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'export_permission') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'export_view') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'controller') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'example') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'filename') !!}

    </fieldset>

    <fieldset class="mt-4" id="advFieldSet">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::administrator.form.fieldSet.advanced')</legend>

        {!! $modelPresenter->getShowNormalText($formData, 'sort') !!}

        {!! $modelPresenter->getShowSelection($formData, 'active') !!}

    </fieldset>

    <fieldset class="mt-4" id="sysFieldSet">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::administrator.form.fieldSet.system_record')</legend>

        {!! $modelPresenter->getShowNormalText($formData, 'created_at') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'updated_at') !!}

    </fieldset>

@endsection
