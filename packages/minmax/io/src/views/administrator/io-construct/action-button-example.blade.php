<?php
/**
 * @var string $uri
 * @var string $id
 */
?>
<a class="btn btn-outline-default btn-sm" role="button" aria-pressed="true" title="@lang('MinmaxIo::administrator.grid.actions.example')" href="{{ langRoute("administrator.{$uri}.example", [$id]) }}">
    <i class="icon-download"></i><span class="text-hide">@lang('MinmaxIo::administrator.grid.actions.example')</span>
</a>