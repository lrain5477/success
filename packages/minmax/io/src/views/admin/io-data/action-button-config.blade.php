<?php
/**
 * @var string $uri
 * @var string $id
 */
?>
<a class="btn btn-outline-default btn-sm" role="button" aria-pressed="true" title="@lang('MinmaxIo::admin.grid.actions.config')" href="{{ langRoute("admin.{$uri}.config", [$id]) }}">
    <i class="icon-loop align-middle"></i><span class="ml-1">@lang('MinmaxIo::admin.grid.actions.config')</span>
</a>