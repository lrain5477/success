<?php
/**
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Io\Models\IoConstruct $formData
 */
?>

@extends('MinmaxBase::admin.layouts.site')

@section('breadcrumbs', Breadcrumbs::view('MinmaxBase::admin.layouts.breadcrumbs', 'config'))

@section('content')
<section class="panel panel-default">
    <header class="panel-heading">
        <h1 class="h5 float-left font-weight-bold">{{ $formData->title }}</h1>

        <div class="float-right">
            <a class="btn btn-sm btn-light" href="{{ langRoute("admin.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::admin.grid.back')">
                <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.grid.back')</span>
            </a>
        </div>
    </header>

    <div class="panel-wrapper">
        <div class="panel-body">
            <header class="mb-4">
                <ul class="nav nav-tabs" id="ioTab" role="tablist">
                    @if($formData->import_enable && (is_null($formData->import_permission) || $adminData->can($formData->import_permission)))
                    <li class="nav-item">
                        <a class="nav-link {{ request('method', 'import') == 'import' ? 'active' : '' }}"
                           id="tab-import" data-toggle="tab"
                           href="#tab-pane-import" role="tab"
                           onclick="window.history.pushState(null, null, '?method=import')"
                           aria-controls="tab-pane-import" aria-selected="true">@lang('MinmaxIo::admin.form.import')</a>
                    </li>
                    @endif
                    @if($formData->export_enable && (is_null($formData->export_permission) || $adminData->can($formData->export_permission)))
                    <li class="nav-item">
                        <a class="nav-link {{ request('method', 'import') == 'export' ? 'active' : ($formData->import_enable ? '' : 'active') }}"
                           id="tab-export" data-toggle="tab"
                           href="#tab-pane-export" role="tab"
                           onclick="window.history.pushState(null, null, '?method=export')"
                           aria-controls="tab-pane-export" aria-selected="true">@lang('MinmaxIo::admin.form.export')</a>
                    </li>
                    @endif
                </ul>
            </header>
            <div class="tab-content" id="myTabContent">
                @if($formData->import_enable && !is_null($formData->import_view) && (is_null($formData->import_permission) || $adminData->can($formData->import_permission)))
                <div class="tab-pane fade {{ request('method', 'import') == 'import' ? 'show active' : '' }}"
                     id="tab-pane-import" role="tabpanel" aria-labelledby="tab-import">
                    @include($formData->import_view)

                    @if($formData->ioRecords()->where('type', 'import')->count() > 0)
                    <fieldset class="mt-4">
                        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-3"></i>@lang('MinmaxIo::admin.form.fieldSet.import_record')</legend>
                        <div class="table-responsive">
                            <table class="table table-sm table-bordered mb-1">
                                <thead class="thead-default">
                                <tr class="text-center">
                                    <th class="w-20">@lang('MinmaxIo::models.IoRecord.created_at')</th>
                                    <th class="w-10">@lang('MinmaxIo::models.IoRecord.result')</th>
                                    <th class="w-10">@lang('MinmaxIo::models.IoRecord.total')</th>
                                    <th class="w-10">@lang('MinmaxIo::models.IoRecord.success')</th>
                                    <th>@lang('MinmaxIo::admin.form.get_report')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($formData->ioRecords()->where('type', 'import')->orderByDesc('id')->limit(10)->get() as $exportRecord)
                                    <tr class="text-center">
                                        <td>{{ $exportRecord->created_at }}</td>
                                        <td>{{ systemParam('result.' . intval($exportRecord->result) . '.title') }}</td>
                                        <td>{{ $exportRecord->total }}</td>
                                        <td>{{ $exportRecord->success }}</td>
                                        <td class="text-left">
                                            @if($exportRecord->success < $exportRecord->total)
                                                <a href="{{ langRoute('admin.io-data.report', ['id' => $formData->id, 'record' => $exportRecord->id]) }}" target="_blank"><i class="icon-cloud-download"></i></a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </fieldset>
                    @endif
                </div>
                @endif
                @if($formData->export_enable && !is_null($formData->export_view) && (is_null($formData->export_permission) || $adminData->can($formData->export_permission)))
                <div class="tab-pane fade {{ request('method', 'import') == 'export' ? 'show active' : ($formData->import_enable ? '' : 'show active') }}"
                     id="tab-pane-export" role="tabpanel" aria-labelledby="tab-export">
                    @include($formData->export_view)

                    @if($formData->ioRecords()->where('type', 'export')->count() > 0)
                    <fieldset class="mt-4">
                        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-3"></i>@lang('MinmaxIo::admin.form.fieldSet.export_record')</legend>
                        <div class="table-responsive">
                            <table class="table table-sm table-bordered mb-1">
                                <thead class="thead-default">
                                <tr class="text-center">
                                    <th class="w-20">@lang('MinmaxIo::models.IoRecord.created_at')</th>
                                    <th class="w-10">@lang('MinmaxIo::models.IoRecord.result')</th>
                                    <th class="w-10">@lang('MinmaxIo::models.IoRecord.success')</th>
                                    <th>@lang('MinmaxIo::admin.form.download')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($formData->ioRecords()->where('type', 'export')->orderByDesc('id')->limit(10)->get() as $exportRecord)
                                <tr class="text-center">
                                    <td>{{ $exportRecord->created_at }}</td>
                                    <td>{{ systemParam('result.' . intval($exportRecord->result) . '.title') }}</td>
                                    <td>{{ $exportRecord->success }}</td>
                                    <td class="text-left">
                                        @if($loop->index < 3 && file_exists(storage_path("app\\admin\\export\\{$exportRecord->file}")))
                                        <a href="{{ langRoute('admin.io-data.download', ['id' => $formData->id, 'record' => $exportRecord->id]) }}" target="_blank">{{ $exportRecord->file }}</a>
                                        @else
                                        {{ $exportRecord->file }}
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </fieldset>
                    @endif
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection

@if($formData->export_enable && ! is_null($formData->export_view) && (is_null($formData->export_permission) || $adminData->can($formData->export_permission)))
@push('scripts')
<script>
$(function () {
    $('#exportForm').submit(function (e) {
        e.preventDefault();
        swal({
            title: '@lang("MinmaxIo::admin.form.alert.exporting_title")',
            text: '@lang("MinmaxIo::admin.form.alert.exporting_text")',
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false
        });
        var $form = $(this);
        $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function () {
                swal({title: '@lang("MinmaxIo::admin.form.alert.export_success_title")', text: '@lang("MinmaxIo::admin.form.alert.export_success_text")', type: 'success'}, function() {
                    location.reload(true);
                });
            },
            error: function () {
                swal('@lang("MinmaxIo::admin.form.alert.export_error_title")', '@lang("MinmaxIo::admin.form.alert.export_error_text")', 'error');
            }
        });
    });
});
</script>
@endpush
@endif
