<?php
/**
 * @var string $uri
 * @var string $id
 */
?>
<a class="btn btn-outline-default btn-sm" role="button" aria-pressed="true" title="@lang('MinmaxIo::admin.grid.actions.example')" href="{{ langRoute("admin.{$uri}.example", [$id]) }}">
    <i class="icon-download align-middle"></i><span class="ml-1">@lang('MinmaxIo::admin.grid.actions.example')</span>
</a>