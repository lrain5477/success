<?php

namespace Minmax\Base\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SiteDashboard
 * @property integer $id
 * @property string $guard
 * @property string $title
 * @property string $presenter
 * @property array $position
 * @property array $options
 * @property integer $sort
 * @property boolean $active
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class SiteDashboard extends Model
{
    protected $table = 'site_dashboard';
    protected $guarded = [];
    protected $casts = [
        'position' => 'array',
        'options' => 'array',
        'active' => 'boolean',
    ];

    public function getTitleAttribute()
    {
        return langDB($this->getAttributeFromArray('title'));
    }
}
