<?php

namespace Minmax\Base\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ServiceConfig
 * @property integer $id
 * @property string $code
 * @property string $group
 * @property string $title
 * @property string $host
 * @property integer $sort
 * @property array $options
 * @property array $parameters
 * @property boolean $active
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $host_url
 * @property string $host_port
 */
class ServiceConfig extends Model
{
    protected $table = 'service_config';
    protected $guarded = [];
    protected $casts = [
        'options' => 'array',
        'parameters' => 'array',
        'active' => 'boolean',
    ];

    public function getTitleAttribute()
    {
        return langDB($this->getAttributeFromArray('title'));
    }

    public function getHostUrlAttribute()
    {
        if (is_null($this->getAttribute('host'))) {
            return null;
        }

        return array_get(explode(':', $this->getAttribute('host')), '0');
    }

    public function getHostPortAttribute()
    {
        if (is_null($this->getAttribute('host'))) {
            return null;
        }

        return array_get(explode(':', $this->getAttribute('host')), '1');
    }
}
