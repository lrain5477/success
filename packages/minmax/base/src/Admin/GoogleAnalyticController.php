<?php

namespace Minmax\Base\Admin;

use Cache;
use Illuminate\Http\Request;
use Minmax\Base\GoogleAnalyticsClient;
use Minmax\World\Admin\WorldCountryRepository;

/**
 * Class GoogleAnalyticController
 *
 * For Google Analytic ajax request.
 */
class GoogleAnalyticController extends Controller
{
    /**
     * @var GoogleAnalyticsClient $gaClient
     */
    protected $gaClient;

    public function __construct(GoogleAnalyticsClient $gaClient)
    {
        $this->gaClient = $gaClient;

        parent::__construct();
    }

    /**
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getVisitedSource(Request $request)
    {
        $days = $request->input('days', 30);
        $cacheMinutes = config('services.google_analytics.cache_lifetime_in_minutes');

        $response = Cache::remember("sourceMedium{$days}", $cacheMinutes, function () use ($days) {
            return $this->gaClient->query('ga', [
                'startTime' => today()->subDays($days)->getTimestamp(),
                'endTime' => time(),
                'metrics' => 'ga:sessions',
                'others' => [
                    'dimensions' => 'ga:medium',
                    'sort' => '-ga:sessions',
                ]
            ]);
        });

        $sourceData = collect($response->rows ?? [])
            ->map(function ($item) {
                $source = 'direct';
                if (strpos($item[0], 'organic') !== false) $source = 'organic';
                if (strpos($item[0], 'referral') !== false) $source = 'referral';

                return ['source' => $source, 'count' => intval($item[1])];
            })
            ->groupBy('source')
            ->map(function($item, $key) {
                return [
                    'source' => __('MinmaxBase::admin.dashboard.medium.json_' . $key),
                    'count' => collect($item)->sum('count')
                ];
            })
            ->values();

        return response($sourceData, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getVisitedSourceList(Request $request)
    {
        $days = $request->input('days', 30);
        $cacheMinutes = config('services.google_analytics.cache_lifetime_in_minutes');

        $response = Cache::remember("sourceMedium{$days}", $cacheMinutes, function () use ($days) {
            return $this->gaClient->query('ga', [
                'startTime' => today()->subDays($days)->getTimestamp(),
                'endTime' => time(),
                'metrics' => 'ga:sessions',
                'others' => [
                    'dimensions' => 'ga:medium',
                    'sort' => '-ga:sessions',
                ]
            ]);
        });

        $timeResponse = Cache::remember("weekSourceMedium", $cacheMinutes, function () {
            return $this->gaClient->query('ga', [
                'startTime' => today()->subDays(6)->getTimestamp(),
                'endTime' => time(),
                'metrics' => 'ga:sessions',
                'others' => [
                    'dimensions' => 'ga:medium,ga:date',
                    'sort' => 'ga:date'
                ]
            ]);
        });

        $sourceSummary = collect($response->rows ?? [])
            ->sum(function ($item) {
                return intval($item[1]);
            });

        $sourceData = collect($response->rows ?? [])
            ->map(function ($item) {
                $source = 'direct';
                if (strpos($item[0], 'organic') !== false) $source = 'organic';
                if (strpos($item[0], 'referral') !== false) $source = 'referral';

                return ['source' => $source, 'count' => intval($item[1])];
            })
            ->groupBy('source')
            ->map(function($item, $key) use ($sourceSummary, $timeResponse) {
                $weekMediumData = collect($timeResponse->rows ?? [])
                    ->map(function ($item) {
                        $source = 'direct';
                        if (strpos($item[0], 'organic') !== false) $source = 'organic';
                        if (strpos($item[0], 'referral') !== false) $source = 'referral';
                        return [
                            'source' => $source,
                            'date' => substr($item[1], 0, 4) . '-' . substr($item[1], 4, 2) . '-' . substr($item[1], 6, 2),
                            'count' => $item[2]
                        ];
                    })
                    ->groupBy('source')
                    ->map(function ($item, $key) {
                        /** @var \Illuminate\Support\Collection $item */
                        $startDate = today()->subDays(6);
                        $endDate = today();

                        for($date = $startDate; $date->lessThanOrEqualTo($endDate); $date = $date->addDay()) {
                            $dateString = $date->format('Y-m-d');

                            if($item->contains('date', '=', $dateString)) continue;

                            $item->push(['source' => $key, 'date' => $dateString, 'count' => '0']);
                        }

                        return $item
                            ->sortBy('date')
                            ->map(function ($item) {
                                return [
                                    'date' => date('Y-m-d\T16:00:00.000\Z', strtotime($item['date'])),
                                    'value' => (int)$item['count']
                                ];
                            })
                            ->values();
                    });
                /** @var \Illuminate\Support\Collection $weekMediumData */
                return [
                    'source' => __('MinmaxBase::admin.dashboard.medium.' . $key),
                    'count' => collect($item)->sum('count'),
                    'rate' => $sourceSummary == 0 ? '0.00' : number_format(collect($item)->sum('count') * 100 / $sourceSummary, 2),
                    'week' => $weekMediumData->get($key)
                ];
            });

        return response($sourceData, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getCurrentVisitor(Request $request)
    {
        $days = $request->input('days', 30);
        $decimals = $request->input('decimals', 2);
        $cacheMinutes = config('services.google_analytics.cache_lifetime_in_minutes');

        $activeVisitorResponse = Cache::remember('activeVisitor', 1, function() {
            return $this->gaClient->query('rt', [
                'metrics' => 'rt:activeVisitors'
            ]);
        });

        $percentNewSessionsResponse = Cache::remember("percentNewSessions{$days}", $cacheMinutes, function() use ($days) {
            return $this->gaClient->query('ga', [
                'startTime' => today()->subDays($days)->getTimestamp(),
                'endTime' => time(),
                'metrics' => 'ga:percentNewSessions'
            ]);
        });

        $pageViewsPerSessionResponse = Cache::remember("pageViewsPerSession{$days}", $cacheMinutes, function() use ($days) {
            return $this->gaClient->query('ga', [
                'startTime' => today()->subDays($days)->getTimestamp(),
                'endTime' => time(),
                'metrics' => 'ga:pageviewsPerSession'
            ]);
        });

        $avgTimeOnPageResponse = Cache::remember("avgTimeOnPage{$days}", $cacheMinutes, function() use ($days) {
            return $this->gaClient->query('ga', [
                'startTime' => today()->subDays($days)->getTimestamp(),
                'endTime' => time(),
                'metrics' => 'ga:avgTimeOnPage'
            ]);
        });

        $exitRateResponse = Cache::remember("exitRate{$days}", $cacheMinutes, function() use ($days) {
            return $this->gaClient->query('ga', [
                'startTime' => today()->subDays($days)->getTimestamp(),
                'endTime' => time(),
                'metrics' => 'ga:exitRate'
            ]);
        });

        return response([
            'currentVisitor'      => array_get($activeVisitorResponse->totalsForAllResults, 'rt:activeVisitors', 0),
            'percentNewSessions'  => number_format(array_get($percentNewSessionsResponse, '0.0') ?? 0, $decimals) . '%',
            'pageViewsPerSession' => number_format(array_get($pageViewsPerSessionResponse, '0.0') ?? 0, $decimals),
            'avgTimeOnPage'       => gmdate('H:i:s', array_get($avgTimeOnPageResponse, '0.0') ?? 0),
            'exitRate'            => number_format(array_get($exitRateResponse, '0.0') ?? 0, $decimals) . '%',
        ], 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getTopBrowsers(Request $request)
    {
        $days = $request->input('days', 30);
        $max  = $request->input('max', 4);
        $cacheMinutes = config('services.google_analytics.cache_lifetime_in_minutes');

        $response = Cache::remember("topBrowsers{$days}", $cacheMinutes, function() use ($days) {
            return $this->gaClient->query('ga', [
                'startTime' => today()->subDays($days)->getTimestamp(),
                'endTime' => time(),
                'metrics' => 'ga:sessions',
                'others' => [
                    'dimensions' => 'ga:browser',
                    'sort' => '-ga:sessions',
                ]
            ]);
        });

        $browsers = collect($response->rows ?? [])->map(function($item) {
            return [
                'browser' => $item[0],
                'sessions' => intval($item[1]),
            ];
        });

        if ($browsers->count() <= $max) {
            $browserData = $browsers;
        } else {
            $browserData = $browsers
                ->take($max - 1)
                ->push([
                    'browser' => 'Others',
                    'sessions' => $browsers->splice($max - 1)->sum('sessions'),
                ]);
        }

        $amountSession = $browserData->sum('sessions');

        $browserData->transform(function ($item) use ($amountSession) {
            return [
                'browser' => $item['browser'],
                'rate' => number_format($item['sessions'] / $amountSession * 100, 2) . '%',
            ];
        });

        return response($browserData, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param  WorldCountryRepository $repository
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getWorldSource(WorldCountryRepository $repository, Request $request)
    {
        $days = $request->input('days', 30);
        $cacheMinutes = config('services.google_analytics.cache_lifetime_in_minutes');

        $response = Cache::remember("sourceCountry{$days}", $cacheMinutes, function () use ($days) {
            return $this->gaClient->query('ga', [
                'startTime' => today()->subDays($days)->getTimestamp(),
                'endTime' => time(),
                'metrics' => 'ga:sessions',
                'others' => [
                    'dimensions' => 'ga:country'
                ]
            ]);
        });

        $worldSource = collect($response->rows ?? [])->pluck(1, 0)->toArray();

        $worldCountries = $repository->query()->with('worldContinent')->whereIn('title', array_keys($worldSource))->get();

        $sourceData = $worldCountries
            ->map(function ($country) use ($worldSource) {
                /** @var \Minmax\World\Models\WorldCountry $country */
                return [
                    'code' => $country->code,
                    'name' => $country->name,
                    'value' => intval(array_get($worldSource, $country->title) ?? 0),
                    'color' => array_get($country->worldContinent->options, 'country_color') ?? "#ee3333",
                    'latitude' => floatval(array_get($country->options, 'lat') ?? 0),
                    'longitude' => floatval(array_get($country->options, 'lng') ?? 0)
                ];
            })
            ->where('value', '>', 0)
            ->values();

        return response($sourceData, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getTodayTotalVisitors()
    {
        $cacheMinutes = config('services.google_analytics.cache_lifetime_in_minutes');

        $response = Cache::remember('todayTotalVisitors', $cacheMinutes, function() {
            return $this->gaClient->query('ga', [
                'startTime' => today()->subDays(1)->getTimestamp(),
                'endTime' => time(),
                'metrics' => 'ga:users',
                'others' => [
                    'dimensions' => 'ga:date'
                ]
            ]);
        });

        $visitorAmount = collect($response->rows ?? [])->map(function($item) {
            return intval($item[1]);
        });

        return response(['total' => $visitorAmount->count() > 0 ? number_format($visitorAmount->last()) : '-'], 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getTotalNewVisitors()
    {
        $cacheMinutes = config('services.google_analytics.cache_lifetime_in_minutes');

        $response = Cache::remember('totalNewVisitors', $cacheMinutes, function() {
            return $this->gaClient->query('ga', [
                'startTime' => today()->subYears(10)->getTimestamp(),
                'endTime' => time(),
                'metrics' => 'ga:newUsers',
            ]);
        });

        $visitorAmount = collect($response->rows ?? [])->sum(function($item) {
            return intval($item[0]);
        });

        return response(['total' => number_format($visitorAmount)], 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getPathViews(Request $request)
    {
        $days = $request->input('days', 30);
        $max = $request->input('max', 5);
        $cacheMinutes = config('services.google_analytics.cache_lifetime_in_minutes');

        $response = Cache::remember("pathViews{$days}", $cacheMinutes, function () use ($days, $max) {
            return $this->gaClient->query('ga', [
                'startTime' => today()->subDays($days)->getTimestamp(),
                'endTime' => time(),
                'metrics' => 'ga:pageviews,ga:avgTimeOnPage',
                'others' => [
                    'dimensions' => 'ga:pagePath',
                    'sort' => '-ga:pageviews',
                    'max-results' => $max
                ]
            ]);
        });

        $pathViews = collect($response->rows ?? [])
            ->map(function ($item) {
                return [
                    'path' => $item[0],
                    'count' => number_format($item[1]),
                    'time' => gmdate('H:i:s', floatval($item[2] ?? 0))
                ];
            });

        return response($pathViews, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getAgeSource(Request $request)
    {
        $days = $request->input('days', 30);
        $cacheMinutes = config('services.google_analytics.cache_lifetime_in_minutes');

        $response = Cache::remember("ageSource{$days}", $cacheMinutes, function () use ($days) {
            return $this->gaClient->query('ga', [
                'startTime' => today()->subDays($days)->getTimestamp(),
                'endTime' => time(),
                'metrics' => 'ga:sessions',
                'others' => [
                    'dimensions' => 'ga:userAgeBracket',
                ]
            ]);
        });

        $ageSource = collect($response->rows ?? [])
            ->map(function ($item) {
                return [
                    'age' => $item[0],
                    'visits' => intval($item[1]),
                ];
            });

        return response($ageSource, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getReferrerKeyword(Request $request)
    {
        $days = $request->input('days', 30);
        $max = $request->input('max', 5);
        $cacheMinutes = config('services.google_analytics.cache_lifetime_in_minutes');

        $response = Cache::remember("referrerKeyword{$days}", $cacheMinutes, function() use ($days) {
            return $this->gaClient->query('ga', [
                'startTime' => today()->subDays($days)->getTimestamp(),
                'endTime' => time(),
                'metrics' => 'ga:users',
                'others' => [
                    'dimensions' => 'ga:keyword'
                ]
            ]);
        });

        $keywords = collect($response->rows ?? [])
            ->map(function ($item) {
                return [
                    'keyword' => $item[0],
                    'count' => intval($item[1]),
                ];
            })
            ->filter(function ($item) {
                return mb_strlen($item['keyword']) < 30;
            })
            ->sortByDesc('count')
            ->transform(function ($item) {
                return [
                    'keyword' => $item['keyword'],
                    'count' => number_format($item['count']),
                ];
            })
            ->values();

        return response($keywords->count() > $max ? $keywords->take($max) : $keywords,
            200, ['Content-Type' => 'application/json']);
    }
}
