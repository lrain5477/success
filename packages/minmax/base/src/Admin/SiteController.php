<?php

namespace Minmax\Base\Admin;

/**
 * Class SiteController
 */
class SiteController extends Controller
{
    public function __construct(SiteDashboardRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    /**
     * Display Dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->viewData['dashboardData'] = $this->modelRepository->getDashboardModules();

        return view('MinmaxBase::admin.site.index', $this->viewData);
    }
}
