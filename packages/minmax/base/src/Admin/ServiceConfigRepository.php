<?php

namespace Minmax\Base\Admin;

use Illuminate\Support\Facades\Cache;
use Minmax\Base\Models\ServiceConfig;

/**
 * Class ServiceConfigRepository
 * @property ServiceConfig $model
 * @method ServiceConfig find($id)
 * @method ServiceConfig one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ServiceConfig[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ServiceConfig create($attributes)
 * @method ServiceConfig save($model, $attributes)
 * @method ServiceConfig|\Illuminate\Database\Eloquent\Builder query()
 */
class ServiceConfigRepository extends Repository
{
    const MODEL = ServiceConfig::class;

    protected $sort = 'sort';

    protected $sorting = true;

    protected $languageColumns = ['title'];

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'service_config';
    }

    protected function afterSave()
    {
        Cache::forget('serviceConfigs');
    }

    public function getSelectParameters($group)
    {
        return $this->query()
            ->where('group', $group)
            ->where('active', true)
            ->orderBy('sort')
            ->get()
            ->mapWithKeys(function ($configItem) {
                /** @var ServiceConfig $configItem */
                return [$configItem->code => ['title' => $configItem->title, 'options' => []]];
            })
            ->toArray();
    }
}
