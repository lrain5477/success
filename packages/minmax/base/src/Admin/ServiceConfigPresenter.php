<?php

namespace Minmax\Base\Admin;

/**
 * Class ServiceConfigPresenter
 */
class ServiceConfigPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxBase::';

    protected $languageColumns = ['title'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'active' => systemParam('active'),
        ];
    }
}
