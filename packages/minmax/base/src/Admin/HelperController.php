<?php

namespace Minmax\Base\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;
use Minmax\Base\Helpers\Captcha as CaptchaHelper;
use Minmax\Base\Helpers\Image as ImageHelper;

/**
 * Class HelperController
 */
class HelperController extends BaseController
{
    /**
     * @param  string $name
     * @param  integer $id
     * @return string
     */
    public function getCaptcha($name, $id = null)
    {
        return CaptchaHelper::createCaptcha('admin_captcha_' . $name, 4, $id);
    }

    /**
     * @param  string $category
     * @return string
     */
    public function getEditorTemplate($category = 'templates')
    {
        $templates = siteParam($category);

        if (is_array($templates)) {
            $templates = collect($templates)
                ->map(function($item) {
                    return [
                        'title' => array_get($item, 'title') ?? 'Undefined',
                        'description' => array_get($item, 'details.description') ?? 'No description.',
                        'html' => array_get($item, 'details.editor') ?? '',
                    ];
                })
                ->values()
                ->toJson(JSON_UNESCAPED_UNICODE);

            return response("CKEDITOR.addTemplates('default',{templates: {$templates} });", 200, ['Content-Type' => 'application/javascript']);
        }

        return response("CKEDITOR.addTemplates('default',{templates: [] });", 200, ['Content-Type' => 'application/javascript']);
    }

    /**
     * @param  Request $request
     * @param  WorldLanguageRepository $repository
     * @return \Illuminate\Http\Response
     */
    public function setFormLocal(Request $request, WorldLanguageRepository $repository)
    {
        if ($model = $repository->one(['code' => $request->input('language', ''), 'active' => true])) {
            session(['admin-formLocal' => $model->code]);
            session()->save();
        }
        return response('', 200);
    }
}
