<?php

namespace Minmax\Base\Admin;

use Minmax\Base\Models\ServiceConfig;

/**
 * Class ServiceConfigTransformer
 */
class ServiceConfigTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'serviceConfigShow',
        'U' => 'serviceConfigEdit',
    ];

    /**
     * Transformer constructor.
     * @param  ServiceConfigPresenter $presenter
     * @param  string $uri
     */
    public function __construct(ServiceConfigPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  ServiceConfig $model
     * @return array
     * @throws \Throwable
     */
    public function transform(ServiceConfig $model)
    {
        return [
            'code' => $this->presenter->getGridText($model, 'code'),
            'title' => $this->presenter->getGridText($model, 'title'),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
