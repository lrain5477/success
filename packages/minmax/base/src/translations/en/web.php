<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Web (Frontend) Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    'page_not_found' => [
        'message' => 'Sorry, the page you are looking for could not be found.',
    ],

    'header' => [
        'menu' => 'Menu',
        'language' => 'Language',
        'account' => 'Account',
        'logout' => 'Logout',
    ],

];