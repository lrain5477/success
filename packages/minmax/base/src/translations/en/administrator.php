<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Administrator Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    'page_not_found' => [
        'message' => 'Sorry, the page you are looking for could not be found.',
    ],

    'header' => [
        'menu' => 'Menu',
        'profile' => 'Profile',
        'language' => 'Language',
        'account' => 'Account',
        'login' => 'Login',
        'logout' => 'Logout',
    ],

    'breadcrumbs' => [
        'home' => 'Home',
    ],

    'sidebar' => [
        'home' => 'Home',
    ],

    'login' => [
        'title' => 'Administrator System',
        'forget' => 'Forgot password?',
        'username' => 'Username',
        'email' => 'Email',
        'password' => 'Password',
        'captcha' => 'Captcha',
        'remember' => 'Remember me',
        'login_submit' => 'Login',
        'forget_submit' => 'Submit',
        'back_button' => 'Back',
        'info' => [
            'topic' => 'Welcome!',
            'message' => 'Here is :site.<br>If you got any problem with login,<br>please contact us, we will help you soon!<br>Enjoy it!',
            'forget' => 'Enter your email, and the system will send reset link to you.',
        ],
    ],

    'dashboard' => [
        'source_from' => 'Source',
        'visits' => 'Visits',
        'online_users' => 'Online Users',
        'new_session' => 'New Session',
        'session_page' => 'Visit Page',
        'stay_time' => 'Stay Time',
        'exit_rate' => 'Exit Rate',
        'browser_usage' => 'Browser',
        'source_country' => 'Country Source',
        'today_visitors' => 'Today Visitors',
        'service_message' => 'Service Message',
        'recently_message' => 'Recently Message',
        'empty_message' => 'Message Empty',
        'view_all' => 'All',
        'path' => 'Page Path',
        'path_view' => 'Visited',
        'path_time' => 'Average Stay',
        'keywords' => 'Hot Keywords',
        'keyword' => 'Keyword',
        'keyword_count' => 'Count',
        'medium' => [
            'direct' => 'Direct',
            'organic' => 'Organic',
            'referral' => 'Referral',
            'json_direct' => 'Direct',
            'json_organic' => 'Organic',
            'json_referral' => 'Referral',
        ],
    ],

    'grid' => [
        'title' => [
            'action' => 'Action'
        ],
        'actions' => [
            'view' => 'View',
            'edit' => 'Edit',
            'log' => 'Log',
            'delete' => 'Delete',
            'children' => 'Children',
        ],
        'selection' => [
            'all' => 'All',
        ],
        'back' => 'Back',
        'root' => 'Root',
        'next_layer' => 'Next Layer',
        'click_to_switch' => 'Click to switch',
        'search' => 'Search',
        'filter' => 'Filter',
    ],

    'form' => [
        'show' => 'View',
        'create' => 'Create',
        'edit' => 'Edit',
        'language' => 'Language',
        'import' => 'Import',
        'export' => 'Export',
        'back_list' => 'Back',
        'record' => 'System Record',
        'note' => 'Note',
        'select_default_title' => 'Please Select',
        'select_nothing_title' => 'No Select',
        'password_build_auto' => 'Default password is 123456. It will be set by system.',
        'select_all' => 'Select All',
        'select_clear' => 'Clear Selected',
        'fieldSet' => [
            'default' => 'Default',
            'information' => 'Information',
            'media' => 'Media',
            'advanced' => 'Advanced',
            'permission' => 'Permission',
            'seo' => 'SEO',
            'system_record' => 'System Record',
        ],
        'button' => [
            'send' => 'Send',
            'reset' => 'Reset',
            'import' => 'Import',
            'export' => 'Export',
            'media_image' => 'Media Storage',
            'media_file' => 'File Storage',
        ],
        'file' => [
            'default_text' => 'Upload file',
            'image_text' => 'Upload Photo',
            'browser' => 'Browser',
            'remove_file' => 'Remove uploaded file',
            'limit_title' => 'Over limit',
            'limit_text' => 'You can only choose :limit files',
        ],
        'image' => [
            'advance' => [
                'tab' => 'Photo Setting',
                'fieldSet' => [
                    'base' => 'Base',
                    'detail' => 'Detail',
                    'option' => 'Advance Option',
                ],
                'field' => [
                    'path' => 'File',
                    'title' => 'Title',
                    'description' => 'Description',
                    'creator' => 'Creator',
                    'copyright' => 'Copyright',
                    'cover' => 'Cover',
                    'size' => 'Size',
                    'max_width' => 'Max Width',
                    'max_height' => 'Max Height',
                    'min_width' => 'Min Width',
                    'min_height' => 'Min Height',
                ],
                'options' => [
                    'cover_0' => 'Disable',
                    'cover_1' => 'Enable',
                ],
            ],
            'anchor' => [
                'tab' => 'Anchor',
            ],
            'submit' => 'Complete',
        ],
        'address' => [
            'zip' => 'Zip Code',
            'country' => 'Country',
            'state' => 'State',
            'county' => 'County',
            'city' => 'City',
            'street' => 'Street',
        ],
        'message' => [
            'create_success' => 'Create row data success.',
            'create_error' => 'Create row data failed.',
            'edit_success' => 'Update row data success.',
            'edit_error' => 'Update row data failed.',
            'delete_success' => 'Delete row data success.',
            'delete_error' => 'Delete row data failed.',
            'delete_error_account_self' => 'You can not delete your own account.',
            'import_success' => 'Import data success.',
            'import_error' => 'Import data failed.',
            'import_error_extension' => 'The file you choose is not fit format limit.',
            'export_error' => 'Export data failed.',
        ],
        'elfinder' => [
            'limit_title' => 'Over limit',
            'limit_text' => 'You can only choose :limit files',
            'limit_confirm_button' => 'I see',
            'remove_title' => 'Delete This',
            'remove_text' => 'You will remove this selection, we still keep the file in server.',
            'remove_cancel_button' => 'Cancel',
            'remove_confirm_button' => 'Yes',
            'remove_success_title' => 'Delete Completed!',
            'remove_success_text' => 'Your selection item was removed!',
        ],
    ],

];
