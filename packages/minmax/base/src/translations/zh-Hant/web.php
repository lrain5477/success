<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Web (Frontend) Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    'page_not_found' => [
        'message' => '對不起，您所請求的頁面不存在。',
    ],

    'header' => [
        'menu' => '選單',
        'language' => '語系',
        'account' => '帳戶',
        'logout' => '登出',
    ],

];