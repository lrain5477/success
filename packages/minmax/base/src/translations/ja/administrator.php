<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Administrator Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    'page_not_found' => [
        'message' => '申し訳ありませんが、探しているページは見つかりませんでした。',
    ],

    'header' => [
        'menu' => 'メニュー',
        'profile' => 'プロフィール',
        'language' => '言語',
        'account' => 'アカウント',
        'login' => 'ログイン',
        'logout' => 'ログアウト',
    ],

    'breadcrumbs' => [
        'home' => 'ホーム',
    ],

    'sidebar' => [
        'home' => 'ホーム',
    ],

    'login' => [
        'title' => '総理者システム',
        'forget' => 'パスワードをお忘れですか?',
        'username' => 'ユーザー名',
        'email' => 'メール',
        'password' => 'パスワード',
        'captcha' => 'キャプチャ',
        'remember' => '覚えている',
        'login_submit' => 'ログイン',
        'forget_submit' => '申し出る',
        'back_button' => '戻る',
        'info' => [
            'topic' => 'Welcome!',
            'message' => 'こは :site.<br>ログインに問題がある場合、<br>お問い合わせください、私たちはすぐにあなたを助けます！<br>楽しめ！',
            'forget' => 'メールアドレスを入力すると、システムからリセットリンクが送信されます。',
        ],
    ],

    'dashboard' => [
        'source_from' => 'Source',
        'visits' => 'Visits',
        'online_users' => 'Online Users',
        'new_session' => 'New Session',
        'session_page' => 'Visit Page',
        'stay_time' => 'Stay Time',
        'exit_rate' => 'Exit Rate',
        'browser_usage' => 'Browser',
        'source_country' => 'Country Source',
        'today_visitors' => 'Today Visitors',
        'service_message' => 'メッセージ',
        'recently_message' => '最近のメッセージ',
        'empty_message' => 'メッセージなし',
        'view_all' => '全て',
        'path' => 'ページ',
        'path_view' => 'ページビュー',
        'path_time' => '平均滞在時間',
        'keywords' => 'Hot Keywords',
        'keyword' => 'キーワード',
        'keyword_count' => '数量',
        'medium' => [
            'direct' => 'Direct',
            'organic' => 'Organic',
            'referral' => 'Referral',
            'json_direct' => 'Direct 直接',
            'json_organic' => 'Organic 捜査',
            'json_referral' => 'Referral 紹介',
        ],
    ],

    'grid' => [
        'title' => [
            'action' => '行動'
        ],
        'actions' => [
            'view' => '見る',
            'edit' => '更新',
            'log' => '記録',
            'delete' => '削除',
            'children' => '次の層',
        ],
        'selection' => [
            'all' => '全て',
        ],
        'back' => '戻る',
        'root' => 'Root',
        'next_layer' => '次の層',
        'click_to_switch' => '変える',
        'search' => '捜査',
        'filter' => '選択',
    ],

    'form' => [
        'show' => '見る',
        'create' => '作る',
        'edit' => '更新',
        'language' => '言語',
        'import' => '輸入',
        'export' => '輸出',
        'back_list' => '戻る',
        'record' => '記録',
        'note' => '付記',
        'select_default_title' => '選んでください',
        'select_nothing_title' => '選ばないで',
        'password_build_auto' => 'デフォルトのパスワードは 123456 です。システムによって設定されます。',
        'select_all' => '全て選択',
        'select_clear' => '選択をクリア',
        'fieldSet' => [
            'default' => '普通',
            'information' => '情報',
            'media' => '媒体',
            'advanced' => '高度',
            'permission' => '許可',
            'seo' => 'SEO',
            'system_record' => '系統記録',
        ],
        'button' => [
            'send' => '送る',
            'reset' => 'リセット',
            'import' => '輸入する',
            'export' => '輸出する',
            'media_image' => '媒体保管',
            'media_file' => 'ファイル保管',
        ],
        'file' => [
            'default_text' => 'ファイルをアップロード',
            'image_text' => '画像をアップロード',
            'browser' => '選択',
            'remove_file' => '選択したファイルを削除',
            'limit_title' => '限度を超えて',
            'limit_text' => ':limit ファイルしか選択できません',
        ],
        'image' => [
            'advance' => [
                'tab' => '画像情報',
                'fieldSet' => [
                    'base' => '普通',
                    'detail' => '細部',
                    'option' => '高度なオプション',
                ],
                'field' => [
                    'path' => 'ファイル',
                    'title' => 'タイトル',
                    'description' => '説明',
                    'creator' => '作成者',
                    'copyright' => '版権',
                    'cover' => 'カバー',
                    'size' => '大小',
                    'max_width' => '最大幅',
                    'max_height' => '最大高',
                    'min_width' => '最小幅',
                    'min_height' => '最小高',
                ],
                'options' => [
                    'cover_0' => '無効',
                    'cover_1' => '有効',
                ],
            ],
            'anchor' => [
                'tab' => 'アンカー',
            ],
            'submit' => '完成',
        ],
        'address' => [
            'zip' => '郵便番号',
            'country' => '国',
            'state' => '州',
            'county' => '郡',
            'city' => '市',
            'street' => '街路',
        ],
        'message' => [
            'create_success' => 'データ作成成功。',
            'create_error' => 'データ作成に失敗しました。',
            'edit_success' => 'データ更新成功。',
            'edit_error' => 'データ更新に失敗しました。',
            'delete_success' => 'データ削除成功。',
            'delete_error' => 'データ削除に失敗しました。',
            'delete_error_account_self' => '自分のアカウントを削除することはできません。',
            'import_success' => 'データ輸入成功。',
            'import_error' => 'データ輸入に失敗しました。',
            'import_error_extension' => '選択したファイルはフォーマット制限に合いません。',
            'export_error' => 'データ輸出に失敗しました。',
        ],
        'elfinder' => [
            'limit_title' => '限度を超えて',
            'limit_text' => ':limit ファイルしか選択できません',
            'limit_confirm_button' => '分かた',
            'remove_title' => '削除する',
            'remove_text' => 'この選択を削除します、まだファイルをサーバーに保存しています。',
            'remove_cancel_button' => 'いいえ',
            'remove_confirm_button' => 'はい',
            'remove_success_title' => '削除完了！',
            'remove_success_text' => 'あなたの選択アイテムは削除されました！',
        ],
    ],

];
