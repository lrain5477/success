<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Web (Frontend) Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    'page_not_found' => [
        'message' => '申し訳ありませんが、探しているページは見つかりませんでした。',
    ],

    'header' => [
        'menu' => 'メニュー',
        'language' => '言語',
        'account' => 'アカウント',
        'logout' => 'ログアウト',
    ],

];