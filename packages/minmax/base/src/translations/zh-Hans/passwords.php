<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => '密码最少必须 6 个字元长度，并且确认输入正确。',
    'reset' => '您的密码已经重设！',
    'sent' => '我们已经将重设密码连结寄送给您！',
    'token' => '重设密码的密钥无效。',
    'user' => "用户的信箱地址不存在。",

];
