<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Administrator Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    'page_not_found' => [
        'message' => '对不起，您所请求的页面不存在。 ',
    ],

    'header' => [
        'menu' => '选单',
        'profile' => '个人资料',
        'language' => '语系',
        'account' => '帐户',
        'login' => '登入',
        'logout' => '登出',
    ],

    'breadcrumbs' => [
        'home' => '系统首页',
    ],

    'sidebar' => [
        'home' => '系统首页',
    ],

    'login' => [
        'title' => '总后台管理系统',
        'forget' => '忘记密码?',
        'username' => '您的帐号',
        'email' => '您的电邮',
        'password' => '您的密码',
        'captcha' => '验证号码',
        'remember' => '记住我',
        'login_submit' => '登入系统',
        'forget_submit' => '送出',
        'back_button' => '返回',
        'info' => [
            'topic' => 'Welcome!',
            'message' => '欢迎您使用:site，<br>若您于登入上有任何问题，<br>请来信与我们联络，我们将会尽快为您处理!<br>祝您使用愉快与方便!',
            'forget' => '输入您的 Email 系统将寄发密码至您的注册电邮.',
        ],
    ],

    'dashboard' => [
        'source_from' => '流量来源',
        'visits' => '访问',
        'online_users' => '线上使用者',
        'new_session' => '新工作阶段',
        'session_page' => '单次页数',
        'stay_time' => '停留时间',
        'exit_rate' => '跳出率',
        'browser_usage' => '浏览器使用',
        'source_country' => '流量地区分布',
        'today_visitors' => '今日参观量',
        'service_message' => '客服信函',
        'recently_message' => '近期联络表单',
        'empty_message' => '没有联络表单',
        'view_all' => '浏览全部',
        'path' => '页面',
        'path_view' => '浏览量',
        'path_time' => '平均停留时间',
        'keywords' => '热门关键字',
        'keyword' => '关键字',
        'keyword_count' => '次数',
        'medium' => [
            'direct' => '直接',
            'organic' => '搜寻',
            'referral' => '推荐',
            'json_direct' => 'Direct 直接',
            'json_organic' => 'Organic 搜寻',
            'json_referral' => 'Referral 推荐',
        ],
    ],

    'grid' => [
        'title' => [
            'action' => '动作'
        ],
        'actions' => [
            'view' => '浏览',
            'edit' => '编辑',
            'log' => '纪录',
            'delete' => '删除',
            'children' => '子项目',
        ],
        'selection' => [
            'all' => '全部',
        ],
        'back' => '返回',
        'root' => '根列表',
        'next_layer' => '下层列表',
        'click_to_switch' => '点选变更状态',
        'search' => '搜寻',
        'filter' => '筛选',
    ],

    'form' => [
        'show' => '浏览',
        'create' => '新增',
        'edit' => '编辑',
        'language' => '语系',
        'import' => '汇入',
        'export' => '汇出',
        'back_list' => '返回列表',
        'record' => '系统纪录',
        'note' => '说明叙述',
        'select_default_title' => '请选择项目',
        'select_nothing_title' => '不选择',
        'password_build_auto' => '预设密码为 123456，由系统自动设定',
        'select_all' => '选择全部',
        'select_clear' => '清除选取',
        'fieldSet' => [
            'default' => '主要设定',
            'information' => '资讯设定',
            'media' => '多媒体设定',
            'advanced' => '进阶选项',
            'permission' => '权限设定',
            'seo' => '搜寻引擎优化',
            'system_record' => '系统纪录',
        ],
        'button' => [
            'send' => '送出',
            'reset' => '重新设定',
            'import' => '汇入',
            'export' => '汇出',
            'media_image' => '媒体库',
            'media_file' => '选择档案',
        ],
        'file' => [
            'default_text' => '档案上传',
            'image_text' => '图片上传',
            'browser' => '浏览',
            'remove_file' => '移除已上传的档案',
            'limit_title' => '超过选择上限',
            'limit_text' => '您最多只能选择 :limit 个档案',
        ],
        'image' => [
            'advance' => [
                'tab' => '图片资讯',
                'fieldSet' => [
                    'base' => '基本设定',
                    'detail' => '详细资讯',
                    'option' => '进阶选项',
                ],
                'field' => [
                    'path' => '档案',
                    'title' => '标题',
                    'description' => '备注',
                    'creator' => '作者',
                    'copyright' => '版权',
                    'cover' => '封面',
                    'size' => '尺寸',
                    'max_width' => '最大宽度',
                    'max_height' => '最大高度',
                    'min_width' => '最小宽度',
                    'min_height' => '最小高度',
                ],
                'options' => [
                    'cover_0' => '停用',
                    'cover_1' => '启用',
                ],
            ],
            'anchor' => [
                'tab' => '标点设定',
            ],
            'submit' => '完成',
        ],
        'address' => [
            'zip' => '邮递区号',
            'country' => '国家',
            'state' => '州区',
            'county' => '县市',
            'city' => '城镇',
            'street' => '街道地址',
        ],
        'message' => [
            'create_success' => '您新增的资料储存成功。 ',
            'create_error' => '您的新增资料操作失败。 ',
            'edit_success' => '您编辑的资料储存成功。 ',
            'edit_error' => '您编辑的资料储存失败。 ',
            'delete_success' => '您选择的资料已经删除成功。 ',
            'delete_error' => '您选择的资料无法删除，请再次确认。 ',
            'delete_error_account_self' => '您无法删除自己的帐号，请再次确认。 ',
            'import_success' => '您的资料已经汇入成功。 ',
            'import_error' => '您的资料汇入失败，请再次确认。 ',
            'import_error_extension' => '您的来源档案不符合要求格式，请再次确认。 ',
            'export_error' => '您的资料汇出失败，请再次尝试。 ',
        ],
        'elfinder' => [
            'limit_title' => '已达到选择上限',
            'limit_text' => '您最多只能选择 :limit 个档案',
            'limit_confirm_button' => '确认',
            'remove_title' => '是否确认删除',
            'remove_text' => '您将删除此项目，但档案仍会保留',
            'remove_cancel_button' => '取消',
            'remove_confirm_button' => '确认',
            'remove_success_title' => '删除完成!',
            'remove_success_text' => '您的档案已删除!',
        ],
    ],

];
