<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Web (Frontend) Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    'page_not_found' => [
        'message' => '对不起，您所请求的页面不存在。',
    ],

    'header' => [
        'menu' => '选单',
        'language' => '语系',
        'account' => '帐户',
        'logout' => '登出',
    ],

];