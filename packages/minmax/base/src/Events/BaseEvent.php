<?php

namespace Minmax\Base\Events;

use Illuminate\Queue\SerializesModels;
use Minmax\Notify\Models\NotifyEmail;
use Minmax\Notify\Models\NotifySms;

/**
 * Class BaseEvent
 */
abstract class BaseEvent
{
    use SerializesModels;

    /**
     * @var string $notifyCode
     */
    protected $notifyCode;

    /**
     * @var NotifyEmail $notifyEmail
     */
    public $notifyEmail;

    /**
     * @var NotifySms $notifySms
     */
    public $notifySms;

    /**
     * BaseEvent constructor.
     *
     * @return void
     */
    public function __construct()
    {
        if (isset($this->notifyCode)) {
            $this->notifyEmail = NotifyEmail::query()->where(['code' => $this->notifyCode, 'active' => true])->first();
            $this->notifySms = NotifySms::query()->where(['code' => $this->notifyCode, 'active' => true])->first();
        }
    }
}
