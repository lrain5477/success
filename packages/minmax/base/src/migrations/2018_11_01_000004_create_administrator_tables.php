<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class CreateAdministratorTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrator_menu', function (Blueprint $table) {
            $table->string('id', 64)->primary();
            $table->string('parent_id', 64)->nullable()->comment('上層目錄');
            $table->string('title')->comment('選單名稱');
            $table->string('uri')->unique()->comment('Uri');
            $table->string('controller')->nullable()->comment('Controller 名稱');
            $table->string('model')->nullable()->comment('Model 名稱');
            $table->string('link')->nullable()->comment('項目連結');
            $table->string('icon')->nullable()->comment('圖示 Class');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->timestamps();
        });

        Schema::create('administrator', function (Blueprint $table) {
            $table->string('id', 64)->primary();
            $table->string('username')->unique()->comment('帳號');
            $table->string('password')->comment('密碼');
            $table->rememberToken();
            $table->string('name')->nullable()->comment('姓名');
            $table->text('allow_ip')->nullable()->comment('IP白名單');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // 建立預設資料
        $this->insertDatabase();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrator');
        Schema::dropIfExists('administrator_menu');
    }

    /**
     * Insert default data
     *
     * @return void
     */
    public function insertDatabase()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        // 管理員帳號
        $administratorData = [
            [
                'id' => Str::uuid(), 'username' => 'sysadmin', 'password' => Hash::make('a24252151-A'),
                'name' => '超級管理員', 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];
        DB::table('administrator')->insert($administratorData);

        // 管理員選單 - 分類
        $administratorMenuData = [
            [
                'id' => $menuClassId1 = uuidl(), 'title' => 'Default', 'uri' => 'root-default',
                'controller' => null, 'model' => null, 'parent_id' => null, 'link' => null, 'icon' => null,
                'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => $menuClassId2 = uuidl(), 'title' => 'Module', 'uri' => 'root-module',
                'controller' => null, 'model' => null, 'parent_id' => null, 'link' => null, 'icon' => null,
                'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => $menuClassId3 = uuidl(), 'title' => 'System', 'uri' => 'root-system',
                'controller' => null, 'model' => null, 'parent_id' => null, 'link' => null, 'icon' => null,
                'sort' => 3, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
        ];

        // 管理員選單
        $administratorMenuData = array_merge($administratorMenuData, [
            // Default
            [
                'id' => uuidl(),
                'title' => '系統首頁',
                'uri' => 'home-administrator',
                'controller' => 'SiteController',
                'model' => null,
                'parent_id' => $menuClassId1,
                'link' => '/',
                'icon' => 'icon-home3',
                'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '後臺首頁',
                'uri' => 'home-admin',
                'controller' => null,
                'model' => null,
                'parent_id' => $menuClassId1,
                'link' => config('app.url') . '/siteadmin',
                'icon' => 'icon-home3',
                'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '網站首頁',
                'uri' => 'home-web',
                'controller' => null,
                'model' => null,
                'parent_id' => $menuClassId1,
                'link' => config('app.url'),
                'icon' => 'icon-home3',
                'sort' => 3, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],

            // System
            [
                'id' => $menuParentId1 = uuidl(),
                'title' => '控制臺',
                'uri' => 'control-configuration',
                'controller' => null,
                'model' => null,
                'parent_id' => $menuClassId3,
                'link' => null,
                'icon' => 'icon-cog',
                'sort' => 301, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '檔案總管',
                'uri' => 'file-manager',
                'controller' => 'FileManagerController',
                'model' => null,
                'parent_id' => $menuParentId1,
                'link' => 'file-manager',
                'icon' => null,
                'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '網站基本資訊',
                'uri' => 'web-data',
                'controller' => 'WebDataController',
                'model' => 'WebData',
                'parent_id' => $menuParentId1,
                'link' => 'web-data',
                'icon' => null,
                'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '後臺選單目錄',
                'uri' => 'admin-menu',
                'controller' => 'AdminMenuController',
                'model' => 'AdminMenu',
                'parent_id' => $menuParentId1,
                'link' => 'admin-menu',
                'icon' => null,
                'sort' => 3, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '前臺選單目錄',
                'uri' => 'web-menu',
                'controller' => 'WebMenuController',
                'model' => 'WebMenu',
                'parent_id' => $menuParentId1,
                'link' => 'web-menu',
                'icon' => null,
                'sort' => 4, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '網站參數項目',
                'uri' => 'site-parameter-item',
                'controller' => 'SiteParameterItemController',
                'model' => 'SiteParameterItem',
                'parent_id' => $menuParentId1,
                'link' => 'site-parameter-item',
                'icon' => null,
                'sort' => 5, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '網站參數群組',
                'uri' => 'site-parameter-group',
                'controller' => 'SiteParameterGroupController',
                'model' => 'SiteParameterGroup',
                'parent_id' => $menuParentId1,
                'link' => 'site-parameter-group',
                'icon' => null,
                'sort' => 6, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '儀錶板管理',
                'uri' => 'site-dashboard',
                'controller' => 'SiteDashboardController',
                'model' => 'SiteDashboard',
                'parent_id' => $menuParentId1,
                'link' => 'site-dashboard',
                'icon' => null,
                'sort' => 7, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],

            [
                'id' => $menuParentId2 = uuidl(),
                'title' => '帳戶資訊',
                'uri' => 'control-account',
                'controller' => null,
                'model' => null,
                'parent_id' => $menuClassId3,
                'link' => null,
                'icon' => 'icon-group',
                'sort' => 302, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '後臺帳戶管理',
                'uri' => 'admin',
                'controller' => 'AdminController',
                'model' => 'Admin',
                'parent_id' => $menuParentId2,
                'link' => 'admin',
                'icon' => null,
                'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '權限角色管理',
                'uri' => 'role',
                'controller' => 'RoleController',
                'model' => 'Role',
                'parent_id' => $menuParentId2,
                'link' => 'role',
                'icon' => null,
                'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '權限物件管理',
                'uri' => 'permission',
                'controller' => 'PermissionController',
                'model' => 'Permission',
                'parent_id' => $menuParentId2,
                'link' => 'permission',
                'icon' => null,
                'sort' => 3, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],

            [
                'id' => $menuParentId3 = uuidl(),
                'title' => '系統整合',
                'uri' => 'control-integration',
                'controller' => null,
                'model' => null,
                'parent_id' => $menuClassId3,
                'link' => null,
                'icon' => 'icon-handshake-o',
                'sort' => 303, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '服務設定',
                'uri' => 'service-config',
                'controller' => 'ServiceConfigController',
                'model' => 'ServiceConfig',
                'parent_id' => $menuParentId3,
                'link' => 'service-config',
                'icon' => null,
                'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],

            [
                'id' => $menuParentId4 = uuidl(),
                'title' => '資訊安全',
                'uri' => 'control-security',
                'controller' => null,
                'model' => null,
                'parent_id' => $menuClassId3,
                'link' => null,
                'icon' => 'icon-shield',
                'sort' => 304, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '防火牆',
                'uri' => 'firewall',
                'controller' => 'FirewallController',
                'model' => 'Firewall',
                'parent_id' => $menuParentId4,
                'link' => 'firewall',
                'icon' => null,
                'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '操作紀錄',
                'uri' => 'system-log',
                'controller' => 'SystemLogController',
                'model' => 'SystemLog',
                'parent_id' => $menuParentId4,
                'link' => 'system-log',
                'icon' => null,
                'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '登入紀錄',
                'uri' => 'login-log',
                'controller' => 'LoginLogController',
                'model' => 'LoginLog',
                'parent_id' => $menuParentId4,
                'link' => 'login-log',
                'icon' => null,
                'sort' => 3, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],

            [
                'id' => $menuParentId5 = uuidl(),
                'title' => '全球化管理',
                'uri' => 'control-world',
                'controller' => null,
                'model' => null,
                'parent_id' => $menuClassId3,
                'link' => null,
                'icon' => 'icon-sphere',
                'sort' => 305, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '語系管理',
                'uri' => 'world-language',
                'controller' => 'WorldLanguageController',
                'model' => 'WorldLanguage',
                'parent_id' => $menuParentId5,
                'link' => 'world-language',
                'icon' => null,
                'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],

            [
                'id' => $menuParentId6 = uuidl(),
                'title' => '系統參數',
                'uri' => 'root-parameter-control',
                'controller' => null,
                'model' => null,
                'parent_id' => $menuClassId3,
                'link' => null,
                'icon' => 'icon-filter',
                'sort' => 306, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '系統參數項目',
                'uri' => 'system-parameter-item',
                'controller' => 'SystemParameterItemController',
                'model' => 'SystemParameterItem',
                'parent_id' => $menuParentId6,
                'link' => 'system-parameter-item',
                'icon' => null,
                'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '系統參數群組',
                'uri' => 'system-parameter-group',
                'controller' => 'SystemParameterGroupController',
                'model' => 'SystemParameterGroup',
                'parent_id' => $menuParentId6,
                'link' => 'system-parameter-group',
                'icon' => null,
                'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
            [
                'id' => uuidl(),
                'title' => '欄位擴充管理',
                'uri' => 'column-extension',
                'controller' => 'ColumnExtensionController',
                'model' => 'ColumnExtension',
                'parent_id' => $menuParentId6,
                'link' => 'column-extension',
                'icon' => null,
                'sort' => 3, 'updated_at' => $timestamp, 'created_at' => $timestamp
            ],
        ]);
        DB::table('administrator_menu')->insert($administratorMenuData);

        // SiteDashboard
        $startDashboardId = $rowDashboardId = SeederHelper::getTableNextIncrement('site_dashboard');
        $rowDashboardId--;
        $siteDashboardData = [
            [
                'guard' => 'administrator',
                'title' => 'site_dashboard.title.' . ++$rowDashboardId,
                'presenter' => '\Minmax\Base\Administrator\GoogleAnalyticPresenter@visitedSource',
                'position' => json_encode([
                    'row' => '1',
                    'column' => '1',
                    'width' => '3',
                    'height' => '300'
                ]),
                'options' => null,
                'sort' => 1,
                'active' => true,
                'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'guard' => 'administrator',
                'title' => 'site_dashboard.title.' . ++$rowDashboardId,
                'presenter' => '\Minmax\Base\Administrator\GoogleAnalyticPresenter@currentVisitor',
                'position' => json_encode([
                    'row' => '1',
                    'column' => '2',
                    'width' => '3',
                    'height' => '60'
                ]),
                'options' => null,
                'sort' => 2,
                'active' => true,
                'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'guard' => 'administrator',
                'title' => 'site_dashboard.title.' . ++$rowDashboardId,
                'presenter' => '\Minmax\Base\Administrator\GoogleAnalyticPresenter@topBrowsers',
                'position' => json_encode([
                    'row' => '1',
                    'column' => '2',
                    'width' => '3',
                    'height' => '100'
                ]),
                'options' => null,
                'sort' => 3,
                'active' => true,
                'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'guard' => 'administrator',
                'title' => 'site_dashboard.title.' . ++$rowDashboardId,
                'presenter' => '\Minmax\Base\Administrator\GoogleAnalyticPresenter@worldSource',
                'position' => json_encode([
                    'row' => '1',
                    'column' => '3',
                    'width' => '6',
                    'height' => '440'
                ]),
                'options' => null,
                'sort' => 4,
                'active' => true,
                'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'guard' => 'administrator',
                'title' => 'site_dashboard.title.' . ++$rowDashboardId,
                'presenter' => '\Minmax\Base\Administrator\GoogleAnalyticPresenter@todayTotalVisitors',
                'position' => json_encode([
                    'row' => '2',
                    'column' => '1',
                    'width' => '3',
                    'height' => '0'
                ]),
                'options' => null,
                'sort' => 5,
                'active' => true,
                'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'guard' => 'administrator',
                'title' => 'site_dashboard.title.' . ++$rowDashboardId,
                'presenter' => '\Minmax\Base\Administrator\GoogleAnalyticPresenter@totalNewVisitors',
                'position' => json_encode([
                    'row' => '2',
                    'column' => '4',
                    'width' => '3',
                    'height' => '0'
                ]),
                'options' => null,
                'sort' => 8,
                'active' => true,
                'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'guard' => 'administrator',
                'title' => 'site_dashboard.title.' . ++$rowDashboardId,
                'presenter' => '\Minmax\Base\Administrator\GoogleAnalyticPresenter@pathViews',
                'position' => json_encode([
                    'row' => '3',
                    'column' => '1',
                    'width' => '4',
                    'height' => '270'
                ]),
                'options' => null,
                'sort' => 9,
                'active' => true,
                'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'guard' => 'administrator',
                'title' => 'site_dashboard.title.' . ++$rowDashboardId,
                'presenter' => '\Minmax\Base\Administrator\GoogleAnalyticPresenter@ageSource',
                'position' => json_encode([
                    'row' => '3',
                    'column' => '2',
                    'width' => '4',
                    'height' => '270'
                ]),
                'options' => null,
                'sort' => 10,
                'active' => true,
                'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'guard' => 'administrator',
                'title' => 'site_dashboard.title.' . ++$rowDashboardId,
                'presenter' => '\Minmax\Base\Administrator\GoogleAnalyticPresenter@referrerKeyword',
                'position' => json_encode([
                    'row' => '3',
                    'column' => '3',
                    'width' => '4',
                    'height' => '270'
                ]),
                'options' => null,
                'sort' => 11,
                'active' => true,
                'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];
        DB::table('site_dashboard')->insert($siteDashboardData);

        $siteDashboardLanguage = [
            'zh-Hant' => [
                ['title' => '流量來源'], ['title' => '線上使用者'], ['title' => '瀏覽器使用'], ['title' => '流量地區分布'],
                ['title' => '今日參觀量'], ['title' => '總流量'],
                ['title' => '熱門頁面'], ['title' => '瀏覽年齡層'], ['title' => '熱門關鍵字'],
            ],
            'zh-Hans' => [
                ['title' => '流量来源'], ['title' => '线上使用者'], ['title' => '浏览器使用'], ['title' => '流量地区分布'],
                ['title' => '今日参观量'], ['title' => '总流量'],
                ['title' => '热门页面'], ['title' => '浏览年龄层'], ['title' => '热门关键字'],
            ],
            'ja' => [
                ['title' => '流れのソース'], ['title' => 'オンラインユーザー'], ['title' => 'ブラウザ使用'], ['title' => '世界ソース分布'],
                ['title' => '今日の訪問数'], ['title' => '総トラフィック'],
                ['title' => '人気のページ'], ['title' => '閲覧年齢層'], ['title' => '人気のキーワード'],
            ],
            'en' => [
                ['title' => 'Traffic Source'], ['title' => 'Online Visitor'], ['title' => 'Top Browser'], ['title' => 'Source from World'],
                ['title' => 'Today Visitor'], ['title' => 'Total Traffic'],
                ['title' => 'Popular Page'], ['title' => 'Visitor Age'], ['title' => 'Referrer Keyword'],
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'site_dashboard', $siteDashboardLanguage, $languageList, $startDashboardId, false);

        DB::table('language_resource')->insert($languageResourceData);
    }
}
