<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class CreateSystemTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 欄位擴充管理
        Schema::create('column_extension', function (Blueprint $table) {
            $table->increments('id');
            $table->string('table_name')->comment('資料表');
            $table->string('column_name')->comment('主欄位名稱');
            $table->string('sub_column_name')->comment('欄位名稱');
            $table->string('title')->comment('欄位標籤');
            $table->json('options')->nullable()->comment('欄位設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
        });

        // 系統參數群組
        Schema::create('system_parameter_group', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique()->comment('群組代碼');
            $table->string('title')->comment('群組名稱');
            $table->json('options')->nullable()->comment('群組設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
        });

        // 系統參數項目
        Schema::create('system_parameter_item', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('group_id')->comment('群組ID');
            $table->string('value')->comment('參數數值');
            $table->string('label')->comment('參數名稱');
            $table->json('options')->nullable()->comment('參數設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');

            $table->unique(['group_id', 'value'], 'idx-group_id-value');

            $table->foreign('group_id')->references('id')->on('system_parameter_group')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // 網站參數群組
        Schema::create('site_parameter_group', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique()->comment('群組代碼');
            $table->string('title')->comment('群組名稱');
            $table->string('category')->nullable()->comment('群組類別');
            $table->json('options')->nullable()->comment('群組設定');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('editable')->default(true)->comment('可否編輯');
        });

        // 網站參數項目
        Schema::create('site_parameter_item', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('group_id')->comment('群組ID');
            $table->string('value')->nullable()->comment('參數數值');
            $table->string('label')->comment('參數名稱');
            $table->string('details')->nullable()->comment('參數細節');         // {description, editor, pic}
            $table->json('options')->nullable()->comment('參數設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');

            $table->unique(['group_id', 'value'], 'idx-group_id-value');

            $table->foreign('group_id')->references('id')->on('site_parameter_group')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // 網站資訊
        Schema::create('web_data', function (Blueprint $table) {
            $table->string('id', 64)->primary();
            $table->string('guard', 16)->comment('平台');
            $table->string('website_name')->comment('網站名稱');
            $table->string('system_language')->comment('預設語系');
            $table->string('system_email')->comment('系統信箱');
            $table->string('system_mobile')->nullable()->comment('系統電話');
            $table->string('system_url')->comment('網站網址');
            $table->json('system_logo')->nullable()->comment('網站Logo');
            $table->string('company')->comment('公司資訊');                 // {name, name_en, id}
            $table->string('contact')->comment('聯絡資訊');                 // {phone, fax, email, address, map, lng, lat}
            $table->json('social')->nullable()->comment('社群連結');        // {facebook, instagram, youtube}
            $table->string('seo')->comment('搜尋引擎');                     // {meta_description, meta_keywords, og_image}
            $table->string('options')->nullable()->comment('網站設定');     // {head, body, foot}
            $table->string('offline_text')->comment('網站離線訊息');
            $table->boolean('active')->default(true)->comment('網站狀態');
            $table->timestamps();
        });

        // 前臺選單
        Schema::create('web_menu', function (Blueprint $table) {
            $table->string('id', 64)->primary();
            $table->string('parent_id', 64)->nullable()->comment('上層目錄');
            $table->string('title')->comment('選單名稱');
            $table->string('uri')->nullable()->comment('識別標籤');
            $table->string('controller')->nullable()->comment('Controller 名稱');
            $table->string('model')->nullable()->comment('Model 名稱');
            $table->string('link')->nullable()->comment('項目連結');
            $table->string('permission_key', 128)->nullable()->comment('權限綁定代碼');
            $table->json('options')->nullable()->comment('選單設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('editable')->default(true)->comment('可否編輯');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // 防火牆
        Schema::create('firewall', function (Blueprint $table) {
            $table->increments('id');
            $table->string('guard', 16)->comment('平台');
            $table->ipAddress('ip')->comment('IP 位址');
            $table->boolean('rule')->default(false)->comment('規則');     // 1:允許 0:禁止
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();

            $table->unique(['guard', 'ip', 'rule'], 'idx-guard-ip-rule');
        });

        // 儀錶板
        Schema::create('site_dashboard', function (Blueprint $table) {
            $table->increments('id');
            $table->string('guard', 16)->comment('平台');
            $table->string('title')->comment('標題');
            $table->string('presenter')->comment('模組涵式');   // \Minmax\Base\Admin\GoogleAnalyticPresenter@activeVisitors
            $table->json('position')->comment('設置位置');      // {row, column, width, height}
            $table->json('options')->nullable()->comment('模組設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');     // work at the same column of the same row
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // 服務設定
        Schema::create('service_config', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique()->comment('服務代碼');
            $table->string('group')->comment('服務群組');
            $table->string('title')->comment('標題');
            $table->text('host')->comment('連線位置');
            $table->json('options')->nullable()->comment('服務設定');
            $table->json('parameters')->nullable()->comment('服務參數');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(false)->comment('啟用狀態');
            $table->timestamps();
        });

        // 系統紀錄
        Schema::create('system_log', function (Blueprint $table) {
            $table->string('guard', 16)->comment('平台');
            $table->string('uri')->comment('操作網址');
            $table->string('action', 64)->comment('動作');
            $table->text('id')->comment('項目ID');
            $table->string('username', 255)->comment('帳號');
            $table->ipAddress('ip')->comment('IP 位置');
            $table->text('remark')->nullable()->comment('文字說明');
            $table->boolean('result')->default(true)->comment('狀態');
            $table->timestamp('created_at')->useCurrent();
        });

        // 登入紀錄
        Schema::create('login_log', function (Blueprint $table) {
            $table->string('guard', 16)->comment('平台');
            $table->string('username', 255)->comment('帳號');
            $table->ipAddress('ip')->comment('IP 位置');
            $table->text('remark')->nullable()->comment('文字說明');
            $table->boolean('result')->default(true)->comment('狀態');
            $table->timestamp('created_at')->useCurrent();
        });

        // Laravel 佇列
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('queue')->index();
            $table->longText('payload');
            $table->unsignedTinyInteger('attempts');
            $table->unsignedInteger('reserved_at')->nullable();
            $table->unsignedInteger('available_at');
            $table->unsignedInteger('created_at');
        });

        // Laravel 失敗佇列
        Schema::create('failed_jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('connection');
            $table->text('queue');
            $table->longText('payload');
            $table->longText('exception');
            $table->timestamp('failed_at')->useCurrent();
        });

        // 建立預設資料
        $this->insertDatabase();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('failed_jobs');
        Schema::dropIfExists('jobs');
        Schema::dropIfExists('login_log');
        Schema::dropIfExists('system_log');
        Schema::dropIfExists('service_config');
        Schema::dropIfExists('site_dashboard');
        Schema::dropIfExists('firewall');
        Schema::dropIfExists('web_menu');
        Schema::dropIfExists('web_data');
        Schema::dropIfExists('site_parameter_item');
        Schema::dropIfExists('site_parameter_group');
        Schema::dropIfExists('system_parameter_item');
        Schema::dropIfExists('system_parameter_group');
        Schema::dropIfExists('column_extension');
    }

    /**
     * Insert default data
     *
     * @return void
     */
    public function insertDatabase()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        // 系統參數
        $startGroupId = $groupIndex = SeederHelper::getTableNextIncrement('system_parameter_group');
        $groupIndex--;
        $startItemId = $itemIndex = SeederHelper::getTableNextIncrement('system_parameter_item');
        $itemIndex--;
        $systemParameterGroupData = [];
        $systemParameterItemData = [];

        array_push($systemParameterGroupData, ['code' => 'active', 'title' => 'system_parameter_group.title.' . ++$groupIndex]);
        $systemParameterItemData = array_merge($systemParameterItemData, [
            ['group_id' => $groupIndex, 'value' => '1', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'danger']), 'sort' => 1],
            ['group_id' => $groupIndex, 'value' => '0', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'secondary']), 'sort' => 2],
        ]);
        array_push($systemParameterGroupData, ['code' => 'active_admin', 'title' => 'system_parameter_group.title.' . ++$groupIndex]);
        $systemParameterItemData = array_merge($systemParameterItemData, [
            ['group_id' => $groupIndex, 'value' => '1', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'danger']), 'sort' => 1],
            ['group_id' => $groupIndex, 'value' => '0', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'secondary']), 'sort' => 2],
        ]);
        array_push($systemParameterGroupData, ['code' => 'result', 'title' => 'system_parameter_group.title.' . ++$groupIndex]);
        $systemParameterItemData = array_merge($systemParameterItemData, [
            ['group_id' => $groupIndex, 'value' => '1', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'success']), 'sort' => 1],
            ['group_id' => $groupIndex, 'value' => '0', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'danger']), 'sort' => 2],
        ]);
        array_push($systemParameterGroupData, ['code' => 'rule', 'title' => 'system_parameter_group.title.' . ++$groupIndex]);
        $systemParameterItemData = array_merge($systemParameterItemData, [
            ['group_id' => $groupIndex, 'value' => '1', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'success']), 'sort' => 1],
            ['group_id' => $groupIndex, 'value' => '0', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'danger']), 'sort' => 2],
        ]);
        array_push($systemParameterGroupData, ['code' => 'top', 'title' => 'system_parameter_group.title.' . ++$groupIndex]);
        $systemParameterItemData = array_merge($systemParameterItemData, [
            ['group_id' => $groupIndex, 'value' => '0', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'info']), 'sort' => 1],
            ['group_id' => $groupIndex, 'value' => '1', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'waring']), 'sort' => 2],
        ]);
        array_push($systemParameterGroupData, ['code' => 'visible', 'title' => 'system_parameter_group.title.' . ++$groupIndex]);
        $systemParameterItemData = array_merge($systemParameterItemData, [
            ['group_id' => $groupIndex, 'value' => '1', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'danger']), 'sort' => 1],
            ['group_id' => $groupIndex, 'value' => '0', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'secondary']), 'sort' => 2],
        ]);
        array_push($systemParameterGroupData, ['code' => 'editable', 'title' => 'system_parameter_group.title.' . ++$groupIndex]);
        $systemParameterItemData = array_merge($systemParameterItemData, [
            ['group_id' => $groupIndex, 'value' => '1', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'danger']), 'sort' => 1],
            ['group_id' => $groupIndex, 'value' => '0', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'secondary']), 'sort' => 2],
        ]);
        array_push($systemParameterGroupData, ['code' => 'target', 'title' => 'system_parameter_group.title.' . ++$groupIndex]);
        $systemParameterItemData = array_merge($systemParameterItemData, [
            ['group_id' => $groupIndex, 'value' => '_self', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'secondary']), 'sort' => 1],
            ['group_id' => $groupIndex, 'value' => '_blank', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'danger']), 'sort' => 2],
        ]);
        array_push($systemParameterGroupData, ['code' => 'searchable', 'title' => 'system_parameter_group.title.' . ++$groupIndex]);
        $systemParameterItemData = array_merge($systemParameterItemData, [
            ['group_id' => $groupIndex, 'value' => '1', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'danger']), 'sort' => 1],
            ['group_id' => $groupIndex, 'value' => '0', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'secondary']), 'sort' => 2],
        ]);
        array_push($systemParameterGroupData, ['code' => 'starred', 'title' => 'system_parameter_group.title.' . ++$groupIndex]);
        $systemParameterItemData = array_merge($systemParameterItemData, [
            ['group_id' => $groupIndex, 'value' => '0', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'text-muted']), 'sort' => 1],
            ['group_id' => $groupIndex, 'value' => '1', 'label' => 'system_parameter_item.label.' . ++$itemIndex, 'options' => json_encode(['class' => 'text-main']), 'sort' => 2],
        ]);

        DB::table('system_parameter_group')->insert($systemParameterGroupData);
        DB::table('system_parameter_item')->insert($systemParameterItemData);

        $systemParameterGroupLanguage = [
            'zh-Hant' => [
                ['title' => '啟用狀態'], ['title' => '後臺啟用'], ['title' => '操作結果'], ['title' => '防火牆規則'],
                ['title' => '置頂狀態'], ['title' => '顯示狀態'], ['title' => '可否編輯'], ['title' => '目標視窗'],
                ['title' => '搜尋顯示'], ['title' => '關注狀態']
            ],
            'zh-Hans' => [
                ['title' => '启用状态'], ['title' => '后台启用'], ['title' => '操作结果'], ['title' => '防火墙规则'],
                ['title' => '置顶状态'], ['title' => '显示状态'], ['title' => '可否编辑'], ['title' => '目的页框'],
                ['title' => '搜寻显示'], ['title' => '关注状态']
            ],
            'ja' => [
                ['title' => '有効状態'], ['title' => '裏台有効'], ['title' => '操作結果'], ['title' => 'ルール'],
                ['title' => '頂上状態'], ['title' => '表示状態'], ['title' => '変更可能'], ['title' => '対象ウィンドウ'],
                ['title' => '検索表示'], ['title' => '注目して状态']
            ],
            'en' => [
                ['title' => 'Active'], ['title' => 'Admin Enable'], ['title' => 'Result'], ['title' => 'Rule'],
                ['title' => 'Top'], ['title' => 'Visible'], ['title' => 'Editable'], ['title' => 'Target Window'],
                ['title' => 'Searchable'], ['title' => 'Starred']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'system_parameter_group', $systemParameterGroupLanguage, $languageList, $startGroupId, false);

        $systemParameterItemLanguage = [
            'zh-Hant' => [
                ['label' => '啟用'], ['label' => '停用'],
                ['label' => '啟用'], ['label' => '停用'],
                ['label' => '成功'], ['label' => '失敗'],
                ['label' => '允許'], ['label' => '禁止'],
                ['label' => '正常'], ['label' => '置頂'],
                ['label' => '顯示'], ['label' => '隱藏'],
                ['label' => '啟用'], ['label' => '停用'],
                ['label' => '原本視窗'], ['label' => '開啟新視窗'],
                ['label' => '顯示'], ['label' => '隱藏'],
                ['label' => '不關注'], ['label' => '關注'],
            ],
            'zh-Hans' => [
                ['label' => '启用'], ['label' => '停用'],
                ['label' => '启用'], ['label' => '停用'],
                ['label' => '成功'], ['label' => '失败'],
                ['label' => '允许'], ['label' => '禁止'],
                ['label' => '正常'], ['label' => '置顶'],
                ['label' => '显示'], ['label' => '隐藏'],
                ['label' => '启用'], ['label' => '停用'],
                ['label' => '原本页框'], ['label' => '开启新页框'],
                ['label' => '显示'], ['label' => '隐藏'],
                ['label' => '不关注'], ['label' => '关注'],
            ],
            'ja' => [
                ['label' => '有効'], ['label' => '無効'],
                ['label' => '有効'], ['label' => '無効'],
                ['label' => '成功'], ['label' => '失敗'],
                ['label' => '許可'], ['label' => '禁止'],
                ['label' => '一般'], ['label' => '頂上'],
                ['label' => '表示'], ['label' => '隠す'],
                ['label' => '有効'], ['label' => '無効'],
                ['label' => '自分のウィンドウ'], ['label' => '新しいウィンドウを開く'],
                ['label' => '表示'], ['label' => '隠す'],
                ['label' => '注目していない'], ['label' => '注目して'],
            ],
            'en' => [
                ['label' => 'On'], ['label' => 'Off'],
                ['label' => 'On'], ['label' => 'Off'],
                ['label' => 'Success'], ['label' => 'Failed'],
                ['label' => 'Allowed'], ['label' => 'Denied'],
                ['label' => 'Default'], ['label' => 'Top'],
                ['label' => 'Show'], ['label' => 'Hide'],
                ['label' => 'On'], ['label' => 'Off'],
                ['label' => 'Self'], ['label' => 'New Window'],
                ['label' => 'Show'], ['label' => 'Hide'],
                ['label' => 'Unstarred'], ['label' => 'Starred'],
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'system_parameter_item', $systemParameterItemLanguage, $languageList, $startItemId, false);

        // 網站參數
        $startGroupId = $rowGroupId = SeederHelper::getTableNextIncrement('site_parameter_group');
        $rowGroupId--;

        $siteGroupData = [
            ['code' => 'templates', 'title' => 'site_parameter_group.title.' . ++$rowGroupId, 'category' => null, 'editable' => true],
        ];
        DB::table('site_parameter_group')->insert($siteGroupData);

        // 多語系
        $siteGroupLanguage = [
            'zh-Hant' => [
                ['title' => '編輯器樣板']
            ],
            'zh-Hans' => [
                ['title' => '编辑器样板']
            ],
            'ja' => [
                ['title' => 'エディタテンプレート']
            ],
            'en' => [
                ['title' => 'Editor Template']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'site_parameter_group', $siteGroupLanguage, $languageList, $startGroupId, false);

        // 網站基本資訊
        $webData = [
            [
                'id' => uuidl(),
                'guard' => 'administrator',
                'website_name' => '總後臺管理系統',
                'system_language' => 'zh-Hant',
                'system_email' => config('mail.from.address'),
                'system_url' => config('app.url') . '/administrator',
                'system_logo' => json_encode([['path' => '/static/admin/images/common/logo-b.png']]),
                'company' => json_encode([
                    'name' => config('app.author'),
                    'name_en' => config('app.author_en'),
                    'id' => '24252151'
                ]),
                'contact' => json_encode([
                    'phone' => '04-24350749', 'fax' => '',
                    'email' => 'info@ecreative.tw',
                    'address' => '臺中市北屯區東山路一段147巷49號',
                    'map' => 'https://goo.gl/maps/CRMLfK3xWA62', 'lng' => '', 'lat' => ''
                ]),
                'social' => json_encode([
                    'facebook' => 'https://www.facebook.com',
                    'instagram' => 'https://www.instagram.com',
                    'youtube' => 'https://www.youtube.com',
                ]),
                'seo' => json_encode([
                    'meta_description' => '',
                    'meta_keywords' => ''
                ]),
                'options' => json_encode([
                    'head' => '',
                    'body' => '',
                    'foot' => ''
                ]),
                'offline_text' => '網站正在維護中，很快就會回來，請稍候一下。',
                'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp,
            ],
            [
                'id' => uuidl(),
                'guard' => 'admin',
                'website_name' => '後臺管理系統',
                'system_language' => 'zh-Hant',
                'system_email' => config('mail.from.address'),
                'system_url' => config('app.url') . '/siteadmin',
                'system_logo' => json_encode([['path' => '/static/admin/images/common/logo-b.png']]),
                'company' => json_encode([
                    'name' => config('app.author'),
                    'name_en' => config('app.author_en'),
                    'id' => '24252151'
                ]),
                'contact' => json_encode([
                    'phone' => '04-24350749', 'fax' => '',
                    'email' => 'info@ecreative.tw',
                    'address' => '臺中市北屯區東山路一段147巷49號',
                    'map' => 'https://goo.gl/maps/CRMLfK3xWA62', 'lng' => '', 'lat' => ''
                ]),
                'social' => json_encode([
                    'facebook' => 'https://www.facebook.com',
                    'instagram' => 'https://www.instagram.com',
                    'youtube' => 'https://www.youtube.com',
                ]),
                'seo' => json_encode([
                    'meta_description' => '',
                    'meta_keywords' => ''
                ]),
                'options' => json_encode([
                    'head' => '',
                    'body' => '',
                    'foot' => ''
                ]),
                'offline_text' => '網站正在維護中，很快就會回來，請稍候一下。',
                'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp,
            ],
            [
                'id' => uuidl(),
                'guard' => 'web',
                'website_name' => config('app.author'),
                'system_language' => 'zh-Hant',
                'system_email' => config('mail.from.address'),
                'system_url' => config('app.url'),
                'system_logo' => json_encode([['path' => '/static/admin/images/common/logo-b.png']]),
                'company' => json_encode([
                    'name' => config('app.author'),
                    'name_en' => config('app.author_en'),
                    'id' => '24252151'
                ]),
                'contact' => json_encode([
                    'phone' => '04-24350749', 'fax' => '',
                    'email' => 'info@ecreative.tw',
                    'address' => '臺中市北屯區東山路一段147巷49號',
                    'map' => 'https://goo.gl/maps/CRMLfK3xWA62', 'lng' => '', 'lat' => ''
                ]),
                'social' => json_encode([
                    'facebook' => 'https://www.facebook.com',
                    'youtube' => 'https://www.youtube.com',
                    'instagram' => 'https://www.instagram.com',
                ]),
                'seo' => json_encode([
                    'meta_description' => '',
                    'meta_keywords' => ''
                ]),
                'options' => json_encode([
                    'head' => '',
                    'body' => '',
                    'foot' => ''
                ]),
                'offline_text' => '網站正在維護中，很快就會回來，請稍候一下。',
                'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp,
            ],
        ];

        SeederHelper::setLanguageExchange($webData, $languageResourceData, 'web_data', [
            'website_name', 'company', 'contact', 'seo', 'options', 'offline_text'
        ], $languageList);

        DB::table('web_data')->insert($webData);

        // 欄位擴充
        $startExtensionId = $extensionRowId = SeederHelper::getTableNextIncrement('column_extension');

        $columnExtensionData = [
            ['table_name' => 'web_data', 'column_name' => 'company', 'sub_column_name' => 'name', 'sort' => 1, 'active' => true,
                'title' => '公司名稱', 'options' => json_encode(['method' => 'getFieldText', 'required' => true])],
            ['table_name' => 'web_data', 'column_name' => 'company', 'sub_column_name' => 'name_en', 'sort' => 2, 'active' => false,
                'title' => '公司英文名稱', 'options' => json_encode(['method' => 'getFieldText'])],
            ['table_name' => 'web_data', 'column_name' => 'company', 'sub_column_name' => 'id', 'sort' => 3, 'active' => false,
                'title' => '統一編號', 'options' => json_encode(['method' => 'getFieldText'])],
            ['table_name' => 'web_data', 'column_name' => 'contact', 'sub_column_name' => 'phone', 'sort' => 1, 'active' => true,
                'title' => '客服電話', 'options' => json_encode(['method' => 'getFieldText', 'required' => true, 'type' => 'tel'])],
            ['table_name' => 'web_data', 'column_name' => 'contact', 'sub_column_name' => 'fax', 'sort' => 2, 'active' => false,
                'title' => '傳真號碼', 'options' => json_encode(['method' => 'getFieldText', 'type' => 'tel'])],
            ['table_name' => 'web_data', 'column_name' => 'contact', 'sub_column_name' => 'email', 'sort' => 3, 'active' => true,
                'title' => '客服信箱', 'options' => json_encode(['method' => 'getFieldText', 'required' => true, 'type' => 'email'])],
            ['table_name' => 'web_data', 'column_name' => 'contact', 'sub_column_name' => 'address', 'sort' => 4, 'active' => true,
                'title' => '公司地址', 'options' => json_encode(['method' => 'getFieldText'])],
            ['table_name' => 'web_data', 'column_name' => 'contact', 'sub_column_name' => 'map', 'sort' => 5, 'active' => true,
                'title' => '地址連結', 'options' => json_encode(['method' => 'getFieldText', 'size' => 10])],
            ['table_name' => 'web_data', 'column_name' => 'contact', 'sub_column_name' => 'lng', 'sort' => 6, 'active' => false,
                'title' => '地圖經度', 'options' => json_encode(['method' => 'getFieldText', 'size' => 2])],
            ['table_name' => 'web_data', 'column_name' => 'contact', 'sub_column_name' => 'lat', 'sort' => 7, 'active' => false,
                'title' => '地圖緯度', 'options' => json_encode(['method' => 'getFieldText', 'size' => 2])],
            ['table_name' => 'web_data', 'column_name' => 'social', 'sub_column_name' => 'facebook', 'sort' => 1, 'active' => true,
                'title' => 'Facebook', 'options' => json_encode(['method' => 'getFieldText', 'icon' => 'icon-facebook3'])],
            ['table_name' => 'web_data', 'column_name' => 'social', 'sub_column_name' => 'instagram', 'sort' => 2, 'active' => false,
                'title' => 'Instagram', 'options' => json_encode(['method' => 'getFieldText', 'icon' => 'icon-instagram2'])],
            ['table_name' => 'web_data', 'column_name' => 'social', 'sub_column_name' => 'youtube', 'sort' => 3, 'active' => false,
                'title' => 'Youtube', 'options' => json_encode(['method' => 'getFieldText', 'icon' => 'icon-youtube2'])],
        ];

        SeederHelper::setLanguageExchange($columnExtensionData, $languageResourceData, 'column_extension', 'title', $languageList, $startExtensionId);

        DB::table('column_extension')->insert($columnExtensionData);

        // 服務設定
        $startServiceId = $rowServiceId = SeederHelper::getTableNextIncrement('service_config');
        $rowServiceId--;

        $serviceConfigData = [
            [
                'code' => 'smtp',
                'group' => 'email',
                'title' => 'service_config.title.' . ++$rowServiceId,
                'host' => config('mail.host') . ':' . config('mail.port'),
                'options' => json_encode([
                    'driver' => config('mail.driver'),
                    'username' => config('mail.username'),
                    'password' => config('mail.password'),
                    'encryption' => config('mail.encryption'),
                    'from.address' => config('mail.from.address'),
                    'from.name' => config('mail.from.name'),
                ]),
                'parameters' => json_encode([
                    'driver' => '連線方式',
                    'username' => '連線帳號',
                    'password' => '連線密碼',
                    'encryption' => '加密方式',
                    'from.address' => '寄件者信箱',
                    'from.name' => '寄件者名稱',
                ]),
                'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];
        DB::table('service_config')->insert($serviceConfigData);

        // 多語系
        $siteServiceLanguage = [
            'zh-Hant' => [
                ['title' => '電子郵件伺服器']
            ],
            'zh-Hans' => [
                ['title' => '电子邮件伺服器']
            ],
            'ja' => [
                ['title' => 'メールサーバー']
            ],
            'en' => [
                ['title' => 'Email Server']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'service_config', $siteServiceLanguage, $languageList, $startServiceId, false);

        DB::table('language_resource')->insert($languageResourceData);
    }
}
