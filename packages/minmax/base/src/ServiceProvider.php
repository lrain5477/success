<?php

namespace Minmax\Base;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRouteLocalization();

        $this->configureServiceConfig();

        $this->loadRoutesFrom(__DIR__ . '/routes/admin.php');
        $this->loadRoutesFrom(__DIR__ . '/routes/administrator.php');
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/translations', 'MinmaxBase');
        $this->loadViewsFrom(__DIR__ . '/views', 'MinmaxBase');
        $this->loadBreadcrumbs();
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerHelper();
    }

    protected function configureRouteLocalization()
    {
        $defaultLocale = config('app.locale');

        $pathUriSet = explode('/', request()->path());

        if (in_array('administrator', $pathUriSet)) {
            try {
                if ($webData = DB::table('web_data')->where('guard', 'administrator')->first()) {
                    $defaultLocale = $webData->system_language;
                }
            } catch (\Exception $e) {}
        }

        if (in_array('siteadmin', $pathUriSet)) {
            try {
                if ($webData = DB::table('web_data')->where('guard', 'admin')->first()) {
                    $defaultLocale = $webData->system_language;
                }
            } catch (\Exception $e) {}
        }

        try {
            $languageSet = DB::table('world_language')
                ->where('active', true)
                ->orderBy('sort')
                ->get()
                ->sortBy(function ($item) use ($defaultLocale) {
                    return $item->code == $defaultLocale ? 0 : $item->sort;
                })
                ->mapWithKeys(function ($item) {
                    return [
                        $item->code => [
                            'name' => $item->name,
                            'script' => json_decode($item->options, true)['script'] ?? '',
                            'native' => $item->native
                        ]
                    ];
                })
                ->toArray();
        } catch (\Exception $e) {
            $languageSet = [
                'zh-Hant' => ['name' => '繁體中文', 'script' => 'Hant', 'native' => '繁體中文']
            ];
        }

        config([
            'laravellocalization.supportedLocales' => $languageSet,
            'laravellocalization.useAcceptLanguageHeader' => true,
            'laravellocalization.hideDefaultLocaleInURL' => ! config('app.locale_uri'),
        ]);
    }

    protected function configureServiceConfig()
    {
        // Email Config
        if ($this->checkNotInitial()) {
            customMailer('email', true);
        }
    }

    protected function loadBreadcrumbs()
    {
        require (__DIR__ . '/breadcrumbs/administrator.php');
        require (__DIR__ . '/breadcrumbs/admin.php');
    }

    protected function registerHelper()
    {
        include(__DIR__ . '/Helpers/ShortcutHelper.php');
    }

    protected function checkNotInitial()
    {
        if (! $this->app->runningInConsole()) {
            return true;
        }

        $exceptCommands = [
            'cache:clear', 'config:cache', 'route:cache', 'config:clear', 'route:clear',
            'migrate', 'migrate:install', 'migrate:status',
            'migrate:fresh', 'migrate:refresh', 'migrate:reset', 'migrate:rollback'
        ];

        if (! in_array($_SERVER['argv'][1] ?? '', $exceptCommands)) {
            return true;
        }

        return false;
    }
}
