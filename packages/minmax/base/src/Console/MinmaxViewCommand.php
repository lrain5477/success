<?php

namespace Minmax\Base\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MinmaxViewCommand extends Command
{
    /**
     * The Laravel application instance.
     *
     * @var \Illuminate\Foundation\Application
     */
    protected $laravel;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'minmax:view';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create custom crud views';

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Views';

    /**
     * Create a new controller creator command instance.
     *
     * @param  \Illuminate\Filesystem\Filesystem  $files
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Get the stub files for the generator.
     *
     * @param  string $type
     * @return string
     */
    protected function getStub($type)
    {
        $stubs = [
            'create' => __DIR__.'/../../resources/stubs/view-create.blade.stub',
            'edit' => __DIR__.'/../../resources/stubs/view-edit.blade.stub',
            'index' => __DIR__.'/../../resources/stubs/view-index.blade.stub',
            'show' => __DIR__.'/../../resources/stubs/view-show.blade.stub',
        ];

        return array_get($stubs, $type);
    }

    /**
     * Execute the console command.
     *
     * @return bool|void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $name = $this->getNameInput();

        $paths = $this->getPaths($name);

        $duplicate = 0;
        foreach ($paths as $viewType => $viewPath) {
            // First we will check to see if the class already exists. If it does, we don't want
            // to create the class and overwrite the user's code. So, we will bail out so the
            // code is untouched. Otherwise, we will continue generating this class' files.
            if ((!$this->hasOption('force') ||
                    !$this->option('force')) &&
                $this->alreadyExists($viewPath)) {
                $this->error($this->type . ' (' . $viewType . ') already exists!');

                $duplicate++;
                continue;
            }

            // Next, we will generate the path to the location where this class' file should get
            // written. Then, we will build the class and make the proper replacements on the
            // stub files so that it gets the correctly formatted namespace and class name.
            $this->makeDirectory($viewPath);

            $this->files->put($viewPath, $this->buildView($viewType, $name));
        }

        if ($duplicate == count($paths)) {
            return false;
        }

        $this->info($this->type . ' created successfully.');
    }

    /**
     * Determine if the view file already exists.
     *
     * @param  string  $path
     * @return bool
     */
    protected function alreadyExists($path)
    {
        return $this->files->exists($path);
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return array
     */
    protected function getPaths($name)
    {
        $inputGuard = $this->option('guard');
        $inputPackage = $this->option('package');

        if ($inputPackage == 'app') {
            $rootPath = $this->laravel->basePath() . '\\resources\\views/';
        } else {
            $rootPath = $this->laravel->basePath() . '\\packages\\' . str_replace('/', '\\', $inputPackage) . '\\src/views/';
        }

        return [
            'create' => $rootPath . kebab_case($inputGuard) . '/' . $name . '/create.blade.php',
            'edit'   => $rootPath . kebab_case($inputGuard) . '/' . $name . '/edit.blade.php',
            'index'  => $rootPath . kebab_case($inputGuard) . '/' . $name . '/index.blade.php',
            'show'   => $rootPath . kebab_case($inputGuard) . '/' . $name . '/show.blade.php',
        ];
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param  string  $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (! $this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);
        }

        return $path;
    }

    /**
     * Build the view with the given type and name.
     *
     * @param  string $type
     * @param  string $name
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function buildView($type, $name)
    {
        $stub = $this->files->get($this->getStub($type));

        return $this->replaceNamespace($stub)->replaceName($stub, $name)->replaceClass($stub, $name);
    }

    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @return $this
     */
    protected function replaceNamespace(&$stub)
    {
        $inputGuard = $this->option('guard');
        $inputPackage = $this->option('package');

        $modelNamespace = $this->rootNamespace() . 'Models';

        if ($inputPackage == 'app') {
            $presenterNamespace = $this->rootNamespace() . 'Presenters\\' . studly_case($inputGuard);
        } else {
            $presenterNamespace = $this->rootNamespace() . studly_case($inputGuard);
        }

        $stub = str_replace(
            ['DummyModelNamespace', 'DummyPresenterNamespace'],
            [$modelNamespace, $presenterNamespace],
            $stub
        );

        return $this;
    }

    /**
     * Replace the kinds of name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceName(&$stub, $name)
    {
        $inputGuard = $this->option('guard');
        $inputPackage = $this->option('package');

        $guardUpper = studly_case($inputGuard);
        $guardLower = strtolower($inputGuard);
        $permissionGroup = camel_case($name);

        if ($inputPackage == 'app') {
            $packagePrefix = '';
        } else {
            $packagePrefix = str_replace(' ', '', title_case(str_replace('/', ' ', $inputPackage))) . '::';
        }

        $stub = str_replace(
            ['DummyGuardUpper', 'DummyGuardLower', 'DummyPermission', 'DummyPackagePrefix'],
            [$guardUpper, $guardLower, $permissionGroup, $packagePrefix],
            $stub
        );

        return $this;
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $modelClass = studly_case($name);
        $presenterClass = studly_case($name) . 'Presenter';

        return str_replace(
            ['DummyModelClass', 'DummyPresenterClass'],
            [$modelClass, $presenterClass],
            $stub
        );
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getNameInput()
    {
        return trim($this->argument('name'));
    }

    /**
     * Get the root namespace for the class.
     *
     * @return string
     */
    protected function rootNamespace()
    {
        $inputPackage = $this->option('package');

        if ($inputPackage == 'app') {
            return $this->laravel->getNamespace();
        } else {
            return str_replace(' ', '\\', title_case(str_replace('/', ' ', $inputPackage))) . '\\';
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the uri'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['guard', null, InputOption::VALUE_OPTIONAL, 'Witch guard for generate', 'admin'],
            ['package', null, InputOption::VALUE_OPTIONAL, 'Can set your package path with lower case', 'app'],
        ];
    }
}
