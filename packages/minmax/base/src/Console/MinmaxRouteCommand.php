<?php

namespace Minmax\Base\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MinmaxRouteCommand extends Command
{
    /**
     * The Laravel application instance.
     *
     * @var \Illuminate\Foundation\Application
     */
    protected $laravel;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'minmax:route';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create custom crud routes';

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Routes';

    /**
     * Create a new controller creator command instance.
     *
     * @param  \Illuminate\Filesystem\Filesystem  $files
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Get the stub files for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        $inputPackage = $this->option('package');

        if ($inputPackage == 'app') {
            return __DIR__.'/../../resources/stubs/route-app.stub';
        } else {
            return __DIR__.'/../../resources/stubs/route-package.stub';
        }
    }

    /**
     * Execute the console command.
     *
     * @return bool|void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $name = $this->getNameInput();

        $path = $this->getPath();

        // First we will check to see if the class already exists. If it does, we don't want
        // to create the class and overwrite the user's code. So, we will bail out so the
        // code is untouched. Otherwise, we will continue generating this class' files.
//        if ((!$this->hasOption('force') ||
//                !$this->option('force')) &&
//            $this->alreadyExists($path)) {
//            $this->error($this->type . ' already exists!');
//
//            return false;
//        }

        // Next, we will generate the path to the location where this class' file should get
        // written. Then, we will build the class and make the proper replacements on the
        // stub files so that it gets the correctly formatted namespace and class name.
        $this->makeDirectory($path);

        $this->files->put($path, $this->buildRoute($path, $name));

        $this->info($this->type . ' created successfully.');
    }

    /**
     * Determine if the view file already exists.
     *
     * @param  string  $path
     * @return bool
     */
    protected function alreadyExists($path)
    {
        return $this->files->exists($path);
    }

    /**
     * Get the destination class path.
     *
     * @return string
     */
    protected function getPath()
    {
        $inputGuard = $this->option('guard');
        $inputPackage = $this->option('package');

        if ($inputPackage == 'app') {
            $rootPath = $this->laravel->basePath() . '\\routes\\';
        } else {
            $rootPath = $this->laravel->basePath() . '\\packages\\' . str_replace('/', '\\', $inputPackage) . '\\src/routes/';
        }

        return $rootPath . kebab_case($inputGuard) . '.php';
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param  string  $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (! $this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);
        }

        return $path;
    }

    /**
     * Build the route with the given path and name.
     *
     * @param  string $path
     * @param  string $name
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function buildRoute($path, $name)
    {
        $stub = $this->files->get($this->alreadyExists($path) ? $path : $this->getStub());

        return $this->replaceNamespace($stub)->replaceName($stub)->insertRoutes($stub, $name);
    }

    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @return $this
     */
    protected function replaceNamespace(&$stub)
    {
        $packageNamespace = $this->getDefaultNamespace($this->rootNamespace());

        $stub = str_replace(
            ['DummyNamespace'],
            [$packageNamespace],
            $stub
        );

        return $this;
    }

    /**
     * Replace the kinds of name for the given stub.
     *
     * @param  string  $stub
     * @return $this
     */
    protected function replaceName(&$stub)
    {
        $inputGuard = $this->option('guard');

        $guardUpper = studly_case($inputGuard);
        $guardLower = strtolower($inputGuard);

        switch ($inputGuard) {
            case 'admin':
                $routePrefix = 'siteadmin'; break;
            case 'web':
                $routePrefix = ''; break;
            default:
                $routePrefix = $inputGuard;
        }

        $stub = str_replace(
            ['DummyGuardUpper', 'DummyGuardLower', 'DummyPrefix'],
            [$guardUpper, $guardLower, $routePrefix],
            $stub
        );

        return $this;
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function insertRoutes($stub, $name)
    {
        $modelClass = studly_case($name);
        $controllerClass = studly_case($name) . 'Controller';
        $routeUri = kebab_case($name);

        $routeString = <<<STR
        /*
         * {$modelClass}
         */
        Route::get('{$routeUri}', '{$controllerClass}@index')->name('{$routeUri}.index');
        Route::post('{$routeUri}', '{$controllerClass}@store')->name('{$routeUri}.store');
        Route::get('{$routeUri}/create', '{$controllerClass}@create')->name('{$routeUri}.create');
        Route::get('{$routeUri}/{id}', '{$controllerClass}@show')->name('{$routeUri}.show');
        Route::put('{$routeUri}/{id}', '{$controllerClass}@update')->name('{$routeUri}.update');
        Route::delete('{$routeUri}/{id}', '{$controllerClass}@destroy')->name('{$routeUri}.destroy');
        Route::get('{$routeUri}/{id}/edit', '{$controllerClass}@edit')->name('{$routeUri}.edit');
        Route::post('{$routeUri}/ajax/datatables', '{$controllerClass}@ajaxDataTable')->name('{$routeUri}.ajaxDataTable');
        Route::patch('{$routeUri}/ajax/switch', '{$controllerClass}@ajaxSwitch')->name('{$routeUri}.ajaxSwitch');
        Route::patch('{$routeUri}/ajax/sort', '{$controllerClass}@ajaxSort')->name('{$routeUri}.ajaxSort');
        Route::patch('{$routeUri}/ajax/multi/switch', '{$controllerClass}@ajaxMultiSwitch')->name('{$routeUri}.ajaxMultiSwitch');
        Route::delete('{$routeUri}/ajax/multi/delete', '{$controllerClass}@ajaxMultiDestroy')->name('{$routeUri}.ajaxMultiDestroy');

    });

});

STR;
        $bottomString = <<<STR
    });

});

STR;

        return str_replace($bottomString, $routeString, $stub);
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getNameInput()
    {
        return trim($this->argument('name'));
    }

    /**
     * Get the root namespace for the class.
     *
     * @return string
     */
    protected function rootNamespace()
    {
        $inputPackage = $this->option('package');

        if ($inputPackage == 'app') {
            return $this->laravel->getNamespace();
        } else {
            return str_replace(' ', '\\', title_case(str_replace('/', ' ', $inputPackage))) . '\\';
        }
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        $inputGuard = studly_case($this->option('guard'));
        $inputPackage = $this->option('package');

        if ($inputPackage == 'app') {
            return "{$rootNamespace}Http\\Controllers\\{$inputGuard}";
        } else {
            return "{$rootNamespace}{$inputGuard}";
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the class'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['guard', null, InputOption::VALUE_OPTIONAL, 'Witch guard for generate', 'admin'],
            ['package', null, InputOption::VALUE_OPTIONAL, 'Can set your package path with lower case', 'app'],
        ];
    }
}
