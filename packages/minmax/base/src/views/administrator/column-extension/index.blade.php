<?php
/**
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 */
?>

@extends('MinmaxBase::administrator.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::administrator.layouts.right-links')
    <a class="btn btn-sm btn-main" href="{{ langRoute("administrator.{$pageData->uri}.create") }}" title="@lang('MinmaxBase::administrator.form.create')">
        <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.create')</span>
    </a>
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Base\Administrator\ColumnExtensionPresenter')

@section('grid-filter')
    @component('MinmaxBase::administrator.layouts.grid.filter-keyword')
    <option value="column_name">@lang('MinmaxBase::models.ColumnExtension.column_name')</option>
    <option value="sub_column_name">@lang('MinmaxBase::models.ColumnExtension.sub_column_name')</option>
    @endcomponent

    @component('MinmaxBase::administrator.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('table_name', 'searchTable', ['emptyLabel' => __('MinmaxBase::models.ColumnExtension.table_name')]) !!}
    {!! $modelPresenter->getFilterSelection('active', 'searchActive', ['emptyLabel' => __('MinmaxBase::models.ColumnExtension.active')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="w-15">@lang('MinmaxBase::models.ColumnExtension.table_name')</th>
        <th class="w-15">@lang('MinmaxBase::models.ColumnExtension.column_name')</th>
        <th class="w-15">@lang('MinmaxBase::models.ColumnExtension.sub_column_name')</th>
        <th class="w-15 nosort">@lang('MinmaxBase::models.ColumnExtension.title')</th>
        <th>@lang('MinmaxBase::models.ColumnExtension.sort')</th>
        <th>@lang('MinmaxBase::models.ColumnExtension.active')</th>
        <th class="nosort">@lang('MinmaxBase::administrator.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'table_name', name: 'table_name'},
            {data: 'column_name', name: 'column_name'},
            {data: 'sub_column_name', name: 'sub_column_name'},
            {data: 'title', name: 'title'},
            {data: 'sort', name: 'sort'},
            {data: 'active', name: 'active'},
            {data: 'action', name: 'action'}
        ],
        ['column_name', 'sub_column_name'],
        {"table_name":"searchTable", "active":"searchActive"},
        [[0, 'asc'], [1, 'asc'], [4, 'asc']],
        '{{ langRoute("administrator.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("administrator.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
