<?php
/**
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 */
?>

@extends('MinmaxBase::administrator.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::administrator.layouts.right-links')
    @if(request('group'))
    <a class="btn btn-sm btn-light" href="{{ langRoute("administrator.site-parameter-group.index") }}" title="@lang('MinmaxBase::administrator.form.back_list')">
        <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.back_list')</span>
    </a>
    @endif
    <a class="btn btn-sm btn-main" href="{{ langRoute("administrator.{$pageData->uri}.create", ['group' => request('group')]) }}" title="@lang('MinmaxBase::administrator.form.create')">
        <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.create')</span>
    </a>
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Base\Administrator\SiteParameterItemPresenter')

@section('grid-filter')
    @component('MinmaxBase::administrator.layouts.grid.filter-keyword')
    <option value="label">@lang('MinmaxBase::models.SiteParameterItem.label')</option>
    <option value="value">@lang('MinmaxBase::models.SiteParameterItem.value')</option>
    @endcomponent

    @component('MinmaxBase::administrator.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('group_id', 'searchGroup', ['emptyLabel' => __('MinmaxBase::models.SiteParameterItem.group_id'), 'current' => intval(request('group'))]) !!}
    {!! $modelPresenter->getFilterSelection('active', 'searchActive', ['emptyLabel' => __('MinmaxBase::models.SiteParameterItem.active')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="w-20">@lang('MinmaxBase::models.SiteParameterItem.group_id')</th>
        <th class="w-20">@lang('MinmaxBase::models.SiteParameterItem.label')</th>
        <th class="w-15">@lang('MinmaxBase::models.SiteParameterItem.value')</th>
        <th>@lang('MinmaxBase::models.SiteParameterItem.sort')</th>
        <th>@lang('MinmaxBase::models.SiteParameterItem.active')</th>
        <th class="nosort">@lang('MinmaxBase::administrator.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'group_id', name: 'group_id'},
            {data: 'label', name: 'label'},
            {data: 'value', name: 'value'},
            {data: 'sort', name: 'sort'},
            {data: 'active', name: 'active'},
            {data: 'action', name: 'action'}
        ],
        ['label', 'value'],
        {"group_id":"searchGroup", "active":"searchActive"},
        [[0, 'asc'], [3, 'asc']],
        '{{ langRoute("administrator.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("administrator.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
