<?php
/**
 * @var \Minmax\Base\Models\Administrator $adminData
 * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Collection[] $dashboardData
 */
?>
@extends('MinmaxBase::administrator.layouts.site')

@section('breadcrumbs', Breadcrumbs::view('MinmaxBase::administrator.layouts.breadcrumbs', 'administrator.home'))

@section('content')
    @foreach($dashboardData as $row => $dashboardColumns)
    <div class="row {{ $row == 2 ? 'text-center' : '' }}">

        @foreach($dashboardColumns as $dashboardColumn)
            @if($firstItem = $dashboardColumn->first())
            <div class="col-xl-{{ array_get($firstItem->position, 'width') ?? 3 }} col-lg-6 col-sm-6 col-xs-12">

                @foreach($dashboardColumn as $dashboardItem)

                {!! app($dashboardItem->class_name)->{$dashboardItem->method_name}($dashboardItem) !!}

                @endforeach

            </div>
            @endif
        @endforeach

    </div>
    @endforeach
@endsection

@push('scripts')
{{-- * * * ammap 分析統計圖 --}}
<script src="{{ asset('static/modules/amcharts/ammap.js') }}"></script>
<script src="{{ asset('static/modules/amcharts/maps/js/worldLow.js') }}"></script>
{{-- * * * amcharts 分析統計圖 --}}
<script src="{{ asset('static/modules/amcharts/amcharts.js') }}"></script>
<script src="{{ asset('static/modules/amcharts/pie.js') }}"></script>
<script src="{{ asset('static/modules/amcharts/serial.js') }}"></script>
<script src="{{ asset('static/modules/amcharts/plugins/export/export.min.js') }}"></script>
<script src="{{ asset('static/modules/amcharts/plugins/dataloader/dataloader.min.js') }}"></script>
<script src="{{ asset('static/modules/amcharts/themes/light.js') }}"></script>
<script src="{{ asset('static/modules/amcharts/lib/amstock.js') }}"></script>
@endpush
