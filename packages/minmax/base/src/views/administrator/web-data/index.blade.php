<?php
/**
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 */
?>

@extends('MinmaxBase::administrator.layouts.page.index')

@inject('modelPresenter', 'Minmax\Base\Administrator\WebDataPresenter')

@section('grid-filter')
    @component('MinmaxBase::administrator.layouts.grid.filter-keyword')
    <option value="website_name">@lang('MinmaxBase::models.WebData.website_name')</option>
    @endcomponent

    @component('MinmaxBase::administrator.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('guard', 'searchGuard', ['emptyLabel' => __('MinmaxBase::models.WebData.guard')]) !!}
    {!! $modelPresenter->getFilterSelection('active', 'searchActive', ['emptyLabel' => __('MinmaxBase::models.WebData.active')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th>@lang('MinmaxBase::models.WebData.guard')</th>
        <th class="w-50">@lang('MinmaxBase::models.WebData.website_name')</th>
        <th>@lang('MinmaxBase::models.WebData.system_email')</th>
        <th>@lang('MinmaxBase::models.WebData.active')</th>
        <th class="nosort">@lang('MinmaxBase::administrator.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'guard', name: 'guard'},
            {data: 'website_name', name: 'website_name'},
            {data: 'system_email', name: 'system_email'},
            {data: 'active', name: 'active'},
            {data: 'action', name: 'action'}
        ],
        ['website_name'],
        {"rule":"searchRule", "active":"searchActive"},
        [[3, 'desc']],
        '{{ langRoute("administrator.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("administrator.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
