<?php
/**
 * @var string $id
 * @var boolean $language
 * @var string $label
 * @var string $name
 * @var array $images
 *
 * Options
 * @var boolean $required
 * @var integer $limit
 * @var string $hint
 * @var string $lang
 * @var string $packagePrefix
 * @var array $additionalFields
 * @var boolean $anchorEnable
 */
?>
<div class="form-group row {{ $language ? 'len' : '' }}">
    <label class="col-sm-2 col-form-label" for="{{ $id }}">
        {{ $label }}<!--
        @if($required)--><span class="text-danger ml-1">*</span><!--@endif
        -->
    </label>
    <div class="col-sm-10">
        <input type="hidden" id="{{ $id }}" name="{{ $name }}" value="" {{ $required === true ? 'required' : '' }} {{ count($images) > 0 ? 'disabled' : '' }} />
        <button class="btn btn-secondary" type="button" data-target="#{{ $id }}-modal" data-toggle="modal"><i class="icon-pictures"> </i> @lang('MinmaxBase::administrator.form.button.media_image')</button>
    </div>
    @if($hint !== '')
    <small class="form-text text-muted ml-sm-auto col-sm-10">{!! $hint !!}</small>
    @endif
</div>
<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        <div class="file-img-list" id="{{ $id }}-list">
            @foreach($images as $key => $image)
            <div class="card mr-2 d-inline-block ui-sortable-handle">
                <input type="hidden" class="card-path" name="{{ $name }}[{{ $loop->index }}][path]" value="{{ array_get($image, 'path') }}" required />
                <input class="form-control form-control-sm mb-1" type="text" value="{{ array_last(explode('/', array_get($image, 'path'))) }}" disabled />
                <a class="thumb" href="{{ getImagePath(array_get($image, 'path')) }}" data-fancybox="">
                    <span class="imgFill imgLiquid_bgSize imgLiquid_ready"><img src="{{ getImagePath(array_get($image, 'path')) }}" /></span>
                </a>
                <div class="form-row mt-1">
                    <div class="col text-center">
                        <div class="btn-group btn-group-sm justify-content-center">
                            <button class="btn btn-outline-default delBtn" type="button"><i class="icon-trash2"></i></button>
                            <button class="btn btn-outline-default set-button" type="button" title="@lang('MinmaxBase::administrator.form.image.advance.tab')" data-target="#{{ $id }}-modal-set-{{ $loop->index }}" data-toggle="modal"><i class="icon-wrench"></i></button>
                            @if($anchorEnable)
                            <button class="btn btn-outline-default anchor-button" type="button" title="@lang('MinmaxBase::administrator.form.image.anchor.tab')" data-target="#{{ $id }}-modal-anchor-{{ $loop->index }}" data-toggle="modal"><i class="icon-location3"></i></button>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal fade bd-example-modal-lg" id="{{ $id }}-modal-set-{{ $loop->index }}" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <ul class="nav nav-tabs" id="{{ $id }}-tabModal-{{ $loop->index }}" role="{{ $id }}-tabModal-{{ $loop->index }}">
                                    <li class="nav-item">
                                        <a class="nav-link active"
                                           id="{{ $id }}-tabModal-{{ $loop->index }}-1"
                                           data-toggle="tab"
                                           href="#{{ $id }}-tabModal-pane-{{ $loop->index }}-1"
                                           role="tab"
                                           aria-controls="{{ $id }}-tabModal-pane-{{ $loop->index }}-1"
                                           aria-selected="true">@lang('MinmaxBase::administrator.form.image.advance.tab')</a>
                                    </li>
                                </ul>
                                <div class="tab-content mt-4" id="{{ $id }}-tabModalContent-{{ $loop->index }}">
                                    <div class="tab-pane fade show active" id="{{ $id }}-tabModal-pane-{{ $loop->index }}-1" role="tabpanel" aria-labelledby="{{ $id }}-tabModal-{{ $loop->index }}-1">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <fieldset>
                                                    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-3"></i>@lang('MinmaxBase::administrator.form.image.advance.fieldSet.base')</legend>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.path')</label>
                                                        <div class="col-sm-10">
                                                            <div class="form-text">{{ array_get($image, 'path') }}</div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.title')</label>
                                                        <div class="col-sm-10">
                                                            <input class="form-control addi-title" type="text" name="{{ $name }}[{{ $loop->index }}][title]" value="{{ array_get($image, 'title') }}" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" >@lang('MinmaxBase::administrator.form.image.advance.field.description')</label>
                                                        <div class="col-sm-10">
                                                            <input class="form-control addi-description" type="text" name="{{ $name }}[{{ $loop->index }}][description]" value="{{ array_get($image, 'description') }}" />
                                                        </div>
                                                    </div>

                                                </fieldset>

                                                <fieldset class="mt-4">
                                                    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-3"></i>@lang('MinmaxBase::administrator.form.image.advance.fieldSet.detail')</legend>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.creator')</label>
                                                        <div class="col-sm-10">
                                                            <input class="form-control addi-creator" type="text" name="{{ $name }}[{{ $loop->index }}][creator]" value="{{ array_get($image, 'creator') }}" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.copyright')</label>
                                                        <div class="col-sm-10">
                                                            <input class="form-control addi-copyright" type="text" name="{{ $name }}[{{ $loop->index }}][copyright]" value="{{ array_get($image, 'copyright') }}" />
                                                        </div>
                                                    </div>

                                                </fieldset>

                                                <fieldset class="mt-4">
                                                    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-3"></i>@lang('MinmaxBase::administrator.form.image.advance.fieldSet.option')</legend>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="{{ $id }}-{{ $loop->index }}-cover">@lang('MinmaxBase::administrator.form.image.advance.field.cover')</label>
                                                        <div class="col-sm-10">
                                                            <div class="custom-control custom-radio custom-control-inline" style="position: relative; top: auto; right: auto; margin-right: 1rem;">
                                                                <input class="custom-control-input addi-cover" type="radio" name="{{ $name }}[{{ $loop->index }}][cover]" value="0" {{ array_get($image, 'cover') != 1 ? 'checked' : '' }} />
                                                                <label class="custom-control-label">@lang('MinmaxBase::administrator.form.image.advance.options.cover_0')</label>
                                                            </div>
                                                            <div class="custom-control custom-radio custom-control-inline" style="position: relative; top: auto; right: auto; margin-right: 1rem;">
                                                                <input class="custom-control-input addi-cover" type="radio" name="{{ $name }}[{{ $loop->index }}][cover]" value="1" {{ array_get($image, 'cover') == 1 ? 'checked' : '' }} />
                                                                <label class="custom-control-label">@lang('MinmaxBase::administrator.form.image.advance.options.cover_1')</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="{{ $id }}-{{ $loop->index }}-max_width">@lang('MinmaxBase::administrator.form.image.advance.field.size')</label>
                                                        <div class="col-sm-10">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="row">
                                                                        <label class="col-auto form-text">@lang('MinmaxBase::administrator.form.image.advance.field.max_width')</label>
                                                                        <div class="col mb-2">
                                                                            <input type="text" class="form-control addi-max_width" name="{{ $name }}[{{ $loop->index }}][max_width]" value="{{ array_get($image, 'max_width') }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="row">
                                                                        <label class="col-auto form-text">@lang('MinmaxBase::administrator.form.image.advance.field.max_height')</label>
                                                                        <div class="col mb-2">
                                                                            <input type="text" class="form-control addi-max_height" name="{{ $name }}[{{ $loop->index }}][max_height]" value="{{ array_get($image, 'max_height') }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="row">
                                                                        <label class="col-auto form-text">@lang('MinmaxBase::administrator.form.image.advance.field.min_width')</label>
                                                                        <div class="col mb-2">
                                                                            <input type="text" class="form-control addi-min_width" name="{{ $name }}[{{ $loop->index }}][min_width]" value="{{ array_get($image, 'min_width') }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="row">
                                                                        <label class="col-auto form-text">@lang('MinmaxBase::administrator.form.image.advance.field.min_height')</label>
                                                                        <div class="col mb-2">
                                                                            <input type="text" class="form-control addi-min_height" name="{{ $name }}[{{ $loop->index }}][min_height]" value="{{ array_get($image, 'min_height') }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @foreach($additionalFields as $column => $type)
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">@lang($packagePrefix . 'models.' . str_replace('-', '.additional.', $id) . '.' . $column)</label>
                                                        <div class="col-sm-10">
                                                        @switch($type)
                                                            @case('text')
                                                            <input class="form-control addi-{{ $column }}" type="text" name="{{ $name }}[{{ $loop->parent->index }}][{{ $column }}]" value="{{ $image[$column] ?? '' }}" readonly />
                                                            @break
                                                            @case('textarea')
                                                            <textarea class="form-control addi-{{ $column }}" type="text" name="{{ $name }}[{{ $loop->parent->index }}][{{ $column }}]" readonly>{{ $image[$column] ?? '' }}</textarea>
                                                            @break
                                                        @endswitch
                                                        </div>
                                                    </div>
                                                    @endforeach

                                                </fieldset>
                                            </div>
                                            <div class="col-lg-6">
                                                <img class="img-fluid" src="{{ getImagePath(array_get($image, 'path')) }}" alt="{{ array_get($image, 'path') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="button" data-dismiss="modal">@lang('MinmaxBase::administrator.form.image.submit')</button>
                            </div>
                        </div>
                    </div>
                </div>
                @if($anchorEnable)
                <div class="modal fade bd-example-modal-lg" id="{{ $id }}-modal-anchor-{{ $loop->index }}" tabindex="-1" role="dialog" aria-hidden="true">
                </div>
                @endif
            </div>
            @endforeach
        </div>
    </div>
</div>

{{-- 圖片小卡樣板 START --}}
<template id="{{ $id }}-template">
    <div class="card mr-2 d-inline-block ui-sortable-handle">
        <input type="hidden" class="card-path" name="{{ $name }}[DumpIndex][path]" value="DumpPath" required />
        <input class="form-control form-control-sm mb-1" type="text" value="DumpFilename" disabled />
        <a class="thumb" href="DumpPath" data-fancybox="">
            <span class="imgFill imgLiquid_bgSize imgLiquid_ready" style="background: url('DumpFullPath') center center no-repeat;background-size:cover">
                <img src="DumpFullPath" style="display:none" />
            </span>
        </a>
        <div class="form-row mt-1">
            <div class="col text-center">
                <div class="btn-group btn-group-sm justify-content-center">
                    <button class="btn btn-outline-default delBtn" type="button"><i class="icon-trash2"></i></button>
                    <button class="btn btn-outline-default set-button" type="button" title="@lang('MinmaxBase::administrator.form.image.advance.tab')" data-target="#{{ $id }}-modal-set-DumpIndex" data-toggle="modal"><i class="icon-wrench"></i></button>
                    @if($anchorEnable)
                    <button class="btn btn-outline-default anchor-button" type="button" title="@lang('MinmaxBase::administrator.form.image.anchor.tab')" data-target="#{{ $id }}-modal-anchor-DumpIndex" data-toggle="modal"><i class="icon-location3"></i></button>
                    @endif
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="{{ $id }}-modal-set-DumpIndex" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <ul class="nav nav-tabs" id="{{ $id }}-tabModal-DumpIndex" role="{{ $id }}-tabModal-DumpIndex">
                            <li class="nav-item">
                                <a class="nav-link active"
                                   id="{{ $id }}-tabModal-DumpIndex-1"
                                   data-toggle="tab"
                                   href="#{{ $id }}-tabModal-pane-DumpIndex-1"
                                   role="tab"
                                   aria-controls="{{ $id }}-tabModal-pane-DumpIndex-1"
                                   aria-selected="true">@lang('MinmaxBase::administrator.form.image.advance.tab')</a>
                            </li>
                        </ul>
                        <div class="tab-content mt-4" id="{{ $id }}-tabModalContent-DumpIndex">
                            <div class="tab-pane fade show active" id="{{ $id }}-tabModal-pane-DumpIndex-1" role="tabpanel" aria-labelledby="{{ $id }}-tabModal-DumpIndex-1">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <fieldset>
                                            <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-3"></i>@lang('MinmaxBase::administrator.form.image.advance.fieldSet.base')</legend>

                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.path')</label>
                                                <div class="col-sm-10">
                                                    <div class="form-text">DumpPath</div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.title')</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control addi-title" type="text" name="{{ $name }}[DumpIndex][title]" value="" />
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.description')</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control addi-description" type="text" name="{{ $name }}[DumpIndex][description]" value="" />
                                                </div>
                                            </div>

                                        </fieldset>

                                        <fieldset class="mt-4">
                                            <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-3"></i>@lang('MinmaxBase::administrator.form.image.advance.fieldSet.detail')</legend>

                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.creator')</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control addi-creator" type="text" name="{{ $name }}[DumpIndex][creator]" value="" />
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.copyright')</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control addi-copyright" type="text" name="{{ $name }}[DumpIndex][copyright]" value="" />
                                                </div>
                                            </div>

                                        </fieldset>

                                        <fieldset class="mt-4">
                                            <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-3"></i>@lang('MinmaxBase::administrator.form.image.advance.fieldSet.option')</legend>

                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.cover')</label>
                                                <div class="col-sm-10">
                                                    <div class="custom-control custom-radio custom-control-inline" style="position: relative; top: auto; right: auto; margin-right: 1rem;">
                                                        <input class="custom-control-input addi-cover" type="radio" name="{{ $name }}[DumpIndex][cover]" value="0" checked />
                                                        <label class="custom-control-label">@lang('MinmaxBase::administrator.form.image.advance.options.cover_0')</label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline" style="position: relative; top: auto; right: auto; margin-right: 1rem;">
                                                        <input class="custom-control-input addi-cover" type="radio" name="{{ $name }}[DumpIndex][cover]" value="1" />
                                                        <label class="custom-control-label">@lang('MinmaxBase::administrator.form.image.advance.options.cover_1')</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.size')</label>
                                                <div class="col-sm-10">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="row">
                                                                <label class="col-auto form-text">@lang('MinmaxBase::administrator.form.image.advance.field.max_width')</label>
                                                                <div class="col mb-2">
                                                                    <input type="text" class="form-control addi-max_width" name="{{ $name }}[DumpIndex][max_width]" value="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="row">
                                                                <label class="col-auto form-text">@lang('MinmaxBase::administrator.form.image.advance.field.max_height')</label>
                                                                <div class="col mb-2">
                                                                    <input type="text" class="form-control addi-max_height" name="{{ $name }}[DumpIndex][max_height]" value="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="row">
                                                                <label class="col-auto form-text">@lang('MinmaxBase::administrator.form.image.advance.field.min_width')</label>
                                                                <div class="col mb-2">
                                                                    <input type="text" class="form-control addi-min_width" name="{{ $name }}[DumpIndex][min_width]" value="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="row">
                                                                <label class="col-auto form-text">@lang('MinmaxBase::administrator.form.image.advance.field.min_height')</label>
                                                                <div class="col mb-2">
                                                                    <input type="text" class="form-control addi-min_width" name="{{ $name }}[DumpIndex][min_height]" value="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            @foreach($additionalFields as $column => $type)
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">@lang($packagePrefix . 'models.' . str_replace('-', '.additional.', $id) . '.' . $column)</label>
                                                <div class="col-sm-10">
                                                @switch($type)
                                                    @case('text')
                                                    <input class="form-control addi-{{ $column }}" type="text" name="{{ $name }}[DumpIndex][{{ $column }}]" value="" />
                                                    @break
                                                    @case('textarea')
                                                    <textarea class="form-control addi-{{ $column }}" type="text" name="{{ $name }}[DumpIndex][{{ $column }}]"></textarea>
                                                    @break
                                                @endswitch
                                                </div>
                                            </div>
                                            @endforeach

                                        </fieldset>
                                    </div>
                                    <div class="col-lg-6">
                                        <img class="img-fluid" src="DumpFullPath" alt="DumpPath" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="button" data-dismiss="modal">@lang('MinmaxBase::administrator.form.image.submit')</button>
                    </div>
                </div>
            </div>
        </div>
        @if($anchorEnable)
        <div class="modal fade bd-example-modal-lg" id="{{ $id }}-modal-anchor-DumpIndex" tabindex="-1" role="dialog" aria-hidden="true">
        </div>
        @endif
    </div>
</template>
{{-- 圖片小卡樣板 END --}}

@push('scripts')
{{-- 彈跳視窗 START --}}
<div class="modal fade bd-example-modal-lg" id="{{ $id }}-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('MinmaxBase::administrator.form.button.media_image')</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"></button><span aria-hidden="true">×</span>
            </div>
            <div class="modal-body">
                <div id="{{ $id }}-elfinder"></div>
            </div>
        </div>
    </div>
</div>
{{-- 彈跳視窗 END --}}
<script>
(function($) {
    $(function() {
        var imagesSorting = function ()
        {
            $('#{{ $id }}-list .card').each(function() {
                var inputName = '{{ $name }}';
                var thisId = '{{ $id }}';
                var $this = $(this);
                var thisIndex = $this.index();
                $('.card-path', $this).attr('name', inputName + '[' + thisIndex + '][path]');

                $('.set-button', $this).attr('data-target', '#' + thisId + '-modal-set-' + thisIndex);
                $('.modal', $this).attr('id', thisId + '-modal-set-' + thisIndex);
                $('.nav-tabs', $this)
                    .attr('id', thisId + '-tabModal-' + thisIndex)
                    .attr('role', thisId + '-tabModal-' + thisIndex);
                $('.nav-link', $this)
                    .attr('id', thisId + '-tabModal-' + thisIndex + '-1')
                    .attr('href', '#' + thisId + '-tabModal-pane-' + thisIndex + '-1')
                    .attr('aria-controls', thisId + '-tabModal-pane-' + thisIndex + '-1');
                $('.tab-content', $this).attr('id', thisId + '-tabModalContent-' + thisIndex);
                $('.tab-pane', $this)
                    .attr('id', thisId + '-tabModal-pane-' + thisIndex + '-1')
                    .attr('aria-labelledby', thisId + '-tabModal-' + thisIndex + '-1');
                $('.addi-title', $this).attr('name', inputName + '[' + thisIndex + '][title]');
                $('.addi-description', $this).attr('name', inputName + '[' + thisIndex + '][description]');
                $('.addi-creator', $this).attr('name', inputName + '[' + thisIndex + '][creator]');
                $('.addi-copyright', $this).attr('name', inputName + '[' + thisIndex + '][copyright]');
                $('.addi-cover', $this).attr('name', inputName + '[' + thisIndex + '][cover]');
                $('.addi-max_width', $this).attr('name', inputName + '[' + thisIndex + '][max_width]');
                $('.addi-max_height', $this).attr('name', inputName + '[' + thisIndex + '][max_height]');
                $('.addi-min_width', $this).attr('name', inputName + '[' + thisIndex + '][min_width]');
                $('.addi-min_height', $this).attr('name', inputName + '[' + thisIndex + '][min_height]');
                @foreach($additionalFields as $column => $type)
                $('.addi-{{ $column }}', $this).attr('name', inputName + '[' + thisIndex + '][{{ $column }}]');
                @endforeach

                @if($anchorEnable)
                $('.anchor-button', $this).attr('data-target', '#' + thisId + '-modal-anchor-' + thisIndex);
                $('.modal', $this).attr('id', thisId + '-modal-anchor-' + thisIndex);
                @endif
            });
        };

        $('#{{ $id }}-list').sortable({
            disabled: false,
            items: '.card',
            zIndex: 9999,
            opacity: 0.7,
            forceHelperSize: false,
            helper: 'clone',
            change: function(event, div) { div.placeholder.css({visibility: 'visible', background: '#cc0000', opacity: 0.2}) },
            stop: function() {
                imagesSorting();
            },
        }).disableSelection();

        var selectLimit = parseInt('{{ $limit }}');
        var elf_{{ str_replace('-', '_', $id) }} = $('#{{ $id }}-elfinder').elfinder({
            lang: '{{ $lang }}',
            customData: {_token: '{{ csrf_token() }}'},
            url: '{{ langRoute('administrator.elfinder.connector') }}',
            commands: elFinder.prototype._options.commands,
            commandsOptions: {upload : {ui : 'uploadbutton'}},
            soundPath: '{{ asset('static/modules/elFinder/sounds') }}',
            reloadClearHistory: true, resizable: false, rememberLastDir: false, height: '500px',
            uiOptions: {
                toolbar: [
                    ['back', 'forward', 'up'], ['view', 'sort'], ['copy', 'cut', 'paste'], ['rm'],
                    ['duplicate', 'rename'], ['mkdir', 'upload'], ['getfile', 'open', 'download'], ['info']
                ]
            },
            contextmenu: {
                cwd: ['reload', '|', 'upload', 'mkdir', 'paste', '|', 'view', 'sort', 'selectall', '|', 'info'],
                files: ['getfile', 'open', 'download', '|', 'copy', 'cut', 'paste', 'rm', '|', 'rename', '|', 'info']
            },
            getFileCallback: function (file) {
                $('#{{ $id }}').attr('disabled', true);

                if(selectLimit !== 0 && $('#{{ $id }}-list .card').length >= selectLimit) {
                    $('#{{ $id }}-modal').modal('hide');
                    swal({
                        title: "@lang('MinmaxBase::administrator.form.elfinder.limit_title')",
                        text: "@lang('MinmaxBase::administrator.form.elfinder.limit_text', ['limit' => $limit])",
                        confirmButtonText: "@lang('MinmaxBase::administrator.form.elfinder.limit_confirm_button')",
                        confirmButtonClass: "btn-danger",
                        closeOnConfirm: true
                    });
                } else {
                    $('#{{ $id }}-list').append(
                        $('#{{ $id }}-template').html()
                            .replace(/DumpPath/g, '/' + file.path.replace(/\\/g, '/'))
                            .replace(/DumpFullPath/g, '{{ url('/') }}/' + file.path.replace(/\\/g, '/'))
                            .replace(/DumpFilename/g, file.path.replace(/\\/g, '/').split('/').slice(-1)[0])
                    );
                }

                imagesSorting();
            }
        }).elfinder('instance');

        $('#{{ $id }}-modal').on('shown.bs.modal', function () { $(window).resize(); });

        {{-- 刪除圖片 --}}
        $('body').delegate('#{{ $id }}-list .delBtn', 'click', function(){
            var $this = $(this);
            swal({
                title: "@lang('MinmaxBase::administrator.form.elfinder.remove_title')",
                text: "@lang('MinmaxBase::administrator.form.elfinder.remove_text')",
                type: "info",
                showCancelButton: true,
                cancelButtonText: "@lang('MinmaxBase::administrator.form.elfinder.remove_cancel_button')",
                confirmButtonText: "@lang('MinmaxBase::administrator.form.elfinder.remove_confirm_button')",
                confirmButtonClass: "btn-danger",
                closeOnConfirm: false
            }, function(){
                $this.parents('.card').remove();

                imagesSorting();

                if ($('#{{ $id }}-list .card').length < 1) $('#{{ $id }}').removeAttr('disabled');

                swal("@lang('MinmaxBase::administrator.form.elfinder.remove_success_title')", "@lang('MinmaxBase::administrator.form.elfinder.remove_success_text')", "success");
            });
        });
    });
})(jQuery);
</script>
@endpush
