<?php
/**
 * @var string $id
 * @var boolean $language
 * @var string $label
 * @var string $name
 * @var array $images
 *
 * Options
 * @var boolean $required
 * @var integer $maxWidth
 * @var integer $maxHeight
 * @var string $path
 * @var integer $limit
 * @var string $hint
 */
?>
<div class="form-group row {{ $language ? 'len' : '' }}">
    <label class="col-sm-2 col-form-label" for="{{ $id }}">
        {{ $label }}<!--
        @if($required)--><span class="text-danger ml-1">*</span><!--@endif
        -->
    </label>

    <div class="col-sm-10">
        <div class="custom-file">
            <input class="custom-file-input" type="file" id="{{ $id }}-file" accept="image/jpeg, image/png, image/gif" multiple />
            <label class="custom-file-label" for="{{ $id }}">@lang('MinmaxBase::administrator.form.file.image_text')</label>
        </div>
        <input type="hidden" name="{{ $name }}[path]" value="{{ $path }}" />
        <input type="hidden" id="{{ $id }}" name="{{ str_replace('[images]', '', $name) }}" value="" {{ $required === true ? 'required' : '' }} {{ count($images) > 0 ? 'disabled' : '' }} />
    </div>
    @if($hint !== '')
    <small class="form-text text-muted ml-sm-auto col-sm-10">{!! $hint !!}</small>
    @endif
</div>

<div class="form-group row">
    <div class="col-sm-10 offset-sm-2">
        <div class="file-img-list" id="{{ $id }}-list">
            @foreach($images as $image)
            <div class="card mr-2 d-inline-block ui-sortable-handle">
                <input type="hidden" class="card-path" name="{{ $name }}[origin][{{ $loop->index }}][path]" data-name="{{ $name }}[origin][DumpIndex][path]" value="{{ array_get($image, 'path') }}" required />
                <input class="form-control form-control-sm mb-1 ignore-valid" type="text" value="{{ array_last(explode('/', array_get($image, 'path'))) }}" disabled />
                <a class="thumb" href="{{ getImagePath(array_get($image, 'path')) }}" data-fancybox="">
                    <span class="imgFill imgLiquid_bgSize imgLiquid_ready"><img src="{{ getImagePath(array_get($image, 'path')) }}" /></span>
                </a>
                <div class="form-row mt-1">
                    <div class="col text-center">
                        <div class="btn-group btn-group-sm justify-content-center">
                            <button class="btn btn-outline-default delBtn" type="button"><i class="icon-trash2"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<template id="{{ $id }}-template">
    <div class="card mr-2 d-inline-block ui-sortable-handle upload">
        <input type="hidden" class="card-path" data-name="{{ $name }}[upload][DumpIndex][data]" value="DumpPath" required />
        <input class="form-control form-control-sm mb-1 card-name ignore-valid" type="text" data-name="{{ $name }}[upload][DumpIndex][name]" value="DumpFilename" />
        <a class="thumb" href="DumpPath" data-fancybox="">
            <span class="imgFill imgLiquid_bgSize imgLiquid_ready" style="background: url('DumpPath') center center no-repeat;background-size:cover">
                <img src="DumpPath" style="display:none" />
            </span>
        </a>
        <div class="form-row mt-1">
            <div class="col text-center">
                <div class="btn-group btn-group-sm justify-content-center">
                    <button class="btn btn-outline-default delBtn" type="button"><i class="icon-trash2"></i></button>
                </div>
            </div>
        </div>
    </div>
</template>

@push('scripts')
<script>
(function($) {

    $(function() {
        var imagesSorting = function ()
        {
            $('#{{ $id }}-list .card').each(function() {
                var $this = $(this);
                var thisIndex = $this.index();
                $('.card-path', $this).attr('name', $('.card-path', $this).attr('data-name').replace(/DumpIndex/g, thisIndex));

                if ($('.card-name', $this).length > 0) {
                    $('.card-name', $this).attr('name', $('.card-name', $this).attr('data-name').replace(/DumpIndex/g, thisIndex));
                }
            });
        };

        $('#{{ $id }}-file').on('change', function() {
            var limit = parseInt('{{ $limit }}');
            $('#{{ $id }}-list .upload').remove();
            if(limit > 0 && ($('#{{ $id }}-list .card').length + this.files.length) > limit) {
                swal('@lang('MinmaxBase::administrator.form.file.limit_title')', '@lang('MinmaxBase::administrator.form.file.limit_text', ['limit' => $limit])');
            } else {
                for(var image of this.files) {
                    imageResize(image, parseInt('{{ $maxWidth }}'), parseInt('{{ $maxHeight }}'), function (path, name) {
                        $('#{{ $id }}-list').append(
                            $('#{{ $id }}-template').html()
                                .replace(/DumpPath/g, path)
                                .replace(/DumpFilename/g, name)
                        );
                        imagesSorting();

                        $('#{{ $id }}').prop('disabled', $('#{{ $id }}-list .card').length > 0);
                    });
                }
            }
            this.value = '';
        });

        {{-- 刪除圖片 --}}
        $('#{{ $id }}-list').on('click', '.delBtn', function(){
            var $this = $(this);
            swal({
                title: "@lang('MinmaxBase::administrator.form.elfinder.remove_title')",
                text: "@lang('MinmaxBase::administrator.form.elfinder.remove_text')",
                type: "info",
                showCancelButton: true,
                cancelButtonText: "@lang('MinmaxBase::administrator.form.elfinder.remove_cancel_button')",
                confirmButtonText: "@lang('MinmaxBase::administrator.form.elfinder.remove_confirm_button')",
                confirmButtonClass: "btn-danger",
                closeOnConfirm: false
            }, function(){
                $this.parents('.card').remove();

                imagesSorting();

                $('#{{ $id }}').prop('disabled', $('#{{ $id }}-list .card').length > 0);

                swal("@lang('MinmaxBase::administrator.form.elfinder.remove_success_title')", "@lang('MinmaxBase::administrator.form.elfinder.remove_success_text')", "success");
            });
        });
    });
})(jQuery);
</script>
@endpush
