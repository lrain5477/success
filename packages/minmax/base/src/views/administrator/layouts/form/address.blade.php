<?php
/**
 * @var string $id
 * @var boolean $language
 * @var string $label
 * @var string $name
 * @var array $value
 * @var array $countries
 *
 * Options
 * @var array $required
 * @var integer $size
 * @var string $hint
 */
?>
<div class="form-group row {{ $language ? 'len' : '' }}">
    <label class="col-sm-2 col-form-label" for="{{ $id }}">
        {{ $label }}<!--
        @if(!blank($required))--><span class="text-danger ml-1">*</span><!--@endif
        -->
    </label>
    <div class="col-sm-{{ $size }}" id="{{ $id }}">
        <div class="form-row">
            <div class="col-lg-auto mb-2 mb-lg-0 col-md-6">
                <input class="form-control" id="{{ $id }}-zip" type="text" value=""
                       placeholder="@lang('MinmaxBase::administrator.form.address.zip')" disabled />
            </div>
            <div class="col-lg-auto mb-2 mb-lg-0 col-md-6">
                <select class="bs-select form-control" id="{{ $id }}-country" name="{{ $name }}[country]"
                        data-live-search="true" {{ in_array('country', $required) ? 'required' : '' }}>
                    <option value="">@lang('MinmaxBase::administrator.form.address.country')</option>
                    @foreach($countries as $countryKey => $countryItem)
                    <option value="{{ $countryKey }}">{{ array_get($countryItem, 'title') }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-auto mb-2 mb-lg-0 col-md-6">
                <select class="bs-select form-control" id="{{ $id }}-state" name="{{ $name }}[state]"
                        data-live-search="true" {{ in_array('state', $required) ? 'required' : '' }}>
                    <option value="">@lang('MinmaxBase::administrator.form.address.state')</option>
                </select>
            </div>
            <div class="col-lg-auto mb-2 mb-lg-0 col-md-6">
                <select class="bs-select form-control" id="{{ $id }}-county" name="{{ $name }}[county]"
                        data-live-search="true" {{ in_array('county', $required) ? 'required' : '' }}>
                    <option value="">@lang('MinmaxBase::administrator.form.address.county')</option>
                </select>
            </div>
            <div class="col-lg-auto mb-2 mb-lg-0 col-md-6">
                <select class="bs-select form-control" id="{{ $id }}-city" name="{{ $name }}[city]"
                        data-live-search="true" {{ in_array('city', $required) ? 'required' : '' }}>
                    <option value="">@lang('MinmaxBase::administrator.form.address.city')</option>
                </select>
            </div>
            <div class="col-lg">
                <input type="text" class="form-control"
                       id="{{ $id }}-street"
                       name="{{ $name }}[street]"
                       value="{{ array_get(old(str_replace(['[', ']'], ['.', ''], $name), $value), 'street') }}"
                       placeholder="@lang('MinmaxBase::administrator.form.address.street')"
                       {{ in_array('street', $required) ? 'required' : '' }} />
            </div>
        </div>
    </div>
    @if($hint !== '')
    <small class="form-text text-muted ml-sm-auto col-sm-10">{!! $hint !!}</small>
    @endif
</div>

@push('scripts')
<script>
$(function() {
    initAddress(
        '{{ $id }}',
        {
            "states": '{{ langRoute('administrator.ajax.world-states', 'id') }}',
            "counties": '{{ langRoute('administrator.ajax.world-counties', 'id') }}',
            "cities": '{{ langRoute('administrator.ajax.world-cities', 'id') }}'
        },
        {
            "country": '{{ array_get(old(str_replace(['[', ']'], ['.', ''], $name), $value), 'country') }}',
            "state": '{{ array_get(old(str_replace(['[', ']'], ['.', ''], $name), $value), 'state') }}',
            "county": '{{ array_get(old(str_replace(['[', ']'], ['.', ''], $name), $value), 'county') }}',
            "city": '{{ array_get(old(str_replace(['[', ']'], ['.', ''], $name), $value), 'city') }}',
        }
    );
});
</script>
@endpush
