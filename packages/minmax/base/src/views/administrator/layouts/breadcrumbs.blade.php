<?php
/**
 * @var \Illuminate\Support\Collection $breadcrumbs
 */
?>
@if (count($breadcrumbs))
<nav class="ml-2 mt-1" aria-label="breadcrumb">
    <ol class="breadcrumb">
    @foreach ($breadcrumbs as $breadcrumb)
        @if ($breadcrumb->url && !$loop->last)
            @if ($breadcrumb->title == __('MinmaxBase::administrator.breadcrumbs.home'))
            <li class="breadcrumb-item"><a href="{{ $breadcrumb->url }}"><i class="icon-home3 mr-2"></i><span>{{ $breadcrumb->title }}</span></a></li>
            @else
            <li class="breadcrumb-item"><a href="{{ $breadcrumb->url }}"><span>{{ $breadcrumb->title }}</span></a></li>
            @endif
        @else
            @if ($breadcrumb->title == __('MinmaxBase::administrator.breadcrumbs.home'))
            <li class="breadcrumb-item active"><span><i class="icon-home3 mr-2"></i>{{ $breadcrumb->title }}</span></li>
            @else
                @if (!$loop->last)
                <li class="breadcrumb-item"><span>{{ $breadcrumb->title }}</span></li>
                @else
                    @if ($breadcrumb->title == __('MinmaxBase::administrator.breadcrumbs.home'))
                    <li class="breadcrumb-item active"><span><i class="icon-home3 mr-2"></i>{{ $breadcrumb->title }}</span></li>
                    @else
                    <li class="breadcrumb-item active"><span>{{ $breadcrumb->title }}</span></li>
                    @endif
                @endif
            @endif
        @endif
    @endforeach
    </ol>
</nav>
@endif
