<?php
/**
 * @var string $uri
 * @var string $id
 */
?>
<a class="btn btn-outline-default btn-sm" role="button" aria-pressed="true" title="@lang('MinmaxBase::administrator.grid.actions.log')"
   href="{{ langRoute("administrator.system-log.index", ['uri' => $uri, 'object' => $id]) }}">
    <i class="icon-book-alt2"></i><span class="text-hide">@lang('MinmaxBase::administrator.grid.actions.log')</span>
</a>
