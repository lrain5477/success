<?php
/**
 * @var string $id
 * @var string $title
 * @var array $position
 * @var array $options
 */
?>
<section class="panel h-100-c">
    <div id="{{ $id }}-map" style="width: 100%; height: 440px"></div>
</section>

@push('scripts')
<script>
$(function () {
    AmCharts.theme = AmCharts.themes.black;
    $.get('{{ langRoute('administrator.ajax.ga.world-source') }}', function (response) {
        var map = new AmCharts.AmMap();
        map.addTitle("{{ $title }}", 14);
        map.projection = "winkel3";
        map.areasSettings = {unlistedAreasColor: "#000000", unlistedAreasAlpha: 0.1};
        map.imagesSettings = {
            balloonText: "<span style='font-size:14px;'><b>[[title]]</b>: [[value]]</span>",
            alpha: 0.6
        };
        map.dataProvider = {mapVar: AmCharts.maps.worldLow, images: []};
        if (typeof response === 'object' && response.length > 0) {
            for (var country of response) {
                map.dataProvider.images.push({
                    type: "circle", width: 10, height: 10,
                    color: country.color,
                    longitude: country.longitude,
                    latitude: country.latitude,
                    title: country.name,
                    value: country.value
                });
            }
        }
        map.write("{{ $id }}-map");
    });
});
</script>
@endpush
