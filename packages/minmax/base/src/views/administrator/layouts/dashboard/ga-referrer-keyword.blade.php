<?php
/**
 * @var string $id
 * @var string $title
 * @var array $position
 * @var array $options
 */
?>
<section class="panel h-100-c">
    <header class="panel-heading">
        <h2 class="h5 float-left">{{ $title }}</h2>
    </header>
    <div class="panel-wrapper"></div>
    <div class="panel-body" style="min-height:{{ array_get($position, 'height') ?? 270 }}px;">
        <div class="table-responsive-sm">
            <table class="table">
                <thead class="font-weight-bold">
                <tr>
                    <th>#</th>
                    <th>@lang('MinmaxBase::administrator.dashboard.keyword')</th>
                    <th class="text-right">@lang('MinmaxBase::administrator.dashboard.keyword_count')</th>
                </tr>
                </thead>
                <tbody id="{{ $id }}">
                <tr>
                    <th class="text-center" scope="row" colspan="3">No Data</th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>

@push('scripts')
<script>
$(function () {
    $.get('{{ langRoute('administrator.ajax.ga.referrer-keyword') }}', function (response) {
        if (response.length > 0) {
            $('#{{ $id }} > tr').remove();
        }
        for (var rowIndex in response) {
            if (response.hasOwnProperty(rowIndex)) {
                $('#{{ $id }}').append('<tr>' +
                    '<th class="text-center" scope="row">' + (parseInt(rowIndex) + 1) + '</th>' +
                    '<td>' + response[rowIndex].keyword + '</td>\n' +
                    '<td class="text-right">' + response[rowIndex].count + '</td></tr>');
            }
        }
    });
});
</script>
@endpush
