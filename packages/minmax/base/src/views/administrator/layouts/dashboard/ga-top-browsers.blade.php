<?php
/**
 * @var string $id
 * @var string $title
 * @var array $position
 * @var array $options
 */
?>
<section class="panel h-55-c">
    <header class="panel-heading text-center">
        <h2 class="h5 float-left">{{ $title }}</h2>
    </header>
    <div id="{{ $id }}" style="min-height: {{ array_get($position, 'height') ?? '80' }}px"></div>
</section>

@push('scripts')
<script>
$(function () {
    $.get('{{ langRoute('administrator.ajax.ga.top-browsers') }}', function (response) {
        var badges = ['badge-main', 'badge-warning', 'badge-info', 'badge-success'];
        for (var rowIndex in response) {
            if (response.hasOwnProperty(rowIndex)) {
                if (rowIndex > 0) {
                    $('#{{ $id }}').append('<hr class="my-2 w-100">');
                }

                $('#{{ $id }}').append('<div class="clearfix px-4 ' + (rowIndex > 0 ? 'py-1' : '') + ' ' + (parseInt(rowIndex) === (response.length - 1) ? 'pb-2' : '') + '">' +
                    '<span class="float-left small">' + response[rowIndex].browser + '</span>' +
                    '<span class="float-right"><span class="badge badge-pill ' + badges[rowIndex] + '">' + response[rowIndex].rate + '</span></span>' +
                    '</div>');
            }
        }
    });
});
</script>
@endpush
