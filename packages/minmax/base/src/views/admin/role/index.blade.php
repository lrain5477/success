<?php
/**
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 */
?>

@extends('MinmaxBase::admin.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links')
        @if($adminData->can('roleCreate'))
        <a class="btn btn-sm btn-main" href="{{ langRoute("admin.{$pageData->uri}.create") }}" title="@lang('MinmaxBase::admin.form.create')">
            <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.create')</span>
        </a>
        @endif
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Base\Admin\RolePresenter')

@section('grid-filter')
    @component('MinmaxBase::admin.layouts.grid.filter-keyword')
    <option value="name">@lang('MinmaxBase::models.Role.name')</option>
    <option value="display_name">@lang('MinmaxBase::models.Role.display_name')</option>
    @endcomponent

    @component('MinmaxBase::admin.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('guard', 'searchGuard', ['emptyLabel' => __('MinmaxBase::models.Role.guard')]) !!}
    {!! $modelPresenter->getFilterSelection('active', 'searchActive', ['emptyLabel' => __('MinmaxBase::models.Role.active')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="w-15">@lang('MinmaxBase::models.Role.guard')</th>
        <th class="w-20">@lang('MinmaxBase::models.Role.name')</th>
        <th class="w-25">@lang('MinmaxBase::models.Role.display_name')</th>
        <th>@lang('MinmaxBase::models.Role.active')</th>
        <th class="nosort">@lang('MinmaxBase::admin.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'guard', name: 'guard'},
            {data: 'name', name: 'name'},
            {data: 'display_name', name: 'display_name'},
            {data: 'active', name: 'active'},
            {data: 'action', name: 'action'}
        ],
        ['name', 'display_name'],
        {"guard":"searchGuard", "active":"searchActive"},
        [[0, 'asc'], [1, 'asc']],
        '{{ langRoute("admin.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("admin.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
