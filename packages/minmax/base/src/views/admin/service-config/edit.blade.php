<?php
/**
 * Edit page of model ServiceConfig
 *
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Base\Models\ServiceConfig $formData
 */
?>

@extends('MinmaxBase::admin.layouts.page.edit', ['formDataId' => $formData->id])

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links', ['languageActive' => $languageActive])
        @if($adminData->can('serviceConfigShow'))
        <a class="btn btn-sm btn-light" href="{{ langRoute("admin.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::admin.form.back_list')">
            <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.back_list')</span>
        </a>
        @endif
    @endcomponent
@endsection

@section('forms')
    @inject('modelPresenter', 'Minmax\Base\Admin\ServiceConfigPresenter')

    <fieldset>
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.default')</legend>

        {!! $modelPresenter->getShowNormalText($formData, 'title') !!}

        {!! $modelPresenter->getShowNormalText($formData, 'code') !!}

        {!! $modelPresenter->getFieldText($formData, 'host', ['required' => true, 'hint' => true]) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.advanced')</legend>

        {!! $modelPresenter->getFieldDynamicOptionText($formData, 'options') !!}

        @if(is_array($formData->parameters) && count($formData->parameters) > 0)
        <div class="form-group row">
            <div class="col-sm-10 offset-sm-2">
                <div class="note note-edfault mt-3">
                    <h6>@lang('MinmaxBase::models.ServiceConfig.parameters-title')</h6>
                    <small>
                        @foreach($formData->parameters as $parameterKey => $parameterValue)
                        <code class="mr-2">{{ $parameterKey }}</code>{{ $parameterValue }}{{ $loop->last ? '' : '，' }}
                        @endforeach
                    </small>
                </div>
            </div>
        </div>
        @endif

        {!! $modelPresenter->getFieldText($formData, 'sort', ['required' => true, 'size' => 2]) !!}

    </fieldset>

    <div class="text-center my-4 form-btn-group">
        <input class="btn btn-main" type="submit" id="submitBut" value="@lang('MinmaxBase::admin.form.button.send')" />
        <input class="btn btn-default" type="reset" value="@lang('MinmaxBase::admin.form.button.reset')" onclick="window.location.reload(true)" />
    </div>
@endsection
