<?php
/**
 * List of model ServiceConfig
 *
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Base\Models\ServiceConfig $formData
 */
?>

@extends('MinmaxBase::admin.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links')
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Base\Admin\ServiceConfigPresenter')

@section('grid-filter')
    @component('MinmaxBase::admin.layouts.grid.filter-keyword')
    <option value="code">@lang('MinmaxBase::models.ServiceConfig.code')</option>
    <option value="title">@lang('MinmaxBase::models.ServiceConfig.title')</option>
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="w-15">@lang('MinmaxBase::models.ServiceConfig.code')</th>
        <th class="w-50">@lang('MinmaxBase::models.ServiceConfig.title')</th>
        <th>@lang('MinmaxBase::models.ServiceConfig.sort')</th>
        <th class="nosort">@lang('MinmaxBase::admin.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'code', name: 'code'},
            {data: 'title', name: 'title'},
            {data: 'sort', name: 'sort'},
            {data: 'action', name: 'action'}
        ],
        ['code', 'title'],
        {},
        [[2, 'asc']],
        '{{ langRoute("admin.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("admin.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
