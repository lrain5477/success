<?php
/**
 * @var string $slot
 */
?>
<div class="col-md">
    <div class="datatableFilter row no-gutters justify-content-end text-nowrap">
        <label class="col-auto mr-1"><i class="icon-narrow i-o align-middle h3 mb-0 p-0"></i>@lang('MinmaxBase::admin.grid.filter')</label>
        {{ $slot }}
    </div>
</div>