<?php
/**
 * @var string $id
 * @var boolean $language
 * @var string $label
 * @var array $images
 * @var string $packagePrefix
 * @var array $additionalFields
 * @var boolean $anchorEnable
 */
?>
<div class="form-group row {{ $language ? 'len' : '' }}">
    <label class="col-sm-2 col-form-label" for="{{ $id }}">{{ $label }}</label>
    <div class="col-sm-10">
        <div class="file-img-list" id="{{ $id }}-list">
            @foreach($images as $image)
            <div class="card mr-2 d-inline-block">
                <input class="form-control form-control-sm mb-1" type="text" value="{{ array_last(explode('/', array_get($image, 'path'))) }}" disabled />
                <a class="thumb" href="{{ getImagePath(array_get($image, 'path')) }}" data-fancybox="">
                    <span class="imgFill imgLiquid_bgSize imgLiquid_ready"><img src="{{ getImagePath(array_get($image, 'path')) }}" alt="" /></span>
                </a>
                <div class="form-row mt-1">
                    <div class="col text-center">
                        <div class="btn-group btn-group-sm justify-content-center">
                            <button class="btn btn-outline-default set-button" type="button" title="@lang('MinmaxBase::admin.form.image.advance.tab')" data-target="#{{ $id }}-modal-set-{{ $loop->index }}" data-toggle="modal"><i class="icon-wrench"></i></button>
                            @if($anchorEnable)
                            <button class="btn btn-outline-default anchor-button" type="button" title="@lang('MinmaxBase::admin.form.image.anchor.tab')" data-target="#{{ $id }}-modal-anchor-{{ $loop->index }}" data-toggle="modal"><i class="icon-location3"></i></button>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal fade bd-example-modal-lg" id="{{ $id }}-modal-set-{{ $loop->index }}" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <ul class="nav nav-tabs" id="{{ $id }}-tabModal-{{ $loop->index }}" role="{{ $id }}-tabModal-{{ $loop->index }}">
                                    <li class="nav-item">
                                        <a class="nav-link active"
                                           id="{{ $id }}-tabModal-{{ $loop->index }}-1"
                                           data-toggle="tab"
                                           href="#{{ $id }}-tabModal-pane-{{ $loop->index }}-1"
                                           role="tab"
                                           aria-controls="{{ $id }}-tabModal-pane-{{ $loop->index }}-1"
                                           aria-selected="true">@lang('MinmaxBase::administrator.form.image.advance.tab')</a>
                                    </li>
                                </ul>
                                <div class="tab-content mt-4" id="{{ $id }}-tabModalContent-{{ $loop->index }}">
                                    <div class="tab-pane fade show active" id="{{ $id }}-tabModal-pane-{{ $loop->index }}-1" role="tabpanel" aria-labelledby="{{ $id }}-tabModal-{{ $loop->index }}-1">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <fieldset>
                                                    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-3"></i>@lang('MinmaxBase::administrator.form.image.advance.fieldSet.base')</legend>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.path')</label>
                                                        <div class="col-sm-10">
                                                            <div class="form-text">{{ array_get($image, 'path') }}</div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.title')</label>
                                                        <div class="col-sm-10">
                                                            <div class="form-text">{{ array_get($image, 'title') }}</div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" >@lang('MinmaxBase::administrator.form.image.advance.field.description')</label>
                                                        <div class="col-sm-10">
                                                            <div class="form-text">{{ array_get($image, 'description') }}</div>
                                                        </div>
                                                    </div>

                                                </fieldset>

                                                <fieldset class="mt-4">
                                                    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-3"></i>@lang('MinmaxBase::administrator.form.image.advance.fieldSet.detail')</legend>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.creator')</label>
                                                        <div class="col-sm-10">
                                                            <div class="form-text">{{ array_get($image, 'creator') }}</div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">@lang('MinmaxBase::administrator.form.image.advance.field.copyright')</label>
                                                        <div class="col-sm-10">
                                                            <div class="form-text">{{ array_get($image, 'copyright') }}</div>
                                                        </div>
                                                    </div>

                                                </fieldset>

                                                <fieldset class="mt-4">
                                                    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-3"></i>@lang('MinmaxBase::administrator.form.image.advance.fieldSet.option')</legend>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="{{ $id }}-{{ $loop->index }}-cover">@lang('MinmaxBase::administrator.form.image.advance.field.cover')</label>
                                                        <div class="col-sm-10">
                                                            <div class="form-text">{{ array_get($image, 'cover') != 1 ? __('MinmaxBase::administrator.form.image.advance.options.cover_0') : __('MinmaxBase::administrator.form.image.advance.options.cover_1') }}</div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="{{ $id }}-{{ $loop->index }}-max_width">@lang('MinmaxBase::administrator.form.image.advance.field.size')</label>
                                                        <div class="col-sm-10">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="row">
                                                                        <label class="col-auto form-text">@lang('MinmaxBase::administrator.form.image.advance.field.max_width')</label>
                                                                        <div class="col mb-2">{{ array_get($image, 'max_width') ?? '-' }}</div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="row">
                                                                        <label class="col-auto form-text">@lang('MinmaxBase::administrator.form.image.advance.field.max_height')</label>
                                                                        <div class="col mb-2">{{ array_get($image, 'max_height') ?? '-' }}</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="row">
                                                                        <label class="col-auto form-text">@lang('MinmaxBase::administrator.form.image.advance.field.min_width')</label>
                                                                        <div class="col mb-2">{{ array_get($image, 'min_width') ?? '-' }}</div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="row">
                                                                        <label class="col-auto form-text">@lang('MinmaxBase::administrator.form.image.advance.field.min_height')</label>
                                                                        <div class="col mb-2">{{ array_get($image, 'min_height') ?? '-' }}</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @foreach($additionalFields as $column => $type)
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">@lang($packagePrefix . 'models.' . str_replace('-', '.additional.', $id) . '.' . $column)</label>
                                                        <div class="col-sm-10">
                                                        @switch($type)
                                                            @case('text')
                                                            <div class="form-text">{{ array_get($image, $column) }}</div>
                                                            @break
                                                            @case('textarea')
                                                            <div class="form-text">{!! nl2br(array_get($image, $column) ?? '') !!}</div>
                                                            @break
                                                        @endswitch
                                                        </div>
                                                    </div>
                                                    @endforeach

                                                </fieldset>
                                            </div>
                                            <div class="col-lg-6">
                                                <img class="img-fluid" src="{{ getImagePath(array_get($image, 'path')) }}" alt="{{ array_get($image, 'path') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if($anchorEnable)
                <div class="modal fade bd-example-modal-lg" id="{{ $id }}-modal-anchor-{{ $loop->index }}" tabindex="-1" role="dialog" aria-hidden="true">
                </div>
                @endif
            </div>
            @endforeach
            @if(count($images) < 1)
            <div class="card mr-2 d-inline-block ui-sortable-handle">
                <div class="thumb">
                    <span class="imgFill imgLiquid_bgSize imgLiquid_ready"><img src="{{ asset('static/images/noimage.gif') }}" alt="" /></span>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
