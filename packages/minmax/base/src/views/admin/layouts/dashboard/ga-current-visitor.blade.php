<?php
/**
 * @var string $id
 * @var string $title
 * @var array $position
 * @var array $options
 */
?>
<section class="panel h-45-c">
    <header class="panel-heading text-center">
        <h2 class="h5 px-2 float-left">{{ $title }}</h2>
    </header>
    <div class="panel-body">
        <div class="h3 text-center font-weight-bold pb-1"><span class="text-main" id="{{ $id }}-current">0</span></div>
        <div id="{{ $id }}-session" class="progress" title="0% @lang('MinmaxBase::admin.dashboard.new_session')" style="height: 5px;">
            <div class="progress-bar progress-bar-striped progress-bar-animated bg-main" role="progressbar" style="width: 0" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <div class="row text-center py-3">
            <div class="col">
                <span class="d-block text-muted small">@lang('MinmaxBase::admin.dashboard.session_page')</span><!--
                --><span id="{{ $id }}-page" class="d-block font-weight-bold mb-1">0.0</span></div>
            <div class="col">
                <span class="d-block text-muted small">@lang('MinmaxBase::admin.dashboard.stay_time')</span><!--
                --><span id="{{ $id }}-stay" class="d-block font-weight-bold mb-1">00:00:00</span></div>
            <div class="col">
                <span class="d-block text-muted small">@lang('MinmaxBase::admin.dashboard.exit_rate')</span><!--
                --><span id="{{ $id }}-exit" class="d-block font-weight-bold mb-1">0.0%</span></div>
        </div>
    </div>
</section>

@push('scripts')
<script>
$(function () {
    $.get('{{ langRoute('admin.ajax.ga.current-visitor') }}', function (response) {
        if (response.hasOwnProperty('currentVisitor')) {
            $('#{{ $id }}-current').text(response.currentVisitor);
        }
        if (response.hasOwnProperty('percentNewSessions')) {
            $('#{{ $id }}-session').attr('title', response.percentNewSessions + " @lang('MinmaxBase::admin.dashboard.new_session')");
            $('#{{ $id }}-session > div').css('width', response.percentNewSessions);
        }
        if (response.hasOwnProperty('pageViewsPerSession')) {
            $('#{{ $id }}-page').text(response.pageViewsPerSession);
        }
        if (response.hasOwnProperty('avgTimeOnPage')) {
            $('#{{ $id }}-stay').text(response.avgTimeOnPage);
        }
        if (response.hasOwnProperty('exitRate')) {
            $('#{{ $id }}-exit').text(response.exitRate);
        }
    });
});
</script>
@endpush
