<?php
/**
 * @var string $id
 * @var string $title
 * @var array $position
 * @var array $options
 */
?>
<section class="panel h-100-c">
    <header class="panel-heading">
        <h2 class="h5 float-left">{{ $title }}</h2>
    </header>
    <div class="panel-wrapper"></div>
    <div class="panel-body" style="min-height:{{ array_get($position, 'height') ?? 270 }}px;">
        <div class="table-responsive-sm">
            <table class="table">
                <thead class="font-weight-bold">
                <tr>
                    <th>#</th>
                    <th>@lang('MinmaxBase::admin.dashboard.path')</th>
                    <th class="text-right">@lang('MinmaxBase::admin.dashboard.path_view')</th>
                    <th class="text-right">@lang('MinmaxBase::admin.dashboard.path_time')</th>
                </tr>
                </thead>
                <tbody id="{{ $id }}">
                <tr>
                    <th class="text-center" scope="row" colspan="4">No Data</th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>

@push('scripts')
<script>
$(function () {
    $.get('{{ langRoute('admin.ajax.ga.path-views') }}', function (response) {
        if (response.length > 0) {
            $('#{{ $id }} > tr').remove();
        }
        var pathRoot = '{{ preg_replace('/\/$/i', '', url('')) }}';
        for (var rowIndex in response) {
            if (response.hasOwnProperty(rowIndex)) {
                $('#{{ $id }}').append('<tr>' +
                    '<th class="text-center" scope="row">' + (parseInt(rowIndex) + 1) + '</th>' +
                    '<td><a href="' + pathRoot + response[rowIndex].path + '" target="_blank">' + response[rowIndex].path + '</a></td>\n' +
                    '<td class="text-right">' + response[rowIndex].count + '</td>\n' +
                    '<td class="text-right">' + response[rowIndex].time + '</td></tr>');
            }
        }
    });
});
</script>
@endpush
