<?php
/**
 * @var string $id
 * @var string $title
 * @var array $position
 * @var array $options
 */
?>
<section class="panel p-3 bg-primary text-white">
    <div class="row">
        <div class="col"><span id="{{ $id }}" class="d-block mb-1 h3">-</span><span class="d-block">{{ $title }}</span></div>
        <div class="col"><span class="{{ array_get($options, 'icon', 'icon-eye2') }} display-4"></span></div>
    </div>
</section>

@push('scripts')
<script>
$(function () {
    $.get('{{ langRoute('admin.ajax.ga.total-new-visitors') }}', function (response) {
        if (response.hasOwnProperty('total')) {
            $('#{{ $id }}').text(response.total);
        }
    });
});
</script>
@endpush
