<?php
/**
 * @var string $id
 * @var string $title
 * @var array $position
 * @var array $options
 */
?>
<section class="panel h-100-c">
    <header class="panel-heading">
        <h2 class="h5 float-left">{{ $title }}</h2>
    </header>
    <div id="{{ $id }}" class="panel-body">
        <div id="{{ $id }}-traffic" style="width: 100%; height:195px;"></div>
    </div>
</section>

@push('scripts')
<script>
$(function () {
    AmCharts.makeChart("{{ $id }}-traffic", {
        "type": "pie",
        "theme": "light",
        "dataLoader": {
            "url": "{{ langRoute('admin.ajax.ga.visited-source') }}",
            "showCurtain": false
        },
        "titleField": "source",
        "valueField": "count",
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "innerRadius": "0",
        "labelRadius": 5,
        "radius": "45%",
        "labelText": "",
    });

    $.get('{{ langRoute('admin.ajax.ga.visited-source-list') }}', function (response) {
        var counter = 0;
        var $moduleBlock = $('#{{ $id }}');
        for(var k in response) {
            if (response.hasOwnProperty(k)) {
                if (counter > 0) $moduleBlock.append('<hr class="my-2 w-100">');

                var rowData = response[k];
                var $row = $('<div class="row pl-3 pr-1"></div>');
                var $label = $('<div class="col-sm-8 col-xs-12"></div>');
                var $timeline = $('<div class="col-sm-4 col-xs-12">' +
                    '<div class="float-right mb-1" id="{{ $id }}-traffic-' + k + '" style="width: 100%; height:30px;"></div>' +
                    '</div>');

                var badgeClass = '';
                switch (counter) {
                    case 0:
                        badgeClass = 'badge-info'; break;
                    case 1:
                        badgeClass = 'badge-warning'; break;
                    case 2:
                        badgeClass = 'badge-success'; break;
                }

                $label
                    .append('<span class="badge ' + badgeClass + ' float-left clabels d-inline mt-1 mr-3 no-radius"></span>')
                    .append('<div class="clabels-text d-inline txt-dark text-capitalize float-left">' +
                        '<span class="d-block font-weight-bold mb-1">' + rowData.rate + '%  ' + rowData.source + '</span>' +
                        '<span class="d-block text-muted small">' + rowData.count + ' @lang('MinmaxBase::admin.dashboard.visits')</span>' +
                        '</div>');

                $row.append($label).append($timeline);

                $moduleBlock.append($row);

                AmCharts.makeChart('{{ $id }}-traffic-' + k, {
                    "type": "serial",
                    "theme": "light",
                    "dataProvider": rowData.week,
                    "valueAxes": [
                        {"id": "v1", "axisAlpha": 0, "gridAlpha": 0, "zeroGridAlpha": 0}
                    ],
                    "categoryAxis": {"dashLength": 0, "gridAlpha": 0, "axisAlpha": 0},
                    "balloon": {"shadowAlpha": 0},
                    "marginTop": 0,
                    "marginRight": 0,
                    "marginLeft": 0,
                    "marginBottom": 0,
                    "autoMargins": false,
                    "startDuration": 1,
                    "graphs": [{
                        "id": "g1",
                        "balloon": {"drop": false, "adjustBorderColor": false, "color": "#ffffff"},
                        "fillAlphas": 0.2,
                        "bullet": "round",
                        "bulletBorderAlpha": 1,
                        "bulletColor": "#FFFFFF",
                        "hideBulletsCount": 5,
                        "lineThickness": 1,
                        "valueField": "value"
                    }],
                    "chartCursor": {
                        "valueLineEnabled": false,
                        "categoryBalloonEnabled": false,
                        "cursorColor": "#67b7dc",
                        "pan": true
                    },
                    "categoryField": "date",
                    "export": {"enabled": false},
                });
            }

            counter++;
        }
    });
});
</script>
@endpush
