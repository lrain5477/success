<?php
/**
 * @var string $id
 * @var string $title
 * @var array $position
 * @var array $options
 */
?>
<section class="panel h-100-c">
    <header class="panel-heading">
        <h2 class="h5 float-left">{{ $title }}</h2>
    </header>
    <div class="panel-wrapper"></div>
    <div class="panel-body pb-3">
        <div id="{{ $id }}-age" style="height: {{ array_get($position, 'height') ?? 265 }}px;"></div>
    </div>
</section>

@push('scripts')
<script>
$(function () {
    AmCharts.makeChart("{{ $id }}-age", {
        "type": "serial", "theme": "light",
        "dataLoader": {
            "url": "{{ langRoute('admin.ajax.ga.age-source') }}",
            "format": "json", "showErrors": true, "noStyles": true, "async": true
        },
        "valueAxes": [{
            "gridColor": "#878787", "gridAlpha": 0.2, "dashLength": 0, "color": "#878787"
        }],
        "gridAboveGraphs": true,
        "startDuration": 1,
        "graphs": [{
            "balloonText": "[[category]]: <b>[[value]]</b>",
            "fillAlphas": 0.8,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "visits",
            "color": "#878787"
        }],
        "chartCursor": {"categoryBalloonEnabled": false, "cursorAlpha": 0, "zoomable": false},
        "categoryField": "age",
        "categoryAxis": {
            "gridPosition": "start",
            "gridAlpha": 0,
            "tickPosition": "start",
            "tickLength": 20,
            "color": "#878787"
        },
        "export": {"enabled": false}
    });
});
</script>
@endpush
