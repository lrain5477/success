<?php

namespace Minmax\Base;

use Exception;
use Google_Client as GoogleClient;
use Google_Service_Analytics as GoogleServiceAnalytics;
use GuzzleHttp\Client as GuzzleClient;

class GoogleAnalyticsClient
{
    /**
     * @var GoogleServiceAnalytics $service
     */
    protected $service;

    /**
     * @var integer $viewId
     */
    protected $viewId;

    /**
     * GoogleAnalyticsClient constructor.
     */
    public function __construct()
    {
        $this->setViewId(config('services.google_analytics.view_id'));

        $this->setService(config('services.google_analytics.service_account_credentials_json'));
    }

    /**
     * @param  string $viewId
     */
    public function setViewId($viewId)
    {
        $this->viewId = $viewId;
    }

    /**
     * Login google api and get Google Analytics service connection
     * @param  string $credentialPath is a JSON file path
     */
    public function setService($credentialPath)
    {
        try {
            $client = new GoogleClient();
            if (config('app.env') !== 'production') {
                $client->setHttpClient(new GuzzleClient(['verify' => false]));
            }
            $client->setScopes([GoogleServiceAnalytics::ANALYTICS_READONLY]);
            $client->setAuthConfig($credentialPath);
            $this->service = new GoogleServiceAnalytics($client);
        } catch (Exception $e) {
            $this->service = null;
        }
    }

    /**
     * @param string $dataType can be blows ga, rt
     * @param array $params
     * @return null
     */
    public function query($dataType = 'ga', $params = [])
    {
        if (is_null($this->service)) {
            return null;
        }

        switch($dataType) {
            case 'ga':
                if(count($params) > 0) {
                    try {
                        $result = $this->service->data_ga->get(
                            "ga:{$this->viewId}",
                            date('Y-m-d', ($params['startTime'] ?? time())),
                            date('Y-m-d', ($params['endTime'] ?? time())),
                            ($params['metrics'] ?? ''),
                            $others = ($params['others'] ?? [])
                        );

                        while ($nextLink = $result->getNextLink()) {
                            if (isset($others['max-results']) && count($result->rows) >= $others['max-results']) {
                                break;
                            }
                            $options = [];
                            parse_str(substr($nextLink, strpos($nextLink, '?') + 1), $options);
                            $response = $this->service->data_ga->call('get', [$options], 'Google_Service_Analytics_GaData');
                            /** @var Object $response */
                            if ($response->rows) {
                                $result->rows = array_merge($result->rows, $response->rows);
                            }
                            $result->nextLink = $response->nextLink;
                        }
                    } catch (Exception $e) {
                        $result = null;
                    }
                }
                break;
            case 'rt':
                if(is_string($params) || (is_array($params) && count($params) > 0)) {
                    try {
                        $result = $this->service->data_realtime->get(
                            "ga:{$this->viewId}",
                            is_string($params) ? $params : ($params['metrics'] ?? '')
                        );
                    } catch (Exception $e) {
                        $result = null;
                    }
                }
                break;
        }

        return $result ?? null;
    }
}
