<?php

if (! function_exists('isJson')) {
    /**
     * @param  string $string
     * @return boolean
     */
    function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}

if (! function_exists('str_cover')) {
    /**
     * @param  string $string
     * @param  integer $start How many chars you want to keep count from head
     * @param  integer $last How many chars you want to keep count from end
     * @param  integer $max
     * @param  string $delimiter
     * @return string
     */
    function str_cover($string, $start = 1, $last = 0, $max = 0, $delimiter = 'Ｏ')
    {
        if (! isset($string)) {
            return '';
        }

        $charSet = preg_split('//u', $string, -1, PREG_SPLIT_NO_EMPTY);

        if (count($charSet) <= $start) {
            $start = 0;
        }

        if (count($charSet) < $start + $last + 1) {
            $last = 0;
        }

        if ($max < 1) {
            $max = count($charSet) - $start - $last;
            $max = max(1, $max);
        }

        $startString = '';
        for ($i = 0; $i < min($start, count($charSet)); $i++) {
            $startString .= $charSet[$i];
        }

        $lastString = '';
        for ($i = count($charSet) - $last; $i < count($charSet); $i++) {
            $lastString .= $charSet[$i];
        }

        $coverString = str_repeat($delimiter, min($max, count($charSet) - $start - $last));

        return $startString . $coverString . $lastString;
    }
}

if (! function_exists('uuidl')) {
    /**
     * Create a custom length uuid.
     *
     * @param  integer  $length
     * @return string
     */
    function uuidl($length = 16)
    {
        $generateUuid = \Illuminate\Support\Str::uuid();
        $generateTime = str_replace(str_split('0123456789abcdef'), str_split('tuwjxyzghmirsnpq'), strtolower(base_convert(time(), 10, 16)));
        $skipIndex = [3, 4, 7, 8, 10, 11, 13, 14];
        $upperIndex = [];
        $collectedIndex = [];

        for($i = 0; $i < ceil($length / 2); $i++) {
            do {
                $randomIndex = rand(0, $length - 1);
            } while (in_array($randomIndex, $upperIndex));

            $upperIndex[] = $randomIndex;
        }

        do {
            $randomIndex = rand(0, strlen($generateUuid) - 1);
            if (!in_array($randomIndex, $collectedIndex) && substr($generateUuid, $randomIndex, 1) !== '-') {
                $collectedIndex[] = $randomIndex;
            }
        } while (count($collectedIndex) < $length);

        sort($collectedIndex);

        $uuidString = '';
        foreach ($collectedIndex as $arrayKey => $index) {
            $uuidString .= in_array($arrayKey, $skipIndex)
                ? substr($generateTime, array_search($arrayKey, $skipIndex), 1)
                : substr($generateUuid, $index, 1);
        }

        foreach ($upperIndex as $index) {
            $originalCharacter = substr($uuidString, $index, 1);
            $uuidString = substr_replace($uuidString, strtoupper($originalCharacter), $index, 1);
        }

        return $uuidString;
    }
}

if (! function_exists('langId')) {
    /**
     * Get a language id via language code.
     *
     * @param  string  $code
     * @return string
     */
    function langId($code)
    {
        /** @var array $langMap */
        $langMap = Cache::rememberForever('langId', function() {
            try {
                return DB::table('world_language')
                    ->where('active', 1)
                    ->orderBy('sort')
                    ->pluck('id', 'code')
                    ->toArray();
            } catch (\Exception $e) {
                return [];
            }
        });

        return $langMap[$code] ?? '';
    }
}

if (! function_exists('langDB')) {
    /**
     * Get a local content from database via key.
     *
     * @param  string  $key
     * @param  bool  $showKey
     * @param  string  $langKey
     * @return string
     */
    function langDB($key, $showKey = false, $langKey = null)
    {
        $langKey = $langKey ?? app()->getLocale();

        $tableName = array_get(explode('.', $key), '0');

        if (! in_array(config('app.env'), ['production', 'development'])) {
            Cache::forget("langMap.{$langKey}.{$tableName}");
        }

        /** @var array $langMap */
        $langMap = Cache::rememberForever("langMap.{$langKey}.{$tableName}", function() use ($langKey, $tableName) {
            try {
                return DB::table('language_resource')
                    ->where('key', 'like', "{$tableName}.%")
                    ->where('language_id', langId($langKey))
                    ->orderBy('key')
                    ->pluck('text', 'key')
                    ->toArray();
            } catch (\Exception $e) {
                return [];
            }
        });

        return $langMap[$key] ?? ($showKey ? $key : null);
    }
}

if (! function_exists('langDBSet')) {
    /**
     * Get a local content set from database via key.
     *
     * @param  string  $key
     * @param  string  $langKey
     * @return array|string[]
     */
    function langDBSet($key, $langKey = null)
    {
        $langKey = $langKey ?? app()->getLocale();

        $tableName = array_get(explode('.', $key), '0');

        if (! in_array(config('app.env'), ['production', 'development'])) {
            Cache::forget("langMap.{$langKey}.{$tableName}");
        }

        /** @var array $langMap */
        $langMap = Cache::rememberForever("langMap.{$langKey}.{$tableName}", function() use ($langKey, $tableName) {
            try {
                return DB::table('language_resource')
                    ->where('key', 'like', "{$tableName}.%")
                    ->where('language_id', langId($langKey))
                    ->orderBy('key')
                    ->pluck('text', 'key')
                    ->toArray();
            } catch (\Exception $e) {
                return [];
            }
        });

        return collect($langMap)
            ->filter(function ($text, $textKey) use ($key) {
                return preg_match("/^{$key}/i", $textKey) === 1;
            })
            ->toArray();
    }
}

if (! function_exists('langRoute')) {
    /**
     * Get a local content from database via key.
     *
     * @param  string  $name
     * @param  array  $parameters
     * @param  boolean  $absolute
     * @param  integer  $position
     * @return string
     */
    function langRoute($name, $parameters = [], $absolute = true, $position = 1)
    {
        if (is_null($name) || $name == '') return '';

        $routeSet = [];
        $nameSet = explode('.', $name);

        foreach ($nameSet as $index => $uri) {
            if ($index == $position) {
                $routeSet[] = app()->getLocale();
            }

            $routeSet[] = $uri;
        }

        return route(implode('.', $routeSet), $parameters, $absolute);
    }
}

if (! function_exists('saveLang')) {
    /**
     * Save (update or insert) a local content to database via key.
     *
     * @param  string  $key
     * @param  string  $value
     * @param  string  $langKey
     * @return bool
     */
    function saveLang($key, $value, $langKey = null)
    {
        $langKey = $langKey ?? session('admin-formLocal', app()->getLocale());

        $tableName = array_get(explode('.', $key), '0');

        $attributes = ['language_id' => langId($langKey), 'key' => $key];
        $values = ['text' => is_array($value) ? json_encode($value) : $value, 'updated_at' => date('Y-m-d H:i:s')];

        try {
            if (DB::table('language_resource')->updateOrInsert($attributes, $values)) {
                Cache::forget("langMap.{$langKey}.{$tableName}");
                return true;
            }
        } catch (\Exception $e) {}

        return false;
    }
}

if (! function_exists('deleteLang')) {
    /**
     * Delete local contents via key or key set.
     *
     * @param  array $keys
     * @param  string $langKey
     * @return bool
     */
    function deleteLang($keys, $langKey = null)
    {
        $keys = array_wrap($keys);

        try {
            if ($langKey) {
                if (DB::table('language_resource')->whereIn('key', $keys)->where('language_id', langId($langKey))->delete()) {
                    foreach ($keys as $key) {
                        $tableName = array_get(explode('.', $key), '0');
                        Cache::forget("langMap.{$langKey}.{$tableName}");
                    }
                    return true;
                }
            } else {
                if (DB::table('language_resource')->whereIn('key', $keys)->delete()) {
                    $langMap = cache('langId') ?? [];
                    foreach ($langMap as $code => $id) {
                        foreach ($keys as $key) {
                            $tableName = array_get(explode('.', $key), '0');
                            Cache::forget("langMap.{$code}.{$tableName}");
                        }
                    }
                    return true;
                }
            }
        } catch (\Exception $e) {}

        return false;
    }
}

if (! function_exists('minmaxEncrypt')) {
    function minmaxEncrypt($str, $upperCase = false)
    {
        $secretKey = array_merge(
            str_split(str_replace(['base64:', '=', '/'], ['', '', ''], config('app.key'))),
            range('a', 'z'),
            range(0, 9)
        );

        return preg_replace_callback('/./', function($matches) use($secretKey, $upperCase) {
            $ascii = ord($matches[0]);
            $hexCode = str_split(dechex($ascii));
            $transform = '';
            foreach ($hexCode as $hex) {
                $charIndex = array_search($hex, $secretKey);
                $transform .= str_pad(dechex($charIndex), 2, '0', STR_PAD_LEFT);
            }
            $reversed = strrev($transform);
            $encoded = $upperCase ? strtoupper($reversed) : $reversed;

            return $encoded;
        }, $str);
    }
}

if (! function_exists('minmaxDecrypt')) {
    function minmaxDecrypt($str)
    {
        $pattern = '[A-F0-9]{4}';

        if (preg_match("/^(?:$pattern)+$/i", $str)) {
            $secretKey = array_merge(
                str_split(str_replace(['base64:', '=', '/'], ['', '', ''], config('app.key'))),
                range('a', 'z'),
                range(0, 9)
            );

            return preg_replace_callback("/$pattern/i", function($matches) use ($secretKey) {
                $transform = strrev($matches[0]);
                $haxCode = preg_replace_callback("/[A-F0-9]{2}/i", function ($matches) use ($secretKey) {
                    $charIndex = hexdec($matches[0]);
                    return $secretKey[$charIndex];
                }, $transform);
                $ascii = hexdec($haxCode);
                $char = chr($ascii);
                return $char;
            }, $str);
        }

        return $str;
    }
}

if (! function_exists('systemParam')) {
    /**
     * Get system parameter via key.
     *
     * @param  string $key
     * @param  string $langKey
     * @return string|array
     */
    function systemParam($key = null, $langKey = null)
    {
        $langKey = $langKey ?? app()->getLocale();

        $params = Cache::rememberForever("systemParams.{$langKey}", function () {
            return \Minmax\Base\Models\SystemParameterGroup::with('systemParameterItems')
                ->where('active', true)
                ->orderBy('sort')
                ->get()
                ->mapWithKeys(function ($item) {
                    /** @var \Minmax\Base\Models\SystemParameterGroup $item */
                    return [
                        $item->code => $item->systemParameterItems
                            ->where('active', true)
                            ->mapWithKeys(function ($item) {
                                /** @var \Minmax\Base\Models\SystemParameterItem $item */
                                return [$item->value => ['title' => $item->label, 'options' => $item->options]];
                            })
                    ];
                })
                ->toArray();
        });

        if (count($params) == 0) Cache::forget("systemParams.{$langKey}");

        return is_null($key) ? $params : array_get($params, $key, $key);
    }
}

if (! function_exists('siteParam')) {
    /**
     * Get site parameter via key.
     *
     * @param  string $key
     * @param  string $langKey
     * @param  string $category
     * @return string|array
     */
    function siteParam($key = null, $langKey = null, $category = null)
    {
        $langKey = $langKey ?? app()->getLocale();

        if (is_null($category)) {
            $params = Cache::rememberForever("siteParams.{$langKey}", function () {
                return \Minmax\Base\Models\SiteParameterGroup::with('siteParameterItems')
                    ->where('active', true)
                    ->orderBy('sort')
                    ->get()
                    ->mapWithKeys(function ($item) {
                        /** @var \Minmax\Base\Models\SiteParameterGroup $item */
                        return [
                            $item->code => $item->siteParameterItems
                                ->where('active', true)
                                ->mapWithKeys(function ($item) {
                                    /** @var \Minmax\Base\Models\SiteParameterItem $item */
                                    return [($item->value ?? $item->id) => ['title' => $item->label, 'options' => $item->options, 'details' => $item->details]];
                                })
                        ];
                    })
                    ->toArray();
            });

            if (count($params) == 0) Cache::forget("siteParams.{$langKey}");
        } else {
            $params = \Minmax\Base\Models\SiteParameterGroup::with('siteParameterItems')
                ->where('category', $category)
                ->where('active', true)
                ->orderBy('sort')
                ->get()
                ->mapWithKeys(function ($item) {
                    /** @var \Minmax\Base\Models\SiteParameterGroup $item */
                    return [
                        $item->code => $item->siteParameterItems
                            ->where('active', true)
                            ->mapWithKeys(function ($item) {
                                /** @var \Minmax\Base\Models\SiteParameterItem $item */
                                return [($item->value ?? $item->id) => ['title' => $item->label, 'options' => $item->options, 'details' => $item->details]];
                            })
                    ];
                })
                ->toArray();
        }

        return is_null($key) ? $params : array_get($params, $key, $key);
    }
}

if (! function_exists('serviceConfig')) {
    /**
     * @param  string $key
     * @param  boolean $group
     * @return \Minmax\Base\Models\ServiceConfig|null
     */
    function serviceConfig($key, $group = false)
    {
        $configs = Cache::rememberForever('serviceConfigs', function () {
            return \Minmax\Base\Models\ServiceConfig::query()
                ->where(['active' => true])
                ->orderBy('sort')
                ->get();
        });

        if (count($configs) == 0) Cache::forget('serviceConfigs');

        return $group
            ? $configs->where('group', $key)->first()
            : $configs->where('code', $key)->first();
    }
}

if (! function_exists('customMailer')) {
    /**
     * @param  string $key
     * @param  boolean $group
     * @param  boolean $register
     * @return void
     */
    function customMailer($key, $group = false, $register = false)
    {
        try {
            if ($emailService = serviceConfig($key, $group)) {
                /** @var \Minmax\Base\Models\ServiceConfig $emailService */
                $override = [
                    'mail.host' => $emailService->host_url,
                    'mail.port' => $emailService->host_port ?? config('mail.port'),
                ];
                foreach ($emailService->options as $optionKey => $optionValue) {
                    $override["mail.{$optionKey}"] = $optionValue;
                }
                config($override);

                if ($register) {
                    app('mailer')->setSwiftMailer(new \Swift_Mailer((new \Illuminate\Mail\TransportManager(app()))->driver()));
                    app('mailer')->alwaysFrom(config('mail.from.address'), config('mail.from.name'));
                }
            }
        } catch (\Exception $e) {}
    }
}

if (! function_exists('getImagePath')) {
    /**
     * Check and get image path.
     *
     * @param  string  $path
     * @param  boolean  $transparent
     * @return string
     */
    function getImagePath($path, $transparent = true)
    {
        $imgTransparent = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
        $imgNoImage = asset('static/images/noimage.gif');

        if (! isset($path)) {
            return $transparent ? $imgTransparent : $imgNoImage;
        }

        if (is_null($path)) {
            return $transparent ? $imgTransparent : $imgNoImage;
        }

        if (is_string($path) && $path == '') {
            return $transparent ? $imgTransparent : $imgNoImage;
        }

        if (! file_exists(public_path($path))) {
            return $transparent ? $imgTransparent : $imgNoImage;
        }

        return asset($path);
    }
}

if (! function_exists('getThumbnailPath')) {
    /**
     * Check and get thumbnail path.
     *
     * @param  string  $path
     * @param  integer  $width
     * @param  integer|boolean  $height
     * @param  boolean  $transparent
     * @param  array  $options  with 'upSize', 'overwrite', 'quality'
     * @return string
     */
    function getThumbnailPath($path, $width, $height = null, $transparent = true, $options = [])
    {
        if (is_bool($height)) {
            $transparent = $height;
            $height = $width;
        }

        if (is_null($height)) {
            $height = $width;
        }

        $thumbnailPath = \Minmax\Base\Helpers\Image::makeThumbnail($path, $width, $height, ...$options);

        return getImagePath($thumbnailPath, $transparent);
    }
}

if(!function_exists('sendProxyMail')) {
    /**
     * @param  array|string $to
     * @param  \Illuminate\Mail\Mailable  $mailable
     * @param  string $from
     * @param  array|string $cc
     * @param  array|string $bcc
     * @return array|mixed
     */
    function sendProxyMail($to = [], $mailable = null, $from = '', $cc = [], $bcc = [])
    {
        $proxyUrl = "http://proxy.mailer.youweb.tw/send";

        $parameters = [
            'project' => config('mail.username'),
            'auth_key' => config('mail.password'),
            'to' => array_wrap($to),
            'cc' => array_wrap($cc),
            'bcc' => array_wrap($bcc),
            'body' => $mailable->render(),
            'subject' => $mailable->subject,
            'from' => $from == '' ? config('mail.from.address') : $from,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $proxyUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameters));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        $output = curl_exec($ch);
        curl_close($ch);

        return empty($output) ? false : json_decode($output, true);
    }
}
