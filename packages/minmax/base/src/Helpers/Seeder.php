<?php

namespace Minmax\Base\Helpers;

class Seeder
{
    /**
     * Get language id list. You can set arguments for where condition.
     *
     * @return array
     */
    public static function getLanguageIdList()
    {
        if (func_num_args() == 0) {
            return \DB::table('world_language')->orderBy('id')->pluck('id', 'code')->toArray();
        } else {
            return \DB::table('world_language')->where(func_get_args())->orderBy('id')->pluck('id', 'code')->toArray();
        }
    }

    /**
     * @param  string $table
     * @param  array $dataSet
     * @param  int $language
     * @param  int $start
     * @return array
     */
    public static function getLanguageResourceArray($table, $dataSet, $language = 1, $start = 1)
    {
        $timestamp    = date('Y-m-d H:i:s');
        $resourceData = [];

        foreach ($dataSet as $key => $attributes) {
            $index = $attributes['id'] ?? ($key + $start);
            foreach (array_except($attributes, 'id') as $column => $text) {
                $thisKey = "{$table}.{$column}.{$index}";
                array_push($resourceData, [
                    'language_id' => $language,
                    'key' => $thisKey,
                    'text' => $text,
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp
                ]);
            }
        }

        return $resourceData;
    }

    /**
     * Make language array merge into language resource array.
     *
     * @param  array $resource is language resource. (will straight be changed)
     * @param  string $table
     * @param  array $dataSet
     * @param  array|integer $language
     * @param  null|integer $start
     * @param  boolean $single
     * @return void
     */
    public static function setLanguageResource(&$resource, $table, $dataSet, $language = [], $start = null, $single = true)
    {
        $language = array_wrap($language);
        $start    = is_null($start) ? 1 : $start;

        foreach ($language as $index => $langId) {
            if ($single) {
                $resource = array_merge($resource, static::getLanguageResourceArray($table, $dataSet, $langId, $start));
            } else {
                if (array_key_exists($index, $dataSet)) {
                    $resource = array_merge($resource, static::getLanguageResourceArray($table, $dataSet[$index], $langId, $start));
                }
            }
        }
    }

    /**
     * Replace data language columns to language key, and make language array merge into language resource array.
     *
     * @param  array $dataSet is data row set. (will straight be changed)
     * @param  array $resource is language resource. (will straight be changed)
     * @param  string $table
     * @param  array|string $columns
     * @param  array|integer $language
     * @param  null|integer $start
     * @return void
     */
    public static function setLanguageExchange(&$dataSet, &$resource, $table, $columns, $language = [], $start = null)
    {
        $columns = array_wrap($columns);
        $start   = is_null($start) ? 1 : $start;
        $languageData = [];

        foreach ($dataSet as $key => $attributes) {
            if (array_key_exists('id', $attributes)) {
                array_set($languageData, "{$key}.id", $keyId = array_get($attributes, 'id'));
            } else {
                $keyId = $key + $start;
            }
            foreach ($columns as $column) {
                if (array_key_exists($column, $attributes)) {
                    array_set($languageData, "{$key}.{$column}", array_get($attributes, $column));
                    array_set($dataSet, "{$key}.{$column}", "{$table}.{$column}.{$keyId}");
                }
            }
        }

        foreach ($language as $index => $langId) {
            $resource = array_merge($resource, static::getLanguageResourceArray($table, $languageData, $langId, $start));
        }
    }

    /**
     * @param  string $guard can be 'admin', 'web'
     * @param  string $groupName
     * @param  string $groupTitle
     * @param  array $permissions
     * @param  integer $sort
     * @return array
     */
    public static function getPermissionArray($guard, $groupName, $groupTitle, $permissions = ['C', 'R', 'U', 'D'], $sort = 1)
    {
        $timestamp       = date('Y-m-d H:i:s');
        $permissionArray = [];

        if (is_int($permissions)) {
            $sort = $permissions;
            $permissions = ['C', 'R', 'U', 'D'];
        }

        if(in_array('R', $permissions)) {
            $permissionArray[] = [
                'guard' => $guard, 'group' => $groupName,
                'name' => $groupName . 'Show', 'label' => '瀏覽', 'display_name' => $groupTitle . ' [瀏覽]', 'description' => $groupTitle . ' [瀏覽]',
                'sort' => $sort, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ];
        }

        if(in_array('C', $permissions)) {
            $permissionArray[] = [
                'guard' => $guard, 'group' => $groupName,
                'name' => $groupName . 'Create', 'label' => '新增', 'display_name' => $groupTitle . ' [新增]', 'description' => $groupTitle . ' [新增]',
                'sort' => $sort, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ];
        }

        if(in_array('U', $permissions)) {
            $permissionArray[] = [
                'guard' => $guard, 'group' => $groupName,
                'name' => $groupName . 'Edit', 'label' => '編輯', 'display_name' => $groupTitle . ' [編輯]', 'description' => $groupTitle . ' [編輯]',
                'sort' => $sort, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ];
        }

        if(in_array('D', $permissions)) {
            $permissionArray[] = [
                'guard' => $guard, 'group' => $groupName,
                'name' => $groupName . 'Destroy', 'label' => '刪除', 'display_name' => $groupTitle . ' [刪除]', 'description' => $groupTitle . ' [刪除]',
                'sort' => $sort, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ];
        }

        if(in_array('I', $permissions)) {
            $permissionArray[] = [
                'guard' => $guard, 'group' => $groupName,
                'name' => $groupName . 'Import', 'label' => '匯入', 'display_name' => $groupTitle . ' [匯入]', 'description' => $groupTitle . ' [匯入]',
                'sort' => $sort, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ];
        }

        if(in_array('O', $permissions)) {
            $permissionArray[] = [
                'guard' => $guard, 'group' => $groupName,
                'name' => $groupName . 'Export', 'label' => '匯出', 'display_name' => $groupTitle . ' [匯出]', 'description' => $groupTitle . ' [匯出]',
                'sort' => $sort, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ];
        }

        return $permissionArray;
    }

    /**
     * Get given table next auto increment id. (this table need have a column with auto increment setting)
     *
     * @param  string $table
     * @return integer
     */
    public static function getTableNextIncrement($table)
    {
        return intval(
            array_get(
                json_decode(
                    json_encode(
                        \DB::select("show table status like '{$table}'")
                    ),
                true),
            '0.Auto_increment', 1)
        );
    }

    /**
     * Delete system parameters via given group codes.
     *
     * @param  array|string $groupCodes
     * @param  boolean $deleteGroup
     * @return void
     */
    public static function deleteSystemParametersByGroupCode($groupCodes, $deleteGroup = true)
    {
        $parameterCodeSet = array_wrap($groupCodes);

        $groups = \DB::table('system_parameter_group')->whereIn('code', $parameterCodeSet)->get(['id', 'title']);
        $items = \DB::table('system_parameter_item')->whereIn('group_id', $groups->pluck('id')->toArray())->get(['id', 'label']);

        \DB::table('language_resource')->whereIn('key', $items->pluck('label')->toArray())->delete();
        \DB::table('system_parameter_item')->whereIn('id', $items->pluck('id')->toArray())->delete();

        if ($deleteGroup) {
            \DB::table('language_resource')->whereIn('key', $groups->pluck('title')->toArray())->delete();
            \DB::table('system_parameter_group')->whereIn('id', $groups->pluck('id')->toArray())->delete();
        }

        unset($groups, $items);
    }

    /**
     * Delete site parameters via given group codes.
     *
     * @param  array|string $groupCodes
     * @param  boolean $deleteGroup
     * @return void
     */
    public static function deleteSiteParametersByGroupCode($groupCodes, $deleteGroup = true)
    {
        $parameterCodeSet = array_wrap($groupCodes);

        $groups = \DB::table('site_parameter_group')->whereIn('code', $parameterCodeSet)->get(['id', 'title']);
        $items = \DB::table('site_parameter_item')->whereIn('group_id', $groups->pluck('id')->toArray())->get(['id', 'label', 'details']);

        \DB::table('language_resource')
            ->whereIn('key', $items->pluck('label')->toArray())
            ->orWhereIn('key', $items->pluck('details')->toArray())
            ->delete();
        \DB::table('site_parameter_item')->whereIn('id', $items->pluck('id')->toArray())->delete();

        if ($deleteGroup) {
            \DB::table('language_resource')->whereIn('key', $groups->pluck('title')->toArray())->delete();
            \DB::table('site_parameter_group')->whereIn('id', $groups->pluck('id')->toArray())->delete();
        }

        unset($groups, $items);
    }
}
