<?php

namespace Minmax\Base\Administrator;

/**
 * Class SiteDashboardPresenter
 */
class SiteDashboardPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxBase::';

    protected $languageColumns = ['title'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'guard' => [
                'admin' => ['title' => 'Admin', 'options' => []],
                'administrator' => ['title' => 'Administrator', 'options' => []],
            ],
            'active' => systemParam('active'),
        ];
    }
}
