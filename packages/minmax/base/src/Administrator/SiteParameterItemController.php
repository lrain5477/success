<?php

namespace Minmax\Base\Administrator;

/**
 * Class SiteParameterItemGroupController
 */
class SiteParameterItemController extends Controller
{
    protected $packagePrefix = 'MinmaxBase::';

    public function __construct(SiteParameterItemRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    /**
     * Set datatable filter.
     *
     * @param  mixed $datatable
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    protected function doDatatableFilter($datatable, $request)
    {
        $datatable->filter(function($query) use ($request) {
            /** @var \Illuminate\Database\Query\Builder $query */

            if($request->has('filter')) {
                $query->where(function ($query) use ($request) {
                    /** @var \Illuminate\Database\Query\Builder $query */

                    foreach ($request->input('filter', []) as $column => $value) {
                        if (empty($column) || is_null($value) || $value === '') continue;

                        if ($column == 'label') {
                            try {
                                $filterLabel = collect(cache('langMap.' . app()->getLocale() . '.site_parameter_item', []))
                                    ->filter(function ($item, $key) use ($value) {
                                        return preg_match('/^site_parameter_item\.label\./', $key) > 0 && strpos($item, $value) !== false;
                                    })
                                    ->keys()
                                    ->toArray();
                                $query->orWhereIn($column, $filterLabel);
                            } catch (\Exception $e) {
                            }
                            continue;
                        }

                        $query->orWhere($column, 'like', "%{$value}%");
                    }
                });
            }

            if($request->has('equal')) {
                foreach($request->input('equal', []) as $column => $value) {
                    if (empty($column) || is_null($value) || $value === '') continue;

                    $query->where($column, $value);
                }
            }
        });

        $labelLanguageSet = collect(langDBSet('site_parameter_item.label.'))->sort()->keys()->implode("','");
        $datatable->orderColumn('label', "field(label,'{$labelLanguageSet}') $1");

        return $datatable;
    }
}
