<?php

namespace Minmax\Base\Administrator;

use Closure;

/**
 * Abstract class Repository
 */
abstract class Repository
{
    /**
     * You must set which model using.
     */
    const MODEL = null;

    /**
     * If doesn't have update timestamp column, please set null.
     */
    const UPDATED_AT = 'updated_at';

    /**
     * @var string $sort
     */
    protected $sort = null;

    /**
     * @var bool $sorting
     */
    protected $sorting = false;

    /**
     * @var array $languageColumns
     */
    protected $languageColumns = [];

    /**
     * @var array $languageBuffer
     */
    protected $languageBuffer = [];

    /**
     * @var \Illuminate\Database\Eloquent\Model $model
     */
    protected $model;

    /**
     * @var array $attributes
     */
    protected $attributes = [];

    /**
     * Get table name of this model
     *
     * @return string
     */
    abstract protected function getTable();

    /**
     * @param  \Illuminate\Database\Eloquent\Model $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @param  array $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * Set $model to null
     */
    public function clearModel()
    {
        $this->setModel(null);
    }

    /**
     * Set $attributes to empty array
     */
    public function clearAttributes()
    {
        $this->setAttributes([]);
    }

    /**
     * Search by primary key
     *
     * @param  mixed $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->query()->find($id);
    }

    /**
     * Search by condition
     *
     * @param  string|array|Closure  $column
     * @param  string  $operator
     * @param  mixed  $value
     * @param  string  $boolean
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function one($column = null, $operator = null, $value = null, $boolean = 'and')
    {
        $query = $this->query();

        if ($column instanceof Closure) {
            $subQuery = $this->query();

            $column($subQuery);

            return $query->addNestedWhereQuery($subQuery->getQuery(), $boolean)->first();
        } elseif (is_null($column)) {
            return $query->first();
        } else {
            return $query->where(...func_get_args())->first();
        }
    }

    /**
     * Search to a collection with condition
     *
     * @param  string|array|Closure  $column
     * @param  string  $operator
     * @param  mixed  $value
     * @param  string  $boolean
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]
     */
    public function all($column = null, $operator = null, $value = null, $boolean = 'and')
    {
        $query = $this->query();

        if ($column instanceof Closure) {
            $subQuery = $this->query();

            $column($subQuery);

            return $query->addNestedWhereQuery($subQuery->getQuery(), $boolean)->get();
        } elseif (is_null($column)) {
            return $query->get();
        } else {
            return $query->where(...func_get_args())->get();
        }
    }

    /**
     * Create a new model
     *
     * @param  array $attributes
     * @return \Illuminate\Database\Eloquent\Model|null
     * @throws \Exception
     */
    public function create($attributes)
    {
        $this->clearAttributes();
        $this->clearModel();

        $this->setAttributes($attributes);

        $this->beforeCreate();

        $this->saveImages();

        $model = $this->serialization();

        try {
            \DB::beginTransaction();

            if ($model->save()) {
                $model = $this->saveLanguage($model);
                $this->setModel($model);
                $this->sorting();
                $this->afterCreate();
                \DB::commit();
                return $this->model;
            }

            \DB::rollBack();
        } catch (\Exception $e) {
            \DB::rollBack();
        }

        return null;
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @param  array $attributes
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function save($model, $attributes)
    {
        $this->clearAttributes();
        $this->clearModel();
        $this->clearLanguageBuffer();

        $this->setAttributes($attributes);
        $this->setModel($model);

        $this->beforeSave();

        $this->saveImages();

        foreach ($this->languageColumns as $column) {
            if (array_key_exists($column, $this->attributes)) {
                $this->attributes[$column] = $this->exchangeLanguage($this->attributes, $column, $this->model->getAttribute($this->model->getKeyName()));
            }
        }

        if (count($this->languageBuffer) > 0 && !is_null(static::UPDATED_AT)) {
            $this->attributes[static::UPDATED_AT] = date('Y-m-d H:i:s');
        }

        $this->model->fill($this->attributes);

        try {
            \DB::beginTransaction();

            if ($this->model->save()) {
                $model = $this->saveLanguage($this->model);
                $this->setModel($model);
                $this->sorting();
                $this->afterSave();
                \DB::commit();
                return $this->model;
            }

            \DB::rollBack();
        } catch (\Exception $e) {
            \DB::rollBack();
        }

        return null;
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @param  bool $force
     * @return bool
     * @throws \Exception
     */
    public function delete($model, $force = false)
    {
        $this->clearModel();

        $this->setModel($model);

        $this->beforeDelete();

        try {
            \DB::beginTransaction();

            $deleteResult = $force ? $this->model->forceDelete() : $this->model->delete();

            if (! (method_exists($this->model, 'trashed') && $this->model->trashed())) {
                $this->deleteLanguage($this->model);
            }

            if ($deleteResult) {
                $this->sorting();
                $this->afterDelete();
                $this->clearModel();
                \DB::commit();
                return $deleteResult;
            }

            \DB::rollBack();
        } catch (\Exception $e) {
            \DB::rollBack();
        }

        return false;
    }

    /**
     * Create a model query builder
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return call_user_func(static::MODEL.'::query');
    }

    /**
     * Clear valuable $languageBuffer to empty array
     *
     * @return void
     */
    protected function clearLanguageBuffer()
    {
        $this->languageBuffer = [];
    }

    /**
     * Save value to buffer array and return a language map key
     *
     * @param  array $attributes
     * @param  string $column
     * @param  string|null $id
     * @return string
     */
    protected function exchangeLanguage($attributes, $column, $id = null)
    {
        if(!array_key_exists($column, $attributes)) return null;

        $attribute = $attributes[$column];

        if (!array_key_exists($column, $this->languageBuffer)) {
            $this->languageBuffer[$column] = $attribute;

            if ($id) {
                $attribute = "{$this->getTable()}.{$column}.{$id}";
            } else {
                $attribute = "{$this->getTable()}.{$column}";
            }
        }

        return $attribute;
    }

    /**
     * Save language value from buffer array to database and after return model
     *
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @param  string|array $columns
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function saveLanguage($model, $columns = [])
    {
        if (!is_array($columns)) {
            $columns = [$columns];
        }

        if (count($this->languageBuffer) > 0) {
            foreach ($this->languageBuffer as $column => $value) {
                if (count($columns) > 0 && !in_array($column, $columns)) continue;

                $key = "{$this->getTable()}.{$column}.{$model->getKey()}";
                $model->setAttribute($column, $key);
                saveLang($key, $value);
            }
            $model->save();
        }

        return $model;
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return bool
     */
    protected function deleteLanguage($model)
    {
        $keyList = [];

        foreach ($this->languageColumns as $column) {
            $keyList[] = "{$this->getTable()}.{$column}.{$model->getKey()}";
        }

        if (count($keyList) > 0) {
            if (deleteLang($keyList)) {
                return true;
            }
            return false;
        }

        return true;
    }

    /**
     * Save Images
     */
    protected function saveImages ()
    {
        if ($uploadFiles = array_pull($this->attributes, 'images')) {
            foreach ($uploadFiles as $columnKey => $columnInput) {
                if (array_key_exists('path', $columnInput)) {
                    $filePath = 'files/' . array_get($columnInput, 'path', 'uploads');
                    $fileList = [];
                    foreach (array_get($columnInput, 'upload') ?? [] as $fileIndex => $fileItem) {
                        if ($base64Str = array_get($fileItem, 'data')) {
                            $fileName = time() . rand(10000, 99999) . str_pad($fileIndex, 3, '0', STR_PAD_LEFT);
                            if (preg_match("/^data\:image\/png\;/i", $base64Str) === 1) {
                                $fileName .= '.png';
                            } elseif (preg_match("/^data\:image\/gif\;/i", $base64Str) === 1) {
                                $fileName .= '.gif';
                            } else {
                                $fileName .= '.jpg';
                            }
                            $base64Image = str_replace(' ', '+', substr($base64Str, strpos($base64Str, ',') + 1));

                            if (! \File::isDirectory(dirname(public_path("{$filePath}/{$fileName}")))) {
                                \File::makeDirectory(dirname(public_path("{$filePath}/{$fileName}")), 0777, true, true);
                            }

                            \File::put(public_path("{$filePath}/{$fileName}"), base64_decode($base64Image));
                            $fileList[] = ['path' => "{$filePath}/{$fileName}"];
                        }
                    }
                    array_set($this->attributes, $columnKey, array_merge(array_get($columnInput, 'origin', []), $fileList));
                } else {
                    foreach ($columnInput as $subColumnKey => $subColumnInput) {
                        $filePath = 'files/' . array_get($subColumnInput, 'path', 'uploads');
                        $fileList = [];
                        foreach (array_get($subColumnInput, 'upload') ?? [] as $fileIndex => $fileItem) {
                            if ($base64Str = array_get($fileItem, 'data')) {
                                $fileName = time() . rand(10000, 99999) . str_pad($fileIndex, 3, '0', STR_PAD_LEFT);
                                if (preg_match("/^data\:image\/png\;/i", $base64Str) === 1) {
                                    $fileName .= '.png';
                                } elseif (preg_match("/^data\:image\/gif\;/i", $base64Str) === 1) {
                                    $fileName .= '.gif';
                                } else {
                                    $fileName .= '.jpg';
                                }
                                $base64Image = str_replace(' ', '+', substr($base64Str, strpos($base64Str, ',') + 1));

                                if (! \File::isDirectory(dirname(public_path("{$filePath}/{$fileName}")))) {
                                    \File::makeDirectory(dirname(public_path("{$filePath}/{$fileName}")), 0777, true, true);
                                }

                                \File::put(public_path("{$filePath}/{$fileName}"), base64_decode($base64Image));
                                $fileList[] = ['path' => "{$filePath}/{$fileName}"];
                            }
                        }
                        array_set($this->attributes, "{$columnKey}.{$subColumnKey}", array_merge(array_get($subColumnInput, 'origin', []), $fileList));
                    }
                }
            }
        }
    }

    /**
     * Serialize input attributes to a new model
     *
     * @param  array $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function serialization($attributes = null)
    {
        $this->clearLanguageBuffer();

        $attributes = $attributes ?? $this->attributes;

        $model = static::MODEL;
        /** @var \Illuminate\Database\Eloquent\Model $model */
        $model = new $model();

        $primaryKey = $model->incrementing ? null : uuidl();

        if (!$model->incrementing) {
            $model->setAttribute($model->getKeyName(), $primaryKey);
        }

        if (! is_null($this->sort) && array_key_exists($this->sort, $attributes)) {
            if (is_null($attributes[$this->sort]) || $attributes[$this->sort] < 1) {
                $amount = \DB::table($this->getTable())->count();
                $attributes[$this->sort] = $amount + 1;
            }
        }

        foreach ($attributes as $column => $value) {
            if (in_array($column, $this->languageColumns)) {
                $model->setAttribute($column, $this->exchangeLanguage($attributes, $column, $primaryKey));
            } else {
                $model->setAttribute($column, $value);
            }
        }

        return $model;
    }

    /**
     * Auto sorting rows.
     * @param  string $func
     */
    protected function sorting($func = null)
    {
        if ($this->sorting && ! is_null($this->sort)) {
            $table = $this->getTable();
            $primaryKey = $this->model->getKeyName();
            $rowKey = $this->model->getKey();
            $where = $this->getSortWhere();
            $where = $where == '' ? '' : "and ({$where}) ";
            $orderBy = is_null($this->model->getAttribute('created_at'))
                ? "{$this->sort} asc"
                : "{$this->sort} asc, created_at asc";

            switch ($func) {
                case 'delete':
                    \DB::update(
                        "update {$table} a inner join (" .
                            "select t.{$primaryKey}, @rowNumber := @rowNumber + 1 as rn from " .
                                "(select {$primaryKey} from {$table} where {$primaryKey} != '{$rowKey}' {$where}" .
                                    "order by {$orderBy}) t, " .
                                "(select @rowNumber := 0) var " .
                        ") as b on a.{$primaryKey} = b.{$primaryKey} set a.{$this->sort} = b.rn");
                    break;
                default:
                    if (array_key_exists($this->sort, $this->attributes)) {
                        $amount = blank($this->getSortWhere())
                            ? \DB::table($this->getTable())->count()
                            : \DB::table($this->getTable())->whereRaw($this->getSortWhere())->count();
                        $currentSort = $this->model->getAttribute($this->sort);
                        if ($currentSort > $amount) {
                            $this->model->update([$this->sort => $amount]);
                            $currentSort = $amount;
                        }
                        $prevAmount = $currentSort - 1;
                        $nextAmount = $amount - $currentSort;
                        \DB::update(
                            "update {$table} a inner join (" .
                                "select t.{$primaryKey}, @rowNumber := @rowNumber + 1 as rn from " .
                                    "(select {$primaryKey} from {$table} where {$primaryKey} != '{$rowKey}' {$where}" .
                                        "order by {$orderBy} limit 0, {$prevAmount}) t, " .
                                    "(select @rowNumber := 0) var " .
                            ") as b on a.{$primaryKey} = b.{$primaryKey} set a.{$this->sort} = b.rn");
                        \DB::update(
                            "update {$table} a inner join (" .
                                "select t.{$primaryKey}, @rowNumber := @rowNumber + 1 as rn from " .
                                    "(select {$primaryKey} from {$table} where {$primaryKey} != '{$rowKey}' {$where}" .
                                        "order by {$orderBy} limit {$prevAmount}, {$nextAmount}) t, " .
                                    "(select @rowNumber := {$currentSort}) var " .
                            ") as b on a.{$primaryKey} = b.{$primaryKey} set a.{$this->sort} = b.rn");
                    }
                    break;
            }
        }
    }

    /**
     * @return string
     */
    protected function getSortWhere()
    {
        return '';
    }

    /**
     * Before create method
     * In here can use or change $this->attributes
     */
    protected function beforeCreate() {}

    /**
     * Before save method
     * In here can use or change $this->attributes and $this->model
     */
    protected function beforeSave() {}

    /**
     * Before delete method
     * In here can use or change $this->model
     */
    protected function beforeDelete() {}

    /**
     * After create method
     * In here can use or change $this->attributes and $this->model
     */
    protected function afterCreate() {}

    /**
     * After save method
     * In here can use or change $this->attributes and $this->model
     */
    protected function afterSave() {}

    /**
     * After delete method
     * In here can use or change $this->model
     */
    protected function afterDelete() {}
}
