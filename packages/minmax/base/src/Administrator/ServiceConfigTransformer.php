<?php

namespace Minmax\Base\Administrator;

use Minmax\Base\Models\ServiceConfig;

/**
 * Class ServiceConfigTransformer
 */
class ServiceConfigTransformer extends Transformer
{
    /**
     * Transformer constructor.
     * @param  ServiceConfigPresenter $presenter
     * @param  string $uri
     */
    public function __construct(ServiceConfigPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  ServiceConfig $model
     * @return array
     * @throws \Throwable
     */
    public function transform(ServiceConfig $model)
    {
        return [
            'code' => $this->presenter->getGridText($model, 'code'),
            'title' => $this->presenter->getGridText($model, 'title'),
            'group' => $this->presenter->getGridText($model, 'group'),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
