<?php

namespace Minmax\Base\Administrator;

/**
 * Class PermissionPresenter
 */
class PermissionPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxBase::';

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'guard' => [
                'admin' => ['title' => 'Admin', 'options' => []],
                'web' => ['title' => 'Web', 'options' => []],
            ],
            'active' => systemParam('active'),
        ];
    }
}