<?php

namespace Minmax\Base\Administrator;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class SiteDashboardRequest
 */
class SiteDashboardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'SiteDashboard.guard' => 'required|string',
                    'SiteDashboard.title' => 'required|string',
                    'SiteDashboard.presenter' => 'required|string',
                    'SiteDashboard.position' => 'required|array',
                    'SiteDashboard.options' => 'nullable|array',
                    'SiteDashboard.sort' => 'required|integer|min:1',
                    'SiteDashboard.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'SiteDashboard.guard' => 'required|string',
                    'SiteDashboard.title' => 'required|string',
                    'SiteDashboard.presenter' => 'required|string',
                    'SiteDashboard.position' => 'required|array',
                    'SiteDashboard.options' => 'nullable|array',
                    'SiteDashboard.sort' => 'nullable|integer|min:1',
                    'SiteDashboard.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'SiteDashboard.guard' => __('MinmaxBase::models.SiteDashboard.guard'),
            'SiteDashboard.title' => __('MinmaxBase::models.SiteDashboard.title'),
            'SiteDashboard.presenter' => __('MinmaxBase::models.SiteDashboard.presenter'),
            'SiteDashboard.position' => __('MinmaxBase::models.SiteDashboard.position'),
            'SiteDashboard.options' => __('MinmaxBase::models.SiteDashboard.options'),
            'SiteDashboard.sort' => __('MinmaxBase::models.SiteDashboard.sort'),
            'SiteDashboard.active' => __('MinmaxBase::models.SiteDashboard.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('administrator', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('administrator', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
