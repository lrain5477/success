<?php

namespace Minmax\Base\Administrator;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class ServiceConfigRequest
 */
class ServiceConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'ServiceConfig.code' => [
                        'required',
                        'string',
                        Rule::unique('service_config', 'code')->ignore($this->route('id')),
                    ],
                    'ServiceConfig.group' => 'nullable|string',
                    'ServiceConfig.title' => 'required|string',
                    'ServiceConfig.host' => 'required|string',
                    'ServiceConfig.options' => 'nullable|array',
                    'ServiceConfig.parameters' => 'nullable|array',
                    'ServiceConfig.sort' => 'required|integer|min:1',
                    'ServiceConfig.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'ServiceConfig.code' => 'required|string|unique:service_config,code',
                    'ServiceConfig.group' => 'nullable|string',
                    'ServiceConfig.title' => 'required|string',
                    'ServiceConfig.host' => 'required|string',
                    'ServiceConfig.options' => 'nullable|array',
                    'ServiceConfig.parameters' => 'nullable|array',
                    'ServiceConfig.sort' => 'nullable|integer|min:1',
                    'ServiceConfig.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'ServiceConfig.code' => __('MinmaxBase::models.ServiceConfig.code'),
            'ServiceConfig.group' => __('MinmaxBase::models.ServiceConfig.group'),
            'ServiceConfig.title' => __('MinmaxBase::models.ServiceConfig.title'),
            'ServiceConfig.host' => __('MinmaxBase::models.ServiceConfig.host'),
            'ServiceConfig.options' => __('MinmaxBase::models.ServiceConfig.options'),
            'ServiceConfig.parameters' => __('MinmaxBase::models.ServiceConfig.parameters'),
            'ServiceConfig.sort' => __('MinmaxBase::models.ServiceConfig.sort'),
            'ServiceConfig.active' => __('MinmaxBase::models.ServiceConfig.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('administrator', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('administrator', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
