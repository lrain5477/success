<?php

namespace Minmax\Base\Administrator;

use Minmax\Base\Models\SiteDashboard;

/**
 * Class SiteDashboardRepository
 * @property SiteDashboard $model
 * @method SiteDashboard find($id)
 * @method SiteDashboard one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method SiteDashboard[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method SiteDashboard create($attributes)
 * @method SiteDashboard save($model, $attributes)
 * @method SiteDashboard|\Illuminate\Database\Eloquent\Builder query()
 */
class SiteDashboardRepository extends Repository
{
    const MODEL = SiteDashboard::class;

    protected $sort = 'sort';

    protected $languageColumns = ['title'];

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'site_dashboard';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|SiteDashboard[]
     */
    public function getDashboardModules()
    {
        return $this->query()
            ->where('guard', 'administrator')
            ->where('active', true)
            ->orderByRaw("json_extract(`position`, '$.row') asc, json_extract(`position`, '$.column') asc, `sort` asc")
            ->get()
            ->map(function ($item) {
                /** @var SiteDashboard $item */
                $presenter = explode('@', $item->presenter);
                $item->setAttribute('class_name', head($presenter));
                $item->setAttribute('method_name', last($presenter));
                return $item;
            })
            ->groupBy(function ($item) {
                /** @var SiteDashboard $item */
                return array_get($item->position, 'row');
            })
            ->map(function ($item) {
                /** @var \Illuminate\Support\Collection|SiteDashboard[] $item */
                return $item
                    ->groupBy(function ($item) {
                        /** @var SiteDashboard $item */
                        return array_get($item->position, 'column');
                    });
            });
    }
}
