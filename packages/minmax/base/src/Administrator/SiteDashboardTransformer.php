<?php

namespace Minmax\Base\Administrator;

use Minmax\Base\Models\SiteDashboard;

/**
 * Class SiteDashboardTransformer
 */
class SiteDashboardTransformer extends Transformer
{
    /**
     * Transformer constructor.
     * @param  SiteDashboardPresenter $presenter
     * @param  string $uri
     */
    public function __construct(SiteDashboardPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  SiteDashboard $model
     * @return array
     * @throws \Throwable
     */
    public function transform(SiteDashboard $model)
    {
        return [
            'guard' => $this->presenter->getGridSelection($model, 'guard'),
            'title' => $this->presenter->getGridText($model, 'title'),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
