<?php

namespace Minmax\Base\Administrator;

/**
 * Class SystemLogController
 */
class SystemLogController extends Controller
{
    protected $packagePrefix = 'MinmaxBase::';

    public function __construct(SystemLogRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    protected function getQueryBuilder()
    {
        if ($logUri = request('uri')) {
            if ($logId = request('object')) {
                return $this->modelRepository->query()
                    ->where(function ($query) use ($logUri) {
                        /** @var \Illuminate\Database\Eloquent\Builder $query */
                        $query
                            ->where('uri', 'like', "%/{$logUri}")
                            ->orWhere('uri', 'like', "%/{$logUri}/%");
                    })
                    ->where('id', $logId);
            }
        }

        return $this->modelRepository->query()
            ->where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('-3 months')));
    }
}
