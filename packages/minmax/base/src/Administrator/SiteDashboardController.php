<?php

namespace Minmax\Base\Administrator;

/**
 * Class SiteDashboardController
 */
class SiteDashboardController extends Controller
{
    protected $packagePrefix = 'MinmaxBase::';

    public function __construct(SiteDashboardRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    /**
     * Set datatable filter.
     *
     * @param  mixed $datatable
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    protected function doDatatableFilter($datatable, $request)
    {
        $datatable->filter(function($query) use ($request) {
            /** @var \Illuminate\Database\Query\Builder $query */

            if($request->has('filter')) {
                $query->where(function ($query) use ($request) {
                    /** @var \Illuminate\Database\Query\Builder $query */

                    foreach ($request->input('filter', []) as $column => $value) {
                        if (empty($column) || is_null($value) || $value === '') continue;

                        if ($column == 'title') {
                            try {
                                $filterDisplayName = collect(cache('langMap.' . app()->getLocale() . '.site_dashboard', []))
                                    ->filter(function ($item, $key) use ($value) {
                                        return preg_match('/^site_dashboard\.title\./', $key) > 0 && strpos($item, $value) !== false;
                                    })
                                    ->keys()
                                    ->toArray();
                                $query->orWhereIn($column, $filterDisplayName);
                            } catch (\Exception $e) {
                            }
                            continue;
                        }

                        $query->orWhere($column, 'like', "%{$value}%");
                    }
                });
            }

            if($request->has('equal')) {
                foreach($request->input('equal', []) as $column => $value) {
                    if (empty($column) || is_null($value) || $value === '') continue;

                    $query->where($column, $value);
                }
            }
        });

        $displayNameLanguageSet = collect(langDBSet('site_dashboard.title.'))->sort()->keys()->implode("','");
        $datatable->orderColumn('title', "field(title,'{$displayNameLanguageSet}') $1");

        return $datatable;
    }
}
