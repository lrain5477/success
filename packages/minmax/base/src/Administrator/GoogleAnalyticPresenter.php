<?php

namespace Minmax\Base\Administrator;

use Minmax\Base\Models\SiteDashboard;

/**
 * Class GoogleAnalyticPresenter
 */
class GoogleAnalyticPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxBase::';

    /**
     * @param  SiteDashboard $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function visitedSource(SiteDashboard $model)
    {
        $componentData = [
            'id' => "dashboard-{$model->id}",
            'title' => $model->title,
            'position' => $model->position,
            'options' => $model->options,
        ];

        return view('MinmaxBase::administrator.layouts.dashboard.ga-visited-source', $componentData);
    }

    /**
     * @param  SiteDashboard $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function currentVisitor(SiteDashboard $model)
    {
        $componentData = [
            'id' => "dashboard-{$model->id}",
            'title' => $model->title,
            'position' => $model->position,
            'options' => $model->options,
        ];

        return view('MinmaxBase::administrator.layouts.dashboard.ga-current-visitor', $componentData);
    }

    /**
     * @param  SiteDashboard $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function topBrowsers(SiteDashboard $model)
    {
        $componentData = [
            'id' => "dashboard-{$model->id}",
            'title' => $model->title,
            'position' => $model->position,
            'options' => $model->options,
        ];

        return view('MinmaxBase::administrator.layouts.dashboard.ga-top-browsers', $componentData);
    }

    /**
     * @param  SiteDashboard $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function worldSource(SiteDashboard $model)
    {
        $componentData = [
            'id' => "dashboard-{$model->id}",
            'title' => $model->title,
            'position' => $model->position,
            'options' => $model->options,
        ];

        return view('MinmaxBase::administrator.layouts.dashboard.ga-world-source', $componentData);
    }

    /**
     * @param  SiteDashboard $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function todayTotalVisitors(SiteDashboard $model)
    {
        $componentData = [
            'id' => "dashboard-{$model->id}",
            'title' => $model->title,
            'position' => $model->position,
            'options' => $model->options,
        ];

        return view('MinmaxBase::administrator.layouts.dashboard.ga-today-total-visitors', $componentData);
    }

    /**
     * @param  SiteDashboard $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function totalNewVisitors(SiteDashboard $model)
    {
        $componentData = [
            'id' => "dashboard-{$model->id}",
            'title' => $model->title,
            'position' => $model->position,
            'options' => $model->options,
        ];

        return view('MinmaxBase::administrator.layouts.dashboard.ga-total-new-visitors', $componentData);
    }

    /**
     * @param  SiteDashboard $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pathViews(SiteDashboard $model)
    {
        $componentData = [
            'id' => "dashboard-{$model->id}",
            'title' => $model->title,
            'position' => $model->position,
            'options' => $model->options,
        ];

        return view('MinmaxBase::administrator.layouts.dashboard.ga-path-views', $componentData);
    }

    /**
     * @param  SiteDashboard $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ageSource(SiteDashboard $model)
    {
        $componentData = [
            'id' => "dashboard-{$model->id}",
            'title' => $model->title,
            'position' => $model->position,
            'options' => $model->options,
        ];

        return view('MinmaxBase::administrator.layouts.dashboard.ga-age-source', $componentData);
    }

    /**
     * @param  SiteDashboard $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function referrerKeyword(SiteDashboard $model)
    {
        $componentData = [
            'id' => "dashboard-{$model->id}",
            'title' => $model->title,
            'position' => $model->position,
            'options' => $model->options,
        ];

        return view('MinmaxBase::administrator.layouts.dashboard.ga-referrer-keyword', $componentData);
    }
}
