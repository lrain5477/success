<?php

namespace Minmax\Newsletter\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Minmax\Member\Models\Member;
use Minmax\Newsletter\Jobs\NewsletterSms;
use Minmax\Newsletter\Mails\NewsletterEpaper;
use Minmax\Newsletter\Models\EpaperHistory;
use Minmax\Newsletter\Models\EpaperReceiver;
use Minmax\Newsletter\Models\EpaperSubscriber;
use Minmax\Newsletter\Models\SmsHistory;
use Minmax\Newsletter\Models\SmsReceiver;
use Minmax\Newsletter\Models\SmsSubscriber;

class MinmaxNewsletterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'minmax:newsletter
                            {type=all : Can be "all", "epaper", "sms"}
                            {--limit=15 : How many mail sent in one time}
                            {--single : Only one newsletter process}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check newsletter can be sent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $inputType = $this->argument('type');
        $inputLimit = $this->option('limit');
        $inputSingle = $this->option('single');

        $counter = 0;

        if ($inputType == 'all' || $inputType == 'epaper') {
            $epaperHistories = EpaperHistory::query()
                ->where('schedule_at', '<=', date('Y-m-d H:i:s'))
                ->whereNull('sent_at')
                ->orderBy('schedule_at')
                ->get();

            foreach ($epaperHistories as $epaper) {
                /** @var EpaperHistory $epaper */
                if ($inputSingle && $counter > 0) break;

                $receivers = EpaperSubscriber::query()
                    ->with('member')
                    ->where(function ($query) use ($epaper) {
                        /** @var \Illuminate\Database\Eloquent\Builder $query */

                        if (!blank($languages = array_get($epaper->objects, 'languages'))) {
                            $query->whereIn('language_id', $languages);
                        }

                        if (!blank($categories = array_get($epaper->objects, 'categories'))) {
                            $query->where(function ($query) use ($categories) {
                                /** @var \Illuminate\Database\Eloquent\Builder $query */
                                foreach ($categories as $category) {
                                    $query->orWhereRaw("json_contains(categories, '\"{$category}\"')");
                                }
                            });
                        }

                        if (!blank($roles = array_get($epaper->objects, 'roles'))) {
                            $query->whereHas('member.roles', function ($query) use ($roles) {
                                /** @var \Illuminate\Database\Eloquent\Builder $query */
                                $query->whereIn('id', $roles);
                            });
                        }
                    })
                    ->get()
                    ->map(function ($subscriber) {
                        /** @var EpaperSubscriber $subscriber */
                        return [
                            'email' => $subscriber->email,
                            'name' => is_null($subscriber->member) ? null : $subscriber->member->name
                        ];
                    })
                    ->toBase();

                if (!blank($username = array_get($epaper->objects, 'members'))) {
                    $members = Member::query()
                        ->whereIn('username', explode(',', $username))
                        ->whereNotNull('email')
                        ->where('active', true)
                        ->get()
                        ->map(function ($member) {
                            /** @var Member $member */
                            return ['email' => $member->email, 'name' => $member->name];
                        })
                        ->toBase();
                    $receivers = $receivers->merge($members);
                }

                if (!blank($emails = array_get($epaper->objects, 'emails'))) {
                    $emails = collect(explode(',', $emails))
                        ->map(function ($email) { return ['email' => $email, 'name' => null]; });
                    $receivers = $receivers->merge($emails);
                }

                $receivers = $receivers->unique('email')->values();

                $epaper->epaperReceivers()->createMany($receivers->toArray());

                $epaper->sent_at = now();
                $epaper->save();

                $counter++;
            }

            $epaperReceivers = EpaperReceiver::query()
                ->with('epaperHistory')
                ->where('epaper_status', 'pending')
                ->orderBy('created_at')
                ->limit($inputLimit)
                ->get();

            foreach ($epaperReceivers as $receiver) {
                /** @var EpaperReceiver $receiver */
                Mail::to([$receiver->only(['email', 'name'])])
                    ->send((new NewsletterEpaper($receiver->epaperHistory, $receiver))->onQueue('newsletterEpaper'));
                $receiver->epaper_status = 'sent';
                $receiver->save();
            }
        }

        if ($inputType == 'all' || $inputType == 'sms') {
            $smsHistories = SmsHistory::query()
                ->where('schedule_at', '<=', date('Y-m-d H:i:s'))
                ->whereNull('sent_at')
                ->orderBy('schedule_at')
                ->get();

            foreach ($smsHistories as $sms) {
                /** @var SmsHistory $sms */
                if ($inputSingle && $counter > 0) break;

                $receivers = SmsSubscriber::query()
                    ->with('member')
                    ->where(function ($query) use ($sms) {
                        /** @var \Illuminate\Database\Eloquent\Builder $query */

                        if (!blank($languages = array_get($sms->objects, 'languages'))) {
                            $query->whereIn('language_id', $languages);
                        }

                        if (!blank($categories = array_get($sms->objects, 'categories'))) {
                            $query->where(function ($query) use ($categories) {
                                /** @var \Illuminate\Database\Eloquent\Builder $query */
                                foreach ($categories as $category) {
                                    $query->orWhereRaw("json_contains(categories, '\"{$category}\"')");
                                }
                            });
                        }

                        if (!blank($roles = array_get($sms->objects, 'roles'))) {
                            $query->whereHas('member.roles', function ($query) use ($roles) {
                                /** @var \Illuminate\Database\Eloquent\Builder $query */
                                $query->whereIn('id', $roles);
                            });
                        }
                    })
                    ->get()
                    ->map(function ($subscriber) {
                        /** @var SmsSubscriber $subscriber */
                        return [
                            'mobile' => $subscriber->mobile,
                            'name' => is_null($subscriber->member) ? null : $subscriber->member->name
                        ];
                    })
                    ->toBase();

                if (!blank($username = array_get($sms->objects, 'members'))) {
                    $members = Member::query()
                        ->with('memberDetail')
                        ->whereIn('username', explode(',', $username))
                        ->whereNotNull('email')
                        ->where('active', true)
                        ->get()
                        ->map(function ($member) {
                            /** @var Member $member */
                            return ['mobile' => array_get($member->memberDetail->contact, 'mobile'), 'name' => $member->name];
                        })
                        ->toBase();
                    $receivers = $receivers->merge($members);
                }

                if (!blank($mobiles = array_get($sms->objects, 'mobiles'))) {
                    $mobiles = collect(explode(',', $mobiles))
                        ->map(function ($mobile) { return ['mobile' => $mobile, 'name' => null]; });
                    $receivers = $receivers->merge($mobiles);
                }

                $receivers = $receivers
                    ->filter(function ($item) {
                        return ! blank(array_get($item, 'mobile'));
                    })
                    ->unique('mobile')
                    ->values();

                $sms->smsReceivers()->createMany($receivers->toArray());

                $sms->sent_at = now();
                $sms->save();

                $counter++;
            }

            $smsReceivers = SmsReceiver::query()
                ->with('smsHistory')
                ->where('sms_status', 'pending')
                ->orderBy('created_at')
                ->limit($inputLimit)
                ->get();

            foreach ($smsReceivers as $receiver) {
                /** @var SmsReceiver $receiver */
                NewsletterSms::dispatch($receiver->smsHistory, $receiver)->onQueue('newsletterSms');
                $receiver->sms_status = 'sent';
                $receiver->save();
            }
        }
    }
}
