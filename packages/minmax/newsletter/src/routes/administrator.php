<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['administrator', 'localizationRedirect'],
    'as' => 'administrator.' . app()->getLocale() . '.'
], function() {

    Route::group(['prefix' => 'administrator', 'namespace' => 'Minmax\Newsletter\Administrator', 'middleware' => 'auth:administrator'], function () {
        /*
         |--------------------------------------------------------------------------
         | 需要登入的路由。
         |--------------------------------------------------------------------------
         */

        /*
         * EpaperHistory 電子報紀錄
         */
        Route::get('epaper-history', 'EpaperHistoryController@index')->name('epaper-history.index');
        Route::post('epaper-history', 'EpaperHistoryController@store')->name('epaper-history.store');
        Route::get('epaper-history/create', 'EpaperHistoryController@create')->name('epaper-history.create');
        Route::get('epaper-history/{id}', 'EpaperHistoryController@show')->name('epaper-history.show');
        Route::put('epaper-history/{id}', 'EpaperHistoryController@update')->name('epaper-history.update');
        Route::delete('epaper-history/{id}', 'EpaperHistoryController@destroy')->name('epaper-history.destroy');
        Route::get('epaper-history/{id}/edit', 'EpaperHistoryController@edit')->name('epaper-history.edit');
        Route::post('epaper-history/ajax/datatables', 'EpaperHistoryController@ajaxDataTable')->name('epaper-history.ajaxDataTable');
        Route::get('epaper-history/ajax/template', 'EpaperHistoryController@ajaxTemplate')->name('epaper-history.ajaxTemplate');

        /*
         * EpaperReceiver 電子報收件者
         */
        Route::post('epaper-receiver/ajax/datatables', 'EpaperReceiverController@ajaxDataTable')->name('epaper-receiver.ajaxDataTable');

        /*
         * EpaperTemplate 電子報範本
         */
        Route::get('epaper-template', 'EpaperTemplateController@index')->name('epaper-template.index');
        Route::post('epaper-template', 'EpaperTemplateController@store')->name('epaper-template.store');
        Route::get('epaper-template/create', 'EpaperTemplateController@create')->name('epaper-template.create');
        Route::get('epaper-template/{id}', 'EpaperTemplateController@show')->name('epaper-template.show');
        Route::put('epaper-template/{id}', 'EpaperTemplateController@update')->name('epaper-template.update');
        Route::delete('epaper-template/{id}', 'EpaperTemplateController@destroy')->name('epaper-template.destroy');
        Route::get('epaper-template/{id}/edit', 'EpaperTemplateController@edit')->name('epaper-template.edit');
        Route::post('epaper-template/ajax/datatables', 'EpaperTemplateController@ajaxDataTable')->name('epaper-template.ajaxDataTable');

        /*
         * EpaperSubscriber 電子報訂閱名單
         */
        Route::get('epaper-subscriber', 'EpaperSubscriberController@index')->name('epaper-subscriber.index');
        Route::post('epaper-subscriber', 'EpaperSubscriberController@store')->name('epaper-subscriber.store');
        Route::get('epaper-subscriber/create', 'EpaperSubscriberController@create')->name('epaper-subscriber.create');
        Route::get('epaper-subscriber/{id}', 'EpaperSubscriberController@show')->name('epaper-subscriber.show');
        Route::put('epaper-subscriber/{id}', 'EpaperSubscriberController@update')->name('epaper-subscriber.update');
        Route::delete('epaper-subscriber/{id}', 'EpaperSubscriberController@destroy')->name('epaper-subscriber.destroy');
        Route::get('epaper-subscriber/{id}/edit', 'EpaperSubscriberController@edit')->name('epaper-subscriber.edit');
        Route::post('epaper-subscriber/ajax/datatables', 'EpaperSubscriberController@ajaxDataTable')->name('epaper-subscriber.ajaxDataTable');
        Route::patch('epaper-subscriber/ajax/switch', 'EpaperSubscriberController@ajaxSwitch')->name('epaper-subscriber.ajaxSwitch');
        Route::patch('epaper-subscriber/ajax/sort', 'EpaperSubscriberController@ajaxSort')->name('epaper-subscriber.ajaxSort');
        Route::patch('epaper-subscriber/ajax/multi/switch', 'EpaperSubscriberController@ajaxMultiSwitch')->name('epaper-subscriber.ajaxMultiSwitch');
        Route::delete('epaper-subscriber/ajax/multi/delete', 'EpaperSubscriberController@ajaxMultiDestroy')->name('epaper-subscriber.ajaxMultiDestroy');

        /*
         * EpaperCategory 電子報類別
         */
        Route::get('epaper-category', 'EpaperCategoryController@index')->name('epaper-category.index');
        Route::post('epaper-category', 'EpaperCategoryController@store')->name('epaper-category.store');
        Route::get('epaper-category/create', 'EpaperCategoryController@create')->name('epaper-category.create');
        Route::get('epaper-category/{id}', 'EpaperCategoryController@show')->name('epaper-category.show');
        Route::put('epaper-category/{id}', 'EpaperCategoryController@update')->name('epaper-category.update');
        Route::delete('epaper-category/{id}', 'EpaperCategoryController@destroy')->name('epaper-category.destroy');
        Route::get('epaper-category/{id}/edit', 'EpaperCategoryController@edit')->name('epaper-category.edit');
        Route::post('epaper-category/ajax/datatables', 'EpaperCategoryController@ajaxDataTable')->name('epaper-category.ajaxDataTable');
        Route::patch('epaper-category/ajax/switch', 'EpaperCategoryController@ajaxSwitch')->name('epaper-category.ajaxSwitch');
        Route::patch('epaper-category/ajax/sort', 'EpaperCategoryController@ajaxSort')->name('epaper-category.ajaxSort');

        /*
         * SmsHistory 簡訊紀錄
         */
        Route::get('sms-history', 'SmsHistoryController@index')->name('sms-history.index');
        Route::post('sms-history', 'SmsHistoryController@store')->name('sms-history.store');
        Route::get('sms-history/create', 'SmsHistoryController@create')->name('sms-history.create');
        Route::get('sms-history/{id}', 'SmsHistoryController@show')->name('sms-history.show');
        Route::put('sms-history/{id}', 'SmsHistoryController@update')->name('sms-history.update');
        Route::delete('sms-history/{id}', 'SmsHistoryController@destroy')->name('sms-history.destroy');
        Route::get('sms-history/{id}/edit', 'SmsHistoryController@edit')->name('sms-history.edit');
        Route::post('sms-history/ajax/datatables', 'SmsHistoryController@ajaxDataTable')->name('sms-history.ajaxDataTable');
        Route::get('sms-history/ajax/template', 'SmsHistoryController@ajaxTemplate')->name('sms-history.ajaxTemplate');

        /*
         * SmsReceiver 簡訊收件者
         */
        Route::post('sms-receiver/ajax/datatables', 'SmsReceiverController@ajaxDataTable')->name('sms-receiver.ajaxDataTable');

        /*
         * SmsTemplate 簡訊範本
         */
        Route::get('sms-template', 'SmsTemplateController@index')->name('sms-template.index');
        Route::post('sms-template', 'SmsTemplateController@store')->name('sms-template.store');
        Route::get('sms-template/create', 'SmsTemplateController@create')->name('sms-template.create');
        Route::get('sms-template/{id}', 'SmsTemplateController@show')->name('sms-template.show');
        Route::put('sms-template/{id}', 'SmsTemplateController@update')->name('sms-template.update');
        Route::delete('sms-template/{id}', 'SmsTemplateController@destroy')->name('sms-template.destroy');
        Route::get('sms-template/{id}/edit', 'SmsTemplateController@edit')->name('sms-template.edit');
        Route::post('sms-template/ajax/datatables', 'SmsTemplateController@ajaxDataTable')->name('sms-template.ajaxDataTable');

        /*
         * SmsSubscriber 簡訊訂閱名單
         */
        Route::get('sms-subscriber', 'SmsSubscriberController@index')->name('sms-subscriber.index');
        Route::post('sms-subscriber', 'SmsSubscriberController@store')->name('sms-subscriber.store');
        Route::get('sms-subscriber/create', 'SmsSubscriberController@create')->name('sms-subscriber.create');
        Route::get('sms-subscriber/{id}', 'SmsSubscriberController@show')->name('sms-subscriber.show');
        Route::put('sms-subscriber/{id}', 'SmsSubscriberController@update')->name('sms-subscriber.update');
        Route::delete('sms-subscriber/{id}', 'SmsSubscriberController@destroy')->name('sms-subscriber.destroy');
        Route::get('sms-subscriber/{id}/edit', 'SmsSubscriberController@edit')->name('sms-subscriber.edit');
        Route::post('sms-subscriber/ajax/datatables', 'SmsSubscriberController@ajaxDataTable')->name('sms-subscriber.ajaxDataTable');
        Route::patch('sms-subscriber/ajax/switch', 'SmsSubscriberController@ajaxSwitch')->name('sms-subscriber.ajaxSwitch');
        Route::patch('sms-subscriber/ajax/sort', 'SmsSubscriberController@ajaxSort')->name('sms-subscriber.ajaxSort');
        Route::patch('sms-subscriber/ajax/multi/switch', 'SmsSubscriberController@ajaxMultiSwitch')->name('sms-subscriber.ajaxMultiSwitch');
        Route::delete('sms-subscriber/ajax/multi/delete', 'SmsSubscriberController@ajaxMultiDestroy')->name('sms-subscriber.ajaxMultiDestroy');

        /*
         * SmsCategory 簡訊類別
         */
        Route::get('sms-category', 'SmsCategoryController@index')->name('sms-category.index');
        Route::post('sms-category', 'SmsCategoryController@store')->name('sms-category.store');
        Route::get('sms-category/create', 'SmsCategoryController@create')->name('sms-category.create');
        Route::get('sms-category/{id}', 'SmsCategoryController@show')->name('sms-category.show');
        Route::put('sms-category/{id}', 'SmsCategoryController@update')->name('sms-category.update');
        Route::delete('sms-category/{id}', 'SmsCategoryController@destroy')->name('sms-category.destroy');
        Route::get('sms-category/{id}/edit', 'SmsCategoryController@edit')->name('sms-category.edit');
        Route::post('sms-category/ajax/datatables', 'SmsCategoryController@ajaxDataTable')->name('sms-category.ajaxDataTable');
        Route::patch('sms-category/ajax/switch', 'SmsCategoryController@ajaxSwitch')->name('sms-category.ajaxSwitch');
        Route::patch('sms-category/ajax/sort', 'SmsCategoryController@ajaxSort')->name('sms-category.ajaxSort');

    });

});
