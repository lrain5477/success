<?php

namespace Minmax\Newsletter\Mails;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Minmax\Newsletter\Models\EpaperHistory;
use Minmax\Newsletter\Models\EpaperReceiver;

class NewsletterEpaper extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var EpaperHistory $history
     */
    protected $history;

    /**
     * @var EpaperReceiver $receiver
     */
    protected $receiver;

    /**
     * Create a new message instance.
     *
     * @param  EpaperHistory $history
     * @param  EpaperReceiver $receiver
     * @return void
     */
    public function __construct(EpaperHistory $history, EpaperReceiver $receiver)
    {
        $this->history = $history;
        $this->receiver = $receiver;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // E-Paper Email Config
        customMailer('epaper', false, true);

        return $this
            ->subject($this->history->subject)
            ->view('MinmaxNewsletter::email.layouts.default')
            ->with([
                'newsletter' => $this->history,
                'receiver' => $this->receiver,
            ]);
    }

    /**
     * Process if queue job failed
     *
     * @param  Exception $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $this->receiver->epaper_status = 'failed';
        $this->receiver->save();
    }
}
