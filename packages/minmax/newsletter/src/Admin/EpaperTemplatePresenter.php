<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Presenter;

/**
 * Class EpaperTemplatePresenter
 */
class EpaperTemplatePresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'editable' => systemParam('editable'),
        ];
    }
}
