<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Newsletter\Models\SmsTemplate;

/**
 * Class SmsTemplateTransformer
 */
class SmsTemplateTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'smsTemplateShow',
        'U' => 'smsTemplateEdit',
        'D' => 'smsTemplateDestroy',
    ];

    /**
     * Transformer constructor.
     * @param  SmsTemplatePresenter $presenter
     * @param  string $uri
     */
    public function __construct(SmsTemplatePresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  SmsTemplate $model
     * @return array
     * @throws \Throwable
     */
    public function transform(SmsTemplate $model)
    {
        if ($model->editable) {
            $this->presenter->setPermissions($this->permissions);
        } else {
            $this->presenter->setPermissions($this->permissions, ['U', 'D']);
        }

        return [
            'title' => $this->presenter->getGridText($model, 'title'),
            'updated_at' => $this->presenter->getGridText($model, 'updated_at'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
