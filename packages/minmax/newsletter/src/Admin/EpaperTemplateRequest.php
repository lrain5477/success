<?php

namespace Minmax\Newsletter\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class EpaperTemplateRequest
 */
class EpaperTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('epaperTemplateEdit');
            case 'POST':
                return $this->user('admin')->can('epaperTemplateCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'EpaperTemplate.title' => 'required|string',
                    'EpaperTemplate.subject' => 'required|string',
                    'EpaperTemplate.editor' => 'required|string',
                ];
            case 'POST':
            default:
                return [
                    'EpaperTemplate.title' => 'required|string',
                    'EpaperTemplate.subject' => 'required|string',
                    'EpaperTemplate.editor' => 'required|string',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'EpaperTemplate.title' => __('MinmaxNewsletter::models.EpaperTemplate.title'),
            'EpaperTemplate.subject' => __('MinmaxNewsletter::models.EpaperTemplate.subject'),
            'EpaperTemplate.editor' => __('MinmaxNewsletter::models.EpaperTemplate.editor'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
