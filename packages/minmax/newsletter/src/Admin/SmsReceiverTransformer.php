<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Newsletter\Models\SmsReceiver;

/**
 * Class SmsReceiverTransformer
 */
class SmsReceiverTransformer extends Transformer
{
    protected $permissions = [];

    /**
     * Transformer constructor.
     * @param  SmsReceiverPresenter $presenter
     * @param  string $uri
     */
    public function __construct(SmsReceiverPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  SmsReceiver $model
     * @return array
     * @throws \Throwable
     */
    public function transform(SmsReceiver $model)
    {
        return [
            'mobile' => $this->presenter->getGridText($model, 'mobile'),
            'name' => $this->presenter->getGridText($model, 'name'),
            'sms_status' => $this->presenter->getGridSelection($model, 'sms_status'),
            'opened_at' => $this->presenter->getGridText($model, 'opened_at'),
            'clicked_at' => $this->presenter->getGridText($model, 'clicked_at'),
        ];
    }
}
