<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Newsletter\Models\EpaperTemplate;

/**
 * Class EpaperTemplateTransformer
 */
class EpaperTemplateTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'epaperTemplateShow',
        'U' => 'epaperTemplateEdit',
        'D' => 'epaperTemplateDestroy',
    ];

    /**
     * Transformer constructor.
     * @param  EpaperTemplatePresenter $presenter
     * @param  string $uri
     */
    public function __construct(EpaperTemplatePresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  EpaperTemplate $model
     * @return array
     * @throws \Throwable
     */
    public function transform(EpaperTemplate $model)
    {
        if ($model->editable) {
            $this->presenter->setPermissions($this->permissions);
        } else {
            $this->presenter->setPermissions($this->permissions, ['U', 'D']);
        }

        return [
            'title' => $this->presenter->getGridText($model, 'title'),
            'updated_at' => $this->presenter->getGridText($model, 'updated_at'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
