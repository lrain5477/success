<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Presenter;

/**
 * Class SmsTemplatePresenter
 */
class SmsTemplatePresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'editable' => systemParam('editable'),
        ];
    }
}
