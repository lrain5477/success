<?php

namespace Minmax\Newsletter\Admin;

use Illuminate\Http\Request;
use Minmax\Base\Admin\Controller;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class SmsTemplateController
 */
class SmsTemplateController extends Controller
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct(SmsTemplateRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    public function edit($id)
    {
        $this->checkPermissionEdit();

        $this->viewData['formData'] = $this->modelRepository->one(['id' => $id, 'editable' => true]) ?? abort(404);

        $this->setCustomViewDataEdit();

        $this->buildBreadcrumbsEdit($id);

        try {
            return view($this->packagePrefix . 'admin.' . $this->uri . '.edit', $this->viewData);
        } catch(\Exception $e) {
            return abort(404);
        }
    }

    public function update(Request $request, $id)
    {
        $this->checkPermissionEdit();

        $this->checkValidate();

        $model = $this->modelRepository->one(['id' => $id, 'editable' => true]) ?? abort(404);

        $inputSet = $request->input($this->pageData->getAttribute('model'));

        $inputSet = $this->doFileUpload($inputSet, $request);

        // 儲存更新資料
        if ($this->modelRepository->save($model, $inputSet)) {
            LogHelper::system('admin', $request->path(), $request->method(), $id, $this->adminData->username, 1, __('MinmaxBase::admin.form.message.edit_success'));
            return redirect(langRoute("admin.{$this->uri}.edit", [$id]))->with('success', __('MinmaxBase::admin.form.message.edit_success'));
        }

        LogHelper::system('admin', $request->path(), $request->method(), $id, $this->adminData->username, 0, __('MinmaxBase::admin.form.message.edit_error'));
        return redirect(langRoute("admin.{$this->uri}.edit", [$id]))->withErrors([__('MinmaxBase::admin.form.message.edit_error')])->withInput();
    }
}
