<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Presenter;
use Minmax\Newsletter\Models\SmsHistory;
use Minmax\Newsletter\Models\SmsSubscriber;

/**
 * Class SmsCategoryPresenter
 */
class SmsCategoryPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    protected $languageColumns = ['title', 'details'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'active' => systemParam('active'),
        ];
    }

    /**
     * @param  \Minmax\Newsletter\Models\SmsCategory $model
     * @return string
     */
    public function getGridHistoryCount($model)
    {
        $amount = SmsHistory::query()->whereRaw("json_contains(objects, '\"{$model->id}\"', '$.categories')")->count();

        $url = langRoute('admin.sms-history.index', ['category' => $model->id]);

        $gridHtml = <<<HTML
<a class="text-center d-block" href="{$url}">{$amount}</a>
HTML;

        return $gridHtml;
    }

    /**
     * @param  \Minmax\Newsletter\Models\SmsCategory $model
     * @return string
     */
    public function getGridSubscriberCount($model)
    {
        $amount = SmsSubscriber::query()->whereRaw("json_contains(categories, '\"{$model->id}\"')")->count();

        $url = langRoute('admin.sms-subscriber.index', ['category' => $model->id]);

        $gridHtml = <<<HTML
<a class="text-center d-block" href="{$url}">{$amount}</a>
HTML;

        return $gridHtml;
    }
}
