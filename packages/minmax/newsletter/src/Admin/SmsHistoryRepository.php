<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Newsletter\Models\SmsHistory;

/**
 * Class SmsHistoryRepository
 * @property SmsHistory $model
 * @method SmsHistory find($id)
 * @method SmsHistory one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method SmsHistory[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method SmsHistory create($attributes)
 * @method SmsHistory save($model, $attributes)
 * @method SmsHistory|\Illuminate\Database\Eloquent\Builder query()
 */
class SmsHistoryRepository extends Repository
{
    const MODEL = SmsHistory::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'sms_history';
    }

    protected function beforeCreate()
    {
        $this->attributes['objects']['languages'] = array_get($this->attributes, 'objects.languages') ?? [];
        $this->attributes['objects']['categories'] = array_get($this->attributes, 'objects.categories') ?? [];
        $this->attributes['objects']['roles'] = array_get($this->attributes, 'objects.roles') ?? [];
        $this->attributes['objects']['members'] = array_get($this->attributes, 'objects.members');
        $this->attributes['objects']['mobiles'] = array_get($this->attributes, 'objects.mobiles');

        $this->attributes['schedule_at'] = array_get($this->attributes, 'schedule_at') ?? date('Y-m-d H:i:s');
    }

    protected function beforeSave()
    {
        $this->attributes['objects']['languages'] = array_get($this->attributes, 'objects.languages') ?? [];
        $this->attributes['objects']['categories'] = array_get($this->attributes, 'objects.categories') ?? [];
        $this->attributes['objects']['roles'] = array_get($this->attributes, 'objects.roles') ?? [];
        $this->attributes['objects']['members'] = array_get($this->attributes, 'objects.members');
        $this->attributes['objects']['mobiles'] = array_get($this->attributes, 'objects.mobiles');

        $this->attributes['schedule_at'] = array_get($this->attributes, 'schedule_at') ?? date('Y-m-d H:i:s');
    }
}
