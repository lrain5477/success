<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Newsletter\Models\SmsCategory;

/**
 * Class SmsCategoryRepository
 * @property SmsCategory $model
 * @method SmsCategory find($id)
 * @method SmsCategory one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method SmsCategory[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method SmsCategory create($attributes)
 * @method SmsCategory save($model, $attributes)
 * @method SmsCategory|\Illuminate\Database\Eloquent\Builder query()
 */
class SmsCategoryRepository extends Repository
{
    const MODEL = SmsCategory::class;

    protected $sort = 'sort';

    protected $sorting = true;

    protected $languageColumns = ['title', 'details'];

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'sms_category';
    }

    public function getSelectParameters()
    {
        return $this->all()
            ->mapWithKeys(function ($item) {
                /** @var SmsCategory $item */
                return [$item->id => ['title' => $item->title, 'options' => []]];
            })
            ->toArray();
    }
}
