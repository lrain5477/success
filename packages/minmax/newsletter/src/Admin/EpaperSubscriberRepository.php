<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Member\Admin\MemberRepository;
use Minmax\Newsletter\Models\EpaperSubscriber;

/**
 * Class EpaperSubscriberRepository
 * @property EpaperSubscriber $model
 * @method EpaperSubscriber find($id)
 * @method EpaperSubscriber one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method EpaperSubscriber[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method EpaperSubscriber create($attributes)
 * @method EpaperSubscriber save($model, $attributes)
 * @method EpaperSubscriber|\Illuminate\Database\Eloquent\Builder query()
 */
class EpaperSubscriberRepository extends Repository
{
    const MODEL = EpaperSubscriber::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'epaper_subscriber';
    }

    protected function beforeCreate()
    {
        if ($memberUsername = array_get($this->attributes, 'member_id')) {
            if ($member = (new MemberRepository)->one('username', $memberUsername)) {
                array_set($this->attributes, 'member_id', $member->id);
            } else {
                array_set($this->attributes, 'member_id', null);
            }
        }
    }

    protected function beforeSave()
    {
        if ($memberUsername = array_get($this->attributes, 'member_id')) {
            if ($member = (new MemberRepository)->one('username', $memberUsername)) {
                array_set($this->attributes, 'member_id', $member->id);
            } else {
                array_set($this->attributes, 'member_id', null);
            }
        }
    }
}
