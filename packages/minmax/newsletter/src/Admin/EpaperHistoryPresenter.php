<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Presenter;
use Minmax\Base\Admin\RoleRepository;
use Minmax\Base\Admin\WorldLanguageRepository;

/**
 * Class EpaperHistoryPresenter
 */
class EpaperHistoryPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'objects' => [
                'languages' => (new WorldLanguageRepository)->getSelectParameters(true),
                'categories' => (new EpaperCategoryRepository)->getSelectParameters(),
                'roles' => (new RoleRepository)->getSelectParameters('web'),
            ],
        ];
    }

    /**
     * @param  \Minmax\Newsletter\Models\EpaperHistory $model
     * @return string
     */
    public function getGridResult($model)
    {
        $amountLabel = __('MinmaxNewsletter::admin.grid.EpaperHistory.amount');
        $processRateLabel = __('MinmaxNewsletter::admin.grid.EpaperHistory.processRate');

        $amount = $model->epaper_receivers_count;

        if ($model->epaper_receivers_count < 1 || $model->sent_count < 1) {
            $processRate = 0;
        } else {
            $processRate = number_format($model->sent_count / $model->epaper_receivers_count * 100, 1);
        }

        $gridHtml = <<<HTML
<div class="text-nowrap small">{$amountLabel}：<span class="text-danger">{$amount}</span></div>
<div class="text-nowrap small">{$processRateLabel}：<span class="text-danger">{$processRate}</span> %</div>
HTML;

        return $gridHtml;
    }

    /**
     * @param  \Minmax\Newsletter\Models\EpaperHistory $model
     * @return string
     */
    public function getGridTrack($model)
    {
        $openedLabel = __('MinmaxNewsletter::admin.grid.EpaperHistory.opened');
        $openedRateLabel = __('MinmaxNewsletter::admin.grid.EpaperHistory.openedRate');
        $clickedRateLabel = __('MinmaxNewsletter::admin.grid.EpaperHistory.clickedRate');

        $openedAmount = $model->opened_count;

        if ($model->epaper_receivers_count < 1 || $model->opened_count < 1) {
            $openedRateAmount = 0;
        } else {
            $openedRateAmount = number_format($model->opened_count / $model->epaper_receivers_count * 100, 1);
        }

        if ($model->epaper_receivers_count < 1 || $model->clicked_count < 1) {
            $clickedRateAmount = 0;
        } else {
            $clickedRateAmount = number_format($model->clicked_count / $model->epaper_receivers_count * 100, 1);
        }

        $gridHtml = <<<HTML
<div class="text-nowrap small">{$openedLabel}：<span class="text-danger">{$openedAmount}</span></div>
<div class="text-nowrap small">{$openedRateLabel}：<span class="text-danger">{$openedRateAmount}</span> %</div>
<div class="text-nowrap small">{$clickedRateLabel}：<span class="text-danger">{$clickedRateAmount}</span> %</div>
HTML;

        return $gridHtml;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFieldTemplates()
    {
        $templates = (new EpaperTemplateRepository)->getSelectParameters();

        $componentData = [
            'id' => 'EpaperHistory-templates',
            'listData' => $templates,
        ];

        return view('MinmaxNewsletter::admin.epaper-history.form-templates-select', $componentData);
    }
}
