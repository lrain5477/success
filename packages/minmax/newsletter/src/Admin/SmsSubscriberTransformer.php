<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Newsletter\Models\SmsSubscriber;

/**
 * Class SmsSubscriberTransformer
 */
class SmsSubscriberTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'smsSubscriberShow',
        'U' => 'smsSubscriberEdit',
        'D' => 'smsSubscriberDestroy',
    ];

    /**
     * Transformer constructor.
     * @param  SmsSubscriberPresenter $presenter
     * @param  string $uri
     */
    public function __construct(SmsSubscriberPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  SmsSubscriber $model
     * @return array
     * @throws \Throwable
     */
    public function transform(SmsSubscriber $model)
    {
        return [
            'id' => $this->presenter->getGridCheckBox($model),
            'mobile' => $this->presenter->getGridText($model, 'mobile'),
            'member_id' => $this->presenter->getPureString(is_null($model->member_id) ? '-' : $model->member->username),
            'created_at' => $this->presenter->getPureString($model->created_at->format('Y-m-d')),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
