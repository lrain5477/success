<?php

namespace Minmax\Newsletter\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class SmsHistoryRequest
 */
class SmsHistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('smsHistoryEdit');
            case 'POST':
                return $this->user('admin')->can('smsHistoryCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'SmsHistory.subject' => 'required|string',
                    'SmsHistory.editor' => 'required|string',
                    'SmsHistory.objects.languages' => 'nullable|array',
                    'SmsHistory.objects.categories' => 'nullable|array',
                    'SmsHistory.objects.roles' => 'nullable|array',
                    'SmsHistory.objects.members' => 'nullable|string',
                    'SmsHistory.objects.mobiles' => 'nullable|string',
                    'SmsHistory.schedule_at' => 'nullable|string',
                ];
            case 'POST':
            default:
                return [
                    'SmsHistory.subject' => 'required|string',
                    'SmsHistory.editor' => 'required|string',
                    'SmsHistory.objects.languages' => 'nullable|array',
                    'SmsHistory.objects.categories' => 'nullable|array',
                    'SmsHistory.objects.roles' => 'nullable|array',
                    'SmsHistory.objects.members' => 'nullable|string',
                    'SmsHistory.objects.mobiles' => 'nullable|string',
                    'SmsHistory.schedule_at' => 'nullable|string',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'SmsHistory.subject' => __('MinmaxNewsletter::models.SmsHistory.subject'),
            'SmsHistory.editor' => __('MinmaxNewsletter::models.SmsHistory.editor'),
            'SmsHistory.objects.languages' => __('MinmaxNewsletter::models.SmsHistory.objects.languages'),
            'SmsHistory.objects.categories' => __('MinmaxNewsletter::models.SmsHistory.objects.categories'),
            'SmsHistory.objects.roles' => __('MinmaxNewsletter::models.SmsHistory.objects.roles'),
            'SmsHistory.objects.members' => __('MinmaxNewsletter::models.SmsHistory.objects.members'),
            'SmsHistory.objects.mobiles' => __('MinmaxNewsletter::models.SmsHistory.objects.mobiles'),
            'SmsHistory.schedule_at' => __('MinmaxNewsletter::models.SmsHistory.schedule_at'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
