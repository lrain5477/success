<?php

namespace Minmax\Newsletter\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class EpaperCategoryRequest
 */
class EpaperCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('epaperCategoryEdit');
            case 'POST':
                return $this->user('admin')->can('epaperCategoryCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'EpaperCategory.title' => 'required|string',
                    'EpaperCategory.details' => 'nullable|array',
                    'EpaperCategory.sort' => 'required|integer|min:1',
                    'EpaperCategory.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'EpaperCategory.title' => 'required|string',
                    'EpaperCategory.details' => 'nullable|array',
                    'EpaperCategory.sort' => 'nullable|integer|min:1',
                    'EpaperCategory.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'EpaperCategory.title' => __('MinmaxNewsletter::models.EpaperCategory.title'),
            'EpaperCategory.details' => __('MinmaxNewsletter::models.EpaperCategory.details'),
            'EpaperCategory.sort' => __('MinmaxNewsletter::models.EpaperCategory.sort'),
            'EpaperCategory.active' => __('MinmaxNewsletter::models.EpaperCategory.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
