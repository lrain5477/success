<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Newsletter\Models\SmsReceiver;

/**
 * Class SmsReceiverRepository
 * @property SmsReceiver $model
 * @method SmsReceiver find($id)
 * @method SmsReceiver one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method SmsReceiver[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method SmsReceiver create($attributes)
 * @method SmsReceiver save($model, $attributes)
 * @method SmsReceiver|\Illuminate\Database\Eloquent\Builder query()
 */
class SmsReceiverRepository extends Repository
{
    const MODEL = SmsReceiver::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'sms_receiver';
    }
}
