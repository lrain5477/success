<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Newsletter\Models\EpaperReceiver;

/**
 * Class EpaperReceiverRepository
 * @property EpaperReceiver $model
 * @method EpaperReceiver find($id)
 * @method EpaperReceiver one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method EpaperReceiver[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method EpaperReceiver create($attributes)
 * @method EpaperReceiver save($model, $attributes)
 * @method EpaperReceiver|\Illuminate\Database\Eloquent\Builder query()
 */
class EpaperReceiverRepository extends Repository
{
    const MODEL = EpaperReceiver::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'epaper_receiver';
    }
}
