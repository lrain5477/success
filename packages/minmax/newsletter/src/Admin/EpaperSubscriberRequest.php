<?php

namespace Minmax\Newsletter\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class EpaperSubscriberRequest
 */
class EpaperSubscriberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('epaperSubscriberEdit');
            case 'POST':
                return $this->user('admin')->can('epaperSubscriberCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'EpaperSubscriber.email' => 'required|email',
                    'EpaperSubscriber.member_id' => 'nullable|exists:member,username',
                    'EpaperSubscriber.language_id' => 'required|exists:world_language,id',
                    'EpaperSubscriber.categories' => 'required|array',
                    'EpaperSubscriber.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'EpaperSubscriber.email' => 'required|email',
                    'EpaperSubscriber.member_id' => 'nullable|exists:member,username',
                    'EpaperSubscriber.language_id' => 'required|exists:world_language,id',
                    'EpaperSubscriber.categories' => 'required|array',
                    'EpaperSubscriber.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'EpaperSubscriber.email' => __('MinmaxNewsletter::models.EpaperSubscriber.email'),
            'EpaperSubscriber.member_id' => __('MinmaxNewsletter::models.EpaperSubscriber.member_id'),
            'EpaperSubscriber.language_id' => __('MinmaxNewsletter::models.EpaperSubscriber.language_id'),
            'EpaperSubscriber.categories' => __('MinmaxNewsletter::models.EpaperSubscriber.categories'),
            'EpaperSubscriber.active' => __('MinmaxNewsletter::models.EpaperSubscriber.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
