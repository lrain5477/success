<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Newsletter\Models\EpaperCategory;

/**
 * Class EpaperCategoryTransformer
 */
class EpaperCategoryTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'epaperCategoryShow',
        'U' => 'epaperCategoryEdit',
        'D' => 'epaperCategoryDestroy',
    ];

    protected $subscriberAmount;

    /**
     * Transformer constructor.
     * @param  EpaperCategoryPresenter $presenter
     * @param  string $uri
     */
    public function __construct(EpaperCategoryPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  EpaperCategory $model
     * @return array
     * @throws \Throwable
     */
    public function transform(EpaperCategory $model)
    {
        return [
            'title' => $this->presenter->getGridText($model, 'title'),
            'history_count' => $this->presenter->getGridHistoryCount($model),
            'subscriber_count' => $this->presenter->getGridSubscriberCount($model),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
