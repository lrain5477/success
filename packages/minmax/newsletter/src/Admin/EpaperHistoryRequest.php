<?php

namespace Minmax\Newsletter\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class EpaperHistoryRequest
 */
class EpaperHistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('epaperHistoryEdit');
            case 'POST':
                return $this->user('admin')->can('epaperHistoryCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'EpaperHistory.subject' => 'required|string',
                    'EpaperHistory.editor' => 'required|string',
                    'EpaperHistory.objects.languages' => 'nullable|array',
                    'EpaperHistory.objects.categories' => 'nullable|array',
                    'EpaperHistory.objects.roles' => 'nullable|array',
                    'EpaperHistory.objects.members' => 'nullable|string',
                    'EpaperHistory.objects.emails' => 'nullable|string',
                    'EpaperHistory.schedule_at' => 'nullable|string',
                ];
            case 'POST':
            default:
                return [
                    'EpaperHistory.subject' => 'required|string',
                    'EpaperHistory.editor' => 'required|string',
                    'EpaperHistory.objects.languages' => 'nullable|array',
                    'EpaperHistory.objects.categories' => 'nullable|array',
                    'EpaperHistory.objects.roles' => 'nullable|array',
                    'EpaperHistory.objects.members' => 'nullable|string',
                    'EpaperHistory.objects.emails' => 'nullable|string',
                    'EpaperHistory.schedule_at' => 'nullable|string',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'EpaperHistory.subject' => __('MinmaxNewsletter::models.EpaperHistory.subject'),
            'EpaperHistory.editor' => __('MinmaxNewsletter::models.EpaperHistory.editor'),
            'EpaperHistory.objects.languages' => __('MinmaxNewsletter::models.EpaperHistory.objects.languages'),
            'EpaperHistory.objects.categories' => __('MinmaxNewsletter::models.EpaperHistory.objects.categories'),
            'EpaperHistory.objects.roles' => __('MinmaxNewsletter::models.EpaperHistory.objects.roles'),
            'EpaperHistory.objects.members' => __('MinmaxNewsletter::models.EpaperHistory.objects.members'),
            'EpaperHistory.objects.emails' => __('MinmaxNewsletter::models.EpaperHistory.objects.emails'),
            'EpaperHistory.schedule_at' => __('MinmaxNewsletter::models.EpaperHistory.schedule_at'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
