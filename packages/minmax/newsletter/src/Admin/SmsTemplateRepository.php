<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Newsletter\Models\SmsTemplate;

/**
 * Class SmsTemplateRepository
 * @property SmsTemplate $model
 * @method SmsTemplate find($id)
 * @method SmsTemplate one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method SmsTemplate[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method SmsTemplate create($attributes)
 * @method SmsTemplate save($model, $attributes)
 * @method SmsTemplate|\Illuminate\Database\Eloquent\Builder query()
 */
class SmsTemplateRepository extends Repository
{
    const MODEL = SmsTemplate::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'sms_template';
    }

    public function getSelectParameters()
    {
        return $this->all()
            ->sortBy('title')
            ->mapWithKeys(function ($item) {
                /** @var SmsTemplate $item */
                return [$item->id => ['title' => $item->title, 'options' => []]];
            })
            ->toArray();
    }
}
