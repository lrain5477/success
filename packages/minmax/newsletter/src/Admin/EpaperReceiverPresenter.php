<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Presenter;

/**
 * Class EpaperReceiverPresenter
 */
class EpaperReceiverPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'epaper_status' => systemParam('epaper_status'),
        ];
    }
}
