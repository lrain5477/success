<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Newsletter\Models\EpaperReceiver;

/**
 * Class EpaperReceiverTransformer
 */
class EpaperReceiverTransformer extends Transformer
{
    protected $permissions = [];

    /**
     * Transformer constructor.
     * @param  EpaperReceiverPresenter $presenter
     * @param  string $uri
     */
    public function __construct(EpaperReceiverPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  EpaperReceiver $model
     * @return array
     * @throws \Throwable
     */
    public function transform(EpaperReceiver $model)
    {
        return [
            'email' => $this->presenter->getGridText($model, 'email'),
            'name' => $this->presenter->getGridText($model, 'name'),
            'epaper_status' => $this->presenter->getGridSelection($model, 'epaper_status'),
            'opened_at' => $this->presenter->getGridText($model, 'opened_at'),
            'clicked_at' => $this->presenter->getGridText($model, 'clicked_at'),
        ];
    }
}
