<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Member\Admin\MemberRepository;
use Minmax\Newsletter\Models\SmsSubscriber;

/**
 * Class SmsSubscriberRepository
 * @property SmsSubscriber $model
 * @method SmsSubscriber find($id)
 * @method SmsSubscriber one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method SmsSubscriber[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method SmsSubscriber create($attributes)
 * @method SmsSubscriber save($model, $attributes)
 * @method SmsSubscriber|\Illuminate\Database\Eloquent\Builder query()
 */
class SmsSubscriberRepository extends Repository
{
    const MODEL = SmsSubscriber::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'sms_subscriber';
    }

    protected function beforeCreate()
    {
        if ($memberUsername = array_get($this->attributes, 'member_id')) {
            if ($member = (new MemberRepository)->one('username', $memberUsername)) {
                array_set($this->attributes, 'member_id', $member->id);
            } else {
                array_set($this->attributes, 'member_id', null);
            }
        }
    }

    protected function beforeSave()
    {
        if ($memberUsername = array_get($this->attributes, 'member_id')) {
            if ($member = (new MemberRepository)->one('username', $memberUsername)) {
                array_set($this->attributes, 'member_id', $member->id);
            } else {
                array_set($this->attributes, 'member_id', null);
            }
        }
    }
}
