<?php

namespace Minmax\Newsletter\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class SmsCategoryRequest
 */
class SmsCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('smsCategoryEdit');
            case 'POST':
                return $this->user('admin')->can('smsCategoryCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'SmsCategory.title' => 'required|string',
                    'SmsCategory.details' => 'nullable|array',
                    'SmsCategory.sort' => 'required|integer|min:1',
                    'SmsCategory.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'SmsCategory.title' => 'required|string',
                    'SmsCategory.details' => 'nullable|array',
                    'SmsCategory.sort' => 'nullable|integer|min:1',
                    'SmsCategory.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'SmsCategory.title' => __('MinmaxNewsletter::models.SmsCategory.title'),
            'SmsCategory.details' => __('MinmaxNewsletter::models.SmsCategory.details'),
            'SmsCategory.sort' => __('MinmaxNewsletter::models.SmsCategory.sort'),
            'SmsCategory.active' => __('MinmaxNewsletter::models.SmsCategory.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
