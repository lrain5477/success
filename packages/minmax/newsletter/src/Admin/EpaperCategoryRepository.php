<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Newsletter\Models\EpaperCategory;

/**
 * Class EpaperCategoryRepository
 * @property EpaperCategory $model
 * @method EpaperCategory find($id)
 * @method EpaperCategory one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method EpaperCategory[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method EpaperCategory create($attributes)
 * @method EpaperCategory save($model, $attributes)
 * @method EpaperCategory|\Illuminate\Database\Eloquent\Builder query()
 */
class EpaperCategoryRepository extends Repository
{
    const MODEL = EpaperCategory::class;

    protected $sort = 'sort';

    protected $sorting = true;

    protected $languageColumns = ['title', 'details'];

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'epaper_category';
    }

    public function getSelectParameters()
    {
        return $this->all()
            ->mapWithKeys(function ($item) {
                /** @var EpaperCategory $item */
                return [$item->id => ['title' => $item->title, 'options' => []]];
            })
            ->toArray();
    }
}
