<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Newsletter\Models\EpaperTemplate;

/**
 * Class EpaperTemplateRepository
 * @property EpaperTemplate $model
 * @method EpaperTemplate find($id)
 * @method EpaperTemplate one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method EpaperTemplate[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method EpaperTemplate create($attributes)
 * @method EpaperTemplate save($model, $attributes)
 * @method EpaperTemplate|\Illuminate\Database\Eloquent\Builder query()
 */
class EpaperTemplateRepository extends Repository
{
    const MODEL = EpaperTemplate::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'epaper_template';
    }

    public function getSelectParameters()
    {
        return $this->all()
            ->sortBy('title')
            ->mapWithKeys(function ($item) {
                /** @var EpaperTemplate $item */
                return [$item->id => ['title' => $item->title, 'options' => []]];
            })
            ->toArray();
    }
}
