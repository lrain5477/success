<?php

namespace Minmax\Newsletter\Admin;

use Minmax\Base\Admin\Presenter;

/**
 * Class SmsReceiverPresenter
 */
class SmsReceiverPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'sms_status' => systemParam('sms_status'),
        ];
    }
}
