<?php

namespace Minmax\Newsletter\Admin;

use Illuminate\Http\Request;
use Minmax\Base\Admin\Controller;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class EpaperReceiverController
 */
class EpaperReceiverController extends Controller
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct(EpaperReceiverRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    protected function checkPermissionShow($type = 'web')
    {
        switch ($type) {
            case 'ajax':
                $statusCode = 401; break;
            default:
                $statusCode = 500; break;
        }
        if($this->adminData->can('epaperHistoryShow') === false) abort($statusCode);
    }

    /**
     * Upload files and return new input set.
     *
     * @param  mixed $datatable
     * @return mixed
     */
    protected function setDatatableTransformer($datatable)
    {
        $datatable->setTransformer(app(EpaperReceiverTransformer::class, ['uri' => $this->uri]));

        return $datatable;
    }

    /**
     * Grid data return for DataTables
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function ajaxDataTable(Request $request)
    {
        $this->checkPermissionShow('ajax');

        $queryBuilder = $this->getQueryBuilder();

        $datatable = DataTables::of($queryBuilder);

        $datatable = $this->doDatatableFilter($datatable, $request);

        $datatable = $this->setDatatableTransformer($datatable);

        return $datatable->make(true);
    }
}
