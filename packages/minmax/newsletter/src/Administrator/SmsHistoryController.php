<?php

namespace Minmax\Newsletter\Administrator;

use Illuminate\Http\Request;
use Minmax\Base\Administrator\Controller;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class SmsHistoryController
 */
class SmsHistoryController extends Controller
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct(SmsHistoryRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getQueryBuilder()
    {
        return $this->modelRepository->query()->withCount([
            'smsReceivers',
            'smsReceivers as sent_count' => function ($query) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->where('sms_status', '!=', 'pending');
            },
            'smsReceivers as opened_count' => function ($query) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->whereNotNull('opened_at');
            },
            'smsReceivers as clicked_count' => function ($query) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->whereNotNull('clicked_at');
            },
        ]);
    }

    public function store(Request $request)
    {
        $this->checkValidate();

        $inputSet = $request->input($this->pageData->getAttribute('model'));

        $inputSet = $this->doFileUpload($inputSet, $request);

        // 儲存新建資料
        if ($modelData = $this->modelRepository->create($inputSet)) {
            LogHelper::system('admin', $request->path(), $request->method(), $modelData->getKey(), $this->adminData->username, 1, __('MinmaxBase::admin.form.message.create_success'));
            return redirect(langRoute("admin.{$this->uri}.index"))->with('success', __('MinmaxBase::admin.form.message.create_success'));
        }

        LogHelper::system('admin', $request->path(), $request->method(), '', $this->adminData->username, 0, __('MinmaxBase::admin.form.message.create_error'));
        return redirect(langRoute("admin.{$this->uri}.create"))->withErrors([__('MinmaxBase::admin.form.message.create_error')])->withInput();
    }

    public function ajaxTemplate(Request $request)
    {
        if ($template = (new SmsTemplateRepository)->find($request->input('id'))) {
            return response(['subject' => $template->subject, 'content' => $template->content], 200, ['Content-Type' => 'application/json']);
        }

        return response(['msg' => 'error'], 400, ['Content-Type' => 'application/json']);
    }
}
