<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Presenter;

/**
 * Class SmsReceiverPresenter
 */
class SmsReceiverPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'sms_status' => systemParam('sms_status'),
        ];
    }
}
