<?php

namespace Minmax\Newsletter\Administrator;

use Illuminate\Http\Request;
use Minmax\Base\Administrator\Controller;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class EpaperReceiverController
 */
class EpaperReceiverController extends Controller
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct(EpaperReceiverRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    /**
     * Upload files and return new input set.
     *
     * @param  mixed $datatable
     * @return mixed
     */
    protected function setDatatableTransformer($datatable)
    {
        $datatable->setTransformer(app(EpaperReceiverTransformer::class, ['uri' => $this->uri]));

        return $datatable;
    }

    /**
     * Grid data return for DataTables
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function ajaxDataTable(Request $request)
    {
        $queryBuilder = $this->getQueryBuilder();

        $datatable = DataTables::of($queryBuilder);

        $datatable = $this->doDatatableFilter($datatable, $request);

        $datatable = $this->setDatatableTransformer($datatable);

        return $datatable->make(true);
    }
}
