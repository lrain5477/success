<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Repository;
use Minmax\Newsletter\Models\EpaperHistory;

/**
 * Class EpaperHistoryRepository
 * @property EpaperHistory $model
 * @method EpaperHistory find($id)
 * @method EpaperHistory one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method EpaperHistory[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method EpaperHistory create($attributes)
 * @method EpaperHistory save($model, $attributes)
 * @method EpaperHistory|\Illuminate\Database\Eloquent\Builder query()
 */
class EpaperHistoryRepository extends Repository
{
    const MODEL = EpaperHistory::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'epaper_history';
    }

    protected function beforeCreate()
    {
        $this->attributes['objects']['languages'] = array_get($this->attributes, 'objects.languages') ?? [];
        $this->attributes['objects']['categories'] = array_get($this->attributes, 'objects.categories') ?? [];
        $this->attributes['objects']['roles'] = array_get($this->attributes, 'objects.roles') ?? [];
        $this->attributes['objects']['members'] = array_get($this->attributes, 'objects.members');
        $this->attributes['objects']['emails'] = array_get($this->attributes, 'objects.emails');

        $this->attributes['schedule_at'] = array_get($this->attributes, 'schedule_at') ?? date('Y-m-d H:i:s');
    }

    protected function beforeSave()
    {
        $this->attributes['objects']['languages'] = array_get($this->attributes, 'objects.languages') ?? [];
        $this->attributes['objects']['categories'] = array_get($this->attributes, 'objects.categories') ?? [];
        $this->attributes['objects']['roles'] = array_get($this->attributes, 'objects.roles') ?? [];
        $this->attributes['objects']['members'] = array_get($this->attributes, 'objects.members');
        $this->attributes['objects']['emails'] = array_get($this->attributes, 'objects.emails');

        $this->attributes['schedule_at'] = array_get($this->attributes, 'schedule_at') ?? date('Y-m-d H:i:s');
    }
}
