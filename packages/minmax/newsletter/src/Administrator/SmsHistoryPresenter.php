<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Presenter;
use Minmax\Base\Administrator\RoleRepository;
use Minmax\Base\Administrator\WorldLanguageRepository;

/**
 * Class SmsHistoryPresenter
 */
class SmsHistoryPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'objects' => [
                'languages' => (new WorldLanguageRepository)->getSelectParameters(true),
                'categories' => (new SmsCategoryRepository)->getSelectParameters(),
                'roles' => (new RoleRepository)->getSelectParameters('web'),
            ],
        ];
    }

    /**
     * @param  \Minmax\Newsletter\Models\SmsHistory $model
     * @return string
     */
    public function getGridResult($model)
    {
        $amountLabel = __('MinmaxNewsletter::administrator.grid.SmsHistory.amount');
        $processRateLabel = __('MinmaxNewsletter::administrator.grid.SmsHistory.processRate');

        $amount = $model->sms_receivers_count;

        if ($model->sms_receivers_count < 1 || $model->sent_count < 1) {
            $processRate = 0;
        } else {
            $processRate = number_format($model->sent_count / $model->sms_receivers_count * 100, 1);
        }

        $gridHtml = <<<HTML
<div class="text-nowrap small">{$amountLabel}：<span class="text-danger">{$amount}</span></div>
<div class="text-nowrap small">{$processRateLabel}：<span class="text-danger">{$processRate}</span> %</div>
HTML;

        return $gridHtml;
    }

    /**
     * @param  \Minmax\Newsletter\Models\SmsHistory $model
     * @return string
     */
    public function getGridTrack($model)
    {
        $openedLabel = __('MinmaxNewsletter::administrator.grid.SmsHistory.opened');
        $openedRateLabel = __('MinmaxNewsletter::administrator.grid.SmsHistory.openedRate');
        $clickedRateLabel = __('MinmaxNewsletter::administrator.grid.SmsHistory.clickedRate');

        $openedAmount = $model->opened_count;

        if ($model->sms_receivers_count < 1 || $model->opened_count < 1) {
            $openedRateAmount = 0;
        } else {
            $openedRateAmount = number_format($model->opened_count / $model->sms_receivers_count * 100, 1);
        }

        if ($model->sms_receivers_count < 1 || $model->clicked_count < 1) {
            $clickedRateAmount = 0;
        } else {
            $clickedRateAmount = number_format($model->clicked_count / $model->sms_receivers_count * 100, 1);
        }

        $gridHtml = <<<HTML
<div class="text-nowrap small">{$openedLabel}：<span class="text-danger">{$openedAmount}</span></div>
<div class="text-nowrap small">{$openedRateLabel}：<span class="text-danger">{$openedRateAmount}</span> %</div>
<div class="text-nowrap small">{$clickedRateLabel}：<span class="text-danger">{$clickedRateAmount}</span> %</div>
HTML;

        return $gridHtml;
    }

    /**
     * @param  \Minmax\Newsletter\Models\SmsHistory $model
     * @param  array $additional will format as ['view' => 'xxx', 'uri' => '???']
     * @return string
     */
    public function getGridActions($model, $additional = [])
    {
        $id = $model->getKey();

        $result = '';

        try {
            $result .= view('MinmaxBase::administrator.layouts.grid.action-button-show', ['id' => $id, 'uri' => $this->uri])->render();

            if (is_null($model->sent_at)) {
                $result .= view('MinmaxBase::administrator.layouts.grid.action-button-edit', ['id' => $id, 'uri' => $this->uri])->render();
            }

            $result .= view('MinmaxBase::administrator.layouts.grid.action-button-destroy', ['id' => $id, 'uri' => $this->uri])->render();
        } catch (\Exception $e) {
            return '';
        } catch (\Throwable $e) {
            $result = '';
        }

        return $result;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFieldTemplates()
    {
        $templates = (new SmsTemplateRepository)->getSelectParameters();

        $componentData = [
            'id' => 'SmsHistory-templates',
            'listData' => $templates,
        ];

        return view('MinmaxNewsletter::administrator.sms-history.form-templates-select', $componentData);
    }
}
