<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Transformer;
use Minmax\Newsletter\Models\EpaperHistory;

/**
 * Class EpaperHistoryTransformer
 */
class EpaperHistoryTransformer extends Transformer
{
    /**
     * Transformer constructor.
     * @param  EpaperHistoryPresenter $presenter
     * @param  string $uri
     */
    public function __construct(EpaperHistoryPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  EpaperHistory $model
     * @return array
     * @throws \Throwable
     */
    public function transform(EpaperHistory $model)
    {
        return [
            'subject' => $this->presenter->getGridText($model, 'subject'),
            'schedule_at' => $this->presenter->getGridText($model, 'schedule_at'),
            'sent_at' => $this->presenter->getGridText($model, 'sent_at'),
            'result' => $this->presenter->getGridResult($model),
            'track' => $this->presenter->getGridTrack($model),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
