<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Transformer;
use Minmax\Newsletter\Models\SmsCategory;

/**
 * Class SmsCategoryTransformer
 */
class SmsCategoryTransformer extends Transformer
{
    protected $subscriberAmount;

    /**
     * Transformer constructor.
     * @param  SmsCategoryPresenter $presenter
     * @param  string $uri
     */
    public function __construct(SmsCategoryPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  SmsCategory $model
     * @return array
     * @throws \Throwable
     */
    public function transform(SmsCategory $model)
    {
        return [
            'title' => $this->presenter->getGridText($model, 'title'),
            'history_count' => $this->presenter->getGridHistoryCount($model),
            'subscriber_count' => $this->presenter->getGridSubscriberCount($model),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
