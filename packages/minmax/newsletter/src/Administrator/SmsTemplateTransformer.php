<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Transformer;
use Minmax\Newsletter\Models\SmsTemplate;

/**
 * Class SmsTemplateTransformer
 */
class SmsTemplateTransformer extends Transformer
{
    /**
     * Transformer constructor.
     * @param  SmsTemplatePresenter $presenter
     * @param  string $uri
     */
    public function __construct(SmsTemplatePresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  SmsTemplate $model
     * @return array
     * @throws \Throwable
     */
    public function transform(SmsTemplate $model)
    {
        return [
            'title' => $this->presenter->getGridText($model, 'title'),
            'updated_at' => $this->presenter->getGridText($model, 'updated_at'),
            'editable' => $this->presenter->getGridSwitch($model, 'editable'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
