<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Presenter;

/**
 * Class EpaperReceiverPresenter
 */
class EpaperReceiverPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'epaper_status' => systemParam('epaper_status'),
        ];
    }
}
