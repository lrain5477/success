<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Controller;

/**
 * Class EpaperTemplateController
 */
class EpaperTemplateController extends Controller
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct(EpaperTemplateRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }
}
