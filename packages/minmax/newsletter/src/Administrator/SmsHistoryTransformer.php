<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Transformer;
use Minmax\Newsletter\Models\SmsHistory;

/**
 * Class SmsHistoryTransformer
 */
class SmsHistoryTransformer extends Transformer
{
    /**
     * Transformer constructor.
     * @param  SmsHistoryPresenter $presenter
     * @param  string $uri
     */
    public function __construct(SmsHistoryPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  SmsHistory $model
     * @return array
     * @throws \Throwable
     */
    public function transform(SmsHistory $model)
    {
        return [
            'subject' => $this->presenter->getGridText($model, 'subject'),
            'schedule_at' => $this->presenter->getGridText($model, 'schedule_at'),
            'sent_at' => $this->presenter->getGridText($model, 'sent_at'),
            'result' => $this->presenter->getGridResult($model),
            'track' => $this->presenter->getGridTrack($model),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
