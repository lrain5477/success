<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Presenter;
use Minmax\Base\Administrator\WorldLanguageRepository;

/**
 * Class EpaperSubscriberPresenter
 */
class EpaperSubscriberPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'language_id' => (new WorldLanguageRepository)->getSelectParameters(true),
            'categories' => (new EpaperCategoryRepository)->getSelectParameters(),
            'active' => systemParam('active'),
        ];
    }
}
