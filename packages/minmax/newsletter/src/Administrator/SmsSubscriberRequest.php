<?php

namespace Minmax\Newsletter\Administrator;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class SmsSubscriberRequest
 */
class SmsSubscriberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'SmsSubscriber.mobile' => 'required|string',
                    'SmsSubscriber.member_id' => 'nullable|exists:member,username',
                    'SmsSubscriber.language_id' => 'required|exists:world_language,id',
                    'SmsSubscriber.categories' => 'required|array',
                    'SmsSubscriber.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'SmsSubscriber.mobile' => 'required|string',
                    'SmsSubscriber.member_id' => 'nullable|exists:member,username',
                    'SmsSubscriber.language_id' => 'required|exists:world_language,id',
                    'SmsSubscriber.categories' => 'required|array',
                    'SmsSubscriber.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'SmsSubscriber.mobile' => __('MinmaxNewsletter::models.SmsSubscriber.mobile'),
            'SmsSubscriber.member_id' => __('MinmaxNewsletter::models.SmsSubscriber.member_id'),
            'SmsSubscriber.language_id' => __('MinmaxNewsletter::models.SmsSubscriber.language_id'),
            'SmsSubscriber.categories' => __('MinmaxNewsletter::models.SmsSubscriber.categories'),
            'SmsSubscriber.active' => __('MinmaxNewsletter::models.SmsSubscriber.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('administrator', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('administrator', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
