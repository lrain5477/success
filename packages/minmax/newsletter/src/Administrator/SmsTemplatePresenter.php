<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Presenter;

/**
 * Class SmsTemplatePresenter
 */
class SmsTemplatePresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'editable' => systemParam('editable'),
        ];
    }
}
