<?php

namespace Minmax\Newsletter\Administrator;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class SmsTemplateRequest
 */
class SmsTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'SmsTemplate.title' => 'required|string',
                    'SmsTemplate.subject' => 'required|string',
                    'SmsTemplate.content' => 'required|string',
                    'SmsTemplate.editable' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'SmsTemplate.title' => 'required|string',
                    'SmsTemplate.subject' => 'required|string',
                    'SmsTemplate.content' => 'required|string',
                    'SmsTemplate.editable' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'SmsTemplate.title' => __('MinmaxNewsletter::models.SmsTemplate.title'),
            'SmsTemplate.subject' => __('MinmaxNewsletter::models.SmsTemplate.subject'),
            'SmsTemplate.content' => __('MinmaxNewsletter::models.SmsTemplate.content'),
            'SmsTemplate.editable' => __('MinmaxNewsletter::models.SmsTemplate.editable'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('administrator', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('administrator', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
