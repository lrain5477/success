<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Presenter;
use Minmax\Newsletter\Models\EpaperHistory;
use Minmax\Newsletter\Models\EpaperSubscriber;

/**
 * Class EpaperCategoryPresenter
 */
class EpaperCategoryPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    protected $languageColumns = ['title', 'details'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'active' => systemParam('active'),
        ];
    }

    /**
     * @param  \Minmax\Newsletter\Models\EpaperCategory $model
     * @return string
     */
    public function getGridHistoryCount($model)
    {
        $amount = EpaperHistory::query()->whereRaw("json_contains(objects, '\"{$model->id}\"', '$.categories')")->count();

        $url = langRoute('administrator.epaper-history.index', ['category' => $model->id]);

        $gridHtml = <<<HTML
<a class="text-center d-block" href="{$url}">{$amount}</a>
HTML;

        return $gridHtml;
    }

    /**
     * @param  \Minmax\Newsletter\Models\EpaperCategory $model
     * @return string
     */
    public function getGridSubscriberCount($model)
    {
        $amount = EpaperSubscriber::query()->whereRaw("json_contains(categories, '\"{$model->id}\"')")->count();

        $url = langRoute('administrator.epaper-subscriber.index', ['category' => $model->id]);

        $gridHtml = <<<HTML
<a class="text-center d-block" href="{$url}">{$amount}</a>
HTML;

        return $gridHtml;
    }
}
