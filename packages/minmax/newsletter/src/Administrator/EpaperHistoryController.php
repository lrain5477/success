<?php

namespace Minmax\Newsletter\Administrator;

use Illuminate\Http\Request;
use Minmax\Base\Administrator\Controller;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class EpaperHistoryController
 */
class EpaperHistoryController extends Controller
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct(EpaperHistoryRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getQueryBuilder()
    {
        return $this->modelRepository->query()->withCount([
            'epaperReceivers',
            'epaperReceivers as sent_count' => function ($query) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->where('epaper_status', '!=', 'pending');
            },
            'epaperReceivers as opened_count' => function ($query) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->whereNotNull('opened_at');
            },
            'epaperReceivers as clicked_count' => function ($query) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->whereNotNull('clicked_at');
            },
        ]);
    }

    public function store(Request $request)
    {
        $this->checkValidate();

        $inputSet = $request->input($this->pageData->getAttribute('model'));

        $inputSet = $this->doFileUpload($inputSet, $request);

        // 儲存新建資料
        if ($modelData = $this->modelRepository->create($inputSet)) {
            LogHelper::system('administrator', $request->path(), $request->method(), $modelData->getKey(), $this->adminData->username, 1, __('MinmaxBase::administrator.form.message.create_success'));
            return redirect(langRoute("administrator.{$this->uri}.index"))->with('success', __('MinmaxBase::administrator.form.message.create_success'));
        }

        LogHelper::system('administrator', $request->path(), $request->method(), '', $this->adminData->username, 0, __('MinmaxBase::administrator.form.message.create_error'));
        return redirect(langRoute("administrator.{$this->uri}.create"))->withErrors([__('MinmaxBase::administrator.form.message.create_error')])->withInput();
    }

    public function ajaxTemplate(Request $request)
    {
        if ($template = (new EpaperTemplateRepository)->find($request->input('id'))) {
            return response(['subject' => $template->subject, 'editor' => $template->editor], 200, ['Content-Type' => 'application/json']);
        }

        return response(['msg' => 'error'], 400, ['Content-Type' => 'application/json']);
    }
}
