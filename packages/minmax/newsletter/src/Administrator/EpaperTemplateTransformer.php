<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Transformer;
use Minmax\Newsletter\Models\EpaperTemplate;

/**
 * Class EpaperTemplateTransformer
 */
class EpaperTemplateTransformer extends Transformer
{
    /**
     * Transformer constructor.
     * @param  EpaperTemplatePresenter $presenter
     * @param  string $uri
     */
    public function __construct(EpaperTemplatePresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  EpaperTemplate $model
     * @return array
     * @throws \Throwable
     */
    public function transform(EpaperTemplate $model)
    {
        return [
            'title' => $this->presenter->getGridText($model, 'title'),
            'updated_at' => $this->presenter->getGridText($model, 'updated_at'),
            'editable' => $this->presenter->getGridSwitch($model, 'editable'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
