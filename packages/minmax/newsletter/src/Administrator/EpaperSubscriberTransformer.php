<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Transformer;
use Minmax\Newsletter\Models\EpaperSubscriber;

/**
 * Class EpaperSubscriberTransformer
 */
class EpaperSubscriberTransformer extends Transformer
{
    /**
     * Transformer constructor.
     * @param  EpaperSubscriberPresenter $presenter
     * @param  string $uri
     */
    public function __construct(EpaperSubscriberPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  EpaperSubscriber $model
     * @return array
     * @throws \Throwable
     */
    public function transform(EpaperSubscriber $model)
    {
        return [
            'id' => $this->presenter->getGridCheckBox($model),
            'email' => $this->presenter->getGridText($model, 'email'),
            'member_id' => $this->presenter->getPureString(is_null($model->member_id) ? '-' : $model->member->username),
            'created_at' => $this->presenter->getPureString($model->created_at->format('Y-m-d')),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
