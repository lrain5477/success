<?php

namespace Minmax\Newsletter\Administrator;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class EpaperTemplateRequest
 */
class EpaperTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'EpaperTemplate.title' => 'required|string',
                    'EpaperTemplate.subject' => 'required|string',
                    'EpaperTemplate.editor' => 'required|string',
                    'EpaperTemplate.editable' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'EpaperTemplate.title' => 'required|string',
                    'EpaperTemplate.subject' => 'required|string',
                    'EpaperTemplate.editor' => 'required|string',
                    'EpaperTemplate.editable' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'EpaperTemplate.title' => __('MinmaxNewsletter::models.EpaperTemplate.title'),
            'EpaperTemplate.subject' => __('MinmaxNewsletter::models.EpaperTemplate.subject'),
            'EpaperTemplate.editor' => __('MinmaxNewsletter::models.EpaperTemplate.editor'),
            'EpaperTemplate.editable' => __('MinmaxNewsletter::models.EpaperTemplate.editable'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('administrator', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('administrator', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
