<?php

namespace Minmax\Newsletter\Administrator;

use Minmax\Base\Administrator\Controller;

/**
 * Class SmsTemplateController
 */
class SmsTemplateController extends Controller
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct(SmsTemplateRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }
}
