<?php
/**
 * Edit page of model SmsHistory
 *
 * @var \Minmax\Base\Models\Administrator $adminData
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 * @var \Minmax\Newsletter\Models\SmsHistory $formData
 */
?>

@extends('MinmaxBase::administrator.layouts.page.edit', ['formDataId' => $formData->id])

@section('action-buttons')
    @component('MinmaxBase::administrator.layouts.right-links', ['languageActive' => $languageActive])
    <a class="btn btn-sm btn-light" href="{{ langRoute("administrator.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::administrator.form.back_list')">
        <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.back_list')</span>
    </a>
    @endcomponent
@endsection

@section('forms')
    @inject('modelPresenter', 'Minmax\Newsletter\Administrator\SmsHistoryPresenter')

    <fieldset>
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::administrator.form.fieldSet.default')</legend>

        {!! $modelPresenter->getFieldText($formData, 'subject', ['required' => true]) !!}

        {!! $modelPresenter->getFieldTextarea($formData, 'content', ['required' => true, 'hint' => true]) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxNewsletter::administrator.form.fieldSet.objects')</legend>

        {!! $modelPresenter->getFieldCheckbox($formData, 'objects', ['inline' => true, 'subColumn' => 'languages']) !!}

        {!! $modelPresenter->getFieldCheckbox($formData, 'objects', ['inline' => true, 'subColumn' => 'categories']) !!}

        {!! $modelPresenter->getFieldCheckbox($formData, 'objects', ['inline' => true, 'subColumn' => 'roles']) !!}

        {!! $modelPresenter->getFieldText($formData, 'objects', ['subColumn' => 'members', 'hint' => true]) !!}

        {!! $modelPresenter->getFieldText($formData, 'objects', ['subColumn' => 'mobiles', 'hint' => true]) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::administrator.form.fieldSet.advanced')</legend>

        {!! $modelPresenter->getFieldDatePicker($formData, 'schedule_at', ['type' => 'datetime', 'hint' => true]) !!}

    </fieldset>

    <div class="text-center my-4 form-btn-group">
        <input class="btn btn-main" type="submit" id="submitBut" value="@lang('MinmaxBase::administrator.form.button.send')" />
        <input class="btn btn-default" type="reset" value="@lang('MinmaxBase::administrator.form.button.reset')" onclick="window.location.reload(true)" />
    </div>
@endsection
