<?php
/**
 * @var \Minmax\Base\Models\Administrator $adminData
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 * @var \Minmax\Newsletter\Models\SmsHistory $formData
 */
?>

@inject('modelPresenter', 'Minmax\Newsletter\Administrator\SmsReceiverPresenter')

<div class="table-toolbar row">
    @component('MinmaxBase::administrator.layouts.grid.filter-keyword')
    <option value="mobile">@lang('MinmaxNewsletter::models.SmsReceiver.mobile')</option>
    @endcomponent

    @component('MinmaxBase::administrator.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('sms_status', 'searchStatus', ['emptyLabel' => __('MinmaxNewsletter::models.SmsReceiver.sms_status')]) !!}
    @endcomponent
</div>

<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th>@lang('MinmaxNewsletter::models.SmsReceiver.mobile')</th>
        <th>@lang('MinmaxNewsletter::models.SmsReceiver.name')</th>
        <th>@lang('MinmaxNewsletter::models.SmsReceiver.sms_status')</th>
        <th>@lang('MinmaxNewsletter::models.SmsReceiver.opened_at')</th>
        <th>@lang('MinmaxNewsletter::models.SmsReceiver.clicked_at')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'mobile', name: 'mobile'},
            {data: 'name', name: 'name'},
            {data: 'sms_status', name: 'sms_status'},
            {data: 'opened_at', name: 'opened_at'},
            {data: 'clicked_at', name: 'clicked_at'}
        ],
        ['email'],
        {"sms_status":"searchStatus"},
        [[0, 'asc']],
        '{{ langRoute("administrator.sms-receiver.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}'
    );
});
</script>
@endpush
