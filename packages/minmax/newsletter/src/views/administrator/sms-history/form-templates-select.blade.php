<?php
/**
 * @var string $id
 * @var array $listData
 */
?>
<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="{{ $id }}">@lang('MinmaxNewsletter::models.SmsHistory.templates')</label>
    <div class="col-sm-5">
        <div class="input-group">
            <select class="custom-select ignore-valid" id="{{ $id }}">
                <option value="" selected>@lang('MinmaxBase::administrator.form.select_default_title')</option>
                @foreach($listData as $listKey => $listItem)
                <option value="{{ $listKey }}">{{ array_get($listItem, 'title') }}</option>
                @endforeach
            </select>
            <div class="input-group-prepend">
                <button class="btn btn-secondary" type="button" id="{{ $id }}-apply">@lang('MinmaxNewsletter::models.SmsHistory.templates_button')</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
$(function () {
    $('#{{ $id }}-apply').on('click', function () {
        var id = $('#{{ $id }}').val();
        if ($('#SmsHistory-subject').length > 0 || $('#SmsHistory-content').length > 0) {
            if (id === '') {
                swal('No Template', 'Please select a template to apply!');
            } else {
                $.get('{{ langRoute('administrator.sms-history.ajaxTemplate') }}', {id: id}, function (response) {
                    if (response.hasOwnProperty('subject') && $('#SmsHistory-subject').length > 0) {
                        $('#SmsHistory-subject').val(response.subject);
                    }
                    if (response.hasOwnProperty('content') && $('#SmsHistory-content').length > 0) {
                        $('#SmsHistory-content').val(response.content);
                    }
                });
            }
        }
    });
});
</script>
@endpush
