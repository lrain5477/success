<?php
/**
 * List of model SmsTemplate
 *
 * @var \Minmax\Base\Models\Administrator $adminData
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 * @var \Minmax\Newsletter\Models\SmsTemplate $formData
 */
?>

@extends('MinmaxBase::administrator.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::administrator.layouts.right-links')
    <a class="btn btn-sm btn-main" href="{{ langRoute("administrator.{$pageData->uri}.create") }}" title="@lang('MinmaxBase::administrator.form.create')">
        <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.create')</span>
    </a>
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Newsletter\Administrator\SmsTemplatePresenter')

@section('grid-filter')
    @component('MinmaxBase::administrator.layouts.grid.filter-keyword')
    <option value="title">@lang('MinmaxNewsletter::models.SmsTemplate.title')</option>
    <option value="subject">@lang('MinmaxNewsletter::models.SmsTemplate.subject')</option>
    @endcomponent

    @component('MinmaxBase::administrator.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('editable', 'searchEditable', ['emptyLabel' => __('MinmaxNewsletter::models.SmsTemplate.editable')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="w-50">@lang('MinmaxNewsletter::models.SmsTemplate.title')</th>
        <th>@lang('MinmaxNewsletter::models.SmsTemplate.updated_at')</th>
        <th>@lang('MinmaxNewsletter::models.SmsTemplate.editable')</th>
        <th class="nosort">@lang('MinmaxBase::administrator.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'title', name: 'title'},
            {data: 'updated_at', name: 'updated_at'},
            {data: 'editable', name: 'editable'},
            {data: 'action', name: 'action'}
        ],
        ['title', 'subject'],
        {"editable":"searchEditable"},
        [[1, 'desc']],
        '{{ langRoute("administrator.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("administrator.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
