<?php
/**
 * @var \Minmax\Base\Models\Administrator $adminData
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 * @var \Minmax\Newsletter\Models\EpaperHistory $formData
 */
?>

@inject('modelPresenter', 'Minmax\Newsletter\Administrator\EpaperReceiverPresenter')

<div class="table-toolbar row">
    @component('MinmaxBase::administrator.layouts.grid.filter-keyword')
    <option value="email">@lang('MinmaxNewsletter::models.EpaperReceiver.email')</option>
    @endcomponent

    @component('MinmaxBase::administrator.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('epaper_status', 'searchStatus', ['emptyLabel' => __('MinmaxNewsletter::models.EpaperReceiver.epaper_status')]) !!}
    @endcomponent
</div>

<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th>@lang('MinmaxNewsletter::models.EpaperReceiver.email')</th>
        <th>@lang('MinmaxNewsletter::models.EpaperReceiver.name')</th>
        <th>@lang('MinmaxNewsletter::models.EpaperReceiver.epaper_status')</th>
        <th>@lang('MinmaxNewsletter::models.EpaperReceiver.opened_at')</th>
        <th>@lang('MinmaxNewsletter::models.EpaperReceiver.clicked_at')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'email', name: 'email'},
            {data: 'name', name: 'name'},
            {data: 'epaper_status', name: 'epaper_status'},
            {data: 'opened_at', name: 'opened_at'},
            {data: 'clicked_at', name: 'clicked_at'}
        ],
        ['email'],
        {"epaper_status":"searchStatus"},
        [[0, 'asc']],
        '{{ langRoute("administrator.epaper-receiver.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}'
    );
});
</script>
@endpush
