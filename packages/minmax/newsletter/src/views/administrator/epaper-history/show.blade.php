<?php
/**
 * Show page of model EpaperHistory
 *
 * @var \Minmax\Base\Models\Administrator $adminData
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 * @var \Minmax\Newsletter\Models\EpaperHistory $formData
 */
?>

@extends('MinmaxBase::administrator.layouts.site')

@section('breadcrumbs', Breadcrumbs::view('MinmaxBase::administrator.layouts.breadcrumbs', 'show'))

@section('content')
<!-- layout-content-->
<section class="panel panel-default">
    <header class="panel-heading">
        <h1 class="h5 float-left font-weight-bold">{{ $pageData->title }} @lang('MinmaxBase::administrator.form.show')</h1>

        @component('MinmaxBase::administrator.layouts.right-links', ['languageActive' => $languageActive])
            <a class="btn btn-sm btn-light" href="{{ langRoute("administrator.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::administrator.form.back_list')">
                <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.back_list')</span>
            </a>
            @if(is_null($formData->sent_at))
            <a class="btn btn-sm btn-main" href="{{ langRoute("administrator.{$pageData->uri}.edit", [$formData->id]) }}" title="@lang('MinmaxBase::administrator.form.edit')">
                <i class="icon-pencil"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.edit')</span>
            </a>
            @endif
        @endcomponent
    </header>

    <div class="panel-wrapper">
        <div class="panel-body">
            <header class="mb-4">
                <ul class="nav nav-tabs" id="ioTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active"
                           id="tab-base" data-toggle="tab"
                           href="#tab-pane-base" role="tab"
                           aria-controls="tab-pane-base" aria-selected="true">@lang('MinmaxNewsletter::administrator.form.tab.base')</a>
                    </li>
                    @if(! is_null($formData->sent_at))
                    <li class="nav-item">
                        <a class="nav-link"
                           id="tab-receiver" data-toggle="tab"
                           href="#tab-pane-receiver" role="tab"
                           aria-controls="tab-pane-receiver" aria-selected="true">@lang('MinmaxNewsletter::administrator.form.tab.receiver')</a>
                    </li>
                    @endif
                </ul>
            </header>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="tab-pane-base" role="tabpanel" aria-labelledby="tab-base">
                    @include('MinmaxNewsletter::administrator.epaper-history.show-panel-base')
                </div>
                @if(! is_null($formData->sent_at))
                <div class="tab-pane fade" id="tab-pane-receiver" role="tabpanel" aria-labelledby="tab-receiver">
                    @include('MinmaxNewsletter::administrator.epaper-history.show-panel-receiver')
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
<!-- / layout-content-->
@endsection
