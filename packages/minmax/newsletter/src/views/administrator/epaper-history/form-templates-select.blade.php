<?php
/**
 * @var string $id
 * @var array $listData
 */
?>
<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="{{ $id }}">@lang('MinmaxNewsletter::models.EpaperHistory.templates')</label>
    <div class="col-sm-5">
        <div class="input-group">
            <select class="custom-select ignore-valid" id="{{ $id }}">
                <option value="" selected>@lang('MinmaxBase::admin.form.select_default_title')</option>
                @foreach($listData as $listKey => $listItem)
                <option value="{{ $listKey }}">{{ array_get($listItem, 'title') }}</option>
                @endforeach
            </select>
            <div class="input-group-prepend">
                <button class="btn btn-secondary" type="button" id="{{ $id }}-apply">@lang('MinmaxNewsletter::models.EpaperHistory.templates_button')</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
$(function () {
    $('#{{ $id }}-apply').on('click', function () {
        var id = $('#{{ $id }}').val();
        if ($('#EpaperHistory-subject').length > 0 || $('#EpaperHistory-editor').length > 0) {
            if (id === '') {
                swal('No Template', 'Please select a template to apply!');
            } else {
                $.get('{{ langRoute('admin.epaper-history.ajaxTemplate') }}', {id: id}, function (response) {
                    if (response.hasOwnProperty('subject') && $('#EpaperHistory-subject').length > 0) {
                        $('#EpaperHistory-subject').val(response.subject);
                    }
                    if (response.hasOwnProperty('editor') && $('#EpaperHistory-editor').length > 0) {
                        CKEDITOR.instances['EpaperHistory-editor'].setData(response.editor);
                    }
                });
            }
        }
    });
});
</script>
@endpush
