<?php
/**
 * List of model EpaperHistory
 *
 * @var \Minmax\Base\Models\Administrator $adminData
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 * @var \Minmax\Newsletter\Models\EpaperHistory $formData
 */
?>

@extends('MinmaxBase::administrator.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::administrator.layouts.right-links')
    <a class="btn btn-sm btn-main" href="{{ langRoute("administrator.{$pageData->uri}.create") }}" title="@lang('MinmaxBase::administrator.form.create')">
        <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.create')</span>
    </a>
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Newsletter\Administrator\EpaperHistoryPresenter')

@section('grid-filter')
    @component('MinmaxBase::administrator.layouts.grid.filter-keyword')
    <option value="subject">@lang('MinmaxNewsletter::models.EpaperHistory.subject')</option>
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="w-50">@lang('MinmaxNewsletter::models.EpaperHistory.subject')</th>
        <th>@lang('MinmaxNewsletter::models.EpaperHistory.schedule_at')</th>
        <th>@lang('MinmaxNewsletter::models.EpaperHistory.sent_at')</th>
        <th class="nosort">@lang('MinmaxNewsletter::administrator.grid.EpaperHistory.result')</th>
        <th class="nosort">@lang('MinmaxNewsletter::administrator.grid.EpaperHistory.track')</th>
        <th class="nosort">@lang('MinmaxBase::administrator.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'subject', name: 'subject'},
            {data: 'schedule_at', name: 'schedule_at'},
            {data: 'sent_at', name: 'sent_at'},
            {data: 'result', name: 'result'},
            {data: 'track', name: 'track'},
            {data: 'action', name: 'action'}
        ],
        ['subject'],
        {},
        [[1, 'desc']],
        '{{ langRoute("administrator.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("administrator.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
