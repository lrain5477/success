<?php
/**
 * Edit page of model SmsHistory
 *
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Newsletter\Models\SmsHistory $formData
 */
?>

@extends('MinmaxBase::admin.layouts.page.edit', ['formDataId' => $formData->id])

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links', ['languageActive' => $languageActive])
        @if($adminData->can('smsHistoryShow'))
        <a class="btn btn-sm btn-light" href="{{ langRoute("admin.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::admin.form.back_list')">
            <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.back_list')</span>
        </a>
        @endif
    @endcomponent
@endsection

@section('forms')
    @inject('modelPresenter', 'Minmax\Newsletter\Admin\SmsHistoryPresenter')

    <fieldset>
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.default')</legend>

        {!! $modelPresenter->getFieldText($formData, 'subject', ['required' => true]) !!}

        {!! $modelPresenter->getFieldTextarea($formData, 'content', ['required' => true, 'hint' => true]) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxNewsletter::admin.form.fieldSet.objects')</legend>

        {!! $modelPresenter->getFieldCheckbox($formData, 'objects', ['inline' => true, 'subColumn' => 'languages']) !!}

        {!! $modelPresenter->getFieldCheckbox($formData, 'objects', ['inline' => true, 'subColumn' => 'categories']) !!}

        {!! $modelPresenter->getFieldCheckbox($formData, 'objects', ['inline' => true, 'subColumn' => 'roles']) !!}

        {!! $modelPresenter->getFieldText($formData, 'objects', ['subColumn' => 'members', 'hint' => true]) !!}

        {!! $modelPresenter->getFieldText($formData, 'objects', ['subColumn' => 'mobiles', 'hint' => true]) !!}

    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.advanced')</legend>

        {!! $modelPresenter->getFieldDatePicker($formData, 'schedule_at', ['type' => 'datetime', 'hint' => true]) !!}

    </fieldset>

    <div class="text-center my-4 form-btn-group">
        <input class="btn btn-main" type="submit" id="submitBut" value="@lang('MinmaxBase::admin.form.button.send')" />
        <input class="btn btn-default" type="reset" value="@lang('MinmaxBase::admin.form.button.reset')" onclick="window.location.reload(true)" />
    </div>
@endsection
