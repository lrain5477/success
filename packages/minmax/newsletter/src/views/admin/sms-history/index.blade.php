<?php
/**
 * List of model SmsHistory
 *
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Newsletter\Models\SmsHistory $formData
 */
?>

@extends('MinmaxBase::admin.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links')
        @if($adminData->can('smsHistoryCreate'))
        <a class="btn btn-sm btn-main" href="{{ langRoute("admin.{$pageData->uri}.create") }}" title="@lang('MinmaxBase::admin.form.create')">
            <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.create')</span>
        </a>
        @endif
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Newsletter\Admin\SmsHistoryPresenter')

@section('grid-filter')
    @component('MinmaxBase::admin.layouts.grid.filter-keyword')
    <option value="subject">@lang('MinmaxNewsletter::models.SmsHistory.subject')</option>
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="w-50">@lang('MinmaxNewsletter::models.SmsHistory.subject')</th>
        <th>@lang('MinmaxNewsletter::models.SmsHistory.schedule_at')</th>
        <th>@lang('MinmaxNewsletter::models.SmsHistory.sent_at')</th>
        <th class="nosort">@lang('MinmaxNewsletter::admin.grid.SmsHistory.result')</th>
        <th class="nosort">@lang('MinmaxNewsletter::admin.grid.SmsHistory.track')</th>
        <th class="nosort">@lang('MinmaxBase::admin.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'subject', name: 'subject'},
            {data: 'schedule_at', name: 'schedule_at'},
            {data: 'sent_at', name: 'sent_at'},
            {data: 'result', name: 'result'},
            {data: 'track', name: 'track'},
            {data: 'action', name: 'action'}
        ],
        ['subject'],
        {},
        [[1, 'desc']],
        '{{ langRoute("admin.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("admin.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
