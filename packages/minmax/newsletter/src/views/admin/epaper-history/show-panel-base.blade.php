<?php
/**
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Newsletter\Models\EpaperHistory $formData
 */
?>

@inject('modelPresenter', 'Minmax\Newsletter\Admin\EpaperHistoryPresenter')

<fieldset>
    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.default')</legend>

    {!! $modelPresenter->getShowNormalText($formData, 'subject') !!}

    {!! $modelPresenter->getShowEditor($formData, 'editor', ['height' => '550px']) !!}

</fieldset>

<fieldset class="mt-4">
    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxNewsletter::admin.form.fieldSet.objects')</legend>

    {!! $modelPresenter->getShowMultiSelection($formData, 'objects', ['subColumn' => 'languages']) !!}

    {!! $modelPresenter->getShowMultiSelection($formData, 'objects', ['subColumn' => 'categories']) !!}

    {!! $modelPresenter->getShowMultiSelection($formData, 'objects', ['subColumn' => 'roles']) !!}

    {!! $modelPresenter->getShowNormalText($formData, 'objects', ['subColumn' => 'members']) !!}

    {!! $modelPresenter->getShowNormalText($formData, 'objects', ['subColumn' => 'emails']) !!}

</fieldset>

<fieldset class="mt-4">
    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.advanced')</legend>

    {!! $modelPresenter->getShowNormalText($formData, 'schedule_at') !!}

    {!! $modelPresenter->getShowNormalText($formData, 'sent_at') !!}

</fieldset>

<fieldset class="mt-4" id="sysFieldSet">
    <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.system_record')</legend>

    {!! $modelPresenter->getShowNormalText($formData, 'created_at') !!}

    {!! $modelPresenter->getShowNormalText($formData, 'updated_at') !!}

</fieldset>
