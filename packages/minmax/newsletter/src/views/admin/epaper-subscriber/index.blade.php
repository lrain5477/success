<?php
/**
 * List of model EpaperSubscriber
 *
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Newsletter\Models\EpaperSubscriber $formData
 */
?>

@extends('MinmaxBase::admin.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links')
        @if($adminData->can('epaperSubscriberCreate'))
        <a class="btn btn-sm btn-main" href="{{ langRoute("admin.{$pageData->uri}.create") }}" title="@lang('MinmaxBase::admin.form.create')">
            <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.create')</span>
        </a>
        @endif
        @slot('batchActions')
            @if($adminData->can('epaperSubscriberEdit'))
            <button class="dropdown-item" type="button" onclick="multiSwitch('{{ langRoute("admin.{$pageData->uri}.ajaxMultiSwitch") }}', 'active', 1)"><i class="icon-eye mr-2 text-muted"></i>啟用</button>
            <button class="dropdown-item" type="button" onclick="multiSwitch('{{ langRoute("admin.{$pageData->uri}.ajaxMultiSwitch") }}', 'active', 0)"><i class="icon-cancel mr-2 text-muted"></i>停用</button>
            @endif
            @if($adminData->can('epaperSubscriberDestroy'))
            <button class="dropdown-item" type="button" onclick="multiDelete('{{ langRoute("admin.{$pageData->uri}.ajaxMultiDestroy") }}')"><i class="icon-trashcan mr-2 text-muted"></i>刪除</button>
            @endif
        @endslot
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Newsletter\Admin\EpaperSubscriberPresenter')

@section('grid-filter')
    @component('MinmaxBase::admin.layouts.grid.filter-keyword')
    <option value="email">@lang('MinmaxNewsletter::models.EpaperSubscriber.email')</option>
    @if(in_array(\Minmax\Member\ServiceProvider::class, config('app.providers')))
    <option value="member_id">@lang('MinmaxNewsletter::models.EpaperSubscriber.member_id')</option>
    @endif
    @endcomponent

    @component('MinmaxBase::admin.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('language_id', 'searchLanguage', ['emptyLabel' => __('MinmaxNewsletter::models.EpaperSubscriber.language_id')]) !!}
    {!! $modelPresenter->getFilterSelection('categories', 'searchCategory', ['emptyLabel' => __('MinmaxNewsletter::models.EpaperSubscriber.categories')]) !!}
    {!! $modelPresenter->getFilterSelection('active', 'searchActive', ['emptyLabel' => __('MinmaxNewsletter::models.EpaperSubscriber.active')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="nosort w-3">
            <div class="custom-control custom-checkbox">
                <input class="custom-control-input group-checkable" type="checkbox" aria-label="Select" data-set="#tableList .checkboxes input" id="checkAll" />
                <label class="custom-control-label" for="checkAll"></label>
            </div>
        </th>
        <th class="w-50">@lang('MinmaxNewsletter::models.EpaperSubscriber.email')</th>
        @if(in_array(\Minmax\Member\ServiceProvider::class, config('app.providers')))
        <th class="nosort">@lang('MinmaxNewsletter::models.EpaperSubscriber.member_id')</th>
        @endif
        <th>@lang('MinmaxNewsletter::models.EpaperSubscriber.created_at')</th>
        <th>@lang('MinmaxNewsletter::models.EpaperSubscriber.active')</th>
        <th class="nosort">@lang('MinmaxBase::admin.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'id', name: 'id'},
            {data: 'email', name: 'email'},
            @if(in_array(\Minmax\Member\ServiceProvider::class, config('app.providers')))
            {data: 'member_id', name: 'member_id'},
            @endif
            {data: 'created_at', name: 'created_at'},
            {data: 'active', name: 'active'},
            {data: 'action', name: 'action'}
        ],
        ['email', 'member_id'],
        {"language_id":"searchLanguage", "categories":"searchCategory", "active":"searchActive"},
        [[parseInt('{{ in_array(\Minmax\Member\ServiceProvider::class, config('app.providers')) ? '3' : '2' }}'), 'desc']],
        '{{ langRoute("admin.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("admin.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
