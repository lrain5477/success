<?php
/**
 * @var \Minmax\Newsletter\Models\EpaperReceiver $receiver
 * @var \Minmax\Newsletter\Models\EpaperHistory $newsletter
 */
?>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>{{ $newsletter->subject }}</title>
</head>
<body style="background-color:#ffffff;margin:0;font-size:13px;font-family: Tahoma, Geneva, sans-serif">
    {!! $newsletter->editor !!}
    <img src="{{ langRoute('web.newsletter.opened', [$receiver->id]) }}" alt="" style="display: block; padding: 0; margin: 0; width: 1px; height: 1px; visibility: hidden; opacity: 0; position: absolute; bottom: 0;" />
</body>
</html>
