<?php

namespace Minmax\Newsletter\Web;

use Minmax\Base\Web\Controller;

/**
 * Class EpaperReceiverController
 */
class EpaperReceiverController extends Controller
{
    protected $packagePrefix = 'MinmaxNewsletter::';

    public function __construct(EpaperReceiverRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }

    public function openedTrack($id)
    {
        $this->modelRepository->newsletterOpened($id);

        return response(
            base64_decode('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII='),
            200, ['Content-Type' => 'image/png']
        );
    }
}
