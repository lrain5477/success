<?php

namespace Minmax\Newsletter;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Minmax\Newsletter\Console\MinmaxNewsletterCommand;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/admin.php');
        $this->loadRoutesFrom(__DIR__ . '/routes/administrator.php');
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/translations', 'MinmaxNewsletter');
        $this->loadViewsFrom(__DIR__ . '/views', 'MinmaxNewsletter');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('command.minmax.newsletter', function () {
            return new MinmaxNewsletterCommand();
        });

        $this->commands(
            'command.minmax.newsletter'
        );
    }
}
