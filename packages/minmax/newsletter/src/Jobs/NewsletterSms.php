<?php

namespace Minmax\Newsletter\Jobs;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Minmax\Newsletter\Models\SmsHistory;
use Minmax\Newsletter\Models\SmsReceiver;

class NewsletterSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var SmsHistory $history
     */
    protected $history;

    /**
     * @var SmsReceiver $receiver
     */
    protected $receiver;

    /**
     * Create a new message instance.
     *
     * @param  SmsHistory $history
     * @param  SmsReceiver $receiver
     * @return void
     */
    public function __construct(SmsHistory $history, SmsReceiver $receiver)
    {
        $this->history = $history;
        $this->receiver = $receiver;

    }

    /**
     * Process Job.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        if ($smsConfig = serviceConfig('sms', true)) {
            if ($senderClass = array_get($smsConfig->options, 'class')) {
                /** @var \Minmax\Newsletter\Services\SmsSender $sender */
                $sender = new $senderClass();
                $result = $sender->send($this->receiver->mobile, $this->history->content);

                if (is_null($result)) {
                    throw new Exception('SMS send failed.');
                }
            }
        }
    }

    /**
     * Process if queue job failed
     *
     * @param  Exception $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $this->receiver->sms_status = 'failed';
        $this->receiver->save();
    }
}
