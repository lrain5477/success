<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class CreateNewsletterTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // 電子報類別
        Schema::create('epaper_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('類別名稱');                           // Language
            $table->string('details')->nullable()->comment('類別詳細');             // Language {description}
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // 訂閱名單
        Schema::create('epaper_subscriber', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->comment('Email');
            $table->string('member_id')->nullable()->comment('會員ID');
            $table->unsignedInteger('language_id')->nullable()->comment('訂閱語系');
            $table->json('categories')->nullable()->comment('訂閱類別');            // [1,2,3,...]
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('world_language')
                ->onUpdate('cascade')->onDelete('cascade');

            if (in_array(\Minmax\Member\ServiceProvider::class, config('app.providers'))) {
                $table->foreign('member_id')->references('id')->on('member')
                    ->onUpdate('cascade')->onDelete('cascade');
            }
        });

        // 電子報範本
        Schema::create('epaper_template', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('範本標題');
            $table->string('subject')->nullable()->comment('信件主旨');
            $table->longText('editor')->nullable()->comment('信件內容');
            $table->boolean('editable')->default(true)->comment('可否編輯');
            $table->timestamps();
        });

        // 發送歷史
        Schema::create('epaper_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject')->comment('信件主旨');
            $table->longText('editor')->nullable()->comment('信件內容');
            $table->json('objects')->nullable()->comment('發送目標');               // {languages:[], categories:[], roles:[], members:[], emails:[]}
            $table->dateTime('schedule_at')->useCurrent()->comment('排程時間');
            $table->timestamp('sent_at')->nullable()->comment('發送時間');
            $table->timestamps();
        });

        // 電子報收件者
        Schema::create('epaper_receiver', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('history_id')->comment('電子報ID');
            $table->string('email')->comment('Email');
            $table->string('name')->nullable()->comment('姓名');
            $table->string('epaper_status')->default('pending')->comment('寄送狀態');   // pending:等待中 sent:已寄送 failed:寄送失敗
            $table->timestamp('opened_at')->nullable()->comment('開信時間');
            $table->timestamp('clicked_at')->nullable()->comment('點擊時間');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('history_id')->references('id')->on('epaper_history')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // 簡訊類別
        Schema::create('sms_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('類別名稱');                           // Language
            $table->string('details')->nullable()->comment('類別詳細');             // Language {description}
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // 簡訊訂閱名單
        Schema::create('sms_subscriber', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mobile')->comment('電話');
            $table->string('member_id')->nullable()->comment('會員ID');
            $table->unsignedInteger('language_id')->nullable()->comment('訂閱語系');
            $table->json('categories')->nullable()->comment('訂閱類別');            // [1,2,3,...]
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('world_language')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // 簡訊範本
        Schema::create('sms_template', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('範本標題');
            $table->string('subject')->nullable()->comment('簡訊主旨');
            $table->longText('content')->nullable()->comment('簡訊內容');
            $table->boolean('editable')->default(true)->comment('可否編輯');
            $table->timestamps();
        });

        // 發送歷史
        Schema::create('sms_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject')->comment('簡訊主旨');
            $table->longText('content')->nullable()->comment('簡訊內容');
            $table->json('objects')->nullable()->comment('發送目標');               // {categories:[], roles:[], members:[], mobiles:[]}
            $table->dateTime('schedule_at')->useCurrent()->comment('排程時間');
            $table->timestamp('sent_at')->nullable()->comment('發送時間');
            $table->timestamps();
        });

        // 簡訊收件者
        Schema::create('sms_receiver', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('history_id')->comment('簡訊ID');
            $table->string('mobile')->comment('電話');
            $table->string('name')->nullable()->comment('姓名');
            $table->string('sms_status')->default('pending')->comment('寄送狀態');      // pending:等待中 sent:已寄送 failed:寄送失敗
            $table->timestamp('opened_at')->nullable()->comment('開信時間');
            $table->timestamp('clicked_at')->nullable()->comment('點擊時間');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('history_id')->references('id')->on('sms_history')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        $this->insertSystemParameters();

        $this->insertDefaultCategory();

        $this->insertNewsletterService();
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        $this->deleteSystemParameter();

        Schema::dropIfExists('sms_receiver');
        Schema::dropIfExists('sms_history');
        Schema::dropIfExists('sms_template');
        Schema::dropIfExists('sms_subscriber');
        Schema::dropIfExists('sms_category');
        Schema::dropIfExists('epaper_receiver');
        Schema::dropIfExists('epaper_history');
        Schema::dropIfExists('epaper_template');
        Schema::dropIfExists('epaper_subscriber');
        Schema::dropIfExists('epaper_category');
    }

    protected function insertSystemParameters()
    {
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $startGroupId = $rowGroupId = SeederHelper::getTableNextIncrement('system_parameter_group');
        $rowGroupId--;

        $systemGroupData = [
            ['code' => 'epaper_status', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
            ['code' => 'sms_status', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
        ];
        DB::table('system_parameter_group')->insert($systemGroupData);

        $systemGroupLanguage = [
            'zh-Hant' => [
                ['title' => '電子報寄送狀態'], ['title' => '簡訊寄送狀態']
            ],
            'zh-Hans' => [
                ['title' => '电子报寄送状态'], ['title' => '短信寄送状态']
            ],
            'ja' => [
                ['title' => 'ニュースレター配信状況'], ['title' => 'メール配信状況']
            ],
            'en' => [
                ['title' => 'E-Paper Status'], ['title' => 'SMS Status']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'system_parameter_group', $systemGroupLanguage, $languageList, $startGroupId, false);

        $startItemId = $rowItemId = SeederHelper::getTableNextIncrement('system_parameter_item');
        $rowItemId--;

        $systemItemData = [
            [
                'group_id' => $startGroupId,
                'value' => 'pending',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'warning']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId,
                'value' => 'sent',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'success']),
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId,
                'value' => 'failed',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'danger']),
                'sort' => 3,
            ],
            [
                'group_id' => $startGroupId + 1,
                'value' => 'pending',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'warning']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 1,
                'value' => 'sent',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'success']),
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 1,
                'value' => 'failed',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'danger']),
                'sort' => 3,
            ],
        ];
        DB::table('system_parameter_item')->insert($systemItemData);

        // 多語系
        $systemItemLanguage = [
            'zh-Hant' => [
                ['label' => '等待中'], ['label' => '已寄送'], ['label' => '寄送失敗'], ['label' => '等待中'], ['label' => '已寄送'], ['label' => '寄送失敗']
            ],
            'zh-Hans' => [
                ['label' => '等待中'], ['label' => '已寄发'], ['label' => '寄发失败'], ['label' => '等待中'], ['label' => '已寄发'], ['label' => '寄发失败']
            ],
            'ja' => [
                ['label' => '待っている'], ['label' => '送信済み'], ['label' => '送信失敗'], ['label' => '待っている'], ['label' => '送信済み'], ['label' => '送信失敗']
            ],
            'en' => [
                ['label' => 'Pending'], ['label' => 'Sent'], ['label' => 'Failed'], ['label' => 'Pending'], ['label' => 'Sent'], ['label' => 'Failed']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'system_parameter_item', $systemItemLanguage, $languageList, $startItemId, false);

        DB::table('language_resource')->insert($languageResourceData);
    }

    protected function insertDefaultCategory()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $startEpaperId = $rowEpaperId = SeederHelper::getTableNextIncrement('epaper_category');
        $rowEpaperId--;

        $epaperCategoryData = [
            [
                'title' => 'epaper_category.title.' . ++$rowEpaperId,
                'details' => 'epaper_category.details.' . $rowEpaperId,
                'sort' => 1, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];
        DB::table('epaper_category')->insert($epaperCategoryData);

        $epaperCategoryLanguage = [
            'zh-Hant' => [
                ['title' => '最新消息']
            ],
            'zh-Hans' => [
                ['title' => '最新消息']
            ],
            'ja' => [
                ['title' => 'ニュース']
            ],
            'en' => [
                ['title' => 'Newsletter']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'epaper_category', $epaperCategoryLanguage, $languageList, $startEpaperId, false);

        $startSmsId = $rowSmsId = SeederHelper::getTableNextIncrement('sms_category');
        $rowSmsId--;

        $smsCategoryData = [
            [
                'title' => 'sms_category.title.' . ++$rowSmsId,
                'details' => 'sms_category.details.' . $rowSmsId,
                'sort' => 1, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];
        DB::table('sms_category')->insert($smsCategoryData);

        $smsCategoryLanguage = [
            'zh-Hant' => [
                ['title' => '最新消息']
            ],
            'zh-Hans' => [
                ['title' => '最新消息']
            ],
            'ja' => [
                ['title' => 'ニュース']
            ],
            'en' => [
                ['title' => 'Newsletter']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'sms_category', $smsCategoryLanguage, $languageList, $startSmsId, false);

        DB::table('language_resource')->insert($languageResourceData);
    }

    protected function insertNewsletterService()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        // 服務設定
        $startServiceId = $rowServiceId = SeederHelper::getTableNextIncrement('service_config');
        $rowServiceId--;

        $serviceConfigData = [
            [
                'code' => 'epaper',
                'group' => 'email',
                'title' => 'service_config.title.' . ++$rowServiceId,
                'host' => config('mail.host') . ':' . config('mail.port'),
                'options' => json_encode([
                    'driver' => config('mail.driver'),
                    'username' => config('mail.username'),
                    'password' => config('mail.password'),
                    'encryption' => config('mail.encryption'),
                    'from.address' => config('mail.from.address'),
                    'from.name' => config('mail.from.name'),
                ]),
                'parameters' => json_encode([
                    'driver' => '連線方式',
                    'username' => '連線帳號',
                    'password' => '連線密碼',
                    'encryption' => '加密方式',
                    'from.address' => '寄件者信箱',
                    'from.name' => '寄件者名稱',
                ]),
                'sort' => 2, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'code' => 'sms-kot',
                'group' => 'sms',
                'title' => 'service_config.title.' . ++$rowServiceId,
                'host' => 'http://202.39.48.216/kotsmsapi-1.php',
                'options' => json_encode([
                    'class' => '\Minmax\Newsletter\Services\SmsKotSender',
                    'username' => 'testapi01',
                    'password' => 'testapi',
                    'dlvtime' => null,
                    'vldtime' => null,
                    'response' => null,
                ]),
                'parameters' => json_encode([
                    'class' => '發送器類別',
                    'username' => '連線帳號',
                    'password' => '連線密碼',
                    'dlvtime' => '預約發送時間',
                    'vldtime' => '發送有效期限',
                    'response' => '回報網址',
                ]),
                'sort' => 3, 'active' => false, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'code' => 'sms-mitake',
                'group' => 'sms',
                'title' => 'service_config.title.' . ++$rowServiceId,
                'host' => 'http://sms.mitake.com.tw/b2c/mtk/SmSend',
                'options' => json_encode([
                    'class' => '\Minmax\Newsletter\Services\SmsMitakeSender',
                    'username' => 'test',
                    'password' => 'test',
                    'dlvtime' => null,
                    'vldtime' => null,
                    'response' => null,
                ]),
                'parameters' => json_encode([
                    'class' => '發送器類別',
                    'username' => '連線帳號',
                    'password' => '連線密碼',
                    'dlvtime' => '預約發送時間',
                    'vldtime' => '發送有效期限',
                    'response' => '回報網址',
                ]),
                'sort' => 4, 'active' => false, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'code' => 'sms-ite',
                'group' => 'sms',
                'title' => 'service_config.title.' . ++$rowServiceId,
                'host' => 'http://ep2.ite2.com.tw/SMS/fpmsservice.asmx?WSDL',
                'options' => json_encode([
                    'class' => '\Minmax\Newsletter\Services\SmsIteSender',
                    'user' => null,
                    'pass' => null,
                    'stoptime' => null,
                ]),
                'parameters' => json_encode([
                    'class' => '發送器類別',
                    'user' => '連線帳號',
                    'pass' => '連線密碼',
                    'stoptime' => '發送有效期限',
                ]),
                'sort' => 5, 'active' => false, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];
        DB::table('service_config')->insert($serviceConfigData);

        // 多語系
        $siteServiceLanguage = [
            'zh-Hant' => [
                ['title' => '電子報伺服器'], ['title' => '簡訊王'], ['title' => '三竹簡訊'], ['title' => '詮力科技簡訊']
            ],
            'zh-Hans' => [
                ['title' => '电子报伺服器'], ['title' => '简讯王'], ['title' => '三竹简讯'], ['title' => '诠力科技简讯']
            ],
            'ja' => [
                ['title' => 'ニュースレターサーバー'], ['title' => '簡訊王'], ['title' => '三竹簡訊'], ['title' => '詮力科技簡訊']
            ],
            'en' => [
                ['title' => 'E-Paper Server'], ['title' => 'SMS King'], ['title' => 'Mitake SMS'], ['title' => 'iTe2 Tech SMS']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'service_config', $siteServiceLanguage, $languageList, $startServiceId, false);

        DB::table('language_resource')->insert($languageResourceData);
    }

    protected function deleteSystemParameter()
    {
        $groupCodes = ['epaper_status', 'sms_status'];

        SeederHelper::deleteSystemParametersByGroupCode($groupCodes);
    }
}
