<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertAdministratorMenuNewsletterData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 建立預設資料
        $this->insertDatabase();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 刪除預設資料
        $this->deleteDatabase();
    }

    /**
     * Insert default data
     *
     * @return void
     */
    public function insertDatabase()
    {
        $timestamp = date('Y-m-d H:i:s');

        // 管理員選單 - 分類
        if ($menuClassId = DB::table('administrator_menu')->where('uri', 'root-module')->value('id')) {
            $administratorMenuData = [
                [
                    'id' => $menuParentId1 = uuidl(),
                    'title' => '電子報',
                    'uri' => 'control-epaper',
                    'controller' => null,
                    'model' => null,
                    'parent_id' => $menuClassId,
                    'link' => null,
                    'icon' => 'icon-newspaper',
                    'sort' => 208, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '發送管理',
                    'uri' => 'epaper-history',
                    'controller' => 'EpaperHistoryController',
                    'model' => 'EpaperHistory',
                    'parent_id' => $menuParentId1,
                    'link' => 'epaper-history',
                    'icon' => null,
                    'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '範本管理',
                    'uri' => 'epaper-template',
                    'controller' => 'EpaperTemplateController',
                    'model' => 'EpaperTemplate',
                    'parent_id' => $menuParentId1,
                    'link' => 'epaper-template',
                    'icon' => null,
                    'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '訂閱名單',
                    'uri' => 'epaper-subscriber',
                    'controller' => 'EpaperSubscriberController',
                    'model' => 'EpaperSubscriber',
                    'parent_id' => $menuParentId1,
                    'link' => 'epaper-subscriber',
                    'icon' => null,
                    'sort' => 3, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '電子報類別',
                    'uri' => 'epaper-category',
                    'controller' => 'EpaperCategoryController',
                    'model' => 'EpaperCategory',
                    'parent_id' => $menuParentId1,
                    'link' => 'epaper-category',
                    'icon' => null,
                    'sort' => 4, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],

                [
                    'id' => $menuParentId2 = uuidl(),
                    'title' => '簡訊發送',
                    'uri' => 'control-sms',
                    'controller' => null,
                    'model' => null,
                    'parent_id' => $menuClassId,
                    'link' => null,
                    'icon' => 'icon-whatsapp',
                    'sort' => 208, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '發送管理',
                    'uri' => 'sms-history',
                    'controller' => 'SmsHistoryController',
                    'model' => 'SmsHistory',
                    'parent_id' => $menuParentId2,
                    'link' => 'sms-history',
                    'icon' => null,
                    'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '範本管理',
                    'uri' => 'sms-template',
                    'controller' => 'SmsTemplateController',
                    'model' => 'SmsTemplate',
                    'parent_id' => $menuParentId2,
                    'link' => 'sms-template',
                    'icon' => null,
                    'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '訂閱名單',
                    'uri' => 'sms-subscriber',
                    'controller' => 'SmsSubscriberController',
                    'model' => 'SmsSubscriber',
                    'parent_id' => $menuParentId2,
                    'link' => 'sms-subscriber',
                    'icon' => null,
                    'sort' => 3, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '簡訊類別',
                    'uri' => 'sms-category',
                    'controller' => 'SmsCategoryController',
                    'model' => 'SmsCategory',
                    'parent_id' => $menuParentId2,
                    'link' => 'sms-category',
                    'icon' => null,
                    'sort' => 4, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
            ];
            DB::table('administrator_menu')->insert($administratorMenuData);
        }
    }

    public function deleteDatabase()
    {
        $uriSet = [
            'control-epaper',
            'epaper-history', 'epaper-template', 'epaper-subscriber', 'epaper-category',
            'sms-history', 'sms-template', 'sms-subscriber', 'sms-category'
        ];

        DB::table('administrator_menu')->whereIn('uri', $uriSet)->delete();
    }
}
