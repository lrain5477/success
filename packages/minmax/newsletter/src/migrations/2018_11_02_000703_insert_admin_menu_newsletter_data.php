<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class InsertAdminMenuNewsletterData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 建立預設資料
        $this->insertDatabase();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 刪除預設資料
        $this->deleteDatabase();
    }

    /**
     * Insert default data
     *
     * @return void
     */
    public function insertDatabase()
    {
        $timestamp = date('Y-m-d H:i:s');

        // 建立權限物件
        $permissionsData = [];
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'epaperHistory', '電子報紀錄', 281));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'epaperTemplate', '電子報範本', 282));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'epaperSubscriber', '電子報訂閱', 283));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'epaperCategory', '電子報類別', 284));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'smsHistory', '簡訊紀錄', 286));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'smsTemplate', '罐頭簡訊', 287));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'smsSubscriber', '簡訊訂閱', 288));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'smsCategory', '簡訊類別', 289));
        DB::table('permissions')->insert($permissionsData);

        // 管理員選單
        if ($menuClassId = DB::table('admin_menu')->where('uri', 'root-module')->value('id')) {
            $adminMenuData = [
                [
                    'id' => $menuParentId1 = uuidl(),
                    'title' => '電子報',
                    'uri' => 'control-epaper',
                    'controller' => null,
                    'model' => null,
                    'parent_id' => $menuClassId,
                    'link' => null,
                    'icon' => 'icon-newspaper',
                    'permission_key' => null,
                    'sort' => 208, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '發送管理',
                    'uri' => 'epaper-history',
                    'controller' => 'EpaperHistoryController',
                    'model' => 'EpaperHistory',
                    'parent_id' => $menuParentId1,
                    'link' => 'epaper-history',
                    'icon' => null,
                    'permission_key' => 'epaperHistoryShow',
                    'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '範本管理',
                    'uri' => 'epaper-template',
                    'controller' => 'EpaperTemplateController',
                    'model' => 'EpaperTemplate',
                    'parent_id' => $menuParentId1,
                    'link' => 'epaper-template',
                    'icon' => null,
                    'permission_key' => 'epaperTemplateShow',
                    'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '訂閱名單',
                    'uri' => 'epaper-subscriber',
                    'controller' => 'EpaperSubscriberController',
                    'model' => 'EpaperSubscriber',
                    'parent_id' => $menuParentId1,
                    'link' => 'epaper-subscriber',
                    'icon' => null,
                    'permission_key' => 'epaperSubscriberShow',
                    'sort' => 3, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '電子報類別',
                    'uri' => 'epaper-category',
                    'controller' => 'EpaperCategoryController',
                    'model' => 'EpaperCategory',
                    'parent_id' => $menuParentId1,
                    'link' => 'epaper-category',
                    'icon' => null,
                    'permission_key' => 'epaperCategoryShow',
                    'sort' => 4, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],

                [
                    'id' => $menuParentId2 = uuidl(),
                    'title' => '簡訊發送',
                    'uri' => 'control-sms',
                    'controller' => null,
                    'model' => null,
                    'parent_id' => $menuClassId,
                    'link' => null,
                    'icon' => 'icon-whatsapp',
                    'permission_key' => null,
                    'sort' => 208, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '發送管理',
                    'uri' => 'sms-history',
                    'controller' => 'SmsHistoryController',
                    'model' => 'SmsHistory',
                    'parent_id' => $menuParentId2,
                    'link' => 'sms-history',
                    'icon' => null,
                    'permission_key' => 'smsHistoryShow',
                    'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '範本管理',
                    'uri' => 'sms-template',
                    'controller' => 'SmsTemplateController',
                    'model' => 'SmsTemplate',
                    'parent_id' => $menuParentId2,
                    'link' => 'sms-template',
                    'icon' => null,
                    'permission_key' => 'smsTemplateShow',
                    'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '訂閱名單',
                    'uri' => 'sms-subscriber',
                    'controller' => 'SmsSubscriberController',
                    'model' => 'SmsSubscriber',
                    'parent_id' => $menuParentId2,
                    'link' => 'sms-subscriber',
                    'icon' => null,
                    'permission_key' => 'smsSubscriberShow',
                    'sort' => 3, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '簡訊類別',
                    'uri' => 'sms-category',
                    'controller' => 'SmsCategoryController',
                    'model' => 'SmsCategory',
                    'parent_id' => $menuParentId2,
                    'link' => 'sms-category',
                    'icon' => null,
                    'permission_key' => 'smsCategoryShow',
                    'sort' => 4, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
            ];
            DB::table('admin_menu')->insert($adminMenuData);
        }
    }

    public function deleteDatabase()
    {
        $uriSet = [
            'control-epaper',
            'epaper-history', 'epaper-template', 'epaper-subscriber', 'epaper-category',
            'sms-history', 'sms-template', 'sms-subscriber', 'sms-category'
        ];

        DB::table('admin_menu')->whereIn('uri', $uriSet)->delete();

        $permissionSet = [
            'epaperHistory', 'epaperTemplate', 'epaperSubscriber', 'epaperCategory',
            'smsHistory', 'smsTemplate', 'smsSubscriber', 'smsCategory'
        ];

        DB::table('permissions')->whereIn('group', $permissionSet)->delete();
    }
}
