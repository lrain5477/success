<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Models (Database Column) Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in backend platform page.
    |
    */

    'EpaperHistory' => [
        'id' => 'ID',
        'subject' => '信件主旨',
        'editor' => '信件內容',
        'objects' => [
            'languages' => '訂閱語言',
            'categories' => '訂閱類別',
            'roles' => '會員類別',
            'members' => '指定會員',
            'emails' => '指定信箱',
        ],
        'schedule_at' => '排程時間',
        'sent_at' => '發送時間',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'objects' => [
                'members' => '可設定會員帳號以寄送指定會員。請使用半形逗號 <code>,</code> 分隔多個帳號。',
                'emails' => '可設定指定電子郵件地址寄送。請使用半形逗號 <code>,</code> 分隔多個電子郵件。',
            ],
            'schedule_at' => '您可以指定時間進行此電子報發送。若不填寫則會是電子報送出當下時間。',
            'sent_at' => '電子報進行發送之時間，不表示所有信件皆於同一時間送達。',
        ],
        'templates' => '範本套用',
        'templates_button' => '套用',
    ],

    'EpaperTemplate' => [
        'id' => 'ID',
        'title' => '範例名稱',
        'subject' => '信件主旨',
        'editor' => '信件內容',
        'editable' => '可否編輯',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'editable' => '由設計師設計之電子報範本，應不可被客戶編輯。',
        ],
    ],

    'EpaperSubscriber' => [
        'id' => 'ID',
        'email' => '訂閱者',
        'member_id' => '關聯會員',
        'language_id' => '訂閱語系',
        'categories' => '訂閱類別',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'member_id' => '此訂閱者所屬之會員。請輸入會員帳號以進行關聯。',
        ],
    ],

    'EpaperCategory' => [
        'id' => 'ID',
        'title' => '標題',
        'details' => '詳細內容',
        'sort' => '排序',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
    ],

    'EpaperReceiver' => [
        'id' => 'ID',
        'history_id' => '電子報ID',
        'email' => 'Email',
        'name' => '姓名',
        'epaper_status' => '寄送狀態',
        'opened_at' => '開信時間',
        'clicked_at' => '點擊時間',
        'created_at' => '建立時間',
    ],

    'SmsHistory' => [
        'id' => 'ID',
        'subject' => '簡訊主旨',
        'content' => '簡訊內容',
        'objects' => [
            'languages' => '訂閱語言',
            'categories' => '訂閱類別',
            'roles' => '會員類別',
            'members' => '指定會員',
            'mobiles' => '指定電話',
        ],
        'schedule_at' => '排程時間',
        'sent_at' => '發送時間',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'content' => '簡訊的最大內容長度為 70 個字。',
            'objects' => [
                'members' => '可設定會員帳號以寄送指定會員。請使用半形逗號 <code>,</code> 分隔多個帳號。',
                'mobiles' => '可設定指定行動電話號碼寄送。請使用半形逗號 <code>,</code> 分隔多個電話。',
            ],
            'schedule_at' => '您可以指定時間進行此簡訊發送。若不填寫則會是簡訊送出當下時間。',
            'sent_at' => '簡訊進行發送之時間，不表示所有簡訊皆於同一時間送達。',
        ],
        'templates' => '範本套用',
        'templates_button' => '套用',
    ],

    'SmsTemplate' => [
        'id' => 'ID',
        'title' => '範例名稱',
        'subject' => '簡訊主旨',
        'content' => '簡訊內容',
        'editable' => '可否編輯',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'content' => '簡訊的最大內容長度為 70 個字。',
            'editable' => '由設計師設計之簡訊範本，應不可被客戶編輯。',
        ],
    ],

    'SmsSubscriber' => [
        'id' => 'ID',
        'mobile' => '訂閱者',
        'member_id' => '關聯會員',
        'language_id' => '訂閱語系',
        'categories' => '訂閱類別',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'member_id' => '此訂閱者所屬之會員。請輸入會員帳號以進行關聯。',
        ],
    ],

    'SmsCategory' => [
        'id' => 'ID',
        'title' => '標題',
        'details' => '詳細內容',
        'sort' => '排序',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
    ],

    'SmsReceiver' => [
        'id' => 'ID',
        'history_id' => '簡訊ID',
        'mobile' => '電話',
        'name' => '姓名',
        'sms_status' => '寄送狀態',
        'opened_at' => '讀取時間',
        'clicked_at' => '點擊時間',
        'created_at' => '建立時間',
    ],

];
