<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    'grid' => [
        'EpaperHistory' => [
            'result' => '發送統計',
            'track' => '追蹤統計',
            'amount' => '總人數',
            'processRate' => '完成度',
            'successRate' => '成功率',
            'opened' => '開信數',
            'openedRate' => '開信率',
            'clickedRate' => '轉換率',
        ],
        'EpaperCategory' => [
            'history_count' => '發報量',
            'subscriber_count' => '訂閱量',
        ],
        'SmsHistory' => [
            'result' => '發送統計',
            'track' => '追蹤統計',
            'amount' => '總人數',
            'processRate' => '完成度',
            'successRate' => '成功率',
            'opened' => '開信數',
            'openedRate' => '開信率',
            'clickedRate' => '轉換率',
        ],
        'SmsCategory' => [
            'history_count' => '發訊量',
            'subscriber_count' => '訂閱量',
        ],
    ],

    'form' => [
        'fieldSet' => [
            'objects' => '發送目標',
        ],
        'tab' => [
            'base' => '詳細內容',
            'receiver' => '收件者列表',
        ],
    ],

];
