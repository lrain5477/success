<?php

namespace Minmax\Newsletter\Services;

use SoapClient;

/**
 * Class SmsIteSender
 *
 * 詮力科技簡訊 (PHP need turn on SOAP extension)
 */
class SmsIteSender extends SmsSender
{
    /**
     * @var string $stop_time
     */
    protected $stop_time;

    /**
     * SmsIteSender constructor.
     */
    public function __construct()
    {
        if ($config = serviceConfig('sms-ite')) {
            $this->setConnection([
                'host' => $config->host,
                'username' => array_get($config->options, 'username'),
                'password' => array_get($config->options, 'password'),
                'stop_time' => array_get($config->options, 'stoptime'),
            ]);
        }
    }

    /**
     * Send sms via SoapClient.
     *
     * @param  string $phone
     * @param  string $content
     * @return string|null
     */
    public function send($phone, $content)
    {
        if (blank($phone) || blank($content)) return null;

        try {
            $client = new SoapClient($this->host);
            $result = $client->SendSMS([
                'DstAddr' => $phone,
                'text' => $content,
                'user' => $this->username,
                'pass' => $this->password,
                'ScheduleTime' => '',
                'stoptime' => $this->stop_time ?? '',
                'isLong' => mb_strlen($content) > 70
            ]);

            $result = json_decode(json_encode($result), true);
            $result = trim($result['SendSMSResult']);
            $xml = simplexml_load_string($result);
            $resultCode = $xml->ERR;

            return $resultCode;
        } catch (\Exception $e) {}

        return null;
    }
}
