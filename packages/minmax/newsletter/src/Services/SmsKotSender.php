<?php

namespace Minmax\Newsletter\Services;

use GuzzleHttp\Client;

/**
 * Class SmsKotSender
 *
 * 簡訊王
 */
class SmsKotSender extends SmsSender
{
    /**
     * @var string $reserve_at
     */
    protected $reserve_at;

    /**
     * @var string $stop_time
     */
    protected $stop_time;

    /**
     * @var string $response_url
     */
    protected $response_url;

    /**
     * SmsIteSender constructor.
     */
    public function __construct()
    {
        if ($config = serviceConfig('sms-kot')) {
            $this->setConnection([
                'host' => $config->host,
                'username' => array_get($config->options, 'username'),
                'password' => array_get($config->options, 'password'),
                'reserve_at' => array_get($config->options, 'dlvtime'),
                'stop_time' => array_get($config->options, 'vldtime'),
                'response_url' => array_get($config->options, 'response'),
            ]);
        }
    }

    /**
     * Send SMS.
     *
     * @param  string $phone
     * @param  string $content
     * @return string|null
     */
    public function send($phone, $content)
    {
        if (blank($phone) || blank($content)) return null;

        try {
            $client = new Client();
            $response = $client->get($this->host, [
                'query' => [
                    'username' => $this->username,
                    'password' => $this->password,
                    'dstaddr' => $phone,
                    'smbody' => mb_convert_encoding($content, 'BIG5', 'UTF-8'),
                    'dlvtime' => $this->reserve_at ?? '0',
                    'vldtime' => $this->stop_time ?? '0',
                    'response' => $this->response_url ?? null,
                ]
            ]);

            $result = $response->getBody()->getContents();

            return preg_match('/^kmsgid\=\-/i', $result) === false;
        } catch (\Exception $e) {}

        return null;
    }
}
