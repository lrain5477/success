<?php

namespace Minmax\Newsletter\Services;

/**
 * Abstract Class SmsSender
 */
abstract class SmsSender
{
    /**
     * @var string $host
     */
    protected $host;

    /**
     * @var string $username
     */
    protected $username;

    /**
     * @var string $password
     */
    protected $password;

    /**
     * Set connection parameters.
     *
     * @param $attributes
     */
    public function setConnection($attributes)
    {
        foreach ($attributes as $property => $attribute) {
            if (property_exists($this, $property)) {
                $this->{$property} = $attribute;
            }
        }
    }

    /**
     * Send SMS.
     *
     * @param  string $phone
     * @param  string $content
     * @return string|null
     */
    abstract public function send($phone, $content);
}
