<?php

namespace Minmax\Newsletter\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SmsTemplate
 * @property integer $id
 * @property string $title
 * @property string $subject
 * @property string $content
 * @property boolean $editable
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class SmsTemplate extends Model
{
    protected $table = 'sms_template';
    protected $guarded = [];
    protected $casts = [
        'active' => 'boolean',
    ];
}
