<?php

namespace Minmax\Newsletter\Models;

use Illuminate\Database\Eloquent\Model;
use Minmax\Base\Models\WorldLanguage;
use Minmax\Member\Models\Member;

/**
 * Class EpaperSubscriber
 * @property integer $id
 * @property string $email
 * @property string $member_id
 * @property integer $language_id
 * @property array $categories
 * @property boolean $active
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property Member $member
 * @property WorldLanguage $worldLanguage
 */
class EpaperSubscriber extends Model
{
    protected $table = 'epaper_subscriber';
    protected $guarded = [];
    protected $casts = [
        'categories' => 'array',
        'active' => 'boolean',
    ];

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id', 'id');
    }

    public function worldLanguage()
    {
        return $this->hasOne(WorldLanguage::class, 'id', 'language_id');
    }
}
