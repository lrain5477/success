<?php

namespace Minmax\Newsletter\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EpaperCategory
 * @property integer $id
 * @property string $title
 * @property array $details
 * @property integer $sort
 * @property boolean $active
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class EpaperCategory extends Model
{
    protected $table = 'epaper_category';
    protected $guarded = [];
    protected $casts = [
        'active' => 'boolean',
    ];

    public function getTitleAttribute()
    {
        return langDB($this->getAttributeFromArray('title'));
    }

    public function getDetailsAttribute()
    {
        return json_decode(langDB($this->getAttributeFromArray('details')), true);
    }
}
