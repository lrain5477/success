<?php

namespace Minmax\Newsletter\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EpaperTemplate
 * @property integer $id
 * @property string $title
 * @property string $subject
 * @property string $editor
 * @property boolean $editable
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class EpaperTemplate extends Model
{
    protected $table = 'epaper_template';
    protected $guarded = [];
    protected $casts = [
        'editable' => 'boolean',
    ];
}
