<?php

namespace Minmax\Newsletter\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SmsHistory
 * @property integer $id
 * @property string $subject
 * @property string $content
 * @property array $objects
 * @property \Illuminate\Support\Carbon $schedule_at
 * @property \Illuminate\Support\Carbon $sent_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \Illuminate\Database\Eloquent\Collection|SmsReceiver[] $smsReceivers
 * @property integer $sms_receivers_count
 */
class SmsHistory extends Model
{
    protected $table = 'sms_history';
    protected $guarded = [];
    protected $dates = ['schedule_at', 'sent_at', 'created_at', 'updated_at'];
    protected $casts = [
        'objects' => 'array',
    ];

    public function smsReceivers()
    {
        return $this->hasMany(SmsReceiver::class, 'history_id', 'id');
    }
}
