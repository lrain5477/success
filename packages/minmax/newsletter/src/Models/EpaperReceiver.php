<?php

namespace Minmax\Newsletter\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EpaperReceiver
 * @property integer $id
 * @property integer $history_id
 * @property string $email
 * @property string $name
 * @property string $epaper_status
 * @property \Illuminate\Support\Carbon $opened_at
 * @property \Illuminate\Support\Carbon $clicked_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property EpaperHistory $epaperHistory
 */
class EpaperReceiver extends Model
{
    protected $table = 'epaper_receiver';
    protected $guarded = [];
    protected $dates = ['opened_at', 'clicked_at', 'created_at'];

    const UPDATED_AT = null;

    public function epaperHistory()
    {
        return $this->belongsTo(EpaperHistory::class, 'history_id', 'id');
    }
}
