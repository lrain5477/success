<?php

namespace Minmax\Newsletter\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EpaperHistory
 * @property integer $id
 * @property string $subject
 * @property string $editor
 * @property array $objects
 * @property \Illuminate\Support\Carbon $schedule_at
 * @property \Illuminate\Support\Carbon $sent_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \Illuminate\Database\Eloquent\Collection|EpaperReceiver[] $epaperReceivers
 * @property integer $epaper_receivers_count
 */
class EpaperHistory extends Model
{
    protected $table = 'epaper_history';
    protected $guarded = [];
    protected $dates = ['schedule_at', 'sent_at', 'created_at', 'updated_at'];
    protected $casts = [
        'objects' => 'array',
    ];

    public function epaperReceivers()
    {
        return $this->hasMany(EpaperReceiver::class, 'history_id', 'id');
    }
}
