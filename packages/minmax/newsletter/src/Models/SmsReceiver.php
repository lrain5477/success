<?php

namespace Minmax\Newsletter\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SmsReceiver
 * @property integer $id
 * @property integer $history_id
 * @property string $mobile
 * @property string $name
 * @property string $sms_status
 * @property \Illuminate\Support\Carbon $opened_at
 * @property \Illuminate\Support\Carbon $clicked_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property SmsHistory $smsHistory
 */
class SmsReceiver extends Model
{
    protected $table = 'sms_receiver';
    protected $guarded = [];
    protected $dates = ['opened_at', 'clicked_at', 'created_at'];

    const UPDATED_AT = null;

    public function smsHistory()
    {
        return $this->belongsTo(SmsHistory::class, 'history_id', 'id');
    }
}
