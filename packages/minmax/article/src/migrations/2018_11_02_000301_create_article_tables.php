<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class CreateArticleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // ArticleCategory 內容類別管理
        Schema::create('article_category', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('uri')->nullable()->unique()->comment('自訂連結');
            $table->string('parent_id')->nullable()->comment('上層類別');
            $table->string('title')->comment('類別名稱');                           // language
            $table->string('details')->nullable()->comment('詳細內容');             // language {pic, topic, description, editor}
            $table->json('options')->nullable()->comment('類別設定');
            $table->string('seo')->nullable()->comment('SEO');                      // language {meta_description, meta_keywords}
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('editable')->default(true)->comment('可否編輯');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // ArticleNews 新聞稿
        Schema::create('article_news', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('uri')->nullable()->unique()->comment('自訂連結');
            $table->string('title')->comment('新聞標題');                        // language
            $table->string('details')->nullable()->comment('詳細內容');          // language {description, editor, pic}
            $table->datetime('start_at')->nullable()->comment('開始時間');
            $table->datetime('end_at')->nullable()->comment('結束時間');
            $table->string('seo')->nullable()->comment('SEO');                  // language {meta_description, meta_keywords}
            $table->json('tags')->nullable()->comment('關聯標籤');               // [1,2,3,...]
            $table->json('roles')->nullable()->comment('限定會員');              // [1,2,3,...]
            $table->json('properties')->nullable()->comment('綜合屬性');         // [top,index,...]
            $table->boolean('comment')->default(false)->comment('評論啟用');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // ArticlePage 靜態頁面
        Schema::create('article_page', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('uri')->nullable()->unique()->comment('自訂連結');
            $table->string('title')->comment('頁面標題');                        // language
            $table->string('details')->comment('頁面內容');                      // language {topic, description, editor, pic}
            $table->string('page_wrap')->comment('框架套用');
            $table->datetime('start_at')->nullable()->comment('開始時間');
            $table->datetime('end_at')->nullable()->comment('結束時間');
            $table->string('seo')->nullable()->comment('SEO');                  // language {meta_description, meta_keywords}
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // ArticleFaq 常見問答
        Schema::create('article_faq', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('uri')->nullable()->unique()->comment('自訂連結');
            $table->string('title')->comment('問答標題');                        // language
            $table->string('details')->comment('問答內容');                      // language {editor, pic}
            $table->datetime('start_at')->nullable()->comment('開始時間');
            $table->datetime('end_at')->nullable()->comment('結束時間');
            $table->string('seo')->nullable()->comment('SEO');                  // language {meta_description, meta_keywords}
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // ArticleDownload 檔案下載
        Schema::create('article_download', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('uri')->nullable()->unique()->comment('自訂連結');
            $table->string('title')->comment('檔案標題');                        // language
            $table->string('details')->comment('檔案內容');                      // language {description}
            $table->json('file')->nullable()->comment('檔案選擇');
            $table->datetime('start_at')->nullable()->comment('開始時間');
            $table->datetime('end_at')->nullable()->comment('結束時間');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // ArticleColumn 專欄文章
        Schema::create('article_column', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('uri')->nullable()->unique()->comment('自訂連結');
            $table->string('title')->comment('文章標題');                        // language
            $table->string('details')->comment('文章內容');                      // language {editor, pic}
            $table->datetime('start_at')->nullable()->comment('開始時間');
            $table->datetime('end_at')->nullable()->comment('結束時間');
            $table->string('seo')->nullable()->comment('SEO');                  // language {meta_description, meta_keywords}
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('top')->default(false)->comment('置頂顯示');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // ArticleLocation 營業據點
        Schema::create('article_location', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('title')->comment('標題名稱');                        // language
            $table->string('details')->nullable()->comment('據點內容');          // language {time, description, editor, pic}
            $table->string('contacts')->nullable()->comment('聯絡資訊');         // language {phone, fax, email, address}
            $table->json('tags')->nullable()->comment('關聯標籤');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // ArticleTeam 團隊介紹
        Schema::create('article_team', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('uri')->nullable()->unique()->comment('自訂連結');
            $table->string('title')->comment('成員名稱');                        // language
            $table->string('position')->comment('成員職稱');                     // language
            $table->string('details')->comment('成員內容');                      // language {editor}
            $table->json('pic')->nullable()->comment('成員圖片');
            $table->datetime('start_at')->nullable()->comment('開始時間');
            $table->datetime('end_at')->nullable()->comment('結束時間');
            $table->string('seo')->nullable()->comment('SEO');                  // language {meta_description, meta_keywords}
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('top')->default(false)->comment('置頂顯示');
            $table->boolean('active')->default(true)->comment('啟用狀態');
            $table->timestamps();
        });

        // article & category (Many-to-Many)
        Schema::create('article_category_relation', function (Blueprint $table) {
            $table->string('category_id');
            $table->string('object_id');
            $table->string('model');

            $table->unique(['category_id', 'object_id', 'model'], 'article_category_relation_all_column_unique');

            $table->foreign('category_id')->references('id')->on('article_category')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // ArticleTrack 點擊追蹤
        Schema::create('article_track', function (Blueprint $table) {
            $table->string('model')->index()->comment('Model');
            $table->string('object_id')->index()->comment('目標ID');
            $table->ipAddress('ip')->comment('IP位址');
            $table->date('click_at')->comment('點擊日期');
            $table->timestamp('created_at')->useCurrent()->comment('建立時間');

            $table->unique(['model', 'object_id', 'ip', 'click_at']);
        });

        // ArticleRelation 內容關聯 (many-to-many)
        Schema::create('article_relation', function (Blueprint $table) {
            $table->string('owner_model')->index()->comment('擁有者Model');
            $table->string('owner_id')->index()->comment('擁有者ID');
            $table->string('foreign_model')->index()->comment('項目Model');
            $table->string('foreign_id')->index()->comment('項目ID');

            $table->unique(['owner_model', 'owner_id', 'foreign_model', 'foreign_id'], 'article_relation_all_column_unique');
        });

        $this->insertSystemParameters();

        $this->insertArticleCategories();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->deleteSystemParameters();

        Schema::dropIfExists('article_relation');
        Schema::dropIfExists('article_track');
        Schema::dropIfExists('article_category_relation');
        Schema::dropIfExists('article_team');
        Schema::dropIfExists('article_location');
        Schema::dropIfExists('article_column');
        Schema::dropIfExists('article_download');
        Schema::dropIfExists('article_faq');
        Schema::dropIfExists('article_page');
        Schema::dropIfExists('article_news');
        Schema::dropIfExists('article_category');
    }

    public function insertSystemParameters()
    {
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $startGroupId = $rowGroupId = SeederHelper::getTableNextIncrement('system_parameter_group');
        $rowGroupId--;

        $systemGroupData = [
            ['code' => 'page_wrap', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
            ['code' => 'comment', 'title' => 'system_parameter_group.title.' . ++$rowGroupId],
        ];

        DB::table('system_parameter_group')->insert($systemGroupData);

        // 多語系
        $systemGroupLanguage = [
            'zh-Hant' => [
                ['title' => '框架套用'], ['title' => '留言開放']
            ],
            'zh-Hans' => [
                ['title' => '框架套用'], ['title' => '留言开放']
            ],
            'ja' => [
                ['title' => '鋳型応用'], ['title' => 'メッセージを開く']
            ],
            'en' => [
                ['title' => 'Wrap Template'], ['title' => 'Comment']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'system_parameter_group', $systemGroupLanguage, $languageList, $startGroupId, false);

        $startItemId = $rowItemId = SeederHelper::getTableNextIncrement('system_parameter_item');
        $rowItemId--;

        $systemItemData = [
            [
                'group_id' => $startGroupId,
                'value' => 'blank',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary', 'template' => 'MinmaxArticle::web.templates.blank']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId,
                'value' => 'default',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary', 'template' => 'MinmaxArticle::web.templates.default']),
                'sort' => 2,
            ],
            [
                'group_id' => $startGroupId + 1,
                'value' => '0',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'secondary']),
                'sort' => 1,
            ],
            [
                'group_id' => $startGroupId + 1,
                'value' => '1',
                'label' => 'system_parameter_item.label.' . ++$rowItemId,
                'options' => json_encode(['class' => 'alert']),
                'sort' => 2,
            ],
        ];

        DB::table('system_parameter_item')->insert($systemItemData);

        // 多語系
        $systemItemLanguage = [
            'zh-Hant' => [
                ['label' => '無 (完整頁面)'], ['label' => '預設版型'],
                ['label' => '停用'], ['label' => '啟用'],
            ],
            'zh-Hans' => [
                ['label' => '无 (完整页面)'], ['label' => '预设版型'],
                ['label' => '停用'], ['label' => '启用'],
            ],
            'ja' => [
                ['label' => 'なし (全ページ)'], ['label' => '既定鋳型'],
                ['label' => '無効'], ['label' => '有効'],
            ],
            'en' => [
                ['label' => 'Blank (Full Page)'], ['label' => 'Default'],
                ['label' => 'Off'], ['label' => 'On'],
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'system_parameter_item', $systemItemLanguage, $languageList, $startItemId, false);

        // 欄位擴充
        $startExtensionId = $extensionRowId = SeederHelper::getTableNextIncrement('column_extension');
        $extensionRowId--;
        $columnExtensionData = [
            ['table_name' => 'article_category', 'column_name' => 'details', 'sub_column_name' => 'topic', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText'])],
            ['table_name' => 'article_category', 'column_name' => 'details', 'sub_column_name' => 'description', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldTextarea'])],
            ['table_name' => 'article_category', 'column_name' => 'details', 'sub_column_name' => 'editor', 'sort' => 3, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'article_category', 'column_name' => 'details', 'sub_column_name' => 'pic', 'sort' => 4, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldMediaImage'])],
            ['table_name' => 'article_news', 'column_name' => 'details', 'sub_column_name' => 'description', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldTextarea'])],
            ['table_name' => 'article_news', 'column_name' => 'details', 'sub_column_name' => 'editor', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor', 'height' => '550px', 'stylesheet' => '/static/admin/editor/demo.css'])],
            ['table_name' => 'article_news', 'column_name' => 'details', 'sub_column_name' => 'pic', 'sort' => 3, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldMediaImage'])],
            ['table_name' => 'article_page', 'column_name' => 'details', 'sub_column_name' => 'editor', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'article_page', 'column_name' => 'details', 'sub_column_name' => 'pic', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldMediaImage'])],
            ['table_name' => 'article_faq', 'column_name' => 'details', 'sub_column_name' => 'editor', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'article_faq', 'column_name' => 'details', 'sub_column_name' => 'pic', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldMediaImage'])],
            ['table_name' => 'article_download', 'column_name' => 'details', 'sub_column_name' => 'description', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldTextarea'])],
            ['table_name' => 'article_column', 'column_name' => 'details', 'sub_column_name' => 'editor', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor', 'height' => '550px'])],
            ['table_name' => 'article_column', 'column_name' => 'details', 'sub_column_name' => 'pic', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldMediaImage'])],
            ['table_name' => 'article_location', 'column_name' => 'details', 'sub_column_name' => 'time', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText'])],
            ['table_name' => 'article_location', 'column_name' => 'details', 'sub_column_name' => 'description', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldTextarea'])],
            ['table_name' => 'article_location', 'column_name' => 'details', 'sub_column_name' => 'editor', 'sort' => 3, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
            ['table_name' => 'article_location', 'column_name' => 'details', 'sub_column_name' => 'pic', 'sort' => 4, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldMediaImage'])],
            ['table_name' => 'article_location', 'column_name' => 'contacts', 'sub_column_name' => 'phone', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldTextarea'])],
            ['table_name' => 'article_location', 'column_name' => 'contacts', 'sub_column_name' => 'fax', 'sort' => 2, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText'])],
            ['table_name' => 'article_location', 'column_name' => 'contacts', 'sub_column_name' => 'email', 'sort' => 3, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText'])],
            ['table_name' => 'article_location', 'column_name' => 'contacts', 'sub_column_name' => 'address', 'sort' => 4, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldText'])],
            ['table_name' => 'article_team', 'column_name' => 'details', 'sub_column_name' => 'editor', 'sort' => 1, 'active' => true,
                'title' => 'column_extension.title.' . ++$extensionRowId, 'options' => json_encode(['method' => 'getFieldEditor'])],
        ];

        DB::table('column_extension')->insert($columnExtensionData);

        // 多語系
        $columnExtensionLanguage = [
            ['title' => '標題'], ['title' => '內容文字'], ['title' => '自訂內容'], ['title' => '圖片'],
            ['title' => '簡短敘述'], ['title' => '新聞內容'], ['title' => '圖片'],
            ['title' => '頁面內容'], ['title' => '圖片'],
            ['title' => '問答內容'], ['title' => '圖片'],
            ['title' => '檔案敘述'],
            ['title' => '文章內容'], ['title' => '圖片'],
            ['title' => '營業時間'], ['title' => '簡短敘述'], ['title' => '詳細內容'], ['title' => '圖片'],
            ['title' => '連絡電話'], ['title' => '傳真號碼'], ['title' => '電子信箱'], ['title' => '地址'],
            ['title' => '詳細內容'],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'column_extension', $columnExtensionLanguage, $languageList, $startExtensionId);

        DB::table('language_resource')->insert($languageResourceData);
    }

    /**
     * Insert system parameters for this module.
     *
     * @return void
     */
    public function insertArticleCategories()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $articleCategoryData = [
            [
                'id' => $categoryId1 = uuidl(),
                'uri' => 'article-news',
                'parent_id' => null,
                'title' => "article_category.title.{$categoryId1}",
                'details' => "article_category.details.{$categoryId1}",
                'options' => json_encode(['relation' => 'articleNews', 'details' => 'pic']),
                'seo' => "article_category.seo.{$categoryId1}",
                'sort' => 1, 'editable' => false, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'id' => $categoryId2 = uuidl(),
                'uri' => 'article-page',
                'parent_id' => null,
                'title' => "article_category.title.{$categoryId2}",
                'details' => "article_category.details.{$categoryId2}",
                'options' => json_encode(['relation' => 'articlePages']),
                'seo' => "article_category.seo.{$categoryId2}",
                'sort' => 2, 'editable' => false, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'id' => $categoryId3 = uuidl(),
                'uri' => 'article-faq',
                'parent_id' => null,
                'title' => "article_category.title.{$categoryId3}",
                'details' => "article_category.details.{$categoryId3}",
                'options' => json_encode(['relation' => 'articleFaq']),
                'seo' => "article_category.seo.{$categoryId3}",
                'sort' => 3, 'editable' => false, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'id' => $categoryId4 = uuidl(),
                'uri' => 'article-download',
                'parent_id' => null,
                'title' => "article_category.title.{$categoryId4}",
                'details' => "article_category.details.{$categoryId4}",
                'options' => json_encode(['relation' => 'articleDownload']),
                'seo' => "article_category.seo.{$categoryId4}",
                'sort' => 4, 'editable' => false, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'id' => $categoryId5 = uuidl(),
                'uri' => 'article-column',
                'parent_id' => null,
                'title' => "article_category.title.{$categoryId5}",
                'details' => "article_category.details.{$categoryId5}",
                'options' => json_encode(['relation' => 'articleColumn']),
                'seo' => "article_category.seo.{$categoryId5}",
                'sort' => 5, 'editable' => false, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'id' => $categoryId6 = uuidl(),
                'uri' => 'article-location',
                'parent_id' => null,
                'title' => "article_category.title.{$categoryId6}",
                'details' => "article_category.details.{$categoryId6}",
                'options' => json_encode(['relation' => 'articleLocation']),
                'seo' => "article_category.seo.{$categoryId6}",
                'sort' => 6, 'editable' => false, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'id' => $categoryId7 = uuidl(),
                'uri' => 'article-team',
                'parent_id' => null,
                'title' => "article_category.title.{$categoryId7}",
                'details' => "article_category.details.{$categoryId7}",
                'options' => json_encode(['relation' => 'articleTeam']),
                'seo' => "article_category.seo.{$categoryId7}",
                'sort' => 7, 'editable' => false, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];

        DB::table('article_category')->insert($articleCategoryData);

        // 多語系
        $articleCategoryLanguage = [
            'zh-Hant' => [
                ['id' => $categoryId1, 'title' => '新聞稿'], ['id' => $categoryId2, 'title' => '靜態頁面'],
                ['id' => $categoryId3, 'title' => '常見問答'], ['id' => $categoryId4, 'title' => '檔案下載'],
                ['id' => $categoryId5, 'title' => '專欄文章'], ['id' => $categoryId6, 'title' => '營業據點'],
                ['id' => $categoryId7, 'title' => '團隊介紹'],
            ],
            'zh-Hans' => [
                ['id' => $categoryId1, 'title' => '新闻稿'], ['id' => $categoryId2, 'title' => '静态页面'],
                ['id' => $categoryId3, 'title' => '常见问答'], ['id' => $categoryId4, 'title' => '档案下载'],
                ['id' => $categoryId5, 'title' => '专栏文章'], ['id' => $categoryId6, 'title' => '营业据点'],
                ['id' => $categoryId7, 'title' => '团队介绍'],
            ],
            'ja' => [
                ['id' => $categoryId1, 'title' => '新聞記事'], ['id' => $categoryId2, 'title' => '静的ページ'],
                ['id' => $categoryId3, 'title' => 'よくある質問'], ['id' => $categoryId4, 'title' => 'ダウンロード'],
                ['id' => $categoryId5, 'title' => 'コラム'], ['id' => $categoryId6, 'title' => '事業の場所'],
                ['id' => $categoryId7, 'title' => 'チーム紹介'],
            ],
            'en' => [
                ['id' => $categoryId1, 'title' => 'News'], ['id' => $categoryId2, 'title' => 'Page'],
                ['id' => $categoryId3, 'title' => 'FAQ'], ['id' => $categoryId4, 'title' => 'Download'],
                ['id' => $categoryId5, 'title' => 'Columns'], ['id' => $categoryId6, 'title' => 'Locations'],
                ['id' => $categoryId7, 'title' => 'Team'],
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'article_category', $articleCategoryLanguage, $languageList, null, false);

        DB::table('language_resource')->insert($languageResourceData);
    }

    /**
     * Delete system parameters for this module.
     *
     * @return void
     */
    public function deleteSystemParameters()
    {
        $columnExtensionTableSet = ['article_category',
            'article_news', 'article_page', 'article_faq', 'article_download', 'article_column',
            'article_location', 'article_team',
        ];

        DB::table('column_extension')->whereIn('table_name', $columnExtensionTableSet)->delete();
    }
}
