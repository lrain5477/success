<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertAdministratorMenuArticleData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 建立預設資料
        $this->insertDatabase();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 刪除預設資料
        $this->deleteDatabase();
    }

    /**
     * Insert default data
     *
     * @return void
     */
    public function insertDatabase()
    {
        $timestamp = date('Y-m-d H:i:s');

        // 管理員選單 - 分類
        if ($menuClassId = DB::table('administrator_menu')->where('uri', 'root-module')->value('id')) {
            $administratorMenuData = [
                [
                    'id' => $menuParentId = uuidl(),
                    'title' => '內容管理',
                    'uri' => 'control-article',
                    'controller' => null,
                    'model' => null,
                    'parent_id' => $menuClassId,
                    'link' => null,
                    'icon' => 'icon-file-text',
                    'sort' => 201, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '內容類別管理',
                    'uri' => 'article-category',
                    'controller' => 'ArticleCategoryController',
                    'model' => 'ArticleCategory',
                    'parent_id' => $menuParentId,
                    'link' => 'article-category',
                    'icon' => null,
                    'sort' => 11, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
            ];
            DB::table('administrator_menu')->insert($administratorMenuData);
        }
    }

    public function deleteDatabase()
    {
        $uriSet = ['control-article', 'article-category'];

        DB::table('administrator_menu')->whereIn('uri', $uriSet)->delete();
    }
}
