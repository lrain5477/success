<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class InsertAdminMenuArticleData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 建立預設資料
        $this->insertDatabase();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 刪除預設資料
        $this->deleteDatabase();
    }

    /**
     * Insert default data
     *
     * @return void
     */
    public function insertDatabase()
    {
        $timestamp = date('Y-m-d H:i:s');

        // 建立權限物件
        $permissionsData = [];
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'articleNews', '新聞稿', 201));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'articlePage', '靜態頁面', 202));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'articleFaq', '常見問答', 203));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'articleDownload', '檔案下載', 204));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'articleColumn', '專欄文章', 205));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'articleLocation', '營業據點', 206));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'articleTeam', '團隊介紹', 207));
        $permissionsData = array_merge($permissionsData, SeederHelper::getPermissionArray('admin', 'articleCategory', '內容類別管理', 211));
        DB::table('permissions')->insert($permissionsData);

        // 管理員選單
        if ($menuClassId = DB::table('admin_menu')->where('uri', 'root-module')->value('id')) {
            $adminMenuData = [
                [
                    'id' => $menuParentId = uuidl(),
                    'title' => '內容管理',
                    'uri' => 'control-article',
                    'controller' => null,
                    'model' => null,
                    'parent_id' => $menuClassId,
                    'link' => null,
                    'icon' => 'icon-file-text',
                    'permission_key' => null,
                    'sort' => 201, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '新聞稿',
                    'uri' => 'article-news',
                    'controller' => 'ArticleNewsController',
                    'model' => 'ArticleNews',
                    'parent_id' => $menuParentId,
                    'link' => 'article-news',
                    'icon' => null,
                    'permission_key' => 'articleNewsShow',
                    'sort' => 1, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '靜態頁面',
                    'uri' => 'article-page',
                    'controller' => 'ArticlePageController',
                    'model' => 'ArticlePage',
                    'parent_id' => $menuParentId,
                    'link' => 'article-page',
                    'icon' => null,
                    'permission_key' => 'articlePageShow',
                    'sort' => 2, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '常見問答',
                    'uri' => 'article-faq',
                    'controller' => 'ArticleFaqController',
                    'model' => 'ArticleFaq',
                    'parent_id' => $menuParentId,
                    'link' => 'article-faq',
                    'icon' => null,
                    'permission_key' => 'articleFaqShow',
                    'sort' => 3, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '檔案下載',
                    'uri' => 'article-download',
                    'controller' => 'ArticleDownloadController',
                    'model' => 'ArticleDownload',
                    'parent_id' => $menuParentId,
                    'link' => 'article-download',
                    'icon' => null,
                    'permission_key' => 'articleDownloadShow',
                    'sort' => 4, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '專欄文章',
                    'uri' => 'article-column',
                    'controller' => 'ArticleColumnController',
                    'model' => 'ArticleColumn',
                    'parent_id' => $menuParentId,
                    'link' => 'article-column',
                    'icon' => null,
                    'permission_key' => 'articleColumnShow',
                    'sort' => 5, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '營業據點',
                    'uri' => 'article-location',
                    'controller' => 'ArticleLocationController',
                    'model' => 'ArticleLocation',
                    'parent_id' => $menuParentId,
                    'link' => 'article-location',
                    'icon' => null,
                    'permission_key' => 'articleLocationShow',
                    'sort' => 6, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '團隊介紹',
                    'uri' => 'article-team',
                    'controller' => 'ArticleTeamController',
                    'model' => 'ArticleTeam',
                    'parent_id' => $menuParentId,
                    'link' => 'article-team',
                    'icon' => null,
                    'permission_key' => 'articleTeamShow',
                    'sort' => 7, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
                [
                    'id' => uuidl(),
                    'title' => '內容類別管理',
                    'uri' => 'article-category',
                    'controller' => 'ArticleCategoryController',
                    'model' => 'ArticleCategory',
                    'parent_id' => $menuParentId,
                    'link' => 'article-category',
                    'icon' => null,
                    'permission_key' => 'articleCategoryShow',
                    'sort' => 11, 'updated_at' => $timestamp, 'created_at' => $timestamp
                ],
            ];
            DB::table('admin_menu')->insert($adminMenuData);
        }
    }

    public function deleteDatabase()
    {
        $uriSet = ['control-article', 'article-category',
            'article-news', 'article-page', 'article-faq', 'article-download', 'article-column',
            'article-location', 'article-team',
        ];

        DB::table('admin_menu')->whereIn('uri', $uriSet)->delete();

        $permissionSet = ['articleCategory',
            'articleNews', 'articlePage', 'articleFaq', 'articleDownload', 'articleColumn',
            'articleLocation', 'articleTeam',
        ];

        DB::table('permissions')->whereIn('group', $permissionSet)->delete();
    }
}
