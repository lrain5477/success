<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['web', 'localizationRedirect'],
    'as' => 'web.' . app()->getLocale() . '.'
], function() {

    Route::group(['namespace' => 'Minmax\Article\Web'], function () {

        Route::get('news/categories/{id}', 'NewsController@categories')->name('news.categories');

        Route::get('news/category/{id}', 'NewsController@category')->name('news.category');

        Route::get('news/article/{id}', 'NewsController@article')->name('news.article');

        Route::get('page/{id}', 'PageController@article')->name('page.article');

    });

});
