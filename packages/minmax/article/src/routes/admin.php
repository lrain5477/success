<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['admin', 'localizationRedirect'],
    'as' => 'admin.' . app()->getLocale() . '.'
], function() {

    Route::group(['prefix' => 'siteadmin', 'namespace' => 'Minmax\Article\Admin', 'middleware' => 'auth:admin'], function () {
        /*
         |--------------------------------------------------------------------------
         | 需要登入的路由。
         |--------------------------------------------------------------------------
         */

        /*
         * ArticleNews 新聞稿
         */
        Route::get('article-news', 'ArticleNewsController@index')->name('article-news.index');
        Route::post('article-news', 'ArticleNewsController@store')->name('article-news.store');
        Route::get('article-news/create', 'ArticleNewsController@create')->name('article-news.create');
        Route::get('article-news/{id}', 'ArticleNewsController@show')->name('article-news.show');
        Route::put('article-news/{id}', 'ArticleNewsController@update')->name('article-news.update');
        Route::delete('article-news/{id}', 'ArticleNewsController@destroy')->name('article-news.destroy');
        Route::get('article-news/{id}/edit', 'ArticleNewsController@edit')->name('article-news.edit');
        Route::post('article-news/ajax/datatables', 'ArticleNewsController@ajaxDataTable')->name('article-news.ajaxDataTable');
        Route::patch('article-news/ajax/switch', 'ArticleNewsController@ajaxSwitch')->name('article-news.ajaxSwitch');
        Route::patch('article-news/ajax/sort', 'ArticleNewsController@ajaxSort')->name('article-news.ajaxSort');
        Route::patch('article-news/ajax/multi/switch', 'ArticleNewsController@ajaxMultiSwitch')->name('article-news.ajaxMultiSwitch');
        Route::delete('article-news/ajax/multi/delete', 'ArticleNewsController@ajaxMultiDestroy')->name('article-news.ajaxMultiDestroy');

        /*
         * ArticlePage 靜態頁面
         */
        Route::get('article-page', 'ArticlePageController@index')->name('article-page.index');
        Route::post('article-page', 'ArticlePageController@store')->name('article-page.store');
        Route::get('article-page/create', 'ArticlePageController@create')->name('article-page.create');
        Route::get('article-page/{id}', 'ArticlePageController@show')->name('article-page.show');
        Route::put('article-page/{id}', 'ArticlePageController@update')->name('article-page.update');
        Route::delete('article-page/{id}', 'ArticlePageController@destroy')->name('article-page.destroy');
        Route::get('article-page/{id}/edit', 'ArticlePageController@edit')->name('article-page.edit');
        Route::post('article-page/ajax/datatables', 'ArticlePageController@ajaxDataTable')->name('article-page.ajaxDataTable');
        Route::patch('article-page/ajax/switch', 'ArticlePageController@ajaxSwitch')->name('article-page.ajaxSwitch');
        Route::patch('article-page/ajax/sort', 'ArticlePageController@ajaxSort')->name('article-page.ajaxSort');
        Route::patch('article-page/ajax/multi/switch', 'ArticlePageController@ajaxMultiSwitch')->name('article-page.ajaxMultiSwitch');
        Route::delete('article-page/ajax/multi/delete', 'ArticlePageController@ajaxMultiDestroy')->name('article-page.ajaxMultiDestroy');

        /*
         * ArticleFaq 常見問答
         */
        Route::get('article-faq', 'ArticleFaqController@index')->name('article-faq.index');
        Route::post('article-faq', 'ArticleFaqController@store')->name('article-faq.store');
        Route::get('article-faq/create', 'ArticleFaqController@create')->name('article-faq.create');
        Route::get('article-faq/{id}', 'ArticleFaqController@show')->name('article-faq.show');
        Route::put('article-faq/{id}', 'ArticleFaqController@update')->name('article-faq.update');
        Route::delete('article-faq/{id}', 'ArticleFaqController@destroy')->name('article-faq.destroy');
        Route::get('article-faq/{id}/edit', 'ArticleFaqController@edit')->name('article-faq.edit');
        Route::post('article-faq/ajax/datatables', 'ArticleFaqController@ajaxDataTable')->name('article-faq.ajaxDataTable');
        Route::patch('article-faq/ajax/switch', 'ArticleFaqController@ajaxSwitch')->name('article-faq.ajaxSwitch');
        Route::patch('article-faq/ajax/sort', 'ArticleFaqController@ajaxSort')->name('article-faq.ajaxSort');
        Route::patch('article-faq/ajax/multi/switch', 'ArticleFaqController@ajaxMultiSwitch')->name('article-faq.ajaxMultiSwitch');
        Route::delete('article-faq/ajax/multi/delete', 'ArticleFaqController@ajaxMultiDestroy')->name('article-faq.ajaxMultiDestroy');

        /*
         * ArticleDownload 檔案下載
         */
        Route::get('article-download', 'ArticleDownloadController@index')->name('article-download.index');
        Route::post('article-download', 'ArticleDownloadController@store')->name('article-download.store');
        Route::get('article-download/create', 'ArticleDownloadController@create')->name('article-download.create');
        Route::get('article-download/{id}', 'ArticleDownloadController@show')->name('article-download.show');
        Route::put('article-download/{id}', 'ArticleDownloadController@update')->name('article-download.update');
        Route::delete('article-download/{id}', 'ArticleDownloadController@destroy')->name('article-download.destroy');
        Route::get('article-download/{id}/edit', 'ArticleDownloadController@edit')->name('article-download.edit');
        Route::post('article-download/ajax/datatables', 'ArticleDownloadController@ajaxDataTable')->name('article-download.ajaxDataTable');
        Route::patch('article-download/ajax/switch', 'ArticleDownloadController@ajaxSwitch')->name('article-download.ajaxSwitch');
        Route::patch('article-download/ajax/sort', 'ArticleDownloadController@ajaxSort')->name('article-download.ajaxSort');
        Route::patch('article-download/ajax/multi/switch', 'ArticleDownloadController@ajaxMultiSwitch')->name('article-download.ajaxMultiSwitch');
        Route::delete('article-download/ajax/multi/delete', 'ArticleDownloadController@ajaxMultiDestroy')->name('article-download.ajaxMultiDestroy');

        /*
         * ArticleColumn 專欄文章
         */
        Route::get('article-column', 'ArticleColumnController@index')->name('article-column.index');
        Route::post('article-column', 'ArticleColumnController@store')->name('article-column.store');
        Route::get('article-column/create', 'ArticleColumnController@create')->name('article-column.create');
        Route::get('article-column/{id}', 'ArticleColumnController@show')->name('article-column.show');
        Route::put('article-column/{id}', 'ArticleColumnController@update')->name('article-column.update');
        Route::delete('article-column/{id}', 'ArticleColumnController@destroy')->name('article-column.destroy');
        Route::get('article-column/{id}/edit', 'ArticleColumnController@edit')->name('article-column.edit');
        Route::post('article-column/ajax/datatables', 'ArticleColumnController@ajaxDataTable')->name('article-column.ajaxDataTable');
        Route::patch('article-column/ajax/switch', 'ArticleColumnController@ajaxSwitch')->name('article-column.ajaxSwitch');
        Route::patch('article-column/ajax/sort', 'ArticleColumnController@ajaxSort')->name('article-column.ajaxSort');
        Route::patch('article-column/ajax/multi/switch', 'ArticleColumnController@ajaxMultiSwitch')->name('article-column.ajaxMultiSwitch');
        Route::delete('article-column/ajax/multi/delete', 'ArticleColumnController@ajaxMultiDestroy')->name('article-column.ajaxMultiDestroy');

        /*
         * ArticleLocation 營業據點
         */
        Route::get('article-location', 'ArticleLocationController@index')->name('article-location.index');
        Route::post('article-location', 'ArticleLocationController@store')->name('article-location.store');
        Route::get('article-location/create', 'ArticleLocationController@create')->name('article-location.create');
        Route::get('article-location/{id}', 'ArticleLocationController@show')->name('article-location.show');
        Route::put('article-location/{id}', 'ArticleLocationController@update')->name('article-location.update');
        Route::delete('article-location/{id}', 'ArticleLocationController@destroy')->name('article-location.destroy');
        Route::get('article-location/{id}/edit', 'ArticleLocationController@edit')->name('article-location.edit');
        Route::post('article-location/ajax/datatables', 'ArticleLocationController@ajaxDataTable')->name('article-location.ajaxDataTable');
        Route::patch('article-location/ajax/switch', 'ArticleLocationController@ajaxSwitch')->name('article-location.ajaxSwitch');
        Route::patch('article-location/ajax/sort', 'ArticleLocationController@ajaxSort')->name('article-location.ajaxSort');
        Route::patch('article-location/ajax/multi/switch', 'ArticleLocationController@ajaxMultiSwitch')->name('article-location.ajaxMultiSwitch');
        Route::delete('article-location/ajax/multi/delete', 'ArticleLocationController@ajaxMultiDestroy')->name('article-location.ajaxMultiDestroy');

        /*
         * ArticleTeam 團隊介紹
         */
        Route::get('article-team', 'ArticleTeamController@index')->name('article-team.index');
        Route::post('article-team', 'ArticleTeamController@store')->name('article-team.store');
        Route::get('article-team/create', 'ArticleTeamController@create')->name('article-team.create');
        Route::get('article-team/{id}', 'ArticleTeamController@show')->name('article-team.show');
        Route::put('article-team/{id}', 'ArticleTeamController@update')->name('article-team.update');
        Route::delete('article-team/{id}', 'ArticleTeamController@destroy')->name('article-team.destroy');
        Route::get('article-team/{id}/edit', 'ArticleTeamController@edit')->name('article-team.edit');
        Route::post('article-team/ajax/datatables', 'ArticleTeamController@ajaxDataTable')->name('article-team.ajaxDataTable');
        Route::patch('article-team/ajax/switch', 'ArticleTeamController@ajaxSwitch')->name('article-team.ajaxSwitch');
        Route::patch('article-team/ajax/sort', 'ArticleTeamController@ajaxSort')->name('article-team.ajaxSort');
        Route::patch('article-team/ajax/multi/switch', 'ArticleTeamController@ajaxMultiSwitch')->name('article-team.ajaxMultiSwitch');
        Route::delete('article-team/ajax/multi/delete', 'ArticleTeamController@ajaxMultiDestroy')->name('article-team.ajaxMultiDestroy');

        /*
         * ArticleCategory 內容類別管理
         */
        Route::get('article-category', 'ArticleCategoryController@index')->name('article-category.index');
        Route::post('article-category', 'ArticleCategoryController@store')->name('article-category.store');
        Route::get('article-category/create', 'ArticleCategoryController@create')->name('article-category.create');
        Route::get('article-category/{id}', 'ArticleCategoryController@show')->name('article-category.show');
        Route::put('article-category/{id}', 'ArticleCategoryController@update')->name('article-category.update');
        Route::delete('article-category/{id}', 'ArticleCategoryController@destroy')->name('article-category.destroy');
        Route::get('article-category/{id}/edit', 'ArticleCategoryController@edit')->name('article-category.edit');
        Route::post('article-category/ajax/datatables', 'ArticleCategoryController@ajaxDataTable')->name('article-category.ajaxDataTable');
        Route::patch('article-category/ajax/switch', 'ArticleCategoryController@ajaxSwitch')->name('article-category.ajaxSwitch');
        Route::patch('article-category/ajax/sort', 'ArticleCategoryController@ajaxSort')->name('article-category.ajaxSort');

    });

});
