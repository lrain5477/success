<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['administrator', 'localizationRedirect'],
    'as' => 'administrator.' . app()->getLocale() . '.'
], function() {

    Route::group(['prefix' => 'administrator', 'namespace' => 'Minmax\Article\Administrator', 'middleware' => 'auth:administrator'], function () {
        /*
         |--------------------------------------------------------------------------
         | 需要登入的路由。
         |--------------------------------------------------------------------------
         */

        /*
         * ArticleCategory 內容類別管理
         */
        Route::get('article-category', 'ArticleCategoryController@index')->name('article-category.index');
        Route::post('article-category', 'ArticleCategoryController@store')->name('article-category.store');
        Route::get('article-category/create', 'ArticleCategoryController@create')->name('article-category.create');
        Route::get('article-category/{id}', 'ArticleCategoryController@show')->name('article-category.show');
        Route::put('article-category/{id}', 'ArticleCategoryController@update')->name('article-category.update');
        Route::delete('article-category/{id}', 'ArticleCategoryController@destroy')->name('article-category.destroy');
        Route::get('article-category/{id}/edit', 'ArticleCategoryController@edit')->name('article-category.edit');
        Route::post('article-category/ajax/datatables', 'ArticleCategoryController@ajaxDataTable')->name('article-category.ajaxDataTable');
        Route::patch('article-category/ajax/switch', 'ArticleCategoryController@ajaxSwitch')->name('article-category.ajaxSwitch');
        Route::patch('article-category/ajax/sort', 'ArticleCategoryController@ajaxSort')->name('article-category.ajaxSort');

    });

});
