<?php

namespace Minmax\Article\Admin;

use Minmax\Base\Admin\Presenter;

/**
 * Class ArticleFaqPresenter
 */
class ArticleFaqPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxArticle::';

    protected $languageColumns = ['title', 'details', 'seo'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'categories' => (new ArticleCategoryRepository)->getArticleCategorySelection('article-faq'),
            'active' => systemParam('active'),
        ];
    }

    /**
     * @param  \Minmax\Article\Models\ArticleFaq $model
     * @return integer
     */
    public function getGridTitle($model)
    {
        $titleValue = $model->title;
        $shortValue = str_limit(trim(strip_tags(array_get($model->details ?? [], 'editor', ''))), 60);

        $urlValue = 'javascript:void(0);';
        if (in_array('R', $this->permissionSet)) {
            $urlValue = langRoute('admin.article-faq.show', ['id' => $model->id]);
        }
        if (in_array('U', $this->permissionSet)) {
            $urlValue = langRoute('admin.article-faq.edit', ['id' => $model->id]);
        }

        $categories = str_limit($model->articleCategories->pluck('title')->implode(', '), 20);

        $thisHtml = <<<HTML
<h3 class="h6 d-inline d-sm-block">
    <a class="text-pre-line" href="{$urlValue}">{$titleValue}</a>
</h3>
<p class="m-0 p-0 text-pre-line">{$shortValue}</p>
<small class="text-success float-right">{$categories}</small>
HTML;

        return $thisHtml;
    }
}
