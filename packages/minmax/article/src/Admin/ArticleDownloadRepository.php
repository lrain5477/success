<?php

namespace Minmax\Article\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Article\Models\ArticleDownload;

/**
 * Class ArticleDownloadRepository
 * @property ArticleDownload $model
 * @method ArticleDownload find($id)
 * @method ArticleDownload one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticleDownload[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticleDownload create($attributes)
 * @method ArticleDownload save($model, $attributes)
 * @method ArticleDownload|\Illuminate\Database\Eloquent\Builder query()
 */
class ArticleDownloadRepository extends Repository
{
    const MODEL = ArticleDownload::class;

    protected $sort = 'sort';

    protected $sorting = true;

    protected $languageColumns = ['title', 'details'];

    protected $categories = null;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'article_download';
    }

    protected function beforeCreate()
    {
        $this->categories = array_pull($this->attributes, 'categories', []);
    }

    protected function afterCreate()
    {
        $this->model->articleCategories()->sync(
            collect($this->categories)
                ->mapWithKeys(function ($item) {
                    return [$item => ['model' => get_class($this->model)]];
                })
                ->toArray()
        );
        $this->categories = null;
    }

    protected function beforeSave()
    {
        if (count($this->attributes) > 1) {
            $this->categories = array_pull($this->attributes, 'categories', []) ?? [];
        }
    }

    protected function afterSave()
    {
        if (! is_null($this->categories)) {
            $this->model->articleCategories()->sync(
                collect($this->categories)
                    ->mapWithKeys(function ($item) {
                        return [$item => ['model' => get_class($this->model)]];
                    })
                    ->toArray()
            );
            $this->categories = null;
        }
    }
}
