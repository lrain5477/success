<?php

namespace Minmax\Article\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class ArticleDownloadRequest
 */
class ArticleDownloadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('articleDownloadEdit');
            case 'POST':
                return $this->user('admin')->can('articleDownloadCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'ArticleDownload.uri' => [
                        'nullable',
                        'string',
                        Rule::unique('article_download', 'uri')->ignore($this->route('id'))
                    ],
                    'ArticleDownload.categories' => 'nullable|array',
                    'ArticleDownload.title' => 'required|string',
                    'ArticleDownload.details' => 'nullable|array',
                    'ArticleDownload.file' => 'required|array',
                    'ArticleDownload.start_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleDownload.end_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleDownload.sort' => 'required|integer',
                    'ArticleDownload.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'ArticleDownload.uri' => 'nullable|string|unique:article_faq,uri',
                    'ArticleDownload.categories' => 'nullable|array',
                    'ArticleDownload.title' => 'required|string',
                    'ArticleDownload.details' => 'nullable|array',
                    'ArticleDownload.file' => 'required|array',
                    'ArticleDownload.start_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleDownload.end_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleDownload.sort' => 'nullable|integer',
                    'ArticleDownload.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'ArticleDownload.uri' => __('MinmaxArticle::models.ArticleDownload.uri'),
            'ArticleDownload.categories' => __('MinmaxArticle::models.ArticleDownload.categories'),
            'ArticleDownload.title' => __('MinmaxArticle::models.ArticleDownload.title'),
            'ArticleDownload.details' => __('MinmaxArticle::models.ArticleDownload.details'),
            'ArticleDownload.file' => __('MinmaxArticle::models.ArticleDownload.file'),
            'ArticleDownload.start_at' => __('MinmaxArticle::models.ArticleDownload.start_at'),
            'ArticleDownload.end_at' => __('MinmaxArticle::models.ArticleDownload.end_at'),
            'ArticleDownload.sort' => __('MinmaxArticle::models.ArticleDownload.sort'),
            'ArticleDownload.active' => __('MinmaxArticle::models.ArticleDownload.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
