<?php

namespace Minmax\Article\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Article\Models\ArticleTeam;

/**
 * Class ArticleTeamTransformer
 */
class ArticleTeamTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'articleTeamShow',
        'U' => 'articleTeamEdit',
        'D' => 'articleTeamDestroy',
    ];

    /**
     * Transformer constructor.
     * @param  ArticleTeamPresenter $presenter
     * @param  string $uri
     */
    public function __construct(ArticleTeamPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  ArticleTeam $model
     * @return array
     * @throws \Throwable
     */
    public function transform(ArticleTeam $model)
    {
        return [
            'id' => $this->presenter->getGridCheckBox($model),
            'pic' => $this->presenter->getGridThumbnail($model, 'pic'),
            'top' => $this->presenter->getGridTitle($model),
            'article_tracks_count' => $this->presenter->getGridText($model, 'article_tracks_count'),
            'start_at' => $this->presenter->getPureString(is_null($model->start_at) ? $model->created_at->format('Y-m-d') : $model->start_at->format('Y-m-d')),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
