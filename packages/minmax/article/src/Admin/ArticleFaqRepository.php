<?php

namespace Minmax\Article\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Article\Models\ArticleFaq;

/**
 * Class ArticleFaqRepository
 * @property ArticleFaq $model
 * @method ArticleFaq find($id)
 * @method ArticleFaq one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticleFaq[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticleFaq create($attributes)
 * @method ArticleFaq save($model, $attributes)
 * @method ArticleFaq|\Illuminate\Database\Eloquent\Builder query()
 */
class ArticleFaqRepository extends Repository
{
    const MODEL = ArticleFaq::class;

    protected $sort = 'sort';

    protected $sorting = true;

    protected $languageColumns = ['title', 'details', 'seo'];

    protected $categories = null;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'article_faq';
    }

    protected function beforeCreate()
    {
        $this->categories = array_pull($this->attributes, 'categories', []);
    }

    protected function afterCreate()
    {
        $this->model->articleCategories()->sync(
            collect($this->categories)
                ->mapWithKeys(function ($item) {
                    return [$item => ['model' => get_class($this->model)]];
                })
                ->toArray()
        );
        $this->categories = null;
    }

    protected function beforeSave()
    {
        if (count($this->attributes) > 1) {
            $this->categories = array_pull($this->attributes, 'categories', []) ?? [];
        }
    }

    protected function afterSave()
    {
        if (! is_null($this->categories)) {
            $this->model->articleCategories()->sync(
                collect($this->categories)
                    ->mapWithKeys(function ($item) {
                        return [$item => ['model' => get_class($this->model)]];
                    })
                    ->toArray()
            );
            $this->categories = null;
        }
    }
}
