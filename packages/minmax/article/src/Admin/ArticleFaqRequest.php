<?php

namespace Minmax\Article\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class ArticleFaqRequest
 */
class ArticleFaqRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('articleFaqEdit');
            case 'POST':
                return $this->user('admin')->can('articleFaqCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'ArticleFaq.uri' => [
                        'nullable',
                        'string',
                        Rule::unique('article_faq', 'uri')->ignore($this->route('id'))
                    ],
                    'ArticleFaq.categories' => 'nullable|array',
                    'ArticleFaq.title' => 'required|string',
                    'ArticleFaq.details' => 'nullable|array',
                    'ArticleFaq.start_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleFaq.end_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleFaq.sort' => 'required|integer',
                    'ArticleFaq.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'ArticleFaq.uri' => 'nullable|string|unique:article_faq,uri',
                    'ArticleFaq.categories' => 'nullable|array',
                    'ArticleFaq.title' => 'required|string',
                    'ArticleFaq.details' => 'nullable|array',
                    'ArticleFaq.start_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleFaq.end_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleFaq.sort' => 'nullable|integer',
                    'ArticleFaq.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'ArticleFaq.uri' => __('MinmaxArticle::models.ArticleFaq.uri'),
            'ArticleFaq.categories' => __('MinmaxArticle::models.ArticleFaq.categories'),
            'ArticleFaq.title' => __('MinmaxArticle::models.ArticleFaq.title'),
            'ArticleFaq.details' => __('MinmaxArticle::models.ArticleFaq.details'),
            'ArticleFaq.start_at' => __('MinmaxArticle::models.ArticleFaq.start_at'),
            'ArticleFaq.end_at' => __('MinmaxArticle::models.ArticleFaq.end_at'),
            'ArticleFaq.sort' => __('MinmaxArticle::models.ArticleFaq.sort'),
            'ArticleFaq.active' => __('MinmaxArticle::models.ArticleFaq.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
