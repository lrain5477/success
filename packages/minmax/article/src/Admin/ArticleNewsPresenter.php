<?php

namespace Minmax\Article\Admin;

use Minmax\Base\Admin\Presenter;
use Minmax\Base\Admin\RoleRepository;

/**
 * Class ArticleNewsPresenter
 */
class ArticleNewsPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxArticle::';

    protected $languageColumns = ['title', 'details', 'seo'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'categories' => (new ArticleCategoryRepository)->getArticleCategorySelection('article-news'),
            'tags' => siteParam('tags'),
            'roles' => (new RoleRepository)->getSelectParameters('web'),
            'properties' => siteParam('property'),
            'comment' => systemParam('comment'),
            'active' => systemParam('active'),
        ];
    }

    /**
     * @param  \Minmax\Article\Models\ArticleNews $model
     * @return integer
     */
    public function getGridTitle($model)
    {
        $titleValue = $model->title;
        $shortValue = str_limit(trim(strip_tags(array_get($model->details ?? [], 'editor', ''))), 60);

        $urlValue = 'javascript:void(0);';
        if (in_array('R', $this->permissionSet)) {
            $urlValue = langRoute('admin.article-news.show', ['id' => $model->id]);
        }
        if (in_array('U', $this->permissionSet)) {
            $urlValue = langRoute('admin.article-news.edit', ['id' => $model->id]);
        }

        $top = in_array('top', $model->properties ?? [])
            ? '<span class="badge badge-pill badge-warning ml-2">' . __('MinmaxArticle::admin.grid.ArticleNews.tags.top') . '</span>'
            : '';

        $categories = str_limit($model->articleCategories->pluck('title')->implode(', '), 20);

        $thisHtml = <<<HTML
<h3 class="h6 d-inline d-sm-block">
    <a class="text-pre-line" href="{$urlValue}">{$titleValue}</a>{$top}
</h3>
<p class="m-0 p-0 text-pre-line">{$shortValue}</p>
<small class="text-success float-right">{$categories}</small>
HTML;

        return $thisHtml;
    }

    /**
     * @param  \Minmax\Article\Models\ArticleNews $model
     * @param  array $options
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFieldTagsInput($model, $options = [])
    {
        $componentData = [
            'id' => 'ArticleNews-tags',
            'label' => __('MinmaxArticle::models.ArticleNews.tags'),
            'name' => 'ArticleNews[tags][]',
            'values' => $model->tags ?? [],
            'hint' => __('MinmaxArticle::models.ArticleNews.hint.tags', ['link' => langRoute('admin.site-parameter-item.index')]),
            'listData' => array_get($this->parameterSet, 'tags', []),
        ];

        return view('MinmaxArticle::admin.article-news.form-tags-input', $componentData);
    }
}
