<?php

namespace Minmax\Article\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class ArticlePageRequest
 */
class ArticlePageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('articlePageEdit');
            case 'POST':
                return $this->user('admin')->can('articlePageCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'ArticlePage.uri' => [
                        'nullable',
                        'string',
                        Rule::unique('article_page', 'uri')->ignore($this->route('id'))
                    ],
                    'ArticlePage.title' => 'required|string',
                    'ArticlePage.details' => 'nullable|array',
                    'ArticlePage.page_wrap' => 'required|string',
                    'ArticlePage.start_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticlePage.end_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticlePage.seo' => 'nullable|array',
                    'ArticlePage.sort' => 'required|integer',
                    'ArticlePage.active' => 'required|in:1,0',
                ];
            case 'POST':
            default:
                return [
                    'ArticlePage.uri' => 'nullable|string|unique:article_page,uri',
                    'ArticlePage.title' => 'required|string',
                    'ArticlePage.details' => 'nullable|array',
                    'ArticlePage.page_wrap' => 'required|string',
                    'ArticlePage.start_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticlePage.end_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticlePage.seo' => 'nullable|array',
                    'ArticlePage.sort' => 'nullable|integer',
                    'ArticlePage.active' => 'required|in:1,0',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'ArticlePage.uri' => __('MinmaxArticle::models.ArticlePage.uri'),
            'ArticlePage.title' => __('MinmaxArticle::models.ArticlePage.title'),
            'ArticlePage.details' => __('MinmaxArticle::models.ArticlePage.details'),
            'ArticlePage.page_wrap' => __('MinmaxArticle::models.ArticlePage.page_wrap'),
            'ArticlePage.start_at' => __('MinmaxArticle::models.ArticlePage.start_at'),
            'ArticlePage.end_at' => __('MinmaxArticle::models.ArticlePage.end_at'),
            'ArticlePage.seo' => __('MinmaxArticle::models.ArticlePage.seo'),
            'ArticlePage.sort' => __('MinmaxArticle::models.ArticlePage.sort'),
            'ArticlePage.active' => __('MinmaxArticle::models.ArticlePage.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
