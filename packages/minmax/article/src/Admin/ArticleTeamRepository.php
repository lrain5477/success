<?php

namespace Minmax\Article\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Article\Models\ArticleTeam;

/**
 * Class ArticleTeamRepository
 * @property ArticleTeam $model
 * @method ArticleTeam find($id)
 * @method ArticleTeam one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticleTeam[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticleTeam create($attributes)
 * @method ArticleTeam save($model, $attributes)
 * @method ArticleTeam|\Illuminate\Database\Eloquent\Builder query()
 */
class ArticleTeamRepository extends Repository
{
    const MODEL = ArticleTeam::class;

    protected $sort = 'sort';

    protected $languageColumns = ['title', 'position', 'details', 'seo'];

    protected $categories = null;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'article_team';
    }

    protected function beforeCreate()
    {
        $this->categories = array_wrap(array_pull($this->attributes, 'category', []));
    }

    protected function afterCreate()
    {
        $this->model->articleCategories()->sync(
            collect($this->categories)
                ->mapWithKeys(function ($item) {
                    return [$item => ['model' => get_class($this->model)]];
                })
                ->toArray()
        );
        $this->categories = null;
    }

    protected function beforeSave()
    {
        if (count($this->attributes) > 1) {
            $this->categories = array_wrap(array_pull($this->attributes, 'category', []) ?? []);
        }
    }

    protected function afterSave()
    {
        if (! is_null($this->categories)) {
            $this->model->articleCategories()->sync(
                collect($this->categories)
                    ->mapWithKeys(function ($item) {
                        return [$item => ['model' => get_class($this->model)]];
                    })
                    ->toArray()
            );
            $this->categories = null;
        }
    }
}
