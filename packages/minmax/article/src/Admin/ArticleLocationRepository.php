<?php

namespace Minmax\Article\Admin;

use Exception;
use Minmax\Base\Admin\Repository;
use Minmax\Article\Models\ArticleLocation;
use Minmax\Base\Admin\SiteParameterGroupRepository;
use Minmax\Base\Admin\SiteParameterItemRepository;

/**
 * Class ArticleLocationRepository
 * @property ArticleLocation $model
 * @method ArticleLocation find($id)
 * @method ArticleLocation one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticleLocation[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticleLocation create($attributes)
 * @method ArticleLocation save($model, $attributes)
 * @method ArticleLocation|\Illuminate\Database\Eloquent\Builder query()
 */
class ArticleLocationRepository extends Repository
{
    const MODEL = ArticleLocation::class;

    protected $sort = 'sort';

    protected $sorting = true;

    protected $languageColumns = ['title', 'details', 'contacts'];

    protected $categories = null;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'article_location';
    }

    protected function beforeCreate()
    {
        $this->categories = array_pull($this->attributes, 'categories', []);

        if ($tags = array_get($this->attributes, 'tags')) {
            if (count($tags) > 0) {
                $allTag = collect(siteParam('tags'))
                    ->map(function ($item) {
                        return array_get($item, 'title');
                    })
                    ->toArray();
                $tagList = [];
                foreach ($tags as $tagLabel) {
                    if (in_array($tagLabel, $allTag)) {
                        $tagList[] = array_search($tagLabel, $allTag);
                    } else {
                        try {
                            $paramGroup = (new SiteParameterGroupRepository)->one('code', 'tags');
                            $tagData = (new SiteParameterItemRepository)->create(['group_id' => $paramGroup->id, 'label' => $tagLabel]);
                            $tagList[] = $tagData->id;
                        } catch (Exception $e) {}
                    }
                }
                array_set($this->attributes, 'tags', $tagList);
            }
        }
    }

    protected function afterCreate()
    {
        $this->model->articleCategories()->sync(
            collect($this->categories)
                ->mapWithKeys(function ($item) {
                    return [$item => ['model' => get_class($this->model)]];
                })
                ->toArray()
        );
        $this->categories = null;
    }

    protected function beforeSave()
    {
        if (count($this->attributes) > 1) {
            $this->categories = array_pull($this->attributes, 'categories', []) ?? [];
        }

        if ($tags = array_get($this->attributes, 'tags')) {
            if (count($tags) > 0) {
                $allTag = collect(siteParam('tags'))
                    ->map(function ($item) {
                        return array_get($item, 'title');
                    })
                    ->toArray();
                $tagList = [];
                foreach ($tags as $tagLabel) {
                    if (in_array($tagLabel, $allTag)) {
                        $tagList[] = array_search($tagLabel, $allTag);
                    } else {
                        try {
                            $paramGroup = (new SiteParameterGroupRepository)->one('code', 'tags');
                            $tagData = (new SiteParameterItemRepository)->create(['group_id' => $paramGroup->id, 'label' => $tagLabel]);
                            $tagList[] = $tagData->id;
                        } catch (Exception $e) {}
                    }
                }
                array_set($this->attributes, 'tags', $tagList);
            }
        }
    }

    protected function afterSave()
    {
        if (! is_null($this->categories)) {
            $this->model->articleCategories()->sync(
                collect($this->categories)
                    ->mapWithKeys(function ($item) {
                        return [$item => ['model' => get_class($this->model)]];
                    })
                    ->toArray()
            );
            $this->categories = null;
        }
    }
}
