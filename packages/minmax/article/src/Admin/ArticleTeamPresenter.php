<?php

namespace Minmax\Article\Admin;

use Minmax\Base\Admin\Presenter;

/**
 * Class ArticleTeamPresenter
 */
class ArticleTeamPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxArticle::';

    protected $languageColumns = ['title', 'position', 'details', 'seo'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'category' => (new ArticleCategoryRepository)->getArticleCategorySelection('article-team'),
            'top' => systemParam('top'),
            'active' => systemParam('active'),
        ];
    }

    /**
     * @param  \Minmax\Article\Models\ArticleTeam $model
     * @return integer
     */
    public function getGridTitle($model)
    {
        $titleValue = $model->title;
        $positionValue = $model->position;

        $urlValue = 'javascript:void(0);';
        if (in_array('R', $this->permissionSet)) {
            $urlValue = langRoute('admin.article-team.show', ['id' => $model->id]);
        }
        if (in_array('U', $this->permissionSet)) {
            $urlValue = langRoute('admin.article-team.edit', ['id' => $model->id]);
        }

        $top = $model->top
            ? '<span class="badge badge-pill badge-warning ml-2">' . __('MinmaxArticle::admin.grid.ArticleTeam.tags.top') . '</span>'
            : '';

        $category = $model->articleCategories->pluck('title')->implode(', ');

        $thisHtml = <<<HTML
<h3 class="h6 d-inline d-sm-block">
    <a class="text-pre-line" href="{$urlValue}">{$titleValue}</a>{$top}
</h3>
<p class="m-0 p-0 text-pre-line"><small>{$positionValue}</small></p>
<small class="text-success float-right">{$category}</small>
HTML;

        return $thisHtml;
    }
}
