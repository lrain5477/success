<?php

namespace Minmax\Article\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Article\Models\ArticleColumn;

/**
 * Class ArticleColumnRepository
 * @property ArticleColumn $model
 * @method ArticleColumn find($id)
 * @method ArticleColumn one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticleColumn[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticleColumn create($attributes)
 * @method ArticleColumn save($model, $attributes)
 * @method ArticleColumn|\Illuminate\Database\Eloquent\Builder query()
 */
class ArticleColumnRepository extends Repository
{
    const MODEL = ArticleColumn::class;

    protected $sort = 'sort';

    protected $sorting = true;

    protected $languageColumns = ['title', 'details', 'seo'];

    protected $categories = null;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'article_column';
    }

    protected function beforeCreate()
    {
        $this->categories = array_pull($this->attributes, 'categories', []);
    }

    protected function afterCreate()
    {
        $this->model->articleCategories()->sync(
            collect($this->categories)
                ->mapWithKeys(function ($item) {
                    return [$item => ['model' => get_class($this->model)]];
                })
                ->toArray()
        );
        $this->categories = null;
    }

    protected function beforeSave()
    {
        if (count($this->attributes) > 1) {
            $this->categories = array_pull($this->attributes, 'categories', []) ?? [];
        }
    }

    protected function afterSave()
    {
        if (! is_null($this->categories)) {
            $this->model->articleCategories()->sync(
                collect($this->categories)
                    ->mapWithKeys(function ($item) {
                        return [$item => ['model' => get_class($this->model)]];
                    })
                    ->toArray()
            );
            $this->categories = null;
        }
    }
}
