<?php

namespace Minmax\Article\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\Article\Models\ArticlePage;

/**
 * Class ArticlePageRepository
 * @property ArticlePage $model
 * @method ArticlePage find($id)
 * @method ArticlePage one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticlePage[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticlePage create($attributes)
 * @method ArticlePage save($model, $attributes)
 * @method ArticlePage|\Illuminate\Database\Eloquent\Builder query()
 */
class ArticlePageRepository extends Repository
{
    const MODEL = ArticlePage::class;

    protected $sort = 'sort';

    protected $languageColumns = ['title', 'details', 'seo'];

    protected $categories = null;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'article_page';
    }

    protected function beforeCreate()
    {
        $this->categories = array_wrap(array_pull($this->attributes, 'category', []));
    }

    protected function afterCreate()
    {
        $this->model->articleCategories()->sync(
            collect($this->categories)
                ->mapWithKeys(function ($item) {
                    return [$item => ['model' => get_class($this->model)]];
                })
                ->toArray()
        );
        $this->categories = null;
    }

    protected function beforeSave()
    {
        if (count($this->attributes) > 1) {
            $this->categories = array_wrap(array_pull($this->attributes, 'category', []) ?? []);
        }
    }

    protected function afterSave()
    {
        if (! is_null($this->categories)) {
            $this->model->articleCategories()->sync(
                collect($this->categories)
                    ->mapWithKeys(function ($item) {
                        return [$item => ['model' => get_class($this->model)]];
                    })
                    ->toArray()
            );
            $this->categories = null;
        }
    }
}
