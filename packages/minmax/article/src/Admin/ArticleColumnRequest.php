<?php

namespace Minmax\Article\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class ArticleColumnRequest
 */
class ArticleColumnRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('articleColumnEdit');
            case 'POST':
                return $this->user('admin')->can('articleColumnCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'ArticleColumn.uri' => [
                        'nullable',
                        'string',
                        Rule::unique('article_faq', 'uri')->ignore($this->route('id'))
                    ],
                    'ArticleColumn.categories' => 'nullable|array',
                    'ArticleColumn.title' => 'required|string',
                    'ArticleColumn.details' => 'nullable|array',
                    'ArticleColumn.start_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleColumn.end_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleColumn.sort' => 'required|integer',
                    'ArticleColumn.top' => 'required|boolean',
                    'ArticleColumn.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'ArticleColumn.uri' => 'nullable|string|unique:article_faq,uri',
                    'ArticleColumn.categories' => 'nullable|array',
                    'ArticleColumn.title' => 'required|string',
                    'ArticleColumn.details' => 'nullable|array',
                    'ArticleColumn.start_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleColumn.end_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleColumn.sort' => 'nullable|integer',
                    'ArticleColumn.top' => 'required|boolean',
                    'ArticleColumn.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'ArticleColumn.uri' => __('MinmaxArticle::models.ArticleColumn.uri'),
            'ArticleColumn.categories' => __('MinmaxArticle::models.ArticleColumn.categories'),
            'ArticleColumn.title' => __('MinmaxArticle::models.ArticleColumn.title'),
            'ArticleColumn.details' => __('MinmaxArticle::models.ArticleColumn.details'),
            'ArticleColumn.start_at' => __('MinmaxArticle::models.ArticleColumn.start_at'),
            'ArticleColumn.end_at' => __('MinmaxArticle::models.ArticleColumn.end_at'),
            'ArticleColumn.sort' => __('MinmaxArticle::models.ArticleColumn.sort'),
            'ArticleColumn.top' => __('MinmaxArticle::models.ArticleColumn.top'),
            'ArticleColumn.active' => __('MinmaxArticle::models.ArticleColumn.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
