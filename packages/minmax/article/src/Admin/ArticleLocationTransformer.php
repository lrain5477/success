<?php

namespace Minmax\Article\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Article\Models\ArticleLocation;

/**
 * Class ArticleLocationTransformer
 */
class ArticleLocationTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'articleLocationShow',
        'U' => 'articleLocationEdit',
        'D' => 'articleLocationDestroy',
    ];

    /**
     * Transformer constructor.
     * @param  ArticleLocationPresenter $presenter
     * @param  string $uri
     */
    public function __construct(ArticleLocationPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  ArticleLocation $model
     * @return array
     * @throws \Throwable
     */
    public function transform(ArticleLocation $model)
    {
        return [
            'id' => $this->presenter->getGridCheckBox($model),
            'title' => $this->presenter->getGridTitle($model),
            'article_tracks_count' => $this->presenter->getGridText($model, 'article_tracks_count'),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
