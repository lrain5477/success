<?php

namespace Minmax\Article\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Article\Models\ArticleFaq;

/**
 * Class ArticleFaqTransformer
 */
class ArticleFaqTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'articleFaqShow',
        'U' => 'articleFaqEdit',
        'D' => 'articleFaqDestroy',
    ];

    /**
     * Transformer constructor.
     * @param  ArticleFaqPresenter $presenter
     * @param  string $uri
     */
    public function __construct(ArticleFaqPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  ArticleFaq $model
     * @return array
     * @throws \Throwable
     */
    public function transform(ArticleFaq $model)
    {
        return [
            'id' => $this->presenter->getGridCheckBox($model),
            'title' => $this->presenter->getGridTitle($model),
            'article_tracks_count' => $this->presenter->getGridText($model, 'article_tracks_count'),
            'start_at' => $this->presenter->getGridText($model, is_null($model->start_at) ? 'created_at' : 'start_at'),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
