<?php

namespace Minmax\Article\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Article\Models\ArticleColumn;

/**
 * Class ArticleColumnTransformer
 */
class ArticleColumnTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'articleColumnShow',
        'U' => 'articleColumnEdit',
        'D' => 'articleColumnDestroy',
    ];

    /**
     * Transformer constructor.
     * @param  ArticleColumnPresenter $presenter
     * @param  string $uri
     */
    public function __construct(ArticleColumnPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  ArticleColumn $model
     * @return array
     * @throws \Throwable
     */
    public function transform(ArticleColumn $model)
    {
        return [
            'id' => $this->presenter->getGridCheckBox($model),
            'top' => $this->presenter->getGridTitle($model),
            'article_tracks_count' => $this->presenter->getGridText($model, 'article_tracks_count'),
            'start_at' => $this->presenter->getGridText($model, is_null($model->start_at) ? 'created_at' : 'start_at'),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
