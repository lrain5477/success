<?php

namespace Minmax\Article\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class ArticleLocationRequest
 */
class ArticleLocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('articleLocationEdit');
            case 'POST':
                return $this->user('admin')->can('articleLocationCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'ArticleLocation.categories' => 'nullable|array',
                    'ArticleLocation.title' => 'required|string',
                    'ArticleLocation.details' => 'nullable|array',
                    'ArticleLocation.contacts' => 'nullable|array',
                    'ArticleLocation.tags' => 'nullable|array',
                    'ArticleLocation.sort' => 'required|integer',
                    'ArticleLocation.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'ArticleLocation.categories' => 'nullable|array',
                    'ArticleLocation.title' => 'required|string',
                    'ArticleLocation.details' => 'nullable|array',
                    'ArticleLocation.contacts' => 'nullable|array',
                    'ArticleLocation.tags' => 'nullable|array',
                    'ArticleLocation.sort' => 'nullable|integer',
                    'ArticleLocation.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'ArticleLocation.categories' => __('MinmaxArticle::models.ArticleLocation.categories'),
            'ArticleLocation.title' => __('MinmaxArticle::models.ArticleLocation.title'),
            'ArticleLocation.details' => __('MinmaxArticle::models.ArticleLocation.details'),
            'ArticleLocation.contacts' => __('MinmaxArticle::models.ArticleLocation.contacts'),
            'ArticleLocation.tags' => __('MinmaxArticle::models.ArticleLocation.tags'),
            'ArticleLocation.sort' => __('MinmaxArticle::models.ArticleLocation.sort'),
            'ArticleLocation.active' => __('MinmaxArticle::models.ArticleLocation.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
