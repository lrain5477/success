<?php

namespace Minmax\Article\Admin;

use Illuminate\Support\Facades\DB;
use Minmax\Article\Models\ArticlePage;
use Minmax\Base\Admin\Presenter;

/**
 * Class ArticlePagePresenter
 */
class ArticlePagePresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxArticle::';

    protected $languageColumns = ['title', 'details', 'seo'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'category' => (new ArticleCategoryRepository)->getArticleCategorySelection('article-page'),
            'page_wrap' => systemParam('page_wrap'),
            'active' => systemParam('active'),
        ];
    }

    /**
     * @param  \Minmax\Article\Models\ArticlePage $model
     * @return integer
     */
    public function getGridTitle($model)
    {
        $titleValue = $model->title;
        $shortValue = str_limit(trim(strip_tags(array_get($model->details, 'editor') ?? '-')), 60);

        $urlValue = 'javascript:void(0);';
        if (in_array('R', $this->permissionSet)) {
            $urlValue = langRoute('admin.article-page.show', ['id' => $model->id]);
        }
        if (in_array('U', $this->permissionSet)) {
            $urlValue = langRoute('admin.article-page.edit', ['id' => $model->id]);
        }

        $category = $model->articleCategories->pluck('title')->implode(', ');

        $thisHtml = <<<HTML
<h3 class="h6 d-inline d-sm-block">
    <a class="text-pre-line" href="{$urlValue}">{$titleValue}</a>
</h3>
<p class="m-0 p-0 text-pre-line">{$shortValue}</p>
<small class="text-success float-right">{$category}</small>
HTML;

        return $thisHtml;
    }
}
