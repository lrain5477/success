<?php

namespace Minmax\Article\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class ArticleNewsRequest
 */
class ArticleNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('articleNewsEdit');
            case 'POST':
                return $this->user('admin')->can('articleNewsCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'ArticleNews.uri' => [
                        'nullable',
                        'string',
                        Rule::unique('article_news', 'uri')->ignore($this->route('id'))
                    ],
                    'ArticleNews.categories' => 'nullable|array',
                    'ArticleNews.title' => 'required|string',
                    'ArticleNews.details' => 'nullable|array',
                    'ArticleNews.start_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleNews.end_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleNews.tags' => 'nullable|array',
                    'ArticleNews.roles' => 'nullable|array',
                    'ArticleNews.properties' => 'nullable|array',
                    'ArticleNews.comment' => 'required|boolean',
                    'ArticleNews.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'ArticleNews.uri' => 'nullable|string|unique:article_news,uri',
                    'ArticleNews.categories' => 'nullable|array',
                    'ArticleNews.title' => 'required|string',
                    'ArticleNews.details' => 'nullable|array',
                    'ArticleNews.start_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleNews.end_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleNews.tags' => 'nullable|array',
                    'ArticleNews.roles' => 'nullable|array',
                    'ArticleNews.properties' => 'nullable|array',
                    'ArticleNews.comment' => 'required|boolean',
                    'ArticleNews.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'ArticleNews.uri' => __('MinmaxArticle::models.ArticleNews.uri'),
            'ArticleNews.categories' => __('MinmaxArticle::models.ArticleNews.categories'),
            'ArticleNews.title' => __('MinmaxArticle::models.ArticleNews.title'),
            'ArticleNews.details' => __('MinmaxArticle::models.ArticleNews.details'),
            'ArticleNews.start_at' => __('MinmaxArticle::models.ArticleNews.start_at'),
            'ArticleNews.end_at' => __('MinmaxArticle::models.ArticleNews.end_at'),
            'ArticleNews.tags' => __('MinmaxArticle::models.ArticleNews.tags'),
            'ArticleNews.roles' => __('MinmaxArticle::models.ArticleNews.roles'),
            'ArticleNews.properties' => __('MinmaxArticle::models.ArticleNews.properties'),
            'ArticleNews.comment' => __('MinmaxArticle::models.ArticleNews.comment'),
            'ArticleNews.active' => __('MinmaxArticle::models.ArticleNews.active'),
        ];
    }

    /**
     * 設定驗證器實例。
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
