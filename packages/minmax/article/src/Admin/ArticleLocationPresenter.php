<?php

namespace Minmax\Article\Admin;

use Minmax\Base\Admin\Presenter;

/**
 * Class ArticleLocationPresenter
 */
class ArticleLocationPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxArticle::';

    protected $languageColumns = ['title', 'details', 'contacts'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'categories' => (new ArticleCategoryRepository)->getArticleCategorySelection('article-location'),
            'tags' => siteParam('tags'),
            'active' => systemParam('active'),
        ];
    }

    /**
     * @param  \Minmax\Article\Models\ArticleLocation $model
     * @return integer
     */
    public function getGridTitle($model)
    {
        $titleValue = $model->title;
        $shortValue = str_limit(trim(strip_tags(array_get($model->contacts ?? [], 'address', ''))), 60);

        $urlValue = 'javascript:void(0);';
        if (in_array('R', $this->permissionSet)) {
            $urlValue = langRoute('admin.article-location.show', ['id' => $model->id]);
        }
        if (in_array('U', $this->permissionSet)) {
            $urlValue = langRoute('admin.article-location.edit', ['id' => $model->id]);
        }

        $categories = str_limit($model->articleCategories->pluck('title')->implode(', '), 20);

        $thisHtml = <<<HTML
<h3 class="h6 d-inline d-sm-block">
    <a class="text-pre-line" href="{$urlValue}">{$titleValue}</a>
</h3>
<p class="m-0 p-0 text-pre-line"><small>{$shortValue}</small><span class="float-right">{$categories}</span></p>
HTML;

        return $thisHtml;
    }

    /**
     * @param  \Minmax\Article\Models\ArticleLocation $model
     * @param  array $options
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getShowTagsInput($model, $options = [])
    {
        $tags = collect(array_get($this->parameterSet, 'tags') ?? [])
            ->filter(function ($tag, $key) use ($model) {
                return in_array($key, $model->tags);
            })
            ->pluck('title')
            ->implode(', ');

        return $this->getShowNormalText($model, 'tags', ['defaultValue' => $tags]);
    }

    /**
     * @param  \Minmax\Article\Models\ArticleLocation $model
     * @param  array $options
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFieldTagsInput($model, $options = [])
    {
        $componentData = [
            'id' => 'ArticleLocation-tags',
            'label' => __('MinmaxArticle::models.ArticleLocation.tags'),
            'name' => 'ArticleLocation[tags][]',
            'values' => $model->tags ?? [],
            'hint' => __('MinmaxArticle::models.ArticleLocation.hint.tags', ['link' => langRoute('admin.site-parameter-item.index')]),
            'listData' => array_get($this->parameterSet, 'tags', []),
        ];

        return view('MinmaxArticle::admin.article-location.form-tags-input', $componentData);
    }
}
