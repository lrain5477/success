<?php

namespace Minmax\Article\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Article\Models\ArticlePage;

/**
 * Class ArticlePageTransformer
 */
class ArticlePageTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'articlePageShow',
        'U' => 'articlePageEdit',
        'D' => 'articlePageDestroy',
    ];

    /**
     * Transformer constructor.
     * @param  ArticlePagePresenter $presenter
     * @param  string $uri
     */
    public function __construct(ArticlePagePresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  ArticlePage $model
     * @return array
     * @throws \Throwable
     */
    public function transform(ArticlePage $model)
    {
        return [
            'id' => $this->presenter->getGridCheckBox($model),
            'title' => $this->presenter->getGridTitle($model),
            'article_tracks_count' => $this->presenter->getGridText($model, 'article_tracks_count'),
            'start_at' => $this->presenter->getPureString($model->start_at ?? $model->created_at),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
