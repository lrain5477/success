<?php

namespace Minmax\Article\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class ArticleTeamRequest
 */
class ArticleTeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('articleTeamEdit');
            case 'POST':
                return $this->user('admin')->can('articleTeamCreate');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'ArticleTeam.uri' => [
                        'nullable',
                        'string',
                        Rule::unique('article_page', 'uri')->ignore($this->route('id'))
                    ],
                    'ArticleTeam.title' => 'required|string',
                    'ArticleTeam.position' => 'nullable|string',
                    'ArticleTeam.details' => 'nullable|array',
                    'ArticleTeam.pic' => 'nullable|array',
                    'ArticleTeam.start_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleTeam.end_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleTeam.seo' => 'nullable|array',
                    'ArticleTeam.sort' => 'required|integer',
                    'ArticleTeam.top' => 'required|boolean',
                    'ArticleTeam.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'ArticleTeam.uri' => 'nullable|string|unique:article_page,uri',
                    'ArticleTeam.title' => 'required|string',
                    'ArticleTeam.position' => 'nullable|string',
                    'ArticleTeam.details' => 'nullable|array',
                    'ArticleTeam.pic' => 'nullable|array',
                    'ArticleTeam.start_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleTeam.end_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'ArticleTeam.seo' => 'nullable|array',
                    'ArticleTeam.sort' => 'nullable|integer',
                    'ArticleTeam.top' => 'required|boolean',
                    'ArticleTeam.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'ArticleTeam.uri' => __('MinmaxArticle::models.ArticleTeam.uri'),
            'ArticleTeam.title' => __('MinmaxArticle::models.ArticleTeam.title'),
            'ArticleTeam.position' => __('MinmaxArticle::models.ArticleTeam.position'),
            'ArticleTeam.details' => __('MinmaxArticle::models.ArticleTeam.details'),
            'ArticleTeam.pic' => __('MinmaxArticle::models.ArticleTeam.pic'),
            'ArticleTeam.start_at' => __('MinmaxArticle::models.ArticleTeam.start_at'),
            'ArticleTeam.end_at' => __('MinmaxArticle::models.ArticleTeam.end_at'),
            'ArticleTeam.seo' => __('MinmaxArticle::models.ArticleTeam.seo'),
            'ArticleTeam.sort' => __('MinmaxArticle::models.ArticleTeam.sort'),
            'ArticleTeam.top' => __('MinmaxArticle::models.ArticleTeam.top'),
            'ArticleTeam.active' => __('MinmaxArticle::models.ArticleTeam.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
