<?php

namespace Minmax\Article\Admin;

use Exception;
use Minmax\Base\Admin\Repository;
use Minmax\Article\Models\ArticleNews;
use Minmax\Base\Admin\SiteParameterGroupRepository;
use Minmax\Base\Admin\SiteParameterItemRepository;

/**
 * Class ArticleNewsRepository
 * @property ArticleNews $model
 * @method ArticleNews find($id)
 * @method ArticleNews one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticleNews create($attributes)
 * @method ArticleNews save($model, $attributes)
 * @method ArticleNews|\Illuminate\Database\Eloquent\Builder query()
 */
class ArticleNewsRepository extends Repository
{
    const MODEL = ArticleNews::class;

    protected $languageColumns = ['title', 'details', 'seo'];

    protected $categories = null;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'article_news';
    }

    protected function beforeCreate()
    {
        $this->categories = array_pull($this->attributes, 'categories', []);

        if ($tags = array_get($this->attributes, 'tags')) {
            if (count($tags) > 0) {
                $allTag = collect(siteParam('tags'))
                    ->map(function ($item) {
                        return array_get($item, 'title');
                    })
                    ->toArray();
                $tagList = [];
                foreach ($tags as $tagLabel) {
                    if (in_array($tagLabel, $allTag)) {
                        $tagList[] = array_search($tagLabel, $allTag);
                    } else {
                        try {
                            $paramGroup = (new SiteParameterGroupRepository)->one('code', 'tags');
                            $tagData = (new SiteParameterItemRepository)->create(['group_id' => $paramGroup->id, 'label' => $tagLabel]);
                            $tagList[] = $tagData->id;
                        } catch (Exception $e) {}
                    }
                }
                array_set($this->attributes, 'tags', $tagList);
            }
        }
    }

    protected function afterCreate()
    {
        $this->model->articleCategories()->sync(
            collect($this->categories)
                ->mapWithKeys(function ($item) {
                    return [$item => ['model' => get_class($this->model)]];
                })
                ->toArray()
        );
        $this->categories = null;
    }

    protected function beforeSave()
    {
        if (count($this->attributes) > 1) {
            $this->categories = array_pull($this->attributes, 'categories', []) ?? [];
        }

        if ($tags = array_get($this->attributes, 'tags')) {
            if (count($tags) > 0) {
                $allTag = collect(siteParam('tags'))
                    ->map(function ($item) {
                        return array_get($item, 'title');
                    })
                    ->toArray();
                $tagList = [];
                foreach ($tags as $tagLabel) {
                    if (in_array($tagLabel, $allTag)) {
                        $tagList[] = array_search($tagLabel, $allTag);
                    } else {
                        try {
                            $paramGroup = (new SiteParameterGroupRepository)->one('code', 'tags');
                            $tagData = (new SiteParameterItemRepository)->create(['group_id' => $paramGroup->id, 'label' => $tagLabel]);
                            $tagList[] = $tagData->id;
                        } catch (Exception $e) {}
                    }
                }
                array_set($this->attributes, 'tags', $tagList);
            }
        }
    }

    protected function afterSave()
    {
        if (! is_null($this->categories)) {
            $this->model->articleCategories()->sync(
                collect($this->categories)
                    ->mapWithKeys(function ($item) {
                        return [$item => ['model' => get_class($this->model)]];
                    })
                    ->toArray()
            );
            $this->categories = null;
        }
    }
}
