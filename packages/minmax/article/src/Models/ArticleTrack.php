<?php

namespace Minmax\Article\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticleTrack
 * @property string $model
 * @property string $object_id
 * @property string $ip
 * @property \Illuminate\Support\Carbon $click_at
 * @property \Illuminate\Support\Carbon $created_at
 */
class ArticleTrack extends Model
{
    protected $table = 'article_track';
    protected $primaryKey = null;
    protected $guarded = [];
    protected $dates = ['click_at', 'created_at'];

    public $incrementing = false;

    const UPDATED_AT = null;
}
