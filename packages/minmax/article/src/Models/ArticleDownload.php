<?php

namespace Minmax\Article\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticleDownload
 * @property integer $id
 * @property string $uri
 * @property string $title
 * @property array $details
 * @property array $file
 * @property \Illuminate\Support\Carbon $start_at
 * @property \Illuminate\Support\Carbon $end_at
 * @property integer $sort
 * @property boolean $active
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \Illuminate\Database\Eloquent\Collection|ArticleCategory[] $articleCategories
 * @property integer $article_tracks_count
 */
class ArticleDownload extends Model
{
    protected $table = 'article_download';
    protected $guarded = [];
    protected $dates = ['start_at', 'end_at', 'created_at', 'updated_at'];
    protected $casts = [
        'file' => 'array',
        'active' => 'boolean',
    ];

    public $incrementing = false;

    public function getTitleAttribute()
    {
        return langDB($this->getAttributeFromArray('title'));
    }

    public function getDetailsAttribute()
    {
        return json_decode(langDB($this->getAttributeFromArray('details')), true);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function articleCategories()
    {
        return $this->belongsToMany(ArticleCategory::class, 'article_category_relation', 'object_id', 'category_id')->wherePivot('model', get_class($this));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articleTracks()
    {
        return $this->hasMany(ArticleTrack::class, 'object_id', 'id')->where('model', get_class($this));
    }
}
