<?php

namespace Minmax\Article\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticleCategory
 * @property string $id
 * @property string $uri
 * @property string $parent_id
 * @property string $title
 * @property array $details
 * @property array $options
 * @property array $seo
 * @property integer $sort
 * @property boolean $editable
 * @property boolean $active
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class ArticleCategory extends Model
{
    protected $table = 'article_category';
    protected $guarded = [];
    protected $casts = [
        'options' => 'array',
        'editable' => 'boolean',
        'active' => 'boolean',
    ];

    public $incrementing = false;

    public function getTitleAttribute()
    {
        return langDB($this->getAttributeFromArray('title'));
    }

    public function getDetailsAttribute()
    {
        return json_decode(langDB($this->getAttributeFromArray('details')), true);
    }

    public function getSeoAttribute()
    {
        return json_decode(langDB($this->getAttributeFromArray('seo')), true);
    }

    /**
     * Dynamically retrieve attributes on the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        if (preg_match("/^article[A-Z].+$/", $key)) {
            if ($this->relationLoaded($key)) {
                return $this->relations[$key];
            }

            if (is_callable($this, $key)) {
                return $this->getRelationshipFromMethod($key);
            }
        }

        return parent::__get($key);
    }

    /**
     * Handle dynamic method calls into the model.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (preg_match("/^article[A-Z].+$/", $method)) {
            $className = 'App\\Models\\' . studly_case(str_singular($method));
            $className = class_exists($className) ? $className : 'Minmax\\Article\\Models\\' . studly_case(str_singular($method));
            return $this->belongsToMany($className, 'article_category_relation', 'category_id', 'object_id')->where('model', $className);
        }

        return parent::__call($method, $parameters);
    }
}
