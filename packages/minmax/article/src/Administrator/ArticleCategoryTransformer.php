<?php

namespace Minmax\Article\Administrator;

use Minmax\Base\Administrator\Transformer;
use Minmax\Article\Models\ArticleCategory;

/**
 * Class ArticleCategoryTransformer
 */
class ArticleCategoryTransformer extends Transformer
{
    protected $menuList;

    /**
     * Transformer constructor.
     * @param  ArticleCategoryPresenter $presenter
     * @param  string $uri
     */
    public function __construct(ArticleCategoryPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        $this->menuList = (new ArticleCategoryRepository)->all();

        parent::__construct($uri);
    }

    /**
     * @param  ArticleCategory $model
     * @return array
     * @throws \Throwable
     */
    public function transform(ArticleCategory $model)
    {
        $loopLevel = 0;
        $loopModel = $model;
        do {
            $childrenFlag = ++$loopLevel < config('minmax.article_layer_limit', 3);
            if ($loopModel->parent_id) { $loopModel = $this->menuList->firstWhere('id', $loopModel->parent_id); } else { break; }
        } while ($childrenFlag);

        return [
            'title' => $this->presenter->getGridText($model, 'title'),
            'obj_amount' => $this->presenter->getGridObjAmount($model),
            'sub_amount' => $this->presenter->getGridSubAmount($model, $childrenFlag),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'editable' => $this->presenter->getGridSwitch($model, 'editable'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model, $childrenFlag
                ? [
                    ['permission' => 'R', 'view' => 'MinmaxArticle::administrator.article-category.action-button-children']
                ]
                : []
            ),
        ];
    }
}
