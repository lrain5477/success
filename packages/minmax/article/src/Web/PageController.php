<?php

namespace Minmax\Article\Web;

use Minmax\Base\Web\Controller;

/**
 * Class PageController
 */
class PageController extends Controller
{
    protected $packagePrefix = 'MinmaxArticle::';

    /**
     * @param  ArticlePageRepository $repository
     * @param  string $id
     * @return \Illuminate\Http\Response
     */
    protected function article(ArticlePageRepository $repository, $id)
    {
        $currentPage = $repository->find($id) ?? $repository->one('uri', $id);

        // Check Active
        if (is_null($currentPage) || !$currentPage->active || is_null($currentPage->page_wrap)) abort(404);

        // Check Expired
        if ((!is_null($currentPage->start_at) && $currentPage->start_at->greaterThan(now()))
            || (!is_null($currentPage->end_at) && $currentPage->end_at->greaterThan(now()))) {
            abort(404);
        }

        // Check Wrap Setting
        $wrapSetting = siteParam("page_wrap.{$currentPage->page_wrap}");
        if (is_string($wrapSetting)) abort(404);

        // Check Template
        $viewTemplate = array_get($wrapSetting, 'options.template');
        if (!isset($viewTemplate)) abort(404);

        $this->viewData['articleData'] = $currentPage;

        return view($viewTemplate, $this->viewData);
    }
}
