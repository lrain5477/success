<?php

namespace Minmax\Article\Web;

use Minmax\Base\Web\Repository;
use Minmax\Article\Models\ArticleNews;

/**
 * Class ArticleNewsRepository
 * @property ArticleNews $model
 * @method ArticleNews find($id)
 * @method ArticleNews one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticleNews create($attributes)
 * @method ArticleNews save($model, $attributes)
 * @method ArticleNews|\Illuminate\Database\Eloquent\Builder query()
 */
class ArticleNewsRepository extends Repository
{
    const MODEL = ArticleNews::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'article_news';
    }

    public function getNewsByCategory($key, $column = 'uri')
    {
        return $this->query()
            ->whereHas('articleCategories', function ($query) use ($column, $key) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->where($column, $key)->where('active', true);
            })
            ->where(function ($query) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->whereNull('start_at')->orWhere('start_at', '<=', date('Y-m-d H:i:s'));
            })
            ->where(function ($query) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->whereNull('end_at')->orWhere('end_at', '>=', date('Y-m-d H:i:s'));
            })
            ->where('active', true)
            ->orderByRaw("json_contains(`properties`, '\"top\"') desc")
            ->orderByRaw('ifnull(`start_at`, `created_at`) desc')
            ->get();
    }
}
