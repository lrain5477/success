<?php

namespace Minmax\Article\Web;

use Minmax\Base\Web\Repository;
use Minmax\Article\Models\ArticlePage;

/**
 * Class ArticlePageRepository
 * @property ArticlePage $model
 * @method ArticlePage find($id)
 * @method ArticlePage one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticlePage create($attributes)
 * @method ArticlePage save($model, $attributes)
 * @method ArticlePage|\Illuminate\Database\Eloquent\Builder query()
 */
class ArticlePageRepository extends Repository
{
    const MODEL = ArticlePage::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'article_page';
    }

    public function getPagesByCategory($key, $column = 'uri')
    {
        return $this->query()
            ->whereHas('articleCategories', function ($query) use ($column, $key) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->where($column, $key)->where('active', true);
            })
            ->where(function ($query) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->whereNull('start_at')->orWhere('start_at', '<=', date('Y-m-d H:i:s'));
            })
            ->where(function ($query) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->whereNull('end_at')->orWhere('end_at', '>=', date('Y-m-d H:i:s'));
            })
            ->where('active', true)
            ->orderBy('sort')
            ->orderByDesc('start_at')
            ->orderByDesc('created_at')
            ->get();
    }
}
