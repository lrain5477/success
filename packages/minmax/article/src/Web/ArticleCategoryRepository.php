<?php

namespace Minmax\Article\Web;

use Minmax\Base\Web\Repository;
use Minmax\Article\Models\ArticleCategory;

/**
 * Class ArticleCategoryRepository
 * @property ArticleCategory $model
 * @method ArticleCategory find($id)
 * @method ArticleCategory one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method ArticleCategory create($attributes)
 * @method ArticleCategory save($model, $attributes)
 * @method ArticleCategory|\Illuminate\Database\Eloquent\Builder query()
 */
class ArticleCategoryRepository extends Repository
{
    const MODEL = ArticleCategory::class;

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'article_category';
    }

    /**
     * Grt categories by condition. (Arguments are pass to Eloquent where function)
     *
     * @return ArticleCategory[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getCategories()
    {
        if (func_num_args() > 0) {
            return $this->query()
                ->where(...func_get_args())
                ->where('active', true)
                ->orderBy('sort')
                ->get();
        } else {

            return $this->query()
                ->where('active', true)
                ->orderBy('sort')
                ->get();
        }
    }
}
