<?php

namespace Minmax\Article\Web;

use Minmax\Base\Web\Controller;

/**
 * Class NewsController
 */
class NewsController extends Controller
{
    protected $packagePrefix = 'MinmaxArticle::';

    /**
     * @param  ArticleCategoryRepository $repository
     * @param  string $id
     * @return \Illuminate\Http\Response
     */
    protected function categories(ArticleCategoryRepository $repository, $id)
    {
        $currentCategory = $repository->find($id) ?? $repository->one('uri', $id);

        if (is_null($currentCategory) || !$currentCategory->active) abort(404);

        $this->viewData['categories'] = $repository->getCategories('parent_id', $currentCategory->id);

        $this->viewData['currentCategory'] = $currentCategory;

        return view($this->packagePrefix . "web.news.categories", $this->viewData);
    }

    /**
     * @param  ArticleCategoryRepository $repository
     * @param  ArticleNewsRepository $newsRepository
     * @param  string $id
     * @return \Illuminate\Http\Response
     */
    protected function category(ArticleCategoryRepository $repository, ArticleNewsRepository $newsRepository, $id)
    {
        $currentCategory = $repository->find($id) ?? $repository->one('uri', $id);

        if (is_null($currentCategory) || !$currentCategory->active) abort(404);

        $this->viewData['newsData'] = $newsRepository->getNewsByCategory($currentCategory->id, 'id');

        $this->viewData['currentCategory'] = $currentCategory;

        return view($this->packagePrefix . "web.news.category", $this->viewData);
    }

    /**
     * @param  ArticleNewsRepository $repository
     * @param  string $id
     * @return \Illuminate\Http\Response
     */
    protected function article(ArticleNewsRepository $repository, $id)
    {
        $currentNews = $repository->find($id) ?? $repository->one('uri', $id);

        if (is_null($currentNews) || !$currentNews->active) abort(404);

        $this->viewData['newsData'] = $currentNews;

        return view($this->packagePrefix . "web.news.article", $this->viewData);
    }
}
