<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Models (Database Column) Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in backend platform page.
    |
    */

    'ArticleNews' => [
        'id' => 'ID',
        'uri' => '自訂連結',
        'title' => '新聞標題',
        'details' => '新聞內容',
        'start_at' => '發佈時間',
        'end_at' => '下架時間',
        'seo' => [
            'meta_description' => 'SEO 網站描述',
            'meta_keywords' => 'SEO 關鍵字',
        ],
        'tags' => '關聯標籤',
        'roles' => '限定會員',
        'properties' => '綜合屬性',
        'comment' => '留言開放',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'uri' => '自訂新聞連結名稱，此名稱為唯一值。',
            'start_at' => '如果需要在未來期限到後自動上架刊登，請在此設定一個日期。',
            'end_at' => '如果需要在有效期限過後自動下架刊登，請在此設定一個日期。',
            'seo' => [
                'meta_description' => '(Metadata Description) 利用簡短的說明讓人清楚的了解網站的主要內容、簡介方向等，搜尋引擎將會幫我們適當的顯示在介紹頁面上。',
                'meta_keywords' => '(Metadata Keywords) 為了幫助搜尋引擎更容易搜尋到網站，你可以在這裡填寫相關的搜尋字詞，多組關鍵字以上請使用半形逗號區隔。',
            ],
            'tags' => '更多標籤設定請前往 <a href=":link">參數項目</a> 新增。',
            'categories' => '一篇新聞可以放置於多個分類中。',
        ],
        'categories' => '新聞類別',
        'article_tracks_count' => '瀏覽次數',
    ],

    'ArticlePage' => [
        'id' => 'ID',
        'uri' => '自訂連結',
        'title' => '頁面標題',
        'details' => '頁面內容',
        'page_wrap' => '框架套用',
        'start_at' => '發佈時間',
        'end_at' => '下架時間',
        'seo' => [
            'meta_description' => 'SEO 網站描述',
            'meta_keywords' => 'SEO 關鍵字',
        ],
        'sort' => '排序',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'uri' => '自訂頁面連結名稱，此名稱為唯一值。',
            'start_at' => '如果需要在未來期限到後自動上架刊登，請在此設定一個日期。',
            'end_at' => '如果需要在有效期限過後自動下架刊登，請在此設定一個日期。',
            'seo' => [
                'meta_description' => '(Metadata Description) 利用簡短的說明讓人清楚的了解網站的主要內容、簡介方向等，搜尋引擎將會幫我們適當的顯示在介紹頁面上。',
                'meta_keywords' => '(Metadata Keywords) 為了幫助搜尋引擎更容易搜尋到網站，你可以在這裡填寫相關的搜尋字詞，多組關鍵字以上請使用半形逗號區隔。',
            ],
        ],
        'category' => '頁面類別',
        'article_tracks_count' => '瀏覽次數',
    ],

    'ArticleFaq' => [
        'id' => 'ID',
        'uri' => '自訂連結',
        'title' => '問答標題',
        'details' => '問答內容',
        'start_at' => '發佈時間',
        'end_at' => '下架時間',
        'seo' => [
            'meta_description' => 'SEO 網站描述',
            'meta_keywords' => 'SEO 關鍵字',
        ],
        'sort' => '排序',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'uri' => '自訂頁面連結名稱，此名稱為唯一值。',
            'start_at' => '如果需要在未來期限到後自動上架刊登，請在此設定一個日期。',
            'end_at' => '如果需要在有效期限過後自動下架刊登，請在此設定一個日期。',
            'seo' => [
                'meta_description' => '(Metadata Description) 利用簡短的說明讓人清楚的了解網站的主要內容、簡介方向等，搜尋引擎將會幫我們適當的顯示在介紹頁面上。',
                'meta_keywords' => '(Metadata Keywords) 為了幫助搜尋引擎更容易搜尋到網站，你可以在這裡填寫相關的搜尋字詞，多組關鍵字以上請使用半形逗號區隔。',
            ],
        ],
        'categories' => '問答類別',
        'article_tracks_count' => '瀏覽次數',
    ],

    'ArticleDownload' => [
        'id' => 'ID',
        'uri' => '自訂連結',
        'title' => '檔案標題',
        'details' => '檔案內容',
        'file' => '檔案選擇',
        'start_at' => '發佈時間',
        'end_at' => '下架時間',
        'sort' => '排序',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'file' => '檔案數量限制：1 項。',
            'uri' => '自訂檔案連結名稱，此名稱為唯一值。',
            'start_at' => '如果需要在未來期限到後自動上架刊登，請在此設定一個日期。',
            'end_at' => '如果需要在有效期限過後自動下架刊登，請在此設定一個日期。',
        ],
        'categories' => '檔案類別',
        'article_tracks_count' => '下載次數',
    ],

    'ArticleColumn' => [
        'id' => 'ID',
        'uri' => '自訂連結',
        'title' => '文章標題',
        'details' => '文章內容',
        'start_at' => '發佈時間',
        'end_at' => '下架時間',
        'seo' => [
            'meta_description' => 'SEO 網站描述',
            'meta_keywords' => 'SEO 關鍵字',
        ],
        'sort' => '排序',
        'top' => '置頂狀態',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'uri' => '自訂頁面連結名稱，此名稱為唯一值。',
            'start_at' => '如果需要在未來期限到後自動上架刊登，請在此設定一個日期。',
            'end_at' => '如果需要在有效期限過後自動下架刊登，請在此設定一個日期。',
            'seo' => [
                'meta_description' => '(Metadata Description) 利用簡短的說明讓人清楚的了解網站的主要內容、簡介方向等，搜尋引擎將會幫我們適當的顯示在介紹頁面上。',
                'meta_keywords' => '(Metadata Keywords) 為了幫助搜尋引擎更容易搜尋到網站，你可以在這裡填寫相關的搜尋字詞，多組關鍵字以上請使用半形逗號區隔。',
            ],
            'top' => '啟用此選項文章將被優先排序到最前面。',
        ],
        'categories' => '文章類別',
        'article_tracks_count' => '瀏覽次數',
    ],

    'ArticleLocation' => [
        'id' => 'ID',
        'title' => '標題名稱',
        'details' => '據點內容',
        'contacts' => '聯絡資訊',
        'tags' => '關聯標籤',
        'sort' => '排序',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'tags' => '更多標籤設定請前往 <a href=":link">參數項目</a> 新增。',
        ],
        'categories' => '據點類別',
        'article_tracks_count' => '瀏覽次數',
    ],

    'ArticleTeam' => [
        'id' => 'ID',
        'uri' => '自訂連結',
        'title' => '成員名稱',
        'position' => '成員職稱',
        'details' => '成員內容',
        'pic' => '圖片',
        'start_at' => '發佈時間',
        'end_at' => '下架時間',
        'seo' => [
            'meta_description' => 'SEO 網站描述',
            'meta_keywords' => 'SEO 關鍵字',
        ],
        'sort' => '排序',
        'top' => '置頂狀態',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'uri' => '自訂頁面連結名稱，此名稱為唯一值。',
            'pic' => '圖片類型：jpg、png。數量限制：1 張。',
            'start_at' => '如果需要在未來期限到後自動上架刊登，請在此設定一個日期。',
            'end_at' => '如果需要在有效期限過後自動下架刊登，請在此設定一個日期。',
            'seo' => [
                'meta_description' => '(Metadata Description) 利用簡短的說明讓人清楚的了解網站的主要內容、簡介方向等，搜尋引擎將會幫我們適當的顯示在介紹頁面上。',
                'meta_keywords' => '(Metadata Keywords) 為了幫助搜尋引擎更容易搜尋到網站，你可以在這裡填寫相關的搜尋字詞，多組關鍵字以上請使用半形逗號區隔。',
            ],
            'top' => '啟用此選項成員將被優先排序到最前面。',
        ],
        'category' => '團隊類別',
        'article_tracks_count' => '瀏覽次數',
    ],

    'ArticleCategory' => [
        'id' => 'ID',
        'uri' => '自訂連結',
        'parent_id' => '上層類別',
        'title' => '類別名稱',
        'details' => '詳細內容',
        'options' => '類別設定',
        'seo' => 'SEO',
        'sort' => '排序',
        'editable' => '可否編輯',
        'active' => '啟用狀態',
        'created_at' => '建立時間',
        'updated_at' => '更新時間',
        'hint' => [
            'uri' => '自訂分類連結名稱，此名稱為唯一值。',
            'options' => '根類別須設定 <code>relation</code> 以設置對應資料關聯。設定 <code>details</code> 並逗號分隔子欄位名稱，以於分類加入擴充欄位。',
        ],
        'obj_amount' => '數量',
        'sub_amount' => '子類數',
    ],

];
