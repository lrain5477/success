<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    'grid' => [
        'ArticleNews' => [
            'pic' => '圖片',
            'tags' => [
                'top' => '置頂',
            ],
        ],
        'ArticleColumn' => [
            'pic' => '圖片',
            'tags' => [
                'top' => '置頂',
            ],
        ],
        'ArticleTeam' => [
            'pic' => '圖片',
            'tags' => [
                'top' => '置頂',
            ],
        ],
    ],

    'form' => [
        'fieldSet' => [
            'property' => '綜合設定'
        ],
    ],

];
