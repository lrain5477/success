<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Administrator Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in administrator page.
    |
    */

    'grid' => [
        'ArticleNews' => [
            'pic' => '圖片',
            'tags' => [
                'top' => '置頂',
            ],
        ],
        'ArticleColumn' => [
            'pic' => '圖片',
            'tags' => [
                'top' => '置頂',
            ],
        ],
        'ArticleTeam' => [
            'pic' => '圖片',
            'tags' => [
                'top' => '置頂',
            ],
        ],
    ],

];
