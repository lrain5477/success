<?php
/**
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Article\Models\ArticleCategory $parentModel
 */
?>

@extends('MinmaxBase::admin.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links')
        @isset($parentModel)
        <a class="btn btn-sm btn-light" href="{{ langRoute("admin.{$pageData->uri}.index", ['parent' => $parentModel->parent_id]) }}" title="@lang('MinmaxBase::admin.grid.back')">
            <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.grid.back')</span>
        </a>
        @endisset
        @if($adminData->can('articleCategoryCreate') && !is_null(request('parent')))
        <a class="btn btn-sm btn-main" href="{{ langRoute("admin.{$pageData->uri}.create", ['parent' => request('parent')]) }}" title="@lang('MinmaxBase::admin.form.create')">
            <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.create')</span>
        </a>
        @endif
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Article\Admin\ArticleCategoryPresenter')

@section('grid-filter')
    @component('MinmaxBase::admin.layouts.grid.filter-keyword')
    <option value="title">@lang('MinmaxArticle::models.ArticleCategory.title')</option>
    @endcomponent

    @component('MinmaxBase::admin.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('active', 'searchActive', ['emptyLabel' => __('MinmaxArticle::models.ArticleCategory.active')]) !!}
    @endcomponent
@endsection

@section('grid-table')
{!! Breadcrumbs::view('MinmaxBase::admin.layouts.table-breadcrumbs', 'datatable') !!}
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        @if(is_null(request('parent')))
        <th class="w-5">No.</th>
        <th class="w-50">@lang('MinmaxArticle::models.ArticleCategory.title')</th>
        <th class="nosort">@lang('MinmaxArticle::models.ArticleCategory.obj_amount')</th>
        <th class="nosort">@lang('MinmaxArticle::models.ArticleCategory.sub_amount')</th>
        @else
        <th class="w-50">@lang('MinmaxArticle::models.ArticleCategory.title')</th>
        <th class="nosort">@lang('MinmaxArticle::models.ArticleCategory.obj_amount')</th>
        <th class="nosort">@lang('MinmaxArticle::models.ArticleCategory.sub_amount')</th>
        <th>@lang('MinmaxArticle::models.ArticleCategory.sort')</th>
        <th>@lang('MinmaxArticle::models.ArticleCategory.active')</th>
        <th class="nosort">@lang('MinmaxBase::admin.grid.title.action')</th>
        @endif
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            @if(is_null(request('parent')))
            {data: 'sort', name: 'sort'},
            {data: 'title', name: 'title'},
            {data: 'obj_amount', name: 'obj_amount'},
            {data: 'sub_amount', name: 'sub_amount'},
            @else
            {data: 'title', name: 'title'},
            {data: 'obj_amount', name: 'obj_amount'},
            {data: 'sub_amount', name: 'sub_amount'},
            {data: 'sort', name: 'sort'},
            {data: 'active', name: 'active'},
            {data: 'action', name: 'action'}
            @endif
        ],
        ['title'],
        {"active":"searchActive"},
        [[parseInt('{{ is_null(request('parent')) ? '0' : '3' }}'), 'asc']],
        '{{ langRoute("admin.{$pageData->uri}.ajaxDataTable", ['parent' => request('parent')]) }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("admin.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
