<?php
/**
 * @var \Minmax\Base\Models\WebData $webData
 * @var \Minmax\Article\Models\ArticlePage $articleData
 */
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>{{ $articleData->title }} | {{ $webData->website_name }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta charset="UTF-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>
<div class="wrapper">
    <div class="container">
        {!! array_get($articleData->details, 'editor') ?? $articleData->title !!}
    </div>
</div>
</body>
</html>
