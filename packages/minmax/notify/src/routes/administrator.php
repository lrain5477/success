<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['administrator', 'localizationRedirect'],
    'as' => 'administrator.' . app()->getLocale() . '.'
], function() {

    Route::group(['prefix' => 'administrator', 'namespace' => 'Minmax\Notify\Administrator', 'middleware' => 'auth:administrator'], function () {
        /*
         |--------------------------------------------------------------------------
         | 需要登入的路由。
         |--------------------------------------------------------------------------
         */

        /*
         * NotifyEmail 通知信件
         */
        Route::get('notify-email', 'NotifyEmailController@index')->name('notify-email.index');
        Route::post('notify-email', 'NotifyEmailController@store')->name('notify-email.store');
        Route::get('notify-email/create', 'NotifyEmailController@create')->name('notify-email.create');
        Route::get('notify-email/{id}', 'NotifyEmailController@show')->name('notify-email.show');
        Route::put('notify-email/{id}', 'NotifyEmailController@update')->name('notify-email.update');
        Route::delete('notify-email/{id}', 'NotifyEmailController@destroy')->name('notify-email.destroy');
        Route::get('notify-email/{id}/edit', 'NotifyEmailController@edit')->name('notify-email.edit');
        Route::post('notify-email/ajax/datatables', 'NotifyEmailController@ajaxDataTable')->name('notify-email.ajaxDataTable');
        Route::patch('notify-email/ajax/switch', 'NotifyEmailController@ajaxSwitch')->name('notify-email.ajaxSwitch');
        Route::patch('notify-email/ajax/sort', 'NotifyEmailController@ajaxSort')->name('notify-email.ajaxSort');

        /*
         * NotifySms 通知簡訊
         */
        Route::get('notify-sms', 'NotifySmsController@index')->name('notify-sms.index');
        Route::post('notify-sms', 'NotifySmsController@store')->name('notify-sms.store');
        Route::get('notify-sms/create', 'NotifySmsController@create')->name('notify-sms.create');
        Route::get('notify-sms/{id}', 'NotifySmsController@show')->name('notify-sms.show');
        Route::put('notify-sms/{id}', 'NotifySmsController@update')->name('notify-sms.update');
        Route::delete('notify-sms/{id}', 'NotifySmsController@destroy')->name('notify-sms.destroy');
        Route::get('notify-sms/{id}/edit', 'NotifySmsController@edit')->name('notify-sms.edit');
        Route::post('notify-sms/ajax/datatables', 'NotifySmsController@ajaxDataTable')->name('notify-sms.ajaxDataTable');
        Route::patch('notify-sms/ajax/switch', 'NotifySmsController@ajaxSwitch')->name('notify-sms.ajaxSwitch');
        Route::patch('notify-sms/ajax/sort', 'NotifySmsController@ajaxSort')->name('notify-sms.ajaxSort');

    });

});
