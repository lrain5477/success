<?php

namespace Minmax\Notify\Administrator;

use Minmax\Base\Administrator\Repository;
use Minmax\Notify\Models\NotifySms;

/**
 * Class NotifySmsRepository
 * @property NotifySms $model
 * @method NotifySms find($id)
 * @method NotifySms one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method NotifySms[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method NotifySms create($attributes)
 * @method NotifySms save($model, $attributes)
 * @method NotifySms|\Illuminate\Database\Eloquent\Builder query()
 */
class NotifySmsRepository extends Repository
{
    const MODEL = NotifySms::class;

    protected $sort = 'sort';

    protected $sorting = true;

    protected $languageColumns = [
        'title',
        'custom_subject', 'custom_content',
        'admin_subject', 'admin_content',
        'replacements'
    ];

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'notify_sms';
    }

    protected function beforeCreate()
    {
        if (! array_key_exists('receivers', $this->attributes)) {
            $this->attributes['receivers'] = [];
        }
    }

    protected function beforeSave()
    {
        if (count($this->attributes) > 1) {
            if (! array_key_exists('receivers', $this->attributes)) {
                $this->attributes['receivers'] = [];
            }
        }
    }
}
