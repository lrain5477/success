<?php

namespace Minmax\Notify\Administrator;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class NotifySmsRequest
 */
class NotifySmsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'NotifySms.code' => [
                        'required',
                        'string',
                        Rule::unique('notify_sms', 'code')->ignore($this->route('id')),
                    ],
                    'NotifySms.title' => 'required|string',
                    'NotifySms.notifiable' => 'required|boolean',
                    'NotifySms.receivers' => 'nullable|array',
                    'NotifySms.custom_subject' => 'nullable|string',
                    'NotifySms.custom_content' => 'nullable|string',
                    'NotifySms.custom_replacement' => 'nullable|string',
                    'NotifySms.admin_subject' => 'nullable|string',
                    'NotifySms.admin_content' => 'nullable|string',
                    'NotifySms.admin_replacement' => 'nullable|string',
                    'NotifySms.replacements' => 'nullable|array',
                    'NotifySms.queueable' => 'required|boolean',
                    'NotifySms.sort' => 'required|integer',
                    'NotifySms.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [
                    'NotifySms.code' => 'required|string|unique:notify_sms,code',
                    'NotifySms.title' => 'required|string',
                    'NotifySms.notifiable' => 'required|boolean',
                    'NotifySms.receivers' => 'nullable|array',
                    'NotifySms.custom_subject' => 'nullable|string',
                    'NotifySms.custom_content' => 'nullable|string',
                    'NotifySms.custom_replacement' => 'nullable|string',
                    'NotifySms.admin_subject' => 'nullable|string',
                    'NotifySms.admin_content' => 'nullable|string',
                    'NotifySms.admin_replacement' => 'nullable|string',
                    'NotifySms.replacements' => 'nullable|array',
                    'NotifySms.queueable' => 'required|boolean',
                    'NotifySms.sort' => 'nullable|integer',
                    'NotifySms.active' => 'required|boolean',
                ];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'NotifySms.code' => __('MinmaxNotify::models.NotifySms.code'),
            'NotifySms.title' => __('MinmaxNotify::models.NotifySms.title'),
            'NotifySms.notifiable' => __('MinmaxNotify::models.NotifySms.notifiable'),
            'NotifySms.receivers' => __('MinmaxNotify::models.NotifySms.receivers'),
            'NotifySms.custom_subject' => __('MinmaxNotify::models.NotifySms.custom_subject'),
            'NotifySms.custom_content' => __('MinmaxNotify::models.NotifySms.custom_content'),
            'NotifySms.custom_replacement' => __('MinmaxNotify::models.NotifySms.custom_replacement'),
            'NotifySms.admin_subject' => __('MinmaxNotify::models.NotifySms.admin_subject'),
            'NotifySms.admin_content' => __('MinmaxNotify::models.NotifySms.admin_content'),
            'NotifySms.admin_replacement' => __('MinmaxNotify::models.NotifySms.admin_replacement'),
            'NotifySms.replacements' => __('MinmaxNotify::models.NotifySms.replacements'),
            'NotifySms.queueable' => __('MinmaxNotify::models.NotifySms.queueable'),
            'NotifySms.sort' => __('MinmaxNotify::models.NotifySms.sort'),
            'NotifySms.active' => __('MinmaxNotify::models.NotifySms.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('administrator', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('administrator', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
