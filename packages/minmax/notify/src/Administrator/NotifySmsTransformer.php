<?php

namespace Minmax\Notify\Administrator;

use Minmax\Base\Administrator\Transformer;
use Minmax\Notify\Models\NotifySms;

/**
 * Class NotifySmsTransformer
 */
class NotifySmsTransformer extends Transformer
{
    /**
     * Transformer constructor.
     * @param  NotifySmsPresenter $presenter
     * @param  string $uri
     */
    public function __construct(NotifySmsPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  NotifySms $model
     * @return array
     * @throws \Throwable
     */
    public function transform(NotifySms $model)
    {
        return [
            'code' => $this->presenter->getGridText($model, 'code'),
            'title' => $this->presenter->getGridText($model, 'title'),
            'notifiable' => $this->presenter->getGridSwitch($model, 'notifiable'),
            'queueable' => $this->presenter->getGridSwitch($model, 'queueable'),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
