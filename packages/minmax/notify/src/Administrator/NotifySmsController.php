<?php

namespace Minmax\Notify\Administrator;

use Minmax\Base\Administrator\Controller;

/**
 * Class NotifySmsController
 */
class NotifySmsController extends Controller
{
    protected $packagePrefix = 'MinmaxNotify::';

    public function __construct(NotifySmsRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }
}
