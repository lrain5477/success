<?php

namespace Minmax\Notify\Administrator;

use Minmax\Base\Administrator\AdminRepository;
use Minmax\Base\Administrator\Presenter;
use Minmax\Base\Administrator\WebDataRepository;

/**
 * Class NotifySmsPresenter
 */
class NotifySmsPresenter extends Presenter
{
    protected $packagePrefix = 'MinmaxNotify::';

    protected $languageColumns = [
        'title',
        'custom_subject', 'custom_content',
        'admin_subject', 'admin_content',
        'replacements'
    ];

    public function __construct()
    {
        parent::__construct();

        $receivers = [];
        $webReceivers = [];
        $adminReceivers = [];

        if ($webData = (new WebDataRepository)->getData('web')) {
            if (! is_null($webData->system_mobile)) {
                $webReceivers["web_data.system_mobile.{$webData->id}"] = ['title' => __('MinmaxNotify::administrator.form.receivers.sms.system') . " ({$webData->system_mobile})"];
            }
            if (isset($webData->contact['mobile'])) {
                $webReceivers["web_data.contact.{$webData->id}.mobile"] = ['title' => __('MinmaxNotify::administrator.form.receivers.sms.contact') . " ({$webData->contact['mobile']})"];
            }
        }

        foreach ((new AdminRepository)->all('username', '!=', 'sysadmin')->sortBy('name') as $adminData) {
            if (! is_null($adminData->mobile)) {
                $adminReceivers["admin.mobile.{$adminData->id}"] = ['title' => "{$adminData->name} ({$adminData->mobile})"];
            }
        }

        if (count($webReceivers) > 0) {
            $receivers[__('MinmaxNotify::administrator.form.receivers.sms.website')] = $webReceivers;
        }
        if (count($adminReceivers) > 0) {
            $receivers[__('MinmaxNotify::administrator.form.receivers.sms.admin')] = $adminReceivers;
        }

        $this->parameterSet = [
            'receivers' => $receivers,
            'notifiable' => systemParam('notifiable'),
            'queueable' => systemParam('queueable'),
            'active' => systemParam('active'),
        ];
    }
}
