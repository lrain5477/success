<?php

namespace Minmax\Notify\Sms;

/**
 * Abstract Class SmsReplace
 */
abstract class SmsReplace
{
    /**
     * @var \Minmax\Notify\Models\NotifySms $notifySms
     */
    protected $notifySms;
    /**
     * @var array $parameters
     */
    protected $parameters;
    /**
     * @var array $additions
     */
    protected $additions;

    /**
     * @var array $phones
     */
    public $phones;

    /**
     * @var string $content
     */
    public $content;

    /**
     * Create a new message instance.
     *
     * @param  \Minmax\Notify\Models\NotifySms $notifySms
     * @param  array $parameters
     * @param  array $phones
     * @param  array $additions
     * @return void
     * @throws \Exception
     */
    public function __construct($notifySms, $parameters, $phones, $additions)
    {
        $this->notifySms = $notifySms;
        $this->parameters = $parameters;
        $this->additions = $additions;

        $this->to($phones);

        $this->build();
    }

    /**
     * @param  array $phones
     * @return void
     */
    public function to($phones)
    {
        $this->phones = array_wrap($phones);
    }

    /**
     * @param  string $content
     * @return void
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Process Replace.
     *
     * @return void
     * @throws \Exception
     */
    abstract public function build();
}
