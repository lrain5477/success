<?php

namespace Minmax\Notify\Jobs;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifySms
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array $phones
     */
    protected $phones;

    /**
     * @var string $content
     */
    protected $content;

    /**
     * Create a new message instance.
     *
     * @param  \Minmax\Notify\Sms\SmsReplace $replacement
     * @return void
     */
    public function __construct($replacement)
    {
        $this->phones = $replacement->phones;
        $this->content = $replacement->content;
    }

    /**
     * Process Job.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        if ($smsConfig = serviceConfig('sms', true)) {
            if ($senderClass = array_get($smsConfig->options, 'class')) {
                /** @var \Minmax\Newsletter\Services\SmsSender $sender */
                $sender = new $senderClass();

                foreach ($this->phones as $phone) {
                    $result = $sender->send($phone, $this->content);

                    if (is_null($result)) {
                        throw new Exception('SMS send failed.');
                    }
                }
            }
        }
    }
}
