<?php

namespace Minmax\Notify\Admin;

use Minmax\Base\Admin\Controller;

/**
 * Class NotifySmsController
 */
class NotifySmsController extends Controller
{
    protected $packagePrefix = 'MinmaxNotify::';

    public function __construct(NotifySmsRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }
}
