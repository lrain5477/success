<?php

namespace Minmax\Notify\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Minmax\Base\Helpers\Log as LogHelper;

/**
 * Class NotifySmsRequest
 */
class NotifySmsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'PUT':
                return $this->user('admin')->can('notifySmsEdit');
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                return [
                    'NotifySms.title' => 'required|string',
                    'NotifySms.notifiable' => 'required|boolean',
                    'NotifySms.receivers' => 'nullable|array',
                    'NotifySms.custom_subject' => 'nullable|string',
                    'NotifySms.custom_content' => 'nullable|string',
                    'NotifySms.admin_subject' => 'nullable|string',
                    'NotifySms.admin_content' => 'nullable|string',
                    'NotifySms.active' => 'required|boolean',
                ];
            case 'POST':
            default:
                return [];
        }
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'NotifySms.title' => __('MinmaxNotify::models.NotifySms.title'),
            'NotifySms.notifiable' => __('MinmaxNotify::models.NotifySms.notifiable'),
            'NotifySms.receivers' => __('MinmaxNotify::models.NotifySms.receivers'),
            'NotifySms.custom_subject' => __('MinmaxNotify::models.NotifySms.custom_subject'),
            'NotifySms.custom_content' => __('MinmaxNotify::models.NotifySms.custom_content'),
            'NotifySms.admin_subject' => __('MinmaxNotify::models.NotifySms.admin_subject'),
            'NotifySms.admin_content' => __('MinmaxNotify::models.NotifySms.admin_content'),
            'NotifySms.active' => __('MinmaxNotify::models.NotifySms.active'),
        ];
    }

    /**
     * Set up a validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            switch ($this->method()) {
                case 'PUT':
                    LogHelper::system('admin', $this->path(), $this->method(), $this->route('id'), $this->user()->username, 0, $validator->errors()->first());
                    break;
                case 'POST':
                    LogHelper::system('admin', $this->path(), $this->method(), '', $this->user()->username, 0, $validator->errors()->first());
                    break;
            }
        }
    }
}
