<?php

namespace Minmax\Notify\Admin;

use Minmax\Base\Admin\Transformer;
use Minmax\Notify\Models\NotifySms;

/**
 * Class NotifySmsTransformer
 */
class NotifySmsTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'notifySmsShow',
        'U' => 'notifySmsEdit',
    ];

    /**
     * Transformer constructor.
     * @param  NotifySmsPresenter $presenter
     * @param  string $uri
     */
    public function __construct(NotifySmsPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  NotifySms $model
     * @return array
     * @throws \Throwable
     */
    public function transform(NotifySms $model)
    {
        return [
            'sort' => $this->presenter->getGridText($model, 'sort'),
            'title' => $this->presenter->getGridText($model, 'title'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
