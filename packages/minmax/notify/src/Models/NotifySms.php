<?php

namespace Minmax\Notify\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class NotifySms
 * @property integer $id
 * @property string $title
 * @property boolean $notifiable
 * @property array $receivers
 * @property string $custom_subject
 * @property string $custom_content
 * @property string $custom_replacement
 * @property string $admin_subject
 * @property string $admin_content
 * @property string $admin_replacement
 * @property array $replacements
 * @property boolean $queueable
 * @property integer $sort
 * @property boolean $active
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class NotifySms extends Model
{
    protected $table = 'notify_sms';
    protected $guarded = [];
    protected $casts = [
        'notifiable' => 'boolean',
        'receivers' => 'array',
        'queueable' => 'boolean',
        'active' => 'boolean',
    ];

    public function getTitleAttribute()
    {
        return langDB($this->getAttributeFromArray('title'));
    }

    public function getCustomSubjectAttribute()
    {
        return langDB($this->getAttributeFromArray('custom_subject'));
    }

    public function getCustomContentAttribute()
    {
        return langDB($this->getAttributeFromArray('custom_content'));
    }

    public function getAdminSubjectAttribute()
    {
        return langDB($this->getAttributeFromArray('admin_subject'));
    }

    public function getAdminContentAttribute()
    {
        return langDB($this->getAttributeFromArray('admin_content'));
    }

    public function getReplacementsAttribute()
    {
        return json_decode(langDB($this->getAttributeFromArray('replacements')), true);
    }
}
