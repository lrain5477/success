<?php

namespace Minmax\Notify\Events;

/**
 * Trait SmsNotifyDefault
 */
trait SmsNotifyDefault
{
    /**
     * @var array $smsParameters
     */
    public $smsParameters = [];

    /**
     * @var array $phones
     */
    public $phones;

    /**
     * @var array $additions
     */
    public $additions = [];
}
