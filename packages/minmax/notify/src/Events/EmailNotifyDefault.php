<?php

namespace Minmax\Notify\Events;

/**
 * Trait EmailNotifyDefault
 */
trait EmailNotifyDefault
{
    /**
     * @var array $mailerParameters
     */
    public $mailerParameters = [];

    /**
     * @var array $emails
     */
    public $emails;

    /**
     * @var array $additions
     */
    public $additions = [];
}
