<?php

namespace Minmax\Notify\Listeners;

use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Minmax\Notify\Jobs\NotifySms as NotifySmsJob;
use Minmax\Notify\Models\NotifySms;

/**
 * Class SmsNotification
 */
class SmsNotification implements ShouldQueue
{
    /**
     * Queue list name
     *
     * @var string $queue
     */
    public $queue = 'listeners';

    /**
     * @var NotifySms $notify
     */
    public $notify;

    /**
     * Create SmsNotification event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param  mixed $event
     * @return void
     */
    public function handle($event)
    {
        if ($notify = $event->notifySms) {
            /** @var \Minmax\Notify\Models\NotifySms $notify */
            $replacementClass = $notify->custom_replacement;

            if ($notify->notifiable && isset($replacementClass)) {
                /** @var \Minmax\Notify\Sms\SmsReplace $replacement */
                $replacement = new $replacementClass($notify, $event->smsParameters, $event->phones, $event->additions);

                if ($notify->queueable) {
                    NotifySmsJob::dispatch($replacement)->onQueue('sms');
                } else {
                    try {
                        (new NotifySmsJob($replacement))->handle();
                    } catch (Exception $e) {}
                }
            }
        }
    }
}
