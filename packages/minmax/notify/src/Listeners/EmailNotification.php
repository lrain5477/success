<?php

namespace Minmax\Notify\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Minmax\Notify\Models\NotifyEmail;

/**
 * Class EmailNotification
 */
class EmailNotification implements ShouldQueue
{
    /**
     * Queue list name
     *
     * @var string $queue
     */
    public $queue = 'listeners';

    /**
     * @var NotifyEmail $notify
     */
    public $notify;

    /**
     * Create EmailNotification event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param  mixed $event
     * @return void
     */
    public function handle($event)
    {
        sendNotifyEmail($event->notifyEmail, $event->emails, $event->mailerParameters, $event->additions);
    }
}
