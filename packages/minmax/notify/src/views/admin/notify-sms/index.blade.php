<?php
/**
 * List of model NotifySms
 *
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \Minmax\Notify\Models\NotifySms $formData
 */
?>

@extends('MinmaxBase::admin.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links')
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Notify\Admin\NotifySmsPresenter')

@section('grid-filter')
    @component('MinmaxBase::admin.layouts.grid.filter-keyword')
    <option value="title">@lang('MinmaxNotify::models.NotifySms.title')</option>
    @endcomponent

    @component('MinmaxBase::admin.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('active', 'searchActive', ['emptyLabel' => __('MinmaxNotify::models.NotifySms.active')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="w-5">No.</th>
        <th class="w-75">@lang('MinmaxNotify::models.NotifySms.title')</th>
        <th>@lang('MinmaxNotify::models.NotifySms.active')</th>
        <th class="nosort">@lang('MinmaxBase::admin.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'sort', name: 'sort'},
            {data: 'title', name: 'title'},
            {data: 'active', name: 'active'},
            {data: 'action', name: 'action'}
        ],
        ['title'],
        {"active":"searchActive"},
        [[0, 'asc']],
        '{{ langRoute("admin.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("admin.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
