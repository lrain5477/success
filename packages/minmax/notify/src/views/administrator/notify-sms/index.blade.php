<?php
/**
 * List of model NotifySms
 *
 * @var \Minmax\Base\Models\Administrator $adminData
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 * @var \Minmax\Notify\Models\NotifySms $formData
 */
?>

@extends('MinmaxBase::administrator.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::administrator.layouts.right-links')
    <a class="btn btn-sm btn-main" href="{{ langRoute("administrator.{$pageData->uri}.create") }}" title="@lang('MinmaxBase::administrator.form.create')">
        <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.create')</span>
    </a>
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Notify\Administrator\NotifySmsPresenter')

@section('grid-filter')
    @component('MinmaxBase::administrator.layouts.grid.filter-keyword')
    <option value="code">@lang('MinmaxNotify::models.NotifySms.code')</option>
    <option value="title">@lang('MinmaxNotify::models.NotifySms.title')</option>
    @endcomponent

    @component('MinmaxBase::administrator.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('notifiable', 'searchNotify', ['emptyLabel' => __('MinmaxNotify::models.NotifySms.notifiable')]) !!}
    {!! $modelPresenter->getFilterSelection('queueable', 'searchQueue', ['emptyLabel' => __('MinmaxNotify::models.NotifySms.queueable')]) !!}
    {!! $modelPresenter->getFilterSelection('active', 'searchActive', ['emptyLabel' => __('MinmaxNotify::models.NotifySms.active')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="w-20">@lang('MinmaxNotify::models.NotifySms.code')</th>
        <th class="w-20">@lang('MinmaxNotify::models.NotifySms.title')</th>
        <th>@lang('MinmaxNotify::models.NotifySms.notifiable')</th>
        <th>@lang('MinmaxNotify::models.NotifySms.queueable')</th>
        <th>@lang('MinmaxNotify::models.NotifySms.sort')</th>
        <th>@lang('MinmaxNotify::models.NotifySms.active')</th>
        <th class="nosort">@lang('MinmaxBase::administrator.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'code', name: 'code'},
            {data: 'title', name: 'title'},
            {data: 'notifiable', name: 'notifiable'},
            {data: 'queueable', name: 'queueable'},
            {data: 'sort', name: 'sort'},
            {data: 'active', name: 'active'},
            {data: 'action', name: 'action'}
        ],
        ['code', 'title'],
        {"notifiable":"searchNotify", "queueable":"searchQueue", "active":"searchActive"},
        [[4, 'asc']],
        '{{ langRoute("administrator.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("administrator.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
