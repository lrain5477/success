<?php
/**
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 */
?>

@extends('MinmaxBase::administrator.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::administrator.layouts.right-links')
    <a class="btn btn-sm btn-main" href="{{ langRoute("administrator.{$pageData->uri}.create") }}" title="@lang('MinmaxBase::administrator.form.create')">
        <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.create')</span>
    </a>
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\Notify\Administrator\NotifyEmailPresenter')

@section('grid-filter')
    @component('MinmaxBase::administrator.layouts.grid.filter-keyword')
    <option value="code">@lang('MinmaxNotify::models.NotifyEmail.code')</option>
    <option value="title">@lang('MinmaxNotify::models.NotifyEmail.title')</option>
    @endcomponent

    @component('MinmaxBase::administrator.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('notifiable', 'searchNotify', ['emptyLabel' => __('MinmaxNotify::models.NotifyEmail.notifiable')]) !!}
    {!! $modelPresenter->getFilterSelection('queueable', 'searchQueue', ['emptyLabel' => __('MinmaxNotify::models.NotifyEmail.queueable')]) !!}
    {!! $modelPresenter->getFilterSelection('active', 'searchActive', ['emptyLabel' => __('MinmaxNotify::models.NotifyEmail.active')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="w-20">@lang('MinmaxNotify::models.NotifyEmail.code')</th>
        <th class="w-20">@lang('MinmaxNotify::models.NotifyEmail.title')</th>
        <th>@lang('MinmaxNotify::models.NotifyEmail.notifiable')</th>
        <th>@lang('MinmaxNotify::models.NotifyEmail.queueable')</th>
        <th>@lang('MinmaxNotify::models.NotifyEmail.sort')</th>
        <th>@lang('MinmaxNotify::models.NotifyEmail.active')</th>
        <th class="nosort">@lang('MinmaxBase::administrator.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'code', name: 'code'},
            {data: 'title', name: 'title'},
            {data: 'notifiable', name: 'notifiable'},
            {data: 'queueable', name: 'queueable'},
            {data: 'sort', name: 'sort'},
            {data: 'active', name: 'active'},
            {data: 'action', name: 'action'}
        ],
        ['code', 'title'],
        {"notifiable":"searchNotify", "queueable":"searchQueue", "active":"searchActive"},
        [[4, 'asc']],
        '{{ langRoute("administrator.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("administrator.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
