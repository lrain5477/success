<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    'form' => [
        'fieldSet' => [
            'email' => [
                'content' => '信件設定',
                'replacement' => '代碼說明',
            ],
            'sms' => [
                'content' => '簡訊設定',
                'replacement' => '代碼說明',
            ],
        ],
        'tab' => [
            'email' => [
                'custom' => '用戶信件',
                'admin' => '系統信件',
            ],
            'sms' => [
                'custom' => '用戶簡訊',
                'admin' => '系統簡訊',
            ],
        ],
        'receivers' => [
            'email' => [
                'website' => '網站信箱',
                'admin' => '使用者帳戶',
                'system' => '系統信箱',
                'contact' => '客服信箱',
            ],
            'sms' => [
                'website' => '網站電話',
                'admin' => '使用者帳戶',
                'system' => '系統電話',
                'contact' => '客服電話',
            ],
        ]
    ],

];
