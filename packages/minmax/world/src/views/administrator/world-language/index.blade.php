<?php
/**
 * @var \Minmax\Base\Models\AdministratorMenu $pageData
 */
?>

@extends('MinmaxBase::administrator.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::administrator.layouts.right-links')
    <a class="btn btn-sm btn-main" href="{{ langRoute("administrator.{$pageData->uri}.create") }}" title="@lang('MinmaxBase::administrator.form.create')">
        <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::administrator.form.create')</span>
    </a>
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\World\Administrator\WorldLanguagePresenter')

@section('grid-filter')
    @component('MinmaxBase::administrator.layouts.grid.filter-keyword')
    <option value="name">@lang('MinmaxWorld::models.WorldLanguage.name')</option>
    <option value="code">@lang('MinmaxWorld::models.WorldLanguage.code')</option>
    @endcomponent

    @component('MinmaxBase::administrator.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('active_admin', 'searchAdmin', ['emptyLabel' => __('MinmaxWorld::models.WorldLanguage.active_admin')]) !!}
    {!! $modelPresenter->getFilterSelection('active', 'searchActive', ['emptyLabel' => __('MinmaxWorld::models.WorldLanguage.active')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="w-25">@lang('MinmaxWorld::models.WorldLanguage.name')</th>
        <th class="w-25">@lang('MinmaxWorld::models.WorldLanguage.code')</th>
        <th>@lang('MinmaxWorld::models.WorldLanguage.sort')</th>
        <th>@lang('MinmaxWorld::models.WorldLanguage.active_admin')</th>
        <th>@lang('MinmaxWorld::models.WorldLanguage.active')</th>
        <th class="nosort">@lang('MinmaxBase::administrator.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'name', name: 'name'},
            {data: 'code', name: 'code'},
            {data: 'sort', name: 'sort'},
            {data: 'active_admin', name: 'active_admin'},
            {data: 'active', name: 'active'},
            {data: 'action', name: 'action'}
        ],
        ['name', 'code'],
        {"active_admin":"searchAdmin", "active":"searchActive"},
        [[2, 'asc']],
        '{{ langRoute("administrator.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("administrator.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
