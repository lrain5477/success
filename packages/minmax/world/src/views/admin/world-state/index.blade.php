<?php
/**
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 */
?>

@extends('MinmaxBase::admin.layouts.page.index')

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links')
        @if($adminData->can('worldStateCreate'))
        <a class="btn btn-sm btn-main" href="{{ langRoute("admin.{$pageData->uri}.create") }}" title="@lang('MinmaxBase::admin.form.create')">
            <i class="icon-plus2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.create')</span>
        </a>
        @endif
    @endcomponent
@endsection

@inject('modelPresenter', 'Minmax\World\Admin\WorldStatePresenter')

@section('grid-filter')
    @component('MinmaxBase::admin.layouts.grid.filter-keyword')
    <option value="title">@lang('MinmaxWorld::models.WorldState.title')</option>
    <option value="code">@lang('MinmaxWorld::models.WorldState.code')</option>
    @endcomponent

    @component('MinmaxBase::admin.layouts.grid.filter-equal')
    {!! $modelPresenter->getFilterSelection('country_id', 'searchCountry', ['emptyLabel' => __('MinmaxWorld::models.WorldState.country_id')]) !!}
    {!! $modelPresenter->getFilterSelection('active', 'searchActive', ['emptyLabel' => __('MinmaxWorld::models.WorldState.active')]) !!}
    @endcomponent
@endsection

@section('grid-table')
<table class="table table-responsive-md table-bordered table-striped table-hover table-checkable datatables" id="tableList">
    <thead>
    <tr role="row">
        <th class="w-20">@lang('MinmaxWorld::models.WorldState.country_id')</th>
        <th class="w-20">@lang('MinmaxWorld::models.WorldState.title')</th>
        <th class="w-20">@lang('MinmaxWorld::models.WorldState.code')</th>
        <th>@lang('MinmaxWorld::models.WorldState.sort')</th>
        <th>@lang('MinmaxWorld::models.WorldState.active')</th>
        <th class="nosort">@lang('MinmaxBase::admin.grid.title.action')</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@push('scripts')
<script>
$(function() {
    datatableInit(
        [
            {data: 'country_id', name: 'country_id'},
            {data: 'title', name: 'title'},
            {data: 'code', name: 'code'},
            {data: 'sort', name: 'sort'},
            {data: 'active', name: 'active'},
            {data: 'action', name: 'action'}
        ],
        ['title', 'code'],
        {"country_id":"searchCountry", "active":"searchActive"},
        [[0, 'asc'], [3, 'asc']],
        '{{ langRoute("admin.{$pageData->uri}.ajaxDataTable") }}',
        '{{ asset("static/admin/js/lang/" . app()->getLocale() . "/datatables.json") }}',
        JSON.parse("{!! request('filters') == 1 ? addslashes(json_encode(session("admin.{$pageData->uri}.datatable", []))) : '{}' !!}")
    );
});
</script>
@endpush
