<?php

namespace Minmax\World\Administrator;

use Illuminate\Routing\Controller as BaseController;

/**
 * Class HelperController
 */
class HelperController extends BaseController
{
    /**
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function getCountries($id)
    {
        $countries = (new WorldCountryRepository)->query()
            ->where('continent_id', $id)
            ->where('active', true)
            ->orderBy('sort')
            ->orderBy('title')
            ->get()
            ->map(function ($country) {
                /** @var \Minmax\World\Models\WorldCountry $country */
                return ['id' => $country->id, 'name' => $country->name];
            })
            ->toArray();

        return response($countries, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function getStates($id)
    {
        $countries = (new WorldStateRepository)->query()
            ->where('country_id', $id)
            ->where('active', true)
            ->orderBy('sort')
            ->get()
            ->map(function ($state) {
                /** @var \Minmax\World\Models\WorldState $state */
                return ['id' => $state->id, 'name' => $state->name];
            })
            ->toArray();

        return response($countries, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function getCounties($id)
    {
        $countries = (new WorldCountyRepository)->query()
            ->where('state_id', $id)
            ->where('active', true)
            ->orderBy('sort')
            ->get()
            ->map(function ($county) {
                /** @var \Minmax\World\Models\WorldCounty $county */
                return ['id' => $county->id, 'name' => $county->name];
            })
            ->toArray();

        return response($countries, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function getCities($id)
    {
        $countries = (new WorldCityRepository)->query()
            ->where('county_id', $id)
            ->where('active', true)
            ->orderBy('sort')
            ->get()
            ->map(function ($city) {
                /** @var \Minmax\World\Models\WorldCity $city */
                return ['id' => $city->id, 'name' => $city->name, 'zip' => $city->code];
            })
            ->toArray();

        return response($countries, 200, ['Content-Type' => 'application/json']);
    }
}
