<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class CreateWorldTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Continent / 大洲
        Schema::create('world_continent', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 128)->comment('大洲名稱');
            $table->string('code', 16)->comment('大洲代碼');
            $table->string('name')->comment('顯示文字');
            $table->json('options')->nullable()->comment('大洲設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('狀態');
            $table->timestamps();
        });

        // Country / 國家
        Schema::create('world_country', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('continent_id')->comment('大洲ID');
            $table->string('title', 128)->comment('國家名稱');
            $table->string('code', 16)->comment('國家代碼');
            $table->string('name')->comment('顯示文字');
            $table->json('options')->nullable()->comment('國家設定');
            $table->unsignedInteger('language_id')->nullable()->comment('語系ID');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('狀態');
            $table->timestamps();

            $table->foreign('continent_id')->references('id')->on('world_continent')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // State / 州區
        Schema::create('world_state', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('country_id')->comment('國家ID');
            $table->string('title', 128)->comment('州區名稱');
            $table->string('code', 16)->comment('州區代碼');
            $table->string('name')->comment('顯示文字');
            $table->json('options')->nullable()->comment('州區設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('狀態');
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('world_country')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // County / 縣市郡
        Schema::create('world_county', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('state_id')->comment('州區ID');
            $table->string('title', 128)->comment('縣市名稱');
            $table->string('code', 32)->comment('縣市代碼');
            $table->string('name')->comment('顯示文字');
            $table->json('options')->nullable()->comment('縣市設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('狀態');
            $table->timestamps();

            $table->foreign('state_id')->references('id')->on('world_state')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // City / 鄉鎮市區
        Schema::create('world_city', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('county_id')->comment('縣市ID');
            $table->string('title', 128)->comment('城市名稱');
            $table->string('code', 16)->comment('城市代碼');
            $table->string('name')->comment('顯示文字');
            $table->json('options')->nullable()->comment('城市設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('狀態');
            $table->timestamps();

            $table->foreign('county_id')->references('id')->on('world_county')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // 銀行
        Schema::create('world_bank', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 128)->comment('銀行名稱');
            $table->string('code', 16)->comment('銀行代碼');
            $table->string('name')->comment('顯示文字');
            $table->json('options')->nullable()->comment('銀行設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('狀態');
            $table->timestamps();
        });

        // 貨幣
        Schema::create('world_currency', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 128)->comment('貨幣名稱');
            $table->string('code', 16)->unique()->comment('貨幣代碼');
            $table->string('name')->comment('顯示文字');
            $table->json('options')->comment('貨幣設定');
            $table->unsignedInteger('sort')->default(1)->comment('排序');
            $table->boolean('active')->default(true)->comment('狀態');
            $table->timestamps();
        });

        Schema::table('world_language', function (Blueprint $table) {
            $table->unsignedInteger('currency_id')->default(1)->after('options')->comment('貨幣ID');
        });

        // 建立預設資料
        $this->insertDatabase();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->deleteDatabase();

        Schema::table('world_language', function (Blueprint $table) {
            $table->dropColumn('currency_id');
        });

        Schema::dropIfExists('world_currency');
        Schema::dropIfExists('world_bank');
        Schema::dropIfExists('world_city');
        Schema::dropIfExists('world_county');
        Schema::dropIfExists('world_state');
        Schema::dropIfExists('world_country');
        Schema::dropIfExists('world_continent');
    }

    /**
     * Insert default data
     *
     * @return void
     */
    protected function insertDatabase()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        // 全球化 - 大洲
        $startContinentId = $rowContinentId = SeederHelper::getTableNextIncrement('world_continent');
        $rowContinentId--;

        $worldContinentData = [
            ['title' => '亞洲', 'code' => 'AS', 'name' => 'world_continent.name.' . ++$rowContinentId, 'options' => json_encode(['country_color' => '#eea638']), 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['title' => '北美洲', 'code' => 'NA', 'name' => 'world_continent.name.' . ++$rowContinentId, 'options' => json_encode(['country_color' => '#a7a737']), 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['title' => '南美洲', 'code' => 'SA', 'name' => 'world_continent.name.' . ++$rowContinentId, 'options' => json_encode(['country_color' => '#86a965']), 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['title' => '歐洲', 'code' => 'EU', 'name' => 'world_continent.name.' . ++$rowContinentId, 'options' => json_encode(['country_color' => '#d8854f']), 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['title' => '非洲', 'code' => 'AF', 'name' => 'world_continent.name.' . ++$rowContinentId, 'options' => json_encode(['country_color' => '#de4c4f']), 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['title' => '大洋洲', 'code' => 'OC', 'name' => 'world_continent.name.' . ++$rowContinentId, 'options' => json_encode(['country_color' => '#8aabb0']), 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['title' => '南極洲', 'code' => 'AN', 'name' => 'world_continent.name.' . ++$rowContinentId, 'options' => json_encode(['country_color' => '#3598dc']), 'created_at' => $timestamp, 'updated_at' => $timestamp],
        ];
        DB::table('world_continent')->insert($worldContinentData);

        // 多語系
        $continentLanguage = [
            'zh-Hant' => [
                ['name' => '亞洲'], ['name' => '北美洲'], ['name' => '南美洲'], ['name' => '歐洲'], ['name' => '非洲'], ['name' => '大洋洲'], ['name' => '南極洲']
            ],
            'zh-Hans' => [
                ['name' => '亞洲'], ['name' => '北美洲'], ['name' => '南美洲'], ['name' => '欧洲'], ['name' => '非洲'], ['name' => '大洋洲'], ['name' => '南极洲']
            ],
            'ja' => [
                ['name' => 'アジア'], ['name' => '北米'], ['name' => '南米'], ['name' => 'ヨーロッパ'], ['name' => 'アフリカ'], ['name' => 'オセアニア'], ['name' => '南极洲']
            ],
            'en' => [
                ['name' => 'Asia'], ['name' => 'North America'], ['name' => 'South America'], ['name' => 'Europe'], ['name' => 'Africa'], ['name' => 'Oceania'], ['name' => 'Antarctica']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'world_continent', $continentLanguage, $languageList, $startContinentId, false);

        // 全球化 - 國家
        $startCountryId = $rowCountryId = SeederHelper::getTableNextIncrement('world_country');
        $rowCountryId--;

        $worldCountryData = [
            ['continent_id' => $startContinentId, 'title' => 'Afghanistan', 'code' => 'AF', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-af', 'lat' => '33', 'lng' => '65']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Armenia', 'code' => 'AM', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-am', 'lat' => '40', 'lng' => '45']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Azerbaijan', 'code' => 'AZ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-az', 'lat' => '40.5', 'lng' => '47.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Bahrain', 'code' => 'BH', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-bh', 'lat' => '26', 'lng' => '50.55']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Bangladesh', 'code' => 'BD', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-bd', 'lat' => '24', 'lng' => '90']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Bhutan', 'code' => 'BT', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-bt', 'lat' => '27.5', 'lng' => '90.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Brunei', 'code' => 'BN', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-bn', 'lat' => '4.5', 'lng' => '114.6667']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Cambodia', 'code' => 'KH', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-kh', 'lat' => '13', 'lng' => '105']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'China', 'code' => 'CN', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-cn', 'lat' => '35', 'lng' => '105']), 'language_id' => $languageList['zh-Hans'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Georgia', 'code' => 'GE', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ge', 'lat' => '42', 'lng' => '43.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Hong Kong', 'code' => 'HK', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-hk', 'lat' => '22.25', 'lng' => '114.1667']), 'language_id' => $languageList['zh-Hant'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'India', 'code' => 'IN', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-in', 'lat' => '20', 'lng' => '77']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Indonesia', 'code' => 'ID', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-id', 'lat' => '-5', 'lng' => '120']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Iran', 'code' => 'IR', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ir', 'lat' => '32', 'lng' => '53']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Iraq', 'code' => 'IQ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-iq', 'lat' => '33', 'lng' => '44']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Israel', 'code' => 'IL', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-il', 'lat' => '31.5', 'lng' => '34.75']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Japan', 'code' => 'JP', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-jp', 'lat' => '36', 'lng' => '138']), 'language_id' => $languageList['ja'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Jordan', 'code' => 'JO', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-jo', 'lat' => '31', 'lng' => '36']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Kazakhstan', 'code' => 'KZ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-kz', 'lat' => '48', 'lng' => '68']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Kuwait', 'code' => 'KW', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-kw', 'lat' => '29.3375', 'lng' => '47.6581']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Kyrgyzstan', 'code' => 'KG', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-kg', 'lat' => '41', 'lng' => '75']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Laos', 'code' => 'LA', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-la', 'lat' => '18', 'lng' => '105']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Lebanon', 'code' => 'LB', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-lb', 'lat' => '33.8333', 'lng' => '35.8333']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Macao', 'code' => 'MO', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-mo', 'lat' => '22.1667', 'lng' => '113.55']), 'language_id' => $languageList['zh-Hans'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Malaysia', 'code' => 'MY', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-my', 'lat' => '2.5', 'lng' => '112.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Mongolia', 'code' => 'MN', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-mn', 'lat' => '46', 'lng' => '105']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Myanmar', 'code' => 'MM', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-mm', 'lat' => '22', 'lng' => '98']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Nepal', 'code' => 'NP', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-np', 'lat' => '28', 'lng' => '84']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'North Korea', 'code' => 'KP', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-kp', 'lat' => '40', 'lng' => '127']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Oman', 'code' => 'OM', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-om', 'lat' => '21', 'lng' => '57']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Pakistan', 'code' => 'PK', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-pk', 'lat' => '30', 'lng' => '70']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Philippines', 'code' => 'PH', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ph', 'lat' => '13', 'lng' => '122']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Qatar', 'code' => 'QA', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-qa', 'lat' => '25.5', 'lng' => '51.25']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Saudi Arabia', 'code' => 'SA', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-sa', 'lat' => '25', 'lng' => '45']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Singapore', 'code' => 'SG', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-sg', 'lat' => '1.3667', 'lng' => '103.8']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'South Korea', 'code' => 'KR', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-kr', 'lat' => '37', 'lng' => '127.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Sri Lanka', 'code' => 'LK', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-lk', 'lat' => '7', 'lng' => '81']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Syria', 'code' => 'SY', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-sy', 'lat' => '35', 'lng' => '38']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Taiwan', 'code' => 'TW', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-tw', 'lat' => '23.5', 'lng' => '121']), 'language_id' => $languageList['zh-Hant'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Tajikistan', 'code' => 'TJ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-tj', 'lat' => '39', 'lng' => '71']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Thailand', 'code' => 'TH', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-th', 'lat' => '15', 'lng' => '100']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Turkmenistan', 'code' => 'TM', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-tm', 'lat' => '40', 'lng' => '60']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'United Arab Emirates', 'code' => 'AE', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ae', 'lat' => '24', 'lng' => '54']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Uzbekistan', 'code' => 'UZ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-uz', 'lat' => '41', 'lng' => '64']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'West Bank and Gaza', 'code' => 'PS', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ps', 'lat' => '32', 'lng' => '35.25']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Vietnam', 'code' => 'VN', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-vn', 'lat' => '16', 'lng' => '106']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId, 'title' => 'Yemen', 'code' => 'YE', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ye', 'lat' => '15', 'lng' => '48']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Antigua and Barbuda', 'code' => 'AG', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ag', 'lat' => '17.05', 'lng' => '-61.8']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Anguilla', 'code' => 'AI', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ai', 'lat' => '18.25', 'lng' => '-63.1667']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Bermuda', 'code' => 'BM', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-bm', 'lat' => '32.3333', 'lng' => '-64.75']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Canada', 'code' => 'CA', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ca', 'lat' => '54', 'lng' => '-100']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Commonwealth of the Bahamas', 'code' => 'BS', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-bs', 'lat' => '24.25', 'lng' => '-76']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Costa Rica', 'code' => 'CR', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-cr', 'lat' => '10', 'lng' => '-84']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Cuba', 'code' => 'CU', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-cu', 'lat' => '21.5', 'lng' => '-80']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Dominican', 'code' => 'DO', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-do', 'lat' => '19', 'lng' => '-70.6667']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'El Salvador', 'code' => 'SV', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-sv', 'lat' => '13.8333', 'lng' => '-88.9167']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Guatemala', 'code' => 'GT', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-gt', 'lat' => '15.5', 'lng' => '-90.25']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Haiti', 'code' => 'HT', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ht', 'lat' => '19', 'lng' => '-72.4167']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Honduras', 'code' => 'HN', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-hn', 'lat' => '15', 'lng' => '-86.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Jamaica', 'code' => 'JM', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-jm', 'lat' => '18.25', 'lng' => '-77.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Mexico', 'code' => 'MX', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-mx', 'lat' => '23', 'lng' => '-102']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Nicaragua', 'code' => 'NI', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ni', 'lat' => '13', 'lng' => '-85']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Panama', 'code' => 'PA', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-pa', 'lat' => '9', 'lng' => '-80']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Puerto Rico', 'code' => 'PR', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-pr', 'lat' => '18.25', 'lng' => '-66.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'Trinidad and Tobago', 'code' => 'TT', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-tt', 'lat' => '11', 'lng' => '-61']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 1, 'title' => 'United States', 'code' => 'US', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-us', 'lat' => '38', 'lng' => '-97']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 2, 'title' => 'Argentina', 'code' => 'AR', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ar', 'lat' => '-34', 'lng' => '-64']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 2, 'title' => 'Aruba', 'code' => 'AW', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-aw', 'lat' => '12.5', 'lng' => '-69.9667']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 2, 'title' => 'Barbados', 'code' => 'BB', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-bb', 'lat' => '13.1667', 'lng' => '-59.5333']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 2, 'title' => 'Bolivia', 'code' => 'BO', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-bo', 'lat' => '-17', 'lng' => '-65']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 2, 'title' => 'Brazil', 'code' => 'BR', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-br', 'lat' => '-10', 'lng' => '-55']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 2, 'title' => 'Chile', 'code' => 'CL', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-cl', 'lat' => '-30', 'lng' => '-71']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 2, 'title' => 'Colombia', 'code' => 'CO', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-co', 'lat' => '4', 'lng' => '-72']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 2, 'title' => 'Ecuador', 'code' => 'EC', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ec', 'lat' => '-2', 'lng' => '-77.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 2, 'title' => 'Guyana', 'code' => 'GY', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-gy', 'lat' => '5', 'lng' => '-59']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 2, 'title' => 'Paraguay', 'code' => 'PY', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-py', 'lat' => '-23', 'lng' => '-58']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 2, 'title' => 'Peru', 'code' => 'PE', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-pe', 'lat' => '-10', 'lng' => '-76']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 2, 'title' => 'Suriname', 'code' => 'SR', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-sr', 'lat' => '4', 'lng' => '-56']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 2, 'title' => 'Uruguay', 'code' => 'UY', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-uy', 'lat' => '-33', 'lng' => '-56']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 2, 'title' => 'Venezuela', 'code' => 'VE', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ve', 'lat' => '8', 'lng' => '-66']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Albania', 'code' => 'AL', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-al', 'lat' => '41', 'lng' => '20']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Andorra', 'code' => 'AD', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ad', 'lat' => '42.5', 'lng' => '1.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Austria', 'code' => 'AT', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-at', 'lat' => '47.3333', 'lng' => '13.3333']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Belarus', 'code' => 'BY', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-by', 'lat' => '53', 'lng' => '28']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Belgium', 'code' => 'BE', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-be', 'lat' => '50.8333', 'lng' => '4']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Bosnia and Herzegovina', 'code' => 'BA', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ba', 'lat' => '44', 'lng' => '18']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Bulgaria', 'code' => 'BG', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-bg', 'lat' => '43', 'lng' => '25']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Croatia', 'code' => 'HR', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-hr', 'lat' => '45.1667', 'lng' => '15.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Cyprus', 'code' => 'CY', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-cy', 'lat' => '35', 'lng' => '33']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Czech', 'code' => 'CZ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-cz', 'lat' => '49.75', 'lng' => '15.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Denmark', 'code' => 'DK', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-dk', 'lat' => '56', 'lng' => '10']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Estonia', 'code' => 'EE', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ee', 'lat' => '59', 'lng' => '26']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Finland', 'code' => 'FI', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-fi', 'lat' => '62', 'lng' => '26']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'France', 'code' => 'FR', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-fr', 'lat' => '46', 'lng' => '2']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Germany', 'code' => 'GE', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ge', 'lat' => '42', 'lng' => '43.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Greece', 'code' => 'GR', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-gr', 'lat' => '39', 'lng' => '22']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Hungary', 'code' => 'HU', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-hu', 'lat' => '47', 'lng' => '20']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Iceland', 'code' => 'IS', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-is', 'lat' => '65', 'lng' => '-18']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Ireland', 'code' => 'IE', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ie', 'lat' => '53', 'lng' => '-8']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Italy', 'code' => 'IT', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-it', 'lat' => '42.8333', 'lng' => '12.8333']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Latvia', 'code' => 'LV', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-lv', 'lat' => '57', 'lng' => '25']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Lithuania', 'code' => 'LT', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-lt', 'lat' => '55', 'lng' => '24']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Luxembourg', 'code' => 'LU', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-lu', 'lat' => '49.75', 'lng' => '6']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Macedonia', 'code' => 'MK', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-mk', 'lat' => '41.8333', 'lng' => '22']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Moldova', 'code' => 'MD', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-md', 'lat' => '47', 'lng' => '29']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Montenegro', 'code' => 'ME', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-me', 'lat' => '42.5', 'lng' => '19.4']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Netherlands', 'code' => 'NL', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-nl', 'lat' => '52.5', 'lng' => '5.75']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Norway', 'code' => 'NO', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-no', 'lat' => '62', 'lng' => '10']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Poland', 'code' => 'PL', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-pl', 'lat' => '52', 'lng' => '20']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Portugal', 'code' => 'PT', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-pt', 'lat' => '39.5', 'lng' => '-8']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Romania', 'code' => 'RO', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ro', 'lat' => '46', 'lng' => '25']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Russia', 'code' => 'RU', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ru', 'lat' => '60', 'lng' => '100']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Serbia', 'code' => 'RS', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-rs', 'lat' => '44', 'lng' => '21']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Slovak', 'code' => 'SK', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-sk', 'lat' => '48.6667', 'lng' => '19.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Slovenia', 'code' => 'SI', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-si', 'lat' => '46', 'lng' => '15']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Spain', 'code' => 'ES', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-es', 'lat' => '40', 'lng' => '-4']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Sweden', 'code' => 'SE', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-se', 'lat' => '62', 'lng' => '15']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Switzerland', 'code' => 'CH', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ch', 'lat' => '47', 'lng' => '8']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Turkey', 'code' => 'TR', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-tr', 'lat' => '39', 'lng' => '35']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'Ukraine', 'code' => 'UA', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ua', 'lat' => '49', 'lng' => '32']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 3, 'title' => 'United Kingdom', 'code' => 'GB', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-gb', 'lat' => '54', 'lng' => '-2']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Algeria', 'code' => 'DZ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-dz', 'lat' => '28', 'lng' => '3']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Angola', 'code' => 'AO', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ao', 'lat' => '-12.5', 'lng' => '18.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Benin', 'code' => 'BJ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-bj', 'lat' => '9.5', 'lng' => '2.25']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Botswana', 'code' => 'BW', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-bw', 'lat' => '-22', 'lng' => '24']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Burkina Faso', 'code' => 'BF', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-bf', 'lat' => '13', 'lng' => '-2']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Burundi', 'code' => 'BI', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-bi', 'lat' => '-3.5', 'lng' => '30']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Cameroon', 'code' => 'CM', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-cm', 'lat' => '6', 'lng' => '12']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Cape Verde', 'code' => 'CV', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-cv', 'lat' => '16', 'lng' => '-24']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Central African', 'code' => 'CF', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-cf', 'lat' => '7', 'lng' => '21']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Chad', 'code' => 'TD', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-td', 'lat' => '15', 'lng' => '19']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Comoros', 'code' => 'KM', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-km', 'lat' => '-12.1667', 'lng' => '44.25']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Congo Dem.', 'code' => 'CD', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-cd', 'lat' => '0', 'lng' => '25']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Congo', 'code' => 'CG', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-cg', 'lat' => '-1', 'lng' => '15']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => "Cote d'Ivoire", 'code' => 'CI', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ci', 'lat' => '8', 'lng' => '-5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Djibouti', 'code' => 'DJ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-dj', 'lat' => '11.5', 'lng' => '43']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Egypt', 'code' => 'EG', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-eg', 'lat' => '27', 'lng' => '30']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Equatorial Guinea', 'code' => 'GQ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-gq', 'lat' => '2', 'lng' => '10']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Eritrea', 'code' => 'ER', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-er', 'lat' => '15', 'lng' => '39']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Ethiopia', 'code' => 'ET', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-et', 'lat' => '8', 'lng' => '38']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Gabon', 'code' => 'GA', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ga', 'lat' => '-1', 'lng' => '11.75']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Gambia', 'code' => 'GM', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-gm', 'lat' => '13.4667', 'lng' => '-16.5667']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Ghana', 'code' => 'GH', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-gh', 'lat' => '8', 'lng' => '-2']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Guinea', 'code' => 'GN', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-gn', 'lat' => '11', 'lng' => '-10']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Guinea-Bissau', 'code' => 'GW', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-gw', 'lat' => '12', 'lng' => '-15']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Kenya', 'code' => 'KE', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ke', 'lat' => '1', 'lng' => '38']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Lesotho', 'code' => 'LS', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ls', 'lat' => '-29.5', 'lng' => '28.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Liberia', 'code' => 'LR', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-lr', 'lat' => '6.5', 'lng' => '-9.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Libya', 'code' => 'LY', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ly', 'lat' => '25', 'lng' => '17']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Madagascar', 'code' => 'MG', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-mg', 'lat' => '-20', 'lng' => '47']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Malawi', 'code' => 'MW', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-mw', 'lat' => '-13.5', 'lng' => '34']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Mali', 'code' => 'ML', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ml', 'lat' => '17', 'lng' => '-4']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Mauritania', 'code' => 'MR', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-mr', 'lat' => '20', 'lng' => '-12']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Mauritius', 'code' => 'MU', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-mu', 'lat' => '-20.2833', 'lng' => '57.55']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Morocco', 'code' => 'MA', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ma', 'lat' => '32', 'lng' => '-5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Mozambique', 'code' => 'MZ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-mz', 'lat' => '-18.25', 'lng' => '35']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Namibia', 'code' => 'NA', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-na', 'lat' => '-22', 'lng' => '17']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Niger', 'code' => 'NE', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ne', 'lat' => '16', 'lng' => '8']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Nigeria', 'code' => 'NG', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ng', 'lat' => '10', 'lng' => '8']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Rwanda', 'code' => 'RW', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-rw', 'lat' => '-2', 'lng' => '30']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Senegal', 'code' => 'SN', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-sn', 'lat' => '14', 'lng' => '-14']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Sierra Leone', 'code' => 'SL', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-sl', 'lat' => '8.5', 'lng' => '-11.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Somalia', 'code' => 'SO', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-so', 'lat' => '10', 'lng' => '49']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'South Africa', 'code' => 'ZA', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-za', 'lat' => '-29', 'lng' => '24']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Sudan', 'code' => 'SD', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-sd', 'lat' => '15', 'lng' => '30']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Swaziland', 'code' => 'SZ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-sz', 'lat' => '-26.5', 'lng' => '31.5']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Tanzania', 'code' => 'TZ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-tz', 'lat' => '-6', 'lng' => '35']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Togo', 'code' => 'TG', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-tg', 'lat' => '8', 'lng' => '1.1667']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Tunisia', 'code' => 'TN', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-tn', 'lat' => '34', 'lng' => '9']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Uganda', 'code' => 'UG', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-ug', 'lat' => '1', 'lng' => '32']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Zambia', 'code' => 'ZM', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-zm', 'lat' => '-15', 'lng' => '30']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 4, 'title' => 'Zimbabwe', 'code' => 'ZW', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-zw', 'lat' => '-20', 'lng' => '30']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 5, 'title' => 'Australia', 'code' => 'AU', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-au', 'lat' => '-27', 'lng' => '133']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 5, 'title' => 'Fiji', 'code' => 'FJ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-fj', 'lat' => '-18', 'lng' => '175']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 5, 'title' => 'New Zealand', 'code' => 'NZ', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-nz', 'lat' => '-41', 'lng' => '174']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 5, 'title' => 'Papua New Guinea', 'code' => 'PG', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-pg', 'lat' => '-6', 'lng' => '147']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['continent_id' => $startContinentId + 5, 'title' => 'Solomon Islands', 'code' => 'SB', 'name' => 'world_country.name.' . ++$rowCountryId, 'options' => json_encode(['icon' => 'flag-icon-sb', 'lat' => '-8', 'lng' => '159']), 'language_id' => $languageList['en'], 'created_at' => $timestamp, 'updated_at' => $timestamp],
        ];
        DB::table('world_country')->insert($worldCountryData);

        // 多語系
        $countryLanguage = [
            'zh-Hant' => [
                ['name' =>'阿富汗'], ['name' => '亞美尼亞'], ['name' => '亞塞拜然'], ['name' => '巴林'], ['name' => '孟加拉'],
                ['name' => '不丹'], ['name' => '汶萊'], ['name' => '柬埔寨'], ['name' => '中國'], ['name' => '喬治亞'],
                ['name' => '香港特別行政區'], ['name' => '印度'], ['name' => '印尼'], ['name' => '伊朗'], ['name' => '伊拉克'],
                ['name' => '以色列'], ['name' => '日本'], ['name' => '約旦'], ['name' => '哈薩克'], ['name' => '科威特'],
                ['name' => '吉爾吉斯'], ['name' => '寮國'], ['name' => '黎巴嫩'], ['name' => '澳門'], ['name' => '馬來西亞'],
                ['name' => '蒙古'], ['name' => '緬甸'], ['name' => '尼泊爾'], ['name' => '北韓'], ['name' => '阿曼'],
                ['name' => '巴基斯坦'], ['name' => '菲律賓'], ['name' => '卡達'], ['name' => '沙烏地阿拉伯'], ['name' => '新加坡'],
                ['name' => '南韓'], ['name' => '斯里蘭卡'], ['name' => '敘利亞'], ['name' => '臺灣'], ['name' => '塔吉克'],
                ['name' => '泰國'], ['name' => '土庫曼'], ['name' => '阿拉伯聯合大公國'], ['name' => '烏茲別克'], ['name' => '巴勒斯坦'],
                ['name' => '越南'], ['name' => '葉門'], ['name' => '安地卡及巴布達'], ['name' => '安圭拉'], ['name' => '百慕達'],
                ['name' => '加拿大'], ['name' => '巴哈馬'], ['name' => '哥斯大黎加'], ['name' => '古巴'], ['name' => '多明尼加'],
                ['name' => '薩爾瓦多'], ['name' => '瓜地馬拉'], ['name' => '海地'], ['name' => '宏都拉斯'], ['name' => '牙買加'],
                ['name' => '墨西哥'], ['name' => '尼加拉瓜'], ['name' => '巴拿馬'], ['name' => '波多黎各'], ['name' => '千里達'],
                ['name' => '美國'], ['name' => '阿根廷'], ['name' => '阿魯巴'], ['name' => '巴貝多'], ['name' => '玻利維亞'],
                ['name' => '巴西'], ['name' => '智利'], ['name' => '哥倫比亞'], ['name' => '厄瓜多'], ['name' => '蓋亞那'],
                ['name' => '巴拉圭'], ['name' => '秘魯'], ['name' => '蘇利南'], ['name' => '烏拉圭'], ['name' => '委內瑞拉'],
                ['name' => '阿爾巴尼亞'], ['name' => '安道爾'], ['name' => '奧地利'], ['name' => '白俄羅斯'], ['name' => '比利時'],
                ['name' => '波士尼亞與赫塞哥維納'], ['name' => '保加利亞'], ['name' => '克羅埃西亞'], ['name' => '賽普勒斯'], ['name' => '捷克'],
                ['name' => '丹麥'], ['name' => '愛沙尼亞'], ['name' => '芬蘭'], ['name' => '法國'], ['name' => '德國'],
                ['name' => '希臘'], ['name' => '匈牙利'], ['name' => '冰島'], ['name' => '愛爾蘭'], ['name' => '義大利'],
                ['name' => '拉脫維亞'], ['name' => '立陶宛'], ['name' => '盧森堡'], ['name' => '北馬其頓'], ['name' => '摩爾多瓦'],
                ['name' => '蒙特內哥羅'], ['name' => '荷蘭'], ['name' => '挪威'], ['name' => '波蘭'], ['name' => '葡萄牙'],
                ['name' => '羅馬尼亞'], ['name' => '俄羅斯'], ['name' => '塞爾維亞'], ['name' => '斯洛伐克'], ['name' => '斯洛維尼亞'],
                ['name' => '西班牙'], ['name' => '瑞典'], ['name' => '瑞士'], ['name' => '土耳其'], ['name' => '烏克蘭'],
                ['name' => '英國'], ['name' => '阿爾及利亞'], ['name' => '安哥拉'], ['name' => '貝南'], ['name' => '波札那'],
                ['name' => '布吉納法索'], ['name' => '蒲隆地'], ['name' => '喀麥隆'], ['name' => '維德角'], ['name' => '中非共和國'],
                ['name' => '查德'], ['name' => '葛摩'], ['name' => '剛果民主共和國'], ['name' => '剛果'], ['name' => '象牙海岸'],
                ['name' => '吉布地'], ['name' => '埃及'], ['name' => '赤道幾內亞'], ['name' => '厄利垂亞'], ['name' => '衣索比亞'],
                ['name' => '加彭'], ['name' => '甘比亞'], ['name' => '迦納'], ['name' => '幾內亞'], ['name' => '幾內亞比索'],
                ['name' => '肯亞'], ['name' => '賴索托'], ['name' => '賴比瑞亞'], ['name' => '利比亞'], ['name' => '馬達加斯加'],
                ['name' => '馬拉威'], ['name' => '馬利'], ['name' => '茅利塔尼亞'], ['name' => '模里西斯'], ['name' => '摩洛哥'],
                ['name' => '莫三比克'], ['name' => '納米比亞'], ['name' => '尼日'], ['name' => '奈及利亞'], ['name' => '盧安達'],
                ['name' => '塞內加爾'], ['name' => '獅子山'], ['name' => '索馬利亞'], ['name' => '南非'], ['name' => '蘇丹'],
                ['name' => '史瓦帝尼王國'], ['name' => '坦尚尼亞'], ['name' => '多哥'], ['name' => '突尼西亞'], ['name' => '烏干達'],
                ['name' => '尚比亞'], ['name' => '辛巴威'], ['name' => '澳洲'], ['name' => '斐濟'], ['name' => '紐西蘭'],
                ['name' => '巴布亞紐幾內亞'], ['name' => '索羅門群島']
            ],
            'zh-Hans' => [
                ['name' =>'阿富汗'], ['name' => '亚美尼亚'], ['name' => '阿塞拜疆'], ['name' => '巴林'], ['name' => '孟加拉国'],
                ['name' => '不丹'], ['name' => '文莱'], ['name' => '柬埔寨'], ['name' => '中国'], ['name' => '格鲁吉亚'],
                ['name' => '香港'], ['name' => '印度'], ['name' => '印度尼西亚'], ['name' => '伊朗'], ['name' => '伊拉克'],
                ['name' => '以色列'], ['name' => '日本'], ['name' => '约旦'], ['name' => '哈萨克斯坦'], ['name' => '科威特'],
                ['name' => '吉尔吉斯斯坦'], ['name' => '老挝'], ['name' => '黎巴嫩'], ['name' => '澳门'], ['name' => '马来西亚'],
                ['name' => '蒙古'], ['name' => '缅甸'], ['name' => '尼泊尔'], ['name' => '北朝鲜'], ['name' => '阿曼'],
                ['name' => '巴基斯坦'], ['name' => '菲律宾'], ['name' => '卡塔尔'], ['name' => '沙特阿拉伯'], ['name' => '新加坡'],
                ['name' => '韩国'], ['name' => '斯里兰卡'], ['name' => '叙利亚'], ['name' => '台湾'], ['name' => '塔吉克斯坦'],
                ['name' => '泰国'], ['name' => '土库曼斯坦'], ['name' => '阿拉伯联合酋长国'], ['name' => '乌兹别克斯坦'], ['name' => '约旦河西岸和加沙'],
                ['name' => '越南'], ['name' => '也门'], ['name' => '安提瓜和巴布达'], ['name' => '安圭拉'], ['name' => '百慕大'],
                ['name' => '加拿大'], ['name' => '巴哈马联邦'], ['name' => '哥斯达黎加'], ['name' => '古巴'], ['name' => '多明尼加'],
                ['name' => '萨尔瓦多'], ['name' => '危地马拉'], ['name' => '海地'], ['name' => '洪都拉斯'], ['name' => '牙买加'],
                ['name' => '墨西哥'], ['name' => '尼加拉瓜'], ['name' => '巴拿马'], ['name' => '波多黎各'], ['name' => '特立尼达和多巴哥'],
                ['name' => '美国'], ['name' => '阿根廷'], ['name' => '阿鲁巴'], ['name' => '巴巴多斯'], ['name' => '玻利维亚'],
                ['name' => '巴西'], ['name' => '智利'], ['name' => '哥伦比亚'], ['name' => '厄瓜多尔'], ['name' => '圭亚那'],
                ['name' => '巴拉圭'], ['name' => '秘鲁'], ['name' => '苏里南'], ['name' => '乌拉圭'], ['name' => '委内瑞拉'],
                ['name' => '阿尔巴尼亚'], ['name' => '安道尔'], ['name' => '奥地利'], ['name' => '白俄罗斯'], ['name' => '比利时'],
                ['name' => '波斯尼亚和黑塞哥维那'], ['name' => '保加利亚'], ['name' => '克罗地亚'], ['name' => '塞浦路斯'], ['name' => '捷克'],
                ['name' => '丹麦'], ['name' => '爱沙尼亚'], ['name' => '芬兰'], ['name' => '法国'], ['name' => '德国'],
                ['name' => '希腊'], ['name' => '匈牙利'], ['name' => '冰岛'], ['name' => '爱尔兰'], ['name' => '意大利'],
                ['name' => '拉脱维亚'], ['name' => '立陶宛'], ['name' => '卢森堡'], ['name' => '马其顿'], ['name' => '摩尔多瓦'],
                ['name' => '黑山'], ['name' => '荷兰'], ['name' => '挪威'], ['name' => '波兰'], ['name' => '葡萄牙'],
                ['name' => '罗马尼亚'], ['name' => '俄国'], ['name' => '塞尔维亚'], ['name' => '斯洛伐克'], ['name' => '斯洛文尼亚'],
                ['name' => '西班牙'], ['name' => '瑞典'], ['name' => '瑞士'], ['name' => '火鸡'], ['name' => '乌克兰'],
                ['name' => '英国'], ['name' => '阿尔及利亚'], ['name' => '安哥拉'], ['name' => '贝宁'], ['name' => '博茨瓦纳'],
                ['name' => '布基纳法索'], ['name' => '布隆迪'], ['name' => '喀麦隆'], ['name' => '佛得角'], ['name' => '中非'],
                ['name' => '乍得'], ['name' => '科摩罗'], ['name' => '刚果民主共和国'], ['name' => '刚果'], ['name' => '科特迪瓦'],
                ['name' => '吉布提'], ['name' => '埃及'], ['name' => '赤道几内亚'], ['name' => '厄立特里亚'], ['name' => '埃塞俄比亚'],
                ['name' => '加蓬'], ['name' => '冈比亚'], ['name' => '加纳'], ['name' => '几内亚'], ['name' => '几内亚比绍'],
                ['name' => '肯尼亚'], ['name' => '莱索托'], ['name' => '利比里亚'], ['name' => '利比亚'], ['name' => '马达加斯加'],
                ['name' => '马拉维'], ['name' => '马里'], ['name' => '毛里塔尼亚'], ['name' => '毛里求斯'], ['name' => '摩洛哥'],
                ['name' => '莫桑比克'], ['name' => '纳米比亚'], ['name' => '尼日尔'], ['name' => '尼日利亚'], ['name' => '卢旺达'],
                ['name' => '塞内加尔'], ['name' => '塞拉利昂'], ['name' => '索马里'], ['name' => '南非'], ['name' => '苏丹'],
                ['name' => '斯威士兰'], ['name' => '坦桑尼亚'], ['name' => '多哥'], ['name' => '突尼斯'], ['name' => '乌干达'],
                ['name' => '赞比亚'], ['name' => '津巴布韦'], ['name' => '澳大利亚'], ['name' => '斐'], ['name' => '新西兰'],
                ['name' => '巴布亚新几内亚'], ['name' => '所罗门群岛']
            ],
            'ja' => [
                ['name' =>'アフガニスタン'], ['name' => 'アルメニア'], ['name' => 'アゼルバイジャン'], ['name' => 'バーレーン'], ['name' => 'バングラデシュ'],
                ['name' => 'ブータン'], ['name' => 'ブルネイ'], ['name' => 'カンボジア'], ['name' => '中国'], ['name' => 'ジョージア'],
                ['name' => '香港'], ['name' => 'インド'], ['name' => 'インドネシア'], ['name' => 'イラン'], ['name' => 'イラク'],
                ['name' => 'イスラエル'], ['name' => '日本'], ['name' => 'ヨルダン'], ['name' => 'カザフスタン'], ['name' => 'クウェート'],
                ['name' => 'キルギスタン'], ['name' => 'ラオス'], ['name' => 'レバノン'], ['name' => 'マカオ'], ['name' => 'マレーシア'],
                ['name' => 'モンゴル'], ['name' => 'ミャンマー'], ['name' => 'ネパール'], ['name' => '北朝鮮'], ['name' => 'オマーン'],
                ['name' => 'パキスタン'], ['name' => 'フィリピン'], ['name' => 'カタール'], ['name' => 'サウジアラビア'], ['name' => 'シンガポール'],
                ['name' => '韓国'], ['name' => 'スリランカ'], ['name' => 'シリア'], ['name' => '台湾'], ['name' => 'タジキスタン'],
                ['name' => 'タイ'], ['name' => 'トルクメニスタン'], ['name' => 'アラブ首長国連邦'], ['name' => 'ウズベキスタン'], ['name' => '西岸とガザ'],
                ['name' => 'ベトナム'], ['name' => 'イエメン'], ['name' => 'アンティグアバーブーダ'], ['name' => 'アンギラ'], ['name' => 'バミューダ'],
                ['name' => 'カナダ'], ['name' => 'バハマ連邦'], ['name' => 'コスタリカ'], ['name' => 'キューバ'], ['name' => 'ドミニカ共和国'],
                ['name' => 'エルサルバドル'], ['name' => 'グアテマラ'], ['name' => 'ハイチ'], ['name' => 'ホンジュラス'], ['name' => 'ジャマイカ'],
                ['name' => 'メキシコ'], ['name' => 'ニカラグア'], ['name' => 'パナマ'], ['name' => 'プエルトリコ'], ['name' => 'トリニダード・トバゴ'],
                ['name' => 'アメリカ'], ['name' => 'アルゼンチン'], ['name' => 'アルバ'], ['name' => 'バルバドス'], ['name' => 'ボリビア'],
                ['name' => 'ブラジル'], ['name' => 'チリ'], ['name' => 'コロンビア'], ['name' => 'エクアドル'], ['name' => 'ガイアナ'],
                ['name' => 'パラグアイ'], ['name' => 'ペルー'], ['name' => 'スリナム'], ['name' => 'ウルグアイ'], ['name' => 'ベネズエラ'],
                ['name' => 'アルバニア'], ['name' => 'アンドラ'], ['name' => 'オーストリア'], ['name' => 'ベラルーシ'], ['name' => 'ベルギー'],
                ['name' => 'ボスニア・ヘルツェゴビナ'], ['name' => 'ブルガリア'], ['name' => 'クロアチア'], ['name' => 'キプロス'], ['name' => 'チェコ語'],
                ['name' => 'デンマーク'], ['name' => 'エストニア'], ['name' => 'フィンランド'], ['name' => 'フランス'], ['name' => 'ドイツ'],
                ['name' => 'ギリシャ'], ['name' => 'ハンガリー'], ['name' => 'アイスランド'], ['name' => 'アイルランド'], ['name' => 'イタリア'],
                ['name' => 'ラトビア'], ['name' => 'リトアニア'], ['name' => 'ルクセンブルク'], ['name' => 'マケドニア'], ['name' => 'モルドバ'],
                ['name' => 'モンテネグロ'], ['name' => 'オランダ'], ['name' => 'ノルウェー'], ['name' => 'ポーランド'], ['name' => 'ポルトガル'],
                ['name' => 'ルーマニア'], ['name' => 'ロシア'], ['name' => 'セルビア'], ['name' => 'スロバキア'], ['name' => 'スロベニア'],
                ['name' => 'スペイン'], ['name' => 'スウェーデン'], ['name' => 'スイス'], ['name' => '七面鳥'], ['name' => 'ウクライナ'],
                ['name' => 'イギリス'], ['name' => 'アルジェリア'], ['name' => 'アンゴラ'], ['name' => 'ベニン'], ['name' => 'ボツワナ'],
                ['name' => 'ブルキナファソ'], ['name' => 'ブルンジ'], ['name' => 'カメルーン'], ['name' => 'カーボベルデ'], ['name' => '中央アフリカ'],
                ['name' => 'チャド'], ['name' => 'コモロ'], ['name' => 'コンゴ民主党'], ['name' => 'コンゴ'], ['name' => 'コートジボワール'],
                ['name' => 'ジブチ'], ['name' => 'エジプト'], ['name' => '赤道ギニア'], ['name' => 'エリトリア'], ['name' => 'エチオピア'],
                ['name' => 'ガボン'], ['name' => 'ガンビア'], ['name' => 'ガーナ'], ['name' => 'ギニア'], ['name' => 'ギニアビサウ'],
                ['name' => 'ケニア'], ['name' => 'レソト'], ['name' => 'リベリア'], ['name' => 'リビア'], ['name' => 'マダガスカル'],
                ['name' => 'マラウイ'], ['name' => 'マリ'], ['name' => 'モーリタニア'], ['name' => 'モーリシャス'], ['name' => 'モロッコ'],
                ['name' => 'モザンビーク'], ['name' => 'ナミビア'], ['name' => 'ニジェール'], ['name' => 'ナイジェリア'], ['name' => 'ルワンダ'],
                ['name' => 'セネガル'], ['name' => 'シエラレオネ'], ['name' => 'ソマリア'], ['name' => '南アフリカ'], ['name' => 'スーダン'],
                ['name' => 'スワジランド'], ['name' => 'タンザニア'], ['name' => '行く'], ['name' => 'チュニジア'], ['name' => 'ウガンダ'],
                ['name' => 'ザンビア'], ['name' => 'ジンバブエ'], ['name' => 'オーストラリア'], ['name' => 'フィジー'], ['name' => 'ニュージーランド'],
                ['name' => 'パプアニューギニア'], ['name' => 'ソロモン諸島']
            ],
            'en' => [
                ['name' =>'Afghanistan'], ['name' => 'Armenia'], ['name' => 'Azerbaijan'], ['name' => 'Bahrain'], ['name' => 'Bangladesh'],
                ['name' => 'Bhutan'], ['name' => 'Brunei'], ['name' => 'Cambodia'], ['name' => 'China'], ['name' => 'Georgia'],
                ['name' => 'Hong Kong'], ['name' => 'India'], ['name' => 'Indonesia'], ['name' => 'Iran'], ['name' => 'Iraq'],
                ['name' => 'Israel'], ['name' => 'Japan'], ['name' => 'Jordan'], ['name' => 'Kazakhstan'], ['name' => 'Kuwait'],
                ['name' => 'Kyrgyzstan'], ['name' => 'Laos'], ['name' => 'Lebanon'], ['name' => 'Macao'], ['name' => 'Malaysia'],
                ['name' => 'Mongolia'], ['name' => 'Myanmar'], ['name' => 'Nepal'], ['name' => 'North Korea'], ['name' => 'Oman'],
                ['name' => 'Pakistan'], ['name' => 'Philippines'], ['name' => 'Qatar'], ['name' => 'Saudi Arabia'], ['name' => 'Singapore'],
                ['name' => 'South Lorea'], ['name' => 'Sri Lanka'], ['name' => 'Syria'], ['name' => 'Taiwan'], ['name' => 'Tajikistan'],
                ['name' => 'Thailand'], ['name' => 'Turkmenistan'], ['name' => 'United Arab Emirates'], ['name' => 'Uzbekistan'], ['name' => 'West Bank and Gaza'],
                ['name' => 'Vietnam'], ['name' => 'Yemen'], ['name' => 'Antigua and Barbuda'], ['name' => 'Anguilla'], ['name' => 'Bermuda'],
                ['name' => 'Canada'], ['name' => 'Commonwealth of the Bahamas'], ['name' => 'Costa Rica'], ['name' => 'Cuba'], ['name' => 'Dominican'],
                ['name' => 'El Salvador'], ['name' => 'Guatemala'], ['name' => 'Haiti'], ['name' => 'Honduras'], ['name' => 'Jamaica'],
                ['name' => 'Mexico'], ['name' => 'Nicaragua'], ['name' => 'Panama'], ['name' => 'Puerto Rico'], ['name' => 'Trinidad and Tobago'],
                ['name' => 'United States'], ['name' => 'Argentina'], ['name' => 'Aruba'], ['name' => 'Barbados'], ['name' => 'Bolivia'],
                ['name' => 'Brazil'], ['name' => 'Chile'], ['name' => 'Colombia'], ['name' => 'Ecuador'], ['name' => 'Guyana'],
                ['name' => 'Paraguay'], ['name' => 'Peru'], ['name' => 'Suriname'], ['name' => 'Uruguay'], ['name' => 'Venezuela'],
                ['name' => 'Albania'], ['name' => 'Andorra'], ['name' => 'Austria'], ['name' => 'Belarus'], ['name' => 'Belgium'],
                ['name' => 'Bosnia and Herzegovina'], ['name' => 'Bulgaria'], ['name' => 'Croatia'], ['name' => 'Cyprus'], ['name' => 'Czech'],
                ['name' => 'Denmark'], ['name' => 'Estonia'], ['name' => 'Finland'], ['name' => 'France'], ['name' => 'Germany'],
                ['name' => 'Greece'], ['name' => 'Hungary'], ['name' => 'Iceland'], ['name' => 'Ireland'], ['name' => 'Italy'],
                ['name' => 'Latvia'], ['name' => 'Lithuania'], ['name' => 'Luxembourg'], ['name' => 'Macedonia, FYR'], ['name' => 'Moldova'],
                ['name' => 'Montenegro'], ['name' => 'Netherlands'], ['name' => 'Norway'], ['name' => 'Poland'], ['name' => 'Portugal'],
                ['name' => 'Romania'], ['name' => 'Russia'], ['name' => 'Serbia'], ['name' => 'Slovak'], ['name' => 'Slovenia'],
                ['name' => 'Spain'], ['name' => 'Sweden'], ['name' => 'Switzerland'], ['name' => 'Turkey'], ['name' => 'Ukraine'],
                ['name' => 'United Kingdom'], ['name' => 'Algeria'], ['name' => 'Angola'], ['name' => 'Benin'], ['name' => 'Botswana'],
                ['name' => 'Burkina Faso'], ['name' => 'Burundi'], ['name' => 'Cameroon'], ['name' => 'Cape Verde'], ['name' => 'Central African'],
                ['name' => 'Chad'], ['name' => 'Comoros'], ['name' => 'Congo Dem.'], ['name' => 'Congo'], ['name' => "Cote d'Ivoire"],
                ['name' => 'Djibouti'], ['name' => 'Egypt'], ['name' => 'Equatorial Guinea'], ['name' => 'Eritrea'], ['name' => 'Ethiopia'],
                ['name' => 'Gabon'], ['name' => 'Gambia'], ['name' => 'Ghana'], ['name' => 'Guinea'], ['name' => 'Guinea-Bissau'],
                ['name' => 'Kenya'], ['name' => 'Lesotho'], ['name' => 'Liberia'], ['name' => 'Libya'], ['name' => 'Madagascar'],
                ['name' => 'Malawi'], ['name' => 'Mali'], ['name' => 'Mauritania'], ['name' => 'Mauritius'], ['name' => 'Morocco'],
                ['name' => 'Mozambique'], ['name' => 'Namibia'], ['name' => 'Niger'], ['name' => 'Nigeria'], ['name' => 'Rwanda'],
                ['name' => 'Senegal'], ['name' => 'Sierra Leone'], ['name' => 'Somalia'], ['name' => 'South Africa'], ['name' => 'Sudan'],
                ['name' => 'Swaziland'], ['name' => 'Tanzania'], ['name' => 'Togo'], ['name' => 'Tunisia'], ['name' => 'Uganda'],
                ['name' => 'Zambia'], ['name' => 'Zimbabwe'], ['name' => 'Australia'], ['name' => 'Fiji'], ['name' => 'New Zealand'],
                ['name' => 'Papua New Guinea'], ['name' => 'Solomon Islands']

            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'world_country', $countryLanguage, $languageList, $startCountryId, false);

        // 全球化 - 州區
        $taiwanCountryId = DB::table('world_country')->where('code', 'TW')->value('id');
        $startStateId = $rowStateId = SeederHelper::getTableNextIncrement('world_state');
        $rowStateId--;

        $worldStateData = [
            ['country_id' => $taiwanCountryId, 'title' => '北部', 'code' => 'TW-North', 'name' => 'world_state.name.' . ++$rowStateId, 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['country_id' => $taiwanCountryId, 'title' => '中部', 'code' => 'TW-West', 'name' => 'world_state.name.' . ++$rowStateId, 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['country_id' => $taiwanCountryId, 'title' => '南部', 'code' => 'TW-South', 'name' => 'world_state.name.' . ++$rowStateId, 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['country_id' => $taiwanCountryId, 'title' => '東部', 'code' => 'TW-East', 'name' => 'world_state.name.' . ++$rowStateId, 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['country_id' => $taiwanCountryId, 'title' => '外島', 'code' => 'TW-Island', 'name' => 'world_state.name.' . ++$rowStateId, 'created_at' => $timestamp, 'updated_at' => $timestamp],
        ];
        DB::table('world_state')->insert($worldStateData);

        // 多語系
        $stateLanguage = [
            'zh-Hant' => [
                ['name' => '北部'], ['name' => '中部'], ['name' => '南部'], ['name' => '東部'], ['name' => '外島']
            ],
            'zh-Hans' => [
                ['name' => '北部'], ['name' => '中部'], ['name' => '南部'], ['name' => '东部'], ['name' => '外岛']
            ],
            'ja' => [
                ['name' => '北部'], ['name' => '中部'], ['name' => '南部'], ['name' => '東部'], ['name' => '外の島']
            ],
            'en' => [
                ['name' => 'North'], ['name' => 'West'], ['name' => 'South'], ['name' => 'East'], ['name' => 'Outer Island']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'world_state', $stateLanguage, $languageList, $startStateId, false);

        // 全球化 - 縣市
        $startCountyId = $rowCountyId = SeederHelper::getTableNextIncrement('world_county');
        $rowCountyId--;

        $worldCountyData = [
            ['state_id' => $startStateId, 'title' => '基隆市', 'code' => 'TW-KL', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId, 'title' => '臺北市', 'code' => 'TW-TP', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId, 'title' => '新北市', 'code' => 'TW-NT', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId, 'title' => '桃園市', 'code' => 'TW-TY', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId, 'title' => '新竹市', 'code' => 'TW-XZ', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId, 'title' => '新竹縣', 'code' => 'TW-XZC', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId, 'title' => '苗栗縣', 'code' => 'TW-MLC', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 1, 'title' => '臺中市', 'code' => 'TW-TC', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 1, 'title' => '彰化縣', 'code' => 'TW-CHC', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 1, 'title' => '南投縣', 'code' => 'TW-NTC', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 1, 'title' => '雲林縣', 'code' => 'TW-YLC', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 2, 'title' => '嘉義市', 'code' => 'TW-CY', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 2, 'title' => '嘉義縣', 'code' => 'TW-CYC', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 2, 'title' => '臺南市', 'code' => 'TW-TN', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 2, 'title' => '高雄市', 'code' => 'TW-KS', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 2, 'title' => '屏東縣', 'code' => 'TW-PTC', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 3, 'title' => '臺東縣', 'code' => 'TW-TTC', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 3, 'title' => '花蓮縣', 'code' => 'TW-HLC', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 3, 'title' => '宜蘭縣', 'code' => 'TW-YLC', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 4, 'title' => '澎湖縣', 'code' => 'TW-PHC', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 4, 'title' => '金門縣', 'code' => 'TW-KMC', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            ['state_id' => $startStateId + 4, 'title' => '連江縣', 'code' => 'TW-LJC', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            // ['state_id' => $startStateId + 4, 'title' => '南海島', 'code' => 'TW-HNI', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
            // ['state_id' => $startStateId + 4, 'title' => '釣魚臺', 'code' => 'TW-DYS', 'name' => 'world_county.name.' . ++$rowCountyId, 'updated_at' => $timestamp, 'created_at' => $timestamp],
        ];
        DB::table('world_county')->insert($worldCountyData);

        // 多語系
        $countyLanguage = [
            'zh-Hant' => [
                ['name' => '基隆市'], ['name' => '臺北市'], ['name' => '新北市'], ['name' => '桃園市'], ['name' => '新竹市'], ['name' => '新竹縣'],
                ['name' => '苗栗縣'], ['name' => '臺中市'], ['name' => '彰化縣'], ['name' => '南投縣'], ['name' => '雲林縣'], ['name' => '嘉義市'],
                ['name' => '嘉義縣'], ['name' => '臺南市'], ['name' => '高雄市'], ['name' => '屏東縣'], ['name' => '臺東縣'], ['name' => '花蓮縣'],
                ['name' => '宜蘭縣'], ['name' => '澎湖縣'], ['name' => '金門縣'], ['name' => '連江縣']
            ],
            'zh-Hans' => [
                ['name' => '基隆市'], ['name' => '台北市'], ['name' => '新北市'], ['name' => '桃园市'], [' name' => '新竹市'], ['name' => '新竹县'],
                ['name' => '苗栗县'], ['name' => '台中市'], ['name' => '彰化县'], ['name' => '南投县'], [' name' => '云林县'], ['name' => '嘉义市'],
                ['name' => '嘉义县'], ['name' => '台南市'], ['name' => '高雄市'], ['name' => '屏东县'], [ 'name' => '台东县'], ['name' => '花莲县'],
                ['name' => '宜兰县'], ['name' => '澎湖县'], ['name' => '金门县'], ['name' => '连江县']
            ],
            'ja' => [
                ['name' => '基隆市'], ['name' => '台北市'], ['name' => '新北市'], ['name' => '桃園市'], ['name' => '新竹市'], ['name' => '新竹県'],
                ['name' => '苗栗県'], ['name' => '台中市'], ['name' => '彰化県'], ['name' => '南投県'], ['name' => '雲林県'], ['name' => '嘉義市'],
                ['name' => '嘉義県'], ['name' => '台南市'], ['name' => '高雄市'], ['name' => '屏東県'], ['name' => '臺東県'], ['name' => '花蓮県'],
                ['name' => '宜蘭県'], ['name' => '澎湖県'], ['name' => '金門県'], ['name' => '連江県']
            ],
            'en' => [
                ['name' => 'Keelung City'], ['name' => 'Taipei City'], ['name' => 'Xinbei City'], ['name' => 'Taoyuan City'], [' Name' => 'Hsinchu City'], ['name' => 'Hsinchu County'],
                ['name' => 'Miaoli County'], ['name' => 'Taichung City'], ['name' => 'Changhua County'], ['name' => 'Nantou County'], [' Name' => 'Yunlin County'], ['name' => 'Chiayi City'],
                ['name' => 'Chiayi County'], ['name' => 'Tainan City'], ['name' => 'Kaohsiung City'], ['name' => 'Pingdong County'], [ 'name' => 'Taitong County'], ['name' => 'Hualien County'],
                ['name' => 'Yilan County'], ['name' => 'Wuhu County'], ['name' => 'Jinmen County'], ['name' => 'Lianjiang County']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'world_county', $countyLanguage, $languageList, $startCountyId, false);

        // 全球化 - 城市
        $startCityId = SeederHelper::getTableNextIncrement('world_city');

        $worldCityData = [
            ['county_id' => $startCountyId, 'title' => '仁愛區', 'code' => '200', 'name' => '仁愛區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId, 'title' => '信義區', 'code' => '201', 'name' => '信義區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId, 'title' => '中正區', 'code' => '202', 'name' => '中正區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId, 'title' => '中山區', 'code' => '203', 'name' => '中山區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId, 'title' => '安樂區', 'code' => '204', 'name' => '安樂區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId, 'title' => '暖暖區', 'code' => '205', 'name' => '暖暖區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId, 'title' => '七堵區', 'code' => '206', 'name' => '七堵區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 1, 'title' => '中正區', 'code' => '100', 'name' => '中正區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 1, 'title' => '大同區', 'code' => '103', 'name' => '大同區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 1, 'title' => '中山區', 'code' => '104', 'name' => '中山區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 1, 'title' => '松山區', 'code' => '105', 'name' => '松山區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 1, 'title' => '大安區', 'code' => '106', 'name' => '大安區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 1, 'title' => '萬華區', 'code' => '108', 'name' => '萬華區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 1, 'title' => '信義區', 'code' => '110', 'name' => '信義區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 1, 'title' => '士林區', 'code' => '111', 'name' => '士林區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 1, 'title' => '北投區', 'code' => '112', 'name' => '北投區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 1, 'title' => '內湖區', 'code' => '114', 'name' => '內湖區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 1, 'title' => '南港區', 'code' => '115', 'name' => '南港區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 1, 'title' => '文山區', 'code' => '116', 'name' => '文山區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '萬里區', 'code' => '207', 'name' => '萬里區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '金山區', 'code' => '208', 'name' => '金山區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '板橋區', 'code' => '220', 'name' => '板橋區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '汐止區', 'code' => '221', 'name' => '汐止區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '深坑區', 'code' => '222', 'name' => '深坑區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '石碇區', 'code' => '223', 'name' => '石碇區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '瑞芳區', 'code' => '224', 'name' => '瑞芳區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '平溪區', 'code' => '226', 'name' => '平溪區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '雙溪區', 'code' => '227', 'name' => '雙溪區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '貢寮區', 'code' => '228', 'name' => '貢寮區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '新店區', 'code' => '231', 'name' => '新店區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '坪林區', 'code' => '232', 'name' => '坪林區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '烏來區', 'code' => '233', 'name' => '烏來區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '永和區', 'code' => '234', 'name' => '永和區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '中和區', 'code' => '235', 'name' => '中和區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '土城區', 'code' => '236', 'name' => '土城區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '三峽區', 'code' => '237', 'name' => '三峽區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '樹林區', 'code' => '238', 'name' => '樹林區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '鶯歌區', 'code' => '239', 'name' => '鶯歌區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '三重區', 'code' => '241', 'name' => '三重區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '新莊區', 'code' => '242', 'name' => '新莊區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '泰山區', 'code' => '243', 'name' => '泰山區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '林口區', 'code' => '244', 'name' => '林口區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '蘆洲區', 'code' => '247', 'name' => '蘆洲區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '五股區', 'code' => '248', 'name' => '五股區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '八里區', 'code' => '249', 'name' => '八里區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '淡水區', 'code' => '251', 'name' => '淡水區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '三芝區', 'code' => '252', 'name' => '三芝區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 2, 'title' => '石門區', 'code' => '253', 'name' => '石門區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 3, 'title' => '中壢區', 'code' => '320', 'name' => '中壢區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 3, 'title' => '平鎮區', 'code' => '324', 'name' => '平鎮區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 3, 'title' => '龍潭區', 'code' => '325', 'name' => '龍潭區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 3, 'title' => '楊梅區', 'code' => '326', 'name' => '楊梅區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 3, 'title' => '新屋區', 'code' => '327', 'name' => '新屋區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 3, 'title' => '觀音區', 'code' => '328', 'name' => '觀音區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 3, 'title' => '桃園區', 'code' => '330', 'name' => '桃園區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 3, 'title' => '龜山區', 'code' => '333', 'name' => '龜山區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 3, 'title' => '八德區', 'code' => '334', 'name' => '八德區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 3, 'title' => '大溪區', 'code' => '335', 'name' => '大溪區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 3, 'title' => '復興區', 'code' => '336', 'name' => '復興區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 3, 'title' => '大園區', 'code' => '337', 'name' => '大園區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 3, 'title' => '蘆竹區', 'code' => '338', 'name' => '蘆竹區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 4, 'title' => '東區', 'code' => '300', 'name' => '東區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 4, 'title' => '北區', 'code' => '300', 'name' => '北區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 4, 'title' => '香山區', 'code' => '300', 'name' => '香山區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 5, 'title' => '竹北市', 'code' => '302', 'name' => '竹北市', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 5, 'title' => '湖口鄉', 'code' => '303', 'name' => '湖口鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 5, 'title' => '新豐鄉', 'code' => '304', 'name' => '新豐鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 5, 'title' => '新埔鎮', 'code' => '305', 'name' => '新埔鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 5, 'title' => '關西鎮', 'code' => '306', 'name' => '關西鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 5, 'title' => '芎林鄉', 'code' => '307', 'name' => '芎林鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 5, 'title' => '寶山鄉', 'code' => '308', 'name' => '寶山鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 5, 'title' => '竹東鎮', 'code' => '310', 'name' => '竹東鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 5, 'title' => '五峰鄉', 'code' => '311', 'name' => '五峰鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 5, 'title' => '橫山鄉', 'code' => '312', 'name' => '橫山鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 5, 'title' => '尖石鄉', 'code' => '313', 'name' => '尖石鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 5, 'title' => '北埔鄉', 'code' => '314', 'name' => '北埔鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 5, 'title' => '峨眉鄉', 'code' => '315', 'name' => '峨眉鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '竹南鎮', 'code' => '350', 'name' => '竹南鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '頭份市', 'code' => '351', 'name' => '頭份市', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '三灣鄉', 'code' => '352', 'name' => '三灣鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '南庄鄉', 'code' => '353', 'name' => '南庄鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '獅潭鄉', 'code' => '354', 'name' => '獅潭鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '後龍鎮', 'code' => '356', 'name' => '後龍鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '通霄鎮', 'code' => '357', 'name' => '通霄鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '苑裡鎮', 'code' => '358', 'name' => '苑裡鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '苗栗市', 'code' => '360', 'name' => '苗栗市', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '造橋鄉', 'code' => '361', 'name' => '造橋鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '頭屋鄉', 'code' => '362', 'name' => '頭屋鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '公館鄉', 'code' => '363', 'name' => '公館鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '大湖鄉', 'code' => '364', 'name' => '大湖鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '泰安鄉', 'code' => '365', 'name' => '泰安鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '銅鑼鄉', 'code' => '366', 'name' => '銅鑼鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '三義鄉', 'code' => '367', 'name' => '三義鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '西湖鄉', 'code' => '368', 'name' => '西湖鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 6, 'title' => '卓蘭鎮', 'code' => '369', 'name' => '卓蘭鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '中區', 'code' => '400', 'name' => '中區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '東區', 'code' => '401', 'name' => '東區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '南區', 'code' => '402', 'name' => '南區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '西區', 'code' => '403', 'name' => '西區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '北區', 'code' => '404', 'name' => '北區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '北屯區', 'code' => '406', 'name' => '北屯區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '西屯區', 'code' => '407', 'name' => '西屯區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '南屯區', 'code' => '408', 'name' => '南屯區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '太平區', 'code' => '411', 'name' => '太平區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '大里區', 'code' => '412', 'name' => '大里區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '霧峰區', 'code' => '413', 'name' => '霧峰區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '烏日區', 'code' => '414', 'name' => '烏日區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '豐原區', 'code' => '420', 'name' => '豐原區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '后里區', 'code' => '421', 'name' => '后里區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '石岡區', 'code' => '422', 'name' => '石岡區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '東勢區', 'code' => '423', 'name' => '東勢區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '和平區', 'code' => '424', 'name' => '和平區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '新社區', 'code' => '426', 'name' => '新社區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '潭子區', 'code' => '427', 'name' => '潭子區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '大雅區', 'code' => '428', 'name' => '大雅區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '神岡區', 'code' => '429', 'name' => '神岡區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '大肚區', 'code' => '432', 'name' => '大肚區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '沙鹿區', 'code' => '433', 'name' => '沙鹿區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '龍井區', 'code' => '434', 'name' => '龍井區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '梧棲區', 'code' => '435', 'name' => '梧棲區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '清水區', 'code' => '436', 'name' => '清水區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '大甲區', 'code' => '437', 'name' => '大甲區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '外埔區', 'code' => '438', 'name' => '外埔區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 7, 'title' => '大安區', 'code' => '439', 'name' => '大安區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '彰化市', 'code' => '500', 'name' => '彰化市', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '芬園鄉', 'code' => '502', 'name' => '芬園鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '花壇鄉', 'code' => '503', 'name' => '花壇鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '秀水鄉', 'code' => '504', 'name' => '秀水鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '鹿港鎮', 'code' => '505', 'name' => '鹿港鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '福興鄉', 'code' => '506', 'name' => '福興鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '線西鄉', 'code' => '507', 'name' => '線西鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '和美鎮', 'code' => '508', 'name' => '和美鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '伸港鄉', 'code' => '509', 'name' => '伸港鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '員林市', 'code' => '510', 'name' => '員林市', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '社頭鄉', 'code' => '511', 'name' => '社頭鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '永靖鄉', 'code' => '512', 'name' => '永靖鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '埔心鄉', 'code' => '513', 'name' => '埔心鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '溪湖鎮', 'code' => '514', 'name' => '溪湖鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '大村鄉', 'code' => '515', 'name' => '大村鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '埔鹽鄉', 'code' => '516', 'name' => '埔鹽鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '田中鎮', 'code' => '520', 'name' => '田中鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '北斗鎮', 'code' => '521', 'name' => '北斗鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '田尾鄉', 'code' => '522', 'name' => '田尾鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '埤頭鄉', 'code' => '523', 'name' => '埤頭鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '溪州鄉', 'code' => '524', 'name' => '溪州鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '竹塘鄉', 'code' => '525', 'name' => '竹塘鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '二林鎮', 'code' => '526', 'name' => '二林鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '大城鄉', 'code' => '527', 'name' => '大城鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '芳苑鄉', 'code' => '528', 'name' => '芳苑鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 8, 'title' => '二水鄉', 'code' => '530', 'name' => '二水鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 9, 'title' => '南投市', 'code' => '540', 'name' => '南投市', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 9, 'title' => '中寮鄉', 'code' => '541', 'name' => '中寮鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 9, 'title' => '草屯鎮', 'code' => '542', 'name' => '草屯鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 9, 'title' => '國姓鄉', 'code' => '544', 'name' => '國姓鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 9, 'title' => '埔里鎮', 'code' => '545', 'name' => '埔里鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 9, 'title' => '仁愛鄉', 'code' => '546', 'name' => '仁愛鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 9, 'title' => '名間鄉', 'code' => '551', 'name' => '名間鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 9, 'title' => '集集鎮', 'code' => '552', 'name' => '集集鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 9, 'title' => '水里鄉', 'code' => '553', 'name' => '水里鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 9, 'title' => '魚池鄉', 'code' => '555', 'name' => '魚池鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 9, 'title' => '信義鄉', 'code' => '556', 'name' => '信義鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 9, 'title' => '竹山鎮', 'code' => '557', 'name' => '竹山鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 9, 'title' => '鹿谷鄉', 'code' => '558', 'name' => '鹿谷鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '斗南鎮', 'code' => '630', 'name' => '斗南鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '大埤鄉', 'code' => '631', 'name' => '大埤鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '虎尾鎮', 'code' => '632', 'name' => '虎尾鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '土庫鎮', 'code' => '633', 'name' => '土庫鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '褒忠鄉', 'code' => '634', 'name' => '褒忠鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '東勢鄉', 'code' => '635', 'name' => '東勢鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '臺西鄉', 'code' => '636', 'name' => '臺西鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '崙背鄉', 'code' => '637', 'name' => '崙背鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '麥寮鄉', 'code' => '638', 'name' => '麥寮鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '斗六市', 'code' => '640', 'name' => '斗六市', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '林內鄉', 'code' => '643', 'name' => '林內鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '古坑鄉', 'code' => '646', 'name' => '古坑鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '莿桐鄉', 'code' => '647', 'name' => '莿桐鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '西螺鎮', 'code' => '648', 'name' => '西螺鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '二崙鄉', 'code' => '649', 'name' => '二崙鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '北港鎮', 'code' => '651', 'name' => '北港鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '水林鄉', 'code' => '652', 'name' => '水林鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '口湖鄉', 'code' => '653', 'name' => '口湖鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '四湖鄉', 'code' => '654', 'name' => '四湖鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 10, 'title' => '元長鄉', 'code' => '655', 'name' => '元長鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 11, 'title' => '東區', 'code' => '600', 'name' => '東區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 11, 'title' => '西區', 'code' => '600', 'name' => '西區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '番路鄉', 'code' => '602', 'name' => '番路鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '梅山鄉', 'code' => '603', 'name' => '梅山鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '竹崎鄉', 'code' => '604', 'name' => '竹崎鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '阿里山鄉', 'code' => '605', 'name' => '阿里山鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '中埔鄉', 'code' => '606', 'name' => '中埔鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '大埔鄉', 'code' => '607', 'name' => '大埔鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '水上鄉', 'code' => '608', 'name' => '水上鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '鹿草鄉', 'code' => '611', 'name' => '鹿草鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '太保市', 'code' => '612', 'name' => '太保市', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '朴子市', 'code' => '613', 'name' => '朴子市', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '東石鄉', 'code' => '614', 'name' => '東石鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '六腳鄉', 'code' => '615', 'name' => '六腳鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '新港鄉', 'code' => '616', 'name' => '新港鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '民雄鄉', 'code' => '621', 'name' => '民雄鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '大林鎮', 'code' => '622', 'name' => '大林鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '溪口鄉', 'code' => '623', 'name' => '溪口鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '義竹鄉', 'code' => '624', 'name' => '義竹鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 12, 'title' => '布袋鎮', 'code' => '625', 'name' => '布袋鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '中西區', 'code' => '700', 'name' => '中西區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '東區', 'code' => '701', 'name' => '東區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '南區', 'code' => '702', 'name' => '南區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '北區', 'code' => '704', 'name' => '北區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '安平區', 'code' => '708', 'name' => '安平區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '安南區', 'code' => '709', 'name' => '安南區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '永康區', 'code' => '710', 'name' => '永康區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '歸仁區', 'code' => '711', 'name' => '歸仁區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '新化區', 'code' => '712', 'name' => '新化區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '左鎮區', 'code' => '713', 'name' => '左鎮區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '玉井區', 'code' => '714', 'name' => '玉井區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '楠西區', 'code' => '715', 'name' => '楠西區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '南化區', 'code' => '716', 'name' => '南化區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '仁德區', 'code' => '717', 'name' => '仁德區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '關廟區', 'code' => '718', 'name' => '關廟區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '龍崎區', 'code' => '719', 'name' => '龍崎區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '官田區', 'code' => '720', 'name' => '官田區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '麻豆區', 'code' => '721', 'name' => '麻豆區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '佳里區', 'code' => '722', 'name' => '佳里區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '西港區', 'code' => '723', 'name' => '西港區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '七股區', 'code' => '724', 'name' => '七股區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '將軍區', 'code' => '725', 'name' => '將軍區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '學甲區', 'code' => '726', 'name' => '學甲區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '北門區', 'code' => '727', 'name' => '北門區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '新營區', 'code' => '730', 'name' => '新營區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '後壁區', 'code' => '731', 'name' => '後壁區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '白河區', 'code' => '732', 'name' => '白河區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '東山區', 'code' => '733', 'name' => '東山區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '六甲區', 'code' => '734', 'name' => '六甲區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '下營區', 'code' => '735', 'name' => '下營區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '柳營區', 'code' => '736', 'name' => '柳營區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '鹽水區', 'code' => '737', 'name' => '鹽水區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '善化區', 'code' => '741', 'name' => '善化區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '大內區', 'code' => '742', 'name' => '大內區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '山上區', 'code' => '743', 'name' => '山上區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '新市區', 'code' => '744', 'name' => '新市區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 13, 'title' => '安定區', 'code' => '745', 'name' => '安定區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '新興區', 'code' => '800', 'name' => '新興區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '前金區', 'code' => '801', 'name' => '前金區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '苓雅區', 'code' => '802', 'name' => '苓雅區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '鹽埕區', 'code' => '803', 'name' => '鹽埕區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '鼓山區', 'code' => '804', 'name' => '鼓山區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '旗津區', 'code' => '805', 'name' => '旗津區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '前鎮區', 'code' => '806', 'name' => '前鎮區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '三民區', 'code' => '807', 'name' => '三民區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '楠梓區', 'code' => '811', 'name' => '楠梓區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '小港區', 'code' => '812', 'name' => '小港區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '左營區', 'code' => '813', 'name' => '左營區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '仁武區', 'code' => '814', 'name' => '仁武區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '大社區', 'code' => '815', 'name' => '大社區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '東沙群島', 'code' => '817', 'name' => '東沙群島', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '南沙群島', 'code' => '819', 'name' => '南沙群島', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '岡山區', 'code' => '820', 'name' => '岡山區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '路竹區', 'code' => '821', 'name' => '路竹區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '阿蓮區', 'code' => '822', 'name' => '阿蓮區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '田寮區', 'code' => '823', 'name' => '田寮區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '燕巢區', 'code' => '824', 'name' => '燕巢區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '橋頭區', 'code' => '825', 'name' => '橋頭區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '梓官區', 'code' => '826', 'name' => '梓官區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '彌陀區', 'code' => '827', 'name' => '彌陀區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '永安區', 'code' => '828', 'name' => '永安區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '湖內區', 'code' => '829', 'name' => '湖內區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '鳳山區', 'code' => '830', 'name' => '鳳山區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '大寮區', 'code' => '831', 'name' => '大寮區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '林園區', 'code' => '832', 'name' => '林園區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '鳥松區', 'code' => '833', 'name' => '鳥松區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '大樹區', 'code' => '840', 'name' => '大樹區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '旗山區', 'code' => '842', 'name' => '旗山區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '美濃區', 'code' => '843', 'name' => '美濃區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '六龜區', 'code' => '844', 'name' => '六龜區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '內門區', 'code' => '845', 'name' => '內門區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '杉林區', 'code' => '846', 'name' => '杉林區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '甲仙區', 'code' => '847', 'name' => '甲仙區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '桃源區', 'code' => '848', 'name' => '桃源區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '那瑪夏區', 'code' => '849', 'name' => '那瑪夏區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '茂林區', 'code' => '851', 'name' => '茂林區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 14, 'title' => '茄萣區', 'code' => '852', 'name' => '茄萣區', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '屏東市', 'code' => '900', 'name' => '屏東市', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '三地門鄉', 'code' => '901', 'name' => '三地門鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '霧臺鄉', 'code' => '902', 'name' => '霧臺鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '瑪家鄉', 'code' => '903', 'name' => '瑪家鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '九如鄉', 'code' => '904', 'name' => '九如鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '里港鄉', 'code' => '905', 'name' => '里港鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '高樹鄉', 'code' => '906', 'name' => '高樹鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '鹽埔鄉', 'code' => '907', 'name' => '鹽埔鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '長治鄉', 'code' => '908', 'name' => '長治鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '麟洛鄉', 'code' => '909', 'name' => '麟洛鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '竹田鄉', 'code' => '911', 'name' => '竹田鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '內埔鄉', 'code' => '912', 'name' => '內埔鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '萬丹鄉', 'code' => '913', 'name' => '萬丹鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '潮州鎮', 'code' => '920', 'name' => '潮州鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '泰武鄉', 'code' => '921', 'name' => '泰武鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '來義鄉', 'code' => '922', 'name' => '來義鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '萬巒鄉', 'code' => '923', 'name' => '萬巒鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '崁頂鄉', 'code' => '924', 'name' => '崁頂鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '新埤鄉', 'code' => '925', 'name' => '新埤鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '南州鄉', 'code' => '926', 'name' => '南州鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '林邊鄉', 'code' => '927', 'name' => '林邊鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '東港鎮', 'code' => '928', 'name' => '東港鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '琉球鄉', 'code' => '929', 'name' => '琉球鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '佳冬鄉', 'code' => '931', 'name' => '佳冬鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '新園鄉', 'code' => '932', 'name' => '新園鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '枋寮鄉', 'code' => '940', 'name' => '枋寮鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '枋山鄉', 'code' => '941', 'name' => '枋山鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '春日鄉', 'code' => '942', 'name' => '春日鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '獅子鄉', 'code' => '943', 'name' => '獅子鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '車城鄉', 'code' => '944', 'name' => '車城鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '牡丹鄉', 'code' => '945', 'name' => '牡丹鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '恆春鎮', 'code' => '946', 'name' => '恆春鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 15, 'title' => '滿州鄉', 'code' => '947', 'name' => '滿州鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '臺東市', 'code' => '950', 'name' => '臺東市', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '綠島鄉', 'code' => '951', 'name' => '綠島鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '蘭嶼鄉', 'code' => '952', 'name' => '蘭嶼鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '延平鄉', 'code' => '953', 'name' => '延平鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '卑南鄉', 'code' => '954', 'name' => '卑南鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '鹿野鄉', 'code' => '955', 'name' => '鹿野鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '關山鎮', 'code' => '956', 'name' => '關山鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '海端鄉', 'code' => '957', 'name' => '海端鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '池上鄉', 'code' => '958', 'name' => '池上鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '東河鄉', 'code' => '959', 'name' => '東河鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '成功鎮', 'code' => '961', 'name' => '成功鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '長濱鄉', 'code' => '962', 'name' => '長濱鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '太麻里鄉', 'code' => '963', 'name' => '太麻里鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '金峰鄉', 'code' => '964', 'name' => '金峰鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '大武鄉', 'code' => '965', 'name' => '大武鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 16, 'title' => '達仁鄉', 'code' => '966', 'name' => '達仁鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 17, 'title' => '花蓮市', 'code' => '970', 'name' => '花蓮市', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 17, 'title' => '新城鄉', 'code' => '971', 'name' => '新城鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 17, 'title' => '秀林鄉', 'code' => '972', 'name' => '秀林鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 17, 'title' => '吉安鄉', 'code' => '973', 'name' => '吉安鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 17, 'title' => '壽豐鄉', 'code' => '974', 'name' => '壽豐鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 17, 'title' => '鳳林鎮', 'code' => '975', 'name' => '鳳林鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 17, 'title' => '光復鄉', 'code' => '976', 'name' => '光復鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 17, 'title' => '豐濱鄉', 'code' => '977', 'name' => '豐濱鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 17, 'title' => '瑞穗鄉', 'code' => '978', 'name' => '瑞穗鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 17, 'title' => '萬榮鄉', 'code' => '979', 'name' => '萬榮鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 17, 'title' => '玉里鎮', 'code' => '981', 'name' => '玉里鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 17, 'title' => '卓溪鄉', 'code' => '982', 'name' => '卓溪鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 17, 'title' => '富里鄉', 'code' => '983', 'name' => '富里鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 18, 'title' => '宜蘭市', 'code' => '260', 'name' => '宜蘭市', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 18, 'title' => '頭城鎮', 'code' => '261', 'name' => '頭城鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 18, 'title' => '礁溪鄉', 'code' => '262', 'name' => '礁溪鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 18, 'title' => '壯圍鄉', 'code' => '263', 'name' => '壯圍鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 18, 'title' => '員山鄉', 'code' => '264', 'name' => '員山鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 18, 'title' => '羅東鎮', 'code' => '265', 'name' => '羅東鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 18, 'title' => '三星鄉', 'code' => '266', 'name' => '三星鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 18, 'title' => '大同鄉', 'code' => '267', 'name' => '大同鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 18, 'title' => '五結鄉', 'code' => '268', 'name' => '五結鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 18, 'title' => '冬山鄉', 'code' => '269', 'name' => '冬山鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 18, 'title' => '蘇澳鎮', 'code' => '270', 'name' => '蘇澳鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 18, 'title' => '南澳鄉', 'code' => '272', 'name' => '南澳鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 18, 'title' => '釣魚臺', 'code' => '290', 'name' => '釣魚臺', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 19, 'title' => '馬公市', 'code' => '880', 'name' => '馬公市', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 19, 'title' => '西嶼鄉', 'code' => '881', 'name' => '西嶼鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 19, 'title' => '望安鄉', 'code' => '882', 'name' => '望安鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 19, 'title' => '七美鄉', 'code' => '883', 'name' => '七美鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 19, 'title' => '白沙鄉', 'code' => '884', 'name' => '白沙鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 19, 'title' => '湖西鄉', 'code' => '885', 'name' => '湖西鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 20, 'title' => '金沙鎮', 'code' => '890', 'name' => '金沙鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 20, 'title' => '金湖鎮', 'code' => '891', 'name' => '金湖鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 20, 'title' => '金寧鄉', 'code' => '892', 'name' => '金寧鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 20, 'title' => '金城鎮', 'code' => '893', 'name' => '金城鎮', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 20, 'title' => '烈嶼鄉', 'code' => '894', 'name' => '烈嶼鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 20, 'title' => '烏坵鄉', 'code' => '896', 'name' => '烏坵鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 21, 'title' => '南竿鄉', 'code' => '209', 'name' => '南竿鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 21, 'title' => '北竿鄉', 'code' => '210', 'name' => '北竿鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 21, 'title' => '莒光鄉', 'code' => '211', 'name' => '莒光鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['county_id' => $startCountyId + 21, 'title' => '東引鄉', 'code' => '212', 'name' => '東引鄉', 'created_at' => $timestamp, 'updated_at' => $timestamp],
        ];

        SeederHelper::setLanguageExchange($worldCityData, $languageResourceData, 'world_city', 'name', $languageList, $startCityId);

        DB::table('world_city')->insert($worldCityData);

        // 全球化 - 貨幣
        $startCurrencyId = $rowCurrencyId = SeederHelper::getTableNextIncrement('world_currency');
        $rowCurrencyId--;

        $worldCurrencyData = [
            ['title' => '新臺幣', 'code' => 'TWD', 'name' => 'world_currency.name.' . ++$rowCurrencyId,
                'options' => json_encode(['symbol' => 'NT$', 'native' => 'NTD', 'exchange' => 1.00]),
                'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['title' => '人民幣', 'code' => 'CNY', 'name' => 'world_currency.name.' . ++$rowCurrencyId,
                'options' => json_encode(['symbol' => '¥', 'native' => 'RMB', 'exchange' => 0.223905399]),
                'active' => false, 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['title' => '日圓', 'code' => 'JPY', 'name' => 'world_currency.name.' . ++$rowCurrencyId,
                'options' => json_encode(['symbol' => '¥', 'native' => 'JPY', 'exchange' => 3.65070264]),
                'active' => false, 'created_at' => $timestamp, 'updated_at' => $timestamp],
            ['title' => '美元', 'code' => 'USD', 'name' => 'world_currency.name.' . ++$rowCurrencyId,
                'options' => json_encode(['symbol' => '$', 'native' => 'USD', 'exchange' => 0.032473]),
                'active' => false, 'created_at' => $timestamp, 'updated_at' => $timestamp],
        ];

        DB::table('world_currency')->insert($worldCurrencyData);

        // 多語系
        $currencyLanguage = [
            'zh-Hant' => [
                ['name' => '新臺幣'], ['name' => '人民幣'], ['name' => '日圓'], ['name' => '美元']
            ],
            'zh-Hans' => [
                ['name' => '新台币'], ['name' => '人民币'], ['name' => '日圆'], ['name' => '美元']
            ],
            'ja' => [
                ['name' => '台湾元'], ['name' => '人民元'], ['name' => '日本円'], ['name' => '米ドル']
            ],
            'en' => [
                ['name' => 'New Taiwan dollar'], ['name' => 'Renminbi'], ['name' => 'Japanese yen'], ['name' => 'US dollar']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'world_currency', $currencyLanguage, $languageList, $startCurrencyId, false);

        DB::table('language_resource')->insert($languageResourceData);
    }

    protected function deleteDatabase()
    {
        DB::table('language_resource')
            ->where(function ($query) {
                /** @var \Illuminate\Database\Query\Builder $query */
                $query
                    ->where('key', 'like', 'world_continent.%')
                    ->orWhere('key', 'like', 'world_country.%')
                    ->orWhere('key', 'like', 'world_state.%')
                    ->orWhere('key', 'like', 'world_county.%')
                    ->orWhere('key', 'like', 'world_city.%')
                    ->orWhere('key', 'like', 'world_currency.%');
            })
            ->delete();
    }
}
