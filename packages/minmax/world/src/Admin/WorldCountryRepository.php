<?php

namespace Minmax\World\Admin;

use Minmax\Base\Admin\Repository;
use Minmax\World\Models\WorldCountry;

/**
 * Class WorldCountryRepository
 * @property WorldCountry $model
 * @method WorldCountry find($id)
 * @method WorldCountry one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method WorldCountry create($attributes)
 * @method WorldCountry save($model, $attributes)
 * @method WorldCountry|\Illuminate\Database\Eloquent\Builder query()
 */
class WorldCountryRepository extends Repository
{
    const MODEL = WorldCountry::class;

    protected $sort = 'sort';

    protected $sorting = true;

    protected $languageColumns = ['name'];

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'world_country';
    }

    protected function getSortWhere()
    {
        return "continent_id = '{$this->model->continent_id}'";
    }

    public function getSelectParameters()
    {
        return $this->all()
            ->mapWithKeys(function ($item) {
                /** @var WorldCountry $item */
                return [$item->id => ['title' => $item->name, 'options' => $item->options]];
            })
            ->toArray();
    }
}
