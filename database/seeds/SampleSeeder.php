<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Minmax\Base\Helpers\Seeder as SeederHelper;

class SampleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertWebMenu();

        $this->insertInboxCategory();
    }

    protected function insertWebMenu()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $insertWebMenuData = [
            [
                'id' => $menuRootId1 = uuidl(),
                'parent_id' => null,
                'title' => 'web_menu.title.' . $menuRootId1,
                'uri' => 'root-header',
                'controller' => null,
                'model' => null,
                'link' => null,
                'permission_key' => null,
                'options' => null,
                'sort' => 1, 'editable' => false, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'id' => $menuId1 = uuidl(),
                'parent_id' => $menuRootId1,
                'title' => 'web_menu.title.' . $menuId1,
                'uri' => 'news',
                'controller' => null,
                'model' => null,
                'link' => url('news/categories/with-category-id'),
                'permission_key' => null,
                'options' => null,
                'sort' => 1, 'editable' => true, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
            [
                'id' => $menuRootId2 = uuidl(),
                'parent_id' => null,
                'title' => 'web_menu.title.' . $menuRootId2,
                'uri' => 'root-footer',
                'controller' => null,
                'model' => null,
                'link' => null,
                'permission_key' => null,
                'options' => null,
                'sort' => 2, 'editable' => false, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];

        DB::table('web_menu')->insert($insertWebMenuData);

        $webMenuLanguage = [
            'zh-Hant' => [
                ['id' => $menuRootId1, 'title' => '網站主選單'],
                ['id' => $menuId1, 'title' => '最新消息'],
                ['id' => $menuRootId2, 'title' => '頁尾選單']
            ],
            'zh-Hans' => [
                ['id' => $menuRootId1, 'title' => '网站主选单'],
                ['id' => $menuId1, 'title' => '最新消息'],
                ['id' => $menuRootId2, 'title' => '页尾选单']
            ],
            'ja' => [
                ['id' => $menuRootId1, 'title' => 'サイトメニュー'],
                ['id' => $menuId1, 'title' => 'ニュース'],
                ['id' => $menuRootId2, 'title' => 'フッターメニュー']
            ],
            'en' => [
                ['id' => $menuRootId1, 'title' => 'Site Main Menu'],
                ['id' => $menuId1, 'title' => 'News'],
                ['id' => $menuRootId2, 'title' => 'Footer Menu']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'web_menu', $webMenuLanguage, $languageList, null, false);

        DB::table('language_resource')->insert($languageResourceData);
    }

    protected function insertInboxCategory()
    {
        $timestamp = date('Y-m-d H:i:s');
        $languageList = SeederHelper::getLanguageIdList();
        $languageResourceData = [];

        $insertCategoryData = [
            [
                'id' => $categoryId = uuidl(),
                'code' => 'contact',
                'title' => 'inbox_category.title.' . $categoryId,
                'notifiable' => true,
                'bcc' => null,
                'options' => json_encode([
                    'icon' => 'icon-mail-envelope-open text-primary',
                ]),
                'editable' => true,
                'sort' => 1, 'active' => true, 'created_at' => $timestamp, 'updated_at' => $timestamp
            ],
        ];
        DB::table('inbox_category')->insert($insertCategoryData);

        $categoryLanguage = [
            'zh-Hant' => [
                ['id' => $categoryId, 'title' => '聯絡我們']
            ],
            'zh-Hans' => [
                ['id' => $categoryId, 'title' => '联络我们']
            ],
            'ja' => [
                ['id' => $categoryId, 'title' => 'お問い合わせ']
            ],
            'en' => [
                ['id' => $categoryId, 'title' => 'Contact Us']
            ],
        ];
        SeederHelper::setLanguageResource($languageResourceData, 'inbox_category', $categoryLanguage, $languageList, null, false);

        DB::table('language_resource')->insert($languageResourceData);

    }
}
