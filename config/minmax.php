<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Minmax Project Parameters
    |--------------------------------------------------------------------------
    |
    | Those parameters are used for project. Usually they are set at .env file.
    |
    */

    'editor_module' => env('EDITOR_MODULE', 'tinymce'),

    'menu_layer_limit' => env('MENU_LAYER_LIMIT', 2),

    'article_layer_limit' => env('ARTICLE_LAYER_LIMIT', 3),

    'ecommerce_layer_limit' => env('ECOMMERCE_LAYER_LIMIT', 3),

    'draft_limit' => env('DRAFT_LIMIT', 5),

];
