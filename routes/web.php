<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@index')->name('home');
Route::get('about-us', 'SiteController@about_us')->name('about-us');
Route::get('news', 'NewsController@news')->name('news');
Route::get('article-details', 'NewsController@news')->name('news');
Route::get('news-details/{id}', 'NewsController@news_details')->name('news_details');
Route::get('display', 'ProductController@display')->name('display');
Route::get('article-details/{id}/{id2}', 'NewsController@article_details')->name('article_details');
Route::get('qa', 'SiteController@qa')->name('qa');
Route::post('ajaxForm', 'SiteController@ajax_form')->name('ajaxForm');
Route::get('download', 'SiteController@download')->name('download');
Route::get('product/{id}', 'ProductController@product')->name('product');
Route::get('product-details/{id}/{id2?}', 'ProductController@product_details')->name('product-details');
Route::get('contact-us', 'ContactController@contact_us')->name('contact-us');
Route::post('send_mail', 'ContactController@send_mail')->name('send_mail');
Route::post('add_car', 'ProductController@add_car')->name('add_car');
Route::get('product-inquiry', 'ProductController@product_inquiry')->name('product-inquiry');
Route::post('send_inquriy', 'ProductController@send_inquriy')->name('send_inquriy');
