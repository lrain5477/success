<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'siteadmin'], function () {

    Route::group(['middleware' => 'auth:admin'], function() {
        Route::get('history', 'HistoryController@index')->name('history.index');
        Route::post('history', 'HistoryController@store')->name('history.store');
        Route::get('history/create', 'HistoryController@create')->name('history.create');
        Route::get('history/{id}', 'HistoryController@show')->name('history.show');
        Route::put('history/{id}', 'HistoryController@update')->name('history.update');
        Route::delete('history/{id}', 'HistoryController@destroy')->name('history.destroy');
        Route::get('history/{id}/edit', 'HistoryController@edit')->name('history.edit');
        Route::post('history/ajax/datatables', 'HistoryController@ajaxDataTable')->name('history.ajaxDataTable');
        Route::patch('history/ajax/switch', 'HistoryController@ajaxSwitch')->name('history.ajaxSwitch');
        Route::patch('history/ajax/sort', 'HistoryController@ajaxSort')->name('history.ajaxSort');
        Route::patch('history/ajax/multi/switch', 'HistoryController@ajaxMultiSwitch')->name('history.ajaxMultiSwitch');
        Route::delete('history/ajax/multi/delete', 'HistoryController@ajaxMultiDestroy')->name('history.ajaxMultiDestroy');

        Route::get('space', 'SpaceController@index')->name('space.index');
        Route::post('space', 'SpaceController@store')->name('space.store');
        Route::get('space/create', 'SpaceController@create')->name('space.create');
        Route::get('space/{id}', 'SpaceController@show')->name('space.show');
        Route::put('space/{id}', 'SpaceController@update')->name('space.update');
        Route::delete('space/{id}', 'SpaceController@destroy')->name('space.destroy');
        Route::get('space/{id}/edit', 'SpaceController@edit')->name('space.edit');
        Route::post('space/ajax/datatables', 'SpaceController@ajaxDataTable')->name('space.ajaxDataTable');
        Route::patch('space/ajax/switch', 'SpaceController@ajaxSwitch')->name('space.ajaxSwitch');
        Route::patch('space/ajax/sort', 'SpaceController@ajaxSort')->name('space.ajaxSort');
        Route::patch('space/ajax/multi/switch', 'SpaceController@ajaxMultiSwitch')->name('space.ajaxMultiSwitch');
        Route::delete('space/ajax/multi/delete', 'SpaceController@ajaxMultiDestroy')->name('space.ajaxMultiDestroy');

        Route::get('inquiry', 'InquiryController@index')->name('inquiry.index');
        Route::post('inquiry', 'InquiryController@store')->name('inquiry.store');
        Route::get('inquiry/create', 'InquiryController@create')->name('inquiry.create');
        Route::get('inquiry/{id}', 'InquiryController@show')->name('inquiry.show');
        Route::put('inquiry/{id}', 'InquiryController@update')->name('inquiry.update');
        Route::delete('inquiry/{id}', 'InquiryController@destroy')->name('inquiry.destroy');
        Route::get('inquiry/{id}/edit', 'InquiryController@edit')->name('inquiry.edit');
        Route::post('inquiry/ajax/datatables', 'InquiryController@ajaxDataTable')->name('inquiry.ajaxDataTable');
        Route::patch('inquiry/ajax/switch', 'InquiryController@ajaxSwitch')->name('inquiry.ajaxSwitch');
        Route::patch('inquiry/ajax/sort', 'InquiryController@ajaxSort')->name('inquiry.ajaxSort');
        Route::patch('inquiry/ajax/multi/switch', 'InquiryController@ajaxMultiSwitch')->name('inquiry.ajaxMultiSwitch');
        Route::delete('inquiry/ajax/multi/delete', 'InquiryController@ajaxMultiDestroy')->name('inquiry.ajaxMultiDestroy');
    });

});
