<?php

namespace App\Repositories\Admin;

use Minmax\Base\Admin\Repository;
use App\Models\Space;

/**
 * Class SpaceRepository
 * @property Space $model
 * @method Space find($id)
 * @method Space one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method Space[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method Space create($attributes)
 * @method Space save($model, $attributes)
 * @method Space|\Illuminate\Database\Eloquent\Builder query()
 */
class SpaceRepository extends Repository
{
    const MODEL = Space::class;

    protected $sort = 'sort';

    protected $sorting = true;

    //protected $languageColumns = [];

    //protected $subjectColumn = 'title';

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'space';
    }
}
