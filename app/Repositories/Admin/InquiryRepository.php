<?php

namespace App\Repositories\Admin;

use Minmax\Base\Admin\Repository;
use App\Models\Inquiry;

/**
 * Class InquiryRepository
 * @property Inquiry $model
 * @method Inquiry find($id)
 * @method Inquiry one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method Inquiry[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method Inquiry create($attributes)
 * @method Inquiry save($model, $attributes)
 * @method Inquiry|\Illuminate\Database\Eloquent\Builder query()
 */
class InquiryRepository extends Repository
{
    const MODEL = Inquiry::class;

    protected $sort = 'sort';

    protected $sorting = true;

    //protected $languageColumns = [];

    //protected $subjectColumn = 'title';

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'inquiry';
    }
}
