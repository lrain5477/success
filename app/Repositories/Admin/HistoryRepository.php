<?php

namespace App\Repositories\Admin;

use Minmax\Base\Admin\Repository;
use App\Models\History;

/**
 * Class HistoryRepository
 * @property History $model
 * @method History find($id)
 * @method History one($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method History[]|\Illuminate\Database\Eloquent\Collection all($column = null, $operator = null, $value = null, $boolean = 'and')
 * @method History create($attributes)
 * @method History save($model, $attributes)
 * @method History|\Illuminate\Database\Eloquent\Builder query()
 */
class HistoryRepository extends Repository
{
    const MODEL = History::class;

    protected $sort = 'sort';

    protected $sorting = true;

    protected $languageColumns = ['title','info'];

    //protected $subjectColumn = 'title';

    /**
     * Get table name of this model
     *
     * @return string
     */
    protected function getTable()
    {
        return 'history';
    }
}
