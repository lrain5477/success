<?php
/**
 * Created by PhpStorm.
 * User: design999
 * Date: 2019/7/2
 * Time: 下午 02:19
 */

namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Minmax\Base\Models\WebData;
use Minmax\Inbox\Models\InboxReceived;
use Minmax\Notify\Models\NotifyEmail;

class ContactMail_user extends Mailable
{
    use Queueable,SerializesModels;
    protected $notify;
    protected $webData;
    protected $contact;

    public function __construct(NotifyEmail $notify, InboxReceived $contact)
    {
        $this->notify = $notify;
        $this->webData = WebData::where('guard', 'web')->first();
        $this->contact = $contact;
    }

    public function build(){

        $replacements = [
            '{[logo]}'=>asset($this->webData->system_logo[0]['path']),
            '{[company]}'=>$this->contact->details['company'],
            '{[title]}'=>$this->contact->subject,
            '{[tel]}'=>$this->contact->details['phone'],
            '{[name]}'=>$this->contact->details['name'],
            '{[email]}'=>$this->contact->details['email'],
            '{[message]}'=>$this->contact->details['message'],


            '{[created_at]}' => $this->contact->created_at,

            '{[websitePhone]}' => array_get($this->webData->contact ?? [], 'phone', ''),
            '{[websiteEmail]}' => array_get($this->webData->contact ?? [], 'email', ''),
            '{[websiteName]}' => $this->webData->website_name,
            '{[websiteUrl]}' => $this->webData->system_url,
        ];
        $html = str_replace(array_keys($replacements), $replacements, $this->notify->custom_editor);

        return $this
            ->subject($this->notify->custom_subject)
            ->view('MinmaxNotify::email.layouts.default')
            ->with([
                'notifyData' => [
                    'subject' => $this->notify->custom_subject,
                    'perheader' => $this->notify->custom_preheader,
                    'editor' => $html,
                ],
            ]);
    }

}