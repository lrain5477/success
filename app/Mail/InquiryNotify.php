<?php


namespace App\Mail;


use App\Models\Inquiry;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Minmax\Base\Models\SiteParameterItem;
use Minmax\Base\Models\WebData;

use Minmax\Notify\Models\NotifyEmail;
use Minmax\Product\Models\ProductSet;

class InquiryNotify extends Mailable
{
    use Queueable,SerializesModels;

    protected $notify;
    protected $webData;
    protected $inquiry;

    public function __construct(NotifyEmail $notify,Inquiry $inquiry)
    {

        $this->notify = $notify;
        $this->webData = WebData::where('guard','web')->first();
        $this->inquiry = $inquiry;

    }
    public function build(){

        $tmp = json_decode($this->inquiry->product_list);
        $list = [];
        if(count($tmp) > 0) {
            foreach ($tmp as $key => $value) {
                $list[$key]['id'] = $value->productNo;
                $productData =  ProductSet::where('id',$value->productNo)->first();
                $list[$key]['productName'] = $productData ->title;
                $list[$key]['pic'] = $productData->pic[0]['path'];
                $list[$key]['spec'] = SiteParameterItem::where('id',$value->spec)->first()->label;
                $list[$key]['amount'] = $value->amount;
            }
        }
        $product_list= '';

        if(count($list) > 0) {
            $product_list='
                                    <table border="0" width="100%">
                                        <tbody>
                                            <tr>
                    
                                                <td>Product Name</td>
                                                <td>Appearance</td>
                                                 <td>Spec</td>
                                                <td>Quantity</td>
                                            </tr>';
            foreach ($list as $value) {
                $product_list.='  <tr>
                                <td style="line-height:80px">'.$value['productName'].'</td>
                                <td>
                                    <img src="'. asset(str_replace(" ","_",$value['pic']))  .'" width="80px" height="80px" class="CToWUd"></td>

                                <td>'.$value['spec'].'</td>
                                <td>'.$value['amount'].'</td>
                            </tr>';
            }
            $product_list .= ' </tbody>
                                    </table>
                               ';
        }
        $replacements = [
            '{[logo]}'=>asset($this->webData->system_logo[0]['path']),
            '{[product_list]}'=>$product_list,
            '{[company]}'=>$this->inquiry->company,

            '{[website]}'=>$this->inquiry->website,
            '{[address]}'=>$this->inquiry->address,
            '{[firstname]}'=>$this->inquiry->first_name,
            '{[lastname]}'=>$this->inquiry->last_name,
            '{[tel]}'=>$this->inquiry->tel,
            '{[fax]}'=>$this->inquiry->fax,
            '{[email]}'=>$this->inquiry->email,
            '{[country]}'=>$this->inquiry->country,

            '{[created_at]}' => $this->inquiry->created_at,
            '{[sitadmin_url]}' => url("/")."/siteadmin/",
            '{[websitePhone]}' => array_get($this->webData->contact ?? [], 'phone', ''),
            '{[websiteEmail]}' => array_get($this->webData->contact ?? [], 'email', ''),
            '{[websiteName]}' => $this->webData->website_name,
            '{[websiteUrl]}' => $this->webData->system_url,
        ];

        $html = str_replace(array_keys($replacements), $replacements, $this->notify->admin_editor);

        return $this
            ->subject($this->notify->custom_subject)
            ->view('MinmaxNotify::email.layouts.default')
            ->with([
                'notifyData' => [
                    'subject' => $this->notify->admin_subject,
                    'perheader' => $this->notify->admin_preheader,
                    'editor' => $html,
                ],
            ]);

    }
}