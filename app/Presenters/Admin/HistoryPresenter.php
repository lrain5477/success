<?php

namespace App\Presenters\Admin;

use Minmax\Base\Admin\Presenter;

/**
 * Class HistoryPresenter
 */
class HistoryPresenter extends Presenter
{
    protected $packagePrefix = '';

    protected $languageColumns = ['title','info'];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'active' => systemParam('active'),
        ];
    }
}
