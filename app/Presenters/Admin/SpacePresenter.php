<?php

namespace App\Presenters\Admin;

use Minmax\Base\Admin\Presenter;

/**
 * Class SpacePresenter
 */
class SpacePresenter extends Presenter
{
    protected $packagePrefix = '';

    //protected $languageColumns = [];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'active' => systemParam('active'),
        ];
    }
}
