<?php

namespace App\Presenters\Admin;

use Minmax\Base\Admin\Presenter;

/**
 * Class InquiryPresenter
 */
class InquiryPresenter extends Presenter
{
    protected $packagePrefix = '';

    //protected $languageColumns = [];

    public function __construct()
    {
        parent::__construct();

        $this->parameterSet = [
            'active' => systemParam('active2'),
        ];
    }
}
