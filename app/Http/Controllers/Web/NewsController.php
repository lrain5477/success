<?php


namespace App\Http\Controllers\Web;


use Minmax\Article\Models\ArticleCategory;
use Minmax\Article\Models\ArticleColumn;
use Minmax\Article\Models\ArticleNews;

class NewsController extends Controller
{
    public function __construct(){

        parent::__construct();
    }

    public function news(){
        $banner = $this ->getBanner('news-banner');
        $category_id = ArticleCategory::where('uri','news')->first()->id;
        $idAry = $this->getArticleIdAryFromId($category_id);
        $pageData = ArticleNews::whereIn('id',$idAry)->where('active',1)->where(function ($query){
            $query->where('start_at',null)->orWhere('start_at','<=',date('Y-m-d'));
        })->where(function ($query){
            $query->where('end_at',null)->orWhere('end_at', ">=",date('Y-m-d'));
        })->orderBy('start_at','DESC')->paginate(8)->appends(request()->input());
        $data = $pageData->items();
        $this->viewData['pageData'] = $pageData;
        $this->viewData['banner'] = $banner;
        $this->viewData['data'] = $data;
        return view('web.news', $this->viewData);

    }

    public function news_details($id) {
        $banner = $this ->getBanner('news-banner');
        $data = ArticleNews::where('id',$id)->first();
        $this->addTrack('Minmax\Article\Models\ArticleNews',$id);
        $this->viewData['banner'] = $banner;
        $this->viewData['data'] = $data;
        return view('web.news-details', $this->viewData);


    }

    public function article_details($id,$id2 ="") {
        $banner = $this ->getBanner('article-banner');
        $data = ArticleColumn::where('id',$id)->first();
        $this->addTrack('Minmax\Article\Models\ArticleColumn',$id);
        $this->viewData['banner'] = $banner;
        $this->viewData['data'] = $data;
        $this->viewData['id2'] = $id2;

        return view('web.article-details', $this->viewData);
    }
}