<?php

namespace App\Http\Controllers\Web;



use App\Models\History;
use Minmax\Article\Models\ArticleDownload;
use Minmax\Article\Models\ArticleFaq;

/**
 * Class SiteController
 */
class SiteController extends Controller
{
    public function __construct(){

        parent::__construct();
    }

    public function index()
    {
        $banner = $this ->getBanner('indexMobileBanner');

        $this->viewData['banner'] = $banner;
        return view('web.index', $this->viewData);
    }

    public function about_us(){
        $banner = $this ->getBanner('about-banner');
        $history = History::where('active',1)->orderBy('sort','ASC')->get();
        $data = $this->getPage('about-us');
        $this->setSeo($data->seo,$data->title."|".$this->pageTitle);
        $this->viewData['banner'] = $banner;
        $this->viewData['data'] = $data;
        $this->viewData['history'] = $history;
        return view('web.about-us', $this->viewData);
    }

    public function qa(){
        $banner = $this ->getBanner('qa-banner');
        $data = ArticleFaq::where('active',1)->where(function ($query){
            $query->where('start_at',null)->orWhere('start_at','<=',date('Y-m-d'));
        })->where(function ($query){
            $query->where('end_at',null)->orWhere('end_at', ">=",date('Y-m-d'));
        })->orderBy('start_at','DESC')->get();

        $this->viewData['banner'] = $banner;
        $this->viewData['data'] = $data;
        return view('web.qa', $this->viewData);
    }

    public function ajax_form(){
        $type = request()->get('type');
        if($type == 'addTrace') {
            $id = request()->get('id');
            $this->addTrack('Minmax\Article\Models\ArticleFaq',$id);
        }
        else if($type == "deleteItem") {
            $id = request()->get('id');
            if(session()->has('inquiry.'.$id)) {
                session()->forget('inquiry.'.$id);
                session()->save();

                echo json_encode(true);
                exit;
            }
            else {
                echo json_encode(false);
                exit;
            }


        }

    }

    public function download(){
        $banner = $this ->getBanner('download-banner');
        $data = ArticleDownload::where('active',1)->orderBy('sort','DESC')->get();
        $this->viewData['banner'] = $banner;
        $this->viewData['data'] = $data;
        return view('web.download', $this->viewData);

    }
}
