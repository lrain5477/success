<?php


namespace App\Http\Controllers\Web;


use App\Models\Inquiry;
use App\Models\Space;
use Illuminate\Http\Request;
use Minmax\Base\Models\SiteParameterItem;
use Minmax\Notify\Models\NotifyEmail;
use Minmax\Product\Models\ProductCategory;
use Minmax\Product\Models\ProductSet;

class ProductController extends Controller
{
    public function __construct(){

        parent::__construct();
    }

    public function display(){
        $banner = $this ->getBanner('display-banner');
        $pageData = Space::where('active',1)->orderBy('sort','DESC')->paginate(4)->appends(request()->input());
        $data = $pageData->items();

        $this->viewData['pageData'] = $pageData;
        $this->viewData['banner'] = $banner;
        $this->viewData['data'] = $data;

        return view('web.display', $this->viewData);

    }
    public function product($id){
        $banner = $this ->getBanner('product-banner');
        $idAry = [];
        $tmpIdAry = \DB::table('product_category_set')->where('category_id',$id)->get();
        if($tmpIdAry ->count() >0) foreach ($tmpIdAry as $value) $idAry[] = $value->set_id;
        $pageData = ProductSet::whereIn('id',$idAry)->where('active',1)->where(function ($query){
            $query->where('start_at',null)->orWhere('start_at','<=',date('Y-m-d'));
        })->where(function ($query){
            $query->where('end_at',null)->orWhere('end_at', ">=",date('Y-m-d'));
        })->orderBy('sort','DESC')->paginate(8)->appends(request()->input());
        $data = $pageData->items();
        $parentId = ProductCategory::where('id',$id)->first()->parent_id;
        $this->viewData['parentId'] = $parentId;
        $this->viewData['pageData'] = $pageData;
        $this->viewData['banner'] = $banner;
        $this->viewData['data'] = $data;
        $this->viewData['id'] = $id;
        return view('web.product', $this->viewData);

    }

    public function product_details($id,$id2="") {
        $banner = $this ->getBanner('product-banner');
        $spec = [];
        $data = ProductSet::where('id',$id)->first();

        if(!empty($data)) {
            $this->setSeo($data->seo,$data->title."|".$this->pageTitle);
            try {
                \DB::table('product_set_track')->insert(['model'=>'Minmax\Product\Models\ProductSet','set_id'=>$id,'ip'=>$this->ip,'click_at'=>date('Y-m-d H:i:s')]);
            }
            catch (\Exception $e) {}
            $specAry = $data->specifications;

            $siteParameter = SiteParameterItem::whereIn('id',$specAry)->get();

            if($siteParameter ->count() >0) foreach ($siteParameter as $value) $spec[$value->id] = $value->label;
        }

        $tmpIdAry = \DB::table('product_category_set')->where('category_id',$id2)->where('set_id','<>',$id)->get();

        $idAry = [];
        if($tmpIdAry ->count() >0) foreach ($tmpIdAry as $value) $idAry[] = $value->set_id;
        $related = ProductSet::whereIn('id',$idAry)->where('active',1)->orderBy('sort','desc')->get();
        $parentId = "";
        if(!empty($id2)) $parentId = ProductCategory::where('id',$id2)->first()->parent_id;

        $this->viewData['parentId'] = $parentId ?? '';
        $this->viewData['banner'] = $banner;
        $this->viewData['data'] = $data;
        $this->viewData['id'] = $id;
        $this->viewData['spec'] = $spec;
        $this->viewData['id2'] = $id2;
        $this->viewData['related'] = $related;
        return view('web.product-details', $this->viewData);
    }
    public function product_inquiry(){
        $banner = $this ->getBanner('inquiry-banner');
        $inquiry = [];
        $tmpData = session()->get('inquiry');

        if(count($tmpData) >0 && !empty($tmpData)) {
            foreach ($tmpData as $key => $value) {
                $product = ProductSet::where('id',$value['productNo'])->where('active',1)->first();
                $spec = SiteParameterItem::where('id',$value['spec'])->first();
                if(!empty($product)) {
                    $inquiry[$key]['id'] = $product->id;
                    $inquiry[$key]['title'] = $product->title;
                    $inquiry[$key]['spec'] = $spec->label;
                    $inquiry[$key]['pic'] = $product->pic[0]['path'];

                }
            }
        }

        $this->viewData['banner'] = $banner;
        $this->viewData['data'] = $inquiry;
        return view('web.product-inquiry', $this->viewData);


    }
    public function add_car(Request $request){

        $POST = request()->input();
        $product = ProductSet::where('active',1)->where('id',$POST['productNo'])->first();
        if(empty($product) || empty($POST['spec'])) {
            return '<script>alert("'.__('web.Menu.Error').'")</script>';
        }

        if(empty($POST['spec']))  return '<script>alert("'.__('web.Menu.SpecError').'")</script>';
        $tmp = ['productNo'=>$POST['productNo'],'spec'=>$POST['spec']];

        if($request->session()->has('inquiry')) {
            $tmpInquiry = $request->session()->get('inquiry');

            if(count($tmpInquiry) >0) {
                foreach ($tmpInquiry as $value) {
                    if($value['productNo'] == $POST['productNo'] && $value['spec'] == $POST['spec'] ) {

                        return '<script>alert("'.__('web.Contact.InquiryAlready').'")</script>';
                    }
                }
            }
            $request->session()->push('inquiry',$tmp);

        }
        else $request->session()->put('inquiry',[$tmp]);


        $request->session()->save();

        return '<script>alert("'.__('web.Contact.InquiryAlready').'")</script>';


    }

    public function send_inquriy(){
        $POST = \request()->input();
        $amount = $POST['t_amount'];

        if(!session()->has('inquiry')) return '<script>alert("'.__('web.Contact.PlzAmount').'");</script>';
        if(empty($amount)) return '<script>alert("'.__('web.Contact.PlzAmount').'")</script>';


        foreach ($amount as $value) {
            if(!is_numeric($value ) ) return '<script>alert("'.__('web.Contact.PlzAmount2').'")</script>';


        }


        if($POST['checkCode'] != session()->get('admin_captcha_web_login')){

            return '<script>alert("'.__('web.Contact.CheckError').'")</script>';
        }
        $inquiry = session()->get('inquiry');
        if(empty($inquiry)) return '<script>alert("'.__('web.Contact.InquiryError').'")</script>';
        foreach ($inquiry as $key => $value) $inquiry[$key]['amount'] = $POST['t_amount'][$key];

        $row = new Inquiry();
        $row->company = $POST['company'];
        $row ->website = $POST['website'];
        $row-> first_name= $POST['firstName'];
        $row-> last_name= $POST['lastName'];
        $row->country = $POST['country'];
        $row->email = $POST['email'];
        $row-> tel= $POST['tel'];
        $row-> fax = $POST['fax'];
        $row-> address = $POST['address'];
        $row-> product_list = json_encode($inquiry);
        $row->active = 0;
        $row->created_at = date('Y-m-d H:i:s');
        $row->save();
        try{
            $notify = NotifyEmail::where('code','inquiry')->where('active',1)->first();

            if(!empty($notify)) sendNotifyEmail($notify,$POST['email'],[$row]);
            return '<script>alert("'.__('web.Contact.InquirySuccess').'");parent.location.href="'.langRoute('web.home').'"</script>';
        }
        catch (\Exception $e) {
                            dd($e);
        }


    }

}