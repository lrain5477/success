<?php


namespace App\Http\Controllers\Web;
use Illuminate\Support\Facades\Route;
use Minmax\Ad\Models\Advertising;
use Minmax\Ad\Models\AdvertisingCategory;
use Minmax\Article\Models\ArticleCategory;
use Minmax\Article\Models\ArticleColumn;
use Minmax\Article\Models\ArticlePage;
use Minmax\Base\Admin\WebDataRepository;
use Minmax\Base\Models\WebMenu;
use Minmax\Base\Models\WorldLanguage;
use Minmax\Base\Web\Controller as BaseController;
use Minmax\Product\Models\ProductBrand;
use Minmax\Product\Models\ProductCategory;

class Controller extends BaseController
{
    protected $pageSeo;
    protected $footMenu;
    protected $lang;
    protected $pageTitle;
    protected $fb;
    protected $currentRouteName;
    protected $ip;
    protected $link;
    public function __construct()
    {
        parent::__construct();

        $this->languageData = WorldLanguage::where('active',1)->orderBy('sort','asc')->get();

        $this->webData = (new WebDataRepository())->getData('web') ?? abort(404);

        $this->ip = \Request::ip();
        $this -> middleware(function($request,$next){
            $this->lang = App()->getLocale();
            $this->viewData['lang'] = $this->lang;
            $this->setSeo();

            $this->setDefaultViewData();
            return $next($request);
        });
        $this->pageTitle = $this->webData->website_name;
        $this->fb = ['title'=> $this->pageTitle,'url'=>url()->full(),'site_name'=>$this->webData->website_name,'description'=>$this->pageSeo['meta_description'],'image'=>$this->webData->system_logo[0]['path']];
        $tmp =explode(".",\Route::currentRouteName()) ;
        $RouteName = end($tmp);

    }

    public function setSeo($Data=[],$title = "") {

        $this->pageData['meta_description'] = $Data['meta_description'] ?? $this->webData->seo['meta_description'];
        $this->pageData['meta_keywords'] = $Data['meta_keywords'] ?? $this->webData->seo['meta_keywords'];
        $this->viewData['pageTitle'] = $title == "" ? $this->pageTitle : $title;

        //  $this->viewData['pageSubTitle'] = $this->pageSubTitle != "" ? $this->pageSubTitle : '';
        $this->viewData['pageSeo'] = $this->pageData;
        $this->fb['title'] =  $this->viewData['pageTitle'];
        $this->fb['description'] = $this->pageData['meta_keywords'];
        $this->fb['image'] = $Data['image'] ?? '';
        $this->viewData['fb'] = $this->fb;

    }
    protected function setDefaultViewData()
    {



        $this->link = "/";
        $linkName="繁體中文";
        if(App()->getLocale() != 'zh-Hant') $this->link = '/'.App()->getLocale().'/';
        if(App()->getLocale() == 'en') $linkName = 'English';
        $productMenu = [];
        $firstCategoryId = "";
        $tmpProductCate1 = ProductCategory::where('active',1)->where('parent_id',null)->orderBy('sort','ASC')->get();
        if($tmpProductCate1 -> count() >0) {
            foreach ($tmpProductCate1 as $key => $value) {
                if($key == 0) $firstCategoryId = $value->id;
                $productMenu[$key] = $value;

                $tmpProductCate2 = ProductCategory::where('parent_id',$value->id)->where('active',1)->orderBy('sort','ASC')->get();
                $productMenu[$key]['child'] = [];
                if($tmpProductCate2 ->count() >0) {
                    $data = [];
                    foreach ($tmpProductCate2 as $k => $v) {
                        $data[] = $v;

                    }
                    $productMenu[$key]['child'] = $data;
                }

            }
        }

        $knowMenu = [];
        $knowCategoryId = ArticleCategory::where('uri',"article-column")->first()->id;
        $tmpKnowCateogry1 = ArticleCategory::Where('parent_id',$knowCategoryId)->where('active',1)->orderBy('sort','ASC')->get();
        if($tmpKnowCateogry1 ->count() >0) {
            foreach ($tmpKnowCateogry1 as $key => $value) {
                $knowMenu[$key] = $value;
                $tmpData = $this->getArticleDataFromId(new ArticleColumn(),$value->id,['sort','DESC']);
                $data = [];
                if($tmpData ->count() >0) foreach ($tmpData as $v) $data[]= $v;
                $knowMenu[$key]["child"] = $data;

            }
        }

        $this->viewData['firstCategoryId'] = $firstCategoryId;
        $this->viewData['productMenu'] = $productMenu;
        $this->viewData['knowMenu'] = $knowMenu;
        $this->viewData['languageData'] = $this->languageData;
        $this->viewData['currentRouteName'] = $this->currentRouteName;
        $this->viewData['webData'] = $this->webData;
        $this->viewData['link'] = $this->link;
        $this->viewData['linkName'] = $linkName;
        $this->viewData['fb'] = $this->fb;
        $this->viewData['ip'] = $this->ip;

        $this->viewData['pageData'] = $this->pageData;
        $this->viewData['pageTitle'] = $this->pageTitle;
        $this->viewData['rootUri'] = ($this->webData->system_langua== app()->getLocale() ? '' : (app()->getLocale() . '/')) . $this->rootUri;

    }

    public function getBanner($uri) {
        $categoryId = AdvertisingCategory::where('code',$uri)->first()->id;
        $banner = Advertising::where('category_id',$categoryId)->first();
        return $banner;
    }

    public function getPage($uri) {
        $data = ArticlePage::where('uri',$uri)->first();
        $this->addTrack('Minmax\Article\Models\ArticleNews',$data->id);

        return $data;
    }

    public function addTrack($model,$object_id) {
        try {
            \DB::table('article_track')->insert(['model'=>$model,'object_id'=>$object_id,'ip'=>$this->ip,'click_at'=>date('Y-m-d H:i:s')]);
        }
        catch (\Exception $e) {}

    }

    public function ArticleData($model,$uri,$sort=['sort','ASC']) {
        $categoryId = ArticleCategory::where('uri',$uri)->first()->id;


        return $this->getArticleDataFromId($model,$categoryId,$sort);
    }
    public function getArticleDataFromId($model,$id,$sort = ['sort','ASC']) {
        $tmpId = \DB::table('article_category_relation')->where('category_id',$id)->get();
        $idAry = [];
        if($tmpId->count()>0) foreach ($tmpId as $value) $idAry[] = $value->object_id;
        $data = $model::whereIn('id',$idAry)->where('active',1)->where(function ($query){
            $query->where('start_at',null)->orWhere('start_at','<=',date('Y-m-d'));
        })->where(function ($query){
            $query->where('end_at',null)->orWhere('end_at', ">=",date('Y-m-d'));
        })->orderBy($sort[0],$sort[1])->get();
        return $data;
    }
    public function getArticleIdAry($uri) {
        $category_id = ArticleCategory::where('uri',$uri)->first()->id;

        return $this->getArticleIdAryFromId($category_id);
    }

    public function getArticleIdAryFromId($id) {
        $idAry = [];
        $tmpIdAry = \DB::table('article_category_relation')->where('category_id',$id)->get();

        if($tmpIdAry ->count() >0) foreach ($tmpIdAry as $value) $idAry[] = $value->object_id;
        return $idAry;

    }
}