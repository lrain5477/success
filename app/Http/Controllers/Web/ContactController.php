<?php


namespace App\Http\Controllers\Web;


use Minmax\Inbox\Models\InboxCategory;
use Minmax\Inbox\Models\InboxReceived;
use Minmax\Notify\Models\NotifyEmail;

class ContactController extends Controller
{
    public function __construct(){

        parent::__construct();
    }
    public function contact_us(){
        $banner = $this->getBanner('contact-banner');
        $this->viewData['banner'] = $banner;
        return view('web.contact-us', $this->viewData);

    }

    public function send_mail(){
        $POST = request()->input();
        if($POST['checkCode'] != session()->get('admin_captcha_login')) {
            return '<script>
                   alert("'.__('web.Contact.CheckCodeError').'");
                </script>';


        }
        foreach ($POST as $key => $value) if($key != "_token") $dataAry[$key]=$value;
        $categoryId = InboxCategory::where('active',1)->orderBy('sort','asc')->first()->id;
        $obj = new InboxReceived();
        $obj->id= uuidl();
        $obj ->subject = $POST['subject'];
        $obj->category_id = $categoryId;
        $obj->content = $POST['message'];
        $obj->ip = $this->ip;
        $obj->created_at = date('Y-m-d H:i:s');
        $obj->details =$dataAry;

        try{

            if ($obj->save()) {
                $notify = NotifyEmail::where('code','contact')->where('active',1)->first();

                if(!empty($notify)) sendNotifyEmail($notify,$POST['email'],[$obj]);
                return __('web.Contact.AlertSuccess');

            }
        }
        catch (\Exception $e) {
            dd($e->getMessage());
        }


    }

}