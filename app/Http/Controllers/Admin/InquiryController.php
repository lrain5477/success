<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Admin\InquiryRepository;
use Minmax\Base\Admin\Controller;

/**
 * Class InquiryController
 */
class InquiryController extends Controller
{
    protected $packagePrefix = '';

    public function __construct(InquiryRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }
}
