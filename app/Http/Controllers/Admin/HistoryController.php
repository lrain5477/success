<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Admin\HistoryRepository;
use Minmax\Base\Admin\Controller;

/**
 * Class HistoryController
 */
class HistoryController extends Controller
{
    protected $packagePrefix = '';

    public function __construct(HistoryRepository $repository)
    {
        $this->modelRepository = $repository;

        parent::__construct();
    }
}
