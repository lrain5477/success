<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Admin\SpaceRepository;
use Minmax\Base\Admin\Controller;

/**
 * Class SpaceController
 */
class SpaceController extends Controller
{
    protected $packagePrefix = '';

    public function __construct(SpaceRepository $repository)
    {

        $this->modelRepository = $repository;

        parent::__construct();
    }
}
