<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Space
 * @property integer $id
 * @property string $title
 * @property boolean $active
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class Space extends Model
{
    protected $table = 'space';
    protected $guarded = [];
    protected $casts = [
        'active' => 'boolean',
        'pic'=>'array',
        'pic2'=>'array',
    ];
}
