<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Inquiry
 * @property integer $id
 * @property string $title
 * @property boolean $active
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class Inquiry extends Model
{
    protected $table = 'inquiry';
    protected $guarded = [];
    protected $casts = [
       // 'active' => 'boolean',
    ];
}
