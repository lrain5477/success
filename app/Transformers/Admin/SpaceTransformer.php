<?php

namespace App\Transformers\Admin;

use Minmax\Base\Admin\Transformer;
use App\Models\Space;
use App\Presenters\Admin\SpacePresenter;

/**
 * Class SpaceTransformer
 */
class SpaceTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'spaceShow',
        'U' => 'spaceEdit',
        'D' => 'spaceDestroy',
    ];

    /**
     * Transformer constructor.
     * @param  SpacePresenter $presenter
     * @param  string $uri
     */
    public function __construct(SpacePresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  Space $model
     * @return array
     * @throws \Throwable
     */
    public function transform(Space $model)
    {


        return [
            'id' => $this->presenter->getGridCheckBox($model),
            'pic' => $this->presenter->getGridThumbnail($model, 'pic'),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
