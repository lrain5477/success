<?php

namespace App\Transformers\Admin;

use Minmax\Base\Admin\Transformer;
use App\Models\History;
use App\Presenters\Admin\HistoryPresenter;

/**
 * Class HistoryTransformer
 */
class HistoryTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'historyShow',
        'U' => 'historyEdit',
        'D' => 'historyDestroy',
    ];

    /**
     * Transformer constructor.
     * @param  HistoryPresenter $presenter
     * @param  string $uri
     */
    public function __construct(HistoryPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  History $model
     * @return array
     * @throws \Throwable
     */
    public function transform(History $model)
    {
        return [
            'id' => $this->presenter->getGridCheckBox($model),
            'title' => $this->presenter->getGridText($model, 'title'),
            'sort' => $this->presenter->getGridSort($model, 'sort'),
            'active' => $this->presenter->getGridSwitch($model, 'active'),
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
