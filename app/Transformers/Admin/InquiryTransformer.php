<?php

namespace App\Transformers\Admin;

use Minmax\Base\Admin\Transformer;
use App\Models\Inquiry;
use App\Presenters\Admin\InquiryPresenter;

/**
 * Class InquiryTransformer
 */
class InquiryTransformer extends Transformer
{
    protected $permissions = [
        'R' => 'inquiryShow',
        'U' => 'inquiryEdit',
        'D' => 'inquiryDestroy',
    ];

    /**
     * Transformer constructor.
     * @param  InquiryPresenter $presenter
     * @param  string $uri
     */
    public function __construct(InquiryPresenter $presenter, $uri)
    {
        $this->presenter = $presenter;

        parent::__construct($uri);
    }

    /**
     * @param  Inquiry $model
     * @return array
     * @throws \Throwable
     */
    public function transform(Inquiry $model)
    {
        $Ary = ['未處理','處理中','已處理'];
        return [
            'id' => $this->presenter->getGridCheckBox($model),
            'company' => $this->presenter->getGridText($model, 'company'),
            'email' => $this->presenter->getGridText($model, 'email'),
            'created_at' => date('Y-m-d H:i:s',strtotime($model->created_at)),
            'active' => $Ary[$model->active],
            'action' => $this->presenter->getGridActions($model),
        ];
    }
}
