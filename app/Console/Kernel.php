<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Process Listener Queue (Every work only retry 3 time if go fail)
        if (config('queue.default') == 'database') {
            $schedule->command('queue:work --queue=listeners,sms,emails --sansdaemon --tries=3')
                ->cron('* * * * *')->withoutOverlapping();

            if (app()->getProvider(\Minmax\Newsletter\ServiceProvider::class)) {
                $schedule->command('minmax:newsletter')->cron('* * * * *')->withoutOverlapping();
                $schedule->command('queue:work --queue=newsletterSms,newsletterEpaper --sansdaemon --tries=3')
                    ->cron('* * * * *')->withoutOverlapping()->everyFiveMinutes();
            }
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
