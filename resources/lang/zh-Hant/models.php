<?php

return [

    'History'=>[
        'title'=>'標題',
        'year'=>'年份',
        'info'=>'內文',
        'active'=>'啟用',
        'sort'=>'排序',
        'created_at'=>'創建日期',
        'updated_at'=>'修改日期',
        ''=>'',
        ''=>'',
        ''=>'',
        ''=>'',
    ],
    'Space'=>[
        'pic'=>'封面圖片',
        'pic2'=>'呈現圖片',
        'info'=>'內文',
        'active'=>'啟用',
        'sort'=>'排序',
        'created_at'=>'創建日期',
        'updated_at'=>'修改日期',
        'hint'=>[
            'pic'=>'建議尺吋 800X235, 數量限制：1張',
            'pic2'=>'建議尺吋 800X535, 數量限制：1張',
        ],
        ''=>'',
        ''=>'',
        ''=>'',
    ],
    'Inquiry'=>[
        'company'=>'公司名稱',
        'website'=>'網址',
        'country'=>'國家',
        'first_name'=>'FirstName',
        'last_name'=>'LastName',
        'created_at'=>'創建日期',
        'updated_at'=>'修改日期',
        'hint'=>[
            'pic'=>'建議尺吋 800X235, 數量限制：1張',
            'pic2'=>'建議尺吋 800X535, 數量限制：1張',
        ],
        'email'=>'電子郵件',
        'tel'=>'電話',
        'fax'=>'傳真',
        'address'=>'地址',
        'memo'=>'處理備註',

        'active'=>'狀態',
        ''=>'',
        ''=>'',
        ''=>'',
        ''=>'',

    ],

];