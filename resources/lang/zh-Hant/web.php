<?php

return [
    'Menu'=>[
        'AboutUs'=>'關於我們',
        'News'=>'最新消息',
        'ProductList'=>'商品目錄',
        'Space'=>'空間展示',
        'Knowledge'=>'技術知識',
        'Download'=>'相關下載',
        'ContactUs'=>'聯絡我們',''=>'',
        'MainMenu'=>'主選單',
        'SiteMap'=>'網站地圖',
        'ProductCate'=>'商品分類',
        'Spec'=>'產品規格',
        'JoinInquiry'=>'加入詢價車',
        'Tab1'=>'矽酸鈣板特性',
        'Tab2'=>'PVC貼皮系列',
        'Related'=>'相關產品',
        'Error'=>'資料錯誤',
        'SpecError'=>'規格錯誤，請重新選擇',
        'Inquiry'=>'詢價車',
        'ProductPic'=>'產品圖片',
        'ProductName'=>'產品名稱',
        'ProductSpec'=>'產品規格',
        'ProductAmount'=>'數量',
        'Delete'=>'刪除',
        'InquiryForm'=>'詢價車',
        'InquiryTitle'=>' Enter your message in the box provided and include as many details as possible to help us assist you with
                            your inquiry. Fields marked with <span class="font_red">*</span>are required.',
        'ContactInfo'=>'聯絡資訊',
        ''=>'',
        ''=>'',
        ''=>'',
        ''=>'',''=>'',''=>'',
        ''=>'',


    ],
    'Contact' =>[
        'Info'=>'聯絡資訊',
        'Title1'=>'若您有任何問題或建議，請填寫下表並提供聯絡方式，我們會盡快回覆您的需求或提問。',
        'Company'=>'公司名稱',
        'Name'=>'聯絡人',

        'Tel'=>'電話',
        'Send'=>'送出訊息',
        'Address'=>'地址',
        'EMail'=>'電子郵件',

        'PlzCompany'=>'請輸入公司名稱',
        'PlzName'=>'請輸入聯絡人姓名',
        'PlzEmail'=>'請輸入電子郵件',
        'PlzEmailError'=>'請輸入電子郵件格式錯誤',
        'PlzTel'=>'請輸入電話號碼',
        'PlzSubject'=>'請輸入主旨',
        'PlzMessage'=>'請輸入訊息',
        'PlzCheckCode'=>'請輸入驗証碼',
        'CheckCodeError'=>'驗証碼錯誤',
        'AlertSuccess'=>'郵件已成功發送',
        'InquiryAlready'=>'此商品規格已放進詢價車',
        'WebSite'=>'公司網址',
        'Country'=>'國家',

        'CheckCode'=>'驗証碼',
        'PlzAmount2'=>'請正確輸入數字',
        'InquiryError'=>'詢價車內沒商品，請重新選擇',
        'Submit'=>'送出',
        'PlzCountry'=>'請輸入國家',
        'PlzAmount'=>'請輸入數量',
        'PlzAmountError'=>'數量格式錯誤，請重新檢查',
        'PlzErrorCode'=>'請輸入驗証碼',
        'CheckError'=>'驗証碼錯誤',
        'InquirySuccess'=>'詢價車已送出',
        ''=>'',
    ],
    /*
    |--------------------------------------------------------------------------
    | Web (Frontend) Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the system text in admin page.
    |
    */

    //

];