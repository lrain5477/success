<?php
/**
 * Edit page of model Inquiry
 *
 * @var \Minmax\Base\Models\Admin $adminData
 * @var \Minmax\Base\Models\AdminMenu $pageData
 * @var \App\Models\Inquiry $formData
 */
?>

@extends('MinmaxBase::admin.layouts.page.edit', ['formDataId' => $formData->id])

@section('action-buttons')
    @component('MinmaxBase::admin.layouts.right-links', ['languageActive' => $languageActive])
        @if($adminData->can('inquiryShow'))
        <a class="btn btn-sm btn-light" href="{{ langRoute("admin.{$pageData->uri}.index", ['filters' => 1]) }}" title="@lang('MinmaxBase::admin.form.back_list')">
            <i class="icon-undo2"></i><span class="ml-1 d-none d-md-inline-block">@lang('MinmaxBase::admin.form.back_list')</span>
        </a>
        @endif
    @endcomponent
@endsection

@section('forms')
    @inject('modelPresenter', 'App\Presenters\Admin\InquiryPresenter')

    <fieldset>
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.default')</legend>
        {!! $modelPresenter->getShowNormalText($formData, 'company') !!}
        {!! $modelPresenter->getShowNormalText($formData, 'website') !!}
        {!! $modelPresenter->getShowNormalText($formData, 'first_name') !!}
        {!! $modelPresenter->getShowNormalText($formData, 'last_name') !!}
        {!! $modelPresenter->getShowNormalText($formData, 'email') !!}
        {!! $modelPresenter->getShowNormalText($formData, 'tel') !!}
        {!! $modelPresenter->getShowNormalText($formData, 'fax') !!}
        {!! $modelPresenter->getShowNormalText($formData, 'address') !!}



    </fieldset>
    <fieldset id="baseFieldSet">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>商品資訊</legend>
        <div class="table-responsive">
            <table class="table table-sm table-bordered mb-1">
                <thead class="thead-default">
                <tr class="text-center table-info">
                    <th>圖片</th>
                    <th>名稱</th>
                    <th>規格</th>
                    <th>數量</th>

                </tr>
                </thead>
                <tbody>
                @php
                    $tmp = [];
                    if($formData->product_list != null) $tmp = json_decode($formData->product_list);
                    $list = [];
                    if(count($tmp) > 0) {
                        foreach ($tmp as $key => $value) {
                            $list[$key]['id'] = $value->productNo;
                            $productData =  \Minmax\Product\Models\ProductSet::where('id',$value->productNo)->first();
                            $list[$key]['productName'] = $productData ->title;
                            $list[$key]['pic'] = $productData->pic[0]['path'];
                            $list[$key]['spec'] = \Minmax\Base\Models\SiteParameterItem::where('id',$value->spec)->first()->label;
                            $list[$key]['amount'] = $value->amount;
                        }
                    }

                @endphp
                @if(count($list) > 0)
                    @foreach($list as $key => $value)
                        <tr class="text-center">
                            <td><a class="thumb " href="{!! asset($value['pic']) !!}" data-fancybox="" data-caption="產品名稱"><span class="imgFill imgLiquid_bgSize imgLiquid_ready" style="background-image: url({!! asset($value['pic']) !!}); background-size: cover; background-position: center center; background-repeat: no-repeat;"><img src="{!! asset($value['pic']) !!}" alt="產品名稱" style="display: none;"></span></a>
                            </td>
                            <td class="text-left"><a class="" href="/siteadmin/product-set/{!! $value['id'] !!}" target="_blank">{!! $value['productName'] !!}</a></td>
                            <td> {!! $value['spec']!!}</td>
                            <td> {!! $value['amount']!!}</td>
                        </tr>
                    @endforeach
                @endif

                </tbody>
            </table>
        </div>
    </fieldset>

    <fieldset class="mt-4">
        <legend class="legend h6 mb-4"><i class="icon-angle-double-down2 mr-2"></i>@lang('MinmaxBase::admin.form.fieldSet.advanced')</legend>
        {!! $modelPresenter->getFieldTextArea($formData, 'memo', ['required' => true]) !!}
        {!! $modelPresenter->getFieldRadio($formData, 'active', ['required' => true, 'inline' => true]) !!}

    </fieldset>

    <div class="text-center my-4 form-btn-group">
        <input class="btn btn-main" type="submit" id="submitBut" value="@lang('MinmaxBase::admin.form.button.send')" />
        <input class="btn btn-default" type="reset" value="@lang('MinmaxBase::admin.form.button.reset')" onclick="window.location.reload(true)" />
    </div>
@endsection
