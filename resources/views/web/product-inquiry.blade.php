@extends('web.site',['pageCssId' => 'product-inquiry'])
@section("product-inquiry-main")

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="{!! $banner->details['pic'][0]['path'] !!}">

        </div>
        <!-- breadcrumb area end -->

        <!-- cart main wrapper start -->
        <div class="cart-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">@lang('web.Menu.Inquiry')</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{!! $link !!}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('web.Menu.Inquiry')</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="section-bg-color">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Cart Table Area -->
                            <div class="cart-table table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="pro-thumbnail">@lang('web.Menu.ProductPic')</th>
                                            <th class="pro-title">@lang('web.Menu.ProductName')</th>
                                            <th class="pro-price">@lang('web.Menu.ProductSpec')</th>
                                            <th class="pro-quantity">@lang('web.Menu.ProductAmount')</th>

                                            <th class="pro-remove">@lang('web.Menu.Delete')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($data) >0)
                                            @foreach($data as $key => $value)
                                                <tr id="tr_{!! $key !!}">
                                                    <td class="pro-thumbnail"><a href="{!! $link !!}product-details/{!! $value['id'] !!}"><img class="img-fluid" src="{!! $value['pic'] !!}" alt="Product" /></a></td>
                                                    <td class="pro-title"><a href="{!! $link !!}product-details/{!! $value['id'] !!}">{!! $value['title'] !!}</a></td>
                                                    <td class="pro-price">{!! $value['spec'] !!}</td>
                                                    <td class="pro-quantity">
                                                        <input type="text" name="amount[]" data="{!! $key !!}" placeholder="1,00000">
                                                    </td>
                                                    <td class="pro-remove"><a href="javascript:;" onclick="deleteItem('{!! $key !!}')"><i class="fa fa-trash-o"></i></a></td>
                                                </tr>
                                            @endforeach
                                        @endif



                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8 mt-5">
                        <h2>@lang('web.Menu.InquiryForm')</h2>
                        <p class="brief mb-4">
                            @lang('web.Menu.InquiryTitle')
                        </p>
                        <form class="contact-form" id="mainForm" method="POST" target="hideFrame" action="{!! langRoute('web.send_inquriy') !!}">
{{--                            <div class="form-group">--}}
{{--                                <label for="Subject" class="must">Subject</label>--}}
{{--                                <input type="text" class="form-control" placeholder="Products Inquiry" value="Products Inquiry">--}}
{{--                            </div>--}}
{{--                            <div class="form-group">--}}
{{--                                <label for="exampleFormControlTextarea1" class="must">Leave Your Message</label>--}}
{{--                                <textarea></textarea>--}}
{{--                            </div>--}}
{{--                            <div class="form-group">--}}
{{--                                <label for="exampleFormControlTextarea1">Inquiry Items</label>--}}
{{--                                <textarea></textarea>--}}
{{--                            </div>--}}

{{--                            <div class="form-group mb-10">--}}
{{--                                <label for="exampleFormControlTextarea1" class="must">Other Requested Information</label>--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" value="">--}}
{{--                                    <label class="form-check-label" for="defaultCheck1">--}}
{{--                                        Price Quote--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" value="">--}}
{{--                                    <label class="form-check-label" for="defaultCheck1">--}}
{{--                                        Delivery Time--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" value="">--}}
{{--                                    <label class="form-check-label" for="defaultCheck1">--}}
{{--                                        Data Sheet / Brochure--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" value="">--}}
{{--                                    <label class="form-check-label" for="defaultCheck1">--}}
{{--                                        Minimum Order--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="form-group mb-10">--}}
{{--                                <label for="exampleFormControlFile1">Upload a File (Max:10mb)</label>--}}
{{--                                <input type="file" class="form-control-file" id="exampleFormControlFile1">--}}
{{--                            </div>--}}

                            <h2 class="mt-30">@lang('web.Menu.ContactInfo')</h2>
                            <div class="form-group">
                                <label for="Subject" class="must">@lang('web.Contact.Company')</label>
                                <input type="text"  name="company" id="company" required class="form-control" placeholder="">
                                @if(count($data) >0)
                                    @foreach($data as $key => $value)
                                        <input type="hidden" name="t_amount[]" id="amount_{!! $key !!}" value="">
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="Subject" class="">@lang('web.Contact.WebSite')</label>
                                <input type="text" name="website" id="website" class="form-control" placeholder="" >
                            </div>
                            <div class="form-group">
                                <label for="name" class="must">@lang('web.Contact.Country')</label>
                                @csrf
                                <input type="text" name="country" id="country" required class="form-control" placeholder="" >
                            </div>
{{--                            <div class="form-group">--}}
{{--                                <label for="name" class="must">Business Type</label>--}}
{{--                                <select class="form-control">--}}
{{--                                    <option value="1">Importer / Exporter</option>--}}
{{--                                    <option value="2">Distributor / Wholesaler</option>--}}
{{--                                    <option value="3">Trading Company</option>--}}
{{--                                    <option value="4">Agents</option>--}}
{{--                                    <option value="5">Buying Offices</option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
                            <div class="form-group">
                                <label for="name" class="must">@lang('web.Contact.Name')</label>
                                <div class="form-row">
                                    <div class="col">
                                        <input type="text" name="firstName" id="firstName" required class="form-control" placeholder="First name">
                                    </div>
                                    <div class="col">
                                        <input type="text" name="lastName" id="lastName" required class="form-control" placeholder="Last name">
                                    </div>
                                </div>
                            </div>
                            <div id="value_div" style="display: none;"></div>
                            <div class="form-group">
                                <label for="inputEmail4" class="must">Email</label>
                                <input type="email" class="form-control" name="email" id="email" required placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="tel" class="must">TEL</label>
                                <input type="text" class="form-control" name="tel" id="tel" required placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="fax">FAX</label>
                                <input type="text" class="form-control" name="fax" id="fax" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress">@lang('web.Contact.Address')</label>
                                <input type="text" class="form-control" name="address" id="inputAddress" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress">@lang('web.Contact.CheckCode')</label>
                                <input type="text" class="form-control" name="checkCode"  id="checkCode" required placeholder="">
                                <img id="brod-rand-img" style="cursor:pointer;" src="{{ langRoute('admin.captcha', ['name' => 'web_login']) }}">
                            </div>

                            <a href="javascript:;" onclick="checkForm()" class="btn btn-sqr mb-5">@lang('web.Contact.Submit')</a>
                            <!-- <button type="submit" class="btn btn-4 mb-30 mt-20">Submit</button> -->

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <iframe name="hideFrame" id="hideFrame" width="0" height="0" style="display:none"></iframe>
        <!-- cart main wrapper end -->
    </main>

 @endsection

    <!-- Bootstrap Form Helpers -->
@push('scripts')
    <script src="zh-Hant/js/bootstrap-formhelpers.min.js"></script>
    <script>
        function getVal() {
            // This will get the value of #timepicker
            alert($("#timepicker").val())
        }
        function checkNum(str){return str.match(/\D/)==null}
        function checkForm() {

            
            if($("#company").val() == "") {
                alert('@lang('web.Contact.PlzCompany')');
                return false;
            }

            if($("#firstName").val() == "" || $("#lastName").val() == "") {
                alert('@lang('web.Contact.PlzName')');
                return false;
            }

            if($("#country").val() == "") {
                alert('@lang('web.Contact.PlzCountry')');
                return false;
            }

            if($("#tel").val() == "") {
                alert('@lang('web.Contact.PlzTel')');
                return false;
            }

            if($("#email").val() == "") {
                alert('@lang('web.Contact.PlzEmail')');
                return false;
            }
            if(!IsEmail($("#email").val())) {
                alert('@lang('web.Contact.PlzEmailError')');
                return false;
            }

            if($("#checkCode").val() == "") {
                alert('@lang('web.Contact.PlzErrorCode')');
                return false;
            }
            $('input[name^="amount"]').each(function(i) {
                var val = $(this).val();

                $("#amount_"+i).val(val);


            });
            $("#mainForm").submit();



        }
        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(!regex.test(email)) {
                return false;
            }else{
                return true;
            }
        }
        $('#brod-rand-img,.brod-rand-img').click(function(){
            $('#brod-rand-img').attr('src', '{!! url("/") !!}/siteadmin/captcha/web_login' + '/' + (new Date()).getMilliseconds());
        });

        function deleteItem(id) {
            $.ajax({
                url: "{!! langRoute('web.ajaxForm') !!}",
                dataType:"json",
                headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                type:"POST",
                data:{
                    "id":id,
                    "type":"deleteItem"
                }
            }).done(function(msg){
                if(msg == true) {
                    $("#tr_"+id).remove();
                }
            });;
        }
    </script>
@endpush

