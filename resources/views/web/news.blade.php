@extends('web.site',['pageCssId' => 'news'])
@section("news-main")
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="{!! $banner->details['pic'][0]['path'] !!}">
        </div>
        <!-- breadcrumb area end -->

        <!-- blog main wrapper start -->
        <div class="blog-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">@lang('web.Menu.News')</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{!! $link !!}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('web.Menu.News')</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 order-1">
                        <div class="blog-item-wrapper">
                            <!-- blog item wrapper end -->
                            <div class="row mbn-30">
                                @if(count($data) >0)
                                    @foreach($data as $value)
                                        <div class="col-12">
                                            <!-- blog post item start -->
                                            <div class="blog-post-item blog-list-item">
                                                <div class="blog-content">
                                                    <span class="news-date">{!! $value->start_at !!}</span>
                                                    <div class="title">
                                                        <a href="{!! $link !!}news-details/{!! $value->id !!}">{!! $value->title !!}</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- blog post item end -->
                                        </div>
                                    @endforeach
                                @endif


                            </div>
                            <!-- blog item wrapper end -->

                            <!-- start pagination area -->
                            @include('web.page')
                            <!-- end pagination area -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- blog main wrapper end -->
    </main>

@endsection
@push('scripts')
    <script type="application/ld+json">
            {
                "@context": "https://schema.org",
                "@type": "Corporation",
            "name": "{!! $webData->company['name'] !!}",
            "description":"{{ $pageSeo['meta_description'] ?? '' }}",
                "url": "{!! url()->full() !!}",
            "logo": "{!!asset($webData->system_logo[0]['path']) !!}",
            "image": "{!! asset($webData->system_logo[0]['path']) !!}",
            "address": "{!! $webData->contact['address'] !!}",
            "faxNumber": "{!! $webData->contact['phone'] !!}",
                "contactPoint": {
                    "@type": "ContactPoint",
                    "telephone": "{!! $webData->contact['phone'] !!}",
                    "contactType": "customer service",
                    "areaServed": "TW",
                    "availableLanguage": "Chinese (Traditional)"
                },
                "sameAs": ""
            }
        </script>
    <script type="application/ld+json">
                {
                  "@context": "http://schema.org",
                  "@type": "BreadcrumbList",
                  "itemListElement":
                  [
                    {
                      "@type": "ListItem",
                      "position": 1,
                      "item":
                      {
                      "@id": "{!! url('/') !!}",
                      "name": "首頁"
                      }
                    },
                     {
                      "@type": "ListItem",
                      "position": 2,
                      "item":
                      {
                        "@id": "{!! url()->full() !!}",
                        "name": "@lang('web.Menu.News')"
                      }
                    }
                  ]
                }
            </script>
@endpush