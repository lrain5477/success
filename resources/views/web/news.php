<?php
?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <?php include_once './head.php'; ?>
</head>

<body>
    <!-- Start Header Area -->
    <?php include_once 'header.php'; ?>
    <!-- end Header Area -->


    <!-- offcanvas search form start -->
    <div class="offcanvas-search-wrapper">
        <div class="offcanvas-search-inner">
            <div class="offcanvas-close">
                <i class="fa fa-close"></i>
            </div>
            <div class="container">
                <div class="offcanvas-search-box">
                    <form class="d-flex bdr-bottom w-100">
                        <input type="text" placeholder="Search entire storage here...">
                        <button class="search-btn"><i class="fa fa-search"></i>search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="zh-Hant/img/banner/news.jpg">
        </div>
        <!-- breadcrumb area end -->

        <!-- blog main wrapper start -->
        <div class="blog-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">最新消息</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">最新消息</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 order-1">
                        <div class="blog-item-wrapper">
                            <!-- blog item wrapper end -->
                            <div class="row mbn-30">
                                <div class="col-12">
                                    <!-- blog post item start -->
                                    <div class="blog-post-item blog-list-item">
                                        <div class="blog-content">
                                            <span class="news-date">2019/06/29</span>
                                            <div class="title">
                                                <a href="news-details.php">認識矽酸鈣板</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- blog post item end -->
                                </div>
                                <div class="col-12">
                                    <!-- blog post item start -->
                                    <div class="blog-post-item blog-list-item">
                                        <div class="blog-content">
                                            <span class="news-date">2019/06/29</span>
                                            <div class="title">
                                                <a href="news-details.php">送愛到偏鄉關注資源弱勢的學童，讓小朋友們擁有完整的訓練，能夠安心無虞全力征戰！</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- blog post item end -->
                                </div>
                                <div class="col-12">
                                    <!-- blog post item start -->
                                    <div class="blog-post-item blog-list-item">
                                        <div class="blog-content">
                                            <span class="news-date">2019/06/29</span>
                                            <div class="title">
                                                <a href="news-details.php">送愛到偏鄉關注資源弱勢的學童，讓小朋友們擁有完整的訓練，能夠安心無虞全力征戰！</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- blog post item end -->
                                </div>
                                <div class="col-12">
                                    <!-- blog post item start -->
                                    <div class="blog-post-item blog-list-item">
                                        <div class="blog-content">
                                            <span class="news-date">2019/06/29</span>
                                            <div class="title">
                                                <a href="news-details.php">志工團捐助物資並協助南田部落弱勢家庭房屋修繕</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- blog post item end -->
                                </div>
                                <div class="col-12">
                                    <!-- blog post item start -->
                                    <div class="blog-post-item blog-list-item">
                                        <div class="blog-content">
                                            <span class="news-date">2019/06/29</span>
                                            <div class="title">
                                                <a href="news-details.php">邀集社會大眾一同前往中角沙珠灣淨灘</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- blog post item end -->
                                </div>
                                <div class="col-12">
                                    <!-- blog post item start -->
                                    <div class="blog-post-item blog-list-item">
                                        <div class="blog-content">
                                            <span class="news-date">2019/06/29</span>
                                            <div class="title">
                                                <a href="news-details.php">邀集社會大眾一同前往中角沙珠灣淨灘</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- blog post item end -->
                                </div>
                                <div class="col-12">
                                    <!-- blog post item start -->
                                    <div class="blog-post-item blog-list-item">
                                        <div class="blog-content">
                                            <span class="news-date">2019/06/29</span>
                                            <div class="title">
                                                <a href="news-details.php">邀集社會大眾一同前往中角沙珠灣淨灘</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- blog post item end -->
                                </div>
                                <div class="col-12">
                                    <!-- blog post item start -->
                                    <div class="blog-post-item blog-list-item">
                                        <div class="blog-content">
                                            <span class="news-date">2019/06/29</span>
                                            <div class="title">
                                                <a href="news-details.php">邀集社會大眾一同前往中角沙珠灣淨灘</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- blog post item end -->
                                </div>
                            </div>
                            <!-- blog item wrapper end -->

                            <!-- start pagination area -->
                            <div class="paginatoin-area shadow-bg text-center mb-5">
                                <ul class="pagination-box">
                                    <li><a class="previous" href="#"><i class="fa fa-angle-left"></i></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a class="next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                            <!-- end pagination area -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- blog main wrapper end -->
    </main>

    <!-- nquiry-wrapper start -->
    <?php include_once 'inquiry.php'; ?>
    <!-- nquiry-wrapper End -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->


    <!-- footer area start -->
    <?php include_once 'footer.php'; ?>
    <!-- footer area end -->



    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="zh-Hant/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="zh-Hant/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="zh-Hant/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="zh-Hant/js/vendor/bootstrap.min.js"></script>
    <!-- slick Slider JS -->
    <script src="zh-Hant/js/plugins/slick.min.js"></script>
    <!-- Countdown JS -->
    <script src="zh-Hant/js/plugins/countdown.min.js"></script>
    <!-- Nice Select JS -->
    <script src="zh-Hant/js/plugins/nice-select.min.js"></script>
    <!-- jquery UI JS -->
    <script src="zh-Hant/js/plugins/jqueryui.min.js"></script>
    <!-- Image zoom JS -->
    <script src="zh-Hant/js/plugins/image-zoom.min.js"></script>
    <!-- image loaded js -->
    <script src="zh-Hant/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- masonry  -->
    <script src="zh-Hant/js/plugins/masonry.pkgd.min.js"></script>
    <!-- mailchimp active js -->
    <script src="zh-Hant/js/plugins/ajaxchimp.js"></script>
    <!-- contact form dynamic js -->
    <script src="zh-Hant/js/plugins/ajax-mail.js"></script>
    <!-- google map api -->
    <script src="http://ditu.google.cn/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="zh-Hant/js/plugins/google-map.js"></script>
    <!-- Main JS -->
    <script src="zh-Hant/js/main.js"></script>
</body>

</html>