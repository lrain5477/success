<?php
?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <?php include_once './head.php'; ?>
</head>

<body>
    <!-- Start Header Area -->
    <?php include_once 'header.php'; ?>
    <!-- end Header Area -->


    <!-- offcanvas search form start -->
    <div class="offcanvas-search-wrapper">
        <div class="offcanvas-search-inner">
            <div class="offcanvas-close">
                <i class="fa fa-close"></i>
            </div>
            <div class="container">
                <div class="offcanvas-search-box">
                    <form class="d-flex bdr-bottom w-100">
                        <input type="text" placeholder="Search entire storage here...">
                        <button class="search-btn"><i class="fa fa-search"></i>search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="zh-Hant/img/banner/news.jpg">
        </div>
        <!-- breadcrumb area end -->

        <!-- blog main wrapper start -->
        <div class="blog-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">最新消息</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="news.php">最新消息</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">認識矽酸鈣板</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 mb-3">
                        <h3 class="text-center news-title">認識矽酸鈣板</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 order-2">
                        <div class="blog-item-wrapper">
                            <!-- blog post item start -->
                            <div class="blog-post-item blog-details-post d-block">

                                <div class="news-content w-100 pl-0">

                                    <!-- <h3><span class="news-type">媒體訊息</span></h3> -->

                                    <div class="entry-summary mb-5">
                                        <h5>了解POWER PLAY矽酸鈣板</h5>
                                        <p>通常從1830mm到3050mm)。石膏板是以專用紙包覆石膏而成，石膏是一種對環境、人體無害的物質。為增加板的強度及防火性，有些種類的石膏板會添加玻璃纖維及蛭石等。另有防潮型的石膏板，則於製造時添加防水劑。石膏板不是「石綿板」，正常的石膏板是不含石綿的。環球水泥公司的石膏板是在台灣製造，原料及製程都經嚴格控管，不僅不含石綿，也不含其他有毒物質。</p>
                                        <p>對環境友善，是環球水泥應盡的企業社會責任。我們從產品的研發、生產到剩料再利用，都致力於成為國內綠建材的最佳代表，且為達到＂reduce, recycle, reuse＂的三項指標的最高標準而不斷努力。節能、環保、愛地球已不是一個口號，環球水泥將落實企業社會責任，為永續發展盡力的企業社會公民。</p>
                                        <h5>產品特性:</h5>
                                        <p> 防火性符合耐燃材料標準，表面耐清洗耐污染，防水抗潮，保養簡單，兼具品質與安全，符合現代建築需求,給予您最經濟的產品，營造最美的空間。</p>
                                        <h5>四種石膏天花板 :</h5>
                                        <p>壓花系列：豹紋、滿天星、晶鑽紋。</p>
                                        <p>設計趨勢反映出當時的經濟狀況及人們的生活型態。」從 2000 年的無把手、純白色系與不銹鋼材質為主的極簡風、2005 年的橫向強烈木紋質感當道、2010 年盛行將直線設計運用在古典摩登風格上、2015 年灰色成為浴室的首選顏色，到 2018 年的天然石材席捲全球，演變為跨材質、多樣色彩的混搭新趨，反映現今消費者具備獨立思考的能力。</p>
                                        <p>構築一個符合心目中理想的衛浴空間，大至收納浴櫃、小至水龍頭、花灑等小細節都不容忽視，今年 睿信三輪 為了讓消費者能一站添足完整的選配方案，除了地板建材、系統廚具外，也陸續引進義大利衛浴品牌 AZZURRA 和義大利百年龍頭品牌 PALAZZANI，以多元豐富的選擇和客製化優勢，打造獨一無二的個人風格衛浴。 </p>
                                        <!-- <div class="blog-share-link">
                                            <h6>Share :</h6>
                                            <div class="blog-social-icon">
                                                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                                <a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
                                                <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <!-- blog post item end -->




                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- blog main wrapper end -->
    </main>

    <!-- nquiry-wrapper start -->
    <?php include_once 'inquiry.php'; ?>
    <!-- nquiry-wrapper End -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->


    <!-- footer area start -->
    <?php include_once 'footer.php'; ?>
    <!-- footer area end -->



    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="zh-Hant/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="zh-Hant/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="zh-Hant/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="zh-Hant/js/vendor/bootstrap.min.js"></script>
    <!-- slick Slider JS -->
    <script src="zh-Hant/js/plugins/slick.min.js"></script>
    <!-- Countdown JS -->
    <script src="zh-Hant/js/plugins/countdown.min.js"></script>
    <!-- Nice Select JS -->
    <script src="zh-Hant/js/plugins/nice-select.min.js"></script>
    <!-- jquery UI JS -->
    <script src="zh-Hant/js/plugins/jqueryui.min.js"></script>
    <!-- Image zoom JS -->
    <script src="zh-Hant/js/plugins/image-zoom.min.js"></script>
    <!-- image loaded js -->
    <script src="zh-Hant/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- masonry  -->
    <script src="zh-Hant/js/plugins/masonry.pkgd.min.js"></script>
    <!-- mailchimp active js -->
    <script src="zh-Hant/js/plugins/ajaxchimp.js"></script>
    <!-- contact form dynamic js -->
    <script src="zh-Hant/js/plugins/ajax-mail.js"></script>
    <!-- google map api -->
    <script src="http://ditu.google.cn/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="zh-Hant/js/plugins/google-map.js"></script>
    <!-- Main JS -->
    <script src="zh-Hant/js/main.js"></script>
</body>

</html>