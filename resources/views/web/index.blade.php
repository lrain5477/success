@extends('web.site',['pageCssId' => 'home'])
@section("home-main")

    <!-- slider area start -->
    <section class="hero-slider">
        <div class="hero-slider-active slick-arrow-style slick-arrow-style_hero slick-dot-style">
            @if($banner->details['pic'] != null  && count($banner->details['pic']) > 0)
                @foreach($banner->details['pic'] as $value)
                    <div class="hero-single-slide hero-overlay">
                        <div class="hero-slider-item hero-1 bg-img" data-bg="{!! $value['path'] !!}">
                            <!-- <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="hero-slider-content slide-1">
                                            <h1 class="slide-title">推薦產品</h1>
                                            <h2 class="slide-subtitle">矽酸鈣天花板是矽質、鈣質添加物及強韌耐熱纖維，經高溫、高壓蒸氣養　
                                                　生製造而成，100%不燃、無公害的安全環保建材。</h2>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                @endforeach
            @endif




        </div>


    </section>
    <!-- slider area end -->


    <article>
        <div id="muteYouTubeVideoPlayer" class="embed-container">
            {!! $banner->details['youtube'] !!}
        </div>
    </article>
    <main>
        <!-- service policy start -->
        <section class=" service-policy bg-gray mtn-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 mb-5">
                        {!! $banner ->details['topic'] !!}
                    </div>
                </div>
                <div class="row row-10 mt-5">
                   {!! $banner ->details['description'] !!}
                </div>
            </div>
        </section>
        <!-- service policy end -->
    </main>

    <!-- nquiry-wrapper start -->
    <div class="inquiry-wrapper">
        <a href="product-inquiry.php">
            <i class="fa fa-calendar-check-o"></i>
        </a>
    </div>
    <!-- nquiry-wrapper End -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->



    <!-- Quick view modal start -->
    <div class="modal" id="quick_view">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <!-- product details inner end -->
                    <div class="product-details-inner">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="product-large-slider">
                                    <div class="pro-large-img">
                                        <img src="zh-Hant/img/product/product-details-img1.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-large-img">
                                        <img src="zh-Hant/img/product/product-details-img2.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-large-img">
                                        <img src="zh-Hant/img/product/product-details-img3.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-large-img">
                                        <img src="zh-Hant/img/product/product-details-img4.jpg" alt="product-details" />
                                    </div>
                                </div>
                                <div class="pro-nav slick-row-10 slick-arrow-style">
                                    <div class="pro-nav-thumb">
                                        <img src="zh-Hant/img/product/product-details-img1.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-nav-thumb">
                                        <img src="zh-Hant/img/product/product-details-img2.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-nav-thumb">
                                        <img src="zh-Hant/img/product/product-details-img3.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-nav-thumb">
                                        <img src="zh-Hant/img/product/product-details-img4.jpg" alt="product-details" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="product-details-des">
                                    <h3 class="product-name">Premium Mens Sports Lather Keds</h3>
                                    <div class="ratings d-flex">
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <div class="pro-review">
                                            <span>1 Reviews</span>
                                        </div>
                                    </div>
                                    <div class="price-box">
                                        <span class="price-old"><del>$90.00</del></span>
                                        <span class="price-regular">$70.00</span>
                                    </div>
                                    <h5 class="offer-text"><strong>Hurry up</strong>! offer ends in:</h5>
                                    <div class="product-countdown" data-countdown="2019/09/20"></div>
                                    <p class="pro-desc">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                                        diam nonumy
                                        eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</p>
                                    <div class="quantity-cart-box d-flex align-items-center">
                                        <h6 class="option-title">qty:</h6>
                                        <div class="quantity">
                                            <div class="pro-qty"><input type="text" value="1"></div>
                                        </div>
                                        <div class="action_link">
                                            <a class="btn btn-cart2" href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                    <div class="useful-links">
                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-refresh"></i>compare</a>
                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i>wishlist</a>
                                    </div>
                                    <div class="like-icon">
                                        <a class="facebook" href="#"><i class="fa fa-facebook"></i>like</a>
                                        <a class="twitter" href="#"><i class="fa fa-twitter"></i>tweet</a>
                                        <a class="pinterest" href="#"><i class="fa fa-pinterest"></i>save</a>
                                        <a class="google" href="#"><i class="fa fa-google-plus"></i>share</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- product details inner end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Quick view modal end -->
@endsection
@push('styles')
    <style>
        .mobile-main-header {
            display: block;
            margin: auto;
            margin-top: 9vh;
            text-align: center;
            font-size: 45px;
        }

        .mobile-main-header .mobile-menu-toggler {
            width: 100%;
        }

        .mobile-logo {
            max-width: 18rem;
            margin: 0 auto;
        }
    </style>
@endpush
    @push('scripts')
        <script src="https://www.youtube.com/iframe_api"></script>


        <!-- 麵包屑結構 需動態產生 -->
        <script type="application/ld+json">
            {
              "@context": "http://schema.org",
              "@type": "BreadcrumbList",
              "itemListElement":
              [
                {
                  "@type": "ListItem",
                  "position": 1,
                  "item":
                  {
                  "@id": "{!! url('/') !!}",
                  "name": "首頁"
                  }
                },

              ]
            }
            </script>
    @endpush