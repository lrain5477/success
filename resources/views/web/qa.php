<?php
?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <?php include_once './head.php'; ?>
</head>

<body>
    <!-- Start Header Area -->
    <?php include_once 'header.php'; ?>
    <!-- end Header Area -->


    <!-- offcanvas search form start -->
    <div class="offcanvas-search-wrapper">
        <div class="offcanvas-search-inner">
            <div class="offcanvas-close">
                <i class="fa fa-close"></i>
            </div>
            <div class="container">
                <div class="offcanvas-search-box">
                    <form class="d-flex bdr-bottom w-100">
                        <input type="text" placeholder="Search entire storage here...">
                        <button class="search-btn"><i class="fa fa-search"></i>search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="zh-Hant/img/banner/shop.jpg">
        </div>
        <!-- breadcrumb area end -->

        <!-- checkout main wrapper start -->
        <div class="checkout-page-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">Q&amp;A</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active"><a href="qa.php">Q&amp;A</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <!-- Checkout Login Coupon Accordion Start -->
                        <div class="checkoutaccordion" id="checkOutAccordion">
                            <div class="card">
                                <h6><span data-toggle="collapse" data-target="#q1"><b>Q1.</b>天花板最時何用矽酸鈣板?</span></h6>
                                <div id="q1" class="collapse" data-parent="#q1">
                                    <div class="card-body">
                                        <p>底材在裝修完成後就被包覆看不見，因此在估價及施工時都要確實把關。矽酸鈣板常用木作天花板及木隔間，其中平頂天花板為將天花板拉平封板後，

                                            不再做特別的造型，是較為簡易的木作天花板。造型天花板：多為配合間接照明而設計的天花板，設計造型多元且富變化性，可視設計師與屋主的喜好規劃。
                                        </p>

                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <h6><span data-toggle="collapse" data-target="#couponaccordion"><b>Q2.</b>2分矽酸鈣版和3分矽酸鈣版的差別</span></h6>
                                <div id="couponaccordion" class="collapse" data-parent="#checkOutAccordion">
                                    <div class="card-body">
                                        <p>2分應該是6M/M的、3分是9M/M的，如果真的是矽酸鈣板(非氧化鎂板)，其實都可以，因為6和9M/M，主要差在前者是證明耐然一級，後者是防火一小時，如果住家要用的都可以，另外小秘訣，你應該不是要施工輕鋼架吧，如果是一般骨架型的，最好裡面多層約7M/M合板，日後要鎖或釘螺絲和鐵丁時才容易鎖緊。</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <h6><span data-toggle="collapse" data-target="#q3"><b>Q3.</b>矽酸鈣板厚度標準是多少</span></h6>
                                <div id="q3" class="collapse" data-parent="#q3">
                                    <div class="card-body">
                                        <p>矽酸鈣板作為新型綠色環保建材，除具有傳統石膏板的功能外，更具有優越防火性能及耐潮、使用壽命超長的優點，大量應用於工商業工程建築的吊頂天花和隔牆，家庭裝修、家具的襯板、廣告牌的襯板、船舶的隔倉板、倉庫的棚板、網絡地板以及隧道等室內工程的壁板。矽酸鈣板是一種是高複合板，主要由天然石膏粉、白水泥、膠水、玻璃纖維符合而成，具有防火、防潮、隔音、隔熱等性能，是天花吊頂的常用材料，也是隔牆板的主要材料，所以在家裝中經常能用到這樣的一個板材。

                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <h6><span data-toggle="collapse" data-target="#q4"><b>Q4.</b>關於各式的材室內裝修運用</span></h6>
                                <div id="q4" class="collapse" data-parent="#q4">
                                    <div class="card-body">
                                        <p>矽酸鈣板是一種以性能穩定而著稱的新型建築板材，最早由美國OCDG公司發表，20世紀(70年代)起被廣為推廣使用。日本和美國是使用最普遍的國家。矽酸鈣板經過30多年的 市場歷練，已被證實是一種耐久耐燃穩定的建材。

                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <h6><span data-toggle="collapse" data-target="#q5"><b>Q5.</b>矽酸鈣板之特性:</span></h6>
                                <div id="q5" class="collapse" data-parent="#q5">
                                    <div class="card-body">
                                        <p>100%英國技術: 世界級矽酸鈣板大廠，榮獲英國女皇技術及出口績優獎，百年防火建材製造經驗,技術領先群倫，CAPE公司在台投資設廠生產。</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- Checkout Login Coupon Accordion End -->
                    </div>
                </div>

            </div>
        </div>
        <!-- checkout main wrapper end -->
    </main>

    <!-- nquiry-wrapper start -->
    <?php include_once 'inquiry.php'; ?>
    <!-- nquiry-wrapper End -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->


    <!-- footer area start -->
    <?php include_once 'footer.php'; ?>
    <!-- footer area end -->



    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="zh-Hant/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="zh-Hant/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="zh-Hant/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="zh-Hant/js/vendor/bootstrap.min.js"></script>
    <!-- slick Slider JS -->
    <script src="zh-Hant/js/plugins/slick.min.js"></script>
    <!-- Countdown JS -->
    <script src="zh-Hant/js/plugins/countdown.min.js"></script>
    <!-- Nice Select JS -->
    <script src="zh-Hant/js/plugins/nice-select.min.js"></script>
    <!-- jquery UI JS -->
    <script src="zh-Hant/js/plugins/jqueryui.min.js"></script>
    <!-- Image zoom JS -->
    <script src="zh-Hant/js/plugins/image-zoom.min.js"></script>
    <!-- image loaded js -->
    <script src="zh-Hant/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- masonry  -->
    <script src="zh-Hant/js/plugins/masonry.pkgd.min.js"></script>
    <!-- mailchimp active js -->
    <script src="zh-Hant/js/plugins/ajaxchimp.js"></script>
    <!-- contact form dynamic js -->
    <script src="zh-Hant/js/plugins/ajax-mail.js"></script>
    <!-- google map api -->
    <script src="http://ditu.google.cn/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="zh-Hant/js/plugins/google-map.js"></script>
    <!-- Main JS -->
    <script src="zh-Hant/js/main.js"></script>
</body>

</html>