<?php
?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>首頁 - 繼威實業</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="/static/{{$lang}}/img/favicon.ico">

    <!-- CSS
        ============================================ -->
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,600,700,800,900%7CPoppins:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/static/{{$lang}}/css/vendor/bootstrap.min.css">
    <!-- Font-awesome CSS -->
    <link rel="stylesheet" href="/static/{{$lang}}/css/vendor/font-awesome.min.css">

    <link rel="stylesheet" href="/static/{{$lang}}/css/vendor/bootstrap-formhelpers.min.css">
    <!-- Slick slider css -->
    <link rel="stylesheet" href="/static/{{$lang}}/css/plugins/slick.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="/static/{{$lang}}/css/plugins/animate.css">
    <!-- Nice Select css -->
    <link rel="stylesheet" href="/static/{{$lang}}/css/plugins/nice-select.css">
    <!-- jquery UI css -->
    <link rel="stylesheet" href="/static/{{$lang}}/css/plugins/jqueryui.min.css">
    <link rel="stylesheet" href="/static/{{$lang}}/css/jquery.exzoom.css">
    <!-- main style css -->
    <link rel="stylesheet" href="/static/{{$lang}}/css/style.css">
    <style>
        .mobile-main-header {
            display: block;
            margin: auto;
            margin-top: 9vh;
            text-align: center;
            font-size: 45px;
        }

        .mobile-main-header .mobile-menu-toggler {
            width: 100%;
        }

        .mobile-logo {
            max-width: 18rem;
            margin: 0 auto;
        }
    </style>
</head>

<body>
    <?php include_once 'header2.php'; ?>


    <!-- offcanvas search form start -->
    <div class="offcanvas-search-wrapper">
        <div class="offcanvas-search-inner">
            <div class="offcanvas-close">
                <i class="fa fa-close"></i>
            </div>
            <div class="container">
                <div class="offcanvas-search-box">
                    <form class="d-flex bdr-bottom w-100">
                        <input type="text" placeholder="Search entire storage here...">
                        <button class="search-btn"><i class="fa fa-search"></i>search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas search form end -->

    <!-- slider area start -->
    <section class="hero-slider">
        <div class="hero-slider-active slick-arrow-style slick-arrow-style_hero slick-dot-style">
            <div class="hero-single-slide hero-overlay">
                <div class="hero-slider-item hero-1 bg-img" data-bg="zh-Hant/img/slider/home1-slide2.jpg">
                    <!-- <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="hero-slider-content slide-1">
                                    <h1 class="slide-title">推薦產品</h1>
                                    <h2 class="slide-subtitle">矽酸鈣天花板是矽質、鈣質添加物及強韌耐熱纖維，經高溫、高壓蒸氣養　
                                        　生製造而成，100%不燃、無公害的安全環保建材。</h2>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>

            <div class="hero-single-slide hero-overlay">
                <div class="hero-slider-item hero-1 bg-img" data-bg="zh-Hant/img/slider/home2-slide1.jpg">
                    <!-- <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="hero-slider-content slide-2">
                                    <h1 class="slide-title">推薦產品</h1>
                                    <h2 class="slide-subtitle">矽酸鈣天花板是矽質、鈣質添加物及強韌耐熱纖維，經高溫、高壓蒸氣養　
                                        　生製造而成，100%不燃、無公害的安全環保建材。</h2>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>

        </div>


    </section>
    <!-- slider area end -->


    <article>
        <div id="muteYouTubeVideoPlayer" class="embed-container">
            <iframe style="position: absolute;
    z-index: 0;
    width: 2300px;
    height: 1222px;
    top: 0px;
    left: 0px;
    overflow: hidden;
    opacity: 1;
    user-select: none;
    margin-top: 0px;
    margin-left: -180px;
    max-width: initial;
    transition-property: opacity;
    transition-duration: 1000ms;" src="https://www.youtube.com/embed/P2d4dlzbUdM?autoplay=1&amp;loop=1&amp;controls=0&amp;mute=1&amp;playlist=P2d4dlzbUdM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" class="video"></iframe>
        </div>
    </article>
    <main>
        <!-- service policy start -->
        <section class=" service-policy bg-gray mtn-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 mb-5">
                        <h1 class="slide-title">領導品牌</h1>
                        <h2 class="slide-subtitle">矽酸鈣天花板是矽質、鈣質添加物及強韌耐熱纖維，100%不燃、無公害的安全環保建材。</h2>
                        <span class="btn-more mt-5"><a href="about-us.php">了解更多</a></span>
                    </div>
                </div>
                <div class="row row-10 mt-5">
                    <div class="col-lg-4 col-sm-6" style="z-index: 9999;">
                        <div class="policy-block text-center">
                            <div class="policy-icon">
                                <i class="fa fa-cogs"></i>
                            </div>
                            <div class="policy-text">
                                <h4 class="policy-title">專業加工</h4>
                                <p>維護良好的環境與建設<br>
                                    是我們對於這一代美好生活的職責所在</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6" style="z-index: 9999;">
                        <div class="policy-block text-center">
                            <div class="policy-icon">
                                <i class="fa fa-globe"></i>
                            </div>
                            <div class="policy-text">
                                <h4 class="policy-title">領導品牌</h4>
                                <p>嚴謹的要求精良的建設品質<br>
                                    建造一貫的根基安全建設為標準</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6" style="z-index: 9999;">
                        <div class="policy-block text-center">
                            <div class="policy-icon">
                                <i class="fa fa-envira"></i>
                            </div>
                            <div class="policy-text">
                                <h4 class="policy-title">環保建材</h4>
                                <p>建立出穩固的建築技術<br>
                                    讓每個細節都能發揮出極致的結構力學</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- service policy end -->
    </main>

    <!-- nquiry-wrapper start -->
    <div class="inquiry-wrapper">
        <a href="product-inquiry.php">
            <i class="fa fa-calendar-check-o"></i>
        </a>
    </div>
    <!-- nquiry-wrapper End -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->



    <!-- Quick view modal start -->
    <div class="modal" id="quick_view">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <!-- product details inner end -->
                    <div class="product-details-inner">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="product-large-slider">
                                    <div class="pro-large-img">
                                        <img src="zh-Hant/img/product/product-details-img1.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-large-img">
                                        <img src="zh-Hant/img/product/product-details-img2.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-large-img">
                                        <img src="zh-Hant/img/product/product-details-img3.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-large-img">
                                        <img src="zh-Hant/img/product/product-details-img4.jpg" alt="product-details" />
                                    </div>
                                </div>
                                <div class="pro-nav slick-row-10 slick-arrow-style">
                                    <div class="pro-nav-thumb">
                                        <img src="zh-Hant/img/product/product-details-img1.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-nav-thumb">
                                        <img src="zh-Hant/img/product/product-details-img2.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-nav-thumb">
                                        <img src="zh-Hant/img/product/product-details-img3.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-nav-thumb">
                                        <img src="zh-Hant/img/product/product-details-img4.jpg" alt="product-details" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="product-details-des">
                                    <h3 class="product-name">Premium Mens Sports Lather Keds</h3>
                                    <div class="ratings d-flex">
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <div class="pro-review">
                                            <span>1 Reviews</span>
                                        </div>
                                    </div>
                                    <div class="price-box">
                                        <span class="price-old"><del>$90.00</del></span>
                                        <span class="price-regular">$70.00</span>
                                    </div>
                                    <h5 class="offer-text"><strong>Hurry up</strong>! offer ends in:</h5>
                                    <div class="product-countdown" data-countdown="2019/09/20"></div>
                                    <p class="pro-desc">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                                        diam nonumy
                                        eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</p>
                                    <div class="quantity-cart-box d-flex align-items-center">
                                        <h6 class="option-title">qty:</h6>
                                        <div class="quantity">
                                            <div class="pro-qty"><input type="text" value="1"></div>
                                        </div>
                                        <div class="action_link">
                                            <a class="btn btn-cart2" href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                    <div class="useful-links">
                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-refresh"></i>compare</a>
                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i>wishlist</a>
                                    </div>
                                    <div class="like-icon">
                                        <a class="facebook" href="#"><i class="fa fa-facebook"></i>like</a>
                                        <a class="twitter" href="#"><i class="fa fa-twitter"></i>tweet</a>
                                        <a class="pinterest" href="#"><i class="fa fa-pinterest"></i>save</a>
                                        <a class="google" href="#"><i class="fa fa-google-plus"></i>share</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- product details inner end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Quick view modal end -->

    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/vendor/bootstrap.min.js"></script>
    <!-- slick Slider JS -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/plugins/slick.min.js"></script>
    <!-- Countdown JS -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/plugins/countdown.min.js"></script>
    <!-- Nice Select JS -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/plugins/nice-select.min.js"></script>
    <!-- jquery UI JS -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/plugins/jqueryui.min.js"></script>
    <!-- Image zoom JS -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/plugins/image-zoom.min.js"></script>
    <!-- image loaded js -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- masonry  -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/plugins/masonry.pkgd.min.js"></script>
    <!-- mailchimp active js -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/plugins/ajaxchimp.js"></script>
    <!-- contact form dynamic js -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/plugins/ajax-mail.js"></script>
    <!-- google map api -->
    <script src="http://ditu.google.cn/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/plugins/google-map.js"></script>
    <!-- Main JS -->
    <script src="/static/{!!$lang!!}/zh-Hant/js/main.js"></script>

    <script src="https://www.youtube.com/iframe_api"></script>




</body>

</html>