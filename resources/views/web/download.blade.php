@extends('web.site',['pageCssId' => 'download'])
@section("download-main")

        <main>
            <!-- breadcrumb area start -->
            <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="{!! $banner->details['pic'][0]['path'] !!}">
                <!-- <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb-wrap">
                                <nav aria-label="breadcrumb">

                                </nav>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
            <!-- breadcrumb area end -->

            <!-- wishlist main wrapper start -->
            <div class="wishlist-main-wrapper section-padding">
                <div class="container">
                    <!-- Wishlist Page Content Start -->
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb-wrap">
                                <nav aria-label="breadcrumb">
                                    <h3 class="breadcrumb-title">@lang('web.Menu.Download')</h3>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{!! $link !!}"><i class="fa fa-home"></i></a></li>
                                        <li class="breadcrumb-item active"><a href="{!! $link !!}download">@lang('web.Menu.Download')</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="download-list row">
                                @if($data->count()>0)
                                    @foreach($data as $value)
                                        <div class="col-md-3 col-sm-6">
                                            <div class="download-img"><a href="{!! $value->file[0] !!}" download><i class="fa fa-download"></i></a></div>
                                            <div class="download-name"><a href="{!! $value->file[0] !!}" download>@lang('web.Menu.Catalogue')</a></div>
                                        </div>
                                    @endforeach
                                @endif


                            </div>
                        </div>
                    </div>

                    <!-- Wishlist Page Content End -->
                </div>
            </div>
            <!-- wishlist main wrapper end -->
        </main>
@endsection