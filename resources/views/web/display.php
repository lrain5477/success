<?php
?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <?php include_once './head.php'; ?>
</head>

<body>
    <!-- Start Header Area -->
    <?php include_once 'header.php'; ?>
    <!-- end Header Area -->


    <!-- offcanvas search form start -->
    <div class="offcanvas-search-wrapper">
        <div class="offcanvas-search-inner">
            <div class="offcanvas-close">
                <i class="fa fa-close"></i>
            </div>
            <div class="container">
                <div class="offcanvas-search-box">
                    <form class="d-flex bdr-bottom w-100">
                        <input type="text" placeholder="Search entire storage here...">
                        <button class="search-btn"><i class="fa fa-search"></i>search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="zh-Hant/img/banner/display.jpg">
        </div>
        <!-- breadcrumb area end -->

        <!-- blog main wrapper start -->
        <div class="blog-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">空間展示</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">空間展示-矽酸鈣天花板</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 order-1 order-lg-1">
                        <aside class="sidebar-wrapper">
                            <!-- single sidebar start -->
                            <div class="sidebar-single">
                                <h6 class="sidebar-title">商品分類</h6>
                                <div class="s-side">
                                    <ul>
                                        <li>
                                            <div class="d-firstNav s-firstNav">
                                                <i class="fa fa-arrow-circle-right mr-1"></i>
                                                <span>沖孔系列</span>
                                            </div>
                                            <ul class="d-firstDrop s-firstDrop">
                                                <li>
                                                    <div class="d-secondNav s-secondNav">
                                                        <a href="">
                                                            <i class="icon-circle"></i>
                                                            <span>矽酸鈣天花板</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="d-secondNav s-secondNav">
                                                        <a href="">
                                                            <i class="icon-circle"></i>
                                                            <span>矽酸鈣天花板</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="d-secondNav s-secondNav">
                                                        <a href="">
                                                            <i class="icon-circle"></i>
                                                            <span>矽酸鈣天花板</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <div class="d-firstNav s-firstNav">
                                                <i class="fa fa-arrow-circle-right mr-1"></i>
                                                <span>立體花紋系列</span>
                                            </div>
                                            <ul class="d-firstDrop s-firstDrop">
                                                <li>
                                                    <div class="d-secondNav s-secondNav">
                                                        <a href="">
                                                            <i class="icon-circle"></i>
                                                            <span>矽酸鈣天花板</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="d-secondNav s-secondNav">
                                                        <a href="">
                                                            <i class="icon-circle"></i>
                                                            <span>矽酸鈣天花板</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <div class="d-firstNav s-firstNav">
                                                <i class="fa fa-image mr-1"></i>
                                                <span><a href="display.php">空間展示</a></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <!-- single sidebar end -->

                        </aside>
                    </div>
                    <div class="col-lg-9 order-2 order-lg-2">
                        <div class="blog-item-wrapper">
                            <!-- blog item wrapper end -->
                            <div class="row mbn-30">
                                <div class="col-md-12">
                                    <!-- blog post item start -->
                                    <div class="blog-post-item d-block mb-10">
                                        <div class="gallery-wrapper w-100">
                                            <a class="galpop-single" href="zh-Hant/img/images/gallery/large/blog-1.jpg">
                                                <img src="zh-Hant/img/images/gallery/thumbs/blog-1.jpg" class="img-thumbnail" alt="An apocalyptic Earth." />
                                            </a>
                                        </div>
                                        <!-- <div class="display-content w-100 pl-0 mt-10">
                                            <h6 class="blog-title">
                                                <a href="blog-details.html">矽酸鈣天花板</a>
                                            </h6>
                                        </div> -->
                                    </div>
                                    <!-- blog post item end -->
                                </div>
                                <div class="col-md-12">
                                    <div class="blog-post-item d-block mb-10">
                                        <div class="gallery-wrapper w-100">
                                            <a class="galpop-single" href="zh-Hant/img/images/gallery/large/blog-1.jpg">
                                                <img src="zh-Hant/img/images/gallery/thumbs/blog-1.jpg" class="img-thumbnail" alt="An apocalyptic Earth." />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="blog-post-item d-block mb-10">
                                        <div class="gallery-wrapper w-100">
                                            <a class="galpop-single" href="zh-Hant/img/images/gallery/large/blog-1.jpg">
                                                <img src="zh-Hant/img/images/gallery/thumbs/blog-1.jpg" class="img-thumbnail" alt="An apocalyptic Earth." />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="blog-post-item d-block mb-10">
                                        <div class="gallery-wrapper w-100">
                                            <a class="galpop-single" href="zh-Hant/img/images/gallery/large/blog-1.jpg">
                                                <img src="zh-Hant/img/images/gallery/thumbs/blog-1.jpg" class="img-thumbnail" alt="An apocalyptic Earth." />
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- blog item wrapper end -->

                            <!-- start pagination area -->
                            <div class="paginatoin-area shadow-bg text-center mb-5">
                                <ul class="pagination-box">
                                    <li><a class="previous" href="#"><i class="fa fa-angle-left"></i></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a class="next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                            <!-- end pagination area -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- blog main wrapper end -->
    </main>

    <!-- nquiry-wrapper start -->
    <?php include_once 'inquiry.php'; ?>
    <!-- nquiry-wrapper End -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->


    <!-- footer area start -->
    <?php include_once 'footer.php'; ?>
    <!-- footer area end -->



    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="zh-Hant/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="zh-Hant/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="zh-Hant/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="zh-Hant/js/vendor/bootstrap.min.js"></script>
    <!-- slick Slider JS -->
    <script src="zh-Hant/js/plugins/slick.min.js"></script>
    <!-- Countdown JS -->
    <script src="zh-Hant/js/plugins/countdown.min.js"></script>
    <!-- Nice Select JS -->
    <script src="zh-Hant/js/plugins/nice-select.min.js"></script>
    <!-- jquery UI JS -->
    <script src="zh-Hant/js/plugins/jqueryui.min.js"></script>
    <!-- Image zoom JS -->
    <script src="zh-Hant/js/plugins/image-zoom.min.js"></script>
    <!-- image loaded js -->
    <script src="zh-Hant/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- masonry  -->
    <script src="zh-Hant/js/plugins/masonry.pkgd.min.js"></script>
    <!-- mailchimp active js -->
    <script src="zh-Hant/js/plugins/ajaxchimp.js"></script>
    <!-- contact form dynamic js -->
    <script src="zh-Hant/js/plugins/ajax-mail.js"></script>
    <!-- google map api -->
    <script src="http://ditu.google.cn/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="zh-Hant/js/plugins/google-map.js"></script>
    <!-- google map active js -->
    <script src="zh-Hant/js/plugins/jquery.galpop.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.galpop-single').galpop();

            $('.galpop-multiple').galpop();

            $('.galpop-info').galpop();


            var callback = function() {
                var wrapper = $('#galpop-wrapper');
                var info = $('#galpop-info');
                var count = wrapper.data('count');
                var index = wrapper.data('index');
                var current = index + 1;
                var string = 'Image ' + current + ' of ' + count;

                info.append('<p>' + string + '</p>').fadeIn();

            };
            $('.galpop-callback').galpop({
                callback: callback
            });

            $('.manual-open').change(function(e) {
                var image = $(this).val();
                if (image) {
                    var settings = {};
                    $.fn.galpop('openBox', settings, image);
                }
            });

            $('.manual-open-group').change(function(e) {
                var v = $(this).val();
                var images = [
                    'images/gallery/large/apocalypse.jpg',
                    'images/gallery/large/vintage.jpg',
                    'images/gallery/large/magicLake.jpg',
                    'images/gallery/large/underwater.jpg',
                    'images/gallery/large/goodBoy.jpg',
                    'images/gallery/large/darkroad.jpg',
                    'images/gallery/large/roadkill.jpg',
                    'images/gallery/large/wolfMarine.jpg',
                    'images/gallery/large/alice.jpg',
                    'images/gallery/large/reflection.jpg',
                ];
                var settings = {};
                $.fn.galpop('openBox', settings, images, v);
            });

            $('.click-open-iframe').galpop({
                contentType: 'iframe',
            });

            $('.click-open-ajax').galpop({
                contentType: 'AJAX',
            });
        });
    </script>
    <!-- Main JS -->
    <script src="zh-Hant/js/main.js"></script>
</body>