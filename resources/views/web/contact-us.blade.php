@extends('web.site',['pageCssId' => 'contact'])
@section("contact-main")

    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="{!! $banner->details['pic'][0]['path'] !!}">
        </div>
        <!-- breadcrumb area end -->

        <!-- contact area start -->
        <div class="contact-area section-padding mb-5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">@lang('web.Menu.AboutUs')</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{!! $link !!}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('web.Menu.AboutUs')</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="contact-message">
                            <h4 class="contact-title">@lang('web.Contact.Info')</h4>
                            <p>@lang('web.Contact.Title1')</p>
                            <form id="contact-form" action="{!! $link !!}send_mail" target="hidFrame"  method="post" class="contact-form">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="company" placeholder="@lang('web.Contact.Company') *" id="company" type="text" required>
                                        @csrf
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="name" placeholder="@lang('web.Contact.Name') *" id="name" type="text" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="phone" placeholder="@lang('web.Contact.Tel') *"  id="phone" type="text" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="email" placeholder="Email *" id="email" type="text"  required>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <input name="subject" placeholder="Subject *" type="text" id="subject">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <input name="checkCode" placeholder="checkCode *" type="text" id="checkCode">
                                        <img id="brod-rand-img" style="cursor:pointer;" src="{{ langRoute('admin.captcha', ['name' => 'login']) }}">

                                    </div>
                                    <div class="col-12">
                                        <div class="contact2-textarea text-center">
                                            <textarea placeholder="Message *" name="message" id="message" class="form-control2" required=""></textarea>
                                        </div>
                                        <div class="contact-btn">
                                            <a class="btn btn-sqr" type="submit" herf="javascript:;" onclick="checkForm()">@lang('web.Contact.Send')</a>
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex justify-content-center">
                                        <p class="form-messege"></p>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="contact-info">
                            <h4 class="contact-title">@lang('web.Menu.AboutUs')</h4>

                            <ul>
                                <li><i class="fa fa-map-marker"></i>@lang('web.Contact.Address')：{!! $webData->contact['address'] !!}</li>
                                <li><i class="fa fa-envelope-o"></i>@lang('web.Contact.EMail')： {!! $webData ->contact['email'] !!}</li>
                                <li><i class="fa fa-phone"></i>@lang('web.Contact.Tel')：  {!! $webData ->contact['phone'] !!}</li>
                            </ul>
                            <div class="working-time">
                              {!! $webData ->contact['workTime'] ?? '' !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- contact area end -->

        <!-- google map start -->
        <!-- <div class="map-wrap">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3643.803549636532!2d120.4332621156015!3d24.03799178364436!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34694f533f36f661%3A0xb0bb62a6eadd4696!2zNTA25b2w5YyW57ij56aP6IiI6YSJ56aP5bu66LevNjHomZ8!5e0!3m2!1szh-TW!2stw!4v1561907124715!5m2!1szh-TW!2stw" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div> -->
        <!-- google map end -->
    </main>
    <iframe name="hidFrame" id="hidFrame"  width="0" height="0" style="display:none"></iframe>
 @endsection
@push('scripts')
    <script>
        function checkForm() {
            if($("#company").val() == "") {
                alert('@lang('web.Contact.PlzCompany')');
                return false;
            }

            if($("#name").val() == "") {
                alert('@lang('web.Contact.PlzName')');
                return false;
            }

            if($("#company").val() == "") {
                alert('@lang('web.Contact.PlzCompany')');
                return false;
            }

            if($("#phone").val() == "") {
                alert('@lang('web.Contact.PlzTel')');
                return false;
            }

            if($("#email").val() == "") {
                alert('@lang('web.Contact.PlzEmail')');
                return false;
            }
            if(!IsEmail($("#email").val())) {
                alert('@lang('web.Contact.PlzEmailError')');
                return false;
            }

            if($("#checkCode").val() == "") {
                alert('@lang('web.Contact.PlzEmail')');
                return false;
            }
            $("#contact-form").submit();



        }
        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(!regex.test(email)) {
                return false;
            }else{
                return true;
            }
        }
        $('#brod-rand-img,.brod-rand-img').click(function(){
            $('#brod-rand-img').attr('src', '{!! url("/") !!}/siteadmin/captcha/login' + '/' + (new Date()).getMilliseconds());
        });
    </script>
@endpush