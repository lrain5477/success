<?php
?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <?php include_once './head.php'; ?>
</head>

<body>
    <!-- Start Header Area -->
    <?php include_once 'header.php'; ?>
    <!-- end Header Area -->


    <!-- offcanvas search form start -->
    <div class="offcanvas-search-wrapper">
        <div class="offcanvas-search-inner">
            <div class="offcanvas-close">
                <i class="fa fa-close"></i>
            </div>
            <div class="container">
                <div class="offcanvas-search-box">
                    <form class="d-flex bdr-bottom w-100">
                        <input type="text" placeholder="Search entire storage here...">
                        <button class="search-btn"><i class="fa fa-search"></i>search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="zh-Hant/img/banner/about.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- about us area start -->
        <section class="about-us section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">關於我們</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active"><a href="qa.php">關於我們</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 mb-5">
                        <div class="timeline">
                            <div class="container right">
                                <div class="content">
                                    <h3>成立於西元1996年的繼威實業</h3>
                                    <p>擁有累積超過20年的加工技術及經驗，一直以來致力於明架天花板加工技術研發，在環保意識及公共安全防火觀念的普及下，我們從國外引入技術改良出適合台灣潮溼環境的硬板材，來取代傳統怕潮溼發霉的材質，並成立自有矽酸鈣板品牌POWER PLAY。</p>
                                </div>
                            </div>
                            <div class="container right">
                                <div class="content">
                                    <h3>於西元2000年從加工廠轉型成矽酸鈣板製造廠</h3>
                                    <p>採生產至加工一條龍的方式製造，確保矽酸鈣板的品質。多年的努力下，使我們成為國內明架天花板市佔率超過一半的領導品牌。</p>
                                </div>
                            </div>
                            <div class="container right">
                                <div class="content">
                                    <h3>西元2005年 採購建置新機器設備</h3>
                                    <p>生產各尺寸矽酸鈣板，適用於各式天花板及隔間牆，成為少數可客製尺寸的矽酸鈣板製造廠。</p>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-thumb js-tilt" data-tilt-perspective="1000" data-tilt-scale="1" data-tilt-speed="500" data-tilt-max="15">
                            <img class="w-100" src="zh-Hant/img/about/about.jpg" alt="about thumb">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- about us area end -->

        <div class="common_block" style="background-image: url(zh-Hant/img/bg/bg-testimonial.jpg)">
            <div class="container">
                <div class="cmb">
                    <h3>環保建材</h3>
                    <p>我們製造之矽酸鈣板皆通過相關單位的規定測試，多年來提供大眾一個良好舒適及安全無虞的使用環境,並且在專業研發人員合作之下，提高產品水準及品質，並取得國家標準檢驗局之耐燃防火證書，也獲得內政部健康綠建材之品質肯定，100%無石綿可安心使用，為符合安全健康的環保建材。</p>
                </div>
            </div>
        </div>

        <div class="common_block cmb_right" style="background-image: url(zh-Hant/img/bg/bg-body1.jpg)">
            <div class="container">
                <div class="cmb">
                    <h3>誠實服務</h3>
                    <p>繼威始終秉持著「誠實服務」的經營理念，以核心的研發生產能力及良好的企業文化，在建材的專業領域不斷創新及提升品質，並持續朝行銷、服務面經營努力，提高品牌的能見度，同時長期貫徹資源再生的理念，成為善盡社會責任、對環境友善的績優企業。</p>
                    <p>期望在我們的努力下，讓台灣品質在國際上受到認證。</p>
                </div>
            </div>
        </div>

        <div class="content container-fluid about-photo">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6"><img src="zh-Hant/img/about/ph01.jpg" alt="About"></div>
                <div class="col-md-3 col-sm-6 col-xs-6"><img src="zh-Hant/img/about/ph02.jpg" alt="About"></div>
                <div class="col-md-3 col-sm-6 col-xs-6"><img src="zh-Hant/img/about/ph03.jpg" alt="About"></div>
                <div class="col-md-3 col-sm-6 col-xs-6"><img src="zh-Hant/img/about/ph04.jpg" alt="About"></div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6"><img src="zh-Hant/img/about/ph05.jpg" alt="About"></div>
                <div class="col-md-3 col-sm-6 col-xs-6"><img src="zh-Hant/img/about/ph06.jpg" alt="About"></div>
                <div class="col-md-3 col-sm-6 col-xs-6"><img src="zh-Hant/img/about/ph07.jpg" alt="About"></div>
                <div class="col-md-3 col-sm-6 col-xs-6"><img src="zh-Hant/img/about/ph08.jpg" alt="About"></div>
            </div>
        </div>

    </main>
    <!-- nquiry-wrapper start -->
    <?php include_once 'inquiry.php'; ?>
    <!-- nquiry-wrapper End -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->


    <!-- footer area start -->
    <?php include_once 'footer.php'; ?>
    <!-- footer area end -->



    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="zh-Hant/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="zh-Hant/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="zh-Hant/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="zh-Hant/js/vendor/bootstrap.min.js"></script>
    <!-- slick Slider JS -->
    <script src="zh-Hant/js/plugins/slick.min.js"></script>
    <!-- Countdown JS -->
    <script src="zh-Hant/js/plugins/countdown.min.js"></script>
    <!-- Nice Select JS -->
    <script src="zh-Hant/js/plugins/nice-select.min.js"></script>
    <!-- jquery UI JS -->
    <script src="zh-Hant/js/plugins/jqueryui.min.js"></script>
    <!-- Image zoom JS -->
    <script src="zh-Hant/js/plugins/image-zoom.min.js"></script>
    <!-- image loaded js -->
    <script src="zh-Hant/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- masonry  -->
    <script src="zh-Hant/js/plugins/masonry.pkgd.min.js"></script>
    <!-- mailchimp active js -->
    <script src="zh-Hant/js/plugins/ajaxchimp.js"></script>
    <!-- contact form dynamic js -->
    <script src="zh-Hant/js/plugins/ajax-mail.js"></script>
    <!-- google map api -->
    <script src="http://ditu.google.cn/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="zh-Hant/js/plugins/google-map.js"></script>
    <!-- Main JS -->
    <script src="zh-Hant/js/main.js"></script>
</body>

</html>