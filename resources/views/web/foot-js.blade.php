<!-- Modernizer JS -->
<script src="/static/{!!$lang!!}/js/vendor/modernizr-3.6.0.min.js"></script>
<!-- jQuery JS -->
<script src="/static/{!!$lang!!}/js/vendor/jquery-3.3.1.min.js"></script>
<!-- Popper JS -->
<script src="/static/{!!$lang!!}/js/vendor/popper.min.js"></script>
<!-- Bootstrap JS -->
<script src="/static/{!!$lang!!}/js/vendor/bootstrap.min.js"></script>
<!-- slick Slider JS -->
<script src="/static/{!!$lang!!}/js/plugins/slick.min.js"></script>
<!-- Countdown JS -->
<script src="/static/{!!$lang!!}/js/plugins/countdown.min.js"></script>
<!-- Nice Select JS -->
<script src="/static/{!!$lang!!}/js/plugins/nice-select.min.js"></script>
<!-- jquery UI JS -->
<script src="/static/{!!$lang!!}/js/plugins/jqueryui.min.js"></script>
<!-- Image zoom JS -->
<script src="/static/{!!$lang!!}/js/plugins/image-zoom.min.js"></script>
<!-- image loaded js -->
<script src="/static/{!!$lang!!}/js/plugins/imagesloaded.pkgd.min.js"></script>
<!-- masonry  -->
<script src="/static/{!!$lang!!}/js/plugins/masonry.pkgd.min.js"></script>
<!-- mailchimp active js -->
<script src="/static/{!!$lang!!}/js/plugins/ajaxchimp.js"></script>
<!-- contact form dynamic js -->
<script src="/static/{!!$lang!!}/js/plugins/ajax-mail.js"></script>
<!-- google map api -->
<script src="http://ditu.google.cn/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
<!-- google map active js -->
<script src="/static/{!!$lang!!}/js/plugins/google-map.js"></script>
<!-- Main JS -->
<script src="/static/{!!$lang!!}/js/main.js"></script>
<script type="application/ld+json">
            {
                "@context": "https://schema.org",
                "@type": "Corporation",
            "name": "{!! $webData->company['name'] !!}",
            "description":"{{ $pageSeo['meta_description'] ?? '' }}",
                "url": "{!! url()->full() !!}",
            "logo": "{!!asset($webData->system_logo[0]['path']) !!}",
            "image": "{!! asset($webData->system_logo[0]['path']) !!}",
            "address": "{!! $webData->contact['address'] !!}",
            "faxNumber": "{!! $webData->contact['phone'] !!}",
                "contactPoint": {
                    "@type": "ContactPoint",
                    "telephone": "{!! $webData->contact['phone'] !!}",
                    "contactType": "customer service",
                    "areaServed": "TW",
                    "availableLanguage": "Chinese (Traditional)"
                },
                "sameAs": ""
            }
        </script>
@stack('scripts')