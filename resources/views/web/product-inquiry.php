<?php
?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <?php include_once './head.php'; ?>
</head>

<body>
    <!-- Start Header Area -->
    <?php include_once 'header.php'; ?>
    <!-- end Header Area -->


    <!-- offcanvas search form start -->
    <div class="offcanvas-search-wrapper">
        <div class="offcanvas-search-inner">
            <div class="offcanvas-close">
                <i class="fa fa-close"></i>
            </div>
            <div class="container">
                <div class="offcanvas-search-box">
                    <form class="d-flex bdr-bottom w-100">
                        <input type="text" placeholder="Search entire storage here...">
                        <button class="search-btn"><i class="fa fa-search"></i>search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="zh-Hant/img/banner/shop.jpg">

        </div>
        <!-- breadcrumb area end -->

        <!-- cart main wrapper start -->
        <div class="cart-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">詢價車</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">詢價車</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="section-bg-color">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Cart Table Area -->
                            <div class="cart-table table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="pro-thumbnail">產品圖片</th>
                                            <th class="pro-title">產品名稱</th>
                                            <th class="pro-price">產品規格</th>
                                            <th class="pro-quantity">數量</th>
                                            <!-- <th class="pro-subtotal">金額</th> -->
                                            <th class="pro-remove">刪除</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="zh-Hant/img/product/product-1.jpg" alt="Product" /></a></td>
                                            <td class="pro-title"><a href="#">PVC貼皮系列</a></td>
                                            <td class="pro-price">4.0±0.2*603*603mm</td>
                                            <td class="pro-quantity">
                                                <input type="text" placeholder="1,00000">
                                            </td>
                                            <td class="pro-remove"><a href="#"><i class="fa fa-trash-o"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="zh-Hant/img/product/product-1.jpg" alt="Product" /></a></td>
                                            <td class="pro-title"><a href="#">PVC貼皮系列</a></td>
                                            <td class="pro-price">4.0±0.2*603*603mm</td>
                                            <td class="pro-quantity">
                                                <input type="text" placeholder="1,00000">
                                            </td>
                                            <td class="pro-remove"><a href="#"><i class="fa fa-trash-o"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="zh-Hant/img/product/product-1.jpg" alt="Product" /></a></td>
                                            <td class="pro-title"><a href="#">PVC貼皮系列</a></td>
                                            <td class="pro-price">4.0±0.2*603*603mm</td>
                                            <td class="pro-quantity">
                                                <input type="text" placeholder="1,00000">
                                            </td>
                                            <td class="pro-remove"><a href="#"><i class="fa fa-trash-o"></i></a></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8 mt-5">
                        <h2>INQUIRY FROM</h2>
                        <p class="brief mb-4">
                            Enter your message in the box provided and include as many details as possible to help us assist you with
                            your inquiry. Fields marked with <span class="font_red">*</span>are required.
                        </p>
                        <form class="contact-form">
                            <div class="form-group">
                                <label for="Subject" class="must">Subject</label>
                                <input type="text" class="form-control" placeholder="Products Inquiry" value="Products Inquiry">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1" class="must">Leave Your Message</label>
                                <textarea></textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Inquiry Items</label>
                                <textarea></textarea>
                            </div>

                            <div class="form-group mb-10">
                                <label for="exampleFormControlTextarea1" class="must">Other Requested Information</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="">
                                    <label class="form-check-label" for="defaultCheck1">
                                        Price Quote
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="">
                                    <label class="form-check-label" for="defaultCheck1">
                                        Delivery Time
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="">
                                    <label class="form-check-label" for="defaultCheck1">
                                        Data Sheet / Brochure
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="">
                                    <label class="form-check-label" for="defaultCheck1">
                                        Minimum Order
                                    </label>
                                </div>
                            </div>
                            <div class="form-group mb-10">
                                <label for="exampleFormControlFile1">Upload a File (Max:10mb)</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <h2 class="mt-30">Contact Information</h2>
                            <div class="form-group">
                                <label for="Subject" class="must">Company Name</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="Subject" class="must">Company Website</label>
                                <input type="text" class="form-control" placeholder="" value="http://">
                            </div>
                            <div class="form-group">
                                <label for="name" class="must">Country</label>
                                <div class="bfh-selectbox bfh-countries" data-country="US" data-flags="true"></div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="must">Business Type</label>
                                <select class="form-control">
                                    <option value="1">Importer / Exporter</option>
                                    <option value="2">Distributor / Wholesaler</option>
                                    <option value="3">Trading Company</option>
                                    <option value="4">Agents</option>
                                    <option value="5">Buying Offices</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name" class="must">Contact Name</label>
                                <div class="form-row">
                                    <div class="col">
                                        <input type="text" class="form-control" placeholder="First name">
                                    </div>
                                    <div class="col">
                                        <input type="text" class="form-control" placeholder="Last name">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail4" class="must">Email</label>
                                <input type="email" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="tel" class="must">TEL</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="fax">FAX</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress">Address</label>
                                <input type="text" class="form-control" id="inputAddress" placeholder="">
                            </div>

                            <a href="order-detail.php" class="btn btn-sqr mb-5">Submit</a>
                            <!-- <button type="submit" class="btn btn-4 mb-30 mt-20">Submit</button> -->

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- cart main wrapper end -->
    </main>

    <!-- nquiry-wrapper start -->
    <?php include_once 'inquiry.php'; ?>
    <!-- nquiry-wrapper End -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->


    <!-- footer area start -->
    <?php include_once 'footer.php'; ?>
    <!-- footer area end -->



    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="zh-Hant/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="zh-Hant/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="zh-Hant/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="zh-Hant/js/vendor/bootstrap.min.js"></script>
    <!-- slick Slider JS -->
    <script src="zh-Hant/js/plugins/slick.min.js"></script>
    <!-- Countdown JS -->
    <script src="zh-Hant/js/plugins/countdown.min.js"></script>
    <!-- Nice Select JS -->
    <script src="zh-Hant/js/plugins/nice-select.min.js"></script>
    <!-- jquery UI JS -->
    <script src="zh-Hant/js/plugins/jqueryui.min.js"></script>
    <!-- Image zoom JS -->
    <script src="zh-Hant/js/plugins/image-zoom.min.js"></script>
    <!-- image loaded js -->
    <script src="zh-Hant/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- masonry  -->
    <script src="zh-Hant/js/plugins/masonry.pkgd.min.js"></script>
    <!-- mailchimp active js -->
    <script src="zh-Hant/js/plugins/ajaxchimp.js"></script>
    <!-- contact form dynamic js -->
    <script src="zh-Hant/js/plugins/ajax-mail.js"></script>
    <!-- google map api -->
    <script src="http://ditu.google.cn/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="zh-Hant/js/plugins/google-map.js"></script>
    <!-- Main JS -->
    <script src="zh-Hant/js/main.js"></script>

    <!-- Bootstrap Form Helpers -->
    <script src="zh-Hant/js/bootstrap-formhelpers.min.js"></script>
    <script>
        function getVal() {
            // This will get the value of #timepicker
            alert($("#timepicker").val())
        }
    </script>
</body>

</html>