@extends('web.site',['pageCssId' => 'product-details'])
@section("product-details-main")


    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="{!! $banner->details['pic'][0]['path'] !!}">
        </div>
        <!-- breadcrumb area end -->

        <!-- page main wrapper start -->
        <div class="shop-main-wrapper section-padding pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">@lang('web.Menu.ProductList')</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="{!! $link !!}product/{!! $id2 !!}">@lang('web.Menu.ProductList')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">{!! $data->title !!}</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- product details wrapper start -->
                    <div class="col-lg-12 order-1 order-lg-2">
                        <!-- product details inner end -->
                        <div class="product-details-inner">
                            <div class="row">
                                <div class="col-lg-3 col-md-3">
                                    <aside class="sidebar-wrapper">
                                        <!-- single sidebar start -->
                                        <div class="sidebar-single">
                                            <h6 class="sidebar-title">@lang('web.Menu.ProductCate')</h6>

                                            <div class="s-side">
                                                <ul>
                                                    @if(count($productMenu) >0)
                                                        @foreach($productMenu as $value)
                                                            <li>
                                                                <div class="d-firstNav s-firstNav">
                                                                    <i class="fa fa-arrow-circle-right mr-1"></i>
                                                                    <span>{!! $value->title !!}</span>
                                                                </div>
                                                                @if(count($value['child']))
                                                                    <ul class="d-firstDrop s-firstDrop"  {!! $value->id == $parentId ? 'style="display:block"' : ''!!}>
                                                                        @foreach($value['child'] as $v)
                                                                            <li>
                                                                                <div class="d-secondNav s-secondNav">
                                                                                    <a href="{!! $link !!}product/{!! $v->id !!}">
                                                                                        <i class="icon-circle"></i>
                                                                                        <span>{!! $value->title !!}</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                        @endforeach


                                                                    </ul>
                                                                @endif

                                                            </li>
                                                        @endforeach
                                                    @endif


                                                    <li>
                                                        <div class="d-firstNav s-firstNav">
                                                            <i class="fa fa-arrow-circle-right mr-1"></i>
                                                            <span><a href="{!! $link !!}download">@lang('web.Menu.Download')</a></span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                        <!-- single sidebar end -->

                                    </aside>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <!-- product details exzoom start -->
                                    <div class="exzoom hidden" id="exzoom">

                                        <div class="exzoom_img_box">

                                            @if( !empty($data->pic) && count($data->pic) >0 )
                                                <ul class='exzoom_img_ul'>
                                                    @foreach($data->pic as $value)
                                                        <li><img src="{!! $value['path'] !!}" alt="{!! $value['title'] !!}"></li>
                                                    @endforeach


                                                </ul>
                                            @endif

                                        </div>
                                        <div class="exzoom_nav"></div>
                                        <p class="exzoom_btn">
                                            <a href="javascript:void(0);" class="exzoom_prev_btn"><i class="fa fa-chevron-left"></i></a>
                                            <a href="javascript:void(0);" class="exzoom_next_btn"><i class="fa fa-chevron-right"></i></a>
                                        </p>
                                    </div>
                                    <!-- product details exzoom end -->
                                </div>

                                <div class="col-lg-5">
                                    <form method="post" action="{!! $link !!}add_car" target="hidFrame">
                                        <div class="product-details-des">
                                        <h3 class="product-name">{!! $data ->title !!}</h3>
                                        <input name="productNo" value="{!! $data->id !!}" type="hidden">
                                            @csrf
                                        {!! $data->details['feature'] !!}
                                        <p class="pro-desc"></p>
                                        <h5 class="offer-text">@lang('web.Menu.Spec'):</h5>
                                        <div class="sel-item">
                                            @if(count($spec) >0)
                                                @php($i = 0)
                                                @foreach($spec as $key=> $value)
                                                    <label class="form-check form-check-inline sel-size">
                                                        <input type="radio"  {!! $i == 0 ? 'checked' :'' !!} value="{!! $key !!}"  name="spec">
                                                        <span class="checkmark">{!! $value !!}</span>
                                                    </label>
                                                    @php($i++)
                                                @endforeach
                                            @endif


                                        </div>

                                        <div class="quantity-cart-box d-flex align-items-center">

                                            <div class="action_link">
                                                <button class="btn btn-cart2">@lang('web.Menu.JoinInquiry')</button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- product details inner end -->

                        <!-- product details reviews start -->
                        <div class="product-details-reviews section-padding pb-0">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-9">
                                    <div class="product-review-info">
                                        <ul class="nav review-tab">
                                            <li>
                                                <a class="active" data-toggle="tab" href="#tab_one">@lang('web.Menu.Tab1'):</a>
                                            </li>
                                            <li>
                                                <a data-toggle="tab" href="#tab_two">@lang('web.Menu.Tab2')</a>
                                            </li>
                                            <!-- <li>
                                                <a data-toggle="tab" href="#tab_three">矽酸鈣天花板之特性:</a>
                                            </li> -->
                                        </ul>
                                        <div class="tab-content reviews-tab">
                                            <div class="tab-pane fade show active" id="tab_one">
                                                <div class="tab-one">

                                                    {!! $data ->details['detail'] !!}
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_two">
                                                 {!! $data->details['specification'] !!}
                                            </div>
                                            <!-- <div class="tab-pane fade" id="tab_three">
                                                具有一般防火天花板如CNS14705耐燃一級、防潮、耐候、耐壓、隔音、隔熱、易切割等特質外，同時由於市場的專業與不斷創新研發，除了有平面印刷的式樣，我們也提供了造型優雅適用各種場合的明架立體天花板與暗明架天花板材。

                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- product details reviews end -->
                    </div>
                    <!-- product details wrapper end -->
                </div>
            </div>
        </div>
        <!-- page main wrapper end -->

        <!-- Related product area start -->
        <section class="product-gallery section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title text-center">
                            <h3 class="title">@lang('web.Menu.Related')</h3>
                            <!-- <h4 class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius claritas est etiam processus dynamicus, qui sequitur mutationem.</h4> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="product-carousel--4 slick-row-5 slick-arrow-style">
                            @if($related->count()>0)
                                @foreach($related as $value)
                                    <div class="product-item">
                                        <div class="product-thumb">
                                            <a href="{!! $link !!}product-details/{!! $value->id !!}/{!! $id2 !!}">
                                                <img src="{!! $value->pic[0]['path'] !!}" alt="{!! $value->pic[0]['title'] !!}">
                                            </a>
{{--                                            <div class="product-label">--}}
{{--                                                <span>new</span>--}}
{{--                                            </div>--}}
                                        </div>
                                        <div class="product-content">
                                            <div class="product-caption">
                                                <h6 class="product-name">
                                                    <a href="{!! $link !!}product-details/{!! $value->id !!}/{!! $id2 !!}">{!! $value->title !!}</a>
                                                </h6>
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Related product area end -->
    </main>

    <iframe name="hidFrame" id="hidFrame"  width="0" height="0" style="display:none"></iframe>
@endsection
@push('scripts')
    <script src="/static/{!! $lang !!}/js/vendor/imagesloaded.pkgd.min.js"></script>
    <script src="/static/{!! $lang !!}/js/vendor/jquery.exzoom.js"></script>

    <script>
        $('.product-details-inner').imagesLoaded(function() {
            $("#exzoom").exzoom({
                autoPlay: false,
            });
            $("#exzoom").removeClass('hidden')
        });
    </script>

@endpush

    <!-- 產品圖放大 -->

