<?php
?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <?php include_once './head.php'; ?>
</head>

<body>
    <!-- Start Header Area -->
    <?php include_once 'header.php'; ?>
    <!-- end Header Area -->


    <!-- offcanvas search form start -->
    <div class="offcanvas-search-wrapper">
        <div class="offcanvas-search-inner">
            <div class="offcanvas-close">
                <i class="fa fa-close"></i>
            </div>
            <div class="container">
                <div class="offcanvas-search-box">
                    <form class="d-flex bdr-bottom w-100">
                        <input type="text" placeholder="Search entire storage here...">
                        <button class="search-btn"><i class="fa fa-search"></i>search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="zh-Hant/img/banner/shop.jpg">
        </div>
        <!-- breadcrumb area end -->

        <!-- blog main wrapper start -->
        <div class="blog-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">網站地圖</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">網站地圖</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="blog-item-wrapper">
                            <!-- blog item wrapper end -->
                            <div class="row mbn-30">
                                <div class="col-md-3">
                                    <!-- blog post item start -->
                                    <div class="blog-post-item d-block mb-30">
                                        <div class="display-content w-100 pl-0 mt-20">
                                            <h6 class="tit2 mb-4">
                                                <a href="#">公司簡介</a>
                                            </h6>
                                            <ul>
                                                <li><a href="#">公司簡介</a></li>
                                                <li><a href="#">最新消息</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- blog post item end -->
                                </div>
                                <div class="col-md-3">
                                    <!-- blog post item start -->
                                    <div class="blog-post-item d-block mb-30">
                                        <div class="display-content w-100 pl-0 mt-20">
                                            <h6 class="tit2 mb-4">
                                                <a href="#">產品目錄</a>
                                            </h6>
                                            <ul>
                                                <li><a href="product.php">矽酸鈣天花板</a></li>
                                                <li><a href="product.php">矽酸鈣天花板</a></li>
                                                <li><a href="product.php">矽酸鈣天花板</a></li>
                                                <li><a href="display.php">空間展示</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- blog post item end -->
                                </div>
                                <div class="col-md-3">
                                    <!-- blog post item start -->
                                    <div class="blog-post-item d-block mb-30">
                                        <div class="display-content w-100 pl-0 mt-20">
                                            <h6 class="tit2 mb-4">
                                                <a href="#">Q&amp;A</a>
                                            </h6>
                                            <ul>
                                                <li><a href="qa.php">Q&amp;A</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- blog post item end -->
                                </div>
                                <div class="col-md-3">
                                    <!-- blog post item start -->
                                    <div class="blog-post-item d-block mb-30">
                                        <div class="display-content w-100 pl-0 mt-20">
                                            <h6 class="tit2 mb-4">
                                                <a href="#">相關下載</a>
                                            </h6>
                                            <ul>
                                                <li><a href="#">相關下載</a></li>
                                                <li><a href="#">相關下載</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- blog post item end -->
                                </div>
                                <div class="col-md-3">
                                    <!-- blog post item start -->
                                    <div class="blog-post-item d-block mb-30">
                                        <div class="display-content w-100 pl-0 mt-20">
                                            <h6 class="tit2 mb-4">
                                                <a href="#">聯絡我們</a>
                                            </h6>
                                            <ul>
                                                <li><a href="contact-us.php">聯絡我們</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- blog post item end -->
                                </div>
                            </div>
                            <!-- blog item wrapper end -->


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- blog main wrapper end -->
    </main>

    <!-- nquiry-wrapper start -->
    <?php include_once 'inquiry.php'; ?>
    <!-- nquiry-wrapper End -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->


    <!-- footer area start -->
    <?php include_once 'footer.php'; ?>
    <!-- footer area end -->



    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="zh-Hant/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="zh-Hant/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="zh-Hant/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="zh-Hant/js/vendor/bootstrap.min.js"></script>
    <!-- slick Slider JS -->
    <script src="zh-Hant/js/plugins/slick.min.js"></script>
    <!-- Countdown JS -->
    <script src="zh-Hant/js/plugins/countdown.min.js"></script>
    <!-- Nice Select JS -->
    <script src="zh-Hant/js/plugins/nice-select.min.js"></script>
    <!-- jquery UI JS -->
    <script src="zh-Hant/js/plugins/jqueryui.min.js"></script>
    <!-- Image zoom JS -->
    <script src="zh-Hant/js/plugins/image-zoom.min.js"></script>
    <!-- image loaded js -->
    <script src="zh-Hant/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- masonry  -->
    <script src="zh-Hant/js/plugins/masonry.pkgd.min.js"></script>
    <!-- mailchimp active js -->
    <script src="zh-Hant/js/plugins/ajaxchimp.js"></script>
    <!-- contact form dynamic js -->
    <script src="zh-Hant/js/plugins/ajax-mail.js"></script>
    <!-- google map api -->
    <script src="http://ditu.google.cn/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="zh-Hant/js/plugins/google-map.js"></script>
    <!-- google map active js -->
    <script src="zh-Hant/js/plugins/jquery.galpop.min.js"></script>
    <!-- Main JS -->
    <script src="zh-Hant/js/main.js"></script>
</body>