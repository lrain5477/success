<?php
?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <?php include_once './head.php'; ?>
</head>



<body>
    <!-- Start Header Area -->
    <?php include_once 'header.php'; ?>
    <!-- end Header Area -->


    <!-- offcanvas search form start -->
    <div class="offcanvas-search-wrapper">
        <div class="offcanvas-search-inner">
            <div class="offcanvas-close">
                <i class="fa fa-close"></i>
            </div>
            <div class="container">
                <div class="offcanvas-search-box">
                    <form class="d-flex bdr-bottom w-100">
                        <input type="text" placeholder="Search entire storage here...">
                        <button class="search-btn"><i class="fa fa-search"></i>search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="zh-Hant/img/banner/shop.jpg">
        </div>
        <!-- breadcrumb area end -->

        <!-- page main wrapper start -->
        <div class="shop-main-wrapper section-padding pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">商品目錄</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="product.php">商品目錄</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">商品名稱-矽酸鈣天花板</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- product details wrapper start -->
                    <div class="col-lg-12 order-1 order-lg-2">
                        <!-- product details inner end -->
                        <div class="product-details-inner">
                            <div class="row">
                                <div class="col-lg-3 col-md-3">
                                    <aside class="sidebar-wrapper">
                                        <!-- single sidebar start -->
                                        <div class="sidebar-single">
                                            <h6 class="sidebar-title">商品分類</h6>

                                            <div class="s-side">
                                                <ul>
                                                    <li>
                                                        <div class="d-firstNav s-firstNav">
                                                            <i class="fa fa-arrow-circle-right mr-1"></i>
                                                            <span>沖孔系列</span>
                                                        </div>
                                                        <ul class="d-firstDrop s-firstDrop">
                                                            <li>
                                                                <div class="d-secondNav s-secondNav">
                                                                    <a href="">
                                                                        <i class="icon-circle"></i>
                                                                        <span>矽酸鈣天花板</span>
                                                                    </a>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="d-secondNav s-secondNav">
                                                                    <a href="">
                                                                        <i class="icon-circle"></i>
                                                                        <span>矽酸鈣天花板</span>
                                                                    </a>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="d-secondNav s-secondNav">
                                                                    <a href="">
                                                                        <i class="icon-circle"></i>
                                                                        <span>矽酸鈣天花板</span>
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="d-firstNav s-firstNav">
                                                            <i class="fa fa-arrow-circle-right mr-1"></i>
                                                            <span>立體花紋系列</span>
                                                        </div>
                                                        <ul class="d-firstDrop s-firstDrop">
                                                            <li>
                                                                <div class="d-secondNav s-secondNav">
                                                                    <a href="">
                                                                        <i class="icon-circle"></i>
                                                                        <span>矽酸鈣天花板</span>
                                                                    </a>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="d-secondNav s-secondNav">
                                                                    <a href="">
                                                                        <i class="icon-circle"></i>
                                                                        <span>矽酸鈣天花板</span>
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="d-firstNav s-firstNav">
                                                            <i class="fa fa-arrow-circle-right mr-1"></i>
                                                            <span><a href="#">相關下載</a></span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                        <!-- single sidebar end -->

                                    </aside>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <!-- product details exzoom start -->
                                    <div class="exzoom hidden" id="exzoom">
                                        <div class="exzoom_img_box">
                                            <ul class='exzoom_img_ul'>
                                                <li><img src="zh-Hant/img/product/product-1.jpg" alt="product thumb"></li>
                                                <li><img src="zh-Hant/img/product/product-2.jpg" alt="product thumb"></li>
                                                <li><img src="zh-Hant/img/product/product-3.jpg" alt="product thumb"></li>
                                                <li><img src="zh-Hant/img/product/product-4.jpg" alt="product thumb"></li>
                                                <li><img src="zh-Hant/img/product/product-5.jpg" alt="product thumb"></li>
                                            </ul>
                                        </div>
                                        <div class="exzoom_nav"></div>
                                        <p class="exzoom_btn">
                                            <a href="javascript:void(0);" class="exzoom_prev_btn"><i class="fa fa-chevron-left"></i></a>
                                            <a href="javascript:void(0);" class="exzoom_next_btn"><i class="fa fa-chevron-right"></i></a>
                                        </p>
                                    </div>
                                    <!-- product details exzoom end -->
                                </div>
                                <div class="col-lg-5">
                                    <div class="product-details-des">
                                        <h3 class="product-name">矽酸鈣天花板</h3>
                                        <h5 class="offer-text">底板物理性質:</h5>
                                        <ul class="pro-desc">
                                            <li>容積密度: 1.05±10% g/cm3</li>
                                            <li>抗彎強度: 13.0 MPa以上</li>
                                            <li>導熱係數: 0.15 W/m˙K 以下</li>
                                            <li>吸水長度變化率: 0.12 % 以下</li>
                                        </ul>
                                        <p class="pro-desc"></p>
                                        <h5 class="offer-text">產品規格:</h5>
                                        <div class="sel-item">
                                            <label class="form-check form-check-inline sel-size">
                                                <input type="radio" checked="checked" name="radio">
                                                <span class="checkmark">1.8mmx14”</span>
                                            </label>
                                            <label class="form-check form-check-inline sel-size">
                                                <input type="radio" name="radio">
                                                <span class="checkmark">2.4mmx16”</span>
                                            </label>
                                            <label class="form-check form-check-inline sel-size">
                                                <input type="radio" name="radio">
                                                <span class="checkmark">3.0mmx18”</span>
                                            </label>
                                            <label class="form-check form-check-inline sel-size">
                                                <input type="radio" name="radio">
                                                <span class="checkmark">3.8mmx20”</span>
                                            </label>
                                            <label class="form-check form-check-inline sel-size">
                                                <input type="radio" name="radio">
                                                <span class="checkmark">4.0mmx22”</span>
                                            </label>
                                            <label class="form-check form-check-inline sel-size">
                                                <input type="radio" name="radio">
                                                <span class="checkmark">4.5mmx24”</span>
                                            </label>
                                        </div>

                                        <div class="quantity-cart-box d-flex align-items-center">

                                            <div class="action_link">
                                                <a class="btn btn-cart2" href="product-inquiry.php">加入詢價車</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- product details inner end -->

                        <!-- product details reviews start -->
                        <div class="product-details-reviews section-padding pb-0">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-9">
                                    <div class="product-review-info">
                                        <ul class="nav review-tab">
                                            <li>
                                                <a class="active" data-toggle="tab" href="#tab_one">矽酸鈣板特性:</a>
                                            </li>
                                            <li>
                                                <a data-toggle="tab" href="#tab_two">PVC貼皮系列</a>
                                            </li>
                                            <!-- <li>
                                                <a data-toggle="tab" href="#tab_three">矽酸鈣天花板之特性:</a>
                                            </li> -->
                                        </ul>
                                        <div class="tab-content reviews-tab">
                                            <div class="tab-pane fade show active" id="tab_one">
                                                <div class="tab-one">

                                                    <ul>
                                                        <li>矽酸鈣天花板是矽質、鈣質添加物及強韌耐熱纖維，經高溫、高壓蒸氣養生製造而成，100%不燃、無公害的安全環保建材。　
                                                        </li>
                                                        <li>具備質輕、防火、防潮、隔熱、隔音、不變形、不發霉、易切割產品等特色，適用於潮濕多變化的氣候及各種商業空間建築室內之天花板。　
                                                        </li>
                                                        <li>防火 : 符合CNS14705耐燃一級，法定防火建材。</li>
                                                        <li>防潮、不變形：矽酸鈣板中之孔細可以吸入並釋出水氣來有效調節板材中的含水量，且不容易產生變形、穩定性高，被稱為會呼吸的人造材料。　
                                                        </li>
                                                        <li>隔熱、隔音：經過測試的矽酸鈣板之隔音效果可達46db以上，導熱係數低，有效使室內冷氣不易散出，可以節省能源，符合環保概念，增加經濟效益。
                                                        </li>
                                                        <li>易切割：任何切割工具就能輕易切斷加工，安裝方便，提高施工效率，降低成本，美工刀亦可作業。
                                                        </li>
                                                        <li>矽酸鈣天花板板具有一般防火天花板CNS14705耐燃一級、防潮、耐候、隔
                                                            　音、隔熱、易切割等特質外，經過不斷研發與創新，除了平面印刷及PVC貼
                                                            　皮外，還有新創的噴塗、沖孔和立體花紋樣式供選擇。
                                                        </li>
                                                        <li>沖孔系列：在矽酸鈣板上穿出口徑一致的孔洞，使原本吸音率不佳之矽酸鈣板產生優異的吸音效果。
                                                        </li>
                                                        <li>立體花紋系列：繼威獨家的立體花紋技術，不同於其他後加工技術，我們的矽酸鈣板花紋以一體成形方式製作，除了如美術雕刻般的立體效果，也克服了傳統雕刻後易產生落塵的問題。
                                                        </li>
                                                        <li>噴塗系列：為追求更高的品質，不同於一般的表面印刷處理，繼威開發新創的噴塗技術，使用於所有矽酸鈣板板面的底色，讓板面顏色更為均勻細緻，品質看的見，並且是市面上唯一通過健康綠建材1.0FKD E1等級之產品。
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_two">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <td>color</td>
                                                            <td>black, blue, red</td>
                                                        </tr>
                                                        <tr>
                                                            <td>size</td>
                                                            <td>L, M, S</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- <div class="tab-pane fade" id="tab_three">
                                                具有一般防火天花板如CNS14705耐燃一級、防潮、耐候、耐壓、隔音、隔熱、易切割等特質外，同時由於市場的專業與不斷創新研發，除了有平面印刷的式樣，我們也提供了造型優雅適用各種場合的明架立體天花板與暗明架天花板材。

                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- product details reviews end -->
                    </div>
                    <!-- product details wrapper end -->
                </div>
            </div>
        </div>
        <!-- page main wrapper end -->

        <!-- Related product area start -->
        <section class="product-gallery section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title text-center">
                            <h3 class="title">相關產品</h3>
                            <!-- <h4 class="sub-title">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius claritas est etiam processus dynamicus, qui sequitur mutationem.</h4> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="product-carousel--4 slick-row-5 slick-arrow-style">
                            <!-- product single item start -->
                            <div class="product-item">
                                <div class="product-thumb">
                                    <a href="product-details.php">
                                        <img src="zh-Hant/img/product/product-1.jpg" alt="product thumb">
                                    </a>
                                    <div class="product-label">
                                        <span>new</span>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <div class="product-caption">
                                        <h6 class="product-name">
                                            <a href="product-details.php">矽酸鈣天花板</a>
                                        </h6>
                                    </div>

                                </div>
                            </div>
                            <!-- product single item end -->

                            <!-- product single item start -->
                            <div class="product-item">
                                <div class="product-thumb">
                                    <a href="product-details.php">
                                        <img src="zh-Hant/img/product/product-2.jpg" alt="product thumb">
                                    </a>

                                    <div class="product-label">
                                        <span>new</span>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <div class="product-caption">
                                        <h6 class="product-name">
                                            <a href="product-details.php">矽酸鈣天花板</a>
                                        </h6>
                                    </div>

                                </div>
                            </div>
                            <!-- product single item end -->

                            <!-- product single item start -->
                            <div class="product-item">
                                <div class="product-thumb">
                                    <a href="product-details.php">
                                        <img src="zh-Hant/img/product/product-3.jpg" alt="product thumb">
                                    </a>

                                    <div class="product-label">
                                        <span>new</span>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <div class="product-caption">
                                        <h6 class="product-name">
                                            <a href="product-details.php">矽酸鈣天花板</a>
                                        </h6>
                                    </div>

                                </div>
                            </div>
                            <!-- product single item end -->

                            <!-- product single item start -->
                            <div class="product-item">
                                <div class="product-thumb">
                                    <a href="product-details.php">
                                        <img src="zh-Hant/img/product/product-4.jpg" alt="product thumb">
                                    </a>

                                    <div class="product-label">
                                        <span>new</span>
                                    </div>

                                </div>
                                <div class="product-content">
                                    <div class="product-caption">
                                        <h6 class="product-name">
                                            <a href="product-details.php">矽酸鈣天花板</a>
                                        </h6>
                                    </div>

                                </div>
                            </div>
                            <!-- product single item end -->

                            <!-- product single item start -->
                            <div class="product-item">
                                <div class="product-thumb">
                                    <a href="product-details.php">
                                        <img src="zh-Hant/img/product/product-5.jpg" alt="product thumb">
                                    </a>

                                    <div class="product-label">
                                        <span>new</span>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <div class="product-caption">
                                        <h6 class="product-name">
                                            <a href="product-details.php">矽酸鈣天花板</a>
                                        </h6>
                                    </div>

                                </div>
                            </div>
                            <!-- product single item end -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Related product area end -->
    </main>

    <!-- nquiry-wrapper start -->
    <?php include_once 'inquiry.php'; ?>
    <!-- nquiry-wrapper End -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->


    <!-- footer area start -->
    <?php include_once 'footer.php'; ?>
    <!-- footer area end -->



    <!-- Quick view modal start -->
    <div class="modal" id="quick_view">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <!-- product details inner end -->
                    <div class="product-details-inner">
                        <div class="row">
                            <div class="col-lg-5">
                                12
                            </div>
                            <div class="col-lg-7">
                                <div class="product-details-des">
                                    <h3 class="product-name">Premium Mens Sports Lather Keds</h3>

                                    <div class="price-box">
                                        <span class="price-old"><del>$90.00</del></span>
                                        <span class="price-regular">$70.00</span>
                                    </div>
                                    <h5 class="offer-text"><strong>Hurry up</strong>! offer ends in:</h5>
                                    <div class="product-countdown" data-countdown="2019/09/20"></div>
                                    <p class="pro-desc">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
                                        eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</p>
                                    <div class="quantity-cart-box d-flex align-items-center">
                                        <h6 class="option-title">qty:</h6>
                                        <div class="quantity">
                                            <div class="pro-qty"><input type="text" value="1"></div>
                                        </div>
                                        <div class="action_link">
                                            <a class="btn btn-cart2" href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                    <div class="useful-links">
                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-refresh"></i>compare</a>
                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i>wishlist</a>
                                    </div>
                                    <div class="like-icon">
                                        <a class="facebook" href="#"><i class="fa fa-facebook"></i>like</a>
                                        <a class="twitter" href="#"><i class="fa fa-twitter"></i>tweet</a>
                                        <a class="pinterest" href="#"><i class="fa fa-pinterest"></i>save</a>
                                        <a class="google" href="#"><i class="fa fa-google-plus"></i>share</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- product details inner end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Quick view modal end -->

    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="zh-Hant/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="zh-Hant/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="zh-Hant/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="zh-Hant/js/vendor/bootstrap.min.js"></script>
    <!-- slick Slider JS -->
    <script src="zh-Hant/js/plugins/slick.min.js"></script>
    <!-- Countdown JS -->
    <script src="zh-Hant/js/plugins/countdown.min.js"></script>
    <!-- Nice Select JS -->
    <script src="zh-Hant/js/plugins/nice-select.min.js"></script>
    <!-- jquery UI JS -->
    <script src="zh-Hant/js/plugins/jqueryui.min.js"></script>
    <!-- Image zoom JS -->
    <script src="zh-Hant/js/plugins/image-zoom.min.js"></script>
    <!-- image loaded js -->
    <script src="zh-Hant/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- masonry  -->
    <script src="zh-Hant/js/plugins/masonry.pkgd.min.js"></script>
    <!-- mailchimp active js -->
    <script src="zh-Hant/js/plugins/ajaxchimp.js"></script>
    <!-- contact form dynamic js -->
    <script src="zh-Hant/js/plugins/ajax-mail.js"></script>
    <!-- google map api -->
    <script src="http://ditu.google.cn/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="zh-Hant/js/plugins/google-map.js"></script>
    <!-- Main JS -->
    <script src="zh-Hant/js/main.js"></script>

    <!-- 產品圖放大 -->
    <script src="zh-Hant/js/vendor/imagesloaded.pkgd.min.js"></script>
    <script src="zh-Hant/js/vendor/jquery.exzoom.js"></script>

    <script>
        $('.product-details-inner').imagesLoaded(function() {
            $("#exzoom").exzoom({
                autoPlay: false,
            });
            $("#exzoom").removeClass('hidden')
        });
    </script>

</body>