@extends('web.site',['pageCssId' => 'display'])
@section("display-main")
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="{!! $banner->details['pic'][0]['path'] !!}">
        </div>
        <!-- breadcrumb area end -->

        <!-- blog main wrapper start -->
        <div class="blog-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">@lang('web.Menu.Space')</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{!! $link !!}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('web.Menu.Space')</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 order-1 order-lg-1">
                        <aside class="sidebar-wrapper">
                            <!-- single sidebar start -->
                            <div class="sidebar-single">
                                <h6 class="sidebar-title">@lang('web.Menu.ProductCate')</h6>
                                <div class="s-side">
                                    <ul>
                                        @if(count($productMenu) >0)
                                            @foreach($productMenu as $value)
                                                <li>
                                                    <div class="d-firstNav s-firstNav">
                                                        <i class="fa fa-arrow-circle-right mr-1"></i>
                                                        <span>{!! $value->title !!}</span>
                                                    </div>
                                                    @if(count($value['child']))
                                                        <ul class="d-firstDrop s-firstDrop">
                                                            @foreach($value['child'] as $v)
                                                                <li>
                                                                    <div class="d-secondNav s-secondNav">
                                                                        <a href="{!! $link !!}products/{!! $value->id !!}">
                                                                            <i class="icon-circle"></i>
                                                                            <span>{!! $value->title !!}</span>
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                            @endforeach


                                                        </ul>
                                                    @endif

                                                </li>
                                            @endforeach
                                        @endif


                                        <li>
                                            <div class="d-firstNav s-firstNav">
                                                <i class="fa fa-image mr-1"></i>
                                                <span><a href="{!! $link !!}display">@lang('web.Menu.Space')</a></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <!-- single sidebar end -->

                        </aside>
                    </div>
                    <div class="col-lg-9 order-2 order-lg-2">
                        <div class="blog-item-wrapper">
                            <!-- blog item wrapper end -->
                            <div class="row mbn-30">
                                @if(count($data) >0)
                                    @foreach($data as $value)
                                        <div class="col-md-12">
                                            <!-- blog post item start -->
                                            <div class="blog-post-item d-block mb-10">
                                                <div class="gallery-wrapper w-100">
                                                    <a class="galpop-single" href="{!! $value->pic2[0]['path'] !!}">
                                                        <img src="{!! $value->pic[0]['path'] !!}" class="img-thumbnail" alt="{!! $value->pic[0]['path'] !!}" />
                                                    </a>
                                                </div>
                                                <!-- <div class="display-content w-100 pl-0 mt-10">
                                                    <h6 class="blog-title">
                                                        <a href="blog-details.html">矽酸鈣天花板</a>
                                                    </h6>
                                                </div> -->
                                            </div>
                                            <!-- blog post item end -->
                                        </div>
                                    @endforeach
                                @endif



                            </div>
                            <!-- blog item wrapper end -->

                            @include('web.Page')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- blog main wrapper end -->
    </main>

    <!-- nquiry-wrapper start -->
@endsection
@push('scripts')
    <script src="/static/{!! $lang !!}/js/plugins/jquery.galpop.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.galpop-single').galpop();

            $('.galpop-multiple').galpop();

            $('.galpop-info').galpop();


            var callback = function() {
                var wrapper = $('#galpop-wrapper');
                var info = $('#galpop-info');
                var count = wrapper.data('count');
                var index = wrapper.data('index');
                var current = index + 1;
                var string = 'Image ' + current + ' of ' + count;

                info.append('<p>' + string + '</p>').fadeIn();

            };
            $('.galpop-callback').galpop({
                callback: callback
            });

            $('.manual-open').change(function(e) {
                var image = $(this).val();
                if (image) {
                    var settings = {};
                    $.fn.galpop('openBox', settings, image);
                }
            });

            $('.manual-open-group').change(function(e) {
                var v = $(this).val();
                var images = [
                    'images/gallery/large/apocalypse.jpg',
                    'images/gallery/large/vintage.jpg',
                    'images/gallery/large/magicLake.jpg',
                    'images/gallery/large/underwater.jpg',
                    'images/gallery/large/goodBoy.jpg',
                    'images/gallery/large/darkroad.jpg',
                    'images/gallery/large/roadkill.jpg',
                    'images/gallery/large/wolfMarine.jpg',
                    'images/gallery/large/alice.jpg',
                    'images/gallery/large/reflection.jpg',
                ];
                var settings = {};
                $.fn.galpop('openBox', settings, images, v);
            });

            $('.click-open-iframe').galpop({
                contentType: 'iframe',
            });

            $('.click-open-ajax').galpop({
                contentType: 'AJAX',
            });
        });
    </script>

    <script type="application/ld+json">
                {
                  "@context": "http://schema.org",
                  "@type": "BreadcrumbList",
                  "itemListElement":
                  [
                    {
                      "@type": "ListItem",
                      "position": 1,
                      "item":
                      {
                      "@id": "{!! url('/') !!}",
                      "name": "首頁"
                      }
                    },
                     {
                      "@type": "ListItem",
                      "position": 2,
                      "item":
                      {
                        "@id": "{!!url()->full() !!}",
                        "name": "@lang('web.Menu.Display')"
                      }
                    }


                  ]
                }
            </script>
@endpush

    <!-- Main JS -->

