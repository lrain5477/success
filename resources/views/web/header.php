<svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <defs>
        <symbol id="icon-facebook-square" viewBox="0 0 24 28">
            <title>facebook-square</title>
            <path d="M19.5 2c2.484 0 4.5 2.016 4.5 4.5v15c0 2.484-2.016 4.5-4.5 4.5h-2.938v-9.297h3.109l0.469-3.625h-3.578v-2.312c0-1.047 0.281-1.75 1.797-1.75l1.906-0.016v-3.234c-0.328-0.047-1.469-0.141-2.781-0.141-2.766 0-4.672 1.687-4.672 4.781v2.672h-3.125v3.625h3.125v9.297h-8.313c-2.484 0-4.5-2.016-4.5-4.5v-15c0-2.484 2.016-4.5 4.5-4.5h15z">
            </path>
        </symbol>
        <symbol id="icon-youtube-square" viewBox="0 0 24 28">
            <title>youtube-square</title>
            <path d="M14.359 20.359v-2.453c0-0.516-0.156-0.781-0.453-0.781-0.172 0-0.344 0.078-0.516 0.25v3.5c0.172 0.172 0.344 0.25 0.516 0.25 0.297 0 0.453-0.25 0.453-0.766zM17.234 18.453h1.031v-0.531c0-0.531-0.172-0.797-0.516-0.797s-0.516 0.266-0.516 0.797v0.531zM8.313 14.297v1.094h-1.25v6.609h-1.156v-6.609h-1.219v-1.094h3.625zM11.453 16.266v5.734h-1.047v-0.625c-0.406 0.469-0.797 0.703-1.188 0.703-0.328 0-0.562-0.141-0.656-0.438-0.063-0.172-0.094-0.438-0.094-0.844v-4.531h1.031v4.219c0 0.234 0 0.375 0.016 0.406 0.016 0.156 0.094 0.234 0.234 0.234 0.219 0 0.422-0.156 0.656-0.484v-4.375h1.047zM15.391 18v2.281c0 0.516-0.031 0.906-0.109 1.141-0.125 0.438-0.406 0.656-0.828 0.656-0.359 0-0.719-0.219-1.062-0.641v0.562h-1.047v-7.703h1.047v2.516c0.328-0.406 0.688-0.625 1.062-0.625 0.422 0 0.703 0.219 0.828 0.656 0.078 0.234 0.109 0.609 0.109 1.156zM19.313 20.016v0.141c0 0.344-0.016 0.562-0.031 0.672-0.031 0.234-0.109 0.438-0.234 0.625-0.281 0.422-0.719 0.625-1.25 0.625-0.547 0-0.969-0.203-1.266-0.594-0.219-0.281-0.328-0.734-0.328-1.344v-2.016c0-0.609 0.094-1.047 0.313-1.344 0.297-0.391 0.719-0.594 1.25-0.594 0.516 0 0.938 0.203 1.219 0.594 0.219 0.297 0.328 0.734 0.328 1.344v1.188h-2.078v1.016c0 0.531 0.172 0.797 0.531 0.797 0.25 0 0.406-0.141 0.469-0.406 0-0.063 0.016-0.297 0.016-0.703h1.062zM12.266 7.141v2.438c0 0.531-0.172 0.797-0.5 0.797-0.344 0-0.5-0.266-0.5-0.797v-2.438c0-0.531 0.156-0.812 0.5-0.812 0.328 0 0.5 0.281 0.5 0.812zM20.594 18.281v0c0-1.344 0-2.766-0.297-4.062-0.219-0.922-0.969-1.594-1.859-1.687-2.125-0.234-4.281-0.234-6.438-0.234-2.141 0-4.297 0-6.422 0.234-0.906 0.094-1.656 0.766-1.859 1.687-0.297 1.297-0.313 2.719-0.313 4.062v0c0 1.328 0 2.75 0.313 4.062 0.203 0.906 0.953 1.578 1.844 1.687 2.141 0.234 4.297 0.234 6.438 0.234s4.297 0 6.438-0.234c0.891-0.109 1.641-0.781 1.844-1.687 0.313-1.313 0.313-2.734 0.313-4.062zM8.797 8.109l1.406-4.625h-1.172l-0.797 3.047-0.828-3.047h-1.219c0.234 0.719 0.5 1.437 0.734 2.156 0.375 1.094 0.609 1.906 0.719 2.469v3.141h1.156v-3.141zM13.312 9.375v-2.031c0-0.609-0.109-1.062-0.328-1.359-0.297-0.391-0.703-0.594-1.219-0.594-0.531 0-0.938 0.203-1.219 0.594-0.219 0.297-0.328 0.75-0.328 1.359v2.031c0 0.609 0.109 1.062 0.328 1.359 0.281 0.391 0.688 0.594 1.219 0.594 0.516 0 0.922-0.203 1.219-0.594 0.219-0.281 0.328-0.75 0.328-1.359zM16.141 11.25h1.047v-5.781h-1.047v4.422c-0.234 0.328-0.453 0.484-0.656 0.484-0.141 0-0.234-0.078-0.25-0.25-0.016-0.031-0.016-0.156-0.016-0.406v-4.25h-1.047v4.578c0 0.406 0.031 0.672 0.094 0.859 0.109 0.281 0.344 0.422 0.672 0.422 0.391 0 0.781-0.234 1.203-0.703v0.625zM24 6.5v15c0 2.484-2.016 4.5-4.5 4.5h-15c-2.484 0-4.5-2.016-4.5-4.5v-15c0-2.484 2.016-4.5 4.5-4.5h15c2.484 0 4.5 2.016 4.5 4.5z">
            </path>
        </symbol>
    </defs>
</svg>
<!-- Start Header Area -->
<header id="header" class="header-area" data-spy="affix" data-offset-top="197">
    <!-- main header start -->
    <div class="main-header d-none d-lg-block">
        <!-- header top start -->
        <div class="header-top theme-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12 d-flex justify-content-end">
                        <div class="top-left-navigation">
                            <ul class="nav align-items-center">
                                <li class="language">

                                    <i class="fa fa-angle-down"></i>
                                    <ul class="dropdown-list">
                                        <li><a href="#">English</a></li>
                                        <!-- <li><a href="#"><img src="assets/img/icon/fr.png" alt="flag"> French</a>
                                            </li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="communityArea ml-5">
                            <span>
                                <a href="#">
                                    <svg class="icon icon-facebook-square">
                                        <use xlink:href="#icon-facebook-square"></use>
                                    </svg>
                                </a>
                            </span>
                            <span>
                                <a href="#">
                                    <svg class="icon icon-youtube-square">
                                        <use xlink:href="#icon-youtube-square"></use>
                                    </svg>
                                </a>
                            </span>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- header top end -->

        <!-- header middle area start -->
        <div class="header-main-area theme-bg sticky">
            <div class="container">
                <div class="row align-items-center position-relative">
                    <!-- start logo area -->
                    <div class="col-auto">
                        <div class="logo">
                            <a href="index.php">
                                <img src="zh-Hant/img/logo/logo.png" alt="Brand Logo">
                            </a>
                        </div>
                    </div>
                    <!-- start logo area -->

                    <!-- main menu area start -->
                    <div class="col-auto position-static">
                        <div class="main-menu-area">
                            <div class="main-menu">
                                <!-- main menu navbar start -->
                                <nav class="desktop-menu">
                                    <ul>
                                        <li class="active"><a href="#">公司簡介<i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown">
                                                <li><a href="about-us.php"><i class="fa fa-angle-right"></i>關於我們</a></li>
                                                <li><a href="news.php"><i class="fa fa-angle-right"></i>最新消息</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">商品目錄<i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown">
                                                <li><a href="product.php"><i class="fa fa-angle-right"></i>沖孔系列</a></a>
                                                    <ul class="dropdown">
                                                        <li><a href="product-details.php">矽酸鈣天花板</a></li>
                                                        <li><a href="product-details.php">矽酸鈣天花板</a></li>
                                                        <li><a href="product-details.php">矽酸鈣天花板</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="product.php"><i class="fa fa-angle-right"></i>立體花紋系列</a></a>
                                                    <ul class="dropdown">
                                                        <li><a href="product-details.php">矽酸鈣天花板</a></li>
                                                        <li><a href="product-details.php">矽酸鈣天花板</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="display.php"><i class="fa fa-angle-right"></i>空間展示</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="article-details.php">技術知識<i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown">
                                                <li><a href="product.php"><i class="fa fa-angle-right"></i>了解POWER PLAY矽酸鈣板</a></a>
                                                    <ul class="dropdown">
                                                        <li><a href="#">POWER PLAY矽酸鈣板特色</a></li>
                                                        <li><a href="#">矽酸鈣板製程</a></li>
                                                        <li><a href="#">救救北極熊! 綠色建材使用</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="product.php"><i class="fa fa-angle-right"></i>矽酸鈣板用途及安裝</a></a>
                                                    <ul class="dropdown">
                                                        <li><a href="#">矽酸鈣板用途</a></li>
                                                        <li><a href="#">安裝及吊掛</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="product.php"><i class="fa fa-angle-right"></i>建築板材比較圖</a></a>
                                                    <ul class="dropdown">
                                                        <li><a href="#">天花板材質選擇</a></li>
                                                        <li><a href="#">隔間牆材質比較</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="active"><a href="qa.php">Q&amp;A</a>
                                        </li>
                                        <li class="active"><a href="download.php">相關下載</a>
                                        </li>
                                        <li><a href="contact-us.php">聯絡我們</a></li>
                                    </ul>
                                </nav>
                                <!-- main menu navbar end -->
                            </div>
                        </div>
                    </div>
                    <!-- main menu area end -->

                    <!-- mini cart area start -->
                    <div class="col-auto ml-auto">
                        <div class="header-right">
                            <!-- <div class="header-configure-area">
                                <ul class="nav">
                                    <li>
                                        <a href="#" class="search-trigger"><i class="fa fa-search"></i></a>
                                    </li>
                                </ul>
                            </div> -->

                        </div>
                    </div>
                    <!-- mini cart area end -->

                </div>
            </div>
        </div>
        <!-- header middle area end -->
    </div>
    <!-- main header start -->

    <!-- mobile header start -->
    <!-- mobile header start -->
    <div class="mobile-header d-lg-none d-md-block sticky theme-bg">
        <!--mobile header top start -->
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="mobile-main-header">
                        <div class="mobile-logo">
                            <a href="index.php">
                                <img src="zh-Hant/img/logo/logo.png" alt="Brand Logo">
                            </a>
                        </div>
                        <div class="mobile-menu-toggler">
                            <button class="mobile-menu-btn">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- mobile header top start -->
    </div>
    <!-- mobile header end -->
    <!-- mobile header end -->

    <!-- offcanvas mobile menu start -->
    <!-- off-canvas menu start -->
    <aside class="off-canvas-wrapper">
        <div class="off-canvas-overlay"></div>
        <div class="off-canvas-inner-content">
            <div class="btn-close-off-canvas">
                <i class="fa fa-close"></i>
            </div>

            <div class="off-canvas-inner">
                <!-- search box start -->
                <div class="search-box-offcanvas">
                    <form>
                        <input type="text" placeholder="Search Here...">
                        <button class="search-btn"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <!-- search box end -->

                <!-- mobile menu start -->
                <div class="mobile-navigation">
                    <!-- mobile menu navigation start -->
                    <nav>
                        <ul class="mobile-menu">
                            <li class="menu-item-has-children"><a href="index.php">Home</a>
                            </li>
                            <li class="menu-item-has-children"><a href="#">公司簡介</a>
                                <ul class="megamenu dropdown">
                                    <li class="mega-title menu-item-has-children"><a href="about-us.php">關於我們</a></li>
                                    <li class="mega-title menu-item-has-children"><a href="news.php">最新消息</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children"><a href="#">商品目錄</a>
                                <ul class="megamenu dropdown">
                                    <li class="mega-title menu-item-has-children"><a href="#">沖孔系列</a>
                                        <ul class="dropdown">
                                            <li><a href="#">矽酸鈣天花板</a></li>
                                            <li><a href="#">矽酸鈣天花板</a></li>
                                            <li><a href="#">矽酸鈣天花板</a></li>
                                        </ul>
                                    </li>
                                    <li class="mega-title menu-item-has-children"><a href="#">立體花紋系列</a>
                                        <ul class="dropdown">
                                            <li><a href="#">矽酸鈣天花板</a></li>
                                            <li><a href="#">矽酸鈣天花板</a></li>
                                        </ul>
                                    </li>
                                    <li class="mega-title menu-item-has-children"><a href="display.php">空間展示</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children "><a href="#">技術知識</a>
                                <ul class="megamenu dropdown">
                                    <li class="mega-title menu-item-has-children"><a href="#">了解POWER PLAY矽酸鈣板</a>
                                        <ul class="dropdown">
                                            <li><a href="article-details.php">POWER PLAY矽酸鈣板特色</a></li>
                                            <li><a href="article-details.php">矽酸鈣板製程</a></li>
                                            <li><a href="article-details.php">救救北極熊! 綠色建材使用</a></li>
                                        </ul>
                                    </li>
                                    <li class="mega-title menu-item-has-children"><a href="#">矽酸鈣板用途及安裝</a>
                                        <ul class="dropdown">
                                            <li><a href="article-details.php">矽酸鈣板用途</a></li>
                                            <li><a href="article-details.php">安裝及吊掛</a></li>
                                        </ul>
                                    </li>
                                    <li class="mega-title menu-item-has-children"><a href="#"> 建築板材比較圖</a>
                                        <ul class="dropdown">
                                            <li><a href="article-details.php">天花板材質選擇</a></li>
                                            <li><a href="article-details.php">隔間牆材質比較</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="qa.php">Q&amp;A</a></li>
                            <li><a href="download.php">相關下載</a></li>
                            <li><a href="contact-us.php">聯絡我們</a></li>
                        </ul>
                    </nav>
                    <!-- mobile menu navigation end -->
                </div>
                <!-- mobile menu end -->

                <div class="offcanvas-widget-area">
                    <div class="off-canvas-contact-widget">
                        <ul>
                            <li><i class="fa fa-mobile"></i>
                                <a href="#">0123456789</a>
                            </li>
                            <li><i class="fa fa-envelope-o"></i>
                                <a href="#">info@yourdomain.com</a>
                            </li>
                        </ul>
                    </div>
                    <div class="off-canvas-social-widget">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-youtube-play"></i></a>
                    </div>
                </div>
                <!-- offcanvas widget area end -->
            </div>
        </div>
    </aside>
    <!-- off-canvas menu end -->
    <!-- offcanvas mobile menu end -->
</header>
<!-- end Header Area -->