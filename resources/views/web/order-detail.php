<?php
?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <?php include_once './head.php'; ?>
</head>

<body>
    <!-- Start Header Area -->
    <?php include_once 'header.php'; ?>
    <!-- end Header Area -->


    <!-- offcanvas search form start -->
    <div class="offcanvas-search-wrapper">
        <div class="offcanvas-search-inner">
            <div class="offcanvas-close">
                <i class="fa fa-close"></i>
            </div>
            <div class="container">
                <div class="offcanvas-search-box">
                    <form class="d-flex bdr-bottom w-100">
                        <input type="text" placeholder="Search entire storage here...">
                        <button class="search-btn"><i class="fa fa-search"></i>search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="zh-Hant/img/banner/shop.jpg">

        </div>
        <!-- breadcrumb area end -->

        <!-- cart main wrapper start -->
        <div class="cart-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">訂單已完成</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">訂單已完成</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row pb-5">
                    <div class="col-md-12">
                        <p class="font-weight-bold mb-4">Client Information</p>
                        <p class="mb-1"><span class="text-muted">Recipient:</span><em>Eden Hazard</em></p>
                        <p class="mb-1"><span class="text-muted">Recipient's mobile:</span><em>886-49-2251588</em></p>
                        <p class="mb-1"><span class="text-muted">Recipient address:</span><em>NO.19, TZUCHIANG 1 ST ROAD NANTOU TAIWAN, R.O.C</em></p>
                    </div>

                    <!-- <div class="col-md-6 text-left">
                        <p class="font-weight-bold mb-4">Payment Details</p>
                        <p class="mb-1"><span class="text-muted">Order number: </span><em>1425782</em></p>
                        <p class="mb-1"><span class="text-muted">Order date: </span><em>2019-02-22 00:00:00</em></p>
                        <p class="mb-1"><span class="text-muted">Payment method: </span><em>Bank transfer</em></p>
                        <p class="mb-1"><span class="text-muted">Bank Code: </span><em>822</em></p>
                    </div> -->
                </div>
                <div class="section-bg-color mb-5">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Cart Table Area -->
                            <div class="cart-table table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="pro-thumbnail">產品圖片</th>
                                            <th class="pro-title">產品名稱</th>
                                            <th class="pro-quantity">數量</th>
                                            <th class="pro-remove">刪除</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="zh-Hant/img/product/product-1.jpg" alt="Product" /></a></td>
                                            <td class="pro-title"><a href="#">橡木板</a></td>
                                            <td class="pro-quantity">
                                                <input type="text" placeholder="1,00000">
                                            </td>
                                            <td class="pro-remove"><a href="#"><i class="fa fa-trash-o"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="zh-Hant/img/product/product-1.jpg" alt="Product" /></a></td>
                                            <td class="pro-title"><a href="#">橡木板</a></td>
                                            <td class="pro-quantity">
                                                <input type="text" placeholder="1,00000">
                                            </td>
                                            <td class="pro-remove"><a href="#"><i class="fa fa-trash-o"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="zh-Hant/img/product/product-1.jpg" alt="Product" /></a></td>
                                            <td class="pro-title"><a href="#">橡木板</a></td>
                                            <td class="pro-quantity">
                                                <input type="text" placeholder="1,00000">
                                            </td>
                                            <td class="pro-remove"><a href="#"><i class="fa fa-trash-o"></i></a></td>
                                        </tr>
                                        <!-- <tr class="total">
                                            <td colspan="3" class="text-right"><strong>Total</strong></td>
                                            <td class="total-amount">$900.00</td>
                                            <td></td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- cart main wrapper end -->
    </main>

    <!-- nquiry-wrapper start -->
    <?php include_once 'inquiry.php'; ?>
    <!-- nquiry-wrapper End -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->


    <!-- footer area start -->
    <?php include_once 'footer.php'; ?>
    <!-- footer area end -->



    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="zh-Hant/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="zh-Hant/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="zh-Hant/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="zh-Hant/js/vendor/bootstrap.min.js"></script>
    <!-- slick Slider JS -->
    <script src="zh-Hant/js/plugins/slick.min.js"></script>
    <!-- Countdown JS -->
    <script src="zh-Hant/js/plugins/countdown.min.js"></script>
    <!-- Nice Select JS -->
    <script src="zh-Hant/js/plugins/nice-select.min.js"></script>
    <!-- jquery UI JS -->
    <script src="zh-Hant/js/plugins/jqueryui.min.js"></script>
    <!-- Image zoom JS -->
    <script src="zh-Hant/js/plugins/image-zoom.min.js"></script>
    <!-- image loaded js -->
    <script src="zh-Hant/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- masonry  -->
    <script src="zh-Hant/js/plugins/masonry.pkgd.min.js"></script>
    <!-- mailchimp active js -->
    <script src="zh-Hant/js/plugins/ajaxchimp.js"></script>
    <!-- contact form dynamic js -->
    <script src="zh-Hant/js/plugins/ajax-mail.js"></script>
    <!-- google map api -->
    <script src="http://ditu.google.cn/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="zh-Hant/js/plugins/google-map.js"></script>
    <!-- Main JS -->
    <script src="zh-Hant/js/main.js"></script>

    <!-- Bootstrap Form Helpers -->
    <script src="zh-Hant/js/bootstrap-formhelpers.min.js"></script>
    <script>
        function getVal() {
            // This will get the value of #timepicker
            alert($("#timepicker").val())
        }
    </script>
</body>

</html>