@extends('web.site',['pageCssId' => 'display'])
@section("display-main")

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="{!! $banner->details['pic'][0]['path'] !!}">
        </div>
        <!-- breadcrumb area end -->

        <!-- page main wrapper start -->
        <div class="shop-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">@lang('web.Menu.ProductList')</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{!! $link !!}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('web.Menu.ProductList')</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- shop main wrapper start -->
                    <div class="col-lg-3 order-1 order-lg-1">
                        <aside class="sidebar-wrapper">
                            <!-- single sidebar start -->
                            <div class="sidebar-single">
                                <h6 class="sidebar-title">@lang('web.Menu.ProductCate')</h6>

                                <div class="s-side">
                                    @if(count($productMenu) >0)
                                        <ul>
                                            @foreach($productMenu as $key=> $value)
                                                <li>
                                                    <div class="d-firstNav s-firstNav">
                                                        <i class="fa fa-arrow-circle-right mr-1"></i>
                                                        <span>{!! $value->title !!}</span>
                                                    </div>
                                                    @if(count($value['child'])>0)
                                                        <ul class="d-firstDrop s-firstDrop"  {!! $value->id == $parentId ? 'style="display:block"' : ''!!}>
                                                            @foreach($value['child'] as $v)
                                                                <li>
                                                                    <div class="d-secondNav s-secondNav">
                                                                        <a href="{!! $link !!}product/{!! $v->id !!}/{!! $id !!}">
                                                                            <i class="icon-circle"></i>
                                                                            <span>{!! $v->title !!}</span>
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                            @endforeach


                                                        </ul>
                                                    @endif

                                                </li>
                                            @endforeach


                                                <div class="d-firstNav s-firstNav">
                                                    <i class="fa fa-image mr-1"></i>
                                                    <span><a href="{!! $link !!}">@lang('web.Menu.Space')</a></span>
                                                </div>
                                            </li>
                                        </ul>
                                    @endif

                                </div>

                            </div>
                            <!-- single sidebar end -->

                        </aside>
                    </div>
                    <div class="col-lg-9 order-2 order-lg-2">
                        <div class="shop-product-wrapper">
                            <!-- shop product top wrap start -->
                            <div class="shop-top-bar">
                                <div class="row align-items-center">
                                    <div class="col-lg-7 col-md-6 order-2 order-md-1">
                                        <div class="top-bar-left">
                                            <div class="product-view-mode">
                                                <a class="active" href="#" data-target="grid-view" data-toggle="tooltip" title="Grid View"><i class="fa fa-th"></i></a>
                                                <a href="#" data-target="list-view" data-toggle="tooltip" title="List View"><i class="fa fa-list"></i></a>
                                            </div>
                                            <!-- <div class="product-amount">
                                                <p>Showing 1–16 of 21 results</p>
                                            </div> -->
                                        </div>
                                    </div>
{{--                                    <div class="col-lg-5 col-md-6 order-1 order-md-2">--}}
{{--                                        <div class="top-bar-right">--}}
{{--                                            <div class="product-short">--}}
{{--                                                <p>Sort By : </p>--}}
{{--                                                <select class="nice-select" name="sortby">--}}
{{--                                                    <option value="trending">Relevance</option>--}}
{{--                                                    <option value="sales">Name (A - Z)</option>--}}
{{--                                                    <option value="sales">Name (Z - A)</option>--}}
{{--                                                    <option value="rating">Price (Low &gt; High)</option>--}}
{{--                                                    <option value="date">Rating (Lowest)</option>--}}
{{--                                                    <option value="price-asc">Model (A - Z)</option>--}}
{{--                                                    <option value="price-asc">Model (Z - A)</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                </div>
                            </div>
                            <!-- shop product top wrap start -->

                            <!-- product item list wrapper start -->
                            <div class="shop-product-wrap grid-view row mbn-30">
                                <!-- product single item start -->

                                    <!-- product grid start -->
                                @if(count($data) >0)
                                    @foreach($data as $value)
                                        <div class="col-md-4 col-sm-6">
                                            <div class="product-item">
                                                <div class="product-thumb">
                                                    <a href="{!! $link !!}product-details/{!! $value->id !!}/{!! $id !!}">
                                                        <img src="{!! $value->pic[0]['path'] !!}" alt="{!! $value->pic[0]['title'] !!}">
                                                    </a>

{{--                                                    <div class="product-label">--}}
{{--                                                        <span>new</span>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="discount-label">--}}
{{--                                                        <span>NEW</span>--}}
{{--                                                    </div>--}}
                                                </div>
                                                <div class="product-content">
                                                    <div class="product-caption">
                                                        <h6 class="product-name">
                                                            <a href="{!! $link !!}product-details/{!! $value->id !!}/{!! $id !!}">{!! $value->title !!}</a>
                                                        </h6>


                                                    </div>

                                                </div>
                                            </div>
                                            <!-- product grid end -->

                                            <!-- product list item end -->
                                            <div class="product-list-item">
                                                <div class="product-thumb">
                                                    <a href="product-details.php">
                                                        <img src="{!! $value->pic[0]['path'] !!}" alt="{!! $value->pic[0]['title'] !!}">
                                                    </a>

{{--                                                    <div class="product-label">--}}
{{--                                                        <span>new</span>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="discount-label">--}}
{{--                                                        <span>NEW</span>--}}
{{--                                                    </div>--}}
                                                </div>
                                                <div class="product-content-list">
                                                    <h4 class="product-name"><a href="{!! $link !!}product-details/{!! $value->id !!}">{!! $value->title !!}</a></h4>

                                                    <!-- <div class="price-box">
                                                        <span class="price-old"><del>$29.99</del></span>
                                                        <span class="price-regular">$200.00</span>
                                                    </div> -->
                                                    <p>{!! $value->details['description'] ?? '' !!}
                                                    </p>

                                                </div>
                                            </div>
                                            <!-- product list item end -->
                                        </div>
                                    @endforeach
                                @endif




                            </div>
                            <!-- product item list wrapper end -->

                            <!-- start pagination area -->
                            @include('web.page')
                            <!-- end pagination area -->
                        </div>
                    </div>
                    <!-- shop main wrapper end -->
                </div>
            </div>
        </div>
        <!-- page main wrapper end -->
    </main>

    <!-- nquiry-wrapper start -->
@endsection