<?php
?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <?php include_once './head.php'; ?>
</head>

<body>
    <!-- Start Header Area -->
    <?php include_once 'header.php'; ?>
    <!-- end Header Area -->

    <!-- offcanvas search form start -->
    <div class="offcanvas-search-wrapper">
        <div class="offcanvas-search-inner">
            <div class="offcanvas-close">
                <i class="fa fa-close"></i>
            </div>
            <div class="container">
                <div class="offcanvas-search-box">
                    <form class="d-flex bdr-bottom w-100">
                        <input type="text" placeholder="Search entire storage here...">
                        <button class="search-btn"><i class="fa fa-search"></i>search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="zh-Hant/img/banner/shop.jpg">
        </div>
        <!-- breadcrumb area end -->

        <!-- page main wrapper start -->
        <div class="shop-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">商品目錄</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">商品目錄</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- shop main wrapper start -->
                    <div class="col-lg-3 order-1 order-lg-1">
                        <aside class="sidebar-wrapper">
                            <!-- single sidebar start -->
                            <div class="sidebar-single">
                                <h6 class="sidebar-title">商品分類</h6>

                                <div class="s-side">
                                    <ul>
                                        <li>
                                            <div class="d-firstNav s-firstNav">
                                                <i class="fa fa-arrow-circle-right mr-1"></i>
                                                <span>沖孔系列</span>
                                            </div>
                                            <ul class="d-firstDrop s-firstDrop">
                                                <li>
                                                    <div class="d-secondNav s-secondNav">
                                                        <a href="">
                                                            <i class="icon-circle"></i>
                                                            <span>矽酸鈣天花板</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="d-secondNav s-secondNav">
                                                        <a href="">
                                                            <i class="icon-circle"></i>
                                                            <span>矽酸鈣天花板</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="d-secondNav s-secondNav">
                                                        <a href="">
                                                            <i class="icon-circle"></i>
                                                            <span>矽酸鈣天花板</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <div class="d-firstNav s-firstNav">
                                                <i class="fa fa-arrow-circle-right mr-1"></i>
                                                <span>立體花紋系列</span>
                                            </div>
                                            <ul class="d-firstDrop s-firstDrop">
                                                <li>
                                                    <div class="d-secondNav s-secondNav">
                                                        <a href="">
                                                            <i class="icon-circle"></i>
                                                            <span>矽酸鈣天花板</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="d-secondNav s-secondNav">
                                                        <a href="">
                                                            <i class="icon-circle"></i>
                                                            <span>矽酸鈣天花板</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <div class="d-firstNav s-firstNav">
                                                <i class="fa fa-image mr-1"></i>
                                                <span><a href="display.php">空間展示</a></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <!-- single sidebar end -->

                        </aside>
                    </div>
                    <div class="col-lg-9 order-2 order-lg-2">
                        <div class="shop-product-wrapper">
                            <!-- shop product top wrap start -->
                            <div class="shop-top-bar">
                                <div class="row align-items-center">
                                    <div class="col-lg-7 col-md-6 order-2 order-md-1">
                                        <div class="top-bar-left">
                                            <div class="product-view-mode">
                                                <a class="active" href="#" data-target="grid-view" data-toggle="tooltip" title="Grid View"><i class="fa fa-th"></i></a>
                                                <a href="#" data-target="list-view" data-toggle="tooltip" title="List View"><i class="fa fa-list"></i></a>
                                            </div>
                                            <!-- <div class="product-amount">
                                                <p>Showing 1–16 of 21 results</p>
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-6 order-1 order-md-2">
                                        <div class="top-bar-right">
                                            <div class="product-short">
                                                <p>Sort By : </p>
                                                <select class="nice-select" name="sortby">
                                                    <option value="trending">Relevance</option>
                                                    <option value="sales">Name (A - Z)</option>
                                                    <option value="sales">Name (Z - A)</option>
                                                    <option value="rating">Price (Low &gt; High)</option>
                                                    <option value="date">Rating (Lowest)</option>
                                                    <option value="price-asc">Model (A - Z)</option>
                                                    <option value="price-asc">Model (Z - A)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- shop product top wrap start -->

                            <!-- product item list wrapper start -->
                            <div class="shop-product-wrap grid-view row mbn-30">
                                <!-- product single item start -->
                                <div class="col-md-4 col-sm-6">
                                    <!-- product grid start -->
                                    <div class="product-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-1.jpg" alt="product thumb">
                                            </a>

                                            <div class="product-label">
                                                <span>new</span>
                                            </div>
                                            <div class="discount-label">
                                                <span>NEW</span>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-caption">
                                                <h6 class="product-name">
                                                    <a href="product-details.php">矽酸鈣天花板</a>
                                                </h6>


                                            </div>

                                        </div>
                                    </div>
                                    <!-- product grid end -->

                                    <!-- product list item end -->
                                    <div class="product-list-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-1.jpg" alt="product thumb">
                                            </a>

                                            <div class="product-label">
                                                <span>new</span>
                                            </div>
                                            <div class="discount-label">
                                                <span>NEW</span>
                                            </div>
                                        </div>
                                        <div class="product-content-list">
                                            <h4 class="product-name"><a href="product-details.php">矽酸鈣天花板</a></h4>

                                            <!-- <div class="price-box">
                                                <span class="price-old"><del>$29.99</del></span>
                                                <span class="price-regular">$200.00</span>
                                            </div> -->
                                            <p>矽酸鈣天花板是矽質、鈣質添加物及強韌耐熱纖維，經高溫、高壓蒸氣養　
                                                　生製造而成，100%不燃、無公害的安全環保建材。
                                            </p>

                                        </div>
                                    </div>
                                    <!-- product list item end -->
                                </div>
                                <!-- product single item start -->

                                <!-- product single item start -->
                                <div class="col-md-4 col-sm-6">
                                    <!-- product grid start -->
                                    <div class="product-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-2.jpg" alt="product thumb">
                                            </a>

                                            <!-- <div class="product-label">
                                                <span>new</span>
                                            </div> -->
                                            <!-- <div class="discount-label">
                                                <span>NEW</span>
                                            </div> -->
                                        </div>
                                        <div class="product-content">
                                            <div class="product-caption">
                                                <h6 class="product-name">
                                                    <a href="product-details.php">矽酸鈣天花板</a>
                                                </h6>

                                                <!-- <a class="add-to-cart" href="cart.html"><i class="fa fa-shopping-cart"></i></a> -->
                                            </div>

                                        </div>
                                    </div>
                                    <!-- product grid end -->

                                    <!-- product list item end -->
                                    <div class="product-list-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-1.jpg" alt="product thumb">
                                            </a>

                                            <div class="product-label">
                                                <span>new</span>
                                            </div>
                                            <div class="discount-label">
                                                <span>NEW</span>
                                            </div>
                                        </div>
                                        <div class="product-content-list">
                                            <h4 class="product-name"><a href="product-details.php">矽酸鈣天花板</a></h4>


                                            <p>矽酸鈣天花板是矽質、鈣質添加物及強韌耐熱纖維，經高溫、高壓蒸氣養　
                                                　生製造而成，100%不燃、無公害的安全環保建材。
                                            </p>

                                        </div>
                                    </div>
                                    <!-- product list item end -->
                                </div>
                                <!-- product single item start -->

                                <!-- product single item start -->
                                <div class="col-md-4 col-sm-6">
                                    <!-- product grid start -->
                                    <div class="product-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-3.jpg" alt="product thumb">
                                            </a>

                                            <!-- <div class="product-label">
                                                <span>new</span>
                                            </div> -->
                                            <!-- <div class="discount-label">
                                                <span>NEW</span>
                                            </div> -->
                                        </div>
                                        <div class="product-content">
                                            <div class="product-caption">
                                                <h6 class="product-name">
                                                    <a href="product-details.php">矽酸鈣天花板</a>
                                                </h6>


                                            </div>

                                        </div>
                                    </div>
                                    <!-- product grid end -->

                                    <!-- product list item end -->
                                    <div class="product-list-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-1.jpg" alt="product thumb">
                                            </a>

                                            <div class="product-label">
                                                <span>new</span>
                                            </div>
                                            <div class="discount-label">
                                                <span>NEW</span>
                                            </div>
                                        </div>
                                        <div class="product-content-list">
                                            <h4 class="product-name"><a href="product-details.php">矽酸鈣天花板</a></h4>


                                            <p>矽酸鈣天花板是矽質、鈣質添加物及強韌耐熱纖維，經高溫、高壓蒸氣養　
                                                　生製造而成，100%不燃、無公害的安全環保建材。
                                            </p>

                                        </div>
                                    </div>
                                    <!-- product list item end -->
                                </div>
                                <!-- product single item start -->

                                <!-- product single item start -->
                                <div class="col-md-4 col-sm-6">
                                    <!-- product grid start -->
                                    <div class="product-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-4.jpg" alt="product thumb">
                                            </a>

                                            <!-- <div class="product-label">
                                                <span>new</span>
                                            </div> -->
                                            <!-- <div class="discount-label">
                                                <span>NEW</span>
                                            </div> -->
                                        </div>
                                        <div class="product-content">
                                            <div class="product-caption">
                                                <h6 class="product-name">
                                                    <a href="product-details.php">矽酸鈣天花板</a>
                                                </h6>


                                            </div>

                                        </div>
                                    </div>
                                    <!-- product grid end -->

                                    <!-- product list item end -->
                                    <div class="product-list-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-1.jpg" alt="product thumb">
                                            </a>

                                            <div class="product-label">
                                                <span>new</span>
                                            </div>
                                            <div class="discount-label">
                                                <span>NEW</span>
                                            </div>
                                        </div>
                                        <div class="product-content-list">
                                            <h4 class="product-name"><a href="product-details.php">矽酸鈣天花板</a></h4>


                                            <p>矽酸鈣天花板是矽質、鈣質添加物及強韌耐熱纖維，經高溫、高壓蒸氣養　
                                                　生製造而成，100%不燃、無公害的安全環保建材。
                                            </p>

                                        </div>
                                    </div>
                                    <!-- product list item end -->
                                </div>
                                <!-- product single item start -->

                                <!-- product single item start -->
                                <div class="col-md-4 col-sm-6">
                                    <!-- product grid start -->
                                    <div class="product-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-1.jpg" alt="product thumb">
                                            </a>

                                            <!-- <div class="product-label">
                                                <span>new</span>
                                            </div> -->
                                            <!-- <div class="discount-label">
                                                <span>NEW</span>
                                            </div> -->
                                        </div>
                                        <div class="product-content">
                                            <div class="product-caption">
                                                <h6 class="product-name">
                                                    <a href="product-details.php">矽酸鈣天花板</a>
                                                </h6>


                                            </div>

                                        </div>
                                    </div>
                                    <!-- product grid end -->

                                    <!-- product list item end -->
                                    <div class="product-list-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-1.jpg" alt="product thumb">
                                            </a>

                                            <div class="product-label">
                                                <span>new</span>
                                            </div>
                                            <div class="discount-label">
                                                <span>NEW</span>
                                            </div>
                                        </div>
                                        <div class="product-content-list">
                                            <h4 class="product-name"><a href="product-details.php">矽酸鈣天花板</a></h4>


                                            <p>矽酸鈣天花板是矽質、鈣質添加物及強韌耐熱纖維，經高溫、高壓蒸氣養　
                                                　生製造而成，100%不燃、無公害的安全環保建材。
                                            </p>

                                        </div>
                                    </div>
                                    <!-- product list item end -->
                                </div>
                                <!-- product single item start -->

                                <!-- product single item start -->
                                <div class="col-md-4 col-sm-6">
                                    <!-- product grid start -->
                                    <div class="product-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-2.jpg" alt="product thumb">
                                            </a>

                                            <!-- <div class="product-label">
                                                <span>new</span>
                                            </div> -->
                                            <!-- <div class="discount-label">
                                                <span>NEW</span>
                                            </div> -->
                                        </div>
                                        <div class="product-content">
                                            <div class="product-caption">
                                                <h6 class="product-name">
                                                    <a href="product-details.php">矽酸鈣天花板</a>
                                                </h6>


                                            </div>

                                        </div>
                                    </div>
                                    <!-- product grid end -->

                                    <!-- product list item end -->
                                    <div class="product-list-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-1.jpg" alt="product thumb">
                                            </a>

                                            <div class="product-label">
                                                <span>new</span>
                                            </div>
                                            <div class="discount-label">
                                                <span>NEW</span>
                                            </div>
                                        </div>
                                        <div class="product-content-list">
                                            <h4 class="product-name"><a href="product-details.php">矽酸鈣天花板</a></h4>


                                            <p>矽酸鈣天花板是矽質、鈣質添加物及強韌耐熱纖維，經高溫、高壓蒸氣養　
                                                　生製造而成，100%不燃、無公害的安全環保建材。
                                            </p>

                                        </div>
                                    </div>
                                    <!-- product list item end -->
                                </div>
                                <!-- product single item start -->

                                <!-- product single item start -->
                                <div class="col-md-4 col-sm-6">
                                    <!-- product grid start -->
                                    <div class="product-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-3.jpg" alt="product thumb">
                                            </a>

                                            <!-- <div class="product-label">
                                                <span>new</span>
                                            </div> -->
                                            <!-- <div class="discount-label">
                                                <span>NEW</span>
                                            </div> -->
                                        </div>
                                        <div class="product-content">
                                            <div class="product-caption">
                                                <h6 class="product-name">
                                                    <a href="product-details.php">矽酸鈣天花板</a>
                                                </h6>

                                            </div>

                                        </div>
                                    </div>
                                    <!-- product grid end -->

                                    <!-- product list item end -->
                                    <div class="product-list-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-1.jpg" alt="product thumb">
                                            </a>

                                            <div class="product-label">
                                                <span>new</span>
                                            </div>
                                            <div class="discount-label">
                                                <span>NEW</span>
                                            </div>
                                        </div>
                                        <div class="product-content-list">
                                            <h4 class="product-name"><a href="product-details.php">矽酸鈣天花板</a></h4>


                                            <p>矽酸鈣天花板是矽質、鈣質添加物及強韌耐熱纖維，經高溫、高壓蒸氣養　
                                                　生製造而成，100%不燃、無公害的安全環保建材。
                                            </p>

                                        </div>
                                    </div>
                                    <!-- product list item end -->
                                </div>
                                <!-- product single item start -->

                                <!-- product single item start -->
                                <div class="col-md-4 col-sm-6">
                                    <!-- product grid start -->
                                    <div class="product-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-4.jpg" alt="product thumb">
                                            </a>

                                            <!-- <div class="product-label">
                                                <span>new</span>
                                            </div> -->
                                            <!-- <div class="discount-label">
                                                <span>NEW</span>
                                            </div> -->
                                        </div>
                                        <div class="product-content">
                                            <div class="product-caption">
                                                <h6 class="product-name">
                                                    <a href="product-details.php">矽酸鈣天花板</a>
                                                </h6>


                                            </div>

                                        </div>
                                    </div>
                                    <!-- product grid end -->

                                    <!-- product list item end -->
                                    <div class="product-list-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-1.jpg" alt="product thumb">
                                            </a>

                                            <div class="product-label">
                                                <span>new</span>
                                            </div>
                                            <div class="discount-label">
                                                <span>NEW</span>
                                            </div>
                                        </div>
                                        <div class="product-content-list">
                                            <h4 class="product-name"><a href="product-details.php">矽酸鈣天花板</a></h4>


                                            <p>矽酸鈣天花板是矽質、鈣質添加物及強韌耐熱纖維，經高溫、高壓蒸氣養　
                                                　生製造而成，100%不燃、無公害的安全環保建材。
                                            </p>

                                        </div>
                                    </div>
                                    <!-- product list item end -->
                                </div>
                                <!-- product single item start -->

                                <!-- product single item start -->
                                <div class="col-md-4 col-sm-6">
                                    <!-- product grid start -->
                                    <div class="product-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-1.jpg" alt="product thumb">
                                            </a>

                                            <!-- <div class="product-label">
                                                <span>new</span>
                                            </div> -->
                                            <!-- <div class="discount-label">
                                                <span>NEW</span>
                                            </div> -->
                                        </div>
                                        <div class="product-content">
                                            <div class="product-caption">
                                                <h6 class="product-name">
                                                    <a href="product-details.php">矽酸鈣天花板</a>
                                                </h6>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- product grid end -->

                                    <!-- product list item end -->
                                    <div class="product-list-item">
                                        <div class="product-thumb">
                                            <a href="product-details.php">
                                                <img src="zh-Hant/img/product/product-1.jpg" alt="product thumb">
                                            </a>

                                            <div class="product-label">
                                                <span>new</span>
                                            </div>
                                            <div class="discount-label">
                                                <span>NEW</span>
                                            </div>
                                        </div>
                                        <div class="product-content-list">
                                            <h4 class="product-name"><a href="product-details.php">矽酸鈣天花板</a></h4>


                                            <p>矽酸鈣天花板是矽質、鈣質添加物及強韌耐熱纖維，經高溫、高壓蒸氣養　
                                                　生製造而成，100%不燃、無公害的安全環保建材。
                                            </p>

                                        </div>
                                    </div>
                                    <!-- product list item end -->
                                </div>
                                <!-- product single item start -->


                            </div>
                            <!-- product item list wrapper end -->

                            <!-- start pagination area -->
                            <div class="paginatoin-area text-center mb-5">
                                <ul class="pagination-box">
                                    <li><a class="previous" href="#"><i class="fa fa-angle-left"></i></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a class="next" href="#"><i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                            <!-- end pagination area -->
                        </div>
                    </div>
                    <!-- shop main wrapper end -->
                </div>
            </div>
        </div>
        <!-- page main wrapper end -->
    </main>

    <!-- nquiry-wrapper start -->
    <?php include_once 'inquiry.php'; ?>
    <!-- nquiry-wrapper End -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->


    <!-- footer area start -->
    <?php include_once 'footer.php'; ?>
    <!-- footer area end -->


    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="zh-Hant/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="zh-Hant/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="zh-Hant/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="zh-Hant/js/vendor/bootstrap.min.js"></script>
    <!-- slick Slider JS -->
    <script src="zh-Hant/js/plugins/slick.min.js"></script>
    <!-- Countdown JS -->
    <script src="zh-Hant/js/plugins/countdown.min.js"></script>
    <!-- Nice Select JS -->
    <script src="zh-Hant/js/plugins/nice-select.min.js"></script>
    <!-- jquery UI JS -->
    <script src="zh-Hant/js/plugins/jqueryui.min.js"></script>
    <!-- Image zoom JS -->
    <script src="zh-Hant/js/plugins/image-zoom.min.js"></script>
    <!-- image loaded js -->
    <script src="zh-Hant/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- masonry  -->
    <script src="zh-Hant/js/plugins/masonry.pkgd.min.js"></script>
    <!-- mailchimp active js -->
    <script src="zh-Hant/js/plugins/ajaxchimp.js"></script>
    <!-- contact form dynamic js -->
    <script src="zh-Hant/js/plugins/ajax-mail.js"></script>
    <!-- google map api -->
    <script src="http://ditu.google.cn/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="zh-Hant/js/plugins/google-map.js"></script>
    <!-- Main JS -->
    <script src="zh-Hant/js/main.js"></script>

</body>

</html>