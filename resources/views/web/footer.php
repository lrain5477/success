<!-- footer area start -->
<footer class="black-bg">

    <!-- <div class="footer-widget-area">
        <div class="container">
            <div class="row mtn-30">
                <div class="col-lg-3 col-sm-6">
                    <div class="footer-widget-item mt-30">
                        <h6 class="widget-title">CUSTOM LINKS</h6>
                        <ul class="usefull-links">
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Order Status</a></li>
                            <li><a href="#">Returns & Exchanges</a></li>
                            <li><a href="#">Size Guide</a></li>
                            <li><a href="#">Cart</a></li>
                            <li><a href="#">FAQ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="footer-widget-item mt-30">
                        <h6 class="widget-title">PRODUCTS</h6>
                        <ul class="usefull-links">
                            <li><a href="#">Prices drop</a></li>
                            <li><a href="#">New products</a></li>
                            <li><a href="#">Best sales</a></li>
                            <li><a href="#">Contact us</a></li>
                            <li><a href="#">Sitemap</a></li>
                            <li><a href="#">Stores</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="footer-widget-item mt-30">
                        <h6 class="widget-title">OUR COMPANY</h6>
                        <ul class="usefull-links">
                            <li><a href="#">Delivery</a></li>
                            <li><a href="#">Legal Notice</a></li>
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Secure payment</a></li>
                            <li><a href="#">Contact us</a></li>
                            <li><a href="#">Sitemap</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="footer-widget-item mt-30">
                        <h6 class="widget-title">YOUR ACCOUNT</h6>
                        <ul class="usefull-links">
                            <li><a href="#">Personal info</a></li>
                            <li><a href="#">Orders</a></li>
                            <li><a href="#">Credit slips</a></li>
                            <li><a href="#">Addresses</a></li>
                            <li><a href="#">Stores</a></li>
                            <li><a href="#">FAQ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="footer-middle-area">
        <div class="container">
            <div class="row mtn-30">
                <div class="col-lg-6 col-sm-12">
                    <div class="address-block mt-30">
                        <div class="footer-logo">
                            <a href="index.html">
                                <img src="zh-Hant/img/logo/logo.png" alt="Brand Logo">
                            </a>
                        </div>
                        <address class="address-info d-flex align-items-center">
                            <i class="fa fa-map-marker"></i>
                            <p><span>ADDRESS : </span> No 40 Baria Sreet 15/2 NewYork City, NY, United States.</p>
                        </address>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <address class="address-info email mt-30">
                        <i class="fa fa-envelope"></i>
                        <p><span>EMAIL : </span><a href="emailto:demo@yoursite.com">demo@yoursite.com</a></p>
                    </address>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <address class="address-info email mt-30">
                        <i class="fa fa-phone"></i>
                        <p><span>PHONE : </span><a href="tel:+880123456789">+880123456789</a></p>
                    </address>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-area text-center">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <div class="footer-link">
                        <a href="#">公司簡介</a>
                        <a href="product.php">產品目錄</a>
                        <a href="news.php">最新消息</a>
                        <a href="qa.php">Q&amp;A</a>
                        <a href="download.php">相關下載</a>
                        <a href="contact-us.php">聯絡我們</a>
                        <a href="sitemap.php">網站地圖</a>
                    </div>
                    <p class="copyright">
                        Copyright &copy; 2019.Company name All rights reserved.
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer area end -->