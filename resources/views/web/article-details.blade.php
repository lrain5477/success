@extends('web.site',['pageCssId' => 'article-details'])
@section("article-details-main")

    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="{!! $banner->details['pic'][0]['path'] !!}">
        </div>
        <!-- breadcrumb area end -->

        <!-- blog main wrapper start -->
        <div class="blog-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">@lang('web.Menu.Knowledge')</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{!! $link !!}"><i class="fa fa-home"></i></a></li>
{{--                                    <li class="breadcrumb-item"><a href="javascript:;">@lang('web.Menu.Knowledge')</a></li>--}}
                                    <li class="breadcrumb-item active" aria-current="page">{!! $data->title !!}</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3 order-1 order-lg-1">
                        <aside class="sidebar-wrapper">
                            <!-- single sidebar start -->
                            <div class="sidebar-single">
                                <h6 class="sidebar-title">@lang('web.Menu.Knowledge')</h6>
                                <div class="s-side">
                                    @if(count($knowMenu) >0)
                                        <ul>
                                            @foreach($knowMenu as $value)
                                                <li class="active">
                                                    <div class="d-firstNav s-firstNav">
                                                        <i class="fa fa-arrow-circle-right mr-1"></i>
                                                        <span>{!! $value->title !!}</span>
                                                    </div>
                                                    @if(count($value['child']))
                                                        <ul class="d-firstDrop s-firstDrop">
                                                            @foreach($value['child'] as $v)
                                                                <li>
                                                                    <div class="d-secondNav s-secondNav">
                                                                        <a href="{!! $link !!}news-details/{!! $v->id !!}/{!! $value->id !!}">
                                                                            <i class="icon-circle"></i>
                                                                            <span>{!! $v->title !!}</span>
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                            @endforeach


                                                        </ul>
                                                    @endif

                                                </li>
                                            @endforeach


                                        </ul>
                                    @endif

                                </div>

                            </div>
                            <!-- single sidebar end -->

                        </aside>
                    </div>
                    <div class="col-lg-9 order-2">
                        <div class="blog-item-wrapper">
                            <!-- blog post item start -->
                            <div class="blog-post-item blog-details-post d-block">
                                <!-- <figure class="blog-thumb w-100">
                                    <div class="blog-carousel-2 slick-row-5 slick-dot-style">
                                        <div class="blog-single-slide">
                                            <img src="zh-Hant/img/blog/blog-1.jpg" alt="blog image">
                                        </div>
                                        <div class="blog-single-slide">
                                            <img src="zh-Hant/img/blog/blog-2.jpg" alt="blog image">
                                        </div>
                                        <div class="blog-single-slide">
                                            <img src="zh-Hant/img/blog/blog-3.jpg" alt="blog image">
                                        </div>
                                    </div>
                                </figure> -->
                                <div class="news-content w-100 pl-0">

                                    <!-- <h3><span class="news-type">媒體訊息</span></h3> -->

                                    <div class="entry-summary mb-5">
                                       {!! $data ->details['editor'] !!}
                                    </div>
                                </div>
                            </div>
                            <!-- blog post item end -->




                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- blog main wrapper end -->
    </main>

@endsection
@push('scripts')

    <script type="application/ld+json">
                {
                  "@context": "http://schema.org",
                  "@type": "BreadcrumbList",
                  "itemListElement":
                  [
                    {
                      "@type": "ListItem",
                      "position": 1,
                      "item":
                      {
                      "@id": "{!! url('/') !!}",
                      "name": "首頁"
                      }
                    },
                     {
                      "@type": "ListItem",
                      "position": 2,
                      "item":
                      {
                        "@id": "{!! url()->full() !!}",
                        "name": "{!! $data->title !!}"
                      }
                    }
                  ]
                }
            </script>
@endpush