@extends('web.site',['pageCssId' => 'qa'])
@section("qa-main")
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="{!! $banner->details['pic'][0]['path'] !!}">
        </div>
        <!-- breadcrumb area end -->

        <!-- checkout main wrapper start -->
        <div class="checkout-page-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">Q&amp;A</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{!! $link !!}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active"><a href="qa.php">Q&amp;A</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <!-- Checkout Login Coupon Accordion Start -->
                        <div class="checkoutaccordion" id="checkOutAccordion">
                            @if($data ->count() >0)
                                @foreach($data as $key=> $value)
                                    <div class="card">
                                        <h6><span data-toggle="collapse" data-target="#q{!! $key+1 !!}" onclick="addTrack('{!! $value->id !!}')"><b>Q{!! $key+1 !!}.</b>{!! $value->title !!}</span></h6>
                                        <div id="q{!! $key+1 !!}" class="collapse" data-parent="#q{!! $key+1 !!}">
                                            <div class="card-body">
                                                <p>{!! $value->details['editor'] !!}
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif




                        </div>
                        <!-- Checkout Login Coupon Accordion End -->
                    </div>
                </div>

            </div>
        </div>
        <!-- checkout main wrapper end -->
    </main>

@endsection
@push('scripts')
    <script>
        function addTrack(id) {
            $.ajax({
                url: "{!! $link !!}ajaxForm",
                dataType:"json",
                headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                type:"POST",
                data:{
                    "id":id,
                    "type":"addTrack"
                }
            });
        }
    </script>

    <script type="application/ld+json">
                {
                  "@context": "http://schema.org",
                  "@type": "BreadcrumbList",
                  "itemListElement":
                  [
                    {
                      "@type": "ListItem",
                      "position": 1,
                      "item":
                      {
                      "@id": "{!! url('/') !!}",
                      "name": "首頁"
                      }
                    },
                     {
                      "@type": "ListItem",
                      "position": 2,
                      "item":
                      {
                        "@id": "{!! url()->full() !!}",
                        "name": "Q&A"
                      }
                    }
                  ]
                }
            </script>
@endpush