
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ is_null($pageTitle) ? '' : ($pageTitle) }}</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="author" content="{{ config('app.author') }}"/>
    <meta name="description" content="{{ $pageSeo['meta_description'] ?? '' }}" />
    <meta name="keywords" content="{{ $pageSeo['meta_keywords'] ?? '' }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="/static/{{$lang}}/img/favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- CSS
        ============================================ -->
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,600,700,800,900%7CPoppins:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/static/{{$lang}}/css/vendor/bootstrap.min.css">
    <!-- Font-awesome CSS -->
    <link rel="stylesheet" href="/static/{{$lang}}/css/vendor/font-awesome.min.css">

    <link rel="stylesheet" href="/static/{{$lang}}/css/vendor/bootstrap-formhelpers.min.css">
    <!-- Slick slider css -->
    <link rel="stylesheet" href="/static/{{$lang}}/css/plugins/slick.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="/static/{{$lang}}/css/plugins/animate.css">
    <!-- Nice Select css -->
    <link rel="stylesheet" href="/static/{{$lang}}/css/plugins/nice-select.css">
    <!-- jquery UI css -->
    <link rel="stylesheet" href="/static/{{$lang}}/css/plugins/jqueryui.min.css">
    <link rel="stylesheet" href="/static/{{$lang}}/css/jquery.exzoom.css">
    <!-- main style css -->
    <link rel="stylesheet" href="/static/{{$lang}}/css/style.css">
    @stack('styles')
    {!! $webData->options['head'] !!}
</head>

<body>
{!! $webData->options['body'] !!}
    @include('web.header2')

@yield($pageCssId."-main")




    <!-- nquiry-wrapper start -->
    <div class="inquiry-wrapper">
        <a href="{!! $link !!}product-inquiry">
            <i class="fa fa-calendar-check-o"></i>
        </a>
    </div>
    <!-- nquiry-wrapper End -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->

    @if($pageCssId != "home")

       @include('web.footer')
    @endif
{!! $webData->options['foot'] !!}

    <!-- JS
============================================ -->

    @include('web.foot-js')








</body>

</html>