<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>首頁 - 繼威實業</title>
<meta name="robots" content="noindex, follow" />
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon" href="zh-Hant/img/favicon.ico">

<!-- CSS
	============================================ -->
<!-- google fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,600,700,800,900%7CPoppins:300,400,500,600,700,800,900" rel="stylesheet">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="zh-Hant/css/vendor/bootstrap.min.css">
<!-- Font-awesome CSS -->
<link rel="stylesheet" href="zh-Hant/css/vendor/font-awesome.min.css">

<link rel="stylesheet" href="zh-Hant/css/vendor/bootstrap-formhelpers.min.css">
<!-- Slick slider css -->
<link rel="stylesheet" href="zh-Hant/css/plugins/slick.min.css">
<!-- animate css -->
<link rel="stylesheet" href="zh-Hant/css/plugins/animate.css">
<!-- Nice Select css -->
<link rel="stylesheet" href="zh-Hant/css/plugins/nice-select.css">
<!-- jquery UI css -->
<link rel="stylesheet" href="zh-Hant/css/plugins/jqueryui.min.css">
<link rel="stylesheet" href="zh-Hant/css/jquery.exzoom.css">
<!-- main style css -->
<link rel="stylesheet" href="zh-Hant/css/style.css">