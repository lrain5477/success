@extends('web.site',['pageCssId' => 'news-details'])
@section("news-details-main")
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="{!! $banner->details['pic'][0]['path'] !!}">
        </div>
        <!-- breadcrumb area end -->

        <!-- blog main wrapper start -->
        <div class="blog-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">@lang('web.Menu.News')</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{!! $link !!}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="{!! $link !!}news">@lang('web.Menu.News')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">{!! $data->title !!}</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 mb-3">
                        <h3 class="text-center news-title">{!! $data->title !!}</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 order-2">
                        <div class="blog-item-wrapper">
                            <!-- blog post item start -->
                            <div class="blog-post-item blog-details-post d-block">

                                <div class="news-content w-100 pl-0">

                                    <!-- <h3><span class="news-type">媒體訊息</span></h3> -->

                                    <div class="entry-summary mb-5">
                                       {!! $data->details['editor'] !!}
                                    </div>
                                </div>
                            </div>
                            <!-- blog post item end -->




                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- blog main wrapper end -->
    </main>

    <!-- nquiry-wrapper start -->
@endsection
@push('scripts')
    <script type="application/ld+json">
            {
                "@context": "https://schema.org",
                "@type": "Corporation",
            "name": "{!! $webData->company['name'] !!}",
            "description":"{{ $pageSeo['meta_description'] ?? '' }}",
                "url": "{!! url()->full() !!}",
            "logo": "{!!asset($webData->system_logo[0]['path']) !!}",
            "image": "{!! asset($webData->system_logo[0]['path']) !!}",
            "address": "{!! $webData->contact['address'] !!}",
            "faxNumber": "{!! $webData->contact['phone'] !!}",
                "contactPoint": {
                    "@type": "ContactPoint",
                    "telephone": "{!! $webData->contact['phone'] !!}",
                    "contactType": "customer service",
                    "areaServed": "TW",
                    "availableLanguage": "Chinese (Traditional)"
                },
                "sameAs": ""
            }
        </script>
    <script type="application/ld+json">
                {
                  "@context": "http://schema.org",
                  "@type": "BreadcrumbList",
                  "itemListElement":
                  [
                    {
                      "@type": "ListItem",
                      "position": 1,
                      "item":
                      {
                      "@id": "{!! url('/') !!}",
                      "name": "首頁"
                      }
                    },
                     {
                      "@type": "ListItem",
                      "position": 2,
                      "item":
                      {
                        "@id": "{!! url('/').$link.'news' !!}",
                        "name": "@lang('web.Menu.News')"
                      }
                    },
                     {
                      "@type": "ListItem",
                      "position": 3,
                      "item":
                      {
                        "@id": "{!! url()->full() !!}",
                        "name": "{!! $data->title !!}"
                      }
                    },

                  ]
                }
            </script>
@endpush
