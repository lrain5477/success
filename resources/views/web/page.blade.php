@php
    $start = 1;
  $end = $pageData->lastPage();
  $HasPages = $HasFirst = $HasLast = $HasPrevious = $HasNext = false;
  if($pageData->lastPage() > 1) $HasPages = true;
  if($pageData->currentPage() != 1) $HasPrevious = true;
  if($pageData->currentPage() != $pageData->lastPage()) $HasNext = true;

@endphp
<div class="paginatoin-area shadow-bg text-center mb-5">
    <ul class="pagination-box">
        <li><a class="previous" href="{!! $pageData->previousPageUrl() ??'javascript:;' !!}"><i class="fa fa-angle-left"></i></a></li>
        @for($i=$start;$i<=$end;$i++)
            <li class="{!! ($i == $pageData->currentPage())?"active":"" !!}"><a href="{!! $pageData->url($i) !!}">{!! $i !!}</a></li>
        @endfor

        <li><a class="next" href="{!! $pageData->nextPageUrl() ?? 'javascript:;'!!}"><i class="fa fa-angle-right"></i></a></li>
    </ul>
</div>