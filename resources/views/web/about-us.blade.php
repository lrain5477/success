@extends('web.site',['pageCssId' => 'about-us'])
@section("about-us-main")
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="{!! $banner->details['pic'][0]['path'] !!}">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- about us area start -->
        <section class="about-us section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">@lang('web.Menu.AboutUs')</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{!! $link !!}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active"><a href="{!! $link !!}about-us">@lang('web.Menu.AboutUs')</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 mb-5">
                        <div class="timeline">
                            @if($history ->count() >0)
                                @foreach($history as $value)
                                    <div class="container right">
                                        <div class="content">
                                            <h3>{!! $value->title !!}</h3>
                                            <p>{!! $value->info !!}</p>
                                        </div>
                                    </div>
                                @endforeach
                            @endif



                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-thumb js-tilt" data-tilt-perspective="1000" data-tilt-scale="1" data-tilt-speed="500" data-tilt-max="15">
                            <img class="w-100" src="/static/{!! $lang !!}/img/about/about.jpg" alt="about thumb">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- about us area end -->

         {!! $data->details['editor'] !!}
    </main>
@endsection
@push('scripts')

        <script type="application/ld+json">
                {
                  "@context": "http://schema.org",
                  "@type": "BreadcrumbList",
                  "itemListElement":
                  [
                    {
                      "@type": "ListItem",
                      "position": 1,
                      "item":
                      {
                      "@id": "{!! url('/') !!}",
                      "name": "首頁"
                      }
                    },
                     {
                      "@type": "ListItem",
                      "position": 2,
                      "item":
                      {
                        "@id": "{!! url()->full() !!}",
                        "name": "@lang('web.Menu.AboutUs')"
                      }
                    }
                  ]
                }
            </script>
@endpush