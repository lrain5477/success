<?php
?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <?php include_once './head.php'; ?>
</head>

<body>
    <!-- Start Header Area -->
    <?php include_once 'header.php'; ?>
    <!-- end Header Area -->


    <!-- offcanvas search form start -->
    <div class="offcanvas-search-wrapper">
        <div class="offcanvas-search-inner">
            <div class="offcanvas-close">
                <i class="fa fa-close"></i>
            </div>
            <div class="container">
                <div class="offcanvas-search-box">
                    <form class="d-flex bdr-bottom w-100">
                        <input type="text" placeholder="Search entire storage here...">
                        <button class="search-btn"><i class="fa fa-search"></i>search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas search form end -->

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area breadcrumb-img bg-img" data-bg="zh-Hant/img/banner/contact.jpg">
        </div>
        <!-- breadcrumb area end -->

        <!-- contact area start -->
        <div class="contact-area section-padding mb-5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h3 class="breadcrumb-title">聯絡我們</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">聯絡我們</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="contact-message">
                            <h4 class="contact-title">聯絡資訊</h4>
                            <p>若您有任何問題或建議，請填寫下表並提供聯絡方式，我們會盡快回覆您的需求或提問。</p>
                            <form id="contact-form" action="#" method="post" class="contact-form">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="name" placeholder="業主名稱 *" type="text" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="phone" placeholder="聯絡人 *" type="text" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="phone" placeholder="電話 *" type="text" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="email_address" placeholder="Email *" type="text" required>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <input name="contact_subject" placeholder="Subject *" type="text">
                                    </div>
                                    <div class="col-12">
                                        <div class="contact2-textarea text-center">
                                            <textarea placeholder="Message *" name="message" class="form-control2" required=""></textarea>
                                        </div>
                                        <div class="contact-btn">
                                            <button class="btn btn-sqr" type="submit">送出訊息</button>
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex justify-content-center">
                                        <p class="form-messege"></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="contact-info">
                            <h4 class="contact-title">聯絡我們</h4>

                            <ul>
                                <li><i class="fa fa-map-marker"></i>地址：彰化縣福興鄉福建路61號</li>
                                <li><i class="fa fa-envelope-o"></i>電子郵件： info@yourdomain.com</li>
                                <li><i class="fa fa-phone"></i>電話： +88013245657</li>
                            </ul>
                            <div class="working-time">
                                <h6>Working Hours</h6>
                                <p><span>Monday – Saturday:</span>08AM – 22PM</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- contact area end -->

        <!-- google map start -->
        <!-- <div class="map-wrap">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3643.803549636532!2d120.4332621156015!3d24.03799178364436!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34694f533f36f661%3A0xb0bb62a6eadd4696!2zNTA25b2w5YyW57ij56aP6IiI6YSJ56aP5bu66LevNjHomZ8!5e0!3m2!1szh-TW!2stw!4v1561907124715!5m2!1szh-TW!2stw" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div> -->
        <!-- google map end -->
    </main>

    <!-- nquiry-wrapper start -->
    <?php include_once 'inquiry.php'; ?>
    <!-- nquiry-wrapper End -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->


    <!-- footer area start -->
    <?php include_once 'footer.php'; ?>
    <!-- footer area end -->



    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="zh-Hant/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="zh-Hant/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="zh-Hant/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="zh-Hant/js/vendor/bootstrap.min.js"></script>
    <!-- slick Slider JS -->
    <script src="zh-Hant/js/plugins/slick.min.js"></script>
    <!-- Countdown JS -->
    <script src="zh-Hant/js/plugins/countdown.min.js"></script>
    <!-- Nice Select JS -->
    <script src="zh-Hant/js/plugins/nice-select.min.js"></script>
    <!-- jquery UI JS -->
    <script src="zh-Hant/js/plugins/jqueryui.min.js"></script>
    <!-- Image zoom JS -->
    <script src="zh-Hant/js/plugins/image-zoom.min.js"></script>
    <!-- image loaded js -->
    <script src="zh-Hant/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- masonry  -->
    <script src="zh-Hant/js/plugins/masonry.pkgd.min.js"></script>
    <!-- mailchimp active js -->
    <script src="zh-Hant/js/plugins/ajaxchimp.js"></script>
    <!-- contact form dynamic js -->
    <script src="zh-Hant/js/plugins/ajax-mail.js"></script>
    <!-- google map api -->
    <script src="http://ditu.google.cn/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="zh-Hant/js/plugins/google-map.js"></script>
    <!-- Main JS -->
    <script src="zh-Hant/js/main.js"></script>
</body>

</html>